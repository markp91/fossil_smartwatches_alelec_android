import sys
import re
import javalang
from pathlib import Path
import os
from concurrent.futures import ThreadPoolExecutor, wait

APK = list(Path(__file__).parent.glob('com.fossil.wearables.fossil*.apk'))[0].name
print("Processing", APK)

def rewrite(sample, target, verbose=False):
    try:
        print(sample, target)
        java = sample.read_text()
        print(len(java))

        java = re.sub(r'^( +)\? ', r'\1Object ', java, flags=re.MULTILINE)

        lines = java.split('\n')

        remove = []
        for i, line in enumerate(lines):
            if '/* access modifiers changed from' in line:
                correct = line.split(': ')[1].split(' ')[0]
                correct = " " if correct == "0000" else (" "+correct+" ")
                n = i+1
                lines[n] = lines[n].replace(" public ", correct)
                remove.append(i)
            if '    kotlin.jvm.internal.Intrinsics.' in line:
                line = line.replace('kotlin.jvm.internal.Intrinsics.', '// kotlin.jvm.internal.Intrinsics.')
                lines[i] = line

            if '@kotlin.Metadata' in line:
                line = line.replace('@kotlin.Metadata', '// @kotlin.Metadata')
                lines[i] = line

        for i in reversed(remove):
            lines.pop(i)

        java = "\n".join(lines)

        java = re.sub(r"( +)(static \{\n)((.*\n)+?)(\1})", r"\1/*\n\1\2\3\5\n\1*/", java)

        tree = javalang.parse.parse(java)
    #     print(tree)
        classes = [node for path, node in tree.filter(javalang.tree.ClassDeclaration)]
        fields = [node for path, node in tree.filter(javalang.tree.FieldDeclaration)]
        methods = [node for path, node in tree.filter(javalang.tree.MethodDeclaration)]
        inits = [node for path, node in tree.filter(javalang.tree.ConstructorDeclaration)]
        enums = [node for path, node in tree.filter(javalang.tree.EnumDeclaration)]
        iface = [node for path, node in tree.filter(javalang.tree.InterfaceDeclaration)]

        details = sorted(classes + fields + methods + inits + enums + iface, key=lambda x:x.position)

        lines = java.split('\n')

        interface = []

        if isinstance(details[0], javalang.tree.ClassDeclaration):
            header = "\n"\
            "import lanchon.dexpatcher.annotation.DexEdit;\n"\
            "import lanchon.dexpatcher.annotation.DexIgnore;\n"\
            "import lanchon.dexpatcher.annotation.DexAction;\n"\
            "\n"\
            "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"
            # "@DexEdit(defaultAction = DexAction.IGNORE)"

        elif isinstance(details[0], (javalang.tree.InterfaceDeclaration, javalang.tree.EnumDeclaration)):
            header = "\n"\
            "import lanchon.dexpatcher.annotation.DexEdit;\n"\
            "import lanchon.dexpatcher.annotation.DexIgnore;\n"\
            "import lanchon.dexpatcher.annotation.DexAction;\n"\
            "\n"\
            "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"

        else:
            raise ValueError(sample)

        for i in iface:
    #         print(dir(i))
    #         print(i.children)
    #         print(i.fields)
            interface.extend([n for n in i.fields if isinstance(n, javalang.tree.FieldDeclaration)])

    #     print(interface)

        pos = details[0].position.line-1
        while lines[pos-1].strip().startswith('@') or \
              lines[pos-1].strip().startswith('/* ') or \
              lines[pos-1].strip().startswith('// '):
            pos -= 1

        lines.insert(pos, header)

        for node in reversed(details[1:]):

            line = lines[node.position.line]

            if isinstance(node, javalang.tree.FieldDeclaration) and node not in interface:
                line = line.replace(' final ', ' /* final */ ')
                line = line.replace(' = ', '; // = ')
                lines[node.position.line] = line

    #             print(dir(node))
    #             print(node)

            indent = len(line) - len(line.lstrip())

            if isinstance(node, javalang.tree.ClassDeclaration):
                prev = lines[node.position.line-1]
                if "/* renamed from" in prev:
                    clash_target = prev.strip().split(' ')[3]
                    annotation = '@DexEdit(defaultAction = DexAction.IGNORE, target = "%s")' % clash_target
                else:
                    annotation = "@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)"
            else:
                annotation = "@DexIgnore"

            pos = node.position.line
            while lines[pos-1].strip().startswith('@') or \
                  lines[pos-1].strip().startswith('// ') or \
                  lines[pos-1].strip().startswith('/* '):
                pos -= 1

            lines.insert(pos, (" "*indent + annotation))


        print("Writing:", target)
        target.parent.mkdir(parents=True, exist_ok=True)
        with open(target, 'wb') as f:
            f.write(("\n".join(lines)).encode())
        # print(target.write_bytes()
        # os.system('git add %s' % target)

    except Exception as ex:
        import traceback
        traceback.print_exc()
        print("EXCEPTION IN", sample, ex)
        # raise


import os
import sys
from zipfile import ZipFile
from concurrent.futures import ThreadPoolExecutor


target = "java_decomp"

jadx_command_verbose = r'jadx-1.0.0-b1165-8ba3e935\bin\jadx -d src -e --no-imports --show-bad-code --no-inline-anonymous --escape-unicode --rename-flags none --single-class %s ' + APK

verbose = False
if len(sys.argv) > 1:
    # print(sys.argv)
    jadx = Path(sys.argv[1]).resolve()
    try:
        tail = jadx.relative_to(Path(r".\src\src\main\java").resolve())
    except ValueError:
        dest = jadx
        tail = jadx.relative_to(Path("../src/main", target).resolve())
        jadx = Path(r".\src\src\main\java") / tail
    else:
        dest = Path(r"..\src\main", target) / tail

    print("Verbose processing", jadx)

    class_name = str(tail)[:-5].replace('\\', '.').replace('/', '.')
    os.system(jadx_command_verbose % class_name)    

    try:
        rewrite(jadx, dest, True)
    except:
        print("Failed, copying instead for inspection:", dest)
        dest.parent.mkdir(parents=True, exist_ok=True)
        dest.write_text(jadx.read_text())
    sys.exit()


conversions = (
  (Path(r".\src\src\main\java\com\fossil"), Path(r"..\src\main\%s\com\fossil" % target)), 
  (Path(r".\src\src\main\java\com\misfit"), Path(r"..\src\main\%s\com\misfit" % target)), 
  (Path(r".\src\src\main\java\com\portfolio"), Path(r"..\src\main\%s\com\portfolio" % target)), 
)

dirname = APK[:-4]
os.makedirs(dirname, exist_ok=True)
os.makedirs('Anon', exist_ok=True)

# os.system('java -jar apktool-cli-all_b05f19b.jar d com.fossil.wearables.fossil-4.2.0.apk')
with ZipFile(APK, 'r') as zipObj:
   # Extract all the contents of zip file in current directory
   zipObj.extractall(dirname)

dex_files = [f for f in os.listdir(dirname) if f.endswith('.dex')]

# dexpatcher_jar = Path.home() / r'.gradle/caches/modules-2/files-2.1/dexpatcher-repo.dexpatcher.dexpatcher-tool/dexpatcher/1.8.0-alpha3/6c242e8efc14f4f662e166c76ed9b8d6b7a3e9af/dexpatcher-1.8.0-alpha3.jar'

dexpatcher_jar = None
dexpatcher = 'dexpatcher-1.8.0-alpha4.jar'
# for root, dirs, files in os.walk(Path.home() / '.gradle' / 'caches' / 'modules-2' / 'files-2.1'):
#     if dexpatcher in files:
#         dexpatcher_jar = Path(root) / dexpatcher

# dexpatcher_jar = Path.home() / '.gradle' / 'caches' / 'modules-2' / 'files-2.1' / 'dexpatcher-repo.dexpatcher.dexpatcher-tool/dexpatcher/1.8.0-alpha4/6b5e1a945d4319dd41554eca80dbb8791b2e7e14/dexpatcher-1.8.0-alpha4.jar'
dexpatcher_jar = Path(__file__).parent / '..' / '..' / 'dexpatcher-tool' / 'tool' / 'build' / 'libs' / 'dexpatcher-1.8.0-alpha4.jar'

for dex in dex_files:

    command = f'java -jar "{dexpatcher_jar.resolve()}" --deanon-source --main-plan "Anon[_Level]" --output Anon\\{dex} {dirname}\\{dex} --map-source --map ../mapping.txt --debug'
    print(command)
    ret = os.system(command)
    if ret != 0:
        raise SystemExit()

    # d2j
    command = f'dex-tools-2.1-SNAPSHOT\\d2j-dex2jar.bat --force Anon\\{dex} -o Anon\\{dex.replace(".dex", ".jar")}'
    os.system(command)

    # cfr_command = f'java -jar cfr-0.150-a986af3f.jar --outputdir src\\src\\main\\java --removeinnerclasssynthetics false --renameillegalidents true --renamedupmembers --jarfilter "com.(fossil|misfit|portfolio)" Anon\\{dex.replace(".dex", ".jar")}'
    # print(cfr_command)
    # ret = os.system(cfr_command)
    # if ret != 0:
    #     raise SystemExit()

    # Path("src").rename("src_prev")
    jadx_command = f'jadx-1.1.0\\bin\\jadx -d src -e -j 4 --show-bad-code --no-inline-anonymous --escape-unicode --respect-bytecode-access-modifiers --rename-flags all Anon\\{dex}'
    print(jadx_command)
    ret = os.system(jadx_command)
    if ret != 0:
        raise SystemExit()
    pass

with ThreadPoolExecutor(max_workers=1) as executor:

    for jadx, dest in conversions:

        args = []
        for root, dirs, files in os.walk(jadx):
            for f in files:
                # if 'com/fossil/blesdk/obfuscated' in root.replace('\\', '/'):
                #     continue

                sample = Path(os.path.join(root, f))

                rel = os.path.relpath(root, jadx)
                target = dest / rel / f

                if target.exists() and False:

                    # print("Not replacing", target)
                    pass
                else:
                    args.append((sample, target))

                    # try:
                    #     futures.append(executor.submit(rewrite, ))
                    # except:
                    #     print(sample)
                    #     # raise
                    # # executor.submit(os.system, "git add %s" % target)
        print(len(args))
        for _ in executor.map(lambda x: rewrite(*x), args):
            pass
        os.system('git add %s' % dest)