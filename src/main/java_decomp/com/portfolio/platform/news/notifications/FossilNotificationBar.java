package com.portfolio.platform.news.notifications;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.core.app.TaskStackBuilder;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.ll6;
import com.fossil.pn4$a$a;
import com.fossil.pn4$a$b;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.NotificationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Action;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.uirenew.home.HomeActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilNotificationBar {
    @DexIgnore
    public static /* final */ a c; // = new a((qg6) null);
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public static /* synthetic */ void b(a aVar, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.b(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateData() - context=" + context);
            try {
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new pn4$a$a(context, (xe6) null), 3, (Object) null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
            }
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final void b(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotificationWithRichContent " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.get.instance().e())) {
                NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, a(context, (int) Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
                return;
            }
            NotificationUtils.Companion.getInstance().updateRichTextNotification(context, 1, fossilNotificationBar.b, fossilNotificationBar.a, (PendingIntent) null, (PendingIntent) null, z);
        }

        @DexIgnore
        public final void a(Context context, String str, String str2) {
            wg6.b(context, "context");
            wg6.b(str, Explore.COLUMN_TITLE);
            wg6.b(str2, "content");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateData() - title=" + str + " - content=" + str2);
            try {
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new pn4$a$b(str, str2, context, (xe6) null), 3, (Object) null);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "updateData - ex=" + e);
                e.printStackTrace();
            }
        }

        @DexIgnore
        public final void a(Context context, Service service, boolean z) {
            wg6.b(context, "context");
            wg6.b(service, Constants.SERVICE);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "startForegroundNotification() - context=" + context + ", service=" + service + ", isStopForeground = " + z);
            try {
                NotificationUtils.Companion.getInstance().startForegroundNotification(context, service, "", "", z);
                a(context);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.e("FossilNotificationBar", "startForegroundNotification() - ex=" + e);
            }
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, FossilNotificationBar fossilNotificationBar, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.a(context, fossilNotificationBar, z);
        }

        @DexIgnore
        public final void a(Context context, FossilNotificationBar fossilNotificationBar, boolean z) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FossilNotificationBar", "updateNotification content " + fossilNotificationBar.a);
            if (!TextUtils.isEmpty(PortfolioApp.get.instance().e())) {
                NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, a(context, (int) Action.DisplayMode.ACTIVITY, 0), a(context, ".news.notifications.NotificationReceiver", 1), z);
                return;
            }
            NotificationUtils.Companion.getInstance().updateNotification(context, 1, fossilNotificationBar.a, (PendingIntent) null, (PendingIntent) null, z);
        }

        @DexIgnore
        public final PendingIntent a(Context context, int i, int i2) {
            Intent intent = new Intent(context, HomeActivity.class);
            intent.putExtra("OUT_STATE_DASHBOARD_CURRENT_TAB", i2);
            TaskStackBuilder b = TaskStackBuilder.a(context).b(intent);
            wg6.a((Object) b, "TaskStackBuilder.create(\u2026ntWithParentStack(intent)");
            return b.a(i, 134217728);
        }

        @DexIgnore
        public final PendingIntent a(Context context, String str, int i) {
            Intent intent = new Intent(context, NotificationReceiver.class);
            intent.setAction(str);
            intent.putExtra("ACTION_EVENT", i);
            return PendingIntent.getBroadcast(context, Action.DisplayMode.DATE, intent, 134217728);
        }
    }

    @DexIgnore
    public FossilNotificationBar() {
        this((String) null, (String) null, 3, (qg6) null);
    }

    @DexIgnore
    public FossilNotificationBar(String str, String str2) {
        wg6.b(str, "mContent");
        wg6.b(str2, "mTitle");
        this.a = str;
        this.b = str2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ FossilNotificationBar(String str, String str2, int i, qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2);
        if ((i & 1) != 0) {
            str = jm4.a((Context) PortfolioApp.get.instance(), 2131886813);
            wg6.a((Object) str, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
        }
    }
}
