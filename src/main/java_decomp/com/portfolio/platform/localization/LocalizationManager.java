package com.portfolio.platform.localization;

import android.app.Activity;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ik6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.km4$f$a;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.lm4;
import com.fossil.nc6;
import com.fossil.nm4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yj6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.response.ResponseKt;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalizationManager extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public static /* final */ a i; // = new a((qg6) null);
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public WeakReference<Activity> b;
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public d d;
    @DexIgnore
    public /* final */ Application.ActivityLifecycleCallbacks e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ GuestApiService g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return LocalizationManager.h;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:27:0x0080  */
        public final boolean a(String str, String str2) {
            wg6.b(str, "filePath");
            if (str2 == null) {
                return true;
            }
            FileInputStream fileInputStream = null;
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                byte[] bArr = new byte[2014];
                FileInputStream fileInputStream2 = new FileInputStream(str);
                while (true) {
                    try {
                        int read = fileInputStream2.read(bArr);
                        if (read == -1) {
                            byte[] digest = instance.digest();
                            wg6.a((Object) digest, "digest.digest()");
                            String a = a(digest);
                            String lowerCase = str2.toLowerCase();
                            wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                            boolean a2 = wg6.a((Object) lowerCase, (Object) a);
                            fileInputStream2.close();
                            return a2;
                        } else if (read > 0) {
                            instance.update(bArr, 0, read);
                        }
                    } catch (Exception e) {
                        e = e;
                        fileInputStream = fileInputStream2;
                    } catch (Throwable th) {
                        th = th;
                        fileInputStream = fileInputStream2;
                        if (fileInputStream != null) {
                            fileInputStream.close();
                        }
                        throw th;
                    }
                }
            } catch (Exception e2) {
                e = e2;
                try {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a3 = a();
                    local.e(a3, "Error inside " + a() + ".verifyDownloadFile - e=" + e);
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                    return false;
                } catch (Throwable th2) {
                    th = th2;
                    if (fileInputStream != null) {
                    }
                    throw th;
                }
            }
        }

        @DexIgnore
        public final String a(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & 255) + 256, 16);
                wg6.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    wg6.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            wg6.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public String a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public c(LocalizationManager localizationManager, String str, boolean z) {
            wg6.b(str, "mFilePath");
            this.c = localizationManager;
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            wg6.b(strArr, "params");
            boolean z = true;
            try {
                if (wg6.a((Object) jm4.b(), (Object) "en_US")) {
                    nm4.a(this.a, this.b);
                    nm4.a(this.c.d() + "/strings.json", true);
                } else {
                    nm4.a(this.a, this.b);
                }
                nm4.b();
            } catch (Exception e) {
                FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "load cache failed e=" + e);
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Application.ActivityLifecycleCallbacks {
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager a;

        @DexIgnore
        public f(LocalizationManager localizationManager) {
            this.a = localizationManager;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
            r7 = r7.getClass();
         */
        @DexIgnore
        public void onActivityCreated(Activity activity, Bundle bundle) {
            Class<?> cls;
            if (activity != null) {
                this.a.a((WeakReference<Activity>) new WeakReference(activity));
                WeakReference<Activity> h = this.a.h();
                if (h != null) {
                    Activity activity2 = (Activity) h.get();
                    String simpleName = (activity2 == null || cls == null) ? null : cls.getSimpleName();
                    if (wg6.a((Object) simpleName, (Object) this.a.f())) {
                        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new km4$f$a((xe6) null, this), 3, (Object) null);
                        return;
                    }
                    return;
                }
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public void onActivityDestroyed(Activity activity) {
            wg6.b(activity, Constants.ACTIVITY);
            if (this.a.h() != null) {
                WeakReference<Activity> h = this.a.h();
                if (h == null) {
                    wg6.a();
                    throw null;
                } else if (wg6.a((Object) (Activity) h.get(), (Object) activity)) {
                    this.a.a((WeakReference<Activity>) null);
                }
            }
        }

        @DexIgnore
        public void onActivityPaused(Activity activity) {
            wg6.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityResumed(Activity activity) {
            wg6.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            wg6.b(activity, Constants.ACTIVITY);
            wg6.b(bundle, "bundle");
        }

        @DexIgnore
        public void onActivityStarted(Activity activity) {
            wg6.b(activity, Constants.ACTIVITY);
        }

        @DexIgnore
        public void onActivityStopped(Activity activity) {
            wg6.b(activity, Constants.ACTIVITY);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.localization.LocalizationManager", f = "LocalizationManager.kt", l = {191}, m = "downloadLanguagePack")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(LocalizationManager localizationManager, xe6 xe6) {
            super(xe6);
            this.this$0 = localizationManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.localization.LocalizationManager$downloadLanguagePack$response$1", f = "LocalizationManager.kt", l = {191}, m = "invokeSuspend")
    public static final class h extends sf6 implements hg6<xe6<? super rx6<ApiResponse<ku3>>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(LocalizationManager localizationManager, xe6 xe6) {
            super(1, xe6);
            this.this$0 = localizationManager;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new h(this.this$0, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((h) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GuestApiService b = this.this$0.g;
                String a2 = this.this$0.f;
                this.label = 1;
                obj = b.getLocalizations(a2, "android", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    /*
    static {
        String simpleName = LocalizationManager.class.getSimpleName();
        wg6.a((Object) simpleName, "LocalizationManager::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public LocalizationManager(Application application, String str, GuestApiService guestApiService) {
        wg6.b(application, "app");
        wg6.b(str, "mAppVersion");
        wg6.b(guestApiService, "mGuestApiService");
        this.f = str;
        this.g = guestApiService;
        wg6.a((Object) application.getSharedPreferences(application.getPackageName() + ".language", 0), "app.getSharedPreferences\u2026e\", Context.MODE_PRIVATE)");
        this.a = application;
        this.e = new f(this);
    }

    @DexIgnore
    public final String c() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wg6.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(ZendeskConfig.SLASH);
        sb.append(e());
        return sb.toString();
    }

    @DexIgnore
    public final String d() {
        StringBuilder sb = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wg6.a((Object) filesDir, "mContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(ZendeskConfig.SLASH);
        sb.append("language");
        sb.append("/values");
        return sb.toString();
    }

    @DexIgnore
    public final String e() {
        StringBuilder sb = new StringBuilder("");
        StringBuilder sb2 = new StringBuilder();
        File filesDir = this.a.getFilesDir();
        wg6.a((Object) filesDir, "mContext.filesDir");
        sb2.append(filesDir.getAbsolutePath());
        sb2.append(ZendeskConfig.SLASH);
        sb2.append("language");
        File file = new File(sb2.toString());
        if (file.exists()) {
            String[] list = file.list();
            wg6.a((Object) list, "file.list()");
            Locale a2 = jm4.a();
            int length = list.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    break;
                }
                String str = list[i2];
                wg6.a((Object) a2, "l");
                String language = a2.getLanguage();
                wg6.a((Object) language, "l.language");
                if (yj6.a((CharSequence) str, (CharSequence) language, false, 2, (Object) null)) {
                    String sb3 = sb.toString();
                    wg6.a((Object) sb3, "path.toString()");
                    String language2 = a2.getLanguage();
                    wg6.a((Object) language2, "l.language");
                    if (!yj6.a((CharSequence) sb3, (CharSequence) language2, false, 2, (Object) null)) {
                        sb.append(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR);
                        sb.append(a2.getLanguage());
                    }
                    String country = a2.getCountry();
                    wg6.a((Object) country, "l.country");
                    if (yj6.a((CharSequence) str, (CharSequence) country, false, 2, (Object) null)) {
                        sb.append("-r");
                        sb.append(a2.getCountry());
                        break;
                    }
                }
                i2++;
            }
        }
        sb.insert(0, "language/values");
        sb.append("/strings.json");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "path=" + sb.toString());
        String sb4 = sb.toString();
        wg6.a((Object) sb4, "path.toString()");
        return sb4;
    }

    @DexIgnore
    public final String f() {
        return this.c;
    }

    @DexIgnore
    public final Context g() {
        return this.a;
    }

    @DexIgnore
    public final WeakReference<Activity> h() {
        return this.b;
    }

    @DexIgnore
    public final void i() {
        nm4.a();
        String c2 = c();
        if (new File(c2).exists()) {
            a(c2, true);
        } else {
            a(e(), false);
        }
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Activity activity;
        wg6.b(context, "context");
        if (intent != null && !TextUtils.isEmpty(intent.getAction()) && wg6.a((Object) intent.getAction(), (Object) "android.intent.action.LOCALE_CHANGED")) {
            PortfolioApp.get.instance().R();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = h;
            local.d(str, "onReceive locale=" + jm4.b());
            WeakReference<Activity> weakReference = this.b;
            if (!(weakReference == null || (activity = (Activity) weakReference.get()) == null)) {
                wg6.a((Object) activity, "it");
                if (!activity.isFinishing()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = h;
                    local2.d(str2, "onReceive finish activity=" + activity.getLocalClassName());
                    activity.setResult(0);
                    activity.finishAffinity();
                }
            }
            PortfolioApp.get.instance().A();
            i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            wg6.b(strArr, "params");
            String str = strArr[0];
            LocalizationManager localizationManager = LocalizationManager.this;
            return Boolean.valueOf(localizationManager.a(localizationManager.g()));
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    LocalizationManager.this.a(new File(LocalizationManager.this.g().getFilesDir().toString() + ""));
                    LocalizationManager localizationManager = LocalizationManager.this;
                    StringBuilder sb = new StringBuilder();
                    File filesDir = LocalizationManager.this.g().getFilesDir();
                    wg6.a((Object) filesDir, "mContext.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append(ZendeskConfig.SLASH);
                    sb.append(LocalizationManager.this.e());
                    localizationManager.a(sb.toString(), true);
                }
                d b = LocalizationManager.this.b();
                if (b != null) {
                    b.a(bool.booleanValue());
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(WeakReference<Activity> weakReference) {
        this.b = weakReference;
    }

    @DexIgnore
    public final d b() {
        return this.d;
    }

    @DexIgnore
    public final Application.ActivityLifecycleCallbacks a() {
        return this.e;
    }

    @DexIgnore
    public final void a(String str, boolean z) {
        wg6.b(str, "path");
        new c(this, str, z).execute(new String[0]);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "splashScreen");
        this.c = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005e  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00b4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(xe6<? super cd6> xe6) {
        g gVar;
        int i2;
        LocalizationManager localizationManager;
        ap4 ap4;
        d dVar;
        ApiResponse apiResponse;
        List list;
        if (xe6 instanceof g) {
            gVar = (g) xe6;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(h, "downloadLanguagePack() called");
                    h hVar = new h(this, (xe6) null);
                    gVar.L$0 = this;
                    gVar.label = 1;
                    obj = ResponseKt.a(hVar, gVar);
                    if (obj == a2) {
                        return a2;
                    }
                    localizationManager = this;
                } else if (i2 == 1) {
                    localizationManager = (LocalizationManager) gVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = h;
                    StringBuilder sb = new StringBuilder();
                    sb.append("download language success isFromCache ");
                    cp4 cp4 = (cp4) ap4;
                    sb.append(cp4.b());
                    local.d(str, sb.toString());
                    if (!cp4.b() && (apiResponse = (ApiResponse) cp4.a()) != null && (list = apiResponse.get_items()) != null && (!list.isEmpty())) {
                        lm4 lm4 = new lm4();
                        lm4.a((ku3) list.get(0));
                        localizationManager.a(lm4);
                    }
                    d dVar2 = localizationManager.d;
                    if (dVar2 != null) {
                        dVar2.a(false);
                    }
                } else if ((ap4 instanceof zo4) && (dVar = localizationManager.d) != null) {
                    dVar.a(false);
                }
                return cd6.a;
            }
        }
        gVar = new g(this, xe6);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final void a(lm4 lm4) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = h;
        local.d(str, "downloadFile response=" + lm4);
        new b(this, this.a, lm4).execute(new String[0]);
    }

    @DexIgnore
    public final void a(File file) {
        wg6.b(file, "folder");
        if (file.exists() && file.isDirectory()) {
            for (File file2 : file.listFiles()) {
                wg6.a((Object) file2, "f");
                if (file2.isDirectory()) {
                    a(file2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends AsyncTask<String, Void, Boolean> {
        @DexIgnore
        public /* final */ Context a;
        @DexIgnore
        public /* final */ lm4 b;
        @DexIgnore
        public /* final */ /* synthetic */ LocalizationManager c;

        @DexIgnore
        public b(LocalizationManager localizationManager, Context context, lm4 lm4) {
            wg6.b(context, "context");
            wg6.b(lm4, Firmware.COLUMN_DOWNLOAD_URL);
            this.c = localizationManager;
            this.a = context;
            this.b = lm4;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
            r2.flush();
            r2.close();
         */
        @DexIgnore
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            BufferedInputStream bufferedInputStream;
            FileOutputStream fileOutputStream;
            wg6.b(strArr, "links");
            if (!TextUtils.isEmpty(this.b.b())) {
                try {
                    StringBuilder sb = new StringBuilder();
                    File filesDir = this.a.getFilesDir();
                    wg6.a((Object) filesDir, "context.filesDir");
                    sb.append(filesDir.getAbsolutePath());
                    sb.append(ZendeskConfig.SLASH);
                    sb.append("language.zip");
                    String sb2 = sb.toString();
                    if (new File(sb2).exists() && LocalizationManager.i.a(sb2, this.b.a())) {
                        return true;
                    }
                    URLConnection openConnection = new URL(this.b.b()).openConnection();
                    openConnection.connect();
                    bufferedInputStream = new BufferedInputStream(openConnection.getInputStream());
                    fileOutputStream = new FileOutputStream(sb2);
                    try {
                        byte[] bArr = new byte[1024];
                        while (true) {
                            int read = bufferedInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            } else if (isCancelled()) {
                                fileOutputStream.flush();
                                fileOutputStream.close();
                                bufferedInputStream.close();
                                return false;
                            } else {
                                fileOutputStream.write(bArr, 0, read);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        fileOutputStream.flush();
                        fileOutputStream.close();
                    }
                    bufferedInputStream.close();
                    if (TextUtils.isEmpty(this.b.a())) {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "Download complete with risk cause by empty checksum");
                        return true;
                    } else if (LocalizationManager.i.a(sb2, this.b.a())) {
                        return true;
                    } else {
                        FLogger.INSTANCE.getLocal().e(LocalizationManager.i.a(), "Inconsistent checksum, retry download?");
                        return true;
                    }
                } catch (Exception e2) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a2 = LocalizationManager.i.a();
                    local.e(a2, "Error inside " + LocalizationManager.i.a() + ".onHandleIntent - e=" + e2);
                } catch (Throwable th) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                    bufferedInputStream.close();
                    throw th;
                }
            }
            return false;
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
            if (bool != null) {
                if (bool.booleanValue()) {
                    new e().execute(new String[]{this.b.c()});
                }
                d b2 = this.c.b();
                if (b2 != null) {
                    b2.a(false);
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00e5 A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00ea A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00f5 A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00fa A[Catch:{ Exception -> 0x00fe }] */
    /* JADX WARNING: Removed duplicated region for block: B:52:? A[Catch:{ Exception -> 0x00fe }, RETURN, SYNTHETIC] */
    public final boolean a(Context context) {
        wg6.b(context, "ctx");
        FLogger.INSTANCE.getLocal().d(h, "unzipFile");
        byte[] bArr = new byte[2048];
        try {
            StringBuilder sb = new StringBuilder();
            File filesDir = context.getFilesDir();
            wg6.a((Object) filesDir, "ctx.filesDir");
            sb.append(filesDir.getAbsolutePath());
            sb.append(ZendeskConfig.SLASH);
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder();
            File filesDir2 = context.getFilesDir();
            wg6.a((Object) filesDir2, "ctx.filesDir");
            sb3.append(filesDir2.getAbsolutePath());
            sb3.append(ZendeskConfig.SLASH);
            sb3.append("language.zip");
            String sb4 = sb3.toString();
            FLogger.INSTANCE.getLocal().d(h, "filePath=" + sb4);
            if (!new File(sb4).exists()) {
                return false;
            }
            FileInputStream fileInputStream = new FileInputStream(sb4);
            ZipInputStream zipInputStream = new ZipInputStream(fileInputStream);
            FileOutputStream fileOutputStream = null;
            ZipEntry zipEntry = null;
            while (true) {
                try {
                    zipEntry = zipInputStream.getNextEntry();
                    if (zipEntry == null) {
                        break;
                    } else if (zipEntry.isDirectory()) {
                        String name = zipEntry.getName();
                        wg6.a((Object) name, "ze.name");
                        a(sb2, name);
                    } else {
                        FileOutputStream fileOutputStream2 = new FileOutputStream(sb2 + zipEntry.getName());
                        while (true) {
                            try {
                                int read = zipInputStream.read(bArr);
                                if (read <= 0) {
                                    break;
                                }
                                fileOutputStream2.write(bArr, 0, read);
                            } catch (Exception e2) {
                                e = e2;
                                fileOutputStream = fileOutputStream2;
                                try {
                                    e.printStackTrace();
                                    zipInputStream.close();
                                    fileInputStream.close();
                                    if (zipEntry != null) {
                                        zipInputStream.closeEntry();
                                    }
                                    if (fileOutputStream != null) {
                                        return true;
                                    }
                                    fileOutputStream.close();
                                    return true;
                                } catch (Throwable th) {
                                    th = th;
                                    zipInputStream.close();
                                    fileInputStream.close();
                                    if (zipEntry != null) {
                                        zipInputStream.closeEntry();
                                    }
                                    if (fileOutputStream != null) {
                                        fileOutputStream.close();
                                    }
                                    throw th;
                                }
                            } catch (Throwable th2) {
                                th = th2;
                                fileOutputStream = fileOutputStream2;
                                zipInputStream.close();
                                fileInputStream.close();
                                if (zipEntry != null) {
                                }
                                if (fileOutputStream != null) {
                                }
                                throw th;
                            }
                        }
                        fileOutputStream = fileOutputStream2;
                    }
                } catch (Exception e3) {
                    e = e3;
                    e.printStackTrace();
                    zipInputStream.close();
                    fileInputStream.close();
                    if (zipEntry != null) {
                    }
                    if (fileOutputStream != null) {
                    }
                }
            }
            zipInputStream.close();
            fileInputStream.close();
            if (fileOutputStream == null) {
                return true;
            }
            fileOutputStream.close();
            return true;
        } catch (Exception e4) {
            FLogger.INSTANCE.getLocal().e(h, "Unzipping failed ex=" + e4);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, String str2) {
        File file = new File(str + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
    }

    @DexIgnore
    public final LocalizationManager a(d dVar) {
        this.d = dVar;
        return this;
    }
}
