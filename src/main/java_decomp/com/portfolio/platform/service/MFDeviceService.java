package com.portfolio.platform.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.ce;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fitness.FitnessData;
import com.fossil.gi4;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.hl4;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jk4;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.li4;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mi4;
import com.fossil.mw5;
import com.fossil.n24;
import com.fossil.nc6;
import com.fossil.ni4;
import com.fossil.oi4;
import com.fossil.qg6;
import com.fossil.qh4;
import com.fossil.ri4;
import com.fossil.rm6;
import com.fossil.rn4;
import com.fossil.rp4;
import com.fossil.s04;
import com.fossil.sf6;
import com.fossil.si4;
import com.fossil.u04;
import com.fossil.wearables.fsl.location.DeviceLocation;
import com.fossil.wearables.fsl.location.LocationProvider;
import com.fossil.wg6;
import com.fossil.ww5;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xx5;
import com.fossil.zl6;
import com.fossil.zm4;
import com.fossil.zx5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.db.DataFile;
import com.misfit.frameworks.buttonservice.db.DataFileProvider;
import com.misfit.frameworks.buttonservice.enums.ConnectionStateChange;
import com.misfit.frameworks.buttonservice.enums.HeartRateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.helper.WatchParamHelper;
import com.portfolio.platform.manager.LinkStreamingManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.receiver.RestartServiceReceiver;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.usecase.DianaSyncDataProcessing;
import com.portfolio.platform.service.usecase.HybridSyncDataProcessing;
import com.portfolio.platform.usecase.VerifySecretKeyUseCase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MFDeviceService extends Service {
    @DexIgnore
    public static /* final */ String T;
    @DexIgnore
    public static /* final */ String U;
    @DexIgnore
    public static /* final */ a V; // = new a((qg6) null);
    @DexIgnore
    public ik4 A;
    @DexIgnore
    public WatchParamHelper B;
    @DexIgnore
    public MisfitDeviceProfile C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public /* final */ rn4 E; // = new rn4();
    @DexIgnore
    public /* final */ Gson F; // = new Gson();
    @DexIgnore
    public /* final */ i G; // = new i(this);
    @DexIgnore
    public boolean H;
    @DexIgnore
    public Handler I;
    @DexIgnore
    public /* final */ Runnable J; // = new m(this);
    @DexIgnore
    public /* final */ d K; // = new d(this);
    @DexIgnore
    public /* final */ e L; // = new e(this);
    @DexIgnore
    public /* final */ k M; // = new k();
    @DexIgnore
    public /* final */ o N; // = new o();
    @DexIgnore
    public /* final */ j O; // = new j();
    @DexIgnore
    public /* final */ f P; // = new f();
    @DexIgnore
    public /* final */ h Q; // = new h();
    @DexIgnore
    public /* final */ n R; // = new n();
    @DexIgnore
    public /* final */ c S; // = new c(this);
    @DexIgnore
    public /* final */ b a; // = new b();
    @DexIgnore
    public an4 b;
    @DexIgnore
    public DeviceRepository c;
    @DexIgnore
    public ActivitiesRepository d;
    @DexIgnore
    public SummariesRepository e;
    @DexIgnore
    public SleepSessionsRepository f;
    @DexIgnore
    public SleepSummariesRepository g;
    @DexIgnore
    public UserRepository h;
    @DexIgnore
    public HybridPresetRepository i;
    @DexIgnore
    public MicroAppRepository j;
    @DexIgnore
    public HeartRateSampleRepository o;
    @DexIgnore
    public HeartRateSummaryRepository p;
    @DexIgnore
    public WorkoutSessionRepository q;
    @DexIgnore
    public FitnessDataRepository r;
    @DexIgnore
    public GoalTrackingRepository s;
    @DexIgnore
    public u04 t;
    @DexIgnore
    public AnalyticsHelper u;
    @DexIgnore
    public PortfolioApp v;
    @DexIgnore
    public LinkStreamingManager w;
    @DexIgnore
    public ThirdPartyRepository x;
    @DexIgnore
    public mw5 y;
    @DexIgnore
    public VerifySecretKeyUseCase z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return MFDeviceService.U;
        }

        @DexIgnore
        public final String b() {
            return MFDeviceService.T;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends Binder {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final MFDeviceService a() {
            return MFDeviceService.this;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        public c(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            if (intent != null) {
                String stringExtra = intent.getStringExtra("message");
                MFDeviceService mFDeviceService = this.a;
                wg6.a((Object) stringExtra, "message");
                mFDeviceService.a(stringExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {278, 289, 385, 424, 469, 487}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int I$0;
            @DexIgnore
            public int I$1;
            @DexIgnore
            public int I$2;
            @DexIgnore
            public int I$3;
            @DexIgnore
            public int I$4;
            @DexIgnore
            public int I$5;
            @DexIgnore
            public int I$6;
            @DexIgnore
            public long J$0;
            @DexIgnore
            public long J$1;
            @DexIgnore
            public Object L$0;
            @DexIgnore
            public Object L$1;
            @DexIgnore
            public Object L$10;
            @DexIgnore
            public Object L$11;
            @DexIgnore
            public Object L$12;
            @DexIgnore
            public Object L$2;
            @DexIgnore
            public Object L$3;
            @DexIgnore
            public Object L$4;
            @DexIgnore
            public Object L$5;
            @DexIgnore
            public Object L$6;
            @DexIgnore
            public Object L$7;
            @DexIgnore
            public Object L$8;
            @DexIgnore
            public Object L$9;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$a")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$a  reason: collision with other inner class name */
            public static final class C0060a implements m24.e<ww5.d, ww5.c> {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public C0060a(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                /* renamed from: a */
                public void onSuccess(VerifySecretKeyUseCase.d dVar) {
                    wg6.b(dVar, "responseValue");
                    PortfolioApp c = this.a.this$0.a.c();
                    String str = this.b;
                    wg6.a((Object) str, "serial");
                    c.b(str, dVar.a());
                }

                @DexIgnore
                public void a(VerifySecretKeyUseCase.c cVar) {
                    wg6.b(cVar, "errorValue");
                    PortfolioApp c = this.a.this$0.a.c();
                    String str = this.b;
                    wg6.a((Object) str, "serial");
                    c.b(str, (String) null);
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$8", f = "MFDeviceService.kt", l = {521}, m = "invokeSuspend")
            public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public b(a aVar, String str, String str2, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    b bVar = new b(this.this$0, this.$serial, this.$secretKey, xe6);
                    bVar.p$ = (il6) obj;
                    return bVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        DeviceRepository d = this.this$0.this$0.a.d();
                        String str = this.$serial;
                        if (str != null) {
                            String str2 = this.$secretKey;
                            if (str2 != null) {
                                this.L$0 = il6;
                                this.label = 1;
                                obj = d.updateDeviceSecretKey(str, str2, this);
                                if (obj == a) {
                                    return a;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else if (i == 1) {
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (((ap4) obj) instanceof cp4) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String b = MFDeviceService.V.b();
                        local.d(b, "update secret key " + this.$secretKey + " for " + this.$serial + " to server success");
                        PortfolioApp.get.instance().b(this.$serial, true);
                    } else {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.V.b();
                        local2.d(b2, "update secret key " + this.$secretKey + " for " + this.$serial + " to server failed");
                        PortfolioApp.get.instance().b(this.$serial, false);
                    }
                    return cd6.a;
                }
            }

            @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
            @lf6(c = "com.portfolio.platform.service.MFDeviceService$bleActionServiceReceiver$1$onReceive$1$9", f = "MFDeviceService.kt", l = {550}, m = "invokeSuspend")
            public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ String $secretKey;
                @DexIgnore
                public /* final */ /* synthetic */ String $serial;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public Object L$3;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public c(a aVar, String str, String str2, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$serial = str;
                    this.$secretKey = str2;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    c cVar = new c(this.this$0, this.$serial, this.$secretKey, xe6);
                    cVar.p$ = (il6) obj;
                    return cVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.mw5] */
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        MFUser currentUser = this.this$0.this$0.a.r().getCurrentUser();
                        if (currentUser != null) {
                            String str = currentUser.getUserId() + ':' + this.$serial;
                            Object e = this.this$0.this$0.a.e();
                            String str2 = this.$secretKey;
                            if (str2 != null) {
                                mw5.b bVar = new mw5.b(str, str2);
                                this.L$0 = il6;
                                this.L$1 = currentUser;
                                this.L$2 = currentUser;
                                this.L$3 = str;
                                this.label = 1;
                                obj = n24.a(e, bVar, this);
                                if (obj == a) {
                                    return a;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        return cd6.a;
                    } else if (i == 1) {
                        String str3 = (String) this.L$3;
                        MFUser mFUser = (MFUser) this.L$2;
                        MFUser mFUser2 = (MFUser) this.L$1;
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
                    return cd6.a;
                }
            }

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$d$a$d")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$d$a$d  reason: collision with other inner class name */
            public static final class C0061d extends sf6 implements ig6<il6, xe6<? super ap4<Void>>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ int $batteryLevel$inlined;
                @DexIgnore
                public /* final */ /* synthetic */ Device $device$inlined;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0061d(xe6 xe6, a aVar, int i, Device device) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$batteryLevel$inlined = i;
                    this.$device$inlined = device;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0061d dVar = new C0061d(xe6, this.this$0, this.$batteryLevel$inlined, this.$device$inlined);
                    dVar.p$ = (il6) obj;
                    return dVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0061d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        DeviceRepository d = this.this$0.this$0.a.d();
                        Device device = this.$device$inlined;
                        this.L$0 = il6;
                        this.label = 1;
                        obj = d.updateDevice(device, true, this);
                        if (obj == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return obj;
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, Intent intent, xe6 xe6) {
                super(2, xe6);
                this.this$0 = dVar;
                this.$intent = intent;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$intent, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v56, resolved type: java.lang.Object} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v22, resolved type: java.lang.String} */
            /* JADX WARNING: type inference failed for: r0v330, types: [com.portfolio.platform.usecase.VerifySecretKeyUseCase, com.portfolio.platform.CoroutineUseCase] */
            /* JADX WARNING: type inference failed for: r2v142, types: [com.portfolio.platform.service.MFDeviceService$d$a$a, com.portfolio.platform.CoroutineUseCase$e] */
            /* JADX WARNING: Can't fix incorrect switch cases order */
            /* JADX WARNING: Code restructure failed: missing block: B:132:0x0656, code lost:
                r0 = (com.fossil.ap4) r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:133:0x0658, code lost:
                r2 = r4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:137:0x0669, code lost:
                r6 = r6;
                r0 = r3;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:142:0x067f, code lost:
                r0.putExtra(r17, true);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:143:0x0682, code lost:
                if (r6 == null) goto L_0x068d;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:144:0x0684, code lost:
                r15.this$0.a.a("ota_success", (java.util.Map<java.lang.String, java.lang.String>) r6);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:145:0x068d, code lost:
                if (r2 == null) goto L_0x06db;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:146:0x068f, code lost:
                r2.a(r23);
                r1 = com.fossil.cd6.a;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x0144, code lost:
                r42 = "Inside ";
                r44 = "sync_session";
             */
            /* JADX WARNING: Code restructure failed: missing block: B:157:0x06db, code lost:
                com.portfolio.platform.helper.AnalyticsHelper.f.e(r18);
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.portfolio.platform.service.MFDeviceService.V.b(), "Inside .OTA receiver, broadcast local in app");
                r1 = com.portfolio.platform.service.BleCommandResultManager.d;
                r2 = com.misfit.frameworks.buttonservice.communite.CommunicateMode.OTA;
                r1.a(r2, new com.portfolio.platform.service.BleCommandResultManager.a(r2, r9, r0));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:171:0x077d, code lost:
                r0 = com.fossil.cd6.a;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:172:0x077f, code lost:
                r8 = r15;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:253:0x0bf9, code lost:
                r7 = r1;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:282:0x0d70, code lost:
                if (r0 == 0) goto L_0x0dab;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:283:0x0d72, code lost:
                r8 = r53;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:285:?, code lost:
                r8.this$0.a.c().a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "Process data OK.");
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.V.b(), "Process data OK.");
                r8.this$0.a.c().c(false);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:286:0x0da7, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:287:0x0da8, code lost:
                r4 = r7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:288:0x0dab, code lost:
                r8 = r53;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:290:?, code lost:
                r0 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                r1 = com.portfolio.platform.service.MFDeviceService.V.b();
                r0.d(r1, r42 + com.portfolio.platform.service.MFDeviceService.V.b() + ".processBleResult - data is empty.");
                r8.this$0.a.c().a(com.misfit.frameworks.buttonservice.communite.CommunicateMode.SYNC, r7, "data is empty.");
                com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getRemote().i(com.misfit.frameworks.buttonservice.log.FLogger.Component.APP, com.misfit.frameworks.buttonservice.log.FLogger.Session.SYNC, r7, com.portfolio.platform.service.MFDeviceService.V.b(), "data is empty.");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:291:0x0e05, code lost:
                r10 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:293:?, code lost:
                r8.this$0.a.c().c(true);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:294:0x0e0b, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:295:0x0e0d, code lost:
                r0 = e;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:296:0x0e0e, code lost:
                r10 = true;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:297:0x0e0f, code lost:
                r4 = r7;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:35:0x02bd, code lost:
                r0 = (com.fossil.ap4) r0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:36:0x02c1, code lost:
                if ((r0 instanceof com.fossil.cp4) == false) goto L_0x02ce;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:37:0x02c3, code lost:
                r3 = ((com.fossil.cp4) r0).a();
                r10 = 0;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:39:0x02d0, code lost:
                if ((r0 instanceof com.fossil.zo4) == false) goto L_0x02ea;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:40:0x02d2, code lost:
                r10 = ((com.fossil.zo4) r0).a();
                r3 = r23;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:41:0x02da, code lost:
                r0 = r15.this$0.a.c();
                com.fossil.wg6.a((java.lang.Object) r9, "serial");
                r0.a(r9, r3, r10);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:43:0x02ef, code lost:
                throw new com.fossil.kc6();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:53:0x0343, code lost:
                r0 = (com.fossil.ap4) r0;
                r1 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
                r2 = com.portfolio.platform.service.MFDeviceService.V.b();
                r1.d(r2, "swap pairing key " + r0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:54:0x0367, code lost:
                if ((r0 instanceof com.fossil.cp4) == false) goto L_0x0389;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:55:0x0369, code lost:
                r1 = r15.this$0.a.c();
                com.fossil.wg6.a((java.lang.Object) r9, "serial");
                r0 = ((com.fossil.cp4) r0).a();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:56:0x037a, code lost:
                if (r0 == null) goto L_0x0384;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:57:0x037c, code lost:
                r1.b(r9, (java.lang.String) r0, 0);
             */
            /* JADX WARNING: Code restructure failed: missing block: B:58:0x0384, code lost:
                com.fossil.wg6.a();
             */
            /* JADX WARNING: Code restructure failed: missing block: B:59:0x0388, code lost:
                throw null;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:61:0x038b, code lost:
                if ((r0 instanceof com.fossil.zo4) == false) goto L_0x077f;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:62:0x038d, code lost:
                r1 = r15.this$0.a.c();
                com.fossil.wg6.a((java.lang.Object) r9, "serial");
                r1.b(r9, r23, ((com.fossil.zo4) r0).a());
             */
            @DexIgnore
            /* JADX WARNING: Multi-variable type inference failed */
            public final Object invokeSuspend(Object obj) {
                String str;
                a aVar;
                boolean z;
                String str2;
                String str3;
                int i;
                String str4;
                String str5;
                String str6;
                Intent intent;
                String str7;
                String str8;
                HashMap hashMap;
                jl4 jl4;
                Intent intent2;
                Object obj2;
                String str9;
                Object obj3;
                Object obj4;
                String str10;
                String str11;
                String str12;
                int i2;
                int i3;
                ArrayList<Integer> arrayList;
                String str13;
                String str14;
                int i4;
                ArrayList<Integer> arrayList2;
                List<DataFile> list;
                FitnessData fitnessData;
                ILocalFLogger local;
                String b2;
                int i5;
                jl4 jl42;
                HashMap hashMap2;
                String str15;
                HashMap hashMap3;
                String str16;
                String str17;
                DeviceRepository deviceRepository;
                String str18;
                Object a = ff6.a();
                switch (this.label) {
                    case 0:
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        int intExtra = this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
                        CommunicateMode communicateMode = CommunicateMode.values()[intExtra];
                        str6 = this.$intent.getStringExtra(Constants.SERIAL_NUMBER);
                        int intExtra2 = this.$intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1);
                        int intExtra3 = this.$intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), FailureCode.UNKNOWN_ERROR);
                        ArrayList<Integer> integerArrayListExtra = this.$intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                        if (integerArrayListExtra == null) {
                            integerArrayListExtra = new ArrayList<>();
                        }
                        ArrayList<Integer> arrayList3 = integerArrayListExtra;
                        Bundle extras = this.$intent.getExtras();
                        if (extras == null) {
                            extras = new Bundle();
                        }
                        String str19 = "Inside ";
                        Bundle bundle = extras;
                        wg6.a((Object) bundle, "intent.extras ?: Bundle()");
                        String str20 = "sync_session";
                        SKUModel skuModelBySerialPrefix = this.this$0.a.d().getSkuModelBySerialPrefix(DeviceHelper.o.b(str6));
                        str5 = "ota_session";
                        str8 = "OTA_RESULT";
                        int i6 = bundle.getInt(ButtonService.Companion.getSYNC_MODE(), -1);
                        int i7 = bundle.getInt(ButtonService.Companion.getORIGINAL_SYNC_MODE(), 10);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        str9 = "";
                        String b3 = MFDeviceService.V.b();
                        Object obj5 = a;
                        StringBuilder sb = new StringBuilder();
                        int i8 = i7;
                        sb.append("Inside .bleActionServiceReceiver phase ");
                        sb.append(communicateMode);
                        sb.append(" result ");
                        sb.append(intExtra2);
                        sb.append(" extra info ");
                        sb.append(bundle);
                        local2.d(b3, sb.toString());
                        int i9 = bundle.getInt(ButtonService.Companion.getLOG_ID(), -1);
                        switch (rp4.a[communicateMode.ordinal()]) {
                            case 1:
                                this.this$0.a.m().b(System.currentTimeMillis());
                                break;
                            case 2:
                                String str21 = "error_code";
                                int i10 = i9;
                                int i11 = i8;
                                int i12 = i6;
                                String str22 = "device";
                                jl4 c2 = AnalyticsHelper.f.c(str20);
                                int i13 = intExtra;
                                CommunicateMode communicateMode2 = communicateMode;
                                this.this$0.a.m().b(System.currentTimeMillis(), str6);
                                if (intExtra2 == ServiceActionResult.ASK_FOR_STOP_WORKOUT.ordinal()) {
                                    FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "sync is pending because workout is working");
                                    this.this$0.a.a(str6, 5, intExtra3, arrayList3, hf6.a(i11));
                                } else if (intExtra2 == ServiceActionResult.PROCESSING.ordinal()) {
                                    FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "sync status is SYNCING");
                                    MFDeviceService.a(this.this$0.a, str6, 0, 0, (ArrayList) null, (Integer) null, 24, (Object) null);
                                } else if (intExtra2 == ServiceActionResult.UNALLOWED_ACTION.ordinal()) {
                                    this.this$0.a.m().b(0);
                                    this.this$0.a.a(str6, 4, intExtra3, arrayList3, hf6.a(i11));
                                } else if (intExtra2 == ServiceActionResult.FAILED.ordinal()) {
                                    this.this$0.a.m().b(0);
                                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                    String b4 = MFDeviceService.V.b();
                                    local3.d(b4, "sync status is FAILED - current sync success number = " + this.this$0.a.m().e());
                                    this.this$0.a.a(str6, 2, intExtra3, arrayList3, hf6.a(i11));
                                    this.this$0.a.a(skuModelBySerialPrefix, i12, 2);
                                    hl4 a2 = AnalyticsHelper.f.a("sync_session_optional_error");
                                    a2.a(str21, String.valueOf(intExtra3));
                                    if (c2 != null) {
                                        c2.a(a2);
                                        cd6 cd6 = cd6.a;
                                    }
                                    if (c2 != null) {
                                        c2.a(String.valueOf(intExtra3));
                                        cd6 cd62 = cd6.a;
                                    }
                                    FossilNotificationBar.c.a(this.this$0.a);
                                } else if (intExtra2 == ServiceActionResult.RECEIVED_DATA.ordinal()) {
                                    if (!bundle.isEmpty()) {
                                        if (!(str6 == null || xj6.a(str6))) {
                                            String str23 = str6;
                                            long j = bundle.getLong(Constants.SYNC_RESULT, -1);
                                            long j2 = bundle.getLong(ButtonService.Companion.getREALTIME_STEPS(), -1);
                                            UserProfile j3 = this.this$0.a.c().j();
                                            MFUser currentUser = this.this$0.a.r().getCurrentUser();
                                            PortfolioApp c3 = this.this$0.a.c();
                                            jl4 jl43 = c2;
                                            CommunicateMode communicateMode3 = CommunicateMode.SYNC;
                                            int i14 = i10;
                                            StringBuilder sb2 = new StringBuilder();
                                            int i15 = i11;
                                            sb2.append("Sync time=");
                                            int i16 = i12;
                                            long j4 = j;
                                            sb2.append(j4);
                                            SKUModel sKUModel = skuModelBySerialPrefix;
                                            String str24 = str23;
                                            c3.a(communicateMode3, str24, sb2.toString());
                                            if (j4 <= ((long) -1) || j3 == null || currentUser == null) {
                                                String str25 = str24;
                                                aVar = this;
                                                String str26 = str19;
                                                str = str20;
                                                if (j4 < 0) {
                                                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                                                    String b5 = MFDeviceService.V.b();
                                                    local4.e(b5, str26 + MFDeviceService.V.b() + ".processBleResult - syncTime wrong: " + j4);
                                                    PortfolioApp c4 = aVar.this$0.a.c();
                                                    CommunicateMode communicateMode4 = CommunicateMode.SYNC;
                                                    str10 = str25;
                                                    c4.a(communicateMode4, str10, "syncTime value wrong: " + j4);
                                                    IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                                                    FLogger.Component component = FLogger.Component.APP;
                                                    FLogger.Session session = FLogger.Session.SYNC;
                                                    String b6 = MFDeviceService.V.b();
                                                    remote.i(component, session, str10, b6, "syncTime value wrong: " + j4);
                                                } else {
                                                    str10 = str25;
                                                }
                                                if (j3 == null) {
                                                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                                    String b7 = MFDeviceService.V.b();
                                                    local5.e(b7, str26 + MFDeviceService.V.b() + ".processBleResult - userProfile empty");
                                                    aVar.this$0.a.c().a(CommunicateMode.SYNC, str10, "userProfile is empty.");
                                                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str10, MFDeviceService.V.b(), "userProfile is empty.");
                                                }
                                                if (currentUser == null) {
                                                    ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                                    String b8 = MFDeviceService.V.b();
                                                    local6.e(b8, str26 + MFDeviceService.V.b() + ".processBleResult - user data empty");
                                                    aVar.this$0.a.c().a(CommunicateMode.SYNC, str10, "user data is empty.");
                                                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str10, MFDeviceService.V.b(), "user data is empty.");
                                                }
                                                aVar.this$0.a.c().c(true);
                                            } else {
                                                try {
                                                    this.this$0.a.c().a(CommunicateMode.SYNC, str24, "Save sync result to DB");
                                                    ArrayList arrayList4 = new ArrayList();
                                                    List<DataFile> allDataFiles = DataFileProvider.getInstance(this.this$0.a).getAllDataFiles(j4, str24);
                                                    IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                                                    FLogger.Component component2 = FLogger.Component.APP;
                                                    FLogger.Session session2 = FLogger.Session.SYNC;
                                                    String b9 = MFDeviceService.V.b();
                                                    StringBuilder sb3 = new StringBuilder();
                                                    Bundle bundle2 = bundle;
                                                    sb3.append("[SYNC] Process sync data from BS dataSize ");
                                                    sb3.append(allDataFiles.size());
                                                    remote2.i(component2, session2, str24, b9, sb3.toString());
                                                    wg6.a((Object) allDataFiles, "cacheData");
                                                    Iterator<T> it = allDataFiles.iterator();
                                                    while (it.hasNext()) {
                                                        try {
                                                            DataFile dataFile = (DataFile) it.next();
                                                            Iterator<T> it2 = it;
                                                            try {
                                                                Gson a3 = this.this$0.a.F;
                                                                list = allDataFiles;
                                                                try {
                                                                    wg6.a((Object) dataFile, "it");
                                                                    fitnessData = (FitnessData) a3.a(dataFile.getDataFile(), FitnessData.class);
                                                                    local = FLogger.INSTANCE.getLocal();
                                                                    b2 = MFDeviceService.V.b();
                                                                    arrayList2 = arrayList3;
                                                                } catch (Exception e) {
                                                                    e = e;
                                                                    i4 = intExtra3;
                                                                    arrayList2 = arrayList3;
                                                                    ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
                                                                    String b10 = MFDeviceService.V.b();
                                                                    local7.d(b10, "Parse fitnessData. exception=" + e);
                                                                    IRemoteFLogger remote3 = FLogger.INSTANCE.getRemote();
                                                                    FLogger.Component component3 = FLogger.Component.APP;
                                                                    FLogger.Session session3 = FLogger.Session.SYNC;
                                                                    String b11 = MFDeviceService.V.b();
                                                                    remote3.i(component3, session3, str24, b11, "[Sync Data Received] Exception when parse data " + e);
                                                                    it = it2;
                                                                    allDataFiles = list;
                                                                    arrayList3 = arrayList2;
                                                                    intExtra3 = i4;
                                                                }
                                                                try {
                                                                    StringBuilder sb4 = new StringBuilder();
                                                                    i4 = intExtra3;
                                                                    try {
                                                                        sb4.append("[SYNC] Add fitness data ");
                                                                        sb4.append(fitnessData);
                                                                        sb4.append(" to sync data");
                                                                        local.d(b2, sb4.toString());
                                                                        wg6.a((Object) fitnessData, "fitnessData");
                                                                        arrayList4.add(fitnessData);
                                                                    } catch (Exception e2) {
                                                                        e = e2;
                                                                    }
                                                                } catch (Exception e3) {
                                                                    e = e3;
                                                                    i4 = intExtra3;
                                                                    ILocalFLogger local72 = FLogger.INSTANCE.getLocal();
                                                                    String b102 = MFDeviceService.V.b();
                                                                    local72.d(b102, "Parse fitnessData. exception=" + e);
                                                                    IRemoteFLogger remote32 = FLogger.INSTANCE.getRemote();
                                                                    FLogger.Component component32 = FLogger.Component.APP;
                                                                    FLogger.Session session32 = FLogger.Session.SYNC;
                                                                    String b112 = MFDeviceService.V.b();
                                                                    remote32.i(component32, session32, str24, b112, "[Sync Data Received] Exception when parse data " + e);
                                                                    it = it2;
                                                                    allDataFiles = list;
                                                                    arrayList3 = arrayList2;
                                                                    intExtra3 = i4;
                                                                }
                                                            } catch (Exception e4) {
                                                                e = e4;
                                                                list = allDataFiles;
                                                                i4 = intExtra3;
                                                                arrayList2 = arrayList3;
                                                                ILocalFLogger local722 = FLogger.INSTANCE.getLocal();
                                                                String b1022 = MFDeviceService.V.b();
                                                                local722.d(b1022, "Parse fitnessData. exception=" + e);
                                                                IRemoteFLogger remote322 = FLogger.INSTANCE.getRemote();
                                                                FLogger.Component component322 = FLogger.Component.APP;
                                                                FLogger.Session session322 = FLogger.Session.SYNC;
                                                                String b1122 = MFDeviceService.V.b();
                                                                remote322.i(component322, session322, str24, b1122, "[Sync Data Received] Exception when parse data " + e);
                                                                it = it2;
                                                                allDataFiles = list;
                                                                arrayList3 = arrayList2;
                                                                intExtra3 = i4;
                                                            }
                                                            it = it2;
                                                            allDataFiles = list;
                                                            arrayList3 = arrayList2;
                                                            intExtra3 = i4;
                                                        } catch (Exception e5) {
                                                            e = e5;
                                                            str2 = str24;
                                                            aVar = this;
                                                            str = str20;
                                                            break;
                                                        }
                                                    }
                                                    List<DataFile> list2 = allDataFiles;
                                                    int i17 = intExtra3;
                                                    ArrayList<Integer> arrayList5 = arrayList3;
                                                    ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
                                                    String b12 = MFDeviceService.V.b();
                                                    local8.d(b12, "[SYNC] Process data done,syncData size " + arrayList4.size());
                                                    IRemoteFLogger remote4 = FLogger.INSTANCE.getRemote();
                                                    FLogger.Component component4 = FLogger.Component.APP;
                                                    FLogger.Session session4 = FLogger.Session.SYNC;
                                                    String b13 = MFDeviceService.V.b();
                                                    remote4.i(component4, session4, str24, b13, "[Sync Data Received] After get data, size " + arrayList4.size());
                                                    i = !arrayList4.isEmpty() ? 1 : 0;
                                                    ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
                                                    String b14 = MFDeviceService.V.b();
                                                    local9.d(b14, ".onReadDataFilesSuccess(), sdk = " + this.this$0.a.F.a(arrayList4));
                                                    if (!FossilDeviceSerialPatternUtil.isDianaDevice(str24)) {
                                                        String str27 = str24;
                                                        Bundle bundle3 = bundle2;
                                                        str3 = str19;
                                                        str = str20;
                                                        Object obj6 = obj5;
                                                        int i18 = i13;
                                                        CommunicateMode communicateMode5 = communicateMode2;
                                                        SKUModel sKUModel2 = sKUModel;
                                                        List<DataFile> list3 = list2;
                                                        jl4 jl44 = jl43;
                                                        int i19 = i14;
                                                        int i20 = i15;
                                                        int i21 = i16;
                                                        ArrayList<Integer> arrayList6 = arrayList5;
                                                        int i22 = i17;
                                                        if (i != 0) {
                                                            Bundle bundle4 = bundle3;
                                                            SKUModel sKUModel3 = sKUModel2;
                                                            try {
                                                                i3 = i;
                                                                arrayList = arrayList6;
                                                                str13 = str27;
                                                            } catch (Exception e6) {
                                                                e = e6;
                                                                str11 = str27;
                                                                z = true;
                                                                aVar = this;
                                                                str2 = str11;
                                                                ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
                                                                String b15 = MFDeviceService.V.b();
                                                                StringBuilder sb5 = new StringBuilder();
                                                                sb5.append("Error while processing sync data - e=");
                                                                e.printStackTrace();
                                                                sb5.append(cd6.a);
                                                                local10.e(b15, sb5.toString());
                                                                e.printStackTrace();
                                                                PortfolioApp c5 = aVar.this$0.a.c();
                                                                CommunicateMode communicateMode6 = CommunicateMode.SYNC;
                                                                c5.a(communicateMode6, str2, "Sync data result OK but has exception error e=" + e);
                                                                IRemoteFLogger remote5 = FLogger.INSTANCE.getRemote();
                                                                FLogger.Component component5 = FLogger.Component.APP;
                                                                FLogger.Session session5 = FLogger.Session.SYNC;
                                                                String b16 = MFDeviceService.V.b();
                                                                remote5.i(component5, session5, str2, b16, "Sync data result OK but has exception error e=" + e);
                                                                aVar.this$0.a.c().c(z);
                                                                AnalyticsHelper.f.e(str);
                                                                FossilNotificationBar.c.a(aVar.this$0.a);
                                                                return cd6.a;
                                                            }
                                                            try {
                                                                List<FitnessDataWrapper> a4 = this.this$0.a.a((List<FitnessData>) arrayList4, str13, new DateTime());
                                                                this.this$0.a.f().saveFitnessData(a4);
                                                                HybridSyncDataProcessing.a a5 = HybridSyncDataProcessing.b.a(str13, a4, currentUser, j3, j4, j2, this.this$0.a.c());
                                                                HybridSyncDataProcessing hybridSyncDataProcessing = HybridSyncDataProcessing.b;
                                                                HybridSyncDataProcessing.a aVar2 = a5;
                                                                SleepSessionsRepository n = this.this$0.a.n();
                                                                SummariesRepository p = this.this$0.a.p();
                                                                SleepSummariesRepository o = this.this$0.a.o();
                                                                FitnessDataRepository f = this.this$0.a.f();
                                                                ActivitiesRepository b17 = this.this$0.a.b();
                                                                HybridPresetRepository l = this.this$0.a.l();
                                                                GoalTrackingRepository h = this.this$0.a.h();
                                                                ThirdPartyRepository q = this.this$0.a.q();
                                                                UserRepository r = this.this$0.a.r();
                                                                PortfolioApp c6 = this.this$0.a.c();
                                                                ik4 g = this.this$0.a.g();
                                                                this.L$0 = il6;
                                                                this.I$0 = i18;
                                                                this.L$1 = communicateMode5;
                                                                this.L$2 = str13;
                                                                this.I$1 = intExtra2;
                                                                this.I$2 = i22;
                                                                this.L$3 = arrayList;
                                                                this.L$4 = bundle4;
                                                                this.L$5 = sKUModel3;
                                                                this.I$3 = i21;
                                                                this.I$4 = i20;
                                                                this.I$5 = i19;
                                                                this.L$6 = jl44;
                                                                this.J$0 = j4;
                                                                this.J$1 = j2;
                                                                this.L$7 = j3;
                                                                this.L$8 = currentUser;
                                                                this.L$9 = arrayList4;
                                                                this.L$10 = list3;
                                                                int i23 = i3;
                                                                this.I$6 = i23;
                                                                this.L$11 = a4;
                                                                HybridSyncDataProcessing.a aVar3 = aVar2;
                                                                this.L$12 = aVar3;
                                                                this.label = 2;
                                                                str11 = str13;
                                                                int i24 = i23;
                                                                try {
                                                                    Object obj7 = obj6;
                                                                    if (hybridSyncDataProcessing.a(aVar3, str13, n, p, o, f, b17, l, h, q, r, c6, g, this) != obj7) {
                                                                        i = i24;
                                                                        str4 = str11;
                                                                        break;
                                                                    } else {
                                                                        return obj7;
                                                                    }
                                                                } catch (Exception e7) {
                                                                    e = e7;
                                                                    z = true;
                                                                    aVar = this;
                                                                    str2 = str11;
                                                                    ILocalFLogger local102 = FLogger.INSTANCE.getLocal();
                                                                    String b152 = MFDeviceService.V.b();
                                                                    StringBuilder sb52 = new StringBuilder();
                                                                    sb52.append("Error while processing sync data - e=");
                                                                    e.printStackTrace();
                                                                    sb52.append(cd6.a);
                                                                    local102.e(b152, sb52.toString());
                                                                    e.printStackTrace();
                                                                    PortfolioApp c52 = aVar.this$0.a.c();
                                                                    CommunicateMode communicateMode62 = CommunicateMode.SYNC;
                                                                    c52.a(communicateMode62, str2, "Sync data result OK but has exception error e=" + e);
                                                                    IRemoteFLogger remote52 = FLogger.INSTANCE.getRemote();
                                                                    FLogger.Component component52 = FLogger.Component.APP;
                                                                    FLogger.Session session52 = FLogger.Session.SYNC;
                                                                    String b162 = MFDeviceService.V.b();
                                                                    remote52.i(component52, session52, str2, b162, "Sync data result OK but has exception error e=" + e);
                                                                    aVar.this$0.a.c().c(z);
                                                                    AnalyticsHelper.f.e(str);
                                                                    FossilNotificationBar.c.a(aVar.this$0.a);
                                                                    return cd6.a;
                                                                }
                                                            } catch (Exception e8) {
                                                                e = e8;
                                                                str11 = str13;
                                                                z = true;
                                                                aVar = this;
                                                                str2 = str11;
                                                                ILocalFLogger local1022 = FLogger.INSTANCE.getLocal();
                                                                String b1522 = MFDeviceService.V.b();
                                                                StringBuilder sb522 = new StringBuilder();
                                                                sb522.append("Error while processing sync data - e=");
                                                                e.printStackTrace();
                                                                sb522.append(cd6.a);
                                                                local1022.e(b1522, sb522.toString());
                                                                e.printStackTrace();
                                                                PortfolioApp c522 = aVar.this$0.a.c();
                                                                CommunicateMode communicateMode622 = CommunicateMode.SYNC;
                                                                c522.a(communicateMode622, str2, "Sync data result OK but has exception error e=" + e);
                                                                IRemoteFLogger remote522 = FLogger.INSTANCE.getRemote();
                                                                FLogger.Component component522 = FLogger.Component.APP;
                                                                FLogger.Session session522 = FLogger.Session.SYNC;
                                                                String b1622 = MFDeviceService.V.b();
                                                                remote522.i(component522, session522, str2, b1622, "Sync data result OK but has exception error e=" + e);
                                                                aVar.this$0.a.c().c(z);
                                                                AnalyticsHelper.f.e(str);
                                                                FossilNotificationBar.c.a(aVar.this$0.a);
                                                                return cd6.a;
                                                            }
                                                        } else {
                                                            i2 = i;
                                                            str12 = str27;
                                                        }
                                                    } else if (i != 0) {
                                                        try {
                                                            List<FitnessDataWrapper> a6 = this.this$0.a.a((List<FitnessData>) arrayList4, str24, new DateTime());
                                                            this.this$0.a.f().saveFitnessData(a6);
                                                            DianaSyncDataProcessing.a a7 = DianaSyncDataProcessing.b.a(str24, a6, currentUser, j3, j4, j2, this.this$0.a.c());
                                                            DianaSyncDataProcessing dianaSyncDataProcessing = DianaSyncDataProcessing.b;
                                                            SleepSessionsRepository n2 = this.this$0.a.n();
                                                            SummariesRepository p2 = this.this$0.a.p();
                                                            SleepSummariesRepository o2 = this.this$0.a.o();
                                                            HeartRateSampleRepository i25 = this.this$0.a.i();
                                                            HeartRateSummaryRepository j5 = this.this$0.a.j();
                                                            WorkoutSessionRepository u = this.this$0.a.u();
                                                            FitnessDataRepository f2 = this.this$0.a.f();
                                                            ActivitiesRepository b18 = this.this$0.a.b();
                                                            ThirdPartyRepository q2 = this.this$0.a.q();
                                                            PortfolioApp c7 = this.this$0.a.c();
                                                            ik4 g2 = this.this$0.a.g();
                                                            this.L$0 = il6;
                                                            this.I$0 = i13;
                                                            this.L$1 = communicateMode2;
                                                            this.L$2 = str24;
                                                            this.I$1 = intExtra2;
                                                            this.I$2 = i17;
                                                            this.L$3 = arrayList5;
                                                            this.L$4 = bundle2;
                                                            this.L$5 = sKUModel;
                                                            this.I$3 = i16;
                                                            this.I$4 = i15;
                                                            this.I$5 = i14;
                                                            this.L$6 = jl43;
                                                            this.J$0 = j4;
                                                            this.J$1 = j2;
                                                            this.L$7 = j3;
                                                            this.L$8 = currentUser;
                                                            this.L$9 = arrayList4;
                                                            this.L$10 = list2;
                                                            this.I$6 = i;
                                                            this.L$11 = a6;
                                                            this.L$12 = a7;
                                                            this.label = 1;
                                                            str3 = str19;
                                                            str14 = str24;
                                                            str = str20;
                                                            Object obj8 = obj5;
                                                            try {
                                                                Object a8 = dianaSyncDataProcessing.a(a7, str24, n2, p2, o2, i25, j5, u, f2, b18, q2, c7, g2, this);
                                                                Object obj9 = obj8;
                                                                if (a8 != obj9) {
                                                                    str4 = str14;
                                                                    break;
                                                                } else {
                                                                    return obj9;
                                                                }
                                                            } catch (Exception e9) {
                                                                e = e9;
                                                                z = true;
                                                                aVar = this;
                                                                str2 = str14;
                                                                ILocalFLogger local10222 = FLogger.INSTANCE.getLocal();
                                                                String b15222 = MFDeviceService.V.b();
                                                                StringBuilder sb5222 = new StringBuilder();
                                                                sb5222.append("Error while processing sync data - e=");
                                                                e.printStackTrace();
                                                                sb5222.append(cd6.a);
                                                                local10222.e(b15222, sb5222.toString());
                                                                e.printStackTrace();
                                                                PortfolioApp c5222 = aVar.this$0.a.c();
                                                                CommunicateMode communicateMode6222 = CommunicateMode.SYNC;
                                                                c5222.a(communicateMode6222, str2, "Sync data result OK but has exception error e=" + e);
                                                                IRemoteFLogger remote5222 = FLogger.INSTANCE.getRemote();
                                                                FLogger.Component component5222 = FLogger.Component.APP;
                                                                FLogger.Session session5222 = FLogger.Session.SYNC;
                                                                String b16222 = MFDeviceService.V.b();
                                                                remote5222.i(component5222, session5222, str2, b16222, "Sync data result OK but has exception error e=" + e);
                                                                aVar.this$0.a.c().c(z);
                                                                AnalyticsHelper.f.e(str);
                                                                FossilNotificationBar.c.a(aVar.this$0.a);
                                                                return cd6.a;
                                                            }
                                                        } catch (Exception e10) {
                                                            e = e10;
                                                            str14 = str24;
                                                            str = str20;
                                                            z = true;
                                                            aVar = this;
                                                            str2 = str14;
                                                            ILocalFLogger local102222 = FLogger.INSTANCE.getLocal();
                                                            String b152222 = MFDeviceService.V.b();
                                                            StringBuilder sb52222 = new StringBuilder();
                                                            sb52222.append("Error while processing sync data - e=");
                                                            e.printStackTrace();
                                                            sb52222.append(cd6.a);
                                                            local102222.e(b152222, sb52222.toString());
                                                            e.printStackTrace();
                                                            PortfolioApp c52222 = aVar.this$0.a.c();
                                                            CommunicateMode communicateMode62222 = CommunicateMode.SYNC;
                                                            c52222.a(communicateMode62222, str2, "Sync data result OK but has exception error e=" + e);
                                                            IRemoteFLogger remote52222 = FLogger.INSTANCE.getRemote();
                                                            FLogger.Component component52222 = FLogger.Component.APP;
                                                            FLogger.Session session52222 = FLogger.Session.SYNC;
                                                            String b162222 = MFDeviceService.V.b();
                                                            remote52222.i(component52222, session52222, str2, b162222, "Sync data result OK but has exception error e=" + e);
                                                            aVar.this$0.a.c().c(z);
                                                            AnalyticsHelper.f.e(str);
                                                            FossilNotificationBar.c.a(aVar.this$0.a);
                                                            return cd6.a;
                                                        }
                                                    } else {
                                                        str3 = str19;
                                                        str = str20;
                                                        i2 = i;
                                                        str12 = str24;
                                                    }
                                                    i = i2;
                                                    String str28 = str12;
                                                    break;
                                                } catch (Exception e11) {
                                                    e = e11;
                                                    str11 = str24;
                                                    aVar = this;
                                                    str = str20;
                                                    z = true;
                                                    str2 = str11;
                                                    ILocalFLogger local1022222 = FLogger.INSTANCE.getLocal();
                                                    String b1522222 = MFDeviceService.V.b();
                                                    StringBuilder sb522222 = new StringBuilder();
                                                    sb522222.append("Error while processing sync data - e=");
                                                    e.printStackTrace();
                                                    sb522222.append(cd6.a);
                                                    local1022222.e(b1522222, sb522222.toString());
                                                    e.printStackTrace();
                                                    PortfolioApp c522222 = aVar.this$0.a.c();
                                                    CommunicateMode communicateMode622222 = CommunicateMode.SYNC;
                                                    c522222.a(communicateMode622222, str2, "Sync data result OK but has exception error e=" + e);
                                                    IRemoteFLogger remote522222 = FLogger.INSTANCE.getRemote();
                                                    FLogger.Component component522222 = FLogger.Component.APP;
                                                    FLogger.Session session522222 = FLogger.Session.SYNC;
                                                    String b1622222 = MFDeviceService.V.b();
                                                    remote522222.i(component522222, session522222, str2, b1622222, "Sync data result OK but has exception error e=" + e);
                                                    aVar.this$0.a.c().c(z);
                                                    AnalyticsHelper.f.e(str);
                                                    FossilNotificationBar.c.a(aVar.this$0.a);
                                                    return cd6.a;
                                                }
                                            }
                                        }
                                    }
                                    String str29 = str6;
                                    aVar = this;
                                    str = str20;
                                    if (bundle.isEmpty()) {
                                        PortfolioApp c8 = aVar.this$0.a.c();
                                        CommunicateMode communicateMode7 = CommunicateMode.SYNC;
                                        if (str29 != null) {
                                            c8.a(communicateMode7, str29, "Sync data result OK but extra info is EMPTY");
                                            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.SYNC, str29, MFDeviceService.V.b(), "Sync data result OK but extra info is EMPTY");
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        PortfolioApp c9 = aVar.this$0.a.c();
                                        CommunicateMode communicateMode8 = CommunicateMode.SYNC;
                                        if (str29 != null) {
                                            c9.a(communicateMode8, str29, "Sync data result OK but serial is " + str29);
                                            IRemoteFLogger remote6 = FLogger.INSTANCE.getRemote();
                                            FLogger.Component component6 = FLogger.Component.APP;
                                            FLogger.Session session6 = FLogger.Session.SYNC;
                                            String b19 = MFDeviceService.V.b();
                                            remote6.i(component6, session6, str29, b19, "Sync data result OK but serial is " + str29);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    }
                                    aVar.this$0.a.c().c(true);
                                } else {
                                    jl4 jl45 = c2;
                                    int i26 = i12;
                                    String str30 = str6;
                                    Bundle bundle5 = bundle;
                                    SKUModel sKUModel4 = skuModelBySerialPrefix;
                                    aVar = this;
                                    str = str20;
                                    an4 m = aVar.this$0.a.m();
                                    m.b(m.e() + 1);
                                    ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
                                    String b20 = MFDeviceService.V.b();
                                    local11.d(b20, "sync status is successful - current sync success number = " + aVar.this$0.a.m().e());
                                    aVar.this$0.a.m().c(System.currentTimeMillis(), str30);
                                    if (!bundle5.isEmpty() && str30 != null) {
                                        aVar.this$0.a.a((MisfitDeviceProfile) bundle5.getParcelable(str22));
                                        MisfitDeviceProfile a9 = aVar.this$0.a.a();
                                        if (a9 != null) {
                                            PortfolioApp.get.instance().a(a9.getBatteryLevel());
                                            cd6 cd63 = cd6.a;
                                        }
                                        MFDeviceService mFDeviceService = aVar.this$0.a;
                                        mFDeviceService.a(mFDeviceService.a(), str30, false);
                                        if (i26 == 13 && !aVar.this$0.a.m().k(str30)) {
                                            aVar.this$0.a.m().a(str30, bundle5.getLong(Constants.SYNC_ID), true);
                                        }
                                    }
                                    MFDeviceService.a(aVar.this$0.a, str30, 1, 0, (ArrayList) null, (Integer) null, 24, (Object) null);
                                    aVar.this$0.a.a(sKUModel4, i26, 1);
                                    if (jl45 != null) {
                                        jl45.a(str9);
                                        cd6 cd64 = cd6.a;
                                    }
                                    FossilNotificationBar.c.a(aVar.this$0.a);
                                    aVar.this$0.a.x();
                                }
                                aVar = this;
                                str = str20;
                                break;
                            case 3:
                                Object obj10 = obj5;
                                int i27 = i8;
                                int i28 = i9;
                                int i29 = i6;
                                int i30 = i28;
                                if (intExtra2 == ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                                    int i31 = bundle.getInt(Constants.BATTERY);
                                    Object obj11 = obj10;
                                    DeviceRepository d = this.this$0.a.d();
                                    wg6.a((Object) str6, "serial");
                                    Device deviceBySerial = d.getDeviceBySerial(str6);
                                    if (deviceBySerial != null) {
                                        if (i31 > 0) {
                                            deviceBySerial.setBatteryLevel(i31);
                                            int i32 = i30;
                                            if (deviceBySerial.getBatteryLevel() > 100) {
                                                deviceBySerial.setBatteryLevel(100);
                                            }
                                            dl6 b21 = zl6.b();
                                            C0061d dVar = new C0061d((xe6) null, this, i31, deviceBySerial);
                                            this.L$0 = il6;
                                            this.I$0 = intExtra;
                                            this.L$1 = communicateMode;
                                            this.L$2 = str6;
                                            this.I$1 = intExtra2;
                                            this.I$2 = intExtra3;
                                            this.L$3 = arrayList3;
                                            this.L$4 = bundle;
                                            this.L$5 = skuModelBySerialPrefix;
                                            this.I$3 = i29;
                                            this.I$4 = i27;
                                            this.I$5 = i32;
                                            this.I$6 = i31;
                                            this.L$6 = deviceBySerial;
                                            this.L$7 = deviceBySerial;
                                            this.label = 3;
                                            Object obj12 = obj11;
                                            if (gk6.a(b21, dVar, this) == obj12) {
                                                return obj12;
                                            }
                                        }
                                    }
                                }
                                break;
                            case 4:
                                String str31 = "error_code";
                                int i33 = i9;
                                String str32 = str9;
                                Object obj13 = obj5;
                                int i34 = i8;
                                int i35 = i33;
                                Intent intent3 = new Intent("BROADCAST_OTA_COMPLETE");
                                jl4 c10 = AnalyticsHelper.f.c(str5);
                                if (skuModelBySerialPrefix != null) {
                                    hashMap2 = new HashMap();
                                    String sku = skuModelBySerialPrefix.getSku();
                                    if (sku != null) {
                                        i5 = i34;
                                        String str33 = sku;
                                        jl42 = c10;
                                        str17 = str33;
                                    } else {
                                        jl42 = c10;
                                        i5 = i34;
                                        str17 = str32;
                                    }
                                    hashMap2.put("Style_Number", str17);
                                    String deviceName = skuModelBySerialPrefix.getDeviceName();
                                    if (deviceName == null) {
                                        deviceName = str32;
                                    }
                                    hashMap2.put("Device_Name", deviceName);
                                } else {
                                    jl42 = c10;
                                    i5 = i34;
                                    hashMap2 = null;
                                }
                                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal()) {
                                    Intent intent4 = intent3;
                                    HashMap hashMap4 = hashMap2;
                                    String str34 = str8;
                                    jl4 jl46 = jl42;
                                    if (hashMap4 != null) {
                                        str15 = str31;
                                        hashMap4.put(str15, String.valueOf(intExtra3));
                                        this.this$0.a.a("ota_fail", (Map<String, String>) hashMap4);
                                    } else {
                                        str15 = str31;
                                    }
                                    hl4 a10 = AnalyticsHelper.f.a("ota_session_optional_error");
                                    a10.a(str15, String.valueOf(intExtra3));
                                    if (jl46 != null) {
                                        jl46.a(a10);
                                        cd6 cd65 = cd6.a;
                                    }
                                    if (jl46 != null) {
                                        jl46.a(String.valueOf(intExtra3));
                                        cd6 cd66 = cd6.a;
                                    }
                                    intent4.putExtra(str34, false);
                                    intent = intent4;
                                    break;
                                } else {
                                    if (!bundle.isEmpty()) {
                                        this.this$0.a.a((MisfitDeviceProfile) bundle.getParcelable("device"));
                                    }
                                    if (this.this$0.a.a() != null) {
                                        ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
                                        String b22 = MFDeviceService.V.b();
                                        StringBuilder sb6 = new StringBuilder();
                                        hashMap3 = hashMap2;
                                        sb6.append("OTA complete, update fw version from device=");
                                        sb6.append(str32);
                                        local12.d(b22, sb6.toString());
                                        MisfitDeviceProfile a11 = this.this$0.a.a();
                                        if (a11 != null) {
                                            str16 = a11.getFirmwareVersion();
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        hashMap3 = hashMap2;
                                        ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
                                        String b23 = MFDeviceService.V.b();
                                        local13.d(b23, "OTA complete, update fw version from temp=" + str32);
                                        str16 = this.this$0.a.m().i(str6);
                                    }
                                    if (TextUtils.isEmpty(str16)) {
                                        str7 = str32;
                                        HashMap hashMap5 = hashMap3;
                                        intent = intent3;
                                        jl4 jl47 = jl42;
                                        break;
                                    } else {
                                        DeviceRepository d2 = this.this$0.a.d();
                                        if (str6 != null) {
                                            Device deviceBySerial2 = d2.getDeviceBySerial(str6);
                                            if (deviceBySerial2 == null) {
                                                intent2 = intent3;
                                                str7 = str32;
                                                jl4 = jl42;
                                                hashMap = hashMap3;
                                                break;
                                            } else {
                                                ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
                                                String b24 = MFDeviceService.V.b();
                                                str7 = str32;
                                                StringBuilder sb7 = new StringBuilder();
                                                int i36 = i6;
                                                sb7.append("OTA complete, update fw version=");
                                                if (str16 != null) {
                                                    sb7.append(str16);
                                                    local14.d(b24, sb7.toString());
                                                    deviceBySerial2.setFirmwareRevision(str16);
                                                    DeviceRepository d3 = this.this$0.a.d();
                                                    this.L$0 = il6;
                                                    this.I$0 = intExtra;
                                                    this.L$1 = communicateMode;
                                                    this.L$2 = str6;
                                                    this.I$1 = intExtra2;
                                                    this.I$2 = intExtra3;
                                                    this.L$3 = arrayList3;
                                                    this.L$4 = bundle;
                                                    this.L$5 = skuModelBySerialPrefix;
                                                    this.I$3 = i36;
                                                    this.I$4 = i5;
                                                    this.I$5 = i35;
                                                    intent2 = intent3;
                                                    this.L$6 = intent2;
                                                    jl4 = jl42;
                                                    this.L$7 = jl4;
                                                    hashMap = hashMap3;
                                                    this.L$8 = hashMap;
                                                    this.L$9 = str16;
                                                    this.L$10 = str16;
                                                    this.L$11 = deviceBySerial2;
                                                    this.L$12 = deviceBySerial2;
                                                    this.label = 4;
                                                    obj2 = d3.updateDevice(deviceBySerial2, false, this);
                                                    Object obj14 = obj13;
                                                    if (obj2 == obj14) {
                                                        return obj14;
                                                    }
                                                } else {
                                                    wg6.a();
                                                    throw null;
                                                }
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    }
                                }
                                break;
                            case 5:
                                PortfolioApp.get.a((Object) new li4(str6, intExtra2 == ServiceActionResult.SUCCEEDED.ordinal()));
                                break;
                            case 6:
                                if (intExtra2 != ServiceActionResult.SUCCEEDED.ordinal()) {
                                    PortfolioApp.get.a((Object) new gi4(str6, false));
                                    break;
                                } else {
                                    PortfolioApp.get.a((Object) new gi4(str6, true));
                                    break;
                                }
                            case 7:
                                if (!bundle.isEmpty()) {
                                    this.this$0.a.a((MisfitDeviceProfile) bundle.getParcelable("device"));
                                    MFDeviceService mFDeviceService2 = this.this$0.a;
                                    MisfitDeviceProfile a12 = mFDeviceService2.a();
                                    wg6.a((Object) str6, "serial");
                                    mFDeviceService2.a(a12, str6);
                                    break;
                                }
                                break;
                            case 8:
                                ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
                                String b25 = MFDeviceService.V.b();
                                StringBuilder sb8 = new StringBuilder();
                                int i37 = i9;
                                sb8.append("onExchangeSecretKey result ");
                                sb8.append(intExtra2);
                                local15.d(b25, sb8.toString());
                                if (intExtra2 != ServiceActionResult.ASK_FOR_RANDOM_KEY.ordinal()) {
                                    Object obj15 = obj5;
                                    int i38 = i8;
                                    int i39 = i37;
                                    if (intExtra2 != ServiceActionResult.ASK_FOR_SERVER_SECRET_KEY.ordinal()) {
                                        if (intExtra2 != ServiceActionResult.ASK_FOR_CURRENT_SECRET_KEY.ordinal()) {
                                            if (intExtra2 != ServiceActionResult.REQUEST_PUSH_SECRET_KEY_TO_CLOUD.ordinal()) {
                                                if (intExtra2 == ServiceActionResult.SUCCEEDED.ordinal() && !bundle.isEmpty()) {
                                                    String string = bundle.getString(ButtonService.DEVICE_SECRET_KEY);
                                                    if (!TextUtils.isEmpty(string)) {
                                                        ILocalFLogger local16 = FLogger.INSTANCE.getLocal();
                                                        String b26 = MFDeviceService.V.b();
                                                        local16.d(b26, "encrypt secret key " + string + " for " + str6);
                                                        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, str6, string, (xe6) null), 3, (Object) null);
                                                        break;
                                                    }
                                                }
                                            } else if (!bundle.isEmpty()) {
                                                String string2 = bundle.getString(ButtonService.DEVICE_SECRET_KEY);
                                                if (!TextUtils.isEmpty(string2)) {
                                                    ILocalFLogger local17 = FLogger.INSTANCE.getLocal();
                                                    String b27 = MFDeviceService.V.b();
                                                    local17.d(b27, "update secret key " + string2 + " for " + str6 + " to server");
                                                    rm6 unused2 = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, str6, string2, (xe6) null), 3, (Object) null);
                                                    break;
                                                } else {
                                                    PortfolioApp instance = PortfolioApp.get.instance();
                                                    if (str6 != null) {
                                                        instance.b(str6, false);
                                                        break;
                                                    } else {
                                                        wg6.a();
                                                        throw null;
                                                    }
                                                }
                                            }
                                        } else {
                                            PortfolioApp c11 = this.this$0.a.c();
                                            wg6.a((Object) str6, "serial");
                                            c11.j(str6);
                                            this.this$0.a.s().a(new VerifySecretKeyUseCase.b(str6), new C0060a(this, str6));
                                            break;
                                        }
                                    } else {
                                        String string3 = bundle.getString(ButtonService.DEVICE_RANDOM_KEY);
                                        Object obj16 = obj15;
                                        DeviceRepository d4 = this.this$0.a.d();
                                        if (string3 != null) {
                                            deviceRepository = d4;
                                            str18 = string3;
                                        } else {
                                            deviceRepository = d4;
                                            str18 = str9;
                                        }
                                        wg6.a((Object) str6, "serial");
                                        this.L$0 = il6;
                                        this.I$0 = intExtra;
                                        this.L$1 = communicateMode;
                                        this.L$2 = str6;
                                        this.I$1 = intExtra2;
                                        this.I$2 = intExtra3;
                                        this.L$3 = arrayList3;
                                        this.L$4 = bundle;
                                        this.L$5 = skuModelBySerialPrefix;
                                        this.I$3 = i6;
                                        this.I$4 = i38;
                                        this.I$5 = i39;
                                        this.L$6 = string3;
                                        this.label = 6;
                                        obj3 = deviceRepository.swapPairingKey(str18, str6, this);
                                        Object obj17 = obj16;
                                        if (obj3 == obj17) {
                                            return obj17;
                                        }
                                    }
                                } else {
                                    DeviceRepository d5 = this.this$0.a.d();
                                    wg6.a((Object) str6, "serial");
                                    this.L$0 = il6;
                                    this.I$0 = intExtra;
                                    this.L$1 = communicateMode;
                                    this.L$2 = str6;
                                    this.I$1 = intExtra2;
                                    this.I$2 = intExtra3;
                                    this.L$3 = arrayList3;
                                    this.L$4 = bundle;
                                    this.L$5 = skuModelBySerialPrefix;
                                    this.I$3 = i6;
                                    this.I$4 = i8;
                                    this.I$5 = i37;
                                    this.label = 5;
                                    obj4 = d5.generatePairingKey(str6, this);
                                    Object obj18 = obj5;
                                    if (obj4 == obj18) {
                                        return obj18;
                                    }
                                }
                                break;
                            case 9:
                                if (intExtra2 == ServiceActionResult.ASK_FOR_WATCH_PARAMS.ordinal()) {
                                    MFDeviceService mFDeviceService3 = this.this$0.a;
                                    wg6.a((Object) str6, "serial");
                                    mFDeviceService3.a(str6, bundle);
                                    break;
                                }
                                break;
                            default:
                                CommunicateMode communicateMode9 = communicateMode;
                                BleCommandResultManager.d.a(communicateMode9, new BleCommandResultManager.a(communicateMode9, str6, this.$intent));
                                break;
                        }
                    case 1:
                        DianaSyncDataProcessing.a aVar4 = (DianaSyncDataProcessing.a) this.L$12;
                        List list4 = (List) this.L$11;
                        i = this.I$6;
                        List list5 = (List) this.L$10;
                        List list6 = (List) this.L$9;
                        MFUser mFUser = (MFUser) this.L$8;
                        UserProfile userProfile = (UserProfile) this.L$7;
                        jl4 jl48 = (jl4) this.L$6;
                        SKUModel sKUModel5 = (SKUModel) this.L$5;
                        Bundle bundle6 = (Bundle) this.L$4;
                        ArrayList arrayList7 = (ArrayList) this.L$3;
                        str4 = (String) this.L$2;
                        CommunicateMode communicateMode10 = (CommunicateMode) this.L$1;
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                        break;
                    case 2:
                        HybridSyncDataProcessing.a aVar5 = (HybridSyncDataProcessing.a) this.L$12;
                        List list7 = (List) this.L$11;
                        i = this.I$6;
                        List list8 = (List) this.L$10;
                        List list9 = (List) this.L$9;
                        MFUser mFUser2 = (MFUser) this.L$8;
                        UserProfile userProfile2 = (UserProfile) this.L$7;
                        jl4 jl49 = (jl4) this.L$6;
                        SKUModel sKUModel6 = (SKUModel) this.L$5;
                        Bundle bundle7 = (Bundle) this.L$4;
                        ArrayList arrayList8 = (ArrayList) this.L$3;
                        str4 = (String) this.L$2;
                        CommunicateMode communicateMode11 = (CommunicateMode) this.L$1;
                        il6 il63 = (il6) this.L$0;
                        try {
                            nc6.a(obj);
                            break;
                        } catch (Exception e12) {
                            e = e12;
                            str2 = str4;
                            str = "sync_session";
                            aVar = this;
                            break;
                        }
                    case 3:
                        Device device = (Device) this.L$7;
                        Device device2 = (Device) this.L$6;
                        SKUModel sKUModel7 = (SKUModel) this.L$5;
                        Bundle bundle8 = (Bundle) this.L$4;
                        ArrayList arrayList9 = (ArrayList) this.L$3;
                        String str35 = (String) this.L$2;
                        CommunicateMode communicateMode12 = (CommunicateMode) this.L$1;
                        il6 il64 = (il6) this.L$0;
                        nc6.a(obj);
                    case 4:
                        Device device3 = (Device) this.L$12;
                        Device device4 = (Device) this.L$11;
                        String str36 = (String) this.L$10;
                        String str37 = (String) this.L$9;
                        jl4 = (jl4) this.L$7;
                        SKUModel sKUModel8 = (SKUModel) this.L$5;
                        Bundle bundle9 = (Bundle) this.L$4;
                        ArrayList arrayList10 = (ArrayList) this.L$3;
                        CommunicateMode communicateMode13 = (CommunicateMode) this.L$1;
                        il6 il65 = (il6) this.L$0;
                        nc6.a(obj);
                        str5 = "ota_session";
                        str8 = "OTA_RESULT";
                        str7 = "";
                        intent2 = (Intent) this.L$6;
                        str6 = (String) this.L$2;
                        hashMap = (HashMap) this.L$8;
                        obj2 = obj;
                        break;
                    case 5:
                        SKUModel sKUModel9 = (SKUModel) this.L$5;
                        Bundle bundle10 = (Bundle) this.L$4;
                        ArrayList arrayList11 = (ArrayList) this.L$3;
                        CommunicateMode communicateMode14 = (CommunicateMode) this.L$1;
                        il6 il66 = (il6) this.L$0;
                        nc6.a(obj);
                        str6 = (String) this.L$2;
                        str9 = "";
                        obj4 = obj;
                        break;
                    case 6:
                        String str38 = (String) this.L$6;
                        SKUModel sKUModel10 = (SKUModel) this.L$5;
                        Bundle bundle11 = (Bundle) this.L$4;
                        ArrayList arrayList12 = (ArrayList) this.L$3;
                        CommunicateMode communicateMode15 = (CommunicateMode) this.L$1;
                        il6 il67 = (il6) this.L$0;
                        nc6.a(obj);
                        str6 = (String) this.L$2;
                        str9 = "";
                        obj3 = obj;
                        break;
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                z = true;
                ILocalFLogger local10222222 = FLogger.INSTANCE.getLocal();
                String b15222222 = MFDeviceService.V.b();
                StringBuilder sb5222222 = new StringBuilder();
                sb5222222.append("Error while processing sync data - e=");
                e.printStackTrace();
                sb5222222.append(cd6.a);
                local10222222.e(b15222222, sb5222222.toString());
                e.printStackTrace();
                PortfolioApp c5222222 = aVar.this$0.a.c();
                CommunicateMode communicateMode6222222 = CommunicateMode.SYNC;
                c5222222.a(communicateMode6222222, str2, "Sync data result OK but has exception error e=" + e);
                IRemoteFLogger remote5222222 = FLogger.INSTANCE.getRemote();
                FLogger.Component component5222222 = FLogger.Component.APP;
                FLogger.Session session5222222 = FLogger.Session.SYNC;
                String b16222222 = MFDeviceService.V.b();
                remote5222222.i(component5222222, session5222222, str2, b16222222, "Sync data result OK but has exception error e=" + e);
                aVar.this$0.a.c().c(z);
                AnalyticsHelper.f.e(str);
                FossilNotificationBar.c.a(aVar.this$0.a);
                return cd6.a;
            }
        }

        @DexIgnore
        public d(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new a(this, intent, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends BroadcastReceiver {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.service.MFDeviceService$connectionStateChangeReceiver$1$onReceive$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ Context $context;
            @DexIgnore
            public /* final */ /* synthetic */ Intent $intent;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.service.MFDeviceService$e$a$a")
            /* renamed from: com.portfolio.platform.service.MFDeviceService$e$a$a  reason: collision with other inner class name */
            public static final class C0062a implements s04.b {
                @DexIgnore
                public /* final */ /* synthetic */ a a;
                @DexIgnore
                public /* final */ /* synthetic */ String b;

                @DexIgnore
                public C0062a(a aVar, String str) {
                    this.a = aVar;
                    this.b = str;
                }

                @DexIgnore
                /* JADX WARNING: type inference failed for: r4v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
                public void a(Location location, int i) {
                    MFDeviceService mFDeviceService = this.a.this$0.a;
                    String str = this.b;
                    wg6.a((Object) str, "serial");
                    mFDeviceService.a(str, location, i);
                    if (location != null) {
                        s04.a((Context) this.a.this$0.a.c()).b((s04.b) this);
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, Intent intent, Context context, xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
                this.$intent = intent;
                this.$context = context;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, this.$intent, this.$context, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r8v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    String stringExtra = this.$intent.getStringExtra(Constants.SERIAL_NUMBER);
                    int intExtra = this.$intent.getIntExtra(Constants.CONNECTION_STATE, -1);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.V.b();
                    local.d(b, "---Inside .connectionStateChangeReceiver " + stringExtra + " status " + intExtra);
                    if (intExtra == ConnectionStateChange.GATT_ON.ordinal()) {
                        DeviceHelper e = DeviceHelper.o.e();
                        wg6.a((Object) stringExtra, "serial");
                        MisfitDeviceProfile a = e.a(stringExtra);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.V.b();
                        local2.d(b2, "---Inside .connectionStateChangeReceiver " + stringExtra + " deviceProfile=" + a);
                        if (a != null) {
                            PortfolioApp.get.instance().a(a.getBatteryLevel());
                        }
                        this.this$0.a.a(a, stringExtra, true);
                        if (!TextUtils.isEmpty(stringExtra) && wg6.a((Object) stringExtra, (Object) PortfolioApp.get.instance().e())) {
                            this.this$0.a.x();
                        }
                        this.this$0.a.z();
                        if (((MusicControlComponent) MusicControlComponent.o.a(this.$context)).b(stringExtra)) {
                            ((MusicControlComponent) MusicControlComponent.o.a(this.$context)).c();
                        }
                    } else if (intExtra == ConnectionStateChange.GATT_OFF.ordinal() && !TextUtils.isEmpty(stringExtra) && wg6.a((Object) stringExtra, (Object) PortfolioApp.get.instance().e())) {
                        this.this$0.a.y();
                    }
                    PortfolioApp.get.a((Object) new oi4(stringExtra, intExtra));
                    MFDeviceService mFDeviceService = this.this$0.a;
                    wg6.a((Object) stringExtra, "serial");
                    mFDeviceService.c(stringExtra);
                    if (this.this$0.a.m().G()) {
                        FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Collect location data only user enable locate feature");
                        s04.a((Context) this.this$0.a).a((s04.b) new C0062a(this, stringExtra));
                    }
                    FossilNotificationBar.c.a(this.this$0.a);
                    ce.a(this.this$0.a.c()).a(this.$intent);
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public e(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new a(this, intent, context, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Device app event receive");
            if (intent != null) {
                PortfolioApp.get.a((Object) new mi4(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getExtras()));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.MFDeviceService$executeGetWatchParamsFlow$1", f = "MFDeviceService.kt", l = {711, 713, 715}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $currentWPVersion;
        @DexIgnore
        public /* final */ /* synthetic */ int $majorVersion;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(MFDeviceService mFDeviceService, String str, int i, float f, xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$majorVersion = i;
            this.$currentWPVersion = f;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, this.$serial, this.$majorVersion, this.$currentWPVersion, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                DeviceRepository d = this.this$0.d();
                String str = this.$serial;
                int i2 = this.$majorVersion;
                this.L$0 = il6;
                this.label = 1;
                obj = d.getLatestWatchParamFromServer(str, i2, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2 || i == 3) {
                WatchParameterResponse watchParameterResponse = (WatchParameterResponse) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            WatchParameterResponse watchParameterResponse2 = (WatchParameterResponse) obj;
            if (watchParameterResponse2 != null) {
                WatchParamHelper t = this.this$0.t();
                String str2 = this.$serial;
                float f = this.$currentWPVersion;
                this.L$0 = il6;
                this.L$1 = watchParameterResponse2;
                this.label = 2;
                if (t.a(str2, f, watchParameterResponse2, this) == a) {
                    return a;
                }
            } else {
                WatchParamHelper t2 = this.this$0.t();
                String str3 = this.$serial;
                float f2 = this.$currentWPVersion;
                this.L$0 = il6;
                this.L$1 = watchParameterResponse2;
                this.label = 3;
                if (t2.a(str3, f2, this) == a) {
                    return a;
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            if (intent != null) {
                int intExtra = intent.getIntExtra(Constants.DAILY_STEPS, 0);
                int intExtra2 = intent.getIntExtra(Constants.DAILY_POINTS, 0);
                Calendar instance = Calendar.getInstance();
                wg6.a((Object) instance, "Calendar.getInstance()");
                long longExtra = intent.getLongExtra(Constants.UPDATED_TIME, instance.getTimeInMillis());
                FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), ".saveSyncResult - Update heartbeat step by heartbeat receiver");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = MFDeviceService.V.b();
                local.i(b, "Heartbeat data received, dailySteps=" + intExtra + ", dailyPoints=" + intExtra2 + ", receivedTime=" + longExtra);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ServiceConnection {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.service.MFDeviceService$mButtonConnectivity$1$onServiceConnected$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ IBinder $service;
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(IBinder iBinder, xe6 xe6) {
                super(2, xe6);
                this.$service = iBinder;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.$service, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    PortfolioApp.inner inner = PortfolioApp.get;
                    IButtonConnectivity asInterface = IButtonConnectivity.Stub.asInterface(this.$service);
                    wg6.a((Object) asInterface, "IButtonConnectivity.Stub.asInterface(service)");
                    inner.b(asInterface);
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public i(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            wg6.b(componentName, "name");
            wg6.b(iBinder, Constants.SERVICE);
            FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Connection to the BLE has been established");
            this.a.a(true);
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new a(iBinder, (xe6) null), 3, (Object) null);
            this.a.A();
            this.a.z();
            FossilNotificationBar.c.a(this.a);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            wg6.b(componentName, "name");
            this.a.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Micro app cancel event receive");
            if (intent != null) {
                PortfolioApp.get.a((Object) new ni4(intent.getStringExtra(Constants.SERIAL_NUMBER), intent.getIntExtra(Constants.MICRO_APP_ID, -1), intent.getIntExtra(Constants.VARIANT_ID, -1), intent.getIntExtra("gesture", -1)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Notification event receive");
            if (intent != null) {
                PortfolioApp.get.a((Object) new ri4(intent.getIntExtra(ButtonService.NOTIFICATION_ID, -1), intent.getBooleanExtra(ButtonService.NOTIFICATION_RESULT, false)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.MFDeviceService$onLocationUpdated$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
    public static final class l extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ DeviceLocation $deviceLocation;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(DeviceLocation deviceLocation, xe6 xe6) {
            super(2, xe6);
            this.$deviceLocation = deviceLocation;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            l lVar = new l(this.$deviceLocation, xe6);
            lVar.p$ = (il6) obj;
            return lVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((l) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                zm4.p.a().g().saveDeviceLocation(this.$deviceLocation);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService a;

        @DexIgnore
        public m(MFDeviceService mFDeviceService) {
            this.a = mFDeviceService;
        }

        @DexIgnore
        public final void run() {
            if (xj6.b("release", Constants.DEBUG, true) && wg6.a((Object) this.a.c().i(), (Object) qh4.PORTFOLIO.getName())) {
                this.a.c().K();
                this.a.x();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            wg6.b(intent, "intent");
            intent.setAction("SCAN_DEVICE_FOUND");
            ce.a(context).a(intent);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            wg6.b(context, "context");
            FLogger.INSTANCE.getLocal().d(MFDeviceService.V.b(), "Streaming event receive");
            if (intent != null) {
                int intExtra = intent.getIntExtra("gesture", -1);
                PortfolioApp.get.a((Object) new si4(intent.getStringExtra(Constants.SERIAL_NUMBER), intExtra));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.MFDeviceService$updateDeviceData$1", f = "MFDeviceService.kt", l = {903, 912}, m = "invokeSuspend")
    public static final class p extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $deviceProfile;
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isNeedUpdateRemote;
        @DexIgnore
        public /* final */ /* synthetic */ String $serial;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(MFDeviceService mFDeviceService, String str, boolean z, MisfitDeviceProfile misfitDeviceProfile, xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
            this.$serial = str;
            this.$isNeedUpdateRemote = z;
            this.$deviceProfile = misfitDeviceProfile;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            p pVar = new p(this.this$0, this.$serial, this.$isNeedUpdateRemote, this.$deviceProfile, xe6);
            pVar.p$ = (il6) obj;
            return pVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((p) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:35:0x0225 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0230  */
        public final Object invokeSuspend(Object obj) {
            Device device;
            il6 il6;
            Device device2;
            MisfitDeviceProfile misfitDeviceProfile;
            VibrationStrengthObj vibrationStrengthObj;
            DeviceRepository d;
            boolean z;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                device2 = this.this$0.d().getDeviceBySerial(this.$serial);
                if (device2 != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.V.b();
                    local.d(b, "updateDeviceData localDevice " + device2 + " forceUpdateRemote " + this.$isNeedUpdateRemote);
                    MisfitDeviceProfile misfitDeviceProfile2 = this.$deviceProfile;
                    if (misfitDeviceProfile2 != null) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String b2 = MFDeviceService.V.b();
                        local2.d(b2, "updateDeviceData batteryLevel " + this.$deviceProfile.getBatteryLevel() + " fwVersion " + this.$deviceProfile.getFirmwareVersion() + " major " + this.$deviceProfile.getMicroAppMajorVersion() + " minor " + this.$deviceProfile.getMicroAppMinorVersion());
                        vibrationStrengthObj = this.$deviceProfile.getVibrationStrength();
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String b3 = MFDeviceService.V.b();
                        StringBuilder sb = new StringBuilder();
                        sb.append("updateDeviceData - isDefaultValue: ");
                        sb.append(vibrationStrengthObj.isDefaultValue());
                        local3.d(b3, sb.toString());
                        if (!vibrationStrengthObj.isDefaultValue()) {
                            int b4 = jk4.b(vibrationStrengthObj.getVibrationStrengthLevel());
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String b5 = MFDeviceService.V.b();
                            local4.d(b5, "updateDeviceData - newVibrationLvl: " + b4 + " - device: " + device2);
                            device2.setVibrationStrength(hf6.a(b4));
                        }
                        if (this.$deviceProfile.getBatteryLevel() > 0) {
                            device2.setBatteryLevel(this.$deviceProfile.getBatteryLevel());
                            if (device2.getBatteryLevel() > 100) {
                                device2.setBatteryLevel(100);
                            }
                        }
                        device2.setFirmwareRevision(this.$deviceProfile.getFirmwareVersion());
                        device2.setSku(this.$deviceProfile.getDeviceModel());
                        device2.setMacAddress(this.$deviceProfile.getAddress());
                        if ((device2.getMajor() == this.$deviceProfile.getMicroAppMajorVersion() && device2.getMinor() == this.$deviceProfile.getMicroAppMinorVersion()) ? false : true) {
                            MicroAppRepository k = this.this$0.k();
                            String str = this.$serial;
                            String valueOf = String.valueOf(this.$deviceProfile.getMicroAppMajorVersion());
                            String valueOf2 = String.valueOf(this.$deviceProfile.getMicroAppMinorVersion());
                            this.L$0 = il6;
                            this.L$1 = device2;
                            this.L$2 = device2;
                            this.L$3 = misfitDeviceProfile2;
                            this.L$4 = vibrationStrengthObj;
                            this.label = 1;
                            if (k.downloadMicroAppVariant(str, valueOf, valueOf2, this) == a) {
                                return a;
                            }
                            misfitDeviceProfile = misfitDeviceProfile2;
                            device = device2;
                        } else {
                            misfitDeviceProfile = misfitDeviceProfile2;
                            device = device2;
                            device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
                            device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
                            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                            String b6 = MFDeviceService.V.b();
                            local5.d(b6, "update device data " + device2);
                            d = this.this$0.d();
                            z = this.$isNeedUpdateRemote;
                            this.L$0 = il6;
                            this.L$1 = device2;
                            this.L$2 = device;
                            this.L$3 = misfitDeviceProfile;
                            this.L$4 = vibrationStrengthObj;
                            this.label = 2;
                            if (d.updateDevice(device2, z, this) == a) {
                                return a;
                            }
                            if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                            }
                        }
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                vibrationStrengthObj = (VibrationStrengthObj) this.L$4;
                misfitDeviceProfile = (MisfitDeviceProfile) this.L$3;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                device = (Device) this.L$2;
                device2 = (Device) this.L$1;
            } else if (i == 2) {
                VibrationStrengthObj vibrationStrengthObj2 = (VibrationStrengthObj) this.L$4;
                MisfitDeviceProfile misfitDeviceProfile3 = (MisfitDeviceProfile) this.L$3;
                Device device3 = (Device) this.L$2;
                Device device4 = (Device) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
                    this.this$0.m().a(this.$deviceProfile.getHeartRateMode());
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.m().a(this.$serial, 0, false);
            device2.setMajor(this.$deviceProfile.getMicroAppMajorVersion());
            device2.setMinor(this.$deviceProfile.getMicroAppMinorVersion());
            ILocalFLogger local52 = FLogger.INSTANCE.getLocal();
            String b62 = MFDeviceService.V.b();
            local52.d(b62, "update device data " + device2);
            d = this.this$0.d();
            z = this.$isNeedUpdateRemote;
            this.L$0 = il6;
            this.L$1 = device2;
            this.L$2 = device;
            this.L$3 = misfitDeviceProfile;
            this.L$4 = vibrationStrengthObj;
            this.label = 2;
            if (d.updateDevice(device2, z, this) == a) {
            }
            if (this.$deviceProfile.getHeartRateMode() != HeartRateMode.NONE) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.MFDeviceService$updatePairedDeviceToButtonService$1", f = "MFDeviceService.kt", l = {}, m = "invokeSuspend")
    public static final class q extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(MFDeviceService mFDeviceService, xe6 xe6) {
            super(2, xe6);
            this.this$0 = mFDeviceService;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            q qVar = new q(this.this$0, xe6);
            qVar.p$ = (il6) obj;
            return qVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((q) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                List<Device> allDevice = this.this$0.d().getAllDevice();
                if (!allDevice.isEmpty()) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String b = MFDeviceService.V.b();
                    local.d(b, "Get all device success devicesList=" + allDevice);
                    if (this.this$0.r().getCurrentUser() != null) {
                        for (Device next : allDevice) {
                            this.this$0.c().d(next.component1(), next.component2());
                        }
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class r extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $serial$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ MisfitDeviceProfile $this_run;
        @DexIgnore
        public int I$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ MFDeviceService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public r(MisfitDeviceProfile misfitDeviceProfile, xe6 xe6, MFDeviceService mFDeviceService, String str) {
            super(2, xe6);
            this.$this_run = misfitDeviceProfile;
            this.this$0 = mFDeviceService;
            this.$serial$inlined = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            r rVar = new r(this.$this_run, xe6, this.this$0, this.$serial$inlined);
            rVar.p$ = (il6) obj;
            return rVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((r) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Integer vibrationStrength;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                Device deviceBySerial = this.this$0.d().getDeviceBySerial(this.$serial$inlined);
                int b = jk4.b(this.$this_run.getVibrationStrength().getVibrationStrengthLevel());
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = MFDeviceService.V.b();
                local.d(b2, "newVibrationLvl: " + b + " - device: " + deviceBySerial);
                if (deviceBySerial != null && ((vibrationStrength = deviceBySerial.getVibrationStrength()) == null || vibrationStrength.intValue() != b)) {
                    deviceBySerial.setVibrationStrength(hf6.a(b));
                    DeviceRepository d = this.this$0.d();
                    this.L$0 = il6;
                    this.L$1 = deviceBySerial;
                    this.I$0 = b;
                    this.label = 1;
                    if (d.updateDevice(deviceBySerial, false, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                Device device = (Device) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = MFDeviceService.class.getSimpleName();
        wg6.a((Object) simpleName, "MFDeviceService::class.java.simpleName");
        T = simpleName;
        StringBuilder sb = new StringBuilder();
        Package packageR = MFDeviceService.class.getPackage();
        if (packageR != null) {
            wg6.a((Object) packageR, "MFDeviceService::class.java.`package`!!");
            sb.append(packageR.getName());
            sb.append(".location_updated");
            U = sb.toString();
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public final rm6 A() {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new q(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final ActivitiesRepository b() {
        ActivitiesRepository activitiesRepository = this.d;
        if (activitiesRepository != null) {
            return activitiesRepository;
        }
        wg6.d("mActivitiesRepository");
        throw null;
    }

    @DexIgnore
    public final PortfolioApp c() {
        PortfolioApp portfolioApp = this.v;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wg6.d("mApp");
        throw null;
    }

    @DexIgnore
    public final DeviceRepository d() {
        DeviceRepository deviceRepository = this.c;
        if (deviceRepository != null) {
            return deviceRepository;
        }
        wg6.d("mDeviceRepository");
        throw null;
    }

    @DexIgnore
    public final mw5 e() {
        mw5 mw5 = this.y;
        if (mw5 != null) {
            return mw5;
        }
        wg6.d("mEncryptUseCase");
        throw null;
    }

    @DexIgnore
    public final FitnessDataRepository f() {
        FitnessDataRepository fitnessDataRepository = this.r;
        if (fitnessDataRepository != null) {
            return fitnessDataRepository;
        }
        wg6.d("mFitnessDataRepository");
        throw null;
    }

    @DexIgnore
    public final ik4 g() {
        ik4 ik4 = this.A;
        if (ik4 != null) {
            return ik4;
        }
        wg6.d("mFitnessHelper");
        throw null;
    }

    @DexIgnore
    public final GoalTrackingRepository h() {
        GoalTrackingRepository goalTrackingRepository = this.s;
        if (goalTrackingRepository != null) {
            return goalTrackingRepository;
        }
        wg6.d("mGoalTrackingRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSampleRepository i() {
        HeartRateSampleRepository heartRateSampleRepository = this.o;
        if (heartRateSampleRepository != null) {
            return heartRateSampleRepository;
        }
        wg6.d("mHeartRateSampleRepository");
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryRepository j() {
        HeartRateSummaryRepository heartRateSummaryRepository = this.p;
        if (heartRateSummaryRepository != null) {
            return heartRateSummaryRepository;
        }
        wg6.d("mHeartRateSummaryRepository");
        throw null;
    }

    @DexIgnore
    public final MicroAppRepository k() {
        MicroAppRepository microAppRepository = this.j;
        if (microAppRepository != null) {
            return microAppRepository;
        }
        wg6.d("mMicroAppRepository");
        throw null;
    }

    @DexIgnore
    public final HybridPresetRepository l() {
        HybridPresetRepository hybridPresetRepository = this.i;
        if (hybridPresetRepository != null) {
            return hybridPresetRepository;
        }
        wg6.d("mPresetRepository");
        throw null;
    }

    @DexIgnore
    public final an4 m() {
        an4 an4 = this.b;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final SleepSessionsRepository n() {
        SleepSessionsRepository sleepSessionsRepository = this.f;
        if (sleepSessionsRepository != null) {
            return sleepSessionsRepository;
        }
        wg6.d("mSleepSessionsRepository");
        throw null;
    }

    @DexIgnore
    public final SleepSummariesRepository o() {
        SleepSummariesRepository sleepSummariesRepository = this.g;
        if (sleepSummariesRepository != null) {
            return sleepSummariesRepository;
        }
        wg6.d("mSleepSummariesRepository");
        throw null;
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        FLogger.INSTANCE.getLocal().i(T, "on misfit service bind");
        return this.a;
    }

    @DexIgnore
    public void onCreate() {
        super.onCreate();
        PortfolioApp.get.instance().g().a(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = T;
        local.d(str, "Inside " + T + ".onCreate");
        v();
    }

    @DexIgnore
    public void onDestroy() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = T;
        local.d(str, "Inside " + T + ".onDestroy");
        super.onDestroy();
        if (this.H) {
            zx5.a.a((Context) this, (ServiceConnection) this.G);
        }
        unregisterReceiver(this.K);
        unregisterReceiver(this.S);
        unregisterReceiver(this.N);
        unregisterReceiver(this.L);
        unregisterReceiver(this.R);
        unregisterReceiver(this.P);
        unregisterReceiver(this.O);
        unregisterReceiver(this.Q);
        unregisterReceiver(this.E);
        unregisterReceiver(this.M);
        this.D = false;
    }

    @DexIgnore
    public int onStartCommand(Intent intent, int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = T;
        local.i(str, "onStartCommand() - isButtonServiceBound=" + this.H);
        if (xj6.b(intent != null ? intent.getAction() : null, com.misfit.frameworks.buttonservice.utils.Constants.START_FOREGROUND_ACTION, false, 2, (Object) null)) {
            FLogger.INSTANCE.getLocal().d(T, "onStartCommand() - Received Start Foreground Intent");
            FossilNotificationBar.c.a((Context) this, (Service) this, false);
        } else {
            FLogger.INSTANCE.getLocal().d(T, "onStartCommand() - Received Stop Foreground Intent");
            FossilNotificationBar.c.a((Context) this, (Service) this, true);
        }
        if (!this.H) {
            zx5.a.a((Context) this, ButtonService.class, (ServiceConnection) this.G, 1);
        }
        return 1;
    }

    @DexIgnore
    public void onTaskRemoved(Intent intent) {
        super.onTaskRemoved(intent);
        FLogger.INSTANCE.getLocal().d(T, "onTaskRemoved()");
        stopSelf();
        sendBroadcast(new Intent(this, RestartServiceReceiver.class));
    }

    @DexIgnore
    public final SummariesRepository p() {
        SummariesRepository summariesRepository = this.e;
        if (summariesRepository != null) {
            return summariesRepository;
        }
        wg6.d("mSummariesRepository");
        throw null;
    }

    @DexIgnore
    public final ThirdPartyRepository q() {
        ThirdPartyRepository thirdPartyRepository = this.x;
        if (thirdPartyRepository != null) {
            return thirdPartyRepository;
        }
        wg6.d("mThirdPartyRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository r() {
        UserRepository userRepository = this.h;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final VerifySecretKeyUseCase s() {
        VerifySecretKeyUseCase verifySecretKeyUseCase = this.z;
        if (verifySecretKeyUseCase != null) {
            return verifySecretKeyUseCase;
        }
        wg6.d("mVerifySecretKeyUseCase");
        throw null;
    }

    @DexIgnore
    public final WatchParamHelper t() {
        WatchParamHelper watchParamHelper = this.B;
        if (watchParamHelper != null) {
            return watchParamHelper;
        }
        wg6.d("mWatchParamHelper");
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionRepository u() {
        WorkoutSessionRepository workoutSessionRepository = this.q;
        if (workoutSessionRepository != null) {
            return workoutSessionRepository;
        }
        wg6.d("mWorkoutSessionRepository");
        throw null;
    }

    @DexIgnore
    public final void v() {
        if (this.D) {
            FLogger.INSTANCE.getLocal().e(T, "Return from device service register receiver");
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = T;
        local.i(str, "Inside " + T + ".registerReceiver " + b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE()));
        registerReceiver(this.K, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_BLE_RESPONSE())));
        registerReceiver(this.S, new IntentFilter(b(ButtonService.Companion.getACTION_ANALYTIC_EVENT())));
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = T;
        local2.i(str2, "Inside " + T + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT());
        registerReceiver(this.N, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_STREAMING_EVENT())));
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = T;
        local3.i(str3, "Inside " + T + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT());
        registerReceiver(this.O, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_MICRO_APP_CANCEL_EVENT())));
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = T;
        local4.i(str4, "Inside " + T + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT());
        registerReceiver(this.P, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_DEVICE_APP_EVENT())));
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = T;
        local5.i(str5, "Inside " + T + ".registerReceiver " + ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA());
        registerReceiver(this.Q, new IntentFilter(b(ButtonService.Companion.getACTION_SERVICE_HEARTBEAT_DATA())));
        registerReceiver(this.L, new IntentFilter(b(ButtonService.Companion.getACTION_CONNECTION_STATE_CHANGE())));
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = T;
        local6.i(str6, "Inside " + T + ".registerReceiver " + ButtonService.Companion.getACTION_NOTIFICATION_SENT());
        registerReceiver(this.M, new IntentFilter(b(ButtonService.Companion.getACTION_NOTIFICATION_SENT())));
        registerReceiver(this.R, new IntentFilter(b(ButtonService.Companion.getACTION_SCAN_DEVICE_FOUND())));
        registerReceiver(this.E, new IntentFilter("android.intent.action.TIME_TICK"));
        this.D = true;
    }

    @DexIgnore
    public final void w() {
        LinkStreamingManager linkStreamingManager = this.w;
        if (linkStreamingManager != null) {
            linkStreamingManager.a();
            PortfolioApp.get.instance().g().a(this);
            return;
        }
        wg6.d("mLinkStreamingManager");
        throw null;
    }

    @DexIgnore
    public final void x() {
        y();
        FLogger.INSTANCE.getLocal().d(T, "Start reading realtime step timeoutHandler 30 mins");
        this.I = new Handler(getMainLooper());
        Handler handler = this.I;
        if (handler != null) {
            handler.postDelayed(this.J, 1800000);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void y() {
        if (this.I != null) {
            FLogger.INSTANCE.getLocal().d(T, "Stop reading realtime step timeoutHandler");
            Handler handler = this.I;
            if (handler != null) {
                handler.removeCallbacks(this.J);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final void z() {
        boolean b2 = FossilNotificationListenerService.s.b();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = T;
        local.d(str, "triggerReconnectNotificationService() - isConnectedNotificationManager = " + b2);
        if (!b2 && xx5.a.e()) {
            FossilNotificationListenerService.s.c();
        }
    }

    @DexIgnore
    public final String b(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = T;
        local.d(str2, "Key=" + getPackageName() + str);
        return getPackageName() + str;
    }

    @DexIgnore
    public final void c(String str) {
        wg6.b(str, "serial");
        LocationProvider g2 = zm4.p.a().g();
        DeviceLocation deviceLocation = g2.getDeviceLocation(str);
        double d2 = 0.0d;
        double latitude = deviceLocation != null ? deviceLocation.getLatitude() : 0.0d;
        if (deviceLocation != null) {
            d2 = deviceLocation.getLongitude();
        }
        g2.saveDeviceLocation(new DeviceLocation(str, latitude, d2, System.currentTimeMillis()));
    }

    @DexIgnore
    public final MisfitDeviceProfile a() {
        return this.C;
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile) {
        this.C = misfitDeviceProfile;
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        int i2 = bundle.getInt(ButtonService.WATCH_PARAMS_MAJOR);
        float f2 = bundle.getFloat(ButtonService.CURRENT_WATCH_PARAMS_VERSION);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new g(this, str, i2, f2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final List<FitnessDataWrapper> a(List<FitnessData> list, String str, DateTime dateTime) {
        wg6.b(list, "syncData");
        wg6.b(str, "serial");
        wg6.b(dateTime, DataFile.COLUMN_SYNC_TIME);
        ArrayList arrayList = new ArrayList();
        for (FitnessData fitnessData : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = T;
            local.d(str2, "saveFitnessData=" + fitnessData);
            arrayList.add(new FitnessDataWrapper(fitnessData, str, dateTime));
        }
        return arrayList;
    }

    @DexIgnore
    public static /* synthetic */ void a(MFDeviceService mFDeviceService, String str, int i2, int i3, ArrayList arrayList, Integer num, int i4, Object obj) {
        if ((i4 & 8) != 0) {
            arrayList = new ArrayList(i3);
        }
        ArrayList arrayList2 = arrayList;
        if ((i4 & 16) != 0) {
            num = 10;
        }
        mFDeviceService.a(str, i2, i3, arrayList2, num);
    }

    @DexIgnore
    public final void a(String str, int i2, int i3, ArrayList<Integer> arrayList, Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = T;
        local.i(str2, "Broadcast sync status=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", i2);
        intent.putExtra("LAST_ERROR_CODE", i3);
        intent.putExtra(ButtonService.Companion.getORIGINAL_SYNC_MODE(), num);
        intent.putIntegerArrayListExtra("LIST_ERROR_CODE", arrayList);
        intent.putExtra("SERIAL", str);
        BleCommandResultManager bleCommandResultManager = BleCommandResultManager.d;
        CommunicateMode communicateMode = CommunicateMode.SYNC;
        bleCommandResultManager.a(communicateMode, new BleCommandResultManager.a(communicateMode, str, intent));
    }

    @DexIgnore
    public final void a(MisfitDeviceProfile misfitDeviceProfile, String str) {
        wg6.b(str, "serial");
        if (misfitDeviceProfile != null) {
            boolean isDefaultValue = misfitDeviceProfile.getVibrationStrength().isDefaultValue();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = T;
            local.d(str2, "updateVibrationStrengthLevel - isDefaultValue: " + isDefaultValue);
            if (!isDefaultValue) {
                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new r(misfitDeviceProfile, (xe6) null, this, str), 3, (Object) null);
            }
        }
    }

    @DexIgnore
    public final rm6 a(MisfitDeviceProfile misfitDeviceProfile, String str, boolean z2) {
        wg6.b(str, "serial");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new p(this, str, z2, misfitDeviceProfile, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a(String str, Location location, int i2) {
        wg6.b(str, "serial");
        if (i2 >= 0) {
            if (location != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = T;
                local.d(str2, "Inside " + T + ".onLocationUpdated - location=[lat:" + location.getLatitude() + ", lon:" + location.getLongitude() + ", accuracy:" + location.getAccuracy() + "]");
                if (location.getAccuracy() <= 500.0f) {
                    try {
                        DeviceLocation deviceLocation = new DeviceLocation(str, location.getLatitude(), location.getLongitude(), System.currentTimeMillis());
                        a(str, deviceLocation);
                        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new l(deviceLocation, (xe6) null), 3, (Object) null);
                    } catch (Exception e2) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = T;
                        local2.e(str3, "Error inside " + T + ".onLocationUpdated - e=" + e2);
                    }
                }
            }
        } else if (i2 != -1) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = T;
            local3.e(str4, "Error inside " + T + ".onLocationUpdated - code=" + i2);
        }
    }

    @DexIgnore
    public final void a(String str, DeviceLocation deviceLocation) {
        Intent intent = new Intent();
        intent.putExtra("SERIAL", str);
        intent.putExtra("device_location", deviceLocation);
        intent.setAction(U);
        sendBroadcast(intent);
    }

    @DexIgnore
    public final void a(SKUModel sKUModel, int i2, int i3) {
        if (sKUModel != null) {
            if (i3 == 1) {
                AnalyticsHelper analyticsHelper = this.u;
                if (analyticsHelper != null) {
                    String sku = sKUModel.getSku();
                    if (sku == null) {
                        sku = "";
                    }
                    String deviceName = sKUModel.getDeviceName();
                    if (deviceName == null) {
                        deviceName = "";
                    }
                    analyticsHelper.c(sku, deviceName, i2);
                } else {
                    wg6.d("mAnalyticsHelper");
                    throw null;
                }
            } else if (i3 == 2) {
                AnalyticsHelper analyticsHelper2 = this.u;
                if (analyticsHelper2 != null) {
                    String sku2 = sKUModel.getSku();
                    if (sku2 == null) {
                        sku2 = "";
                    }
                    String deviceName2 = sKUModel.getDeviceName();
                    if (deviceName2 == null) {
                        deviceName2 = "";
                    }
                    analyticsHelper2.b(sku2, deviceName2, i2);
                } else {
                    wg6.d("mAnalyticsHelper");
                    throw null;
                }
            }
            an4 an4 = this.b;
            if (an4 != null) {
                an4.a((Boolean) false);
            } else {
                wg6.d("mSharedPreferencesManager");
                throw null;
            }
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, Constants.EVENT);
        AnalyticsHelper.f.c().a(str);
    }

    @DexIgnore
    public final void a(String str, Map<String, String> map) {
        wg6.b(str, Constants.EVENT);
        AnalyticsHelper.f.c().a(str, (Map<String, ? extends Object>) map);
    }
}
