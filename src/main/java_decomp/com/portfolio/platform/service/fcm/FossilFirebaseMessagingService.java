package com.portfolio.platform.service.fcm;

import android.text.TextUtils;
import com.fossil.ct3;
import com.fossil.dp5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FossilFirebaseMessagingService extends FirebaseMessagingService {
    @DexIgnore
    public dp5 g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void a(ct3 ct3) {
        String str;
        wg6.b(ct3, "remoteMessage");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("From: ");
        String B = ct3.B();
        if (B != null) {
            sb.append(B);
            local.d("FossilFirebaseMessagingService", sb.toString());
            if (ct3.p() != null) {
                Map p = ct3.p();
                wg6.a((Object) p, "remoteMessage.data");
                if (!p.isEmpty()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("FossilFirebaseMessagingService", "Message data payload: " + ct3.p());
                }
            }
            if (ct3.C() != null) {
                ct3.a C = ct3.C();
                if (C != null) {
                    wg6.a((Object) C, "remoteMessage.notification!!");
                    if (!TextUtils.isEmpty(C.b())) {
                        ct3.a C2 = ct3.C();
                        if (C2 != null) {
                            wg6.a((Object) C2, "remoteMessage.notification!!");
                            str = C2.b();
                            if (str == null) {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        str = "";
                    }
                    wg6.a((Object) str, "if (!TextUtils.isEmpty(r\u2026ication!!.title!! else \"\"");
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("Message Notification title: ");
                    sb2.append(str);
                    sb2.append("\n body: ");
                    ct3.a C3 = ct3.C();
                    if (C3 != null) {
                        wg6.a((Object) C3, "remoteMessage.notification!!");
                        String a2 = C3.a();
                        if (a2 != null) {
                            sb2.append(a2);
                            local3.d("FossilFirebaseMessagingService", sb2.toString());
                            dp5 dp5 = this.g;
                            if (dp5 != null) {
                                ct3.a C4 = ct3.C();
                                if (C4 != null) {
                                    wg6.a((Object) C4, "remoteMessage.notification!!");
                                    String a3 = C4.a();
                                    if (a3 != null) {
                                        wg6.a((Object) a3, "remoteMessage.notification!!.body!!");
                                        dp5.a(str, a3);
                                        return;
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.a();
                                throw null;
                            }
                            wg6.d("mInAppNotificationManager");
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.service.fcm.FossilFirebaseMessagingService, android.app.Service] */
    public void onCreate() {
        FossilFirebaseMessagingService.super.onCreate();
        PortfolioApp.get.instance().g().a((FossilFirebaseMessagingService) this);
    }
}
