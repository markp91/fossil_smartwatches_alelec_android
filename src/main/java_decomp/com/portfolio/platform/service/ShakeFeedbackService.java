package com.portfolio.platform.service;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import androidx.core.content.FileProvider;
import com.fossil.af6;
import com.fossil.ak4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.do1;
import com.fossil.ej6;
import com.fossil.ff6;
import com.fossil.fk4;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.qg3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.tj4;
import com.fossil.tp4;
import com.fossil.up4$b$a;
import com.fossil.up4$b$b;
import com.fossil.up4$b$c;
import com.fossil.up4$b$d;
import com.fossil.up4$b$e;
import com.fossil.up4$b$f;
import com.fossil.up4$b$g;
import com.fossil.up4$d$a;
import com.fossil.up4$e$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xx5;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HwLogProvider;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.debug.DebugActivity;
import com.portfolio.platform.ui.debug.LogcatActivity;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShakeFeedbackService {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public WeakReference<Context> a;
    @DexIgnore
    public tp4 b;
    @DexIgnore
    public qg3 c;
    @DexIgnore
    public qg3 d;
    @DexIgnore
    public String e;
    @DexIgnore
    public int f; // = -1;
    @DexIgnore
    public int g; // = -1;
    @DexIgnore
    public /* final */ UserRepository h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements tp4.a {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore
        public b(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0138  */
        /* JADX WARNING: Removed duplicated region for block: B:42:0x013c  */
        public final void a() {
            qg3 a2;
            WeakReference b = this.a.a;
            if (b == null) {
                wg6.a();
                throw null;
            } else if (fk4.a((Context) b.get()) && !this.a.c()) {
                if (this.a.c != null) {
                    qg3 a3 = this.a.c;
                    if (a3 != null) {
                        a3.dismiss();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                WeakReference b2 = this.a.a;
                if (b2 != null) {
                    Object obj = b2.get();
                    if (obj != null) {
                        Object systemService = ((Context) obj).getSystemService("layout_inflater");
                        if (systemService != null) {
                            View inflate = ((LayoutInflater) systemService).inflate(2131558463, (ViewGroup) null);
                            ShakeFeedbackService shakeFeedbackService = this.a;
                            WeakReference b3 = shakeFeedbackService.a;
                            if (b3 != null) {
                                Object obj2 = b3.get();
                                if (obj2 != null) {
                                    shakeFeedbackService.c = new qg3((Context) obj2);
                                    qg3 a4 = this.a.c;
                                    if (a4 != null) {
                                        a4.setContentView(inflate);
                                        View findViewById = inflate.findViewById(2131363227);
                                        if (findViewById != null) {
                                            ((TextView) findViewById).setText("4.3.0");
                                            inflate.findViewById(2131362937).setOnClickListener(new up4$b$a(this));
                                            inflate.findViewById(2131362936).setOnClickListener(new up4$b$b(this));
                                            inflate.findViewById(2131362933).setOnClickListener(new up4$b$c(this));
                                            inflate.findViewById(2131362935).setOnClickListener(new up4$b$d(this));
                                            inflate.findViewById(2131362938).setOnClickListener(new up4$b$e(this));
                                            inflate.findViewById(2131362934).setOnClickListener(new up4$b$f(this));
                                            View findViewById2 = inflate.findViewById(2131362682);
                                            WeakReference b4 = this.a.a;
                                            if (b4 != null) {
                                                if (!(b4.get() instanceof DebugActivity)) {
                                                    WeakReference b5 = this.a.a;
                                                    if (b5 == null) {
                                                        wg6.a();
                                                        throw null;
                                                    } else if (!(b5.get() instanceof LogcatActivity)) {
                                                        wg6.a((Object) findViewById2, "llOpenDebug");
                                                        findViewById2.setVisibility(0);
                                                        inflate.findViewById(2131362765).setOnClickListener(new up4$b$g(this));
                                                        a2 = this.a.c;
                                                        if (a2 == null) {
                                                            a2.show();
                                                            return;
                                                        } else {
                                                            wg6.a();
                                                            throw null;
                                                        }
                                                    }
                                                }
                                                wg6.a((Object) findViewById2, "llOpenDebug");
                                                findViewById2.setVisibility(8);
                                                inflate.findViewById(2131362765).setOnClickListener(new up4$b$g(this));
                                                a2 = this.a.c;
                                                if (a2 == null) {
                                                }
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            throw new rc6("null cannot be cast to non-null type android.widget.TextView");
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            throw new rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.ShakeFeedbackService$sendLog$2", f = "ShakeFeedbackService.kt", l = {82}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Bitmap $bitmap;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ShakeFeedbackService shakeFeedbackService, Bitmap bitmap, xe6 xe6) {
            super(2, xe6);
            this.this$0 = shakeFeedbackService;
            this.$bitmap = bitmap;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$bitmap, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ShakeFeedbackService shakeFeedbackService = this.this$0;
                Bitmap bitmap = this.$bitmap;
                this.L$0 = il6;
                this.label = 1;
                if (shakeFeedbackService.a(bitmap, (xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore
        public d(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.g = 0;
            xx5.a aVar = xx5.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new rc6("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            wg6.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new up4$d$a(this, createBitmap, (xe6) null), 3, (Object) null);
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ShakeFeedbackService a;

        @DexIgnore
        public e(ShakeFeedbackService shakeFeedbackService) {
            this.a = shakeFeedbackService;
        }

        @DexIgnore
        public final void onClick(View view) {
            xx5.a aVar = xx5.a;
            WeakReference b = this.a.a;
            if (b != null) {
                Object obj = b.get();
                if (obj == null) {
                    throw new rc6("null cannot be cast to non-null type android.app.Activity");
                } else if (aVar.c((Activity) obj, 123)) {
                    WeakReference b2 = this.a.a;
                    if (b2 != null) {
                        Object obj2 = b2.get();
                        if (obj2 != null) {
                            Window window = ((Activity) obj2).getWindow();
                            wg6.a((Object) window, "(contextWeakReference!!.get() as Activity).window");
                            View decorView = window.getDecorView();
                            wg6.a((Object) decorView, "(contextWeakReference!!.\u2026ctivity).window.decorView");
                            View rootView = decorView.getRootView();
                            wg6.a((Object) rootView, "v1");
                            rootView.setDrawingCacheEnabled(true);
                            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
                            wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
                            rootView.setDrawingCacheEnabled(false);
                            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new up4$e$a(this, createBitmap, (xe6) null), 3, (Object) null);
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type android.app.Activity");
                    }
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ShakeFeedbackService.class.getSimpleName();
        wg6.a((Object) simpleName, "ShakeFeedbackService::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public ShakeFeedbackService(UserRepository userRepository) {
        String str;
        wg6.b(userRepository, "mUserRepository");
        this.h = userRepository;
        if (b()) {
            str = Environment.getExternalStorageDirectory().toString();
        } else {
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            str = applicationContext.getFilesDir().toString();
        }
        this.e = str;
        this.e = wg6.a(this.e, (Object) "/com.fossil.wearables.fossil/");
    }

    @DexIgnore
    public final boolean c() {
        qg3 qg3;
        qg3 qg32 = this.c;
        if (qg32 != null) {
            if (qg32 == null) {
                wg6.a();
                throw null;
            } else if (qg32.isShowing() && (qg3 = this.d) != null) {
                if (qg3 == null) {
                    wg6.a();
                    throw null;
                } else if (qg3.isShowing()) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        WeakReference<Context> weakReference = this.a;
        if (weakReference == null) {
            wg6.a();
            throw null;
        } else if (fk4.a((Context) weakReference.get()) && !c()) {
            qg3 qg3 = this.d;
            if (qg3 != null) {
                if (qg3 != null) {
                    qg3.dismiss();
                } else {
                    wg6.a();
                    throw null;
                }
            }
            WeakReference<Context> weakReference2 = this.a;
            if (weakReference2 != null) {
                Object obj = weakReference2.get();
                if (obj != null) {
                    Object systemService = ((Context) obj).getSystemService("layout_inflater");
                    if (systemService != null) {
                        View inflate = ((LayoutInflater) systemService).inflate(2131558464, (ViewGroup) null);
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Object obj2 = weakReference3.get();
                            if (obj2 != null) {
                                this.d = new qg3((Context) obj2);
                                qg3 qg32 = this.d;
                                if (qg32 != null) {
                                    qg32.setContentView(inflate);
                                    View findViewById = inflate.findViewById(2131363227);
                                    if (findViewById != null) {
                                        ((TextView) findViewById).setText("4.3.0");
                                        inflate.findViewById(2131361940).setOnClickListener(new d(this));
                                        inflate.findViewById(2131361942).setOnClickListener(new e(this));
                                        qg3 qg33 = this.d;
                                        if (qg33 != null) {
                                            qg33.show();
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        throw new rc6("null cannot be cast to non-null type android.widget.TextView");
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type android.view.LayoutInflater");
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final synchronized void e() {
        tp4 tp4 = this.b;
        if (tp4 != null) {
            tp4.a();
        }
        if (this.c != null) {
            qg3 qg3 = this.c;
            if (qg3 != null) {
                qg3.dismiss();
                this.c = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        if (this.d != null) {
            qg3 qg32 = this.d;
            if (qg32 != null) {
                qg32.dismiss();
                this.d = null;
            } else {
                wg6.a();
                throw null;
            }
        }
        this.a = null;
    }

    @DexIgnore
    public final boolean b() {
        return wg6.a((Object) "mounted", (Object) Environment.getExternalStorageState());
    }

    @DexIgnore
    public final Object a(Context context, xe6<? super cd6> xe6) {
        if (context != null) {
            Window window = ((Activity) context).getWindow();
            wg6.a((Object) window, "(context as Activity).window");
            View decorView = window.getDecorView();
            wg6.a((Object) decorView, "(context as Activity).window.decorView");
            View rootView = decorView.getRootView();
            wg6.a((Object) rootView, "v1");
            rootView.setDrawingCacheEnabled(true);
            Bitmap createBitmap = Bitmap.createBitmap(rootView.getDrawingCache());
            wg6.a((Object) createBitmap, "Bitmap.createBitmap(v1.drawingCache)");
            Object a2 = gk6.a(zl6.b(), new c(this, createBitmap, (xe6) null), xe6);
            if (a2 == ff6.a()) {
                return a2;
            }
            return cd6.a;
        }
        throw new rc6("null cannot be cast to non-null type android.app.Activity");
    }

    @DexIgnore
    public final synchronized void a(Context context) {
        wg6.b(context, Constants.ACTIVITY);
        this.a = new WeakReference<>(context);
        Object systemService = context.getSystemService("sensor");
        if (systemService != null) {
            SensorManager sensorManager = (SensorManager) systemService;
            this.b = new tp4(new b(this));
            tp4 tp4 = this.b;
            if (tp4 != null) {
                tp4.a(sensorManager);
            }
        } else {
            throw new rc6("null cannot be cast to non-null type android.hardware.SensorManager");
        }
    }

    @DexIgnore
    public final /* synthetic */ Object a(xe6<? super cd6> xe6) {
        WeakReference<Context> weakReference = this.a;
        if (weakReference != null) {
            HwLogProvider instance = HwLogProvider.getInstance((Context) weakReference.get());
            wg6.a((Object) instance, "HwLogProvider.getInstanc\u2026extWeakReference!!.get())");
            String a2 = new Gson().a(instance.getAllHardwareLogs());
            String str = this.e;
            if (str != null) {
                try {
                    new File(str).mkdir();
                    String str2 = this.e;
                    nh6 nh6 = nh6.a;
                    Object[] objArr = {hf6.a(System.currentTimeMillis())};
                    String format = String.format("hwlog_%s.txt", Arrays.copyOf(objArr, objArr.length));
                    wg6.a((Object) format, "java.lang.String.format(format, *args)");
                    File file = new File(str2, format);
                    if (file.exists()) {
                        file.delete();
                    }
                    file.createNewFile();
                    FileOutputStream fileOutputStream = new FileOutputStream(file);
                    wg6.a((Object) a2, "jsonLog");
                    Charset charset = ej6.a;
                    if (a2 != null) {
                        byte[] bytes = a2.getBytes(charset);
                        wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        fileOutputStream.write(bytes);
                        fileOutputStream.flush();
                        fileOutputStream.close();
                        ak4 ak4 = ak4.a;
                        WeakReference<Context> weakReference2 = this.a;
                        if (weakReference2 != null) {
                            Object obj = weakReference2.get();
                            if (obj != null) {
                                wg6.a(obj, "contextWeakReference!!.get()!!");
                                Context context = (Context) obj;
                                String str3 = this.e;
                                if (str3 == null) {
                                    str3 = "";
                                }
                                String a3 = ak4.a(context, str3);
                                ArrayList arrayList = new ArrayList();
                                if (a3 != null) {
                                    arrayList.add(a3);
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String str4 = i;
                                    local.d(str4, "HwLogFile=" + file.getAbsolutePath());
                                    ArrayList arrayList2 = new ArrayList();
                                    arrayList2.add(file);
                                    a((String) null, arrayList, arrayList2);
                                    WeakReference<Context> weakReference3 = this.a;
                                    if (weakReference3 != null) {
                                        HwLogProvider.getInstance((Context) weakReference3.get()).setHardwareLogRead();
                                        return cd6.a;
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.a();
                                throw null;
                            }
                            wg6.a();
                            throw null;
                        }
                        wg6.a();
                        throw null;
                    }
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                } catch (Exception e2) {
                    e2.printStackTrace();
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str5 = i;
                    local2.e(str5, ".sendHardwareLog - ex=" + e2);
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a(String str, List<String> list, List<? extends File> list2) {
        Uri a2;
        String str2 = str;
        Intent intent = new Intent("android.intent.action.SEND_MULTIPLE");
        intent.setType("vnd.android.cursor.dir/email");
        if (!PortfolioApp.get.e()) {
            intent.putExtra("android.intent.extra.EMAIL", new String[]{"help@misfit.com"});
        } else if (this.g != -1) {
            intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
        } else {
            int i2 = this.f;
            if (i2 == 0) {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"fossiluat@fossil.com", "bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
            } else if (i2 == 2) {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "dungdna@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com", "fossiluat@fossil.com"});
            } else {
                intent.putExtra("android.intent.extra.EMAIL", new String[]{"bugs@fossil.com", "sw-qa@fossil.com", "minh@fossil.com", "dungdna@fossil.com", "sw-q-android@fossil.com", "sw-diana@fossil.com", "diana-bugs-sm@fossil.com", "diana-bugs-sm@fossil.com", "sw-sa@fossil.com"});
            }
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        long j = 0;
        if ((!list.isEmpty()) && PortfolioApp.get.e()) {
            for (String next : list) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local.e(str3, ".sendFeedbackEmail - databaseFile=" + next);
                if (!(next == null || (a2 = a(next)) == null)) {
                    arrayList.add(a2);
                    arrayList2.add(next);
                    String path = a2.getPath();
                    if (path != null) {
                        j += new File(path).length();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
        wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
        File filesDir = applicationContext.getFilesDir();
        wg6.a((Object) filesDir, "PortfolioApp.instance.applicationContext.filesDir");
        sb.append(filesDir.getAbsolutePath());
        sb.append(File.separator);
        sb.append(do1.b.c());
        File a3 = a("NewSDKLog", new File(sb.toString()));
        if (a3 != null) {
            String absolutePath = a3.getAbsolutePath();
            wg6.a((Object) absolutePath, "newSdkZipFile.absolutePath");
            Uri a4 = a(absolutePath);
            if (a4 != null) {
                arrayList.add(a4);
                arrayList2.add(a3.getAbsolutePath());
                j += a3.length();
            }
        }
        if (!TextUtils.isEmpty(str)) {
            if (str2 != null) {
                Uri a5 = a(str);
                if (a5 != null) {
                    arrayList.add(a5);
                    arrayList2.add(str2);
                    String path2 = a5.getPath();
                    if (path2 != null) {
                        j += new File(path2).length();
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        for (File file : list2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = i;
            local2.d(str4, "sendFeedbackEmail - logFile=" + file.getName() + ", length=" + file.length());
            String absolutePath2 = file.getAbsolutePath();
            wg6.a((Object) absolutePath2, "log.absolutePath");
            Uri a6 = a(absolutePath2);
            if (a6 != null) {
                j += file.length();
                arrayList.add(a6);
                arrayList2.add(file.getAbsolutePath());
            }
        }
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str5 = i;
        local3.d(str5, ".sendFeedbackEmail - dataFeedbackSize=" + j);
        if (j > 9437184) {
            File a7 = a((ArrayList<String>) arrayList2);
            arrayList.clear();
            String absolutePath3 = a7.getAbsolutePath();
            wg6.a((Object) absolutePath3, "file.absolutePath");
            Uri a8 = a(absolutePath3);
            if (a8 != null) {
                arrayList.add(a8);
            }
        }
        intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
        int i3 = this.g;
        if (i3 == -1) {
            String str6 = "Staging] - ";
            if (this.f == 0) {
                StringBuilder sb2 = new StringBuilder();
                sb2.append("[BETA - ");
                sb2.append(PortfolioApp.get.instance().i());
                sb2.append(" ");
                if (!PortfolioApp.get.e()) {
                    str6 = "] - ";
                }
                sb2.append(str6);
                sb2.append("[Android - Feedback]");
                intent.putExtra("android.intent.extra.SUBJECT", sb2.toString());
            } else {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("[");
                sb3.append(PortfolioApp.get.instance().i());
                sb3.append(" ");
                if (!PortfolioApp.get.e()) {
                    str6 = "] - ";
                }
                sb3.append(str6);
                sb3.append("[Android Feedback] -");
                nh6 nh6 = nh6.a;
                Object[] objArr = {Long.valueOf(System.currentTimeMillis() / ((long) 1000)), bk4.c(new Date())};
                String format = String.format(" on %s (%s)", Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                sb3.append(format);
                intent.putExtra("android.intent.extra.SUBJECT", sb3.toString());
            }
        } else if (i3 == 0) {
            intent.putExtra("android.intent.extra.SUBJECT", "GATT CONNECTION");
        } else if (i3 == 1) {
            intent.putExtra("android.intent.extra.SUBJECT", "HID CONNECTION");
        }
        try {
            StringBuilder sb4 = new StringBuilder();
            sb4.append("[");
            sb4.append(PortfolioApp.get.instance().i());
            sb4.append(" ");
            sb4.append(PortfolioApp.get.e() ? "Staging" : "");
            sb4.append("]\n");
            tj4 b2 = tj4.f.b();
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    wg6.a(obj, "contextWeakReference!!.get()!!");
                    sb4.append(b2.a((Context) obj, this.f, this.g));
                    intent.putExtra("android.intent.extra.TEXT", sb4.toString());
                    WeakReference<Context> weakReference2 = this.a;
                    if (weakReference2 == null) {
                        return;
                    }
                    if (weakReference2 == null) {
                        wg6.a();
                        throw null;
                    } else if (weakReference2.get() != null) {
                        WeakReference<Context> weakReference3 = this.a;
                        if (weakReference3 != null) {
                            Object obj2 = weakReference3.get();
                            if (obj2 != null) {
                                Context context = (Context) obj2;
                                WeakReference<Context> weakReference4 = this.a;
                                if (weakReference4 != null) {
                                    Object obj3 = weakReference4.get();
                                    if (obj3 != null) {
                                        context.startActivity(Intent.createChooser(intent, ((Context) obj3).getString(2131887261)));
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str7 = i;
            local4.d(str7, ".sendFeedbackEmail - ex=" + e2);
        }
    }

    @DexIgnore
    public final File a(String str, File file) {
        File[] listFiles;
        File file2 = null;
        if (file == null || (listFiles = file.listFiles()) == null) {
            return null;
        }
        if (!(!(listFiles.length == 0))) {
            return null;
        }
        FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - files.length=" + listFiles.length);
        try {
            File file3 = new File(Environment.getExternalStorageDirectory(), str + ".zip");
            try {
                ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file3));
                for (File file4 : listFiles) {
                    byte[] bArr = new byte[1024];
                    FileInputStream fileInputStream = new FileInputStream(file4);
                    wg6.a((Object) file4, "file");
                    zipOutputStream.putNextEntry(new ZipEntry(file4.getName()));
                    hh6 hh6 = new hh6();
                    while (true) {
                        hh6.element = fileInputStream.read(bArr);
                        if (!(hh6.element > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, hh6.element);
                    }
                    fileInputStream.close();
                }
                zipOutputStream.close();
                return file3;
            } catch (IOException e2) {
                e = e2;
                file2 = file3;
                FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
                return file2;
            }
        } catch (IOException e3) {
            e = e3;
            FLogger.INSTANCE.getLocal().e(i, ".sendFeedbackEmail - read sdk log ex=" + e);
            return file2;
        }
    }

    @DexIgnore
    public final List<File> a() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(FLogger.INSTANCE.getLocal().exportAppLogs());
        arrayList2.addAll(FLogger.INSTANCE.getRemote().exportAppLogs());
        arrayList2.addAll(MicroAppEventLogger.exportLogFiles());
        Iterator it = arrayList2.iterator();
        while (it.hasNext()) {
            File file = (File) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = i;
            StringBuilder sb = new StringBuilder();
            sb.append("Exporting ");
            wg6.a((Object) file, "file");
            sb.append(file.getName());
            sb.append(", size=");
            sb.append(file.length());
            local.d(str, sb.toString());
            try {
                File file2 = new File(this.e, file.getName());
                if (file2.exists()) {
                    file2.delete();
                } else {
                    file2.createNewFile();
                }
                FileChannel channel = new FileInputStream(file).getChannel();
                wg6.a((Object) channel, "FileInputStream(file).channel");
                FileChannel channel2 = new FileOutputStream(file2).getChannel();
                wg6.a((Object) channel2, "FileOutputStream(exportFile).channel");
                channel2.transferFrom(channel, 0, channel.size());
                channel.close();
                channel2.close();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = i;
                local2.d(str2, "Done exporting " + file2.getName() + ", size=" + file2.length());
                arrayList.add(file2);
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local3.e(str3, "Error while exporting log files - e=" + e2);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final /* synthetic */ Object a(Bitmap bitmap, xe6<? super cd6> xe6) {
        String str;
        String str2 = this.e;
        if (str2 != null) {
            try {
                new File(str2).mkdir();
                File file = new File(this.e, "screenshot.jpg");
                if (file.exists()) {
                    file.delete();
                }
                file.createNewFile();
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fileOutputStream);
                fileOutputStream.flush();
                fileOutputStream.close();
            } catch (IOException e2) {
                e2.printStackTrace();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = i;
                local.e(str3, ".ScreenShootTask - doInBackground ex=" + e2);
            }
            nh6 nh6 = nh6.a;
            Object[] objArr = {hf6.a(System.currentTimeMillis() / ((long) 1000))};
            String format = String.format("logCat_%s.txt", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            File file2 = new File(this.e, format);
            try {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("logcat -v time -d -f " + file2.getAbsolutePath());
            } catch (IOException e3) {
                e3.printStackTrace();
            }
            MFUser currentUser = this.h.getCurrentUser();
            String str4 = "";
            if (currentUser == null || (str = currentUser.getUserId()) == null) {
                str = str4;
            }
            ak4 ak4 = ak4.a;
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    wg6.a(obj, "contextWeakReference!!.get()!!");
                    Context context = (Context) obj;
                    String str5 = this.e;
                    if (str5 != null) {
                        str4 = str5;
                    }
                    List<String> a2 = ak4.a(context, str, str4);
                    StringBuilder sb = new StringBuilder();
                    String str6 = this.e;
                    if (str6 != null) {
                        sb.append(str6);
                        sb.append("screenshot.jpg");
                        String sb2 = sb.toString();
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(file2);
                        List<File> a3 = a();
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str7 = i;
                        local2.d(str7, "Number of app logs=" + a3.size());
                        arrayList.addAll(a3);
                        a(sb2, a2, arrayList);
                        return cd6.a;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final File a(ArrayList<String> arrayList) {
        File file = new File(Environment.getExternalStorageDirectory(), "DebugDataLog.zip");
        try {
            ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(file));
            Iterator<String> it = arrayList.iterator();
            while (it.hasNext()) {
                byte[] bArr = new byte[1024];
                File file2 = new File(it.next());
                FileInputStream fileInputStream = new FileInputStream(file2);
                try {
                    zipOutputStream.putNextEntry(new ZipEntry(file2.getName()));
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = i;
                    local.d(str, "zipDataFile - Processing file=" + file2.getName() + ", length=" + file2.length());
                    hh6 hh6 = new hh6();
                    while (true) {
                        hh6.element = fileInputStream.read(bArr);
                        if (!(hh6.element > 0)) {
                            break;
                        }
                        zipOutputStream.write(bArr, 0, hh6.element);
                    }
                    fileInputStream.close();
                } catch (ZipException e2) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = i;
                    local2.e(str2, ".zipDataFile - ZipException=" + e2);
                }
            }
            zipOutputStream.close();
        } catch (IOException e3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = i;
            local3.e(str3, ".zipDataFile - ex=" + e3);
        }
        return file;
    }

    @DexIgnore
    public final Uri a(String str) {
        if (Build.VERSION.SDK_INT < 24) {
            return Uri.parse("file://" + str);
        } else if (this.a == null || TextUtils.isEmpty(str)) {
            return null;
        } else {
            WeakReference<Context> weakReference = this.a;
            if (weakReference != null) {
                Object obj = weakReference.get();
                if (obj != null) {
                    return FileProvider.getUriForFile((Context) obj, "com.fossil.wearables.fossil.provider", new File(str));
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }
}
