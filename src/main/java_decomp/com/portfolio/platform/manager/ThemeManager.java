package com.portfolio.platform.manager;

import android.graphics.Typeface;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.t4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeManager {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static ThemeManager k;
    @DexIgnore
    public static /* final */ b l; // = new b((qg6) null);
    @DexIgnore
    public /* final */ String a; // = "theme/fonts/soleil/";
    @DexIgnore
    public /* final */ String b; // = "theme/fonts/ginger/";
    @DexIgnore
    public /* final */ String c; // = "theme/fonts/sf/";
    @DexIgnore
    public /* final */ String d; // = ".otf";
    @DexIgnore
    public /* final */ ArrayList<Style> e; // = new ArrayList<>();
    @DexIgnore
    public /* final */ t4<String, String> f; // = new t4<>(10);
    @DexIgnore
    public /* final */ t4<String, Typeface> g; // = new t4<>(10);
    @DexIgnore
    public /* final */ ArrayList<String> h; // = qd6.a((T[]) new String[]{this.a, this.b, this.c});
    @DexIgnore
    public ThemeRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.ThemeManager$1", f = "ThemeManager.kt", l = {33}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ThemeManager themeManager, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themeManager;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThemeManager themeManager = this.this$0;
                this.L$0 = il6;
                this.label = 1;
                if (themeManager.a((xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final synchronized ThemeManager a() {
            ThemeManager b;
            if (ThemeManager.k == null) {
                ThemeManager.k = new ThemeManager();
            }
            b = ThemeManager.k;
            if (b == null) {
                wg6.a();
                throw null;
            }
            return b;
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.ThemeManager$addNewTheme$1", f = "ThemeManager.kt", l = {129, 133}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ThemeManager themeManager, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themeManager;
            this.$id = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$id, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                ThemeRepository a2 = this.this$0.a();
                this.L$0 = il6;
                this.label = 1;
                obj = a2.getThemeById("default", this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                Theme theme = (Theme) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Theme theme2 = (Theme) obj;
            if (theme2 != null) {
                theme2.setId(this.$id);
                theme2.setName("New theme");
                theme2.setType("modified");
                ThemeRepository a3 = this.this$0.a();
                this.L$0 = il6;
                this.L$1 = theme2;
                this.label = 2;
                if (a3.upsertUserTheme(theme2, this) == a) {
                    return a;
                }
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.ThemeManager", f = "ThemeManager.kt", l = {41}, m = "reloadStyleCache")
    public static final class d extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ ThemeManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ThemeManager themeManager, xe6 xe6) {
            super(xe6);
            this.this$0 = themeManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    /*
    static {
        String simpleName = ThemeManager.class.getSimpleName();
        wg6.a((Object) simpleName, "ThemeManager::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public ThemeManager() {
        PortfolioApp.get.instance().g().a(this);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new a(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Typeface c(String str) {
        Style style;
        wg6.b(str, "themeStyle");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                style = null;
                break;
            }
            style = it.next();
            if (wg6.a((Object) style.getKey(), (Object) str)) {
                break;
            }
        }
        Style style2 = (Style) style;
        String value = style2 != null ? style2.getValue() : null;
        if (TextUtils.isEmpty(value)) {
            return null;
        }
        t4<String, Typeface> t4Var = this.g;
        if (value != null) {
            Typeface typeface = (Typeface) t4Var.b(value);
            if (typeface == null) {
                for (String str2 : this.h) {
                    try {
                        Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.get.instance().getAssets(), str2 + value + this.d);
                        t4<String, Typeface> t4Var2 = this.g;
                        if (createFromAsset != null) {
                            t4Var2.a(value, createFromAsset);
                            return createFromAsset;
                        }
                        wg6.a();
                        throw null;
                    } catch (Exception unused) {
                        FLogger.INSTANCE.getLocal().d(j, "font file is not exists");
                    }
                }
            }
            return typeface;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final ThemeRepository a() {
        ThemeRepository themeRepository = this.i;
        if (themeRepository != null) {
            return themeRepository;
        }
        wg6.d("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final String b(String str) {
        T t;
        wg6.b(str, "themeStyle");
        String str2 = (String) this.f.b(str);
        if (str2 == null) {
            Iterator<T> it = this.e.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) ((Style) t).getKey(), (Object) str)) {
                    break;
                }
            }
            Style style = (Style) t;
            str2 = style != null ? style.getValue() : null;
            if (!TextUtils.isEmpty(str2)) {
                t4<String, String> t4Var = this.f;
                if (str2 != null) {
                    t4Var.a(str, str2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return str2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(xe6<? super cd6> xe6) {
        d dVar;
        int i2;
        ArrayList<Style> arrayList;
        if (xe6 instanceof d) {
            dVar = (d) xe6;
            int i3 = dVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dVar.label = i3 - Integer.MIN_VALUE;
                Object obj = dVar.result;
                Object a2 = ff6.a();
                i2 = dVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    this.f.a();
                    this.g.a();
                    this.e.clear();
                    ArrayList<Style> arrayList2 = this.e;
                    ThemeRepository themeRepository = this.i;
                    if (themeRepository != null) {
                        dVar.L$0 = this;
                        dVar.L$1 = arrayList2;
                        dVar.label = 1;
                        Object currentTheme = themeRepository.getCurrentTheme(dVar);
                        if (currentTheme == a2) {
                            return a2;
                        }
                        arrayList = arrayList2;
                        obj = currentTheme;
                    } else {
                        wg6.d("mThemeRepository");
                        throw null;
                    }
                } else if (i2 == 1) {
                    arrayList = (ArrayList) dVar.L$1;
                    ThemeManager themeManager = (ThemeManager) dVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                Theme theme = (Theme) obj;
                if (theme == null || (r5 = theme.getStyles()) == null) {
                    ArrayList<Style> arrayList3 = new ArrayList<>();
                }
                arrayList.addAll(arrayList3);
                return cd6.a;
            }
        }
        dVar = new d(this, xe6);
        Object obj2 = dVar.result;
        Object a22 = ff6.a();
        i2 = dVar.label;
        if (i2 != 0) {
        }
        Theme theme2 = (Theme) obj2;
        ArrayList<Style> arrayList32 = new ArrayList<>();
        arrayList.addAll(arrayList32);
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Typeface b(String str, List<Style> list) {
        Style style;
        wg6.b(str, "themeStyle");
        wg6.b(list, "predefineStyles");
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                style = null;
                break;
            }
            style = it.next();
            if (wg6.a((Object) style.getKey(), (Object) str)) {
                break;
            }
        }
        Style style2 = (Style) style;
        String value = style2 != null ? style2.getValue() : null;
        for (String str2 : this.h) {
            try {
                Typeface createFromAsset = Typeface.createFromAsset(PortfolioApp.get.instance().getAssets(), str2 + value + this.d);
                t4<String, Typeface> t4Var = this.g;
                if (createFromAsset != null) {
                    t4Var.a(str, createFromAsset);
                    return createFromAsset;
                }
                wg6.a();
                throw null;
            } catch (Exception unused) {
            }
        }
        return null;
    }

    @DexIgnore
    public final String a(String str, List<Style> list) {
        T t;
        wg6.b(str, "themeStyle");
        wg6.b(list, "predefineStyles");
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((Style) t).getKey(), (Object) str)) {
                break;
            }
        }
        Style style = (Style) t;
        if (style != null) {
            return style.getValue();
        }
        return null;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = j;
        local.d(str2, "addNewTheme id=" + str);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new c(this, str, (xe6) null), 3, (Object) null);
    }
}
