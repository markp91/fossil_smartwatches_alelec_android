package com.portfolio.platform.manager;

import android.content.Context;
import android.location.Location;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.en4$f$a;
import com.fossil.en4$f$b;
import com.fossil.en4$f$c;
import com.fossil.en4$h$a;
import com.fossil.en4$i$a;
import com.fossil.en4$j$a;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.fn4;
import com.fossil.fu3;
import com.fossil.gk6;
import com.fossil.gp4;
import com.fossil.gy5;
import com.fossil.hf6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rl6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.vi4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xp6;
import com.fossil.zh4;
import com.fossil.zj4;
import com.fossil.zl6;
import com.fossil.zo4;
import com.fossil.zp6;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.ChanceOfRainComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherInfoWatchAppResponse;
import com.misfit.frameworks.buttonservice.model.watchapp.response.weather.WeatherWatchAppInfo;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.model.CustomizeRealData;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.microapp.weather.AddressOfWeather;
import com.portfolio.platform.data.model.microapp.weather.Temperature;
import com.portfolio.platform.data.model.microapp.weather.Weather;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.interfaces.CustomizeRealDataRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherManager {
    @DexIgnore
    public static /* final */ String l;
    @DexIgnore
    public static WeatherManager m;
    @DexIgnore
    public static /* final */ a n; // = new a((qg6) null);
    @DexIgnore
    public PortfolioApp a;
    @DexIgnore
    public ApiServiceV2 b;
    @DexIgnore
    public LocationSource c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public CustomizeRealDataRepository e;
    @DexIgnore
    public DianaPresetRepository f;
    @DexIgnore
    public GoogleApiService g;
    @DexIgnore
    public WeatherWatchAppSetting h;
    @DexIgnore
    public String i;
    @DexIgnore
    public Weather j;
    @DexIgnore
    public xp6 k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(WeatherManager weatherManager) {
            WeatherManager.m = weatherManager;
        }

        @DexIgnore
        public final WeatherManager b() {
            return WeatherManager.m;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final synchronized WeatherManager a() {
            WeatherManager b;
            if (WeatherManager.n.b() == null) {
                WeatherManager.n.a(new WeatherManager((qg6) null));
            }
            b = WeatherManager.n.b();
            if (b == null) {
                wg6.a();
                throw null;
            }
            return b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {257}, m = "getAddressBaseOnLocation")
    public static final class b extends jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(WeatherManager weatherManager, xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a(0.0d, 0.0d, (xe6<? super String>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$getAddressBaseOnLocation$response$1", f = "WeatherManager.kt", l = {257}, m = "invokeSuspend")
    public static final class c extends sf6 implements hg6<xe6<? super rx6<ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $type;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(WeatherManager weatherManager, double d, double d2, String str, xe6 xe6) {
            super(1, xe6);
            this.this$0 = weatherManager;
            this.$lat = d;
            this.$lng = d2;
            this.$type = str;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new c(this.this$0, this.$lat, this.$lng, this.$type, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((c) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                GoogleApiService d = this.this$0.d();
                StringBuilder sb = new StringBuilder();
                sb.append(this.$lat);
                sb.append(',');
                sb.append(this.$lng);
                String sb2 = sb.toString();
                String str = this.$type;
                this.label = 1;
                obj = d.getAddressWithType(sb2, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {285}, m = "getWeather")
    public static final class d extends jf6 {
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WeatherManager weatherManager, xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a(0.0d, 0.0d, (String) null, (xe6<? super lc6<Weather, Boolean>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeather$repoResponse$1", f = "WeatherManager.kt", l = {285}, m = "invokeSuspend")
    public static final class e extends sf6 implements hg6<xe6<? super rx6<ku3>>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ double $lat;
        @DexIgnore
        public /* final */ /* synthetic */ double $lng;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(WeatherManager weatherManager, double d, double d2, String str, xe6 xe6) {
            super(1, xe6);
            this.this$0 = weatherManager;
            this.$lat = d;
            this.$lng = d2;
            this.$tempUnit = str;
        }

        @DexIgnore
        public final xe6<cd6> create(xe6<?> xe6) {
            wg6.b(xe6, "completion");
            return new e(this.this$0, this.$lat, this.$lng, this.$tempUnit, xe6);
        }

        @DexIgnore
        public final Object invoke(Object obj) {
            return ((e) create((xe6) obj)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                ApiServiceV2 a2 = this.this$0.a();
                String valueOf = String.valueOf(this.$lat);
                String valueOf2 = String.valueOf(this.$lng);
                String str = this.$tempUnit;
                this.label = 1;
                obj = a2.getWeather(valueOf, valueOf2, str, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return obj;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ Location $currentLocation;
        @DexIgnore
        public /* final */ /* synthetic */ fh6 $isFromCaches;
        @DexIgnore
        public /* final */ /* synthetic */ String $tempUnit$inlined;
        @DexIgnore
        public double D$0;
        @DexIgnore
        public double D$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(Location location, fh6 fh6, xe6 xe6, WeatherManager weatherManager, xe6 xe62, String str) {
            super(2, xe6);
            this.$currentLocation = location;
            this.$isFromCaches = fh6;
            this.this$0 = weatherManager;
            this.$continuation$inlined = xe62;
            this.$tempUnit$inlined = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.$currentLocation, this.$isFromCaches, xe6, this.this$0, this.$continuation$inlined, this.$tempUnit$inlined);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v26, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v5, resolved type: java.lang.String} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x01f2  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x01fd  */
        public final Object invokeSuspend(Object obj) {
            String str;
            Weather a;
            Weather a2;
            double d;
            double d2;
            Object obj2;
            double d3;
            double d4;
            Object obj3;
            double d5;
            il6 il6;
            List<AddressOfWeather> list;
            String str2;
            double d6;
            Object obj4;
            Object a3 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                Location location = this.$currentLocation;
                if (location != null) {
                    d5 = location.getLatitude();
                    double longitude = this.$currentLocation.getLongitude();
                    List<AddressOfWeather> weatherAddressList = this.this$0.e().getWeatherAddressList();
                    dl6 a4 = zl6.a();
                    en4$f$a en4_f_a = r0;
                    list = weatherAddressList;
                    en4$f$a en4_f_a2 = new en4$f$a(this, d5, longitude, (xe6) null);
                    this.L$0 = il6;
                    this.D$0 = d5;
                    double d7 = longitude;
                    this.D$1 = d7;
                    this.L$1 = null;
                    this.L$2 = list;
                    this.label = 1;
                    obj4 = gk6.a(a4, en4_f_a, this);
                    if (obj4 == a3) {
                        return a3;
                    }
                    str2 = null;
                    d6 = d7;
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                double d8 = this.D$1;
                double d9 = this.D$0;
                nc6.a(obj);
                list = (List) this.L$2;
                d5 = d9;
                il6 = (il6) this.L$0;
                str2 = this.L$1;
                d6 = d8;
                obj4 = obj;
            } else if (i == 2) {
                lc6 lc6 = (lc6) this.L$3;
                List list2 = (List) this.L$2;
                String str3 = (String) this.L$1;
                double d10 = this.D$1;
                d4 = this.D$0;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                d3 = d10;
                obj3 = obj;
                str = (String) obj3;
                this.this$0.e().saveWeatherAddress(new AddressOfWeather(d4, d3, str));
                FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), currentAddress = " + str);
                a = this.this$0.j;
                if (a != null) {
                }
                a2 = this.this$0.j;
                if (a2 != null) {
                }
                return cd6.a;
            } else if (i == 3) {
                lc6 lc62 = (lc6) this.L$3;
                List list3 = (List) this.L$2;
                String str4 = (String) this.L$1;
                double d11 = this.D$1;
                d2 = this.D$0;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                d = d11;
                obj2 = obj;
                str = (String) obj2;
                this.this$0.e().saveWeatherAddress(new AddressOfWeather(d2, d, str));
                FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), currentAddress = " + str);
                a = this.this$0.j;
                if (a != null) {
                    a.setAddress(str);
                }
                a2 = this.this$0.j;
                if (a2 != null) {
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "Calendar.getInstance()");
                    a2.setUpdatedAt(instance.getTimeInMillis());
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc63 = (lc6) obj4;
            this.this$0.j = (Weather) lc63.getFirst();
            this.$isFromCaches.element = ((Boolean) lc63.getSecond()).booleanValue();
            if (!list.isEmpty()) {
                Iterator<AddressOfWeather> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    AddressOfWeather next = it.next();
                    if (this.this$0.a(next, d5, d6)) {
                        FLogger.INSTANCE.getLocal().d(WeatherManager.l, "in acceptable range, saved address=" + next);
                        str2 = next.getAddress();
                        break;
                    }
                }
                if (str2 == null) {
                    FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), there isn't any saved address near from current lat lng");
                    dl6 a5 = zl6.a();
                    en4$f$b en4_f_b = r0;
                    en4$f$b en4_f_b2 = new en4$f$b(this, d5, d6, (xe6) null);
                    this.L$0 = il6;
                    this.D$0 = d5;
                    this.D$1 = d6;
                    this.L$1 = str2;
                    this.L$2 = list;
                    this.L$3 = lc63;
                    this.label = 2;
                    obj3 = gk6.a(a5, en4_f_b, this);
                    Object obj5 = a3;
                    if (obj3 == obj5) {
                        return obj5;
                    }
                    d3 = d6;
                    d4 = d5;
                    str = (String) obj3;
                    this.this$0.e().saveWeatherAddress(new AddressOfWeather(d4, d3, str));
                    FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), currentAddress = " + str);
                    a = this.this$0.j;
                    if (a != null) {
                    }
                    a2 = this.this$0.j;
                    if (a2 != null) {
                    }
                    return cd6.a;
                }
                str = str2;
                FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), currentAddress = " + str);
                a = this.this$0.j;
                if (a != null) {
                }
                a2 = this.this$0.j;
                if (a2 != null) {
                }
                return cd6.a;
            }
            dl6 a6 = zl6.a();
            en4$f$c en4_f_c = r0;
            en4$f$c en4_f_c2 = new en4$f$c(this, d5, d6, (xe6) null);
            this.L$0 = il6;
            this.D$0 = d5;
            this.D$1 = d6;
            this.L$1 = str2;
            this.L$2 = list;
            this.L$3 = lc63;
            this.label = 3;
            obj2 = gk6.a(a6, en4_f_c, this);
            Object obj6 = a3;
            if (obj2 == obj6) {
                return obj6;
            }
            d = d6;
            d2 = d5;
            str = (String) obj2;
            this.this$0.e().saveWeatherAddress(new AddressOfWeather(d2, d, str));
            FLogger.INSTANCE.getLocal().d(WeatherManager.l, "getWeatherBaseOnLocation(), currentAddress = " + str);
            a = this.this$0.j;
            if (a != null) {
            }
            a2 = this.this$0.j;
            if (a2 != null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager", f = "WeatherManager.kt", l = {381, 194, 199}, m = "getWeatherBaseOnLocation")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(WeatherManager weatherManager, xe6 xe6) {
            super(xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, (String) null, (xe6<? super lc6<Weather, Boolean>>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForChanceOfRain$1", f = "WeatherManager.kt", l = {158}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(WeatherManager weatherManager, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0074 A[RETURN] */
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                MFUser currentUser = this.this$0.f().getCurrentUser();
                if (currentUser != null) {
                    zh4 temperatureUnit = currentUser.getTemperatureUnit();
                    if (temperatureUnit != null) {
                        int i2 = fn4.b[temperatureUnit.ordinal()];
                        if (i2 == 1) {
                            str = Weather.TEMP_UNIT.CELSIUS.getValue();
                        } else if (i2 == 2) {
                            str = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                        }
                        dl6 b = zl6.b();
                        en4$h$a en4_h_a = new en4$h$a(str, (xe6) null, this);
                        this.L$0 = il6;
                        this.L$1 = currentUser;
                        this.L$2 = str;
                        this.label = 1;
                        obj = gk6.a(b, en4_h_a, this);
                        if (obj == a) {
                            return a;
                        }
                    }
                    str = Weather.TEMP_UNIT.CELSIUS.getValue();
                    dl6 b2 = zl6.b();
                    en4$h$a en4_h_a2 = new en4$h$a(str, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = currentUser;
                    this.L$2 = str;
                    this.label = 1;
                    obj = gk6.a(b2, en4_h_a2, this);
                    if (obj == a) {
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                String str2 = (String) this.L$2;
                MFUser mFUser = (MFUser) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            Weather weather = (Weather) lc6.component1();
            boolean booleanValue = ((Boolean) lc6.component2()).booleanValue();
            if (weather != null) {
                this.this$0.a(weather, booleanValue);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForTemperature$1", f = "WeatherManager.kt", l = {178}, m = "invokeSuspend")
    public static final class i extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(WeatherManager weatherManager, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            i iVar = new i(this.this$0, xe6);
            iVar.p$ = (il6) obj;
            return iVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((i) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0074 A[RETURN] */
        public final Object invokeSuspend(Object obj) {
            String str;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                MFUser currentUser = this.this$0.f().getCurrentUser();
                if (currentUser != null) {
                    zh4 temperatureUnit = currentUser.getTemperatureUnit();
                    if (temperatureUnit != null) {
                        int i2 = fn4.c[temperatureUnit.ordinal()];
                        if (i2 == 1) {
                            str = Weather.TEMP_UNIT.CELSIUS.getValue();
                        } else if (i2 == 2) {
                            str = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                        }
                        dl6 b = zl6.b();
                        en4$i$a en4_i_a = new en4$i$a(str, (xe6) null, this);
                        this.L$0 = il6;
                        this.L$1 = currentUser;
                        this.L$2 = str;
                        this.label = 1;
                        obj = gk6.a(b, en4_i_a, this);
                        if (obj == a) {
                            return a;
                        }
                    }
                    str = Weather.TEMP_UNIT.CELSIUS.getValue();
                    dl6 b2 = zl6.b();
                    en4$i$a en4_i_a2 = new en4$i$a(str, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = currentUser;
                    this.L$2 = str;
                    this.label = 1;
                    obj = gk6.a(b2, en4_i_a2, this);
                    if (obj == a) {
                    }
                }
                return cd6.a;
            } else if (i == 1) {
                String str2 = (String) this.L$2;
                MFUser mFUser = (MFUser) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            Weather weather = (Weather) lc6.component1();
            boolean booleanValue = ((Boolean) lc6.component2()).booleanValue();
            if (weather != null) {
                this.this$0.b(weather, booleanValue);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$getWeatherForWatchApp$1", f = "WeatherManager.kt", l = {122, 130, 134}, m = "invokeSuspend")
    public static final class j extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public int I$1;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$10;
        @DexIgnore
        public Object L$11;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public Object L$9;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(WeatherManager weatherManager, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            j jVar = new j(this.this$0, xe6);
            jVar.p$ = (il6) obj;
            return jVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((j) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: java.util.ArrayList} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v4, resolved type: java.util.ArrayList} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v5, resolved type: java.util.ArrayList} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.util.ArrayList} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: java.util.ArrayList} */
        /* JADX WARNING: type inference failed for: r4v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0145  */
        /* JADX WARNING: Removed duplicated region for block: B:46:0x0260  */
        /* JADX WARNING: Removed duplicated region for block: B:56:0x02ab  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x0314  */
        /* JADX WARNING: Removed duplicated region for block: B:81:0x0382  */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x0387  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x038e  */
        /* JADX WARNING: Removed duplicated region for block: B:88:0x03a3  */
        /* JADX WARNING: Removed duplicated region for block: B:91:0x03ba  */
        /* JADX WARNING: Removed duplicated region for block: B:96:0x03ca  */
        public final Object invokeSuspend(Object obj) {
            Object obj2;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
            DianaPreset dianaPreset;
            DianaPreset dianaPreset2;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
            String str;
            il6 il6;
            MFUser mFUser;
            List list;
            WeatherLocationWrapper weatherLocationWrapper;
            int i;
            lc6 lc6;
            lc6 lc62;
            lc6 lc63;
            j jVar;
            int i2;
            int i3;
            int i4;
            lc6 lc64;
            Object obj3;
            String address;
            Weather weather;
            lc6 lc65;
            Object obj4;
            Weather weather2;
            Object obj5;
            Weather weather3;
            Object obj6;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting3;
            int i5;
            Object obj7;
            int i6;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting4;
            String str2;
            String b;
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting5;
            Object a = ff6.a();
            int i7 = this.label;
            int i8 = 1;
            if (i7 == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                MFUser currentUser = this.this$0.f().getCurrentUser();
                if (currentUser != null) {
                    zh4 temperatureUnit = currentUser.getTemperatureUnit();
                    if (temperatureUnit != null) {
                        int i9 = fn4.a[temperatureUnit.ordinal()];
                        if (i9 == 1) {
                            str2 = Weather.TEMP_UNIT.CELSIUS.getValue();
                        } else if (i9 == 2) {
                            str2 = Weather.TEMP_UNIT.FAHRENHEIT.getValue();
                        }
                        str = str2;
                        DianaPresetRepository c = this.this$0.c();
                        b = this.this$0.i;
                        if (b == null) {
                            DianaPreset activePresetBySerial = c.getActivePresetBySerial(b);
                            if (activePresetBySerial != null) {
                                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                String i10 = WeatherManager.l;
                                local.d(i10, "getWeatherForWatchApp activePreset " + activePresetBySerial);
                                Iterator<T> it = activePresetBySerial.getWatchapps().iterator();
                                while (true) {
                                    if (!it.hasNext()) {
                                        dianaPresetWatchAppSetting5 = null;
                                        break;
                                    }
                                    dianaPresetWatchAppSetting5 = it.next();
                                    if (hf6.a(wg6.a((Object) dianaPresetWatchAppSetting5.getId(), (Object) "weather")).booleanValue()) {
                                        break;
                                    }
                                }
                                DianaPresetWatchAppSetting dianaPresetWatchAppSetting6 = dianaPresetWatchAppSetting5;
                                if (dianaPresetWatchAppSetting6 != null) {
                                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                    String i11 = WeatherManager.l;
                                    local2.d(i11, "getWeatherForWatchApp settings=" + dianaPresetWatchAppSetting6.getSettings());
                                    if (!vi4.a(dianaPresetWatchAppSetting6.getSettings())) {
                                        this.this$0.h = (WeatherWatchAppSetting) new Gson().a(dianaPresetWatchAppSetting6.getSettings(), WeatherWatchAppSetting.class);
                                        WeatherLocationWrapper weatherLocationWrapper2 = new WeatherLocationWrapper("", 0.0d, 0.0d, (String) null, (String) null, true, false, 94, (qg6) null);
                                        WeatherWatchAppSetting c2 = this.this$0.h;
                                        if (c2 != null) {
                                            c2.getLocations().add(0, weatherLocationWrapper2);
                                            List arrayList = new ArrayList();
                                            WeatherWatchAppSetting c3 = this.this$0.h;
                                            if (c3 != null) {
                                                for (WeatherLocationWrapper en4_j_a : c3.getLocations()) {
                                                    List list2 = arrayList;
                                                    list2.add(ik6.a(il62, (af6) null, (ll6) null, new en4$j$a(en4_j_a, (xe6) null, str, this, il62), 3, (Object) null));
                                                    arrayList = list2;
                                                }
                                                list = arrayList;
                                                i3 = list.size();
                                                jVar = this;
                                                obj2 = a;
                                                dianaPresetWatchAppSetting = dianaPresetWatchAppSetting6;
                                                weatherLocationWrapper = weatherLocationWrapper2;
                                                i2 = 0;
                                                lc63 = null;
                                                lc62 = null;
                                                lc6 = null;
                                                dianaPreset = activePresetBySerial;
                                                dianaPreset2 = dianaPreset;
                                                dianaPresetWatchAppSetting2 = dianaPresetWatchAppSetting;
                                                MFUser mFUser2 = currentUser;
                                                il6 = il62;
                                                mFUser = mFUser2;
                                            } else {
                                                wg6.a();
                                                throw null;
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    }
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                    str2 = Weather.TEMP_UNIT.CELSIUS.getValue();
                    str = str2;
                    DianaPresetRepository c4 = this.this$0.c();
                    b = this.this$0.i;
                    if (b == null) {
                    }
                }
                return cd6.a;
            } else if (i7 == 1) {
                i5 = this.I$1;
                int i12 = this.I$0;
                lc63 = (lc6) this.L$10;
                lc6 lc66 = (lc6) this.L$7;
                nc6.a(obj);
                i4 = i12;
                dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) this.L$9;
                obj6 = a;
                weatherLocationWrapper = (WeatherLocationWrapper) this.L$6;
                obj3 = obj;
                list = (List) this.L$8;
                lc64 = (lc6) this.L$11;
                jVar = this;
                String str3 = (String) this.L$2;
                dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$5;
                mFUser = (MFUser) this.L$1;
                dianaPreset2 = (DianaPreset) this.L$4;
                il6 = (il6) this.L$0;
                dianaPreset = (DianaPreset) this.L$3;
                str = str3;
                lc6 lc67 = (lc6) obj3;
                Weather weather4 = (Weather) lc67.getFirst();
                address = weather4 == null ? weather4.getAddress() : null;
                if (TextUtils.isEmpty(address)) {
                    address = jm4.a((Context) PortfolioApp.get.instance(), 2131886228);
                }
                weather = (Weather) lc67.getFirst();
                if (weather != null) {
                    String a2 = gy5.a(address);
                    wg6.a((Object) a2, "Utils.formatLocationName(name)");
                    weather.setAddress(a2);
                }
                lc62 = lc64;
                i = 1;
                lc6 = lc67;
                i2 = i4;
                i2 += i;
                i8 = 1;
            } else if (i7 == 2) {
                i6 = this.I$1;
                i2 = this.I$0;
                lc6 lc68 = (lc6) this.L$10;
                lc6 = (lc6) this.L$7;
                nc6.a(obj);
                obj4 = obj;
                lc65 = (lc6) this.L$11;
                dianaPresetWatchAppSetting4 = (DianaPresetWatchAppSetting) this.L$9;
                jVar = this;
                obj7 = a;
                weatherLocationWrapper = (WeatherLocationWrapper) this.L$6;
                list = (List) this.L$8;
                String str4 = (String) this.L$2;
                dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$5;
                mFUser = (MFUser) this.L$1;
                dianaPreset2 = (DianaPreset) this.L$4;
                il6 = (il6) this.L$0;
                dianaPreset = (DianaPreset) this.L$3;
                str = str4;
                lc6 lc69 = (lc6) obj4;
                weather2 = (Weather) lc69.getFirst();
                if (weather2 != null) {
                    WeatherWatchAppSetting c5 = jVar.this$0.h;
                    if (c5 != null) {
                        weather2.setAddress(c5.getLocations().get(i2).getName());
                    }
                    wg6.a();
                    throw null;
                }
                lc63 = lc69;
                lc62 = lc65;
                i = 1;
                i2 += i;
                i8 = 1;
            } else if (i7 == 3) {
                i3 = this.I$1;
                i2 = this.I$0;
                lc6 lc610 = (lc6) this.L$11;
                lc6 = (lc6) this.L$7;
                dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) this.L$5;
                dianaPreset2 = (DianaPreset) this.L$4;
                dianaPreset = (DianaPreset) this.L$3;
                str = (String) this.L$2;
                mFUser = (MFUser) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj5 = obj;
                dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) this.L$9;
                obj2 = a;
                weatherLocationWrapper = (WeatherLocationWrapper) this.L$6;
                lc63 = (lc6) this.L$10;
                list = (List) this.L$8;
                jVar = this;
                lc6 lc611 = (lc6) obj5;
                weather3 = (Weather) lc611.getFirst();
                if (weather3 != null) {
                    WeatherWatchAppSetting c6 = jVar.this$0.h;
                    if (c6 != null) {
                        weather3.setAddress(c6.getLocations().get(i2).getName());
                    }
                    wg6.a();
                    throw null;
                }
                lc62 = lc611;
                i = 1;
                i2 += i;
                i8 = 1;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (i2 >= i3) {
                jVar.this$0.a((lc6<Weather, Boolean>) lc6, (lc6<Weather, Boolean>) lc63, (lc6<Weather, Boolean>) lc62);
            } else {
                if (i2 == 0) {
                    int i13 = i3;
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting7 = dianaPresetWatchAppSetting;
                    Object obj8 = obj2;
                    jVar.L$0 = il6;
                    jVar.L$1 = mFUser;
                    jVar.L$2 = str;
                    jVar.L$3 = dianaPreset;
                    jVar.L$4 = dianaPreset2;
                    jVar.L$5 = dianaPresetWatchAppSetting2;
                    jVar.L$6 = weatherLocationWrapper;
                    jVar.L$7 = lc6;
                    jVar.L$8 = list;
                    jVar.L$9 = dianaPresetWatchAppSetting7;
                    jVar.L$10 = lc63;
                    lc64 = lc62;
                    jVar.L$11 = lc64;
                    jVar.I$0 = i2;
                    jVar.I$1 = i13;
                    i4 = i2;
                    jVar.label = 1;
                    obj3 = ((rl6) list.get(i2)).a(jVar);
                    if (obj3 == obj8) {
                        return obj8;
                    }
                    obj6 = obj8;
                    dianaPresetWatchAppSetting3 = dianaPresetWatchAppSetting7;
                    i5 = i13;
                    lc6 lc672 = (lc6) obj3;
                    Weather weather42 = (Weather) lc672.getFirst();
                    if (weather42 == null) {
                    }
                    if (TextUtils.isEmpty(address)) {
                    }
                    weather = (Weather) lc672.getFirst();
                    if (weather != null) {
                    }
                    lc62 = lc64;
                    i = 1;
                    lc6 = lc672;
                    i2 = i4;
                    i2 += i;
                    i8 = 1;
                    if (i2 >= i3) {
                    }
                    return obj8;
                }
                if (i2 == i8) {
                    int i14 = i3;
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting8 = dianaPresetWatchAppSetting;
                    Object obj9 = obj2;
                    jVar.L$0 = il6;
                    jVar.L$1 = mFUser;
                    jVar.L$2 = str;
                    jVar.L$3 = dianaPreset;
                    jVar.L$4 = dianaPreset2;
                    jVar.L$5 = dianaPresetWatchAppSetting2;
                    jVar.L$6 = weatherLocationWrapper;
                    jVar.L$7 = lc6;
                    jVar.L$8 = list;
                    jVar.L$9 = dianaPresetWatchAppSetting8;
                    jVar.L$10 = lc63;
                    jVar.L$11 = lc62;
                    jVar.I$0 = i2;
                    int i15 = i14;
                    jVar.I$1 = i15;
                    lc65 = lc62;
                    jVar.label = 2;
                    obj4 = ((rl6) list.get(i2)).a(jVar);
                    if (obj4 == obj9) {
                        return obj9;
                    }
                    obj7 = obj9;
                    i6 = i15;
                    dianaPresetWatchAppSetting4 = dianaPresetWatchAppSetting8;
                    lc6 lc692 = (lc6) obj4;
                    weather2 = (Weather) lc692.getFirst();
                    if (weather2 != null) {
                    }
                    lc63 = lc692;
                    lc62 = lc65;
                    i = 1;
                    i2 += i;
                    i8 = 1;
                    if (i2 >= i3) {
                    }
                    return obj9;
                }
                if (i2 == 2) {
                    jVar.L$0 = il6;
                    jVar.L$1 = mFUser;
                    jVar.L$2 = str;
                    jVar.L$3 = dianaPreset;
                    jVar.L$4 = dianaPreset2;
                    jVar.L$5 = dianaPresetWatchAppSetting2;
                    jVar.L$6 = weatherLocationWrapper;
                    jVar.L$7 = lc6;
                    jVar.L$8 = list;
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting9 = dianaPresetWatchAppSetting;
                    jVar.L$9 = dianaPresetWatchAppSetting9;
                    jVar.L$10 = lc63;
                    jVar.L$11 = lc62;
                    jVar.I$0 = i2;
                    jVar.I$1 = i3;
                    jVar.label = 3;
                    obj5 = ((rl6) list.get(i2)).a(jVar);
                    int i16 = i3;
                    Object obj10 = obj2;
                    if (obj5 == obj10) {
                        return obj10;
                    }
                    obj2 = obj10;
                    dianaPresetWatchAppSetting = dianaPresetWatchAppSetting9;
                    i3 = i16;
                    lc6 lc6112 = (lc6) obj5;
                    weather3 = (Weather) lc6112.getFirst();
                    if (weather3 != null) {
                    }
                    lc62 = lc6112;
                    return obj10;
                }
                i = 1;
                i2 += i;
                i8 = 1;
                if (i2 >= i3) {
                }
            }
            jVar.this$0.a((lc6<Weather, Boolean>) lc6, (lc6<Weather, Boolean>) lc63, (lc6<Weather, Boolean>) lc62);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$showChanceOfRain$2", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class k extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int $rainProbabilityPercent;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(WeatherManager weatherManager, int i, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$rainProbabilityPercent = i;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            k kVar = new k(this.this$0, this.$rainProbabilityPercent, xe6);
            kVar.p$ = (il6) obj;
            return kVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((k) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.b().upsertCustomizeRealData(new CustomizeRealData("chance_of_rain", String.valueOf(this.$rainProbabilityPercent)));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.manager.WeatherManager$showTemperature$2", f = "WeatherManager.kt", l = {}, m = "invokeSuspend")
    public static final class l extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ float $tempInCelsius;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherManager this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(WeatherManager weatherManager, float f, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherManager;
            this.$tempInCelsius = f;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            l lVar = new l(this.this$0, this.$tempInCelsius, xe6);
            lVar.p$ = (il6) obj;
            return lVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((l) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.b().upsertCustomizeRealData(new CustomizeRealData("temperature", String.valueOf(this.$tempInCelsius)));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = WeatherManager.class.getSimpleName();
        wg6.a((Object) simpleName, "WeatherManager::class.java.simpleName");
        l = simpleName;
    }
    */

    @DexIgnore
    public WeatherManager() {
        this.k = zp6.a(false, 1, (Object) null);
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final LocationSource e() {
        LocationSource locationSource = this.c;
        if (locationSource != null) {
            return locationSource;
        }
        wg6.d("mLocationSource");
        throw null;
    }

    @DexIgnore
    public final UserRepository f() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final void g() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final DianaPresetRepository c() {
        DianaPresetRepository dianaPresetRepository = this.f;
        if (dianaPresetRepository != null) {
            return dianaPresetRepository;
        }
        wg6.d("mDianaPresetRepository");
        throw null;
    }

    @DexIgnore
    public final GoogleApiService d() {
        GoogleApiService googleApiService = this.g;
        if (googleApiService != null) {
            return googleApiService;
        }
        wg6.d("mGoogleApiService");
        throw null;
    }

    @DexIgnore
    public final CustomizeRealDataRepository b() {
        CustomizeRealDataRepository customizeRealDataRepository = this.e;
        if (customizeRealDataRepository != null) {
            return customizeRealDataRepository;
        }
        wg6.d("mCustomizeRealDataRepository");
        throw null;
    }

    @DexIgnore
    public final void c(String str) {
        wg6.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForWatchApp");
        this.i = str;
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new j(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public /* synthetic */ WeatherManager(qg6 qg6) {
        this();
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForTemperature");
        this.i = str;
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new i(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final ApiServiceV2 a() {
        ApiServiceV2 apiServiceV2 = this.b;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wg6.d("mApiServiceV2");
        throw null;
    }

    @DexIgnore
    public final void b(Weather weather, boolean z) {
        float f2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        StringBuilder sb = new StringBuilder();
        sb.append("showTemperature - forecast=");
        sb.append(weather.getCurrently().getForecast());
        sb.append(", temperature=");
        Temperature temperature = weather.getCurrently().getTemperature();
        if (temperature != null) {
            sb.append(temperature.getCurrently());
            sb.append(", temperatureUnit=");
            sb.append(weather.getCurrently().getTemperature().getUnit());
            local.d(str, sb.toString());
            Weather.TEMP_UNIT.Companion companion = Weather.TEMP_UNIT.Companion;
            String unit = weather.getCurrently().getTemperature().getUnit();
            wg6.a((Object) unit, "weather.currently.temperature.unit");
            if (companion.getTempUnit(unit) == Weather.TEMP_UNIT.FAHRENHEIT) {
                f2 = zj4.c(weather.getCurrently().getTemperature().getCurrently());
            } else {
                f2 = weather.getCurrently().getTemperature().getCurrently();
            }
            WeatherComplicationAppInfo weatherComplicationAppInfo = weather.toWeatherComplicationAppInfo();
            String str2 = this.i;
            if (str2 != null) {
                PortfolioApp.get.instance().a((DeviceAppResponse) weatherComplicationAppInfo, str2);
            }
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new l(this, f2, (xe6) null), 3, (Object) null);
            jl4 c2 = AnalyticsHelper.f.c("weather");
            if (c2 != null) {
                String str3 = this.i;
                if (str3 != null) {
                    c2.a(str3, z, "");
                } else {
                    wg6.a();
                    throw null;
                }
            }
            AnalyticsHelper.f.e("weather");
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "serial");
        FLogger.INSTANCE.getLocal().d(l, "getWeatherForChanceOfRain");
        this.i = str;
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: com.fossil.xp6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v9, resolved type: com.portfolio.platform.manager.WeatherManager$f} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v38, resolved type: com.fossil.xp6} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v39, resolved type: com.fossil.xp6} */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00c0 A[Catch:{ all -> 0x01e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00fa A[Catch:{ all -> 0x01e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x011f A[Catch:{ all -> 0x01e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0178 A[Catch:{ all -> 0x0174, all -> 0x0170 }] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01d6  */
    public final /* synthetic */ Object a(String str, String str2, xe6<? super lc6<Weather, Boolean>> xe6) {
        g gVar;
        int i2;
        xp6 xp6;
        Object obj;
        xp6 xp62;
        Object obj2;
        lc6 lc6;
        xp6 xp63;
        WeatherManager weatherManager;
        fh6 fh6;
        xp6 xp64;
        String str3;
        String str4;
        xp6 xp65;
        LocationSource.Result result;
        xp6 xp66;
        WeatherManager weatherManager2;
        String str5;
        String str6;
        xp6 xp67;
        String str7;
        LocationSource locationSource;
        xp6 xp68;
        xe6<? super lc6<Weather, Boolean>> xe62 = xe6;
        if (xe62 instanceof g) {
            gVar = (g) xe62;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj3 = gVar.result;
                Object a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj3);
                    xp6 xp69 = this.k;
                    gVar.L$0 = this;
                    str6 = str;
                    gVar.L$1 = str6;
                    str5 = str2;
                    gVar.L$2 = str5;
                    gVar.L$3 = xp69;
                    gVar.label = 1;
                    if (xp69.a((Object) null, gVar) == a2) {
                        return a2;
                    }
                    xp65 = xp69;
                    weatherManager2 = this;
                } else if (i2 == 1) {
                    str5 = (String) gVar.L$2;
                    weatherManager2 = (WeatherManager) gVar.L$0;
                    nc6.a(obj3);
                    String str8 = (String) gVar.L$1;
                    xp65 = (xp6) gVar.L$3;
                    str6 = str8;
                } else if (i2 == 2) {
                    xp6 xp610 = (xp6) gVar.L$3;
                    String str9 = (String) gVar.L$2;
                    String str10 = (String) gVar.L$1;
                    WeatherManager weatherManager3 = (WeatherManager) gVar.L$0;
                    xp68 = xp610;
                    nc6.a(obj3);
                    xp68 = xp610;
                    xp65 = xp610;
                    str3 = str9;
                    str4 = str10;
                    weatherManager = weatherManager3;
                    result = (LocationSource.Result) obj3;
                    if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                        Location location = result.getLocation();
                        fh6 fh62 = new fh6();
                        fh62.element = false;
                        Location location2 = location;
                        ig6 ig6 = r4;
                        fh6 fh63 = fh62;
                        dl6 a3 = zl6.a();
                        fh6 = fh62;
                        xp6 xp611 = xp65;
                        String str11 = str4;
                        try {
                            xp66 = r4;
                            f fVar = new f(location2, fh63, (xe6) null, weatherManager, gVar, str3);
                            gVar.L$0 = weatherManager;
                            gVar.L$1 = str11;
                            gVar.L$2 = str3;
                            xp6 xp612 = xp611;
                            xp66 = xp612;
                            gVar.L$3 = xp612;
                            gVar.L$4 = result;
                            gVar.L$5 = location;
                            gVar.L$6 = fh6;
                            gVar.label = 3;
                            xp64 = xp612;
                            if (gk6.a(a3, ig6, gVar) == a2) {
                                return a2;
                            }
                            lc6 = new lc6(weatherManager.j, hf6.a(fh6.element));
                            obj2 = null;
                            xp62 = xp64;
                            xp62.a(obj2);
                            return lc6;
                        } catch (Throwable th) {
                            th = th;
                            xp63 = xp66;
                            obj = null;
                            xp6 = xp63;
                            xp6.a(obj);
                            throw th;
                        }
                    } else {
                        xp6 xp613 = xp65;
                        FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation getLocation error=" + result.getErrorState());
                        jl4 c2 = AnalyticsHelper.f.c("weather");
                        if (c2 != null) {
                            String str12 = weatherManager.i;
                            if (str12 != null) {
                                c2.a(str12, false, ErrorCodeBuilder.AppError.GET_TEMPERATURE_FAILED.getValue());
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                        xp66 = xp613;
                        AnalyticsHelper.f.e("weather");
                        obj2 = null;
                        try {
                            lc6 = new lc6(null, hf6.a(false));
                            xp62 = xp613;
                            xp62.a(obj2);
                            return lc6;
                        } catch (Throwable th2) {
                            th = th2;
                            obj = null;
                            xp6 = xp613;
                            xp6.a(obj);
                            throw th;
                        }
                    }
                } else if (i2 == 3) {
                    fh6 fh64 = (fh6) gVar.L$6;
                    Location location3 = (Location) gVar.L$5;
                    LocationSource.Result result2 = (LocationSource.Result) gVar.L$4;
                    xp6 xp614 = (xp6) gVar.L$3;
                    String str13 = (String) gVar.L$2;
                    String str14 = (String) gVar.L$1;
                    WeatherManager weatherManager4 = (WeatherManager) gVar.L$0;
                    try {
                        xp68 = xp614;
                        nc6.a(obj3);
                        xp68 = xp614;
                        fh6 = fh64;
                        weatherManager = weatherManager4;
                        xp64 = xp614;
                        lc6 = new lc6(weatherManager.j, hf6.a(fh6.element));
                        obj2 = null;
                        xp62 = xp64;
                        xp62.a(obj2);
                        return lc6;
                    } catch (Throwable th3) {
                        th = th3;
                        xp67 = xp68;
                        obj = null;
                        xp6 = xp67;
                        xp6.a(obj);
                        throw th;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation");
                if (weatherManager2.j == null) {
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "Calendar.getInstance()");
                    long timeInMillis = instance.getTimeInMillis();
                    Weather weather = weatherManager2.j;
                    if (weather != null) {
                        str7 = str5;
                        if (timeInMillis - weather.getUpdatedAt() < ((long) 60000)) {
                            lc6 lc62 = new lc6(weatherManager2.j, hf6.a(true));
                            xp65.a((Object) null);
                            return lc62;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    str7 = str5;
                }
                locationSource = weatherManager2.c;
                if (locationSource == null) {
                    Object r6 = weatherManager2.a;
                    if (r6 != 0) {
                        gVar.L$0 = weatherManager2;
                        gVar.L$1 = str6;
                        String str15 = str7;
                        gVar.L$2 = str15;
                        gVar.L$3 = xp65;
                        gVar.label = 2;
                        obj3 = locationSource.getLocation(r6, false, gVar);
                        if (obj3 == a2) {
                            return a2;
                        }
                        str3 = str15;
                        weatherManager = weatherManager2;
                        str4 = str6;
                        result = (LocationSource.Result) obj3;
                        if (result.getErrorState() != LocationSource.ErrorState.SUCCESS) {
                        }
                    } else {
                        try {
                            wg6.d("mPortfolioApp");
                            throw null;
                        } catch (Throwable th4) {
                            th = th4;
                            xp63 = xp65;
                            obj = null;
                            xp6 = xp63;
                            xp6.a(obj);
                            throw th;
                        }
                    }
                } else {
                    obj = null;
                    try {
                        wg6.d("mLocationSource");
                        throw null;
                    } catch (Throwable th5) {
                        th = th5;
                        xp6 = xp65;
                        xp6.a(obj);
                        throw th;
                    }
                }
            }
        }
        gVar = new g(this, xe62);
        Object obj32 = gVar.result;
        Object a22 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        try {
            FLogger.INSTANCE.getLocal().d(l, "getWeatherBaseOnLocation");
            if (weatherManager2.j == null) {
            }
            locationSource = weatherManager2.c;
            if (locationSource == null) {
            }
        } catch (Throwable th6) {
            th = th6;
            xp67 = xp65;
            obj = null;
            xp6 = xp67;
            xp6.a(obj);
            throw th;
        }
    }

    @DexIgnore
    public final boolean a(AddressOfWeather addressOfWeather, double d2, double d3) {
        Location location = new Location("LocationA");
        location.setLatitude(addressOfWeather.getLat());
        location.setLongitude(addressOfWeather.getLng());
        Location location2 = new Location("LocationB");
        location2.setLatitude(d2);
        location2.setLongitude(d3);
        float distanceTo = location.distanceTo(location2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "distance between two address=" + distanceTo);
        return distanceTo < ((float) 5000);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a0  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x014f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final /* synthetic */ Object a(double d2, double d3, xe6<? super String> xe6) {
        b bVar;
        int i2;
        String str;
        ap4 ap4;
        double d4 = d2;
        double d5 = d3;
        xe6<? super String> xe62 = xe6;
        if (xe62 instanceof b) {
            bVar = (b) xe62;
            int i3 = bVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                bVar.label = i3 - Integer.MIN_VALUE;
                b bVar2 = bVar;
                Object obj = bVar2.result;
                Object a2 = ff6.a();
                i2 = bVar2.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = l;
                    local.d(str2, "getAddressBaseOnLocation latLng=" + d4 + ',' + d5);
                    c cVar = r0;
                    str = "administrative_area_level_1";
                    c cVar2 = new c(this, d2, d3, "administrative_area_level_1", (xe6) null);
                    bVar2.L$0 = this;
                    bVar2.D$0 = d4;
                    bVar2.D$1 = d5;
                    bVar2.L$1 = str;
                    bVar2.label = 1;
                    obj = ResponseKt.a(cVar, bVar2);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    double d6 = bVar2.D$1;
                    double d7 = bVar2.D$0;
                    WeatherManager weatherManager = (WeatherManager) bVar2.L$0;
                    nc6.a(obj);
                    str = (String) bVar2.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ku3 ku3 = (ku3) ((cp4) ap4).a();
                    fu3 b2 = ku3 != null ? ku3.b("results") : null;
                    if (b2 != null) {
                        Iterator it = b2.iterator();
                        while (it.hasNext()) {
                            JsonElement jsonElement = (JsonElement) it.next();
                            wg6.a((Object) jsonElement, Constants.RESULT);
                            ku3 d8 = jsonElement.d();
                            fu3 b3 = d8 != null ? d8.b("address_components") : null;
                            if (b3 != null) {
                                Iterator it2 = b3.iterator();
                                while (it2.hasNext()) {
                                    JsonElement jsonElement2 = (JsonElement) it2.next();
                                    wg6.a((Object) jsonElement2, "addressComponent");
                                    ku3 d9 = jsonElement2.d();
                                    fu3 b4 = d9 != null ? d9.b("types") : null;
                                    if (b4 != null && b4.size() > 0) {
                                        JsonElement jsonElement3 = b4.get(0);
                                        wg6.a((Object) jsonElement3, "types[0]");
                                        if (wg6.a((Object) jsonElement3.f(), (Object) str)) {
                                            JsonElement a3 = jsonElement2.d().a("long_name");
                                            wg6.a((Object) a3, "addressComponent.asJsonObject.get(\"long_name\")");
                                            String f2 = a3.f();
                                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                            String str3 = l;
                                            local2.d(str3, "cityName=" + f2);
                                            wg6.a((Object) f2, "cityName");
                                            return f2;
                                        }
                                    }
                                }
                                continue;
                            }
                        }
                    }
                    return "";
                } else if (ap4 instanceof zo4) {
                    return "";
                } else {
                    throw new kc6();
                }
            }
        }
        bVar = new b(this, xe62);
        b bVar22 = bVar;
        Object obj2 = bVar22.result;
        Object a22 = ff6.a();
        i2 = bVar22.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0027  */
    public final /* synthetic */ Object a(double d2, double d3, String str, xe6<? super lc6<Weather, Boolean>> xe6) {
        d dVar;
        int i2;
        ap4 ap4;
        xe6<? super lc6<Weather, Boolean>> xe62 = xe6;
        if (xe62 instanceof d) {
            dVar = (d) xe62;
            int i3 = dVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                dVar.label = i3 - Integer.MIN_VALUE;
                d dVar2 = dVar;
                Object obj = dVar2.result;
                Object a2 = ff6.a();
                i2 = dVar2.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    e eVar = new e(this, d2, d3, str, (xe6) null);
                    dVar2.L$0 = this;
                    dVar2.D$0 = d2;
                    dVar2.D$1 = d3;
                    dVar2.L$1 = str;
                    dVar2.label = 1;
                    obj = ResponseKt.a(eVar, dVar2);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    String str2 = (String) dVar2.L$1;
                    double d4 = dVar2.D$1;
                    double d5 = dVar2.D$0;
                    WeatherManager weatherManager = (WeatherManager) dVar2.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = l;
                local.d(str3, "getWeather onResponse: response = " + ap4);
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() == null) {
                        return new lc6(null, hf6.a(false));
                    }
                    gp4 gp4 = new gp4();
                    gp4.a((ku3) cp4.a());
                    return new lc6(gp4.a(), hf6.a(false));
                } else if (ap4 instanceof zo4) {
                    return new lc6(null, hf6.a(false));
                } else {
                    throw new kc6();
                }
            }
        }
        dVar = new d(this, xe62);
        d dVar22 = dVar;
        Object obj2 = dVar22.result;
        Object a22 = ff6.a();
        i2 = dVar22.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = l;
        local2.d(str32, "getWeather onResponse: response = " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final void a(Weather weather, boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "showChanceOfRain - probability=" + weather.getCurrently().getRainProbability());
        int rainProbability = (int) (weather.getCurrently().getRainProbability() * ((float) 100));
        ChanceOfRainComplicationAppInfo chanceOfRainComplicationAppInfo = weather.toChanceOfRainComplicationAppInfo();
        String str2 = this.i;
        if (str2 != null) {
            PortfolioApp.get.instance().a((DeviceAppResponse) chanceOfRainComplicationAppInfo, str2);
        }
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new k(this, rainProbability, (xe6) null), 3, (Object) null);
        jl4 c2 = AnalyticsHelper.f.c("chance-of-rain");
        if (c2 != null) {
            String str3 = this.i;
            if (str3 != null) {
                c2.a(str3, z, "");
            } else {
                wg6.a();
                throw null;
            }
        }
        AnalyticsHelper.f.e("chance-of-rain");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x002f, code lost:
        r1 = r5.getFirst();
     */
    @DexIgnore
    public final void a(lc6<Weather, Boolean> lc6, lc6<Weather, Boolean> lc62, lc6<Weather, Boolean> lc63) {
        Weather first;
        Weather first2;
        Weather first3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = l;
        local.d(str, "showWeatherWatchApp - firstWeather=" + lc6 + ", secondWeather=" + lc62 + ", thirdWeather=" + lc63);
        WeatherWatchAppInfo weatherWatchAppInfo = (lc6 == null || first3 == null) ? null : first3.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo2 = (lc62 == null || (first2 = lc62.getFirst()) == null) ? null : first2.toWeatherWatchAppInfo();
        WeatherWatchAppInfo weatherWatchAppInfo3 = (lc63 == null || (first = lc63.getFirst()) == null) ? null : first.toWeatherWatchAppInfo();
        if (weatherWatchAppInfo == null) {
            return;
        }
        if (weatherWatchAppInfo != null) {
            PortfolioApp.get.instance().a((DeviceAppResponse) new WeatherInfoWatchAppResponse(weatherWatchAppInfo, weatherWatchAppInfo2, weatherWatchAppInfo3), PortfolioApp.get.instance().e());
            jl4 c2 = AnalyticsHelper.f.c("weather");
            if (c2 != null) {
                String str2 = this.i;
                if (str2 != null) {
                    Boolean second = lc6 != null ? lc6.getSecond() : null;
                    if (second != null) {
                        c2.a(str2, second.booleanValue(), "");
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            AnalyticsHelper.f.e("weather");
            return;
        }
        wg6.a();
        throw null;
    }
}
