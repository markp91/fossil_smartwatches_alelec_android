package com.portfolio.platform.manager;

import android.content.Context;
import android.util.Base64;
import android.util.Log;
import com.google.gson.Gson;
import com.j256.ormlite.logger.Logger;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.AccessGroup;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SoLibraryLoader {
    @DexIgnore
    public String a; // = "SoLibraryLoader";
    @DexIgnore
    public char[][] b; // = {new char[]{'A', 1, '1', 'D', 'E', 'F'}, new char[]{'F', 2, 'H', 'L', 'K', '~'}, new char[]{'#', 'O', 'P', 'Q', 'R', 'S'}, new char[]{'T', '1', 'V', 'W', ')', 'Y'}, new char[]{'$', 'I', 'D', 'O', '0', 'E'}, new char[]{'X', 'V', '%', '^', 'G', '3'}, new char[]{'a', 'b', 'c', '&', 'e', 'f'}, new char[]{'g', 'h', 'i', 'j', 'l', 'k'}, new char[]{'k', 'm', 'n', 'o', 'p', 'q'}, new char[]{'1', '2', 8, '4', '5', '6'}, new char[]{'7', '(', '9', '0', '+', '-'}, new char[]{',', '.', '!', '@', '#', '$'}, new char[]{'^', '&', '|', ']', '[', ';'}};
    @DexIgnore
    public Gson c; // = new Gson();

    /*
    static {
        System.loadLibrary("res-c");
    }
    */

    @DexIgnore
    public final Access a(Context context, int i) throws Exception {
        int i2;
        Random random = new Random();
        int nextInt = random.nextInt(1000);
        while (true) {
            i2 = nextInt + 1;
            if (a(i2)) {
                break;
            }
            nextInt = random.nextInt(1000);
        }
        String str = new String(getData(i2, i), StandardCharsets.UTF_8);
        AccessGroup accessGroup = (AccessGroup) this.c.a(a(a(str, i2), a(new String(find(context, i2), StandardCharsets.UTF_8), i2).trim()), AccessGroup.class);
        if (accessGroup == null) {
            return null;
        }
        if (i != 0) {
            if (i == 1) {
                return accessGroup.getOreo();
            }
            if (i != 4) {
                return accessGroup.getLollipop();
            }
        }
        return accessGroup.getPie();
    }

    @DexIgnore
    public final native byte[] find(Context context, int i);

    @DexIgnore
    public final native byte[] getData(int i, int i2);

    @DexIgnore
    public Access a(Context context) {
        int hashCode = "release".hashCode();
        int i = 0;
        char c2 = (hashCode == -1897523141 || hashCode != 1090594823) ? (char) 65535 : 0;
        if (c2 != 0) {
            i = c2 != 1 ? 2 : 1;
        }
        try {
            return a(context, i);
        } catch (Exception e) {
            String str = this.a;
            Log.e(str, "exception with flavor " + i + " exception " + e.toString());
            if (i == 0) {
                try {
                    return a(context, 4);
                } catch (Exception e2) {
                    String str2 = this.a;
                    Log.e(str2, "exception again with flavor 4 exception " + e2.toString());
                    return null;
                }
            }
            return null;
        }
    }

    @DexIgnore
    public final boolean a(int i) {
        int i2 = 3;
        if (i > 3 && i % 2 != 0) {
            while (((double) i2) <= Math.sqrt((double) i) && i % i2 != 0) {
                i2 += 2;
            }
            if (i % i2 != 0) {
                return true;
            }
            return false;
        } else if (i == 2 || i == 3) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public final String a(String str, int i) {
        char[][] cArr = this.b;
        char[] cArr2 = {cArr[1][5], cArr[0][1], cArr[1][1], cArr[2][0], cArr[4][0], cArr[5][2], cArr[5][3], cArr[6][3], cArr[9][2], cArr[10][1], cArr[3][4], 0};
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            sb.append((char) (str.charAt(i2) ^ cArr2[(i2 + i) % cArr2.length]));
        }
        return sb.toString();
    }

    @DexIgnore
    public final String a(String str, String str2) throws Exception {
        String[] split = str.split("]");
        if (split.length == 3) {
            try {
                byte[] decode = Base64.decode(split[0], 0);
                byte[] decode2 = Base64.decode(split[1], 0);
                byte[] decode3 = Base64.decode(split[2], 0);
                SecretKey a2 = a(decode, str2);
                Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                instance.init(2, a2, new GCMParameterSpec(Logger.DEFAULT_FULL_MESSAGE_LENGTH, decode2));
                return new String(instance.doFinal(decode3), "UTF-8");
            } catch (Throwable th) {
                String str3 = this.a;
                Log.e(str3, "exception 2 " + th.toString());
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Invalid encypted text format");
        }
    }

    @DexIgnore
    public final SecretKey a(byte[] bArr, String str) throws Exception {
        try {
            return new SecretKeySpec(SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1", "BC").generateSecret(new PBEKeySpec(str.toCharArray(), bArr, 1000, Logger.DEFAULT_FULL_MESSAGE_LENGTH)).getEncoded(), "AES");
        } catch (Throwable th) {
            String str2 = this.a;
            Log.e(str2, "exception 1 " + th.toString());
            throw th;
        }
    }
}
