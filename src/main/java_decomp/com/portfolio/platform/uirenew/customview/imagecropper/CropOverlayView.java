package com.portfolio.platform.uirenew.customview.imagecropper;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import com.fossil.lw4;
import com.fossil.nw4;
import com.fossil.ow4;
import com.fossil.pw4;
import com.portfolio.platform.uirenew.customview.imagecropper.CropImageView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CropOverlayView extends View {
    @DexIgnore
    public int A;
    @DexIgnore
    public float B;
    @DexIgnore
    public CropImageView.d C;
    @DexIgnore
    public CropImageView.c D;
    @DexIgnore
    public /* final */ Rect E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public Integer G;
    @DexIgnore
    public ScaleGestureDetector a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ow4 c;
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public Paint f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Path j;
    @DexIgnore
    public /* final */ float[] o;
    @DexIgnore
    public /* final */ RectF p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public float w;
    @DexIgnore
    public pw4 x;
    @DexIgnore
    public boolean y;
    @DexIgnore
    public int z;

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        @TargetApi(11)
        public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
            RectF f = CropOverlayView.this.c.f();
            float focusX = scaleGestureDetector.getFocusX();
            float focusY = scaleGestureDetector.getFocusY();
            float currentSpanY = scaleGestureDetector.getCurrentSpanY() / 2.0f;
            float currentSpanX = scaleGestureDetector.getCurrentSpanX() / 2.0f;
            float f2 = focusY - currentSpanY;
            float f3 = focusX - currentSpanX;
            float f4 = focusX + currentSpanX;
            float f5 = focusY + currentSpanY;
            if (f3 >= f4 || f2 > f5 || f3 < 0.0f || f4 > CropOverlayView.this.c.c() || f2 < 0.0f || f5 > CropOverlayView.this.c.b()) {
                return true;
            }
            f.set(f3, f2, f4, f5);
            CropOverlayView.this.c.a(f);
            CropOverlayView.this.invalidate();
            return true;
        }
    }

    @DexIgnore
    public CropOverlayView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public boolean b(boolean z2) {
        if (this.b == z2) {
            return false;
        }
        this.b = z2;
        if (!this.b || this.a != null) {
            return true;
        }
        this.a = new ScaleGestureDetector(getContext(), new c());
        return true;
    }

    @DexIgnore
    public boolean c() {
        return this.y;
    }

    @DexIgnore
    public final void d(Canvas canvas) {
        if (this.h != null) {
            Paint paint = this.f;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : 0.0f;
            RectF f2 = this.c.f();
            f2.inset(strokeWidth, strokeWidth);
            float width = f2.width() / 3.0f;
            float height = f2.height() / 3.0f;
            if (this.D == CropImageView.c.OVAL) {
                float width2 = (f2.width() / 2.0f) - strokeWidth;
                float height2 = (f2.height() / 2.0f) - strokeWidth;
                float f3 = f2.left + width;
                float f4 = f2.right - width;
                float sin = (float) (((double) height2) * Math.sin(Math.acos((double) ((width2 - width) / width2))));
                canvas.drawLine(f3, (f2.top + height2) - sin, f3, (f2.bottom - height2) + sin, this.h);
                canvas.drawLine(f4, (f2.top + height2) - sin, f4, (f2.bottom - height2) + sin, this.h);
                float f5 = f2.top + height;
                float f6 = f2.bottom - height;
                float cos = (float) (((double) width2) * Math.cos(Math.asin((double) ((height2 - height) / height2))));
                canvas.drawLine((f2.left + width2) - cos, f5, (f2.right - width2) + cos, f5, this.h);
                canvas.drawLine((f2.left + width2) - cos, f6, (f2.right - width2) + cos, f6, this.h);
                return;
            }
            float f7 = f2.left + width;
            float f8 = f2.right - width;
            Canvas canvas2 = canvas;
            canvas2.drawLine(f7, f2.top, f7, f2.bottom, this.h);
            canvas.drawLine(f8, f2.top, f8, f2.bottom, this.h);
            float f9 = f2.top + height;
            float f10 = f2.bottom - height;
            canvas2.drawLine(f2.left, f9, f2.right, f9, this.h);
            canvas.drawLine(f2.left, f10, f2.right, f10, this.h);
        }
    }

    @DexIgnore
    public final void e() {
        if (this.x != null) {
            this.x = null;
            a(false);
            invalidate();
        }
    }

    @DexIgnore
    public void f() {
        if (this.F) {
            setCropWindowRect(lw4.b);
            b();
            invalidate();
        }
    }

    @DexIgnore
    public int getAspectRatioX() {
        return this.z;
    }

    @DexIgnore
    public int getAspectRatioY() {
        return this.A;
    }

    @DexIgnore
    public CropImageView.c getCropShape() {
        return this.D;
    }

    @DexIgnore
    public RectF getCropWindowRect() {
        return this.c.f();
    }

    @DexIgnore
    public CropImageView.d getGuidelines() {
        return this.C;
    }

    @DexIgnore
    public Rect getInitialCropWindowRect() {
        return this.E;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        a(canvas);
        if (this.c.i()) {
            CropImageView.d dVar = this.C;
            if (dVar == CropImageView.d.ON) {
                d(canvas);
            } else if (dVar == CropImageView.d.ON_TOUCH && this.x != null) {
                d(canvas);
            }
        }
        b(canvas);
        c(canvas);
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!isEnabled()) {
            return false;
        }
        if (this.b) {
            this.a.onTouchEvent(motionEvent);
        }
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    b(motionEvent.getX(), motionEvent.getY());
                    getParent().requestDisallowInterceptTouchEvent(true);
                    return true;
                } else if (action != 3) {
                    return false;
                }
            }
            getParent().requestDisallowInterceptTouchEvent(false);
            e();
            return true;
        }
        a(motionEvent.getX(), motionEvent.getY());
        return true;
    }

    @DexIgnore
    public void setAspectRatioX(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.z != i2) {
            this.z = i2;
            this.B = ((float) this.z) / ((float) this.A);
            if (this.F) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setAspectRatioY(int i2) {
        if (i2 <= 0) {
            throw new IllegalArgumentException("Cannot set aspect ratio value to a number less than or equal to 0.");
        } else if (this.A != i2) {
            this.A = i2;
            this.B = ((float) this.z) / ((float) this.A);
            if (this.F) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setCropShape(CropImageView.c cVar) {
        if (this.D != cVar) {
            this.D = cVar;
            if (Build.VERSION.SDK_INT <= 17) {
                if (this.D == CropImageView.c.OVAL) {
                    this.G = Integer.valueOf(getLayerType());
                    if (this.G.intValue() != 1) {
                        setLayerType(1, (Paint) null);
                    } else {
                        this.G = null;
                    }
                } else {
                    Integer num = this.G;
                    if (num != null) {
                        setLayerType(num.intValue(), (Paint) null);
                        this.G = null;
                    }
                }
            }
            invalidate();
        }
    }

    @DexIgnore
    public void setCropWindowChangeListener(b bVar) {
        this.d = bVar;
    }

    @DexIgnore
    public void setCropWindowRect(RectF rectF) {
        this.c.a(rectF);
    }

    @DexIgnore
    public void setFixedAspectRatio(boolean z2) {
        if (this.y != z2) {
            this.y = z2;
            if (this.F) {
                b();
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setGuidelines(CropImageView.d dVar) {
        if (this.C != dVar) {
            this.C = dVar;
            if (this.F) {
                invalidate();
            }
        }
    }

    @DexIgnore
    public void setInitialAttributeValues(nw4 nw4) {
        this.c.a(nw4);
        setCropShape(nw4.a);
        setSnapRadius(nw4.b);
        setGuidelines(nw4.d);
        setFixedAspectRatio(nw4.p);
        setAspectRatioX(nw4.q);
        setAspectRatioY(nw4.r);
        b(nw4.i);
        this.v = nw4.c;
        this.u = nw4.o;
        this.f = a(nw4.s, nw4.t);
        this.s = nw4.v;
        this.t = nw4.w;
        this.g = a(nw4.u, nw4.x);
        this.h = a(nw4.y, nw4.z);
        this.i = a(nw4.A);
    }

    @DexIgnore
    public void setInitialCropWindowRect(Rect rect) {
        Rect rect2 = this.E;
        if (rect == null) {
            rect = lw4.a;
        }
        rect2.set(rect);
        if (this.F) {
            b();
            invalidate();
            a(false);
        }
    }

    @DexIgnore
    public void setSnapRadius(float f2) {
        this.w = f2;
    }

    @DexIgnore
    public CropOverlayView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.c = new ow4();
        this.e = new RectF();
        this.j = new Path();
        this.o = new float[8];
        this.p = new RectF();
        this.B = ((float) this.z) / ((float) this.A);
        this.E = new Rect();
    }

    @DexIgnore
    public void a() {
        RectF cropWindowRect = getCropWindowRect();
        b(cropWindowRect);
        this.c.a(cropWindowRect);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        if (this.g != null) {
            Paint paint = this.f;
            float f2 = 0.0f;
            float strokeWidth = paint != null ? paint.getStrokeWidth() : 0.0f;
            float strokeWidth2 = this.g.getStrokeWidth();
            float f3 = strokeWidth2 / 2.0f;
            if (this.D == CropImageView.c.RECTANGLE) {
                f2 = this.s;
            }
            float f4 = f2 + f3;
            RectF f5 = this.c.f();
            f5.inset(f4, f4);
            float f6 = (strokeWidth2 - strokeWidth) / 2.0f;
            float f7 = f3 + f6;
            float f8 = f5.left;
            float f9 = f5.top;
            Canvas canvas2 = canvas;
            canvas2.drawLine(f8 - f6, f9 - f7, f8 - f6, f9 + this.t, this.g);
            float f10 = f5.left;
            float f11 = f5.top;
            canvas2.drawLine(f10 - f7, f11 - f6, f10 + this.t, f11 - f6, this.g);
            float f12 = f5.right;
            float f13 = f5.top;
            canvas2.drawLine(f12 + f6, f13 - f7, f12 + f6, f13 + this.t, this.g);
            float f14 = f5.right;
            float f15 = f5.top;
            canvas2.drawLine(f14 + f7, f15 - f6, f14 - this.t, f15 - f6, this.g);
            float f16 = f5.left;
            float f17 = f5.bottom;
            canvas2.drawLine(f16 - f6, f17 + f7, f16 - f6, f17 - this.t, this.g);
            float f18 = f5.left;
            float f19 = f5.bottom;
            canvas2.drawLine(f18 - f7, f19 + f6, f18 + this.t, f19 + f6, this.g);
            float f20 = f5.right;
            float f21 = f5.bottom;
            canvas2.drawLine(f20 + f6, f21 + f7, f20 + f6, f21 - this.t, this.g);
            float f22 = f5.right;
            float f23 = f5.bottom;
            canvas2.drawLine(f22 + f7, f23 + f6, f22 - this.t, f23 + f6, this.g);
        }
    }

    @DexIgnore
    public void a(float[] fArr, int i2, int i3) {
        if (fArr == null || !Arrays.equals(this.o, fArr)) {
            if (fArr == null) {
                Arrays.fill(this.o, 0.0f);
            } else {
                System.arraycopy(fArr, 0, this.o, 0, fArr.length);
            }
            this.q = i2;
            this.r = i3;
            RectF f2 = this.c.f();
            if (f2.width() == 0.0f || f2.height() == 0.0f) {
                b();
            }
        }
    }

    @DexIgnore
    public void b(int i2, int i3) {
        this.c.b(i2, i3);
    }

    @DexIgnore
    public final void b() {
        float max = Math.max(lw4.e(this.o), 0.0f);
        float max2 = Math.max(lw4.g(this.o), 0.0f);
        float min = Math.min(lw4.f(this.o), (float) getWidth());
        float min2 = Math.min(lw4.a(this.o), (float) getHeight());
        if (min > max && min2 > max2) {
            RectF rectF = new RectF();
            this.F = true;
            float f2 = this.u;
            float f3 = min - max;
            float f4 = f2 * f3;
            float f5 = min2 - max2;
            float f6 = f2 * f5;
            if (this.E.width() > 0 && this.E.height() > 0) {
                rectF.left = (((float) this.E.left) / this.c.h()) + max;
                rectF.top = (((float) this.E.top) / this.c.g()) + max2;
                rectF.right = rectF.left + (((float) this.E.width()) / this.c.h());
                rectF.bottom = rectF.top + (((float) this.E.height()) / this.c.g());
                rectF.left = Math.max(max, rectF.left);
                rectF.top = Math.max(max2, rectF.top);
                rectF.right = Math.min(min, rectF.right);
                rectF.bottom = Math.min(min2, rectF.bottom);
            } else if (!this.y || min <= max || min2 <= max2) {
                rectF.left = max + f4;
                rectF.top = max2 + f6;
                rectF.right = min - f4;
                rectF.bottom = min2 - f6;
            } else if (f3 / f5 > this.B) {
                rectF.top = max2 + f6;
                rectF.bottom = min2 - f6;
                float width = ((float) getWidth()) / 2.0f;
                this.B = ((float) this.z) / ((float) this.A);
                float max3 = Math.max(this.c.e(), rectF.height() * this.B) / 2.0f;
                rectF.left = width - max3;
                rectF.right = width + max3;
            } else {
                rectF.left = max + f4;
                rectF.right = min - f4;
                float height = ((float) getHeight()) / 2.0f;
                float max4 = Math.max(this.c.d(), rectF.width() / this.B) / 2.0f;
                rectF.top = height - max4;
                rectF.bottom = height + max4;
            }
            b(rectF);
            this.c.a(rectF);
        }
    }

    @DexIgnore
    public void a(int i2, int i3) {
        this.c.a(i2, i3);
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5) {
        this.c.a(f2, f3, f4, f5);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        RectF f2 = this.c.f();
        float max = Math.max(lw4.e(this.o), 0.0f);
        float max2 = Math.max(lw4.g(this.o), 0.0f);
        float min = Math.min(lw4.f(this.o), (float) getWidth());
        float min2 = Math.min(lw4.a(this.o), (float) getHeight());
        if (this.D != CropImageView.c.RECTANGLE) {
            this.j.reset();
            if (Build.VERSION.SDK_INT > 17 || this.D != CropImageView.c.OVAL) {
                this.e.set(f2.left, f2.top, f2.right, f2.bottom);
            } else {
                this.e.set(f2.left + 2.0f, f2.top + 2.0f, f2.right - 2.0f, f2.bottom - 2.0f);
            }
            this.j.addOval(this.e, Path.Direction.CW);
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.j);
            } else {
                canvas.clipPath(this.j, Region.Op.XOR);
            }
            canvas.drawRect(max, max2, min, min2, this.i);
            canvas.restore();
        } else if (!d() || Build.VERSION.SDK_INT <= 17) {
            Canvas canvas2 = canvas;
            float f3 = max;
            float f4 = min;
            canvas2.drawRect(f3, max2, f4, f2.top, this.i);
            canvas2.drawRect(f3, f2.bottom, f4, min2, this.i);
            canvas2.drawRect(f3, f2.top, f2.left, f2.bottom, this.i);
            canvas.drawRect(f2.right, f2.top, min, f2.bottom, this.i);
        } else {
            this.j.reset();
            Path path = this.j;
            float[] fArr = this.o;
            path.moveTo(fArr[0], fArr[1]);
            Path path2 = this.j;
            float[] fArr2 = this.o;
            path2.lineTo(fArr2[2], fArr2[3]);
            Path path3 = this.j;
            float[] fArr3 = this.o;
            path3.lineTo(fArr3[4], fArr3[5]);
            Path path4 = this.j;
            float[] fArr4 = this.o;
            path4.lineTo(fArr4[6], fArr4[7]);
            this.j.close();
            canvas.save();
            if (Build.VERSION.SDK_INT >= 26) {
                canvas.clipOutPath(this.j);
            } else {
                canvas.clipPath(this.j, Region.Op.INTERSECT);
            }
            canvas.clipRect(f2, Region.Op.XOR);
            canvas.drawRect(max, max2, min, min2, this.i);
            canvas.restore();
        }
    }

    @DexIgnore
    public final boolean d() {
        float[] fArr = this.o;
        return (fArr[0] == fArr[6] || fArr[1] == fArr[7]) ? false : true;
    }

    @DexIgnore
    public final void b(RectF rectF) {
        if (rectF.width() < this.c.e()) {
            float e2 = (this.c.e() - rectF.width()) / 2.0f;
            rectF.left -= e2;
            rectF.right += e2;
        }
        if (rectF.height() < this.c.d()) {
            float d2 = (this.c.d() - rectF.height()) / 2.0f;
            rectF.top -= d2;
            rectF.bottom += d2;
        }
        if (rectF.width() > this.c.c()) {
            float width = (rectF.width() - this.c.c()) / 2.0f;
            rectF.left += width;
            rectF.right -= width;
        }
        if (rectF.height() > this.c.b()) {
            float height = (rectF.height() - this.c.b()) / 2.0f;
            rectF.top += height;
            rectF.bottom -= height;
        }
        a(rectF);
        if (this.p.width() > 0.0f && this.p.height() > 0.0f) {
            float max = Math.max(this.p.left, 0.0f);
            float max2 = Math.max(this.p.top, 0.0f);
            float min = Math.min(this.p.right, (float) getWidth());
            float min2 = Math.min(this.p.bottom, (float) getHeight());
            if (rectF.left < max) {
                rectF.left = max;
            }
            if (rectF.top < max2) {
                rectF.top = max2;
            }
            if (rectF.right > min) {
                rectF.right = min;
            }
            if (rectF.bottom > min2) {
                rectF.bottom = min2;
            }
        }
        if (this.y && ((double) Math.abs(rectF.width() - (rectF.height() * this.B))) > 0.1d) {
            if (rectF.width() > rectF.height() * this.B) {
                float abs = Math.abs((rectF.height() * this.B) - rectF.width()) / 2.0f;
                rectF.left += abs;
                rectF.right -= abs;
                return;
            }
            float abs2 = Math.abs((rectF.width() / this.B) - rectF.height()) / 2.0f;
            rectF.top += abs2;
            rectF.bottom -= abs2;
        }
    }

    @DexIgnore
    public static Paint a(int i2) {
        Paint paint = new Paint();
        paint.setColor(i2);
        return paint;
    }

    @DexIgnore
    public static Paint a(float f2, int i2) {
        if (f2 <= 0.0f) {
            return null;
        }
        Paint paint = new Paint();
        paint.setColor(i2);
        paint.setStrokeWidth(f2);
        paint.setStyle(Paint.Style.STROKE);
        paint.setAntiAlias(true);
        return paint;
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        this.x = this.c.a(f2, f3, this.v, this.D);
        if (this.x != null) {
            invalidate();
        }
    }

    @DexIgnore
    public final boolean a(RectF rectF) {
        RectF rectF2 = rectF;
        float e2 = lw4.e(this.o);
        float g2 = lw4.g(this.o);
        float f2 = lw4.f(this.o);
        float a2 = lw4.a(this.o);
        if (!d()) {
            this.p.set(e2, g2, f2, a2);
            return false;
        }
        float[] fArr = this.o;
        float f3 = fArr[0];
        float f4 = fArr[1];
        float f5 = fArr[4];
        float f6 = fArr[5];
        float f7 = fArr[6];
        float f8 = fArr[7];
        if (fArr[7] < fArr[1]) {
            if (fArr[1] < fArr[3]) {
                f3 = fArr[6];
                f4 = fArr[7];
                f5 = fArr[2];
                f6 = fArr[3];
                f7 = fArr[4];
                f8 = fArr[5];
            } else {
                f3 = fArr[4];
                f4 = fArr[5];
                f5 = fArr[0];
                f6 = fArr[1];
                f7 = fArr[2];
                f8 = fArr[3];
            }
        } else if (fArr[1] > fArr[3]) {
            f3 = fArr[2];
            f4 = fArr[3];
            f5 = fArr[6];
            f6 = fArr[7];
            f7 = fArr[0];
            f8 = fArr[1];
        }
        float f9 = (f8 - f4) / (f7 - f3);
        float f10 = -1.0f / f9;
        float f11 = f4 - (f9 * f3);
        float f12 = f4 - (f3 * f10);
        float f13 = f6 - (f9 * f5);
        float f14 = f6 - (f5 * f10);
        float centerY = rectF.centerY() - rectF2.top;
        float centerX = rectF.centerX();
        float f15 = rectF2.left;
        float f16 = centerY / (centerX - f15);
        float f17 = -f16;
        float f18 = rectF2.top;
        float f19 = f18 - (f15 * f16);
        float f20 = rectF2.right;
        float f21 = f18 - (f17 * f20);
        float f22 = f9 - f16;
        float f23 = (f19 - f11) / f22;
        float max = Math.max(e2, f23 < f20 ? f23 : e2);
        float f24 = (f19 - f12) / (f10 - f16);
        if (f24 >= rectF2.right) {
            f24 = max;
        }
        float max2 = Math.max(max, f24);
        float f25 = f10 - f17;
        float f26 = (f21 - f14) / f25;
        if (f26 >= rectF2.right) {
            f26 = max2;
        }
        float max3 = Math.max(max2, f26);
        float f27 = (f21 - f12) / f25;
        if (f27 <= rectF2.left) {
            f27 = f2;
        }
        float min = Math.min(f2, f27);
        float f28 = (f21 - f13) / (f9 - f17);
        if (f28 <= rectF2.left) {
            f28 = min;
        }
        float min2 = Math.min(min, f28);
        float f29 = (f19 - f13) / f22;
        if (f29 <= rectF2.left) {
            f29 = min2;
        }
        float min3 = Math.min(min2, f29);
        float max4 = Math.max(g2, Math.max((f9 * max3) + f11, (f10 * min3) + f12));
        float min4 = Math.min(a2, Math.min((f10 * max3) + f14, (f9 * min3) + f13));
        RectF rectF3 = this.p;
        rectF3.left = max3;
        rectF3.top = max4;
        rectF3.right = min3;
        rectF3.bottom = min4;
        return true;
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        Paint paint = this.f;
        if (paint != null) {
            float strokeWidth = paint.getStrokeWidth();
            RectF f2 = this.c.f();
            float f3 = strokeWidth / 2.0f;
            f2.inset(f3, f3);
            if (this.D == CropImageView.c.RECTANGLE) {
                canvas.drawRect(f2, this.f);
            } else {
                canvas.drawOval(f2, this.f);
            }
        }
    }

    @DexIgnore
    public final void b(float f2, float f3) {
        if (this.x != null) {
            float f4 = this.w;
            RectF f5 = this.c.f();
            this.x.a(f5, f2, f3, this.p, this.q, this.r, a(f5) ? 0.0f : f4, this.y, this.B);
            this.c.a(f5);
            a(true);
            invalidate();
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        try {
            if (this.d != null) {
                this.d.a(z2);
            }
        } catch (Exception e2) {
            Log.e("AIC", "Exception in crop window changed", e2);
        }
    }
}
