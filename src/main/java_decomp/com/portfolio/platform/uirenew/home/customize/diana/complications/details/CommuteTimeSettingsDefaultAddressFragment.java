package com.portfolio.platform.uirenew.home.customize.diana.complications.details;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.c55;
import com.fossil.cu4;
import com.fossil.d55;
import com.fossil.f74;
import com.fossil.h34;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.yd6;
import com.fossil.yj6;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressFragment extends BaseFragment implements d55, h34.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<f74> f;
    @DexIgnore
    public c55 g;
    @DexIgnore
    public cu4 h;
    @DexIgnore
    public h34 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CommuteTimeSettingsDefaultAddressFragment a() {
            return new CommuteTimeSettingsDefaultAddressFragment();
        }

        @DexIgnore
        public final String b() {
            return CommuteTimeSettingsDefaultAddressFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements cu4.b {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;

        @DexIgnore
        public b(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "address");
            CommuteTimeSettingsDefaultAddressFragment.d(this.a).a(yj6.d(str).toString());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ f74 b;

        @DexIgnore
        public c(View view, f74 f74) {
            this.a = view;
            this.b = f74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.w.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                f74 f74 = this.b;
                wg6.a((Object) f74, "binding");
                f74.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = CommuteTimeSettingsDefaultAddressFragment.q.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                Object r0 = this.b.q;
                wg6.a((Object) r0, "binding.autocompletePlaces");
                r0.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;

        @DexIgnore
        public d(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment, f74 f74) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            h34 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText((CharacterStyle) null);
                wg6.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b = CommuteTimeSettingsFragment.q.b();
                local.i(b, "Autocomplete item selected: " + fullText);
                this.a.j = fullText.toString();
                if (this.a.j != null) {
                    String b2 = this.a.j;
                    if (b2 != null) {
                        if (b2.length() > 0) {
                            c55 d = CommuteTimeSettingsDefaultAddressFragment.d(this.a);
                            String b3 = this.a.j;
                            if (b3 == null) {
                                wg6.a();
                                throw null;
                            } else if (b3 != null) {
                                d.a(yj6.d(b3).toString());
                            } else {
                                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                            }
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ f74 a;

        @DexIgnore
        public e(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment, f74 f74) {
            this.a = f74;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.a.r;
            wg6.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;

        @DexIgnore
        public f(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment, f74 f74) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wg6.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.i1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;

        @DexIgnore
        public g(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ f74 b;

        @DexIgnore
        public h(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment, f74 f74) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
            this.b = f74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onClick(View view) {
            this.a.j = "";
            this.b.q.setText("");
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressFragment a;

        @DexIgnore
        public i(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment) {
            this.a = commuteTimeSettingsDefaultAddressFragment;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r10v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onClick(View view) {
            String str;
            Object r10;
            Editable text;
            MapPickerActivity.a aVar = MapPickerActivity.B;
            CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment = this.a;
            f74 f74 = (f74) CommuteTimeSettingsDefaultAddressFragment.c(commuteTimeSettingsDefaultAddressFragment).a();
            if (f74 == null || (r10 = f74.q) == 0 || (text = r10.getText()) == null || (str = text.toString()) == null) {
                str = "";
            }
            MapPickerActivity.a.a(aVar, commuteTimeSettingsDefaultAddressFragment, 0.0d, 0.0d, str, 6, (Object) null);
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsDefaultAddressFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ ax5 c(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment) {
        ax5<f74> ax5 = commuteTimeSettingsDefaultAddressFragment.f;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ c55 d(CommuteTimeSettingsDefaultAddressFragment commuteTimeSettingsDefaultAddressFragment) {
        c55 c55 = commuteTimeSettingsDefaultAddressFragment.g;
        if (c55 != null) {
            return c55;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void E(String str) {
        if (str != null) {
            Intent intent = new Intent();
            intent.putExtra("KEY_DEFAULT_PLACE", str);
            FragmentActivity activity = getActivity();
            if (activity != null) {
                activity.setResult(-1, intent);
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    public void P(boolean z) {
        ax5<f74> ax5 = this.f;
        if (ax5 != null) {
            f74 a2 = ax5.a();
            if (a2 != null) {
                if (z) {
                    Object r3 = a2.q;
                    wg6.a((Object) r3, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(r3.getText())) {
                        this.j = null;
                        k1();
                        return;
                    }
                }
                j1();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void Z(String str) {
        wg6.b(str, "address");
        ax5<f74> ax5 = this.f;
        if (ax5 != null) {
            f74 a2 = ax5.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText(str, false);
                    this.j = str;
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        ax5<f74> ax5 = this.f;
        String str = null;
        if (ax5 == null) {
            wg6.d("mBinding");
            throw null;
        } else if (ax5.a() == null) {
            return true;
        } else {
            c55 c55 = this.g;
            if (c55 != null) {
                String str2 = this.j;
                if (str2 != null) {
                    if (str2 != null) {
                        str = yj6.d(str2).toString();
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                }
                c55.a(str);
                return true;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void j(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        wg6.b(list, "recentSearchedList");
        j1();
        if (!list.isEmpty()) {
            cu4 cu4 = this.h;
            if (cu4 != null) {
                cu4.a((List<String>) yd6.d(list));
            }
            ax5<f74> ax5 = this.f;
            if (ax5 != null) {
                f74 a2 = ax5.a();
                if (a2 != null && (linearLayout2 = a2.x) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        ax5<f74> ax52 = this.f;
        if (ax52 != null) {
            f74 a3 = ax52.a();
            if (a3 != null && (linearLayout = a3.x) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void j1() {
        ax5<f74> ax5 = this.f;
        if (ax5 != null) {
            f74 a2 = ax5.a();
            if (a2 != null) {
                LinearLayout linearLayout = a2.x;
                wg6.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                Object r0 = a2.s;
                wg6.a((Object) r0, "it.ftvAddressError");
                r0.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void k1() {
        ax5<f74> ax5 = this.f;
        if (ax5 != null) {
            f74 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.s;
                wg6.a((Object) r1, "it.ftvAddressError");
                Object instance = PortfolioApp.get.instance();
                Object r6 = a2.q;
                wg6.a((Object) r6, "it.autocompletePlaces");
                r1.setText(instance.getString(2131886193, new Object[]{r6.getText()}));
                Object r12 = a2.s;
                wg6.a((Object) r12, "it.ftvAddressError");
                r12.setVisibility(0);
                LinearLayout linearLayout = a2.x;
                wg6.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        CommuteTimeSettingsDefaultAddressFragment.super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            if (string != null) {
                Z(string);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r4v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        f74 a2 = kb.a(layoutInflater, 2131558515, viewGroup, false, e1());
        String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
        if (!TextUtils.isEmpty(b2)) {
            a2.w.setBackgroundColor(Color.parseColor(b2));
        }
        Object r4 = a2.q;
        r4.setDropDownBackgroundDrawable(w6.c(r4.getContext(), 2131230823));
        r4.setOnItemClickListener(new d(this, a2));
        r4.addTextChangedListener(new e(this, a2));
        r4.setOnKeyListener(new f(this, a2));
        a2.u.setOnClickListener(new g(this));
        a2.r.setOnClickListener(new h(this, a2));
        a2.v.setOnClickListener(new i(this));
        cu4 cu4 = new cu4();
        cu4.a((cu4.b) new b(this));
        this.h = cu4;
        RecyclerView recyclerView = a2.z;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new c(d2, a2));
        this.f = new ax5<>(this, a2);
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        c55 c55 = this.g;
        if (c55 != null) {
            c55.g();
            CommuteTimeSettingsDefaultAddressFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        CommuteTimeSettingsDefaultAddressFragment.super.onResume();
        c55 c55 = this.g;
        if (c55 != null) {
            c55.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: com.portfolio.platform.view.FlexibleTextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: android.widget.TextView} */
    /* JADX WARNING: Multi-variable type inference failed */
    public void setTitle(String str) {
        wg6.b(str, Explore.COLUMN_TITLE);
        ax5<f74> ax5 = this.f;
        if (ax5 != null) {
            f74 a2 = ax5.a();
            TextView textView = a2 != null ? a2.t : null;
            if (textView != null) {
                wg6.a((Object) textView, "mBinding.get()?.ftvTitle!!");
                textView.setText(str);
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r6v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void y(String str) {
        wg6.b(str, "address");
        if (isActive()) {
            ax5<f74> ax5 = this.f;
            if (ax5 != null) {
                f74 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.q;
                    wg6.a((Object) r1, "it.autocompletePlaces");
                    Editable text = r1.getText();
                    wg6.a((Object) text, "it.autocompletePlaces.text");
                    CharSequence d2 = yj6.d(text);
                    boolean z = true;
                    if (d2.length() > 0) {
                        a2.q.setText(d2);
                        return;
                    }
                    if (str.length() <= 0) {
                        z = false;
                    }
                    if (z) {
                        a2.q.setText(str);
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(c55 c55) {
        wg6.b(c55, "presenter");
        this.g = c55;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                this.i = new h34(context, placesClient);
                h34 h34 = this.i;
                if (h34 != null) {
                    h34.a((h34.b) this);
                }
                ax5<f74> ax5 = this.f;
                if (ax5 != null) {
                    f74 a2 = ax5.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.i);
                        Object r0 = a2.q;
                        wg6.a((Object) r0, "it.autocompletePlaces");
                        Editable text = r0.getText();
                        wg6.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = yj6.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        j1();
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
    }
}
