package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.e85;
import com.fossil.f85;
import com.fossil.h34;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.nh6;
import com.fossil.oh6;
import com.fossil.qg6;
import com.fossil.tu4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.ze4;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.CommuteTimeSettingsFragment;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingFragment extends BaseFragment implements f85, h34.b {
    @DexIgnore
    public static /* final */ String o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public ax5<ze4> f;
    @DexIgnore
    public e85 g;
    @DexIgnore
    public tu4 h;
    @DexIgnore
    public h34 i;
    @DexIgnore
    public HashMap j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final WeatherSettingFragment a() {
            return new WeatherSettingFragment();
        }

        @DexIgnore
        public final String b() {
            return WeatherSettingFragment.o;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ ze4 b;

        @DexIgnore
        public b(View view, ze4 ze4) {
            this.a = view;
            this.b = ze4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.u.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                ze4 ze4 = this.b;
                wg6.a((Object) ze4, "binding");
                ze4.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = CommuteTimeSettingsFragment.q.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                Object r0 = this.b.q;
                wg6.a((Object) r0, "binding.autocompletePlaces");
                r0.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ ze4 b;

        @DexIgnore
        public c(WeatherSettingFragment weatherSettingFragment, ze4 ze4) {
            this.a = weatherSettingFragment;
            this.b = ze4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r6v9, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            h34 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText((CharacterStyle) null);
                wg6.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = WeatherSettingFragment.p.b();
                local.i(b2, "Autocomplete item selected: " + fullText);
                if (this.a.h != null && !TextUtils.isEmpty(item.getPlaceId())) {
                    this.b.q.setText("");
                    e85 c = WeatherSettingFragment.c(this.a);
                    String spannableString = fullText.toString();
                    wg6.a((Object) spannableString, "primaryText.toString()");
                    String placeId = item.getPlaceId();
                    wg6.a((Object) placeId, "item.placeId");
                    h34 a3 = this.a.i;
                    if (a3 != null) {
                        c.a(spannableString, placeId, a3.b());
                        h34 a4 = this.a.i;
                        if (a4 != null) {
                            a4.a();
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ze4 a;

        @DexIgnore
        public d(WeatherSettingFragment weatherSettingFragment, ze4 ze4) {
            this.a = ze4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.a.s;
            wg6.a((Object) imageView, "binding.clearIv");
            imageView.setVisibility(TextUtils.isEmpty(charSequence) ? 8 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingFragment a;

        @DexIgnore
        public e(WeatherSettingFragment weatherSettingFragment, ze4 ze4) {
            this.a = weatherSettingFragment;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wg6.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.i1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements tu4.b {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingFragment a;

        @DexIgnore
        public f(WeatherSettingFragment weatherSettingFragment) {
            this.a = weatherSettingFragment;
        }

        @DexIgnore
        public void a(int i, boolean z) {
            WeatherSettingFragment.c(this.a).a(i, z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingFragment a;

        @DexIgnore
        public g(WeatherSettingFragment weatherSettingFragment) {
            this.a = weatherSettingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ze4 a;

        @DexIgnore
        public h(ze4 ze4) {
            this.a = ze4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = WeatherSettingFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "WeatherSettingFragment::class.java.simpleName");
        o = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ e85 c(WeatherSettingFragment weatherSettingFragment) {
        e85 e85 = weatherSettingFragment.g;
        if (e85 != null) {
            return e85;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void L0() {
        FLogger.INSTANCE.getLocal().d(o, "showLocationError");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.q(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    public void P(boolean z) {
        ax5<ze4> ax5 = this.f;
        if (ax5 != null) {
            ze4 a2 = ax5.a();
            if (a2 != null) {
                if (z) {
                    Object r2 = a2.q;
                    wg6.a((Object) r2, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(r2.getText())) {
                        k1();
                        return;
                    }
                }
                j1();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.j;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        ax5<ze4> ax5 = this.f;
        if (ax5 == null) {
            wg6.d("mBinding");
            throw null;
        } else if (ax5.a() == null || this.h == null) {
            return true;
        } else {
            e85 e85 = this.g;
            if (e85 != null) {
                e85.i();
                return true;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void j1() {
        ax5<ze4> ax5 = this.f;
        if (ax5 != null) {
            ze4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.t;
                wg6.a((Object) r1, "it.ftvLocationError");
                r1.setVisibility(8);
                Object r12 = a2.x;
                wg6.a((Object) r12, "it.tvAdded");
                r12.setVisibility(0);
                RecyclerView recyclerView = a2.v;
                wg6.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void k1() {
        ax5<ze4> ax5 = this.f;
        if (ax5 != null) {
            ze4 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.t;
                wg6.a((Object) r1, "it.ftvLocationError");
                nh6 nh6 = nh6.a;
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886579);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026xt__NothingFoundForInput)");
                Object r5 = a2.q;
                wg6.a((Object) r5, "it.autocompletePlaces");
                Object[] objArr = {r5.getText()};
                String format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r1.setText(format);
                Object r12 = a2.t;
                wg6.a((Object) r12, "it.ftvLocationError");
                r12.setVisibility(0);
                Object r13 = a2.x;
                wg6.a((Object) r13, "it.tvAdded");
                r13.setVisibility(8);
                RecyclerView recyclerView = a2.v;
                wg6.a((Object) recyclerView, "it.recyclerView");
                recyclerView.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void l0() {
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.r(childFragmentManager);
        }
    }

    @DexIgnore
    public void o(List<WeatherLocationWrapper> list) {
        wg6.b(list, "locations");
        tu4 tu4 = this.h;
        if (tu4 != null) {
            tu4.a((List<WeatherLocationWrapper>) oh6.c(list));
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r4v7, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ze4 a2 = kb.a(layoutInflater, 2131558626, viewGroup, false, e1());
        Object r4 = a2.q;
        r4.setDropDownBackgroundDrawable(w6.c(r4.getContext(), 2131230823));
        r4.setOnItemClickListener(new c(this, a2));
        r4.addTextChangedListener(new d(this, a2));
        r4.setOnKeyListener(new e(this, a2));
        this.h = new tu4();
        tu4 tu4 = this.h;
        if (tu4 != null) {
            tu4.a((tu4.b) new f(this));
        }
        RecyclerView recyclerView = a2.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, a2));
        a2.r.setOnClickListener(new g(this));
        a2.s.setOnClickListener(new h(a2));
        this.f = new ax5<>(this, a2);
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        e85 e85 = this.g;
        if (e85 != null) {
            e85.g();
            WeatherSettingFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        WeatherSettingFragment.super.onResume();
        e85 e85 = this.g;
        if (e85 != null) {
            e85.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void u(boolean z) {
        if (z) {
            Intent intent = new Intent();
            e85 e85 = this.g;
            if (e85 != null) {
                intent.putExtra("WEATHER_WATCH_APP_SETTING", e85.h());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    public void a(e85 e85) {
        wg6.b(e85, "presenter");
        this.g = e85;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void a(PlacesClient placesClient) {
        Object r5;
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                this.i = new h34(context, placesClient);
                h34 h34 = this.i;
                if (h34 != null) {
                    h34.a((h34.b) this);
                }
                ax5<ze4> ax5 = this.f;
                if (ax5 != null) {
                    ze4 a2 = ax5.a();
                    if (a2 != null && (r5 = a2.q) != 0) {
                        r5.setAdapter(this.i);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
    }
}
