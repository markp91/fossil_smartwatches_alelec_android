package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages;

import android.content.Context;
import android.content.Intent;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bz4;
import com.fossil.cd6;
import com.fossil.d15;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.jy4;
import com.fossil.ky4;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.oy4$d$a;
import com.fossil.oy4$e$a;
import com.fossil.oy4$e$b;
import com.fossil.oy4$f$a;
import com.fossil.oy4$f$b;
import com.fossil.oy4$h$a;
import com.fossil.oy4$h$b;
import com.fossil.oy4$h$c;
import com.fossil.oy4$h$d;
import com.fossil.oz4;
import com.fossil.pz4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rl6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.uh4;
import com.fossil.vx4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wg6;
import com.fossil.wx4;
import com.fossil.xe6;
import com.fossil.xm4;
import com.fossil.y24;
import com.fossil.z24;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDao;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationCallsAndMessagesPresenter extends jy4 {
    @DexIgnore
    public static /* final */ String D;
    @DexIgnore
    public static /* final */ a E; // = new a((qg6) null);
    @DexIgnore
    public /* final */ SetReplyMessageMappingUseCase A;
    @DexIgnore
    public /* final */ QuickResponseRepository B;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase C;
    @DexIgnore
    public List<AppNotificationFilter> e; // = new ArrayList();
    @DexIgnore
    public List<vx4> f; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> g; // = new ArrayList();
    @DexIgnore
    public /* final */ b h; // = new b();
    @DexIgnore
    public volatile boolean i;
    @DexIgnore
    public int j;
    @DexIgnore
    public boolean k; // = true;
    @DexIgnore
    public int l;
    @DexIgnore
    public boolean m; // = true;
    @DexIgnore
    public boolean n; // = true;
    @DexIgnore
    public /* final */ List<wx4> o; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> p; // = new ArrayList();
    @DexIgnore
    public /* final */ LiveData<List<NotificationSettingsModel>> q; // = this.z.getListNotificationSettings();
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public bz4 s;
    @DexIgnore
    public /* final */ ky4 t;
    @DexIgnore
    public /* final */ z24 u;
    @DexIgnore
    public /* final */ mz4 v;
    @DexIgnore
    public /* final */ oz4 w;
    @DexIgnore
    public /* final */ d15 x;
    @DexIgnore
    public /* final */ an4 y;
    @DexIgnore
    public /* final */ NotificationSettingsDao z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationCallsAndMessagesPresenter.D;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.E.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode + " isSettingRules " + NotificationCallsAndMessagesPresenter.this.i);
            if (communicateMode == CommunicateMode.SET_NOTIFICATION_FILTERS && NotificationCallsAndMessagesPresenter.this.i) {
                NotificationCallsAndMessagesPresenter.this.i = false;
                NotificationCallsAndMessagesPresenter.this.t.a();
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "onReceive - success");
                    NotificationCallsAndMessagesPresenter.this.t.close();
                    return;
                }
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "onReceive - failed");
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = NotificationCallsAndMessagesPresenter.E.a();
                local2.d(a3, "mAllowMessagesFromFirsLoad=" + NotificationCallsAndMessagesPresenter.this.m() + " mAlowCallsFromFirstLoad=" + NotificationCallsAndMessagesPresenter.this.n() + " mListFavoriteContactFirstLoad=" + NotificationCallsAndMessagesPresenter.this.q() + " size=" + NotificationCallsAndMessagesPresenter.this.q().size());
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", NotificationCallsAndMessagesPresenter.this.n(), true);
                NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", NotificationCallsAndMessagesPresenter.this.m(), false);
                arrayList.add(notificationSettingsModel);
                arrayList.add(notificationSettingsModel2);
                NotificationCallsAndMessagesPresenter.this.a((List<NotificationSettingsModel>) arrayList);
                int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra2);
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationCallsAndMessagesPresenter.E.a();
                local3.d(a4, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
                int size = integerArrayListExtra.size();
                for (int i = 0; i < size; i++) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a5 = NotificationCallsAndMessagesPresenter.E.a();
                    local4.d(a5, "error code " + i + " =" + integerArrayListExtra.get(i));
                }
                if (intExtra2 == 1101 || intExtra2 == 1112 || intExtra2 == 1113) {
                    List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(integerArrayListExtra);
                    wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
                    ky4 o = NotificationCallsAndMessagesPresenter.this.t;
                    Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
                    if (array != null) {
                        uh4[] uh4Arr = (uh4[]) array;
                        o.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
                NotificationCallsAndMessagesPresenter.this.t.c();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements y24.d<oz4.c, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ ContactGroup b;

        @DexIgnore
        public c(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, ContactGroup contactGroup) {
            this.a = notificationCallsAndMessagesPresenter;
            this.b = contactGroup;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(oz4.c cVar) {
            wg6.b(cVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), ".Inside mRemoveContactGroup onSuccess");
            this.a.t.a(this.b);
        }

        @DexIgnore
        public void a(y24.a aVar) {
            wg6.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), ".Inside mRemoveContactGroup onError");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$saveNotificationSettings$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {486}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $settings;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
            this.$settings = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$settings, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                oy4$d$a oy4_d_a = new oy4$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, oy4_d_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setReplyMessageToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {414}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r6v3, types: [com.fossil.oy4$e$a, com.portfolio.platform.CoroutineUseCase$e] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                this.this$0.t.b();
                dl6 b = this.this$0.c();
                oy4$e$b oy4_e_b = new oy4$e$b(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, oy4_e_b, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.A.a(new SetReplyMessageMappingUseCase.b((List) obj), new oy4$e$a(this));
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {389, 391}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0181  */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x01f9  */
        public final Object invokeSuspend(Object obj) {
            long j;
            Object obj2;
            List list;
            Object obj3;
            il6 il6;
            List list2;
            rl6 rl6;
            rl6 rl62;
            List list3;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il62 = this.p$;
                this.this$0.t.b();
                this.this$0.t.Z();
                list2 = new ArrayList();
                if (this.this$0.t.G() != this.this$0.n()) {
                    list2.add(new NotificationSettingsModel("AllowCallsFrom", this.this$0.t.G(), true));
                }
                if (this.this$0.t.M() != this.this$0.m()) {
                    list2.add(new NotificationSettingsModel("AllowMessagesFrom", this.this$0.t.M(), false));
                }
                if (!list2.isEmpty()) {
                    this.this$0.a((List<NotificationSettingsModel>) list2);
                }
                long currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = NotificationCallsAndMessagesPresenter.E.a();
                local.d(a2, "filter notification, time start=" + currentTimeMillis);
                this.this$0.e.clear();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String a3 = NotificationCallsAndMessagesPresenter.E.a();
                local2.d(a3, "mListAppWrapper.size = " + this.this$0.p().size());
                il6 il63 = il62;
                rl6 a4 = ik6.a(il63, this.this$0.b(), (ll6) null, new oy4$f$b(this, (xe6) null), 2, (Object) null);
                rl6 a5 = ik6.a(il63, this.this$0.b(), (ll6) null, new oy4$f$a(this, (xe6) null), 2, (Object) null);
                List i2 = this.this$0.e;
                this.L$0 = il62;
                this.L$1 = list2;
                this.J$0 = currentTimeMillis;
                this.L$2 = a4;
                this.L$3 = a5;
                this.L$4 = i2;
                this.label = 1;
                obj3 = a4.a(this);
                if (obj3 == a) {
                    return a;
                }
                List list4 = i2;
                il6 = il62;
                list3 = list4;
                long j2 = currentTimeMillis;
                rl62 = a5;
                rl6 = a4;
                j = j2;
            } else if (i == 1) {
                list3 = (List) this.L$4;
                rl62 = (rl6) this.L$3;
                rl6 = (rl6) this.L$2;
                j = this.J$0;
                list2 = (List) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = obj;
            } else if (i == 2) {
                rl6 rl63 = (rl6) this.L$3;
                rl6 rl64 = (rl6) this.L$2;
                long j3 = this.J$0;
                List list5 = (List) this.L$1;
                il6 il64 = (il6) this.L$0;
                nc6.a(obj);
                j = j3;
                obj2 = obj;
                list = (List) obj2;
                if (list == null) {
                    this.this$0.e.addAll(list);
                    long b = PortfolioApp.get.instance().b(new AppNotificationFilterSettings(this.this$0.e, System.currentTimeMillis()), PortfolioApp.get.instance().e());
                    this.this$0.i = true;
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String a6 = NotificationCallsAndMessagesPresenter.E.a();
                    local3.d(a6, "filter notification, time end= " + b);
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String a7 = NotificationCallsAndMessagesPresenter.E.a();
                    local4.d(a7, "delayTime =" + (b - j) + " mili seconds");
                } else {
                    this.this$0.t.a();
                    this.this$0.t.Q();
                    this.this$0.t.close();
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            list3.addAll((Collection) obj3);
            this.L$0 = il6;
            this.L$1 = list2;
            this.J$0 = j;
            this.L$2 = rl6;
            this.L$3 = rl62;
            this.label = 2;
            obj2 = rl62.a(this);
            if (obj2 == a) {
                return a;
            }
            list = (List) obj2;
            if (list == null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements ld<bz4.a> {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter a;

        @DexIgnore
        public g(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter) {
            this.a = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(bz4.a aVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.E.a();
            local.d(a2, "NotificationSettingChanged value = " + aVar);
            String a3 = this.a.a(aVar.a());
            if (aVar.b()) {
                this.a.t.p(a3);
            } else {
                this.a.t.i(a3);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$start$2", f = "NotificationCallsAndMessagesPresenter.kt", l = {158, 165}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(NotificationCallsAndMessagesPresenter notificationCallsAndMessagesPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationCallsAndMessagesPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r8v2, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.oy4$h$b, com.portfolio.platform.CoroutineUseCase$e] */
        /* JADX WARNING: type inference failed for: r8v4, types: [com.fossil.d15, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.oy4$h$c] */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x007e  */
        /* JADX WARNING: Removed duplicated region for block: B:21:0x00b6  */
        public final Object invokeSuspend(Object obj) {
            List<NotificationSettingsModel> list;
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                if (!PortfolioApp.get.instance().w().P()) {
                    dl6 a2 = this.this$0.b();
                    oy4$h$a oy4_h_a = new oy4$h$a((xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    if (gk6.a(a2, oy4_h_a, this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                list = (List) obj;
                if (!list.isEmpty()) {
                    ArrayList arrayList = new ArrayList();
                    NotificationSettingsModel notificationSettingsModel = new NotificationSettingsModel("AllowCallsFrom", 0, true);
                    NotificationSettingsModel notificationSettingsModel2 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
                    arrayList.add(notificationSettingsModel);
                    arrayList.add(notificationSettingsModel2);
                    this.this$0.a((List<NotificationSettingsModel>) arrayList);
                    String a3 = this.this$0.a(0);
                    this.this$0.t.p(a3);
                    this.this$0.t.i(a3);
                } else {
                    for (NotificationSettingsModel notificationSettingsModel3 : list) {
                        int component2 = notificationSettingsModel3.component2();
                        if (notificationSettingsModel3.component3()) {
                            String a4 = this.this$0.a(component2);
                            if (this.this$0.k) {
                                this.this$0.c(component2);
                                this.this$0.k = false;
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String a5 = NotificationCallsAndMessagesPresenter.E.a();
                            local.d(a5, "start, mAlowCallsFromFirstLoad=" + this.this$0.n());
                            this.this$0.t.p(a4);
                        } else {
                            String a6 = this.this$0.a(component2);
                            if (this.this$0.m) {
                                this.this$0.b(component2);
                                this.this$0.m = false;
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a7 = NotificationCallsAndMessagesPresenter.E.a();
                            local2.d(a7, "start, mAllowMessagesFromFirsLoad=" + this.this$0.m());
                            this.this$0.t.i(a6);
                        }
                    }
                }
                this.this$0.v.a(null, new oy4$h$b(this));
                this.this$0.x.a(null, new oy4$h$c(this));
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            if (this.this$0.r) {
                this.this$0.r = false;
                dl6 b = this.this$0.c();
                oy4$h$d oy4_h_d = new oy4$h$d(this, (xe6) null);
                this.L$0 = il6;
                this.label = 2;
                obj = gk6.a(b, oy4_h_d, this);
                if (obj == a) {
                    return a;
                }
                list = (List) obj;
                if (!list.isEmpty()) {
                }
            }
            this.this$0.v.a(null, new oy4$h$b(this));
            this.this$0.x.a(null, new oy4$h$c(this));
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = NotificationCallsAndMessagesPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationCallsAndMess\u2026er::class.java.simpleName");
        D = simpleName;
    }
    */

    @DexIgnore
    public NotificationCallsAndMessagesPresenter(ky4 ky4, z24 z24, mz4 mz4, oz4 oz4, pz4 pz4, d15 d15, an4 an4, NotificationSettingsDao notificationSettingsDao, SetReplyMessageMappingUseCase setReplyMessageMappingUseCase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        wg6.b(ky4, "mView");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(mz4, "mGetAllContactGroup");
        wg6.b(oz4, "mRemoveContactGroup");
        wg6.b(pz4, "mSaveContactGroupsNotification");
        wg6.b(d15, "mGetApps");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(notificationSettingsDao, "mNotificationSettingDao");
        wg6.b(setReplyMessageMappingUseCase, "mReplyMessageUseCase");
        wg6.b(quickResponseRepository, "mQRRepository");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.t = ky4;
        this.u = z24;
        this.v = mz4;
        this.w = oz4;
        this.x = d15;
        this.y = an4;
        this.z = notificationSettingsDao;
        this.A = setReplyMessageMappingUseCase;
        this.B = quickResponseRepository;
        this.C = notificationSettingsDatabase;
    }

    @DexIgnore
    public final List<vx4> p() {
        return this.f;
    }

    @DexIgnore
    public final List<wx4> q() {
        return this.o;
    }

    @DexIgnore
    public final boolean r() {
        return (this.t.G() == this.j && this.t.M() == this.l && !(wg6.a((Object) this.p, (Object) this.t.W()) ^ true)) ? false : true;
    }

    @DexIgnore
    public final void s() {
        FLogger.INSTANCE.getLocal().d(D, "registerBroadcastReceiver");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void t() {
        this.t.a(this);
    }

    @DexIgnore
    public final void u() {
        FLogger.INSTANCE.getLocal().d(D, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.h, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(D, "start");
        s();
        BleCommandResultManager.d.a(CommunicateMode.SET_NOTIFICATION_FILTERS);
        ky4 ky4 = this.t;
        if (ky4 != null) {
            FragmentActivity activity = ((NotificationCallsAndMessagesFragment) ky4).getActivity();
            bz4 bz4 = this.s;
            if (bz4 != null) {
                bz4.a().a(this.t, new g(this));
                xm4 xm4 = xm4.d;
                if (activity == null) {
                    wg6.a();
                    throw null;
                } else if (xm4.a(xm4, activity, "NOTIFICATION_CONTACTS", false, true, false, 20, (Object) null) && xm4.a(xm4.d, activity, "NOTIFICATION_APPS", false, false, false, 28, (Object) null)) {
                    rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
                }
            } else {
                wg6.d("mNotificationSettingViewModel");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        }
    }

    @DexIgnore
    public void g() {
        u();
        LiveData<List<NotificationSettingsModel>> liveData = this.q;
        ky4 ky4 = this.t;
        if (ky4 != null) {
            liveData.a((NotificationCallsAndMessagesFragment) ky4);
            bz4 bz4 = this.s;
            if (bz4 != null) {
                bz4.a().a(this.t);
                FLogger.INSTANCE.getLocal().d(D, "stop");
                return;
            }
            wg6.d("mNotificationSettingViewModel");
            throw null;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
    }

    @DexIgnore
    public boolean h() {
        Boolean O = this.y.O();
        wg6.a((Object) O, "mSharedPreferencesManager.isQuickResponseEnabled");
        return O.booleanValue();
    }

    @DexIgnore
    public void i() {
        this.t.a();
        this.t.close();
    }

    @DexIgnore
    public void j() {
        this.y.a(PortfolioApp.get.instance().e(), 0, false);
    }

    @DexIgnore
    public void k() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void l() {
        if (!r()) {
            FLogger.INSTANCE.getLocal().d(D, "setRuleToDevice, nothing changed");
            this.t.close();
            return;
        }
        xm4 xm4 = xm4.d;
        ky4 ky4 = this.t;
        if (ky4 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesFragment");
        } else if (xm4.a(xm4, ((NotificationCallsAndMessagesFragment) ky4).getContext(), "SET_NOTIFICATION", false, false, true, 12, (Object) null)) {
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
        }
    }

    @DexIgnore
    public final int m() {
        return this.l;
    }

    @DexIgnore
    public final int n() {
        return this.j;
    }

    @DexIgnore
    public final boolean o() {
        return this.n;
    }

    @DexIgnore
    public final void c(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public final void b(int i2) {
        this.l = i2;
    }

    @DexIgnore
    public final void c(boolean z2) {
        this.n = z2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886089);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886091);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886090);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    public final void b(List<ContactGroup> list) {
        wg6.b(list, "<set-?>");
        this.p = list;
    }

    @DexIgnore
    public void b(boolean z2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = D;
        local.d(str, "updateNotificationSetting isCall=" + z2);
        ky4 ky4 = this.t;
        bz4.a aVar = new bz4.a(z2 ? ky4.G() : ky4.M(), z2);
        bz4 bz4 = this.s;
        if (bz4 != null) {
            bz4.a().a(aVar);
            this.t.V();
            return;
        }
        wg6.d("mNotificationSettingViewModel");
        throw null;
    }

    @DexIgnore
    public void a(ContactGroup contactGroup) {
        wg6.b(contactGroup, "contactGroup");
        this.u.a(this.w, new oz4.b(contactGroup), new c(this, contactGroup));
    }

    @DexIgnore
    public void a(boolean z2) {
        this.y.b(Boolean.valueOf(z2));
    }

    @DexIgnore
    public void a(bz4 bz4) {
        wg6.b(bz4, "viewModel");
        this.s = bz4;
    }

    @DexIgnore
    public final void a(List<NotificationSettingsModel> list) {
        FLogger.INSTANCE.getLocal().d(D, "saveNotificationSettings");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, list, (xe6) null), 3, (Object) null);
    }
}
