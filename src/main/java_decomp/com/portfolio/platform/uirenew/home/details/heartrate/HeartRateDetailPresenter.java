package com.portfolio.platform.uirenew.home.details.heartrate;

import android.os.Bundle;
import android.util.Pair;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.aj6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gg6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.hg6;
import com.fossil.hh6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jz5;
import com.fossil.lc6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nh6;
import com.fossil.oi5;
import com.fossil.pc6;
import com.fossil.pi5;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rd6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.si5$e$a;
import com.fossil.si5$f$a;
import com.fossil.si5$g$a;
import com.fossil.si5$g$b;
import com.fossil.si5$g$c;
import com.fossil.si5$g$d;
import com.fossil.si5$h$a;
import com.fossil.si5$h$b;
import com.fossil.si5$h$c;
import com.fossil.ti6;
import com.fossil.u04;
import com.fossil.v3;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.yd6;
import com.fossil.yi4;
import com.fossil.yx5;
import com.fossil.zh4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateDetailPresenter extends oi5 implements vk4.a {
    @DexIgnore
    public Date e;
    @DexIgnore
    public Date f; // = new Date();
    @DexIgnore
    public MutableLiveData<lc6<Date, Date>> g; // = new MutableLiveData<>();
    @DexIgnore
    public List<DailyHeartRateSummary> h; // = new ArrayList();
    @DexIgnore
    public List<HeartRateSample> i; // = new ArrayList();
    @DexIgnore
    public DailyHeartRateSummary j;
    @DexIgnore
    public List<HeartRateSample> k;
    @DexIgnore
    public zh4 l; // = zh4.METRIC;
    @DexIgnore
    public LiveData<yx5<List<DailyHeartRateSummary>>> m;
    @DexIgnore
    public LiveData<yx5<List<HeartRateSample>>> n;
    @DexIgnore
    public Listing<WorkoutSession> o;
    @DexIgnore
    public /* final */ pi5 p;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository q;
    @DexIgnore
    public /* final */ HeartRateSampleRepository r;
    @DexIgnore
    public /* final */ UserRepository s;
    @DexIgnore
    public /* final */ WorkoutSessionRepository t;
    @DexIgnore
    public /* final */ FitnessDataDao u;
    @DexIgnore
    public /* final */ WorkoutDao v;
    @DexIgnore
    public /* final */ FitnessDatabase w;
    @DexIgnore
    public /* final */ u04 x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<HeartRateSample, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Date date) {
            super(1);
            this.$date = date;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke((HeartRateSample) obj));
        }

        @DexIgnore
        public final boolean invoke(HeartRateSample heartRateSample) {
            wg6.b(heartRateSample, "it");
            return bk4.d(heartRateSample.getDate(), this.$date);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<cf<WorkoutSession>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public c(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(cf<WorkoutSession> cfVar) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HeartRateDetailPresenter", "getWorkoutSessionsPaging observed size = " + cfVar.size());
            if (DeviceHelper.o.g(PortfolioApp.get.instance().e())) {
                wg6.a((Object) cfVar, "pageList");
                if (yd6.d(cfVar).isEmpty()) {
                    this.a.p.a(false, this.a.l, cfVar);
                    return;
                }
            }
            pi5 m = this.a.p;
            zh4 c = this.a.l;
            wg6.a((Object) cfVar, "pageList");
            m.a(true, c, cfVar);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public d(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<HeartRateSample>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.r.getHeartRateSamples((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$setDate$1", f = "HeartRateDetailPresenter.kt", l = {143}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Date $date;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(HeartRateDetailPresenter heartRateDetailPresenter, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
            this.$date = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$date, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:19:0x011b  */
        /* JADX WARNING: Removed duplicated region for block: B:20:0x012f  */
        public final Object invokeSuspend(Object obj) {
            Pair<Date, Date> a;
            lc6 lc6;
            HeartRateDetailPresenter heartRateDetailPresenter;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                if (this.this$0.e == null) {
                    HeartRateDetailPresenter heartRateDetailPresenter2 = this.this$0;
                    dl6 a3 = heartRateDetailPresenter2.b();
                    si5$e$a si5_e_a = new si5$e$a((xe6) null);
                    this.L$0 = il6;
                    this.L$1 = heartRateDetailPresenter2;
                    this.label = 1;
                    obj = gk6.a(a3, si5_e_a, this);
                    if (obj == a2) {
                        return a2;
                    }
                    heartRateDetailPresenter = heartRateDetailPresenter2;
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
                this.this$0.f = this.$date;
                Boolean t = bk4.t(this.$date);
                pi5 m = this.this$0.p;
                Date date = this.$date;
                wg6.a((Object) t, "isToday");
                m.a(date, bk4.c(this.this$0.e, this.$date), t.booleanValue(), !bk4.c(new Date(), this.$date));
                a = bk4.a(this.$date, this.this$0.e);
                wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
                lc6 = (lc6) this.this$0.g.a();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
                if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
                    this.this$0.g.a(new lc6(a.first, a.second));
                } else {
                    rm6 unused = this.this$0.l();
                    rm6 unused2 = this.this$0.m();
                    HeartRateDetailPresenter heartRateDetailPresenter3 = this.this$0;
                    heartRateDetailPresenter3.c(heartRateDetailPresenter3.f);
                }
                return cd6.a;
            } else if (i == 1) {
                heartRateDetailPresenter = (HeartRateDetailPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            heartRateDetailPresenter.e = (Date) obj;
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            local3.d("HeartRateDetailPresenter", "setDate - date=" + this.$date + ", createdAt=" + this.this$0.e);
            this.this$0.f = this.$date;
            Boolean t2 = bk4.t(this.$date);
            pi5 m2 = this.this$0.p;
            Date date2 = this.$date;
            wg6.a((Object) t2, "isToday");
            m2.a(date2, bk4.c(this.this$0.e, this.$date), t2.booleanValue(), !bk4.c(new Date(), this.$date));
            a = bk4.a(this.$date, this.this$0.e);
            wg6.a((Object) a, "DateHelper.getLimitWeekR\u2026(date, mUserRegisterDate)");
            lc6 = (lc6) this.this$0.g.a();
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            local22.d("HeartRateDetailPresenter", "setDate - rangeDateValue=" + lc6 + ", newRange=" + new lc6(a.first, a.second));
            if (lc6 == null || !bk4.d((Date) lc6.getFirst(), (Date) a.first) || !bk4.d((Date) lc6.getSecond(), (Date) a.second)) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDayDetail$1", f = "HeartRateDetailPresenter.kt", l = {192}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(HeartRateDetailPresenter heartRateDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0062, code lost:
            r6 = com.fossil.hf6.a((r6 = r6.getResting()).getValue());
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Integer a;
            Resting resting;
            Integer a2;
            Object a3 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a4 = this.this$0.b();
                si5$f$a si5_f_a = new si5$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a4, si5_f_a, this);
                if (obj == a3) {
                    return a3;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
            if (this.this$0.j == null || (!wg6.a((Object) this.this$0.j, (Object) dailyHeartRateSummary))) {
                this.this$0.j = dailyHeartRateSummary;
                DailyHeartRateSummary h = this.this$0.j;
                int i2 = 0;
                int intValue = (h == null || resting == null || a2 == null) ? 0 : a2.intValue();
                DailyHeartRateSummary h2 = this.this$0.j;
                if (!(h2 == null || (a = hf6.a(h2.getMax())) == null)) {
                    i2 = a.intValue();
                }
                this.this$0.p.c(intValue, i2);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$showDetailChart$1", f = "HeartRateDetailPresenter.kt", l = {205, 209, 218, 225}, m = "invokeSuspend")
    public static final class g extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int I$0;
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(HeartRateDetailPresenter heartRateDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            g gVar = new g(this.this$0, xe6);
            gVar.p$ = (il6) obj;
            return gVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((g) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:103:0x03ba  */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d8  */
        /* JADX WARNING: Removed duplicated region for block: B:39:0x013d A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:40:0x013e  */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x019c A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x01c9  */
        public final Object invokeSuspend(Object obj) {
            ArrayList arrayList;
            List list;
            int i;
            int i2;
            long j;
            il6 il6;
            List list2;
            Object obj2;
            si5$g$b si5_g_b;
            dl6 a;
            il6 il62;
            List list3;
            long j2;
            Object obj3;
            il6 il63;
            Object a2 = ff6.a();
            int i3 = this.label;
            if (i3 == 0) {
                nc6.a(obj);
                il63 = this.p$;
                FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "showDetailChart");
                dl6 a3 = this.this$0.b();
                si5$g$d si5_g_d = new si5$g$d(this, (xe6) null);
                this.L$0 = il63;
                this.label = 1;
                obj3 = gk6.a(a3, si5_g_d, this);
                if (obj3 == a2) {
                    return a2;
                }
            } else if (i3 == 1) {
                il63 = (il6) this.L$0;
                nc6.a(obj);
                obj3 = obj;
            } else if (i3 == 2) {
                list3 = (List) this.L$1;
                nc6.a(obj);
                il62 = (il6) this.L$0;
                ArrayList arrayList2 = new ArrayList();
                if (this.this$0.k != null) {
                    List f = this.this$0.k;
                    if (f == null) {
                        wg6.a();
                        throw null;
                    } else if (!f.isEmpty()) {
                        List f2 = this.this$0.k;
                        if (f2 != null) {
                            DateTime withTimeAtStartOfDay = ((HeartRateSample) f2.get(0)).getStartTimeId().withTimeAtStartOfDay();
                            wg6.a((Object) withTimeAtStartOfDay, "mHeartRateSamplesOfDate!\u2026().withTimeAtStartOfDay()");
                            j2 = withTimeAtStartOfDay.getMillis();
                            dl6 a4 = this.this$0.b();
                            si5$g$c si5_g_c = new si5$g$c(this, (xe6) null);
                            this.L$0 = il62;
                            this.L$1 = list3;
                            this.L$2 = arrayList2;
                            this.J$0 = j2;
                            this.label = 3;
                            obj2 = gk6.a(a4, si5_g_c, this);
                            if (obj2 == a2) {
                                return a2;
                            }
                            arrayList = arrayList2;
                            j = j2;
                            list2 = list3;
                            il6 = il62;
                            int intValue = ((Number) obj2).intValue();
                            hh6 hh6 = new hh6();
                            hh6.element = -1;
                            ArrayList arrayList3 = new ArrayList();
                            si5_g_b = r0;
                            a = this.this$0.b();
                            hh6 hh62 = hh6;
                            ArrayList arrayList4 = arrayList3;
                            int i4 = intValue;
                            si5$g$b si5_g_b2 = new si5$g$b(this, j, hh62, arrayList4, arrayList, i4, (xe6) null);
                            this.L$0 = il6;
                            this.L$1 = list2;
                            this.L$2 = arrayList;
                            this.J$0 = j;
                            this.I$0 = i4;
                            this.L$3 = hh62;
                            list = arrayList4;
                            this.L$4 = list;
                            this.label = 4;
                            if (gk6.a(a, si5_g_b, this) == a2) {
                            }
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d("HeartRateDetailPresenter", "listTimeZoneChange=" + yi4.a(list));
                            if (!(!list.isEmpty()) || list.size() <= 1) {
                            }
                            int i5 = i + DateTimeConstants.MINUTES_PER_DAY;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            local2.d("HeartRateDetailPresenter", "minutesOfDay=" + i5);
                            this.this$0.p.a(i5, (List<jz5>) arrayList, (List<pc6<Integer, lc6<Integer, Float>, String>>) list);
                            return cd6.a;
                        }
                        wg6.a();
                        throw null;
                    }
                }
                Date o = bk4.o(this.this$0.f);
                wg6.a((Object) o, "DateHelper.getStartOfDay(mDate)");
                j2 = o.getTime();
                dl6 a42 = this.this$0.b();
                si5$g$c si5_g_c2 = new si5$g$c(this, (xe6) null);
                this.L$0 = il62;
                this.L$1 = list3;
                this.L$2 = arrayList2;
                this.J$0 = j2;
                this.label = 3;
                obj2 = gk6.a(a42, si5_g_c2, this);
                if (obj2 == a2) {
                }
            } else if (i3 == 3) {
                long j3 = this.J$0;
                nc6.a(obj);
                j = j3;
                arrayList = (List) this.L$2;
                list2 = (List) this.L$1;
                il6 = (il6) this.L$0;
                obj2 = obj;
                int intValue2 = ((Number) obj2).intValue();
                hh6 hh63 = new hh6();
                hh63.element = -1;
                ArrayList arrayList32 = new ArrayList();
                si5_g_b = si5_g_b2;
                a = this.this$0.b();
                hh6 hh622 = hh63;
                ArrayList arrayList42 = arrayList32;
                int i42 = intValue2;
                si5$g$b si5_g_b22 = new si5$g$b(this, j, hh622, arrayList42, arrayList, i42, (xe6) null);
                this.L$0 = il6;
                this.L$1 = list2;
                this.L$2 = arrayList;
                this.J$0 = j;
                this.I$0 = i42;
                this.L$3 = hh622;
                list = arrayList42;
                this.L$4 = list;
                this.label = 4;
                if (gk6.a(a, si5_g_b, this) == a2) {
                    return a2;
                }
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("HeartRateDetailPresenter", "listTimeZoneChange=" + yi4.a(list));
                if (!(!list.isEmpty()) || list.size() <= 1) {
                }
                int i52 = i + DateTimeConstants.MINUTES_PER_DAY;
                ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                local22.d("HeartRateDetailPresenter", "minutesOfDay=" + i52);
                this.this$0.p.a(i52, (List<jz5>) arrayList, (List<pc6<Integer, lc6<Integer, Float>, String>>) list);
                return cd6.a;
            } else if (i3 == 4) {
                list = (List) this.L$4;
                hh6 hh64 = (hh6) this.L$3;
                List list4 = (List) this.L$1;
                il6 il64 = (il6) this.L$0;
                nc6.a(obj);
                arrayList = (List) this.L$2;
                ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                local32.d("HeartRateDetailPresenter", "listTimeZoneChange=" + yi4.a(list));
                if (!(!list.isEmpty()) || list.size() <= 1) {
                    list.clear();
                    i = 0;
                } else {
                    float floatValue = ((Number) ((lc6) ((pc6) list.get(0)).getSecond()).getSecond()).floatValue();
                    float floatValue2 = ((Number) ((lc6) ((pc6) list.get(qd6.a(list))).getSecond()).getSecond()).floatValue();
                    float f3 = (float) 0;
                    if ((floatValue >= f3 || floatValue2 >= f3) && (floatValue < f3 || floatValue2 < f3)) {
                        i2 = Math.abs((int) ((floatValue - floatValue2) * ((float) 60)));
                        if (floatValue2 < f3) {
                            i2 = -i2;
                        }
                    } else {
                        i2 = (int) ((floatValue - floatValue2) * ((float) 60));
                    }
                    i = i2;
                    ArrayList<pc6> arrayList5 = new ArrayList<>();
                    for (Object next : list) {
                        if (hf6.a(((Number) ((lc6) ((pc6) next).getSecond()).getSecond()).floatValue() == floatValue).booleanValue()) {
                            arrayList5.add(next);
                        }
                    }
                    ArrayList<lc6> arrayList6 = new ArrayList<>(rd6.a(arrayList5, 10));
                    for (pc6 second : arrayList5) {
                        arrayList6.add((lc6) second.getSecond());
                    }
                    ArrayList arrayList7 = new ArrayList(rd6.a(arrayList6, 10));
                    for (lc6 first : arrayList6) {
                        arrayList7.add(hf6.a(((Number) first.getFirst()).intValue()));
                    }
                    if (!arrayList7.contains(hf6.a(0))) {
                        String a5 = this.this$0.a(hf6.a(floatValue));
                        Integer a6 = hf6.a(0);
                        lc6 lc6 = new lc6(hf6.a(0), hf6.a(floatValue));
                        list.add(0, new pc6(a6, lc6, "12a" + a5));
                    }
                    ArrayList<pc6> arrayList8 = new ArrayList<>();
                    for (Object next2 : list) {
                        if (hf6.a(((Number) ((lc6) ((pc6) next2).getSecond()).getSecond()).floatValue() == floatValue2).booleanValue()) {
                            arrayList8.add(next2);
                        }
                    }
                    ArrayList<lc6> arrayList9 = new ArrayList<>(rd6.a(arrayList8, 10));
                    for (pc6 second2 : arrayList8) {
                        arrayList9.add((lc6) second2.getSecond());
                    }
                    ArrayList arrayList10 = new ArrayList(rd6.a(arrayList9, 10));
                    for (lc6 first2 : arrayList9) {
                        arrayList10.add(hf6.a(((Number) first2.getFirst()).intValue()));
                    }
                    if (!arrayList10.contains(hf6.a(24))) {
                        String a7 = this.this$0.a(hf6.a(floatValue2));
                        Integer a8 = hf6.a(i + DateTimeConstants.MINUTES_PER_DAY);
                        lc6 lc62 = new lc6(hf6.a(24), hf6.a(floatValue2));
                        list.add(new pc6(a8, lc62, "12a" + a7));
                    }
                }
                int i522 = i + DateTimeConstants.MINUTES_PER_DAY;
                ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                local222.d("HeartRateDetailPresenter", "minutesOfDay=" + i522);
                this.this$0.p.a(i522, (List<jz5>) arrayList, (List<pc6<Integer, lc6<Integer, Float>, String>>) list);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list5 = (List) obj3;
            if (this.this$0.k == null || (!wg6.a((Object) this.this$0.k, (Object) list5))) {
                this.this$0.k = list5;
                dl6 a9 = this.this$0.b();
                si5$g$a si5_g_a = new si5$g$a(this, (xe6) null);
                this.L$0 = il63;
                this.L$1 = list5;
                this.label = 2;
                if (gk6.a(a9, si5_g_a, this) == a2) {
                    return a2;
                }
                List list6 = list5;
                il62 = il63;
                list3 = list6;
                ArrayList arrayList22 = new ArrayList();
                if (this.this$0.k != null) {
                }
                Date o2 = bk4.o(this.this$0.f);
                wg6.a((Object) o2, "DateHelper.getStartOfDay(mDate)");
                j2 = o2.getTime();
                dl6 a422 = this.this$0.b();
                si5$g$c si5_g_c22 = new si5$g$c(this, (xe6) null);
                this.L$0 = il62;
                this.L$1 = list3;
                this.L$2 = arrayList22;
                this.J$0 = j2;
                this.label = 3;
                obj2 = gk6.a(a422, si5_g_c22, this);
                if (obj2 == a2) {
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter$start$1", f = "HeartRateDetailPresenter.kt", l = {110}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(HeartRateDetailPresenter heartRateDetailPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateDetailPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            zh4 zh4;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.b();
                si5$h$c si5_h_c = new si5$h$c(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, si5_h_c, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            HeartRateDetailPresenter heartRateDetailPresenter = this.this$0;
            if (mFUser == null || (zh4 = mFUser.getDistanceUnit()) == null) {
                zh4 = zh4.METRIC;
            }
            heartRateDetailPresenter.l = zh4;
            LiveData o = this.this$0.m;
            pi5 m = this.this$0.p;
            if (m != null) {
                o.a((HeartRateDetailFragment) m, new si5$h$a(this));
                this.this$0.n.a(this.this$0.p, new si5$h$b(this));
                return cd6.a;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateDetailPresenter a;

        @DexIgnore
        public i(HeartRateDetailPresenter heartRateDetailPresenter) {
            this.a = heartRateDetailPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<DailyHeartRateSummary>>> apply(lc6<? extends Date, ? extends Date> lc6) {
            return this.a.q.getHeartRateSummaries((Date) lc6.component1(), (Date) lc6.component2(), true);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HeartRateDetailPresenter(pi5 pi5, HeartRateSummaryRepository heartRateSummaryRepository, HeartRateSampleRepository heartRateSampleRepository, UserRepository userRepository, WorkoutSessionRepository workoutSessionRepository, FitnessDataDao fitnessDataDao, WorkoutDao workoutDao, FitnessDatabase fitnessDatabase, u04 u04) {
        wg6.b(pi5, "mView");
        wg6.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(workoutSessionRepository, "mWorkoutSessionRepository");
        wg6.b(fitnessDataDao, "mFitnessDataDao");
        wg6.b(workoutDao, "mWorkoutDao");
        wg6.b(fitnessDatabase, "mWorkoutDatabase");
        wg6.b(u04, "appExecutors");
        this.p = pi5;
        this.q = heartRateSummaryRepository;
        this.r = heartRateSampleRepository;
        this.s = userRepository;
        this.t = workoutSessionRepository;
        this.u = fitnessDataDao;
        this.v = workoutDao;
        this.w = fitnessDatabase;
        this.x = u04;
        LiveData<yx5<List<DailyHeartRateSummary>>> b2 = sd.b(this.g, new i(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.m = b2;
        LiveData<yx5<List<HeartRateSample>>> b3 = sd.b(this.g, new d(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026irst, second, true)\n    }");
        this.n = b3;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "stop");
        LiveData<yx5<List<HeartRateSample>>> liveData = this.n;
        pi5 pi5 = this.p;
        if (pi5 != null) {
            liveData.a((HeartRateDetailFragment) pi5);
            this.m.a(this.p);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
    }

    @DexIgnore
    public void h() {
        LiveData<cf<WorkoutSession>> pagedList;
        try {
            Listing<WorkoutSession> listing = this.o;
            if (!(listing == null || (pagedList = listing.getPagedList()) == null)) {
                pi5 pi5 = this.p;
                if (pi5 != null) {
                    pagedList.a((HeartRateDetailFragment) pi5);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
                }
            }
            this.t.removePagingListener();
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("removeDataSourceObserver - ex=");
            e2.printStackTrace();
            sb.append(cd6.a);
            local.e("HeartRateDetailPresenter", sb.toString());
        }
    }

    @DexIgnore
    public void i() {
        Date m2 = bk4.m(this.f);
        wg6.a((Object) m2, "DateHelper.getNextDate(mDate)");
        b(m2);
    }

    @DexIgnore
    public void j() {
        Date n2 = bk4.n(this.f);
        wg6.a((Object) n2, "DateHelper.getPrevDate(mDate)");
        b(n2);
    }

    @DexIgnore
    public void k() {
        this.p.a(this);
    }

    @DexIgnore
    public final rm6 l() {
        return ik6.b(e(), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final rm6 m() {
        return ik6.b(e(), (af6) null, (ll6) null, new g(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void c(Date date) {
        h();
        WorkoutSessionRepository workoutSessionRepository = this.t;
        this.o = workoutSessionRepository.getWorkoutSessionsPaging(date, workoutSessionRepository, this.u, this.v, this.w, this.x, this);
        Listing<WorkoutSession> listing = this.o;
        if (listing != null) {
            LiveData<cf<WorkoutSession>> pagedList = listing.getPagedList();
            pi5 pi5 = this.p;
            if (pi5 != null) {
                pagedList.a((HeartRateDetailFragment) pi5, new c(this));
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailFragment");
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void b(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new e(this, date, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final DailyHeartRateSummary b(Date date, List<DailyHeartRateSummary> list) {
        T t2 = null;
        if (list == null) {
            return null;
        }
        Iterator<T> it = list.iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            T next = it.next();
            if (bk4.d(((DailyHeartRateSummary) next).getDate(), date)) {
                t2 = next;
                break;
            }
        }
        return (DailyHeartRateSummary) t2;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        c(date);
    }

    @DexIgnore
    public void a(Bundle bundle) {
        wg6.b(bundle, "outState");
        bundle.putLong("KEY_LONG_TIME", this.f.getTime());
    }

    @DexIgnore
    public final List<HeartRateSample> a(Date date, List<HeartRateSample> list) {
        ti6<T> b2;
        ti6<T> a2;
        if (list == null || (b2 = yd6.b(list)) == null || (a2 = aj6.a(b2, new b(date))) == null) {
            return null;
        }
        return aj6.g(a2);
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }

    @DexIgnore
    public void a(vk4.g gVar) {
        gg6<cd6> retry;
        wg6.b(gVar, "report");
        FLogger.INSTANCE.getLocal().d("HeartRateDetailPresenter", "retry all failed request");
        Listing<WorkoutSession> listing = this.o;
        if (listing != null && (retry = listing.getRetry()) != null) {
            cd6 invoke = retry.invoke();
        }
    }
}
