package com.portfolio.platform.uirenew.home.customize.diana;

import android.os.Parcelable;
import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.WatchFaceHelper;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.dl4;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.h45$c$a;
import com.fossil.h45$d$a;
import com.fossil.h45$m$a;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.v3;
import com.fossil.vi4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.yj4;
import com.fossil.zi4;
import com.fossil.zl6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.RingStyleRepository;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.ComplicationLastSetting;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.diana.preset.WatchFace;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.ComplicationLastSettingRepository;
import com.portfolio.platform.data.source.ComplicationRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.WatchAppLastSettingRepository;
import com.portfolio.platform.data.source.WatchAppRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeViewModel extends td {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public static /* final */ a G; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ComplicationLastSettingRepository A;
    @DexIgnore
    public /* final */ WatchAppRepository B;
    @DexIgnore
    public /* final */ RingStyleRepository C;
    @DexIgnore
    public /* final */ WatchAppLastSettingRepository D;
    @DexIgnore
    public /* final */ WatchFaceRepository E;
    @DexIgnore
    public DianaPreset a;
    @DexIgnore
    public MutableLiveData<DianaPreset> b; // = new MutableLiveData<>();
    @DexIgnore
    public MutableLiveData<Boolean> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<Complication> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<WatchApp> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<WatchFaceWrapper> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> g; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> h; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<Complication> i; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> j; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> k; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<WatchApp> l; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ MutableLiveData<String> m; // = new MutableLiveData<>();
    @DexIgnore
    public LiveData<List<WatchFace>> n;
    @DexIgnore
    public List<WatchFace> o;
    @DexIgnore
    public DianaComplicationRingStyle p;
    @DexIgnore
    public /* final */ Gson q; // = new Gson();
    @DexIgnore
    public /* final */ LiveData<String> r;
    @DexIgnore
    public /* final */ LiveData<String> s;
    @DexIgnore
    public /* final */ LiveData<Complication> t;
    @DexIgnore
    public /* final */ LiveData<WatchApp> u;
    @DexIgnore
    public /* final */ LiveData<Boolean> v;
    @DexIgnore
    public /* final */ ld<DianaPreset> w;
    @DexIgnore
    public /* final */ ld<List<WatchFace>> x;
    @DexIgnore
    public /* final */ DianaPresetRepository y;
    @DexIgnore
    public /* final */ ComplicationRepository z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return DianaCustomizeViewModel.F;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<DianaPreset> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public b(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(DianaPreset dianaPreset) {
            T t;
            T t2;
            boolean z;
            boolean z2;
            FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "current preset change=" + dianaPreset);
            if (dianaPreset != null) {
                String str = (String) this.a.g.a();
                String str2 = (String) this.a.h.a();
                FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "selectedComplicationPos=" + str + " selectedComplicationId=" + str2);
                this.a.m.a(dianaPreset.getWatchFaceId());
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                    if (!wg6.a((Object) dianaPresetComplicationSetting.getPosition(), (Object) str) || xj6.b(dianaPresetComplicationSetting.getId(), str2, true)) {
                        z2 = false;
                        continue;
                    } else {
                        z2 = true;
                        continue;
                    }
                    if (z2) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting2 = (DianaPresetComplicationSetting) t;
                if (dianaPresetComplicationSetting2 != null) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "Update new complication id=" + dianaPresetComplicationSetting2.getId() + " at position=" + str);
                    this.a.h.a(dianaPresetComplicationSetting2.getId());
                }
                String str3 = (String) this.a.j.a();
                String str4 = (String) this.a.k.a();
                Iterator<T> it2 = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        t2 = null;
                        break;
                    }
                    t2 = it2.next();
                    DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t2;
                    if (!wg6.a((Object) dianaPresetWatchAppSetting.getPosition(), (Object) str3) || xj6.b(dianaPresetWatchAppSetting.getId(), str4, true)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        continue;
                    }
                    if (z) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting2 = (DianaPresetWatchAppSetting) t2;
                if (dianaPresetWatchAppSetting2 != null) {
                    FLogger.INSTANCE.getLocal().d(DianaCustomizeViewModel.G.a(), "Update new watchapp id=" + dianaPresetWatchAppSetting2.getId() + " at position=" + str3);
                    this.a.k.a(dianaPresetWatchAppSetting2.getId());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$init$1", f = "DianaCustomizeViewModel.kt", l = {151, 156}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DianaCustomizeViewModel dianaCustomizeViewModel, String str, String str2, String str3, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaCustomizeViewModel;
            this.$presetId = str;
            this.$watchAppPos = str2;
            this.$complicationPos = str3;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, this.$presetId, this.$watchAppPos, this.$complicationPos, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DianaCustomizeViewModel.G.a();
                local.d(a2, "init presetId=" + this.$presetId + " originalPreset=" + this.this$0.a + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos);
                if (this.this$0.a == null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = il6;
                    this.label = 1;
                    if (dianaCustomizeViewModel.a(str, (xe6<? super cd6>) this) == a) {
                        return a;
                    }
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.a(this.$complicationPos);
            this.this$0.j.a(this.$watchAppPos);
            cn6 c = zl6.c();
            h45$c$a h45_c_a = new h45$c$a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 2;
            if (gk6.a(c, h45_c_a, this) == a) {
                return a;
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$initFromSaveInstanceState$1", f = "DianaCustomizeViewModel.kt", l = {167, 170, 178}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $complicationPos;
        @DexIgnore
        public /* final */ /* synthetic */ String $presetId;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $savedCurrentPreset;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $savedOriginalPreset;
        @DexIgnore
        public /* final */ /* synthetic */ String $watchAppPos;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(DianaCustomizeViewModel dianaCustomizeViewModel, String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaCustomizeViewModel;
            this.$presetId = str;
            this.$savedOriginalPreset = dianaPreset;
            this.$savedCurrentPreset = dianaPreset2;
            this.$watchAppPos = str2;
            this.$complicationPos = str3;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$presetId, this.$savedOriginalPreset, this.$savedCurrentPreset, this.$watchAppPos, this.$complicationPos, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00ed A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00f2  */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            cn6 c;
            h45$d$a h45_d_a;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = DianaCustomizeViewModel.G.a();
                local.d(a2, "initFromSaveInstanceState presetId " + this.$presetId + " savedOriginalPreset=" + this.$savedOriginalPreset + ',' + " savedCurrentPreset=" + this.$savedCurrentPreset + " watchAppPos=" + this.$watchAppPos + " complicationPos=" + this.$complicationPos + " currentPreset");
                if (this.$savedOriginalPreset == null) {
                    DianaCustomizeViewModel dianaCustomizeViewModel = this.this$0;
                    String str = this.$presetId;
                    this.L$0 = il6;
                    this.label = 1;
                    if (dianaCustomizeViewModel.a(str, (xe6<? super cd6>) this) == a) {
                        return a;
                    }
                    this.this$0.g.a(this.$complicationPos);
                    this.this$0.j.a(this.$watchAppPos);
                    c = zl6.c();
                    h45_d_a = new h45$d$a(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 3;
                    if (gk6.a(c, h45_d_a, this) == a) {
                    }
                    if (this.$savedCurrentPreset != null) {
                    }
                    return cd6.a;
                }
                this.this$0.k();
                DianaCustomizeViewModel dianaCustomizeViewModel2 = this.this$0;
                DianaPreset dianaPreset = this.$savedCurrentPreset;
                DianaPreset clone = dianaPreset != null ? dianaPreset.clone() : null;
                this.L$0 = il6;
                this.label = 2;
                if (dianaCustomizeViewModel2.a(clone, (xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.g.a(this.$complicationPos);
                this.this$0.j.a(this.$watchAppPos);
                c = zl6.c();
                h45_d_a = new h45$d$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 3;
                if (gk6.a(c, h45_d_a, this) == a) {
                    return a;
                }
                if (this.$savedCurrentPreset != null) {
                }
                return cd6.a;
            } else if (i == 2) {
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 3) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                if (this.$savedCurrentPreset != null) {
                    this.this$0.b.a(this.$savedCurrentPreset);
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.a = this.$savedOriginalPreset;
            this.this$0.g.a(this.$complicationPos);
            this.this$0.j.a(this.$watchAppPos);
            c = zl6.c();
            h45_d_a = new h45$d$a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 3;
            if (gk6.a(c, h45_d_a, this) == a) {
            }
            if (this.$savedCurrentPreset != null) {
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ xe6 $continuation$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ DianaPreset $currentPreset$inlined;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(xe6 xe6, DianaCustomizeViewModel dianaCustomizeViewModel, DianaPreset dianaPreset, xe6 xe62) {
            super(2, xe6);
            this.this$0 = dianaCustomizeViewModel;
            this.$currentPreset$inlined = dianaPreset;
            this.$continuation$inlined = xe62;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(xe6, this.this$0, this.$currentPreset$inlined, this.$continuation$inlined);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                LiveData m = this.this$0.n;
                if (m == null) {
                    return null;
                }
                m.a(this.this$0.x);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel", f = "DianaCustomizeViewModel.kt", l = {438}, m = "initializeCustomizeTheme")
    public static final class f extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(DianaCustomizeViewModel dianaCustomizeViewModel, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaCustomizeViewModel;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((DianaPreset) null, (xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel", f = "DianaCustomizeViewModel.kt", l = {412}, m = "initializePreset")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(DianaCustomizeViewModel dianaCustomizeViewModel, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaCustomizeViewModel;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((String) null, (xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public h(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Complication> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformComplicationIdToModel complicationId=" + str);
            DianaCustomizeViewModel dianaCustomizeViewModel = this.a;
            wg6.a((Object) str, "id");
            this.a.i.a(dianaCustomizeViewModel.b(str));
            return this.a.i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public i(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformComplicationPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.b.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getComplications().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((DianaPresetComplicationSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                if (dianaPresetComplicationSetting != null) {
                    this.a.h.b(dianaPresetComplicationSetting.getId());
                }
            }
            return this.a.h;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public j(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<WatchApp> apply(String str) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformWatchAppIdToModel watchAppId=" + str);
            DianaCustomizeViewModel dianaCustomizeViewModel = this.a;
            wg6.a((Object) str, "id");
            this.a.l.a(dianaCustomizeViewModel.d(str));
            return this.a.l;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public k(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<String> apply(String str) {
            T t;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = DianaCustomizeViewModel.G.a();
            local.d(a2, "transformWatchAppPosToId pos=" + str);
            DianaPreset dianaPreset = (DianaPreset) this.a.b.a();
            if (dianaPreset != null) {
                Iterator<T> it = dianaPreset.getWatchapps().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (wg6.a((Object) ((DianaPresetWatchAppSetting) t).getPosition(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting = (DianaPresetWatchAppSetting) t;
                if (dianaPresetWatchAppSetting != null) {
                    this.a.k.b(dianaPresetWatchAppSetting.getId());
                }
            }
            return this.a.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public l(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final MutableLiveData<Boolean> apply(DianaPreset dianaPreset) {
            ArrayList<DianaPresetComplicationSetting> complications = dianaPreset.getComplications();
            Gson d = this.a.q;
            DianaPreset e = this.a.a;
            if (e != null) {
                boolean a2 = zi4.a((List<DianaPresetComplicationSetting>) complications, d, (List<DianaPresetComplicationSetting>) e.getComplications());
                ArrayList<DianaPresetWatchAppSetting> watchapps = dianaPreset.getWatchapps();
                Gson d2 = this.a.q;
                DianaPreset e2 = this.a.a;
                if (e2 != null) {
                    boolean c = zi4.c(watchapps, d2, e2.getWatchapps());
                    String watchFaceId = dianaPreset.getWatchFaceId();
                    DianaPreset e3 = this.a.a;
                    if (e3 != null) {
                        boolean a3 = wg6.a((Object) watchFaceId, (Object) e3.getWatchFaceId());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a4 = DianaCustomizeViewModel.G.a();
                        local.d(a4, "isComplicationSettingListChange " + a2 + " isWatchAppChanged " + c + " isThemeChanged " + a3);
                        if (!a2 || !c || !a3) {
                            this.a.c.a(true);
                        } else {
                            this.a.c.a(false);
                        }
                        return this.a.c;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m<T> implements ld<List<? extends WatchFace>> {
        @DexIgnore
        public /* final */ /* synthetic */ DianaCustomizeViewModel a;

        @DexIgnore
        public m(DianaCustomizeViewModel dianaCustomizeViewModel) {
            this.a = dianaCustomizeViewModel;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<WatchFace> list) {
            rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new h45$m$a(this, list, (xe6) null), 3, (Object) null);
        }
    }

    /*
    static {
        String simpleName = DianaCustomizeViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "DianaCustomizeViewModel::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    public DianaCustomizeViewModel(DianaPresetRepository dianaPresetRepository, ComplicationRepository complicationRepository, ComplicationLastSettingRepository complicationLastSettingRepository, WatchAppRepository watchAppRepository, RingStyleRepository ringStyleRepository, WatchAppLastSettingRepository watchAppLastSettingRepository, WatchFaceRepository watchFaceRepository) {
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(complicationRepository, "mComplicationRepository");
        wg6.b(complicationLastSettingRepository, "mLastComplicationSettingRepository");
        wg6.b(watchAppRepository, "mWatchAppRepository");
        wg6.b(ringStyleRepository, "mRingStyleRepository");
        wg6.b(watchAppLastSettingRepository, "mWatchAppLastSettingRepository");
        wg6.b(watchFaceRepository, "mWatchFaceRepository");
        this.y = dianaPresetRepository;
        this.z = complicationRepository;
        this.A = complicationLastSettingRepository;
        this.B = watchAppRepository;
        this.C = ringStyleRepository;
        this.D = watchAppLastSettingRepository;
        this.E = watchFaceRepository;
        LiveData<String> b2 = sd.b(this.g, new i(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026plicationIdLiveData\n    }");
        this.r = b2;
        LiveData<String> b3 = sd.b(this.j, new k(this));
        wg6.a((Object) b3, "Transformations.switchMa\u2026dWatchAppIdLiveData\n    }");
        this.s = b3;
        LiveData<Complication> b4 = sd.b(this.r, new h(this));
        wg6.a((Object) b4, "Transformations.switchMa\u2026omplicationLiveData\n    }");
        this.t = b4;
        LiveData<WatchApp> b5 = sd.b(this.s, new j(this));
        wg6.a((Object) b5, "Transformations.switchMa\u2026tedWatchAppLiveData\n    }");
        this.u = b5;
        LiveData<Boolean> b6 = sd.b(this.b, new l(this));
        wg6.a((Object) b6, "Transformations.switchMa\u2026    isPresetChanged\n    }");
        this.v = b6;
        this.w = new b(this);
        this.x = new m(this);
    }

    @DexIgnore
    public void onCleared() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        local.d(str, " same =" + wg6.a((Object) this.a, (Object) (DianaPreset) this.b.a()) + "onCleared originalPreset=" + this.a + " currentPreset=" + ((DianaPreset) this.b.a()));
        this.b.b(this.w);
        LiveData<List<WatchFace>> liveData = this.n;
        if (liveData != null) {
            liveData.b(this.x);
        }
        DianaCustomizeViewModel.super.onCleared();
    }

    @DexIgnore
    public final LiveData<Boolean> b() {
        LiveData<Boolean> liveData = this.v;
        if (liveData != null) {
            return liveData;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final DianaComplicationRingStyle c() {
        return this.p;
    }

    @DexIgnore
    public final DianaPreset d() {
        return this.a;
    }

    @DexIgnore
    public final LiveData<Complication> e() {
        return this.t;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v3, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: com.portfolio.platform.data.model.setting.SecondTimezoneSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeSetting} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Parcelable f(String str) {
        CommuteTimeSetting commuteTimeSetting;
        DianaPresetComplicationSetting dianaPresetComplicationSetting;
        DianaPresetComplicationSetting dianaPresetComplicationSetting2;
        String settings;
        wg6.b(str, "complicationId");
        Gson gson = new Gson();
        if (!yj4.b.c(str)) {
            return null;
        }
        DianaPreset dianaPreset = (DianaPreset) a().a();
        String str2 = "";
        if (dianaPreset != null) {
            Iterator<T> it = dianaPreset.getComplications().iterator();
            while (true) {
                if (!it.hasNext()) {
                    dianaPresetComplicationSetting2 = null;
                    break;
                }
                dianaPresetComplicationSetting2 = it.next();
                if (wg6.a((Object) dianaPresetComplicationSetting2.getId(), (Object) str)) {
                    break;
                }
            }
            DianaPresetComplicationSetting dianaPresetComplicationSetting3 = (DianaPresetComplicationSetting) dianaPresetComplicationSetting2;
            if (!(dianaPresetComplicationSetting3 == null || (settings = dianaPresetComplicationSetting3.getSettings()) == null)) {
                str2 = settings;
            }
        }
        if (str2.length() == 0) {
            ComplicationLastSetting complicationLastSetting = this.A.getComplicationLastSetting(str);
            if (complicationLastSetting != null) {
                str2 = complicationLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && dianaPreset != null) {
                DianaPreset clone = dianaPreset.clone();
                Iterator<T> it2 = clone.getComplications().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        dianaPresetComplicationSetting = null;
                        break;
                    }
                    dianaPresetComplicationSetting = it2.next();
                    if (wg6.a((Object) dianaPresetComplicationSetting.getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetComplicationSetting dianaPresetComplicationSetting4 = (DianaPresetComplicationSetting) dianaPresetComplicationSetting;
                if (dianaPresetComplicationSetting4 != null) {
                    dianaPresetComplicationSetting4.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (vi4.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 134170930) {
                    return null;
                }
                if (!str.equals("second-timezone")) {
                    return null;
                }
                SecondTimezoneSetting secondTimezoneSetting = (SecondTimezoneSetting) gson.a(str2, SecondTimezoneSetting.class);
                if (TextUtils.isEmpty(secondTimezoneSetting.getTimeZoneId())) {
                    return null;
                }
                commuteTimeSetting = secondTimezoneSetting;
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeSetting commuteTimeSetting2 = (CommuteTimeSetting) gson.a(str2, CommuteTimeSetting.class);
                boolean isEmpty = TextUtils.isEmpty(commuteTimeSetting2.getAddress());
                commuteTimeSetting = commuteTimeSetting2;
                if (isEmpty) {
                    return null;
                }
            }
            return commuteTimeSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(F, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v7, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v11, resolved type: com.portfolio.platform.data.model.setting.WeatherWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v12, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v13, resolved type: com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Parcelable g(String str) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting;
        DianaPresetWatchAppSetting dianaPresetWatchAppSetting2;
        String settings;
        wg6.b(str, "appId");
        if (!dl4.c.f(str)) {
            return null;
        }
        DianaPreset dianaPreset = (DianaPreset) a().a();
        String str2 = "";
        if (dianaPreset != null) {
            Iterator<T> it = dianaPreset.getWatchapps().iterator();
            while (true) {
                if (!it.hasNext()) {
                    dianaPresetWatchAppSetting2 = null;
                    break;
                }
                dianaPresetWatchAppSetting2 = it.next();
                if (wg6.a((Object) dianaPresetWatchAppSetting2.getId(), (Object) str)) {
                    break;
                }
            }
            DianaPresetWatchAppSetting dianaPresetWatchAppSetting3 = (DianaPresetWatchAppSetting) dianaPresetWatchAppSetting2;
            if (!(dianaPresetWatchAppSetting3 == null || (settings = dianaPresetWatchAppSetting3.getSettings()) == null)) {
                str2 = settings;
            }
        }
        if (str2.length() == 0) {
            WatchAppLastSetting watchAppLastSetting = this.D.getWatchAppLastSetting(str);
            if (watchAppLastSetting != null) {
                str2 = watchAppLastSetting.getSetting();
            }
            if (!TextUtils.isEmpty(str2) && dianaPreset != null) {
                DianaPreset clone = dianaPreset.clone();
                Iterator<T> it2 = clone.getWatchapps().iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        dianaPresetWatchAppSetting = null;
                        break;
                    }
                    dianaPresetWatchAppSetting = it2.next();
                    if (wg6.a((Object) dianaPresetWatchAppSetting.getId(), (Object) str)) {
                        break;
                    }
                }
                DianaPresetWatchAppSetting dianaPresetWatchAppSetting4 = (DianaPresetWatchAppSetting) dianaPresetWatchAppSetting;
                if (dianaPresetWatchAppSetting4 != null) {
                    dianaPresetWatchAppSetting4.setSettings(str2);
                    a(clone);
                }
            }
        }
        if (vi4.a(str2)) {
            return null;
        }
        try {
            int hashCode = str.hashCode();
            if (hashCode != -829740640) {
                if (hashCode != 1223440372) {
                    return null;
                }
                if (!str.equals("weather")) {
                    return null;
                }
                WeatherWatchAppSetting weatherWatchAppSetting = (WeatherWatchAppSetting) this.q.a(str2, WeatherWatchAppSetting.class);
                List<WeatherLocationWrapper> locations = weatherWatchAppSetting.getLocations();
                ArrayList arrayList = new ArrayList();
                for (WeatherLocationWrapper next : locations) {
                    if (!TextUtils.isEmpty(next.getId())) {
                        arrayList.add(next);
                    }
                }
                commuteTimeWatchAppSetting = weatherWatchAppSetting;
                if (!(!arrayList.isEmpty())) {
                    return null;
                }
            } else if (!str.equals("commute-time")) {
                return null;
            } else {
                CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) this.q.a(str2, CommuteTimeWatchAppSetting.class);
                boolean z2 = !commuteTimeWatchAppSetting2.getAddresses().isEmpty();
                commuteTimeWatchAppSetting = commuteTimeWatchAppSetting2;
                if (!z2) {
                    return null;
                }
            }
            return commuteTimeWatchAppSetting;
        } catch (Exception e2) {
            FLogger.INSTANCE.getLocal().d(F, "exception when parse setting from json " + e2);
            return null;
        }
    }

    @DexIgnore
    public final LiveData<WatchApp> h() {
        return this.u;
    }

    @DexIgnore
    public final MutableLiveData<String> i() {
        return this.j;
    }

    @DexIgnore
    public final void j(String str) {
        wg6.b(str, "complicationPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "setSelectedComplicationPos complicationPos=" + str);
        this.g.a(str);
    }

    @DexIgnore
    public final void k(String str) {
        wg6.b(str, "watchAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = F;
        local.d(str2, "setSelectedWatchApp watchAppPos=" + str);
        this.j.a(str);
    }

    @DexIgnore
    public final boolean l() {
        Boolean bool = (Boolean) this.c.a();
        if (bool == null || bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final Complication b(String str) {
        T t2;
        wg6.b(str, "complicationId");
        Iterator<T> it = this.d.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (wg6.a((Object) str, (Object) ((Complication) t2).getComplicationId())) {
                break;
            }
        }
        return (Complication) t2;
    }

    @DexIgnore
    public final List<WatchApp> c(String str) {
        wg6.b(str, "category");
        CopyOnWriteArrayList<WatchApp> copyOnWriteArrayList = this.e;
        ArrayList arrayList = new ArrayList();
        for (T next : copyOnWriteArrayList) {
            if (((WatchApp) next).getCategories().contains(str)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public final WatchApp d(String str) {
        T t2;
        wg6.b(str, "watchAppId");
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (wg6.a((Object) str, (Object) ((WatchApp) t2).getWatchappId())) {
                break;
            }
        }
        return (WatchApp) t2;
    }

    @DexIgnore
    public final WatchFaceWrapper e(String str) {
        T t2;
        wg6.b(str, "watchFaceId");
        Iterator<T> it = this.f.iterator();
        while (true) {
            if (!it.hasNext()) {
                t2 = null;
                break;
            }
            t2 = it.next();
            if (wg6.a((Object) str, (Object) ((WatchFaceWrapper) t2).getId())) {
                break;
            }
        }
        return (WatchFaceWrapper) t2;
    }

    @DexIgnore
    public final boolean h(String str) {
        DianaPreset dianaPreset;
        wg6.b(str, "complicationId");
        if (!wg6.a((Object) str, (Object) "empty") && (dianaPreset = (DianaPreset) this.b.a()) != null) {
            Iterator<DianaPresetComplicationSetting> it = dianaPreset.getComplications().iterator();
            while (it.hasNext()) {
                if (wg6.a((Object) it.next().getId(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final boolean i(String str) {
        DianaPreset dianaPreset;
        wg6.b(str, "watchAppId");
        if (!wg6.a((Object) str, (Object) "empty") && (dianaPreset = (DianaPreset) this.b.a()) != null) {
            Iterator<DianaPresetWatchAppSetting> it = dianaPreset.getWatchapps().iterator();
            while (it.hasNext()) {
                DianaPresetWatchAppSetting next = it.next();
                next.component1();
                if (wg6.a((Object) next.component2(), (Object) str)) {
                    return true;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public final rm6 a(String str, String str2, String str3) {
        wg6.b(str, "presetId");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new c(this, str, str3, str2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final CopyOnWriteArrayList<WatchFaceWrapper> j() {
        return this.f;
    }

    @DexIgnore
    public final void k() {
        this.d.clear();
        this.d.addAll(this.z.getAllComplicationRaw());
        this.e.clear();
        this.e.addAll(this.B.getAllWatchAppRaw());
    }

    @DexIgnore
    public final rm6 a(String str, DianaPreset dianaPreset, DianaPreset dianaPreset2, String str2, String str3) {
        wg6.b(str, "presetId");
        return ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new d(this, str, dianaPreset, dianaPreset2, str3, str2, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final MutableLiveData<DianaPreset> a() {
        return this.b;
    }

    @DexIgnore
    public final void a(DianaPreset dianaPreset) {
        wg6.b(dianaPreset, "preset");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = F;
        local.d(str, "savePreset newPreset=" + dianaPreset);
        this.b.a(dianaPreset.clone());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object a(String str, xe6<? super cd6> xe6) {
        g gVar;
        int i2;
        DianaPreset dianaPreset;
        DianaCustomizeViewModel dianaCustomizeViewModel;
        if (xe6 instanceof g) {
            gVar = (g) xe6;
            int i3 = gVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                gVar.label = i3 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                i2 = gVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = F;
                    local.d(str2, "initializePreset presetId=" + str);
                    DianaPreset presetById = this.y.getPresetById(str);
                    k();
                    DianaPreset clone = presetById != null ? presetById.clone() : null;
                    gVar.L$0 = this;
                    gVar.L$1 = str;
                    gVar.L$2 = presetById;
                    gVar.label = 1;
                    if (a(clone, (xe6<? super cd6>) gVar) == a2) {
                        return a2;
                    }
                    dianaCustomizeViewModel = this;
                    dianaPreset = presetById;
                } else if (i2 == 1) {
                    dianaPreset = (DianaPreset) gVar.L$2;
                    String str3 = (String) gVar.L$1;
                    dianaCustomizeViewModel = (DianaCustomizeViewModel) gVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (dianaPreset != null) {
                    dianaCustomizeViewModel.a = dianaPreset.clone();
                    dianaCustomizeViewModel.b.a(dianaPreset.clone());
                }
                return cd6.a;
            }
        }
        gVar = new g(this, xe6);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        i2 = gVar.label;
        if (i2 != 0) {
        }
        if (dianaPreset != null) {
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object a(DianaPreset dianaPreset, xe6<? super cd6> xe6) {
        f fVar;
        int i2;
        if (xe6 instanceof f) {
            fVar = (f) xe6;
            int i3 = fVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                fVar.label = i3 - Integer.MIN_VALUE;
                Object obj = fVar.result;
                Object a2 = ff6.a();
                i2 = fVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    if (dianaPreset != null) {
                        String serialNumber = dianaPreset.getSerialNumber();
                        this.p = this.C.getRingStylesBySerial(serialNumber);
                        this.f.clear();
                        this.o = this.E.getWatchFacesWithSerial(serialNumber);
                        List<WatchFace> list = this.o;
                        if (list != null) {
                            hf6.a(this.f.addAll(WatchFaceHelper.a(list, dianaPreset.getComplications())));
                        }
                        this.n = this.E.getWatchFacesLiveDataWithSerial(serialNumber);
                        cn6 c2 = zl6.c();
                        e eVar = new e((xe6) null, this, dianaPreset, fVar);
                        fVar.L$0 = this;
                        fVar.L$1 = dianaPreset;
                        fVar.L$2 = dianaPreset;
                        fVar.L$3 = serialNumber;
                        fVar.label = 1;
                        obj = gk6.a(c2, eVar, fVar);
                        if (obj == a2) {
                            return a2;
                        }
                    }
                    return cd6.a;
                } else if (i2 == 1) {
                    String str = (String) fVar.L$3;
                    DianaPreset dianaPreset2 = (DianaPreset) fVar.L$2;
                    DianaPreset dianaPreset3 = (DianaPreset) fVar.L$1;
                    DianaCustomizeViewModel dianaCustomizeViewModel = (DianaCustomizeViewModel) fVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                cd6 cd6 = (cd6) obj;
                return cd6.a;
            }
        }
        fVar = new f(this, xe6);
        Object obj2 = fVar.result;
        Object a22 = ff6.a();
        i2 = fVar.label;
        if (i2 != 0) {
        }
        cd6 cd62 = (cd6) obj2;
        return cd6.a;
    }

    @DexIgnore
    public final MutableLiveData<String> f() {
        return this.g;
    }

    @DexIgnore
    public final MutableLiveData<String> g() {
        return this.m;
    }

    @DexIgnore
    public final List<Complication> a(String str) {
        wg6.b(str, "category");
        CopyOnWriteArrayList<Complication> copyOnWriteArrayList = this.d;
        ArrayList arrayList = new ArrayList();
        for (T next : copyOnWriteArrayList) {
            if (((Complication) next).getCategories().contains(str)) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
