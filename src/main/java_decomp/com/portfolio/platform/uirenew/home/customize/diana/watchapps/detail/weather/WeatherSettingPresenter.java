package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather;

import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.e85;
import com.fossil.f85;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.i85$b$a;
import com.fossil.i85$d$a;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lc3;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mc3;
import com.fossil.nc6;
import com.fossil.qc3;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import com.portfolio.platform.data.source.remote.GoogleApiService;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettingPresenter extends e85 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public WeatherWatchAppSetting f;
    @DexIgnore
    public List<WeatherLocationWrapper> g; // = new ArrayList();
    @DexIgnore
    public /* final */ f85 h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<TResult> implements mc3<FetchPlaceResponse> {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;
        @DexIgnore
        public /* final */ /* synthetic */ String c;

        @DexIgnore
        public b(WeatherSettingPresenter weatherSettingPresenter, String str, String str2) {
            this.a = weatherSettingPresenter;
            this.b = str;
            this.c = str2;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onSuccess(FetchPlaceResponse fetchPlaceResponse) {
            this.a.h.a();
            wg6.a((Object) fetchPlaceResponse, "response");
            Place place = fetchPlaceResponse.getPlace();
            wg6.a((Object) place, "response.place");
            LatLng latLng = place.getLatLng();
            if (latLng != null) {
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new i85$b$a(latLng, (xe6) null, this), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements lc3 {
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter a;

        @DexIgnore
        public c(WeatherSettingPresenter weatherSettingPresenter) {
            this.a = weatherSettingPresenter;
        }

        @DexIgnore
        public final void onFailure(Exception exc) {
            wg6.b(exc, "exception");
            this.a.h.a();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String k = WeatherSettingPresenter.i;
            local.e(k, "FetchPlaceRequest - exception=" + exc);
            this.a.h.L0();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter$saveWeatherWatchAppSetting$1", f = "WeatherSettingPresenter.kt", l = {127}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ WeatherSettingPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(WeatherSettingPresenter weatherSettingPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = weatherSettingPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            WeatherWatchAppSetting weatherWatchAppSetting;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                WeatherWatchAppSetting d = this.this$0.f;
                if (d != null) {
                    dl6 a2 = this.this$0.b();
                    i85$d$a i85_d_a = new i85$d$a((xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = d;
                    this.label = 1;
                    obj = gk6.a(a2, i85_d_a, this);
                    if (obj == a) {
                        return a;
                    }
                    weatherWatchAppSetting = d;
                }
                this.this$0.h.a();
                this.this$0.h.u(true);
                return cd6.a;
            } else if (i == 1) {
                weatherWatchAppSetting = (WeatherWatchAppSetting) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            weatherWatchAppSetting.setLocations(yd6.d((List) obj));
            this.this$0.h.a();
            this.this$0.h.u(true);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = WeatherSettingPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "WeatherSettingPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public WeatherSettingPresenter(f85 f85, GoogleApiService googleApiService) {
        wg6.b(f85, "mView");
        wg6.b(googleApiService, "mGoogleApiService");
        this.h = f85;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void f() {
        this.e = Places.createClient(PortfolioApp.get.instance());
        this.h.o(this.g);
        this.h.a(this.e);
    }

    @DexIgnore
    public void g() {
        this.e = null;
    }

    @DexIgnore
    public WeatherWatchAppSetting h() {
        return this.f;
    }

    @DexIgnore
    public void i() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "saveWeatherWatchAppSetting - mLocationWrappers=" + this.g);
        this.h.b();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.h.a(this);
    }

    @DexIgnore
    public void a(String str) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        wg6.b(str, MicroAppSetting.SETTING);
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) new Gson().a(str, WeatherWatchAppSetting.class);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = i;
            local.d(str2, "exception when parse weather setting " + e2);
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        this.f = weatherWatchAppSetting;
        if (this.f == null) {
            this.f = new WeatherWatchAppSetting();
        }
        WeatherWatchAppSetting weatherWatchAppSetting2 = this.f;
        if (weatherWatchAppSetting2 != null) {
            List<WeatherLocationWrapper> locations = weatherWatchAppSetting2.getLocations();
            ArrayList arrayList = new ArrayList();
            for (T next : locations) {
                if (!TextUtils.isEmpty(((WeatherLocationWrapper) next).getId())) {
                    arrayList.add(next);
                }
            }
            if (arrayList.isEmpty()) {
                this.f = new WeatherWatchAppSetting();
            }
            this.g.clear();
            WeatherWatchAppSetting weatherWatchAppSetting3 = this.f;
            if (weatherWatchAppSetting3 != null) {
                for (WeatherLocationWrapper add : weatherWatchAppSetting3.getLocations()) {
                    this.g.add(add);
                }
                return;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void a(int i2, boolean z) {
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((WeatherLocationWrapper) next).isEnableLocation()) {
                arrayList.add(next);
            }
        }
        if (z && arrayList.size() > 1) {
            this.h.l0();
        } else if (i2 < this.g.size()) {
            this.g.get(i2).setEnableLocation(z);
            this.h.o(this.g);
        }
    }

    @DexIgnore
    public void a(String str, String str2, AutocompleteSessionToken autocompleteSessionToken) {
        wg6.b(str, "address");
        wg6.b(str2, "placeId");
        List<WeatherLocationWrapper> list = this.g;
        ArrayList arrayList = new ArrayList();
        for (T next : list) {
            if (((WeatherLocationWrapper) next).isEnableLocation()) {
                arrayList.add(next);
            }
        }
        if (arrayList.size() > 1) {
            this.h.l0();
            return;
        }
        this.h.b();
        if (this.e != null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(Place.Field.ADDRESS);
            arrayList2.add(Place.Field.LAT_LNG);
            FetchPlaceRequest.Builder builder = FetchPlaceRequest.builder(str2, arrayList2);
            wg6.a((Object) builder, "FetchPlaceRequest.builder(placeId, placeFields)");
            builder.setSessionToken(autocompleteSessionToken);
            PlacesClient placesClient = this.e;
            if (placesClient != null) {
                qc3 fetchPlace = placesClient.fetchPlace(builder.build());
                wg6.a((Object) fetchPlace, "mPlacesClient!!.fetchPlace(request.build())");
                fetchPlace.a(new b(this, str, str2));
                fetchPlace.a(new c(this));
                return;
            }
            wg6.a();
            throw null;
        }
    }
}
