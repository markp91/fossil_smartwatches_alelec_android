package com.portfolio.platform.uirenew.home.profile.theme.user;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.ly5;
import com.fossil.nh6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.r74;
import com.fossil.rm5;
import com.fossil.sm5;
import com.fossil.ut4;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeActiveCaloriesChartViewModel;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeActiveCaloriesChartFragment extends BaseFragment implements ly5, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static String o;
    @DexIgnore
    public static /* final */ a p; // = new a((qg6) null);
    @DexIgnore
    public w04 f;
    @DexIgnore
    public CustomizeActiveCaloriesChartViewModel g;
    @DexIgnore
    public ax5<r74> h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CustomizeActiveCaloriesChartFragment.o;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<sm5.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeActiveCaloriesChartFragment a;

        @DexIgnore
        public b(CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment) {
            this.a = customizeActiveCaloriesChartFragment;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(CustomizeActiveCaloriesChartViewModel.b bVar) {
            if (bVar != null) {
                Integer a2 = bVar.a();
                if (a2 != null) {
                    this.a.s(a2.intValue());
                }
                Integer b = bVar.b();
                if (b != null) {
                    this.a.r(b.intValue());
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeActiveCaloriesChartFragment a;

        @DexIgnore
        public c(CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment) {
            this.a = customizeActiveCaloriesChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.a(childFragmentManager, 503);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeActiveCaloriesChartFragment a;

        @DexIgnore
        public d(CustomizeActiveCaloriesChartFragment customizeActiveCaloriesChartFragment) {
            this.a = customizeActiveCaloriesChartFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.g(childFragmentManager);
        }
    }

    /*
    static {
        String simpleName = CustomizeActiveCaloriesChartFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CustomizeActiveCaloriesC\u2026nt::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        FLogger.INSTANCE.getLocal().d(j, "onDialogFragmentResult");
        if (str.hashCode() == 657140349 && str.equals("APPLY_NEW_COLOR_THEME") && i2 == 2131363190) {
            CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.g;
            if (customizeActiveCaloriesChartViewModel != null) {
                customizeActiveCaloriesChartViewModel.a(UserCustomizeThemeFragment.p.a(), o);
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d(int i2, int i3) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onColorSelected dialogId=" + i2 + " color=" + i3);
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf(i3 & 16777215)};
        String format = String.format("#%06X", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.g;
        if (customizeActiveCaloriesChartViewModel != null) {
            customizeActiveCaloriesChartViewModel.a(i2, Color.parseColor(format));
            if (i2 == 503) {
                o = format;
                return;
            }
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public final void j1() {
        OverviewDayChart overviewDayChart;
        ax5<r74> ax5 = this.h;
        if (ax5 != null) {
            r74 a2 = ax5.a();
            ArrayList arrayList = new ArrayList();
            arrayList.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 10, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a3 = qd6.a((T[]) new ArrayList[]{arrayList});
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 214, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a4 = qd6.a((T[]) new ArrayList[]{arrayList2});
            ArrayList arrayList3 = new ArrayList();
            arrayList3.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 24, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a5 = qd6.a((T[]) new ArrayList[]{arrayList3});
            ArrayList arrayList4 = new ArrayList();
            arrayList4.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 20, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a6 = qd6.a((T[]) new ArrayList[]{arrayList4});
            ArrayList arrayList5 = new ArrayList();
            arrayList5.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 6, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a7 = qd6.a((T[]) new ArrayList[]{arrayList5});
            ArrayList arrayList6 = new ArrayList();
            arrayList6.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 100, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a8 = qd6.a((T[]) new ArrayList[]{arrayList6});
            ArrayList arrayList7 = new ArrayList();
            arrayList7.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 250, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a9 = qd6.a((T[]) new ArrayList[]{arrayList7});
            ArrayList arrayList8 = new ArrayList();
            arrayList8.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 280, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a10 = qd6.a((T[]) new ArrayList[]{arrayList8});
            ArrayList arrayList9 = new ArrayList();
            arrayList9.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 272, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a11 = qd6.a((T[]) new ArrayList[]{arrayList9});
            ArrayList arrayList10 = new ArrayList();
            arrayList10.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 79, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a12 = qd6.a((T[]) new ArrayList[]{arrayList10});
            ArrayList arrayList11 = new ArrayList();
            arrayList11.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 10, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a13 = qd6.a((T[]) new ArrayList[]{arrayList11});
            ArrayList arrayList12 = new ArrayList();
            arrayList12.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 5, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a14 = qd6.a((T[]) new ArrayList[]{arrayList12});
            ArrayList arrayList13 = new ArrayList();
            arrayList13.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 82, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a15 = qd6.a((T[]) new ArrayList[]{arrayList13});
            ArrayList arrayList14 = new ArrayList();
            arrayList14.add(new BarChart.b(-1, BarChart.f.DEFAULT, 0, 5, new RectF(0.0f, 0.0f, 0.0f, 0.0f)));
            ArrayList a16 = qd6.a((T[]) new ArrayList[]{arrayList14});
            ArrayList arrayList15 = new ArrayList();
            arrayList15.add(new BarChart.a(-1, a3, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a4, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a5, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a6, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a7, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a8, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a9, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a10, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a11, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a12, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a13, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a14, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a15, -1, false, 8, (qg6) null));
            arrayList15.add(new BarChart.a(-1, a16, -1, false, 8, (qg6) null));
            BarChart.c cVar = new BarChart.c(300, 150, arrayList15);
            if (a2 != null && (overviewDayChart = a2.q) != null) {
                overviewDayChart.b((ut4) cVar);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        r74 a2 = kb.a(LayoutInflater.from(getContext()), 2131558523, (ViewGroup) null, false, e1());
        PortfolioApp.get.instance().g().a(new rm5()).a(this);
        w04 w04 = this.f;
        if (w04 != null) {
            CustomizeActiveCaloriesChartViewModel a3 = vd.a(this, w04).a(CustomizeActiveCaloriesChartViewModel.class);
            wg6.a((Object) a3, "ViewModelProviders.of(th\u2026artViewModel::class.java)");
            this.g = a3;
            CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.g;
            if (customizeActiveCaloriesChartViewModel != null) {
                customizeActiveCaloriesChartViewModel.b().a(getViewLifecycleOwner(), new b(this));
                CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel2 = this.g;
                if (customizeActiveCaloriesChartViewModel2 != null) {
                    customizeActiveCaloriesChartViewModel2.c();
                    this.h = new ax5<>(this, a2);
                    j1();
                    wg6.a((Object) a2, "binding");
                    return a2.d();
                }
                wg6.d("mViewModel");
                throw null;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    public void onDestroy() {
        CustomizeActiveCaloriesChartFragment.super.onDestroy();
        FLogger.INSTANCE.getLocal().d(j, "onDestroy");
        o = null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CustomizeActiveCaloriesChartFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d(j, "onResume");
        CustomizeActiveCaloriesChartViewModel customizeActiveCaloriesChartViewModel = this.g;
        if (customizeActiveCaloriesChartViewModel != null) {
            customizeActiveCaloriesChartViewModel.c();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<r74> ax5 = this.h;
        if (ax5 != null) {
            r74 a2 = ax5.a();
            if (a2 != null) {
                a2.s.setOnClickListener(new c(this));
                a2.r.setOnClickListener(new d(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void p(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "onDialogDismissed dialogId=" + i2);
    }

    @DexIgnore
    public final void r(int i2) {
        ax5<r74> ax5 = this.h;
        if (ax5 != null) {
            r74 a2 = ax5.a();
            if (a2 != null) {
                a2.q.setGraphPreviewColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public final void s(int i2) {
        ax5<r74> ax5 = this.h;
        if (ax5 != null) {
            r74 a2 = ax5.a();
            if (a2 != null) {
                a2.t.setBackgroundColor(i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }
}
