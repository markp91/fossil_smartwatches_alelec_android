package com.portfolio.platform.uirenew.home.dashboard.activetime.overview;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.fb5;
import com.fossil.ff4;
import com.fossil.fi4;
import com.fossil.gb5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lc6;
import com.fossil.nh6;
import com.fossil.pc6;
import com.fossil.qg6;
import com.fossil.t54;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.yk4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailActivity;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActiveTimeOverviewDayFragment extends BaseFragment implements gb5 {
    @DexIgnore
    public ax5<t54> f;
    @DexIgnore
    public fb5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public static /* final */ b a; // = new b();

        @DexIgnore
        public final void onClick(View view) {
            ActiveTimeDetailActivity.a aVar = ActiveTimeDetailActivity.D;
            Date date = new Date();
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(date, context);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void b(ut4 ut4, ArrayList<String> arrayList) {
        t54 a2;
        OverviewDayChart overviewDayChart;
        wg6.b(ut4, "baseModel");
        wg6.b(arrayList, "arrayLegend");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("ActiveTimeOverviewDayFragment", "showDayDetails - baseModel=" + ut4);
        ax5<t54> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.q) != null) {
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            if (!arrayList.isEmpty()) {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) arrayList, false, 2, (Object) null);
            } else {
                BarChart.a((BarChart) overviewDayChart, (ArrayList) yk4.b.a(), false, 2, (Object) null);
            }
            overviewDayChart.a(ut4);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "ActiveTimeOverviewDayFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        t54 a2;
        OverviewDayChart overviewDayChart;
        ax5<t54> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewDayChart = a2.q) != null) {
            fb5 fb5 = this.g;
            if ((fb5 != null ? fb5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                overviewDayChart.a("dianaActiveMinutesTab", "nonBrandNonReachGoal");
            } else {
                overviewDayChart.a("hybridActiveMinutesTab", "nonBrandNonReachGoal");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        t54 a2;
        wg6.b(layoutInflater, "inflater");
        ActiveTimeOverviewDayFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onCreateView");
        t54 a3 = kb.a(layoutInflater, 2131558496, viewGroup, false, e1());
        a3.r.setOnClickListener(b.a);
        this.f = new ax5<>(this, a3);
        j1();
        ax5<t54> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ActiveTimeOverviewDayFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onResume");
        j1();
        fb5 fb5 = this.g;
        if (fb5 != null) {
            fb5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        ActiveTimeOverviewDayFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onStop");
        fb5 fb5 = this.g;
        if (fb5 != null) {
            fb5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("ActiveTimeOverviewDayFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(boolean z, List<WorkoutSession> list) {
        t54 a2;
        View d;
        View d2;
        wg6.b(list, "workoutSessions");
        ax5<t54> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                LinearLayout linearLayout = a2.u;
                wg6.a((Object) linearLayout, "it.llWorkout");
                linearLayout.setVisibility(0);
                int size = list.size();
                a(a2.s, list.get(0));
                if (size == 1) {
                    ff4 ff4 = a2.t;
                    if (ff4 != null && (d2 = ff4.d()) != null) {
                        d2.setVisibility(8);
                        return;
                    }
                    return;
                }
                ff4 ff42 = a2.t;
                if (!(ff42 == null || (d = ff42.d()) == null)) {
                    d.setVisibility(0);
                }
                a(a2.t, list.get(1));
                return;
            }
            LinearLayout linearLayout2 = a2.u;
            wg6.a((Object) linearLayout2, "it.llWorkout");
            linearLayout2.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(fb5 fb5) {
        wg6.b(fb5, "presenter");
        this.g = fb5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void a(ff4 ff4, WorkoutSession workoutSession) {
        String str;
        String str2;
        if (ff4 != null) {
            View d = ff4.d();
            wg6.a((Object) d, "binding.root");
            Context context = d.getContext();
            lc6<Integer, Integer> a2 = fi4.Companion.a(workoutSession.getWorkoutType());
            String a3 = jm4.a(context, a2.getSecond().intValue());
            ff4.t.setImageResource(a2.getFirst().intValue());
            Object r1 = ff4.r;
            wg6.a((Object) r1, "it.ftvWorkoutTitle");
            r1.setText(a3);
            pc6<Integer, Integer, Integer> c = bk4.c(workoutSession.getDuration());
            wg6.a((Object) c, "DateHelper.getTimeValues(workoutSession.duration)");
            Object r2 = ff4.s;
            wg6.a((Object) r2, "it.ftvWorkoutValue");
            if (wg6.a(c.getFirst().intValue(), 0) > 0) {
                nh6 nh6 = nh6.a;
                String a4 = jm4.a(context, 2131886485);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
                Object[] objArr = {c.getFirst(), c.getSecond(), c.getThird()};
                str = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) str, "java.lang.String.format(format, *args)");
            } else if (wg6.a(c.getSecond().intValue(), 0) > 0) {
                nh6 nh62 = nh6.a;
                String a5 = jm4.a(context, 2131886486);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
                Object[] objArr2 = {c.getSecond(), c.getThird()};
                str = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) str, "java.lang.String.format(format, *args)");
            } else {
                nh6 nh63 = nh6.a;
                String a6 = jm4.a(context, 2131886487);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
                Object[] objArr3 = {c.getThird()};
                str = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                wg6.a((Object) str, "java.lang.String.format(format, *args)");
            }
            r2.setText(str);
            Object r0 = ff4.q;
            wg6.a((Object) r0, "it.ftvWorkoutTime");
            r0.setText(bk4.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond()));
            fb5 fb5 = this.g;
            if ((fb5 != null ? fb5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                str2 = ThemeManager.l.a().b("dianaActiveMinutesTab");
            } else {
                str2 = ThemeManager.l.a().b("hybridActiveMinutesTab");
            }
            if (str2 != null) {
                ff4.t.setColorFilter(Color.parseColor(str2));
            }
        }
    }
}
