package com.portfolio.platform.uirenew.home.customize.diana.complications.details;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.ax5;
import com.fossil.cu4;
import com.fossil.d74;
import com.fossil.fg4;
import com.fossil.gy5;
import com.fossil.h34;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.nh4;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.y45;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.z45;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressActivity;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsFragment extends BaseFragment implements z45, h34.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<d74> f;
    @DexIgnore
    public y45 g;
    @DexIgnore
    public cu4 h;
    @DexIgnore
    public h34 i;
    @DexIgnore
    public String j;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CommuteTimeSettingsFragment a() {
            return new CommuteTimeSettingsFragment();
        }

        @DexIgnore
        public final String b() {
            return CommuteTimeSettingsFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements cu4.b {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ d74 b;

        @DexIgnore
        public b(CommuteTimeSettingsFragment commuteTimeSettingsFragment, d74 d74) {
            this.a = commuteTimeSettingsFragment;
            this.b = d74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public void a(String str) {
            wg6.b(str, "address");
            this.b.q.setText(str, false);
            this.a.Z(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public c(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public d(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).k();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public e(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).l();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public f(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).m();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ d74 b;

        @DexIgnore
        public g(View view, d74 d74) {
            this.a = view;
            this.b = d74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.z.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                d74 d74 = this.b;
                wg6.a((Object) d74, "binding");
                d74.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = CommuteTimeSettingsFragment.q.b();
                local.d(b2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                Object r0 = this.b.q;
                wg6.a((Object) r0, "binding.autocompletePlaces");
                r0.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ d74 b;

        @DexIgnore
        public h(CommuteTimeSettingsFragment commuteTimeSettingsFragment, d74 d74) {
            this.a = commuteTimeSettingsFragment;
            this.b = d74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v9, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            h34 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText((CharacterStyle) null);
                wg6.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String b2 = CommuteTimeSettingsFragment.q.b();
                local.i(b2, "Autocomplete item selected: " + fullText);
                String spannableString = fullText.toString();
                wg6.a((Object) spannableString, "primaryText.toString()");
                if (!TextUtils.isEmpty(spannableString)) {
                    this.a.Z(spannableString);
                    this.b.q.setText(spannableString, false);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ d74 b;

        @DexIgnore
        public i(CommuteTimeSettingsFragment commuteTimeSettingsFragment, d74 d74) {
            this.a = commuteTimeSettingsFragment;
            this.b = d74;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.b.r;
            if (!wg6.a((Object) this.a.j1(), (Object) String.valueOf(editable))) {
                this.a.Z((String) null);
            }
            ImageView imageView2 = this.b.r;
            wg6.a((Object) imageView2, "binding.closeIv");
            imageView2.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public j(CommuteTimeSettingsFragment commuteTimeSettingsFragment, d74 d74) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wg6.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.i1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public k(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).b("travel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public l(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.B;
            CommuteTimeSettingsFragment commuteTimeSettingsFragment = this.a;
            CommuteTimeSetting i = CommuteTimeSettingsFragment.b(commuteTimeSettingsFragment).i();
            if (i == null || (str = i.getAddress()) == null) {
                str = "";
            }
            aVar.a(commuteTimeSettingsFragment, 0.0d, 0.0d, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class m implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public m(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.i1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class n implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ d74 b;

        @DexIgnore
        public n(CommuteTimeSettingsFragment commuteTimeSettingsFragment, d74 d74) {
            this.a = commuteTimeSettingsFragment;
            this.b = d74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onClick(View view) {
            this.b.q.setText("", false);
            CommuteTimeSettingsFragment.b(this.a).h();
            this.a.k1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class o implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsFragment a;

        @DexIgnore
        public o(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
            this.a = commuteTimeSettingsFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CommuteTimeSettingsFragment.b(this.a).b("eta");
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsFragment::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ y45 b(CommuteTimeSettingsFragment commuteTimeSettingsFragment) {
        y45 y45 = commuteTimeSettingsFragment.g;
        if (y45 != null) {
            return y45;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void A(String str) {
        wg6.b(str, "userCurrentAddress");
        if (isActive()) {
            ax5<d74> ax5 = this.f;
            if (ax5 != null) {
                d74 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.q;
                    wg6.a((Object) r1, "it.autocompletePlaces");
                    Editable text = r1.getText();
                    wg6.a((Object) text, "it.autocompletePlaces.text");
                    boolean z = true;
                    if (yj6.d(text).length() == 0) {
                        if (str.length() <= 0) {
                            z = false;
                        }
                        if (z) {
                            a2.q.setText(str, false);
                            this.j = str;
                            return;
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void G(String str) {
        fg4 fg4;
        wg6.b(str, "userWorkDefaultAddress");
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null && (fg4 = a2.v) != null) {
                Object r2 = fg4.r;
                wg6.a((Object) r2, "workButton.ftvContent");
                r2.setText(str);
                Context context = getContext();
                if (context != null) {
                    int a3 = (int) gy5.a(12, context);
                    fg4.t.setPadding(a3, a3, a3, a3);
                    if (!TextUtils.isEmpty(str)) {
                        ImageButton imageButton = fg4.t;
                        wg6.a((Object) imageButton, "workButton.ivEditButton");
                        imageButton.setImageTintList(ColorStateList.valueOf(w6.a(PortfolioApp.get.instance(), R.color.activeColorPrimary)));
                        fg4.t.setImageResource(2131231060);
                        return;
                    }
                    ImageButton imageButton2 = fg4.t;
                    wg6.a((Object) imageButton2, "workButton.ivEditButton");
                    imageButton2.setImageTintList(ColorStateList.valueOf(w6.a(PortfolioApp.get.instance(), 2131099811)));
                    fg4.t.setImageResource(2131231060);
                    return;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void O(boolean z) {
        FlexibleSwitchCompat flexibleSwitchCompat;
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null && (flexibleSwitchCompat = a2.E) != null) {
                wg6.a((Object) flexibleSwitchCompat, "it");
                flexibleSwitchCompat.setChecked(z);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    public void P(boolean z) {
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null) {
                if (z) {
                    Object r3 = a2.q;
                    wg6.a((Object) r3, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(r3.getText())) {
                        this.j = null;
                        l1();
                        return;
                    }
                }
                k1();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final void Z(String str) {
        this.j = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void a0(String str) {
        wg6.b(str, "address");
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null) {
                if (str.length() > 0) {
                    a2.q.setText(str, false);
                    this.j = str;
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void c(String str, String str2) {
        wg6.b(str, "addressType");
        wg6.b(str2, "address");
        Bundle bundle = new Bundle();
        bundle.putString("KEY_DEFAULT_PLACE", str2);
        bundle.putString("AddressType", str);
        int hashCode = str.hashCode();
        if (hashCode != 2255103) {
            if (hashCode == 76517104 && str.equals("Other")) {
                h34 h34 = this.i;
                if (h34 != null) {
                    h34.a((h34.b) null);
                }
                CommuteTimeSettingsDefaultAddressActivity.C.b(this, bundle);
            }
        } else if (str.equals("Home")) {
            h34 h342 = this.i;
            if (h342 != null) {
                h342.a((h34.b) null);
            }
            CommuteTimeSettingsDefaultAddressActivity.C.a(this, bundle);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat, java.lang.Object] */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x001d, code lost:
        if (r2 != null) goto L_0x0028;
     */
    @DexIgnore
    public boolean i1() {
        String str;
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 == null) {
                return true;
            }
            String str2 = this.j;
            if (str2 != null) {
                if (str2 != null) {
                    str = yj6.d(str2).toString();
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
            str = "";
            String str3 = str;
            ImageView imageView = a2.w;
            wg6.a((Object) imageView, "it.ivArrivalTime");
            String str4 = imageView.getAlpha() == 1.0f ? "eta" : "travel";
            y45 y45 = this.g;
            if (y45 != null) {
                Object r1 = a2.q;
                wg6.a((Object) r1, "it.autocompletePlaces");
                String obj = r1.getText().toString();
                if (obj != null) {
                    String obj2 = yj6.d(obj).toString();
                    nh4 nh4 = nh4.CAR;
                    Object r0 = a2.E;
                    wg6.a((Object) r0, "it.scAvoidTolls");
                    y45.a(str3, obj2, nh4, r0.isChecked(), str4);
                    return true;
                }
                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
            }
            wg6.d("mPresenter");
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void j(List<String> list) {
        LinearLayout linearLayout;
        LinearLayout linearLayout2;
        wg6.b(list, "recentSearchedList");
        k1();
        if (!list.isEmpty()) {
            cu4 cu4 = this.h;
            if (cu4 != null) {
                cu4.a((List<String>) yd6.d(list));
            }
            ax5<d74> ax5 = this.f;
            if (ax5 != null) {
                d74 a2 = ax5.a();
                if (a2 != null && (linearLayout2 = a2.B) != null) {
                    linearLayout2.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        ax5<d74> ax52 = this.f;
        if (ax52 != null) {
            d74 a3 = ax52.a();
            if (a3 != null && (linearLayout = a3.B) != null) {
                linearLayout.setVisibility(4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final String j1() {
        return this.j;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void k1() {
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.s;
                wg6.a((Object) r1, "it.ftvAddressError");
                r1.setVisibility(8);
                LinearLayout linearLayout = a2.B;
                wg6.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(0);
                fg4 fg4 = a2.u;
                wg6.a((Object) fg4, "it.icHome");
                View d2 = fg4.d();
                wg6.a((Object) d2, "it.icHome.root");
                d2.setVisibility(0);
                fg4 fg42 = a2.v;
                wg6.a((Object) fg42, "it.icWork");
                View d3 = fg42.d();
                wg6.a((Object) d3, "it.icWork.root");
                d3.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void l1() {
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.s;
                wg6.a((Object) r1, "it.ftvAddressError");
                Object instance = PortfolioApp.get.instance();
                Object r6 = a2.q;
                wg6.a((Object) r6, "it.autocompletePlaces");
                r1.setText(instance.getString(2131886193, new Object[]{r6.getText()}));
                Object r12 = a2.s;
                wg6.a((Object) r12, "it.ftvAddressError");
                r12.setVisibility(0);
                LinearLayout linearLayout = a2.B;
                wg6.a((Object) linearLayout, "it.llRecentContainer");
                linearLayout.setVisibility(8);
                fg4 fg4 = a2.u;
                wg6.a((Object) fg4, "it.icHome");
                View d2 = fg4.d();
                wg6.a((Object) d2, "it.icHome.root");
                d2.setVisibility(8);
                fg4 fg42 = a2.v;
                wg6.a((Object) fg42, "it.icWork");
                View d3 = fg42.d();
                wg6.a((Object) d3, "it.icWork.root");
                d3.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        String str;
        String str2;
        CommuteTimeSettingsFragment.super.onActivityResult(i2, i3, intent);
        if (intent != null) {
            String str3 = null;
            if (i2 == 100) {
                Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
                if (bundleExtra != null) {
                    str3 = bundleExtra.getString("address");
                }
                if (str3 != null) {
                    a0(str3);
                }
            } else if (i2 == 111) {
                String stringExtra = intent.getStringExtra("KEY_DEFAULT_PLACE");
                if (stringExtra != null) {
                    str = stringExtra;
                } else {
                    str = "";
                }
                z(str);
                y45 y45 = this.g;
                if (y45 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    y45.a(stringExtra);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            } else if (i2 == 112) {
                String stringExtra2 = intent.getStringExtra("KEY_DEFAULT_PLACE");
                if (stringExtra2 != null) {
                    str2 = stringExtra2;
                } else {
                    str2 = "";
                }
                G(str2);
                y45 y452 = this.g;
                if (y452 != null) {
                    if (stringExtra2 == null) {
                        stringExtra2 = "";
                    }
                    y452.a(stringExtra2);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v4, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r5v5, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r6v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        d74 a2 = kb.a(layoutInflater, 2131558514, viewGroup, false, e1());
        String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            a2.A.setBackgroundColor(Color.parseColor(b2));
        }
        Object r5 = a2.q;
        r5.setDropDownBackgroundDrawable(w6.c(r5.getContext(), 2131230823));
        r5.setOnItemClickListener(new h(this, a2));
        r5.addTextChangedListener(new i(this, a2));
        r5.setOnKeyListener(new j(this, a2));
        a2.t.setOnClickListener(new m(this));
        fg4 fg4 = a2.u;
        fg4.u.setImageResource(2131231085);
        Object r6 = fg4.s;
        wg6.a((Object) r6, "it.ftvTitle");
        r6.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886188));
        fg4.q.setOnClickListener(new c(this));
        fg4.t.setOnClickListener(new d(this));
        fg4 fg42 = a2.v;
        fg42.u.setImageResource(2131231086);
        Object r62 = fg42.s;
        wg6.a((Object) r62, "it.ftvTitle");
        r62.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886189));
        fg42.q.setOnClickListener(new e(this));
        fg42.t.setOnClickListener(new f(this));
        a2.r.setOnClickListener(new n(this, a2));
        cu4 cu4 = new cu4();
        cu4.a((cu4.b) new b(this, a2));
        this.h = cu4;
        RecyclerView recyclerView = a2.D;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        recyclerView.setAdapter(this.h);
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new g(d2, a2));
        a2.w.setOnClickListener(new o(this));
        a2.y.setOnClickListener(new k(this));
        a2.x.setOnClickListener(new l(this));
        this.f = new ax5<>(this, a2);
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        y45 y45 = this.g;
        if (y45 != null) {
            y45.g();
            CommuteTimeSettingsFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        CommuteTimeSettingsFragment.super.onResume();
        y45 y45 = this.g;
        if (y45 != null) {
            y45.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void u(boolean z) {
        if (z) {
            Intent intent = new Intent();
            y45 y45 = this.g;
            if (y45 != null) {
                intent.putExtra("COMMUTE_TIME_SETTING", y45.i());
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.setResult(-1, intent);
                }
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.supportFinishAfterTransition();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void z(String str) {
        fg4 fg4;
        wg6.b(str, "userHomeDefaultAddress");
        ax5<d74> ax5 = this.f;
        if (ax5 != null) {
            d74 a2 = ax5.a();
            if (a2 != null && (fg4 = a2.u) != null) {
                Object r2 = fg4.r;
                wg6.a((Object) r2, "homeButton.ftvContent");
                r2.setText(str);
                Context context = getContext();
                if (context != null) {
                    int a3 = (int) gy5.a(12, context);
                    fg4.t.setPadding(a3, a3, a3, a3);
                    if (!TextUtils.isEmpty(str)) {
                        ImageButton imageButton = fg4.t;
                        wg6.a((Object) imageButton, "homeButton.ivEditButton");
                        imageButton.setImageTintList(ColorStateList.valueOf(w6.a(PortfolioApp.get.instance(), 2131100008)));
                        fg4.t.setImageResource(2131231060);
                        return;
                    }
                    ImageButton imageButton2 = fg4.t;
                    wg6.a((Object) imageButton2, "homeButton.ivEditButton");
                    imageButton2.setImageTintList(ColorStateList.valueOf(w6.a(PortfolioApp.get.instance(), 2131099811)));
                    fg4.t.setImageResource(2131231060);
                    return;
                }
                wg6.a();
                throw null;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(y45 y45) {
        wg6.b(y45, "presenter");
        this.g = y45;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                this.i = new h34(context, placesClient);
                h34 h34 = this.i;
                if (h34 != null) {
                    h34.a((h34.b) this);
                }
                ax5<d74> ax5 = this.f;
                if (ax5 != null) {
                    d74 a2 = ax5.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.i);
                        Object r0 = a2.q;
                        wg6.a((Object) r0, "it.autocompletePlaces");
                        Editable text = r0.getText();
                        wg6.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = yj6.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2, false);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        k1();
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(CommuteTimeSetting commuteTimeSetting) {
        FLogger.INSTANCE.getLocal().d(p, "showCommuteTimeSettings");
        String format = commuteTimeSetting != null ? commuteTimeSetting.getFormat() : null;
        if (format != null) {
            int hashCode = format.hashCode();
            if (hashCode != -865698022) {
                if (hashCode == 100754 && format.equals("eta")) {
                    ax5<d74> ax5 = this.f;
                    if (ax5 != null) {
                        d74 a2 = ax5.a();
                        if (a2 != null) {
                            ImageView imageView = a2.w;
                            wg6.a((Object) imageView, "it.ivArrivalTime");
                            imageView.setAlpha(1.0f);
                            ImageView imageView2 = a2.y;
                            wg6.a((Object) imageView2, "it.ivTravelTime");
                            imageView2.setAlpha(0.5f);
                            return;
                        }
                        return;
                    }
                    wg6.d("mBinding");
                    throw null;
                }
            } else if (format.equals("travel")) {
                ax5<d74> ax52 = this.f;
                if (ax52 != null) {
                    d74 a3 = ax52.a();
                    if (a3 != null) {
                        ImageView imageView3 = a3.y;
                        wg6.a((Object) imageView3, "it.ivTravelTime");
                        imageView3.setAlpha(1.0f);
                        ImageView imageView4 = a3.w;
                        wg6.a((Object) imageView4, "it.ivArrivalTime");
                        imageView4.setAlpha(0.5f);
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
        }
    }
}
