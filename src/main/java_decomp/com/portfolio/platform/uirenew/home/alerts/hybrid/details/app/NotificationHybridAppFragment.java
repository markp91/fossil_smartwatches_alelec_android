package com.portfolio.platform.uirenew.home.alerts.hybrid.details.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.AppWrapper;
import com.fossil.ax5;
import com.fossil.dc4;
import com.fossil.g25;
import com.fossil.h25;
import com.fossil.iv4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.vx4;
import com.fossil.wg6;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHybridAppFragment extends BaseFragment implements h25, AlertDialogFragment.g {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public g25 f;
    @DexIgnore
    public iv4 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationHybridAppFragment.i;
        }

        @DexIgnore
        public final NotificationHybridAppFragment b() {
            return new NotificationHybridAppFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iv4.a {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridAppFragment a;

        @DexIgnore
        public b(NotificationHybridAppFragment notificationHybridAppFragment) {
            this.a = notificationHybridAppFragment;
        }

        @DexIgnore
        public void a(AppWrapper appWrapper, boolean z) {
            wg6.b(appWrapper, "appWrapper");
            if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == NotificationHybridAppFragment.b(this.a).h()) {
                NotificationHybridAppFragment.b(this.a).a(appWrapper, z);
                return;
            }
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = this.a.getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            String title = installedApp != null ? installedApp.getTitle() : null;
            if (title != null) {
                lx5.a(childFragmentManager, title, appWrapper.getCurrentHandGroup(), NotificationHybridAppFragment.b(this.a).h(), appWrapper);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridAppFragment a;

        @DexIgnore
        public c(NotificationHybridAppFragment notificationHybridAppFragment) {
            this.a = notificationHybridAppFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            NotificationHybridAppFragment.b(this.a).i();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationHybridAppFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ dc4 b;

        @DexIgnore
        public d(NotificationHybridAppFragment notificationHybridAppFragment, dc4 dc4) {
            this.a = notificationHybridAppFragment;
            this.b = dc4;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            ImageView imageView = this.b.s;
            wg6.a((Object) imageView, "binding.ivClear");
            imageView.setVisibility(i3 == 0 ? 4 : 0);
            NotificationHybridAppFragment.a(this.a).getFilter().filter(String.valueOf(charSequence));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnFocusChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ dc4 a;

        @DexIgnore
        public e(dc4 dc4) {
            this.a = dc4;
        }

        @DexIgnore
        public final void onFocusChange(View view, boolean z) {
            if (!z) {
                dc4 dc4 = this.a;
                wg6.a((Object) dc4, "binding");
                dc4.d().requestFocus();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ dc4 a;

        @DexIgnore
        public f(dc4 dc4) {
            this.a = dc4;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        public final void onClick(View view) {
            this.a.q.setText("");
        }
    }

    /*
    static {
        String simpleName = NotificationHybridAppFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridAppFra\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ iv4 a(NotificationHybridAppFragment notificationHybridAppFragment) {
        iv4 iv4 = notificationHybridAppFragment.g;
        if (iv4 != null) {
            return iv4;
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ g25 b(NotificationHybridAppFragment notificationHybridAppFragment) {
        g25 g25 = notificationHybridAppFragment.f;
        if (g25 != null) {
            return g25;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void d() {
        a();
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e() {
        b();
    }

    @DexIgnore
    public boolean i1() {
        g25 g25 = this.f;
        if (g25 != null) {
            g25.i();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v3, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        dc4 a2 = kb.a(layoutInflater, 2131558582, viewGroup, false, e1());
        a2.r.setOnClickListener(new c(this));
        a2.q.addTextChangedListener(new d(this, a2));
        a2.q.setOnFocusChangeListener(new e(a2));
        a2.s.setOnClickListener(new f(a2));
        iv4 iv4 = new iv4();
        iv4.a((iv4.a) new b(this));
        this.g = iv4;
        RecyclerView recyclerView = a2.v;
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        iv4 iv42 = this.g;
        if (iv42 != null) {
            recyclerView.setAdapter(iv42);
            String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b2)) {
                a2.t.setBackgroundColor(Color.parseColor(b2));
            }
            new ax5(this, a2);
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mAdapter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        g25 g25 = this.f;
        if (g25 != null) {
            g25.g();
            NotificationHybridAppFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationHybridAppFragment.super.onResume();
        g25 g25 = this.f;
        if (g25 != null) {
            g25.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(g25 g25) {
        wg6.b(g25, "presenter");
        this.f = g25;
    }

    @DexIgnore
    public void a(List<vx4> list, int i2) {
        wg6.b(list, "listAppWrapper");
        iv4 iv4 = this.g;
        if (iv4 != null) {
            iv4.a(list, i2);
        } else {
            wg6.d("mAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void a(ArrayList<String> arrayList) {
        wg6.b(arrayList, "uriAppsSelected");
        Intent intent = new Intent();
        intent.putStringArrayListExtra("APP_DATA", arrayList);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.setResult(-1, intent);
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        AppWrapper appWrapper;
        AppWrapper appWrapper2;
        wg6.b(str, "tag");
        if (str.hashCode() != -1984760733 || !str.equals("CONFIRM_REASSIGN_APP")) {
            return;
        }
        if (i2 != 2131363190) {
            Bundle extras = intent != null ? intent.getExtras() : null;
            if (extras != null && (appWrapper2 = (AppWrapper) extras.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                g25 g25 = this.f;
                if (g25 != null) {
                    g25.a(appWrapper2, false);
                    iv4 iv4 = this.g;
                    if (iv4 != null) {
                        iv4.notifyDataSetChanged();
                    } else {
                        wg6.d("mAdapter");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            Bundle extras2 = intent != null ? intent.getExtras() : null;
            if (extras2 != null && (appWrapper = (AppWrapper) extras2.getSerializable("CONFIRM_REASSIGN_APPWRAPPER")) != null) {
                g25 g252 = this.f;
                if (g252 != null) {
                    g252.a(appWrapper, true);
                    iv4 iv42 = this.g;
                    if (iv42 != null) {
                        iv42.notifyDataSetChanged();
                    } else {
                        wg6.d("mAdapter");
                        throw null;
                    }
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
    }
}
