package com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.h34;
import com.fossil.h74;
import com.fossil.kb;
import com.fossil.ld;
import com.fossil.lx5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.v75;
import com.fossil.vd;
import com.fossil.w04;
import com.fossil.w6;
import com.fossil.w75;
import com.fossil.wg6;
import com.fossil.yj6;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.commutetime.settings.CommuteTimeSettingsDetailViewModel;
import com.portfolio.platform.uirenew.mappicker.MapPickerActivity;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDetailFragment extends BaseFragment implements h34.b {
    @DexIgnore
    public static /* final */ String p;
    @DexIgnore
    public static /* final */ a q; // = new a((qg6) null);
    @DexIgnore
    public ax5<h74> f;
    @DexIgnore
    public CommuteTimeSettingsDetailViewModel g;
    @DexIgnore
    public w04 h;
    @DexIgnore
    public h34 i;
    @DexIgnore
    public Boolean j; // = false;
    @DexIgnore
    public HashMap o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return CommuteTimeSettingsDetailFragment.p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final CommuteTimeSettingsDetailFragment a(AddressWrapper addressWrapper, ArrayList<String> arrayList, boolean z) {
            CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment = new CommuteTimeSettingsDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putParcelable("KEY_BUNDLE_SETTING_ADDRESS", addressWrapper);
            bundle.putStringArrayList("KEY_BUNDLE_LIST_ADDRESS", arrayList);
            bundle.putBoolean("KEY_BUNDLE_IS_MAP_RESULT", z);
            commuteTimeSettingsDetailFragment.setArguments(bundle);
            return commuteTimeSettingsDetailFragment;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ View a;
        @DexIgnore
        public /* final */ /* synthetic */ h74 b;

        @DexIgnore
        public b(View view, h74 h74) {
            this.a = view;
            this.b = h74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
        public final void onGlobalLayout() {
            Rect rect = new Rect();
            this.a.getWindowVisibleDisplayFrame(rect);
            int height = this.a.getHeight();
            if (((double) (height - rect.bottom)) > ((double) height) * 0.15d) {
                int[] iArr = new int[2];
                this.b.x.getLocationOnScreen(iArr);
                Rect rect2 = new Rect();
                h74 h74 = this.b;
                wg6.a((Object) h74, "binding");
                h74.d().getWindowVisibleDisplayFrame(rect2);
                int i = rect2.bottom - iArr[1];
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = CommuteTimeSettingsDetailFragment.q.a();
                local.d(a2, "observeKeyboard - isOpen=" + true + " - dropDownHeight=" + i);
                Object r0 = this.b.q;
                wg6.a((Object) r0, "binding.autocompletePlaces");
                r0.setDropDownHeight(i);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public c(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment, h74 h74) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            AutocompletePrediction item;
            h34 a2 = this.a.i;
            if (a2 != null && (item = a2.getItem(i)) != null) {
                SpannableString fullText = item.getFullText((CharacterStyle) null);
                wg6.a((Object) fullText, "item.getFullText(null)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a3 = CommuteTimeSettingsDetailFragment.q.a();
                local.i(a3, "Autocomplete item selected: " + fullText);
                CommuteTimeSettingsDetailViewModel b = CommuteTimeSettingsDetailFragment.b(this.a);
                String spannableString = fullText.toString();
                wg6.a((Object) spannableString, "primaryText.toString()");
                if (spannableString != null) {
                    String obj = yj6.d(spannableString).toString();
                    String placeId = item.getPlaceId();
                    h34 a4 = this.a.i;
                    if (a4 != null) {
                        b.a(obj, placeId, a4.b());
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ h74 a;

        @DexIgnore
        public d(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment, h74 h74) {
            this.a = h74;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            ImageView imageView = this.a.s;
            wg6.a((Object) imageView, "binding.closeIv");
            imageView.setVisibility(TextUtils.isEmpty(editable) ? 8 : 0);
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnKeyListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public e(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment, h74 h74) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final boolean onKey(View view, int i, KeyEvent keyEvent) {
            wg6.a((Object) keyEvent, "keyEvent");
            if (keyEvent.getAction() != 1 || i != 4) {
                return false;
            }
            this.a.i1();
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public f(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            CommuteTimeSettingsDetailViewModel b = CommuteTimeSettingsDetailFragment.b(this.a);
            String valueOf = String.valueOf(charSequence);
            if (valueOf != null) {
                String obj = yj6.d(valueOf).toString();
                if (obj != null) {
                    String upperCase = obj.toUpperCase();
                    wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
                    b.a(upperCase);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public g(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            this.a.u(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public h(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            if (CommuteTimeSettingsDetailFragment.b(this.a).c()) {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = this.a.getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.t(childFragmentManager);
            } else if (CommuteTimeSettingsDetailFragment.b(this.a).d()) {
                this.a.u(true);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ h74 b;

        @DexIgnore
        public i(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment, h74 h74) {
            this.a = commuteTimeSettingsDetailFragment;
            this.b = h74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r7v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        public final void onClick(View view) {
            CommuteTimeSettingsDetailViewModel.a(CommuteTimeSettingsDetailFragment.b(this.a), (String) null, (String) null, (AutocompleteSessionToken) null, 7, (Object) null);
            this.b.q.setText("");
            this.a.j1();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class j implements CompoundButton.OnCheckedChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public j(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            CommuteTimeSettingsDetailFragment.b(this.a).a(z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class k implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;

        @DexIgnore
        public k(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
            this.a = commuteTimeSettingsDetailFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            String str;
            MapPickerActivity.a aVar = MapPickerActivity.B;
            CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment = this.a;
            AddressWrapper a2 = CommuteTimeSettingsDetailFragment.b(commuteTimeSettingsDetailFragment).a();
            double d = 0.0d;
            double lat = a2 != null ? a2.getLat() : 0.0d;
            AddressWrapper a3 = CommuteTimeSettingsDetailFragment.b(this.a).a();
            if (a3 != null) {
                d = a3.getLng();
            }
            double d2 = d;
            AddressWrapper a4 = CommuteTimeSettingsDetailFragment.b(this.a).a();
            if (a4 == null || (str = a4.getAddress()) == null) {
                str = "";
            }
            aVar.a(commuteTimeSettingsDetailFragment, lat, d2, str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class l<T> implements ld<w75.b> {
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDetailFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ h74 b;

        @DexIgnore
        public l(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment, h74 h74) {
            this.a = commuteTimeSettingsDetailFragment;
            this.b = h74;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v3, types: [java.lang.Object, com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* JADX WARNING: type inference failed for: r1v7, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
        /* JADX WARNING: type inference failed for: r1v9, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
        /* renamed from: a */
        public final void onChanged(CommuteTimeSettingsDetailViewModel.b bVar) {
            PlacesClient d = bVar.d();
            if (d != null) {
                this.a.a(d);
            }
            String c = bVar.c();
            if (c != null) {
                this.b.t.setText(c);
            }
            String a2 = bVar.a();
            if (a2 != null) {
                this.b.q.setText(a2);
            }
            Boolean b2 = bVar.b();
            if (b2 != null) {
                boolean booleanValue = b2.booleanValue();
                FlexibleSwitchCompat flexibleSwitchCompat = this.b.z;
                wg6.a((Object) flexibleSwitchCompat, "binding.scAvoidTolls");
                flexibleSwitchCompat.setChecked(booleanValue);
            }
            Boolean g = bVar.g();
            if (g != null) {
                boolean booleanValue2 = g.booleanValue();
                Object r1 = this.b.t;
                wg6.a((Object) r1, "binding.fetAddressName");
                r1.setEnabled(booleanValue2);
                if (booleanValue2) {
                    this.b.t.requestFocus();
                }
            }
            Boolean h = bVar.h();
            if (h != null) {
                if (h.booleanValue()) {
                    this.b.r.a("flexible_button_primary");
                } else {
                    this.b.r.a("flexible_button_disabled");
                }
            }
            Boolean f = bVar.f();
            if (f != null && f.booleanValue()) {
                this.a.L0();
            }
            Boolean e = bVar.e();
            if (e == null) {
                return;
            }
            if (e.booleanValue()) {
                this.a.b();
            } else {
                this.a.a();
            }
        }
    }

    /*
    static {
        String simpleName = CommuteTimeSettingsDetailFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsDetai\u2026nt::class.java.simpleName");
        p = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ CommuteTimeSettingsDetailViewModel b(CommuteTimeSettingsDetailFragment commuteTimeSettingsDetailFragment) {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = commuteTimeSettingsDetailFragment.g;
        if (commuteTimeSettingsDetailViewModel != null) {
            return commuteTimeSettingsDetailViewModel;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public final void L0() {
        FLogger.INSTANCE.getLocal().d(p, "showLocationError");
        if (isActive()) {
            lx5 lx5 = lx5.c;
            FragmentManager childFragmentManager = getChildFragmentManager();
            wg6.a((Object) childFragmentManager, "childFragmentManager");
            lx5.q(childFragmentManager);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    public void P(boolean z) {
        ax5<h74> ax5 = this.f;
        if (ax5 != null) {
            h74 a2 = ax5.a();
            if (a2 != null) {
                if (z) {
                    Object r3 = a2.q;
                    wg6.a((Object) r3, "it.autocompletePlaces");
                    if (!TextUtils.isEmpty(r3.getText())) {
                        Boolean bool = this.j;
                        if (bool == null) {
                            wg6.a();
                            throw null;
                        } else if (!bool.booleanValue()) {
                            k1();
                            return;
                        }
                    }
                }
                j1();
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.o;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void j1() {
        ax5<h74> ax5 = this.f;
        if (ax5 != null) {
            h74 a2 = ax5.a();
            if (a2 != null) {
                Object r0 = a2.u;
                wg6.a((Object) r0, "it.ftvAddressError");
                r0.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public final void k1() {
        ax5<h74> ax5 = this.f;
        if (ax5 != null) {
            h74 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.u;
                wg6.a((Object) r1, "it.ftvAddressError");
                Object instance = PortfolioApp.get.instance();
                Object r6 = a2.q;
                wg6.a((Object) r6, "it.autocompletePlaces");
                r1.setText(instance.getString(2131886193, new Object[]{r6.getText()}));
                Object r0 = a2.u;
                wg6.a((Object) r0, "it.ftvAddressError");
                r0.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void onActivityResult(int i2, int i3, Intent intent) {
        CommuteTimeSettingsDetailFragment.super.onActivityResult(i2, i3, intent);
        if (intent != null && i2 == 100) {
            Bundle bundleExtra = intent.getBundleExtra(Constants.RESULT);
            String string = bundleExtra != null ? bundleExtra.getString("address") : null;
            Location location = bundleExtra != null ? (Location) bundleExtra.getParcelable("location") : null;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = p;
            StringBuilder sb = new StringBuilder();
            sb.append("addressResult: ");
            sb.append(string);
            sb.append(", lat: ");
            sb.append(location != null ? Double.valueOf(location.getLatitude()) : null);
            sb.append(", lng: ");
            sb.append(location != null ? Double.valueOf(location.getLongitude()) : null);
            local.d(str, sb.toString());
            if (string != null && location != null) {
                CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
                if (commuteTimeSettingsDetailViewModel != null) {
                    commuteTimeSettingsDetailViewModel.a(string, location);
                    this.j = true;
                    return;
                }
                wg6.d("mViewModel");
                throw null;
            }
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        PortfolioApp.get.instance().g().a(new v75()).a(this);
        w04 w04 = this.h;
        Boolean bool = null;
        if (w04 != null) {
            CommuteTimeSettingsDetailViewModel a2 = vd.a(this, w04).a(CommuteTimeSettingsDetailViewModel.class);
            wg6.a((Object) a2, "ViewModelProviders.of(th\u2026ailViewModel::class.java)");
            this.g = a2;
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
            if (commuteTimeSettingsDetailViewModel != null) {
                Bundle arguments = getArguments();
                AddressWrapper addressWrapper = arguments != null ? (AddressWrapper) arguments.getParcelable("KEY_BUNDLE_SETTING_ADDRESS") : null;
                Bundle arguments2 = getArguments();
                commuteTimeSettingsDetailViewModel.a(addressWrapper, arguments2 != null ? arguments2.getStringArrayList("KEY_BUNDLE_LIST_ADDRESS") : null);
                Bundle arguments3 = getArguments();
                if (arguments3 != null) {
                    bool = Boolean.valueOf(arguments3.getBoolean("KEY_BUNDLE_IS_MAP_RESULT"));
                }
                this.j = bool;
                return;
            }
            wg6.d("mViewModel");
            throw null;
        }
        wg6.d("viewModelFactory");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.FlexibleEditText, android.widget.EditText] */
    /* JADX WARNING: type inference failed for: r4v3, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r4v6, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        h74 a2 = kb.a(layoutInflater, 2131558516, viewGroup, false, e1());
        Object r4 = a2.q;
        r4.setDropDownBackgroundDrawable(w6.c(r4.getContext(), 2131230823));
        r4.setOnItemClickListener(new c(this, a2));
        r4.addTextChangedListener(new d(this, a2));
        r4.setOnKeyListener(new e(this, a2));
        a2.t.addTextChangedListener(new f(this));
        a2.v.setOnClickListener(new g(this));
        a2.r.setOnClickListener(new h(this));
        a2.s.setOnClickListener(new i(this, a2));
        a2.z.setOnCheckedChangeListener(new j(this));
        wg6.a((Object) a2, "binding");
        View d2 = a2.d();
        wg6.a((Object) d2, "binding.root");
        d2.getViewTreeObserver().addOnGlobalLayoutListener(new b(d2, a2));
        a2.w.setOnClickListener(new k(this));
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.b().a(getViewLifecycleOwner(), new l(this, a2));
            this.f = new ax5<>(this, a2);
            return a2.d();
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.f();
            CommuteTimeSettingsDetailFragment.super.onPause();
            return;
        }
        wg6.d("mViewModel");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        CommuteTimeSettingsDetailFragment.super.onResume();
        CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
        if (commuteTimeSettingsDetailViewModel != null) {
            commuteTimeSettingsDetailViewModel.e();
        } else {
            wg6.d("mViewModel");
            throw null;
        }
    }

    @DexIgnore
    public final void u(boolean z) {
        if (z) {
            CommuteTimeSettingsDetailViewModel commuteTimeSettingsDetailViewModel = this.g;
            if (commuteTimeSettingsDetailViewModel != null) {
                AddressWrapper a2 = commuteTimeSettingsDetailViewModel.a();
                if (a2 != null) {
                    Intent intent = new Intent();
                    intent.putExtra("KEY_SELECTED_ADDRESS", a2);
                    FragmentActivity activity = getActivity();
                    if (activity != null) {
                        activity.setResult(-1, intent);
                    }
                }
            } else {
                wg6.d("mViewModel");
                throw null;
            }
        }
        FragmentActivity activity2 = getActivity();
        if (activity2 != null) {
            activity2.finish();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, java.lang.Object, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r1v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleAutoCompleteTextView, android.widget.AutoCompleteTextView] */
    public final void a(PlacesClient placesClient) {
        if (isActive()) {
            Context context = getContext();
            if (context != null) {
                wg6.a((Object) context, "context!!");
                this.i = new h34(context, placesClient);
                h34 h34 = this.i;
                if (h34 != null) {
                    h34.a((h34.b) this);
                }
                ax5<h74> ax5 = this.f;
                if (ax5 != null) {
                    h74 a2 = ax5.a();
                    if (a2 != null) {
                        a2.q.setAdapter(this.i);
                        Object r0 = a2.q;
                        wg6.a((Object) r0, "it.autocompletePlaces");
                        Editable text = r0.getText();
                        wg6.a((Object) text, "it.autocompletePlaces.text");
                        CharSequence d2 = yj6.d(text);
                        if (d2.length() > 0) {
                            a2.q.setText(d2);
                            a2.q.setSelection(d2.length());
                            return;
                        }
                        j1();
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
            wg6.a();
            throw null;
        }
    }
}
