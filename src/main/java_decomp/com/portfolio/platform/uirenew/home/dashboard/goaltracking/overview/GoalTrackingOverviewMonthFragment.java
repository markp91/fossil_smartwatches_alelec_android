package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.bk4;
import com.fossil.da4;
import com.fossil.kb;
import com.fossil.pe5;
import com.fossil.qe5;
import com.fossil.qg6;
import com.fossil.vd;
import com.fossil.w6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.goaltracking.GoalTrackingDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewMonthFragment extends BaseFragment implements qe5, RecyclerViewCalendar.b {
    @DexIgnore
    public b06 f;
    @DexIgnore
    public ax5<da4> g;
    @DexIgnore
    public pe5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthFragment a;

        @DexIgnore
        public b(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment, da4 da4) {
            this.a = goalTrackingOverviewMonthFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewMonthFragment.a(this.a).a().a(1);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewMonthFragment a;

        @DexIgnore
        public c(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment) {
            this.a = goalTrackingOverviewMonthFragment;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wg6.b(calendar, "calendar");
            pe5 b = this.a.h;
            if (b != null) {
                Date time = calendar.getTime();
                wg6.a((Object) time, "calendar.time");
                b.a(time);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ b06 a(GoalTrackingOverviewMonthFragment goalTrackingOverviewMonthFragment) {
        b06 b06 = goalTrackingOverviewMonthFragment.f;
        if (b06 != null) {
            return b06;
        }
        wg6.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v1, types: [java.lang.Object, com.portfolio.platform.view.recyclerview.RecyclerViewCalendar, android.view.ViewGroup] */
    /* JADX WARNING: type inference failed for: r6v3, types: [java.lang.Object, com.portfolio.platform.view.recyclerview.RecyclerViewCalendar, android.view.ViewGroup] */
    public void c(boolean z) {
        da4 a2;
        ax5<da4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                Object r6 = a2.q;
                wg6.a((Object) r6, "binding.calendarMonth");
                r6.setVisibility(4);
                ConstraintLayout constraintLayout = a2.r;
                wg6.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            Object r62 = a2.q;
            wg6.a((Object) r62, "binding.calendarMonth");
            r62.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.r;
            wg6.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "GoalTrackingOverviewMonthFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        da4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ax5<da4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.b("hybridGoalTrackingTab");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        da4 a2;
        wg6.b(layoutInflater, "inflater");
        GoalTrackingOverviewMonthFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onCreateView");
        da4 a3 = kb.a(layoutInflater, 2131558555, viewGroup, false, e1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            b06 a4 = vd.a(activity).a(b06.class);
            wg6.a((Object) a4, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = a4;
            a3.s.setOnClickListener(new b(this, a3));
        }
        RecyclerViewCalendar recyclerViewCalendar = a3.q;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        a3.q.setOnCalendarMonthChanged(new c(this));
        a3.q.setOnCalendarItemClickListener(this);
        this.g = new ax5<>(this, a3);
        j1();
        ax5<da4> ax5 = this.g;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        GoalTrackingOverviewMonthFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onResume");
        j1();
        pe5 pe5 = this.h;
        if (pe5 != null) {
            pe5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        GoalTrackingOverviewMonthFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onStop");
        pe5 pe5 = this.h;
        if (pe5 != null) {
            pe5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(int i2, Calendar calendar) {
        wg6.b(calendar, "calendar");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i2 + ", calendar=" + calendar);
        FragmentActivity activity = getActivity();
        if (activity != null) {
            GoalTrackingDetailActivity.a aVar = GoalTrackingDetailActivity.D;
            Date time = calendar.getTime();
            wg6.a((Object) time, "it.time");
            wg6.a((Object) activity, Constants.ACTIVITY);
            aVar.a(time, activity);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(TreeMap<Long, Float> treeMap) {
        da4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        wg6.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ax5<da4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setTintColor(w6.a(PortfolioApp.get.instance(), 2131099879));
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        da4 a2;
        wg6.b(date, "selectDate");
        wg6.b(date2, "startDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("GoalTrackingOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ax5<da4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            wg6.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            wg6.a((Object) instance2, "startCalendar");
            instance2.setTime(bk4.o(date2));
            wg6.a((Object) instance3, "endCalendar");
            instance3.setTime(bk4.j(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(pe5 pe5) {
        wg6.b(pe5, "presenter");
        this.h = pe5;
    }
}
