package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.fossil.al4;
import com.fossil.ax5;
import com.fossil.ci6;
import com.fossil.jb;
import com.fossil.jh6;
import com.fossil.kb;
import com.fossil.o34;
import com.fossil.p05;
import com.fossil.pd4;
import com.fossil.q05;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uh6;
import com.fossil.wg6;
import com.fossil.wh6;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.BaseBottomSheetDialogFragment;
import com.portfolio.platform.view.NumberPicker;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimeFragment extends BaseBottomSheetDialogFragment implements q05 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public /* final */ jb j; // = new o34(this);
    @DexIgnore
    public ax5<pd4> o;
    @DexIgnore
    public p05 p;
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return RemindTimeFragment.r;
        }

        @DexIgnore
        public final RemindTimeFragment b() {
            return new RemindTimeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements NumberPicker.g {
        @DexIgnore
        public /* final */ /* synthetic */ RemindTimeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ pd4 b;

        @DexIgnore
        public b(RemindTimeFragment remindTimeFragment, pd4 pd4) {
            this.a = remindTimeFragment;
            this.b = pd4;
        }

        @DexIgnore
        public final void a(NumberPicker numberPicker, int i, int i2) {
            p05 a2 = RemindTimeFragment.a(this.a);
            NumberPicker numberPicker2 = this.b.s;
            wg6.a((Object) numberPicker2, "binding.numberPicker");
            a2.a(numberPicker2.getValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ RemindTimeFragment a;

        @DexIgnore
        public c(RemindTimeFragment remindTimeFragment) {
            this.a = remindTimeFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            RemindTimeFragment.a(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ pd4 a;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 b;

        @DexIgnore
        public d(pd4 pd4, jh6 jh6) {
            this.a = pd4;
            this.b = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            ConstraintLayout constraintLayout = this.a.q;
            wg6.a((Object) constraintLayout, "it.clRoot");
            ViewParent parent = constraintLayout.getParent();
            if (parent != null) {
                CoordinatorLayout.e layoutParams = ((View) parent).getLayoutParams();
                if (layoutParams != null) {
                    BottomSheetBehavior d = layoutParams.d();
                    if (d != null) {
                        d.e(3);
                        pd4 pd4 = this.a;
                        wg6.a((Object) pd4, "it");
                        View d2 = pd4.d();
                        wg6.a((Object) d2, "it.root");
                        d2.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.b.element);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                throw new rc6("null cannot be cast to non-null type androidx.coordinatorlayout.widget.CoordinatorLayout.LayoutParams");
            }
            throw new rc6("null cannot be cast to non-null type android.view.View");
        }
    }

    /*
    static {
        String simpleName = RemindTimeFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "RemindTimeFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ p05 a(RemindTimeFragment remindTimeFragment) {
        p05 p05 = remindTimeFragment.p;
        if (p05 != null) {
            return p05;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void b(int i) {
        NumberPicker numberPicker;
        ax5<pd4> ax5 = this.o;
        if (ax5 != null) {
            pd4 a2 = ax5.a();
            if (a2 != null && (numberPicker = a2.s) != null) {
                wg6.a((Object) numberPicker, "numberPicker");
                numberPicker.setValue(i / 20);
                p05 p05 = this.p;
                if (p05 != null) {
                    p05.a(numberPicker.getValue());
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void close() {
        dismissAllowingStateLoss();
    }

    @DexIgnore
    public void e1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        pd4 a2 = kb.a(layoutInflater, 2131558602, viewGroup, false, this.j);
        String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        if (!TextUtils.isEmpty(b2)) {
            a2.q.setBackgroundColor(Color.parseColor(b2));
        }
        a2.r.setOnClickListener(new c(this));
        wg6.a((Object) a2, "binding");
        a(a2);
        this.o = new ax5<>(this, a2);
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        e1();
    }

    @DexIgnore
    public void onPause() {
        p05 p05 = this.p;
        if (p05 != null) {
            p05.g();
            RemindTimeFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        RemindTimeFragment.super.onResume();
        p05 p05 = this.p;
        if (p05 != null) {
            p05.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        RemindTimeFragment.super.onViewCreated(view, bundle);
        ax5<pd4> ax5 = this.o;
        if (ax5 != null) {
            pd4 a2 = ax5.a();
            if (a2 != null) {
                jh6 jh6 = new jh6();
                jh6.element = null;
                jh6.element = new d(a2, jh6);
                wg6.a((Object) a2, "it");
                View d2 = a2.d();
                wg6.a((Object) d2, "it.root");
                d2.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) jh6.element);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(p05 p05) {
        wg6.b(p05, "presenter");
        this.p = p05;
    }

    @DexIgnore
    public final void a(pd4 pd4) {
        ArrayList arrayList = new ArrayList();
        uh6 a2 = ci6.a((uh6) new wh6(20, 120), 20);
        int a3 = a2.a();
        int b2 = a2.b();
        int c2 = a2.c();
        if (c2 < 0 ? a3 >= b2 : a3 <= b2) {
            while (true) {
                String d2 = al4.d(a3);
                wg6.a((Object) d2, "timeString");
                arrayList.add(d2);
                if (a3 == b2) {
                    break;
                }
                a3 += c2;
            }
        }
        NumberPicker numberPicker = pd4.s;
        wg6.a((Object) numberPicker, "binding.numberPicker");
        numberPicker.setMinValue(1);
        NumberPicker numberPicker2 = pd4.s;
        wg6.a((Object) numberPicker2, "binding.numberPicker");
        numberPicker2.setMaxValue(6);
        NumberPicker numberPicker3 = pd4.s;
        Object[] array = arrayList.toArray(new String[0]);
        if (array != null) {
            numberPicker3.setDisplayedValues((String[]) array);
            pd4.s.setOnValueChangedListener(new b(this, pd4));
            return;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
