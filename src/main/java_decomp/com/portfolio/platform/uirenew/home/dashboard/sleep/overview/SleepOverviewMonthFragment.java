package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.bk4;
import com.fossil.fe4;
import com.fossil.kb;
import com.fossil.pg5;
import com.fossil.qg5;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity;
import com.portfolio.platform.view.recyclerview.RecyclerViewCalendar;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewMonthFragment extends BaseFragment implements qg5 {
    @DexIgnore
    public ax5<fe4> f;
    @DexIgnore
    public pg5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements RecyclerViewCalendar.c {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthFragment a;

        @DexIgnore
        public b(SleepOverviewMonthFragment sleepOverviewMonthFragment) {
            this.a = sleepOverviewMonthFragment;
        }

        @DexIgnore
        public void a(Calendar calendar) {
            wg6.b(calendar, "calendar");
            pg5 a2 = this.a.g;
            if (a2 != null) {
                Date time = calendar.getTime();
                wg6.a((Object) time, "calendar.time");
                a2.a(time);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RecyclerViewCalendar.b {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthFragment a;

        @DexIgnore
        public c(SleepOverviewMonthFragment sleepOverviewMonthFragment) {
            this.a = sleepOverviewMonthFragment;
        }

        @DexIgnore
        public void a(int i, Calendar calendar) {
            wg6.b(calendar, "calendar");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewMonthFragment", "OnCalendarItemClickListener: position=" + i + ", calendar=" + calendar);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                SleepDetailActivity.a aVar = SleepDetailActivity.D;
                Date time = calendar.getTime();
                wg6.a((Object) time, "it.time");
                wg6.a((Object) activity, Constants.ACTIVITY);
                aVar.a(time, activity);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "SleepOverviewMonthFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        fe4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        ax5<fe4> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            pg5 pg5 = this.g;
            if ((pg5 != null ? pg5.h() : null) == FossilDeviceSerialPatternUtil.DEVICE.DIANA) {
                recyclerViewCalendar.b("dianaSleepTab");
            } else {
                recyclerViewCalendar.b("hybridSleepTab");
            }
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fe4 a2;
        wg6.b(layoutInflater, "inflater");
        SleepOverviewMonthFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onCreateView");
        fe4 a3 = kb.a(layoutInflater, 2131558613, viewGroup, false, e1());
        RecyclerViewCalendar recyclerViewCalendar = a3.q;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        recyclerViewCalendar.setEndDate(instance);
        a3.q.setOnCalendarMonthChanged(new b(this));
        a3.q.setOnCalendarItemClickListener(new c(this));
        this.f = new ax5<>(this, a3);
        j1();
        ax5<fe4> ax5 = this.f;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        SleepOverviewMonthFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onResume");
        j1();
        pg5 pg5 = this.g;
        if (pg5 != null) {
            pg5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        SleepOverviewMonthFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onStop");
        pg5 pg5 = this.g;
        if (pg5 != null) {
            pg5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthFragment", "onViewCreated");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(TreeMap<Long, Float> treeMap) {
        fe4 a2;
        RecyclerViewCalendar recyclerViewCalendar;
        wg6.b(treeMap, Constants.MAP);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewMonthFragment", "showMonthDetails - map=" + treeMap.size());
        ax5<fe4> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null && (recyclerViewCalendar = a2.q) != null) {
            recyclerViewCalendar.setTintColor(w6.a(PortfolioApp.get.instance(), R.color.steps));
            recyclerViewCalendar.setData(treeMap);
            recyclerViewCalendar.setEnableButtonNextAndPrevMonth(true);
        }
    }

    @DexIgnore
    public void a(Date date, Date date2) {
        fe4 a2;
        wg6.b(date, "selectDate");
        wg6.b(date2, "startDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepOverviewMonthFragment", "showSelectedDate - selectDate=" + date + ", startDate=" + date2);
        ax5<fe4> ax5 = this.f;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            Calendar instance = Calendar.getInstance();
            Calendar instance2 = Calendar.getInstance();
            Calendar instance3 = Calendar.getInstance();
            wg6.a((Object) instance, "selectCalendar");
            instance.setTime(date);
            wg6.a((Object) instance2, "startCalendar");
            instance2.setTime(bk4.o(date2));
            wg6.a((Object) instance3, "endCalendar");
            instance3.setTime(bk4.j(instance3.getTime()));
            a2.q.a(instance, instance2, instance3);
        }
    }

    @DexIgnore
    public void a(pg5 pg5) {
        wg6.b(pg5, "presenter");
        this.g = pg5;
    }
}
