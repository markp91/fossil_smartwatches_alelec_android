package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import android.graphics.RectF;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fg5;
import com.fossil.fj5;
import com.fossil.gg5;
import com.fossil.gk6;
import com.fossil.hg5$b$a;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewDayPresenter extends fg5 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.n.e());
    @DexIgnore
    public Date f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public LiveData<yx5<List<MFSleepDay>>> i; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<MFSleepSession>>> j; // = new MutableLiveData();
    @DexIgnore
    public /* final */ gg5 k;
    @DexIgnore
    public /* final */ SleepSummariesRepository l;
    @DexIgnore
    public /* final */ SleepSessionsRepository m;
    @DexIgnore
    public /* final */ PortfolioApp n;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter$showDetailChart$1", f = "SleepOverviewDayPresenter.kt", l = {167}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SleepOverviewDayPresenter sleepOverviewDayPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepOverviewDayPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            List list;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                yx5 yx5 = (yx5) this.this$0.j.a();
                if (yx5 == null || (list = (List) yx5.d()) == null) {
                    this.this$0.k.s(new ArrayList());
                    return cd6.a;
                }
                dl6 a2 = this.this$0.b();
                hg5$b$a hg5_b_a = new hg5$b$a(list, (xe6) null, this);
                this.L$0 = il6;
                this.L$1 = list;
                this.label = 1;
                obj = gk6.a(a2, hg5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                List list2 = (List) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.k.s((List) obj);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<yx5<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public c(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<MFSleepDay>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSummaries -- sleepSummaries=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                this.a.g = true;
                if (this.a.g && this.a.h) {
                    rm6 unused = this.a.k();
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<MFSleepSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewDayPresenter a;

        @DexIgnore
        public d(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
            this.a = sleepOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<MFSleepSession>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mSleepSessions -- sleepSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            sb.append('\"');
            local.d("SleepOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                this.a.h = true;
                if (this.a.g && this.a.h) {
                    rm6 unused = this.a.k();
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SleepOverviewDayPresenter(gg5 gg5, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        wg6.b(gg5, "mView");
        wg6.b(sleepSummariesRepository, "mSummariesRepository");
        wg6.b(sleepSessionsRepository, "mSessionsRepository");
        wg6.b(portfolioApp, "mApp");
        this.k = gg5;
        this.l = sleepSummariesRepository;
        this.m = sleepSessionsRepository;
        this.n = portfolioApp;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(SleepOverviewDayPresenter sleepOverviewDayPresenter) {
        Date date = sleepOverviewDayPresenter.f;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wg6.d("mDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("SleepOverviewDayPresenter", sb.toString());
                    return;
                }
                wg6.d("mDate");
                throw null;
            }
        }
        this.g = false;
        this.h = false;
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("SleepOverviewDayPresenter", sb2.toString());
            SleepSummariesRepository sleepSummariesRepository = this.l;
            Date date4 = this.f;
            if (date4 == null) {
                wg6.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.i = sleepSummariesRepository.getSleepSummaries(date4, date4, false);
                SleepSessionsRepository sleepSessionsRepository = this.m;
                Date date5 = this.f;
                if (date5 == null) {
                    wg6.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.j = sleepSessionsRepository.getSleepSessionList(date5, date5, false);
                } else {
                    wg6.d("mDate");
                    throw null;
                }
            } else {
                wg6.d("mDate");
                throw null;
            }
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        this.k.a(this);
    }

    @DexIgnore
    public final rm6 k() {
        return ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "start");
        i();
        LiveData<yx5<List<MFSleepDay>>> liveData = this.i;
        gg5 gg5 = this.k;
        if (gg5 != null) {
            liveData.a((SleepOverviewDayFragment) gg5, new c(this));
            this.j.a(this.k, new d(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewDayPresenter", "stop");
        try {
            LiveData<yx5<List<MFSleepDay>>> liveData = this.i;
            gg5 gg5 = this.k;
            if (gg5 != null) {
                liveData.a((SleepOverviewDayFragment) gg5);
                this.j.a(this.k);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewDayPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v1, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v3, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v10, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v8, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v5, resolved type: com.portfolio.platform.data.model.room.sleep.MFSleepDay} */
    /* JADX WARNING: Multi-variable type inference failed */
    public final List<fj5.b> a(Date date, List<MFSleepSession> list) {
        ArrayList arrayList;
        List list2;
        ArrayList arrayList2 = new ArrayList();
        for (MFSleepSession next : list) {
            if (bk4.a(Long.valueOf(date.getTime()), Long.valueOf(next.getDate()))) {
                arrayList2.add(next);
            }
        }
        yx5 yx5 = (yx5) this.i.a();
        MFSleepDay mFSleepDay = null;
        if (!(yx5 == null || (list2 = (List) yx5.d()) == null)) {
            Iterator it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Object next2 = it.next();
                if (bk4.d(date, next2.getDate())) {
                    mFSleepDay = next2;
                    break;
                }
            }
            mFSleepDay = mFSleepDay;
        }
        int a2 = ik4.d.a(mFSleepDay);
        ArrayList arrayList3 = new ArrayList();
        Iterator it2 = arrayList2.iterator();
        while (it2.hasNext()) {
            MFSleepSession mFSleepSession = (MFSleepSession) it2.next();
            BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
            ArrayList arrayList4 = new ArrayList();
            SleepDistribution realSleepStateDistInMinute = mFSleepSession.getRealSleepStateDistInMinute();
            List<WrapperSleepStateChange> sleepStateChange = mFSleepSession.getSleepStateChange();
            ArrayList arrayList5 = new ArrayList();
            int realStartTime = mFSleepSession.getRealStartTime();
            int totalMinuteBySleepDistribution = realSleepStateDistInMinute.getTotalMinuteBySleepDistribution();
            if (sleepStateChange != null) {
                for (WrapperSleepStateChange next3 : sleepStateChange) {
                    BarChart.b bVar = new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 31, (qg6) null);
                    MFSleepSession mFSleepSession2 = mFSleepSession;
                    bVar.a((int) next3.index);
                    bVar.c(realStartTime);
                    bVar.b(totalMinuteBySleepDistribution);
                    int i2 = next3.state;
                    if (i2 == 0) {
                        bVar.a(BarChart.f.LOWEST);
                    } else if (i2 == 1) {
                        bVar.a(BarChart.f.DEFAULT);
                    } else if (i2 == 2) {
                        bVar.a(BarChart.f.HIGHEST);
                    }
                    arrayList5.add(bVar);
                    mFSleepSession = mFSleepSession2;
                }
            }
            MFSleepSession mFSleepSession3 = mFSleepSession;
            arrayList4.add(arrayList5);
            if (arrayList4.size() != 0) {
                arrayList = arrayList4;
            } else {
                arrayList = qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})});
            }
            ArrayList<BarChart.a> a3 = cVar.a();
            BarChart.a aVar = r4;
            Iterator it3 = it2;
            BarChart.a aVar2 = new BarChart.a(a2, arrayList, 0, false, 12, (qg6) null);
            a3.add(aVar);
            cVar.b(a2);
            cVar.a(a2);
            int awake = realSleepStateDistInMinute.getAwake();
            int light = realSleepStateDistInMinute.getLight();
            int deep = realSleepStateDistInMinute.getDeep();
            if (totalMinuteBySleepDistribution > 0) {
                float f2 = (float) totalMinuteBySleepDistribution;
                float f3 = ((float) awake) / f2;
                float f4 = ((float) light) / f2;
                arrayList3.add(new fj5.b(cVar, f3, f4, ((float) 1) - (f3 + f4), awake, light, deep, mFSleepSession3.getTimezoneOffset()));
            }
            it2 = it3;
        }
        return arrayList3;
    }
}
