package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.fossil.ax5;
import com.fossil.kb;
import com.fossil.pa4;
import com.fossil.qg6;
import com.fossil.uf5;
import com.fossil.vf5;
import com.fossil.wg6;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.chart.WeekHeartRateChart;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewWeekFragment extends BaseFragment implements vf5 {
    @DexIgnore
    public ax5<pa4> f;
    @DexIgnore
    public uf5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "HeartRateOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        MFLogger.d("HeartRateOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HeartRateOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        MFLogger.d("HeartRateOverviewWeekFragment", "onCreateView");
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558561, viewGroup, false, e1()));
        ax5<pa4> ax5 = this.f;
        if (ax5 != null) {
            pa4 a2 = ax5.a();
            if (a2 != null) {
                return a2.d();
            }
            return null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        HeartRateOverviewWeekFragment.super.onResume();
        MFLogger.d("HeartRateOverviewWeekFragment", "onResume");
        uf5 uf5 = this.g;
        if (uf5 != null) {
            uf5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        HeartRateOverviewWeekFragment.super.onStop();
        MFLogger.d("HeartRateOverviewWeekFragment", "onStop");
        uf5 uf5 = this.g;
        if (uf5 != null) {
            uf5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(uf5 uf5) {
        wg6.b(uf5, "presenter");
        this.g = uf5;
    }

    @DexIgnore
    public void a(List<Integer> list, List<String> list2) {
        WeekHeartRateChart weekHeartRateChart;
        wg6.b(list, "data");
        wg6.b(list2, "listWeekDays");
        ax5<pa4> ax5 = this.f;
        if (ax5 != null) {
            pa4 a2 = ax5.a();
            if (a2 != null && (weekHeartRateChart = a2.q) != null) {
                weekHeartRateChart.setListWeekDays(list2);
                weekHeartRateChart.a(list);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
