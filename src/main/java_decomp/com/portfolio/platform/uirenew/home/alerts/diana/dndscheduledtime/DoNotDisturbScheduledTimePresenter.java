package com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime;

import android.content.Context;
import android.text.format.DateFormat;
import com.fossil.a15$b$a;
import com.fossil.a15$c$a;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.y05;
import com.fossil.z05;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DoNotDisturbScheduledTimePresenter extends y05 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public DNDScheduledTimeModel g;
    @DexIgnore
    public DNDScheduledTimeModel h;
    @DexIgnore
    public /* final */ z05 i;
    @DexIgnore
    public /* final */ DNDSettingsDatabase j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$save$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {98}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = doNotDisturbScheduledTimePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                a15$b$a a15_b_a = new a15$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, a15_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.i.close();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.dndscheduledtime.DoNotDisturbScheduledTimePresenter$start$1", f = "DoNotDisturbScheduledTimePresenter.kt", l = {35}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DoNotDisturbScheduledTimePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = doNotDisturbScheduledTimePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                a15$c$a a15_c_a = new a15$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, a15_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            for (DNDScheduledTimeModel dNDScheduledTimeModel : (List) obj) {
                if (dNDScheduledTimeModel.getScheduledTimeType() == this.this$0.f) {
                    z05 g = this.this$0.i;
                    DoNotDisturbScheduledTimePresenter doNotDisturbScheduledTimePresenter = this.this$0;
                    g.a(doNotDisturbScheduledTimePresenter.b(doNotDisturbScheduledTimePresenter.f));
                    this.this$0.i.b(dNDScheduledTimeModel.getMinutes());
                }
                if (dNDScheduledTimeModel.getScheduledTimeType() == 0) {
                    this.this$0.g = dNDScheduledTimeModel;
                } else {
                    this.this$0.h = dNDScheduledTimeModel;
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = DoNotDisturbScheduledTimePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "DoNotDisturbScheduledTim\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public DoNotDisturbScheduledTimePresenter(z05 z05, DNDSettingsDatabase dNDSettingsDatabase) {
        wg6.b(z05, "mView");
        wg6.b(dNDSettingsDatabase, "mDNDSettingsDatabase");
        this.i = z05;
        this.j = dNDSettingsDatabase;
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        this.i.a(this);
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String b(int i2) {
        if (i2 == 0) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886122);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026eAlerts_Main_Text__Start)");
            return a2;
        }
        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886120);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026oveAlerts_Main_Text__End)");
        return a3;
    }

    @DexIgnore
    public void a(int i2) {
        this.f = i2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
    public void a(String str, String str2, boolean z) {
        int i2;
        int i3;
        wg6.b(str, "hourValue");
        wg6.b(str2, "minuteValue");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = k;
        local.d(str3, "updateTime: hourValue = " + str + ", minuteValue = " + str2 + ", isPM = " + z);
        int i4 = 0;
        try {
            Integer valueOf = Integer.valueOf(str);
            wg6.a((Object) valueOf, "Integer.valueOf(hourValue)");
            i3 = valueOf.intValue();
            try {
                Integer valueOf2 = Integer.valueOf(str2);
                wg6.a((Object) valueOf2, "Integer.valueOf(minuteValue)");
                i2 = valueOf2.intValue();
            } catch (Exception e2) {
                e = e2;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = k;
                local2.e(str4, "Exception when parse time e=" + e);
                i2 = 0;
                if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
                }
            }
        } catch (Exception e3) {
            e = e3;
            i3 = 0;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str42 = k;
            local22.e(str42, "Exception when parse time e=" + e);
            i2 = 0;
            if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            }
        }
        if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            if (z) {
                i4 = i3 == 12 ? 12 : i3 + 12;
            } else if (i3 != 12) {
                i4 = i3;
            }
            this.e = (i4 * 60) + i2;
            return;
        }
        if (z) {
            i4 = i3 == 12 ? 12 : i3 + 12;
        } else if (i3 != 12) {
            i4 = i3;
        }
        this.e = (i4 * 60) + i2;
    }
}
