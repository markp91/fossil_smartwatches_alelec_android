package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.ff5;
import com.fossil.gf5;
import com.fossil.hf5$b$a;
import com.fossil.ik6;
import com.fossil.ld;
import com.fossil.ll6;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewDayPresenter extends ff5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public LiveData<yx5<List<HeartRateSample>>> f; // = new MutableLiveData();
    @DexIgnore
    public LiveData<yx5<List<WorkoutSession>>> g; // = new MutableLiveData();
    @DexIgnore
    public /* final */ gf5 h;
    @DexIgnore
    public /* final */ HeartRateSampleRepository i;
    @DexIgnore
    public /* final */ WorkoutSessionRepository j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements ld<yx5<? extends List<HeartRateSample>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore
        public b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<HeartRateSample>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mHeartRateSamples -- heartRateSamples=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 != wh4.DATABASE_LOADING) {
                rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new hf5$b$a(this, list, (xe6) null), 3, (Object) null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<T> implements ld<yx5<? extends List<WorkoutSession>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewDayPresenter a;

        @DexIgnore
        public c(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
            this.a = heartRateOverviewDayPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<WorkoutSession>> yx5) {
            wh4 a2 = yx5.a();
            List list = (List) yx5.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("start - mWorkoutSessions -- workoutSessions=");
            sb.append(list != null ? Integer.valueOf(list.size()) : null);
            sb.append(", status=");
            sb.append(a2);
            local.d("HeartRateOverviewDayPresenter", sb.toString());
            if (a2 == wh4.DATABASE_LOADING) {
                return;
            }
            if (list == null || list.isEmpty()) {
                this.a.h.a(false, new ArrayList());
            } else {
                this.a.h.a(true, list);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewDayPresenter(gf5 gf5, HeartRateSampleRepository heartRateSampleRepository, WorkoutSessionRepository workoutSessionRepository) {
        wg6.b(gf5, "mView");
        wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wg6.b(workoutSessionRepository, "mWorkoutSessionRepository");
        this.h = gf5;
        this.i = heartRateSampleRepository;
        this.j = workoutSessionRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date b(HeartRateOverviewDayPresenter heartRateOverviewDayPresenter) {
        Date date = heartRateOverviewDayPresenter.e;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "start");
        h();
        LiveData<yx5<List<HeartRateSample>>> liveData = this.f;
        gf5 gf5 = this.h;
        if (gf5 != null) {
            liveData.a((HeartRateOverviewDayFragment) gf5, new b(this));
            this.g.a(this.h, new c(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewDayPresenter", "stop");
        LiveData<yx5<List<HeartRateSample>>> liveData = this.f;
        gf5 gf5 = this.h;
        if (gf5 != null) {
            liveData.a((HeartRateOverviewDayFragment) gf5);
            this.g.a(this.h);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewDayFragment");
    }

    @DexIgnore
    public void h() {
        Date date = this.e;
        if (date != null) {
            if (date == null) {
                wg6.d("mDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.e;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("HeartRateOverviewDayPresenter", sb.toString());
                    return;
                }
                wg6.d("mDate");
                throw null;
            }
        }
        this.e = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.e;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("HeartRateOverviewDayPresenter", sb2.toString());
            HeartRateSampleRepository heartRateSampleRepository = this.i;
            Date date4 = this.e;
            if (date4 == null) {
                wg6.d("mDate");
                throw null;
            } else if (date4 != null) {
                this.f = heartRateSampleRepository.getHeartRateSamples(date4, date4, false);
                WorkoutSessionRepository workoutSessionRepository = this.j;
                Date date5 = this.e;
                if (date5 == null) {
                    wg6.d("mDate");
                    throw null;
                } else if (date5 != null) {
                    this.g = workoutSessionRepository.getWorkoutSessions(date5, date5, true);
                } else {
                    wg6.d("mDate");
                    throw null;
                }
            } else {
                wg6.d("mDate");
                throw null;
            }
        } else {
            wg6.d("mDate");
            throw null;
        }
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public final String a(Float f2) {
        if (f2 == null) {
            return "";
        }
        nh6 nh6 = nh6.a;
        Object[] objArr = {Integer.valueOf((int) Math.abs(f2.floatValue())), Integer.valueOf(((int) Math.abs(f2.floatValue() - (f2.floatValue() % ((float) 10)))) * 60)};
        String format = String.format("%02d:%02d", Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        if (f2.floatValue() >= ((float) 0)) {
            return '+' + format;
        }
        return '-' + format;
    }
}
