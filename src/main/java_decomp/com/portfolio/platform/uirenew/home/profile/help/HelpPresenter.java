package com.portfolio.platform.uirenew.home.profile.help;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.cl5;
import com.fossil.dl5;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.gl5$b$a;
import com.fossil.gl5$c$a;
import com.fossil.gl5$d$a;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.ws4;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HelpPresenter extends cl5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public /* final */ dl5 e;
    @DexIgnore
    public /* final */ DeviceRepository f;
    @DexIgnore
    public /* final */ ws4 g;
    @DexIgnore
    public /* final */ AnalyticsHelper h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendFeedbackSuccessfullyEvent$1", f = "HelpPresenter.kt", l = {78}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HelpPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HelpPresenter helpPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = helpPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                gl5$b$a gl5_b_a = new gl5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, gl5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                this.this$0.h.a("feedback_submit", (Map<String, ? extends Object>) hashMap);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1", f = "HelpPresenter.kt", l = {92}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HelpPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(HelpPresenter helpPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = helpPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                gl5$c$a gl5_c_a = new gl5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, gl5_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            SKUModel sKUModel = (SKUModel) obj;
            if (sKUModel != null) {
                HashMap hashMap = new HashMap();
                String sku = sKUModel.getSku();
                if (sku == null) {
                    sku = "";
                }
                hashMap.put("Style_Number", sku);
                String deviceName = sKUModel.getDeviceName();
                if (deviceName == null) {
                    deviceName = "";
                }
                hashMap.put("Device_Name", deviceName);
                hashMap.put("Trigger_Screen", "Support");
                this.this$0.h.a("feedback_open", (Map<String, ? extends Object>) hashMap);
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements m24.e<ws4.d, ws4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ HelpPresenter a;

        @DexIgnore
        public d(HelpPresenter helpPresenter) {
            this.a = helpPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ws4.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(HelpPresenter.i, "sendFeedback onSuccess");
            ZendeskConfig.INSTANCE.setIdentity(new AnonymousIdentity.Builder().withNameIdentifier(dVar.f()).withEmailIdentifier(dVar.c()).build());
            ZendeskConfig.INSTANCE.setCustomFields(dVar.b());
            this.a.e.b(new gl5$d$a(dVar));
        }

        @DexIgnore
        public void a(ws4.b bVar) {
            wg6.b(bVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(HelpPresenter.i, "sendFeedback onError");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = HelpPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "HelpPresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public HelpPresenter(dl5 dl5, DeviceRepository deviceRepository, ws4 ws4, AnalyticsHelper analyticsHelper) {
        wg6.b(dl5, "mView");
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(ws4, "mGetZendeskInformation");
        wg6.b(analyticsHelper, "mAnalyticsHelper");
        this.e = dl5;
        this.f = deviceRepository;
        this.g = ws4;
        this.h = analyticsHelper;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "presenter start");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "presenter stop");
    }

    @DexIgnore
    public void h() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void i() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void j() {
        this.e.a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.fossil.ws4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$d] */
    public void a(String str) {
        wg6.b(str, "subject");
        this.g.a(new ws4.c(str), new d(this));
    }
}
