package com.portfolio.platform.uirenew.home;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.fossil.ax5;
import com.fossil.bu4;
import com.fossil.db4;
import com.fossil.g34;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.kl4;
import com.fossil.n35;
import com.fossil.nh6;
import com.fossil.oj3;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.u8;
import com.fossil.w85;
import com.fossil.wg6;
import com.fossil.x85;
import com.fossil.xj6;
import com.fossil.xm4;
import com.google.android.material.tabs.TabLayout;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BasePermissionFragment;
import com.portfolio.platform.uirenew.home.RenamePresetDialogFragment;
import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import com.portfolio.platform.uirenew.pairing.instructions.PairingInstructionsActivity;
import com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HomeHybridCustomizeFragment extends BasePermissionFragment implements x85, View.OnClickListener, g34.d, AlertDialogFragment.g, bu4 {
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public w85 g;
    @DexIgnore
    public ConstraintLayout h;
    @DexIgnore
    public ViewPager2 i;
    @DexIgnore
    public int j;
    @DexIgnore
    public g34 o;
    @DexIgnore
    public ax5<db4> p;
    @DexIgnore
    public String q;
    @DexIgnore
    public String r;
    @DexIgnore
    public String s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public HashMap u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HomeHybridCustomizeFragment a() {
            return new HomeHybridCustomizeFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends ViewPager2.i {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;

        @DexIgnore
        public b(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
            this.a = homeHybridCustomizeFragment;
        }

        @DexIgnore
        public void a(int i, float f, int i2) {
            TabLayout tabLayout;
            TabLayout.g b;
            Drawable b2;
            TabLayout tabLayout2;
            TabLayout.g b3;
            Drawable b4;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "onPageScrolled position " + i + " mCurrentPosition " + this.a.j1());
            HomeHybridCustomizeFragment.super.a(i, f, i2);
            if (!TextUtils.isEmpty(this.a.r)) {
                int parseColor = Color.parseColor(this.a.r);
                db4 db4 = (db4) HomeHybridCustomizeFragment.b(this.a).a();
                if (!(db4 == null || (tabLayout2 = db4.v) == null || (b3 = tabLayout2.b(i)) == null || (b4 = b3.b()) == null)) {
                    b4.setTint(parseColor);
                }
            }
            if (!TextUtils.isEmpty(this.a.q) && this.a.j1() != i) {
                int parseColor2 = Color.parseColor(this.a.q);
                db4 db42 = (db4) HomeHybridCustomizeFragment.b(this.a).a();
                if (!(db42 == null || (tabLayout = db42.v) == null || (b = tabLayout.b(this.a.j1())) == null || (b2 = b.b()) == null)) {
                    b2.setTint(parseColor2);
                }
            }
            this.a.r(i);
            this.a.k1().a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements RenamePresetDialogFragment.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexIgnore
        public c(HomeHybridCustomizeFragment homeHybridCustomizeFragment, String str) {
            this.a = homeHybridCustomizeFragment;
            this.b = str;
        }

        @DexIgnore
        public void a(String str) {
            wg6.b(str, "presetName");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "showRenamePresetDialog - presetName=" + str);
            if (!TextUtils.isEmpty(str)) {
                this.a.k1().a(str, this.b);
            }
        }

        @DexIgnore
        public void onCancel() {
            FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showRenamePresetDialog - onCancel");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements oj3.b {
        @DexIgnore
        public /* final */ /* synthetic */ HomeHybridCustomizeFragment a;

        @DexIgnore
        public d(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
            this.a = homeHybridCustomizeFragment;
        }

        @DexIgnore
        public final void a(TabLayout.g gVar, int i) {
            wg6.b(gVar, "tab");
            int itemCount = this.a.getItemCount();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("HomeHybridCustomizeFragment", "tabLayoutConfig dataSize=" + itemCount);
            if (!TextUtils.isEmpty(this.a.q)) {
                int parseColor = Color.parseColor(this.a.q);
                if (i == 0) {
                    gVar.b(2131230975);
                    Drawable b = gVar.b();
                    if (b != null) {
                        b.setTint(parseColor);
                    }
                } else if (i == itemCount - 1) {
                    gVar.b(2131230819);
                    Drawable b2 = gVar.b();
                    if (b2 != null) {
                        b2.setTint(parseColor);
                    }
                } else {
                    gVar.b(2131231004);
                    Drawable b3 = gVar.b();
                    if (b3 != null) {
                        b3.setTint(parseColor);
                    }
                }
                if (!TextUtils.isEmpty(this.a.r) && i == this.a.j1()) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeHybridCustomizeFragment", "tabLayoutConfig primary mCurrentPosition " + this.a.j1());
                    Drawable b4 = gVar.b();
                    if (b4 != null) {
                        b4.setTint(Color.parseColor(this.a.r));
                    }
                }
            }
        }
    }

    @DexIgnore
    public static final /* synthetic */ ax5 b(HomeHybridCustomizeFragment homeHybridCustomizeFragment) {
        ax5<db4> ax5 = homeHybridCustomizeFragment.p;
        if (ax5 != null) {
            return ax5;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void G0() {
        w85 w85 = this.g;
        if (w85 != null) {
            w85.a(true);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void H0() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onAddPresetClick");
        w85 w85 = this.g;
        if (w85 != null) {
            w85.h();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void Q(boolean z) {
        if (z) {
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        kl4 g12 = g1();
        if (g12 != null) {
            g12.a("");
        }
    }

    @DexIgnore
    public void d(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showDeleteSuccessfully - position=" + i2 + " mCurrentPosition=" + this.j);
        g34 g34 = this.o;
        if (g34 == null) {
            wg6.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (g34.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.j);
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.u;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public void e(int i2) {
        g34 g34 = this.o;
        if (g34 == null) {
            return;
        }
        if (g34 == null) {
            wg6.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (g34.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(this.j);
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public void f(int i2) {
        this.t = true;
        g34 g34 = this.o;
        if (g34 == null) {
            wg6.d("mHybridPresetDetailAdapter");
            throw null;
        } else if (g34.getItemCount() > i2) {
            ViewPager2 viewPager2 = this.i;
            if (viewPager2 != null) {
                viewPager2.setCurrentItem(i2);
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        }
    }

    @DexIgnore
    public int getItemCount() {
        ViewPager2 viewPager2 = this.i;
        if (viewPager2 != null) {
            RecyclerView.g adapter = viewPager2.getAdapter();
            if (adapter != null) {
                return adapter.getItemCount();
            }
            return 0;
        }
        wg6.d("rvCustomize");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return "HomeHybridCustomizeFragment";
    }

    @DexIgnore
    public void i(List<n35> list) {
        wg6.b(list, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizeFragment", "showPresets - data=" + list.size());
        g34 g34 = this.o;
        if (g34 != null) {
            g34.a((ArrayList<n35>) new ArrayList(list));
            if (!this.t && this.j == 0) {
                this.t = false;
            }
            if (!this.t) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(this.j);
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            wg6.d("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r4v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public void j(boolean z) {
        if (z) {
            ax5<db4> ax5 = this.p;
            if (ax5 != null) {
                db4 a2 = ax5.a();
                if (a2 != null) {
                    Object r4 = a2.w;
                    wg6.a((Object) r4, "tvTapIconToCustomize");
                    r4.setVisibility(0);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
        ax5<db4> ax52 = this.p;
        if (ax52 != null) {
            db4 a3 = ax52.a();
            if (a3 != null) {
                Object r42 = a3.w;
                wg6.a((Object) r42, "tvTapIconToCustomize");
                r42.setVisibility(4);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public final int j1() {
        return this.j;
    }

    @DexIgnore
    public final w85 k1() {
        w85 w85 = this.g;
        if (w85 != null) {
            return w85;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public final void l1() {
        ax5<db4> ax5 = this.p;
        if (ax5 != null) {
            db4 a2 = ax5.a();
            TabLayout tabLayout = a2 != null ? a2.v : null;
            if (tabLayout != null) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    new oj3(tabLayout, viewPager2, new d(this)).a();
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void m() {
        String string = getString(2131886580);
        wg6.a((Object) string, "getString(R.string.Desig\u2026on_Text__ApplyingToWatch)");
        X(string);
    }

    @DexIgnore
    public void n() {
        a();
    }

    @DexIgnore
    public void onClick(View view) {
        FragmentActivity activity;
        wg6.b(view, "v");
        if (view.getId() == 2131362404 && (activity = getActivity()) != null) {
            PairingInstructionsActivity.a aVar = PairingInstructionsActivity.C;
            wg6.a((Object) activity, "it");
            PairingInstructionsActivity.a.a(aVar, activity, false, false, 6, (Object) null);
        }
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        HomeHybridCustomizeFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        db4 a2 = kb.a(layoutInflater, 2131558568, viewGroup, false, e1());
        wg6.a((Object) a2, "binding");
        a(a2);
        this.p = new ax5<>(this, a2);
        ax5<db4> ax5 = this.p;
        if (ax5 != null) {
            db4 a3 = ax5.a();
            if (a3 != null) {
                wg6.a((Object) a3, "mBinding.get()!!");
                return a3.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        HomeHybridCustomizeFragment.super.onPause();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onPause");
        w85 w85 = this.g;
        if (w85 == null) {
            return;
        }
        if (w85 != null) {
            w85.g();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.a("");
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        HomeHybridCustomizeFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "onResume");
        w85 w85 = this.g;
        if (w85 == null) {
            return;
        }
        if (w85 != null) {
            w85.f();
            kl4 g1 = g1();
            if (g1 != null) {
                g1.d();
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        l1();
        if (this.g != null) {
            W("customize_view");
        }
    }

    @DexIgnore
    public final void r(int i2) {
        this.j = i2;
    }

    @DexIgnore
    public void t(String str) {
        wg6.b(str, "microAppId");
        if (isActive()) {
            xm4 xm4 = xm4.d;
            Context context = getContext();
            if (context != null) {
                xm4.b(context, str);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public void v() {
        FLogger.INSTANCE.getLocal().d("HomeHybridCustomizeFragment", "showCreateNewSuccessfully");
        g34 g34 = this.o;
        if (g34 != null) {
            int itemCount = g34.getItemCount();
            int i2 = this.j;
            if (itemCount > i2) {
                ViewPager2 viewPager2 = this.i;
                if (viewPager2 != null) {
                    viewPager2.setCurrentItem(i2);
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
        } else {
            wg6.d("mHybridPresetDetailAdapter");
            throw null;
        }
    }

    @DexIgnore
    public void b(String str, String str2) {
        wg6.b(str, "presetName");
        wg6.b(str2, "presetId");
        if (getChildFragmentManager().b("RenamePresetDialogFragment") == null) {
            RenamePresetDialogFragment a2 = RenamePresetDialogFragment.g.a(str, new c(this, str2));
            if (isActive()) {
                a2.show(getChildFragmentManager(), "RenamePresetDialogFragment");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v11, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    public final void a(db4 db4) {
        this.q = ThemeManager.l.a().b("disabledButton");
        this.r = ThemeManager.l.a().b("primaryText");
        this.s = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
        this.o = new g34(new ArrayList(), this);
        ConstraintLayout constraintLayout = db4.q;
        wg6.a((Object) constraintLayout, "binding.clNoDevice");
        this.h = constraintLayout;
        db4.r.setOnClickListener(this);
        db4.s.setImageResource(2131230977);
        ViewPager2 viewPager2 = db4.u;
        wg6.a((Object) viewPager2, "binding.rvPreset");
        this.i = viewPager2;
        ViewPager2 viewPager22 = this.i;
        if (viewPager22 != null) {
            if (viewPager22.getChildAt(0) != null) {
                ViewPager2 viewPager23 = this.i;
                if (viewPager23 != null) {
                    RecyclerView childAt = viewPager23.getChildAt(0);
                    if (childAt != null) {
                        childAt.setOverScrollMode(2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type androidx.recyclerview.widget.RecyclerView");
                    }
                } else {
                    wg6.d("rvCustomize");
                    throw null;
                }
            }
            ViewPager2 viewPager24 = this.i;
            if (viewPager24 != null) {
                g34 g34 = this.o;
                if (g34 != null) {
                    viewPager24.setAdapter(g34);
                    if (!TextUtils.isEmpty(this.s)) {
                        TabLayout tabLayout = db4.v;
                        wg6.a((Object) tabLayout, "binding.tab");
                        tabLayout.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(this.s)));
                    }
                    ViewPager2 viewPager25 = this.i;
                    if (viewPager25 != null) {
                        viewPager25.a(new b(this));
                        ViewPager2 viewPager26 = this.i;
                        if (viewPager26 != null) {
                            viewPager26.setCurrentItem(this.j);
                        } else {
                            wg6.d("rvCustomize");
                            throw null;
                        }
                    } else {
                        wg6.d("rvCustomize");
                        throw null;
                    }
                } else {
                    wg6.d("mHybridPresetDetailAdapter");
                    throw null;
                }
            } else {
                wg6.d("rvCustomize");
                throw null;
            }
        } else {
            wg6.d("rvCustomize");
            throw null;
        }
    }

    @DexIgnore
    public void j() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            TroubleshootingActivity.a aVar = TroubleshootingActivity.C;
            wg6.a((Object) activity, "it");
            TroubleshootingActivity.a.a(aVar, activity, PortfolioApp.get.instance().e(), false, 4, (Object) null);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void b(boolean z, String str, String str2, String str3) {
        wg6.b(str, "currentPresetName");
        wg6.b(str2, "nextPresetName");
        wg6.b(str3, "nextPresetId");
        if (isActive()) {
            FragmentActivity activity = getActivity();
            if (activity != null) {
                String string = activity.getString(2131886378);
                wg6.a((Object) string, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
                if (z) {
                    FragmentActivity activity2 = getActivity();
                    if (activity2 != null) {
                        String string2 = activity2.getString(2131886379);
                        wg6.a((Object) string2, "activity!!.getString(R.s\u2026ingAPresetIsPermanentAnd)");
                        nh6 nh6 = nh6.a;
                        Object[] objArr = {xj6.d(str2)};
                        string = String.format(string2, Arrays.copyOf(objArr, objArr.length));
                        wg6.a((Object) string, "java.lang.String.format(format, *args)");
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                Bundle bundle = new Bundle();
                bundle.putString("NEXT_ACTIVE_PRESET_ID", str3);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizeFragment", "nextPresetId " + str3);
                AlertDialogFragment.f fVar = new AlertDialogFragment.f(2131558487);
                nh6 nh62 = nh6.a;
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886380);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026_Title__DeletePresetName)");
                Object[] objArr2 = {str};
                String format = String.format(a2, Arrays.copyOf(objArr2, objArr2.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                fVar.a(2131363218, format);
                fVar.a(2131363129, string);
                fVar.a(2131363190, jm4.a((Context) PortfolioApp.get.instance(), 2131886377));
                fVar.a(2131363105, jm4.a((Context) PortfolioApp.get.instance(), 2131886376));
                fVar.b(2131363190);
                fVar.b(2131363105);
                fVar.a(getChildFragmentManager(), "DIALOG_DELETE_PRESET", bundle);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(n35 n35, List<? extends u8<View, String>> list, List<? extends u8<CustomizeWidget, String>> list2, String str, int i2) {
        wg6.b(list, "views");
        wg6.b(list2, "customizeWidgetViews");
        wg6.b(str, "microAppPos");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("onPresetWatchClick preset=");
        sb.append(n35 != null ? n35.a() : null);
        sb.append(" microAppPos=");
        sb.append(str);
        sb.append(" position=");
        sb.append(i2);
        local.d("HomeHybridCustomizeFragment", sb.toString());
        if (n35 != null) {
            HybridCustomizeEditActivity.a aVar = HybridCustomizeEditActivity.E;
            FragmentActivity activity = getActivity();
            if (activity != null) {
                wg6.a((Object) activity, "activity!!");
                aVar.a(activity, n35.b(), new ArrayList(list), list2, str);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        String str2;
        wg6.b(str, "tag");
        if (str.hashCode() == -1353443012 && str.equals("DIALOG_DELETE_PRESET") && i2 == 2131363190) {
            if (intent != null) {
                str2 = intent.getStringExtra("NEXT_ACTIVE_PRESET_ID");
                wg6.a((Object) str2, "it.getStringExtra(NEXT_ACTIVE_PRESET_ID)");
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizeFragment", "onDialogFragmentResult - nextActivePreset " + str2);
            } else {
                str2 = "";
            }
            w85 w85 = this.g;
            if (w85 != null) {
                w85.a(str2);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (z) {
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                ConstraintLayout constraintLayout = this.h;
                if (constraintLayout != null) {
                    constraintLayout.setBackgroundColor(Color.parseColor(b2));
                } else {
                    wg6.d("clNoDevice");
                    throw null;
                }
            }
            ConstraintLayout constraintLayout2 = this.h;
            if (constraintLayout2 != null) {
                constraintLayout2.setVisibility(0);
            } else {
                wg6.d("clNoDevice");
                throw null;
            }
        } else {
            ConstraintLayout constraintLayout3 = this.h;
            if (constraintLayout3 != null) {
                constraintLayout3.setVisibility(8);
            } else {
                wg6.d("clNoDevice");
                throw null;
            }
        }
    }

    @DexIgnore
    public void a(w85 w85) {
        wg6.b(w85, "presenter");
        this.g = w85;
    }
}
