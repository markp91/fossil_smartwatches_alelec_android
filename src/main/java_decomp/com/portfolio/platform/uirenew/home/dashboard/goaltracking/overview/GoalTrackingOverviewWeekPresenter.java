package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.graphics.RectF;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ue5;
import com.fossil.ve5;
import com.fossil.we5$b$a;
import com.fossil.we5$b$b;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewWeekPresenter extends ue5 {
    @DexIgnore
    public Date e;
    @DexIgnore
    public LiveData<yx5<List<GoalTrackingSummary>>> f; // = new MutableLiveData();
    @DexIgnore
    public BarChart.c g;
    @DexIgnore
    public /* final */ ve5 h;
    @DexIgnore
    public /* final */ UserRepository i;
    @DexIgnore
    public /* final */ an4 j;
    @DexIgnore
    public /* final */ GoalTrackingRepository k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekPresenter$start$1", f = "GoalTrackingOverviewWeekPresenter.kt", l = {50}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = goalTrackingOverviewWeekPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:16:0x00c3  */
        /* JADX WARNING: Removed duplicated region for block: B:18:0x00e4  */
        public final Object invokeSuspend(Object obj) {
            ve5 h;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "start");
                this.this$0.h();
                if (this.this$0.e == null || !bk4.t(GoalTrackingOverviewWeekPresenter.d(this.this$0)).booleanValue()) {
                    this.this$0.e = new Date();
                    dl6 a2 = this.this$0.b();
                    we5$b$b we5_b_b = new we5$b$b(this, (xe6) null);
                    this.L$0 = il6;
                    this.label = 1;
                    obj = gk6.a(a2, we5_b_b, this);
                    if (obj == a) {
                        return a;
                    }
                }
                LiveData f = this.this$0.f;
                h = this.this$0.h;
                if (h == null) {
                    f.a((GoalTrackingOverviewWeekFragment) h, new we5$b$a(this));
                    this.this$0.h.c(!this.this$0.j.I());
                    return cd6.a;
                }
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            lc6 lc6 = (lc6) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewWeekPresenter", "start - startDate=" + ((Date) lc6.getFirst()) + ", endDate=" + ((Date) lc6.getSecond()));
            GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter = this.this$0;
            goalTrackingOverviewWeekPresenter.f = goalTrackingOverviewWeekPresenter.k.getSummaries((Date) lc6.getFirst(), (Date) lc6.getSecond(), false);
            LiveData f2 = this.this$0.f;
            h = this.this$0.h;
            if (h == null) {
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GoalTrackingOverviewWeekPresenter(ve5 ve5, UserRepository userRepository, an4 an4, GoalTrackingRepository goalTrackingRepository) {
        wg6.b(ve5, "mView");
        wg6.b(userRepository, "userRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        this.h = ve5;
        this.i = userRepository;
        this.j = an4;
        this.k = goalTrackingRepository;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(GoalTrackingOverviewWeekPresenter goalTrackingOverviewWeekPresenter) {
        Date date = goalTrackingOverviewWeekPresenter.e;
        if (date != null) {
            return date;
        }
        wg6.d("mDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "stop");
        try {
            LiveData<yx5<List<GoalTrackingSummary>>> liveData = this.f;
            ve5 ve5 = this.h;
            if (ve5 != null) {
                liveData.a((GoalTrackingOverviewWeekFragment) ve5);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview.GoalTrackingOverviewWeekFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("GoalTrackingOverviewWeekPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public void h() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekPresenter", "loadData");
    }

    @DexIgnore
    public void i() {
        this.h.a(this);
    }

    @DexIgnore
    public final BarChart.c a(Date date, List<GoalTrackingSummary> list) {
        int i2;
        T t;
        Date date2 = date;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date2);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("GoalTrackingOverviewWeekPresenter", sb.toString());
        BarChart.c cVar = new BarChart.c(0, 0, (ArrayList) null, 7, (qg6) null);
        Calendar instance = Calendar.getInstance(Locale.US);
        wg6.a((Object) instance, "calendar");
        instance.setTime(date2);
        instance.add(5, -6);
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 <= 6) {
            Date time = instance.getTime();
            wg6.a((Object) time, "calendar.time");
            long time2 = time.getTime();
            if (list != null) {
                Iterator<T> it = list.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        t = null;
                        break;
                    }
                    t = it.next();
                    if (bk4.d(((GoalTrackingSummary) t).getDate(), instance.getTime())) {
                        break;
                    }
                }
                GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) t;
                if (goalTrackingSummary != null) {
                    int goalTarget = goalTrackingSummary.getGoalTarget();
                    int totalTracked = goalTrackingSummary.getTotalTracked();
                    i4 = Math.max(Math.max(goalTarget, totalTracked), i4);
                    cVar.a().add(new BarChart.a(goalTarget, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, totalTracked, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                    i5 = goalTarget;
                    i2 = 1;
                } else {
                    i2 = 1;
                    cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
                }
            } else {
                i2 = 1;
                cVar.a().add(new BarChart.a(i5, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new BarChart.b[]{new BarChart.b(0, (BarChart.f) null, 0, 0, (RectF) null, 23, (qg6) null)})}), time2, i3 == 6));
            }
            instance.add(5, i2);
            i3++;
        }
        if (i4 > 0) {
            i5 = i4;
        }
        cVar.b(i5);
        return cVar;
    }

    @DexIgnore
    public final lc6<Date, Date> a(Date date) {
        Date b2 = bk4.b(date, 6);
        MFUser currentUser = this.i.getCurrentUser();
        if (currentUser != null) {
            Date d = bk4.d(currentUser.getCreatedAt());
            if (!bk4.b(b2, d)) {
                b2 = d;
            }
        }
        return new lc6<>(b2, date);
    }
}
