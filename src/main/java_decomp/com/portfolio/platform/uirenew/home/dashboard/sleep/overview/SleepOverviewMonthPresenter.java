package com.portfolio.platform.uirenew.home.dashboard.sleep.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.pg5;
import com.fossil.qg5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rg5$b$a;
import com.fossil.rg5$d$a;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepOverviewMonthPresenter extends pg5 {
    @DexIgnore
    public /* final */ FossilDeviceSerialPatternUtil.DEVICE e; // = FossilDeviceSerialPatternUtil.getDeviceBySerial(this.r.e());
    @DexIgnore
    public MutableLiveData<Date> f; // = new MutableLiveData<>();
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public Date j;
    @DexIgnore
    public LiveData<yx5<List<MFSleepDay>>> k; // = new MutableLiveData();
    @DexIgnore
    public List<MFSleepDay> l; // = new ArrayList();
    @DexIgnore
    public LiveData<yx5<List<MFSleepDay>>> m;
    @DexIgnore
    public TreeMap<Long, Float> n;
    @DexIgnore
    public /* final */ qg5 o;
    @DexIgnore
    public /* final */ UserRepository p;
    @DexIgnore
    public /* final */ SleepSummariesRepository q;
    @DexIgnore
    public /* final */ PortfolioApp r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthPresenter$loadData$2", f = "SleepOverviewMonthPresenter.kt", l = {99}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(SleepOverviewMonthPresenter sleepOverviewMonthPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sleepOverviewMonthPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                rg5$b$a rg5_b_a = new rg5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, rg5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.j = bk4.d(mFUser.getCreatedAt());
                qg5 m = this.this$0.o;
                Date e = SleepOverviewMonthPresenter.e(this.this$0);
                Date c = this.this$0.j;
                if (c == null) {
                    c = new Date();
                }
                m.a(e, c);
                this.this$0.f.a(new Date());
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthPresenter a;

        @DexIgnore
        public c(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            this.a = sleepOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<MFSleepDay>>> apply(Date date) {
            SleepOverviewMonthPresenter sleepOverviewMonthPresenter = this.a;
            wg6.a((Object) date, "it");
            if (sleepOverviewMonthPresenter.b(date)) {
                SleepOverviewMonthPresenter sleepOverviewMonthPresenter2 = this.a;
                sleepOverviewMonthPresenter2.k = sleepOverviewMonthPresenter2.q.getSleepSummaries(SleepOverviewMonthPresenter.j(this.a), SleepOverviewMonthPresenter.h(this.a), true);
            }
            return this.a.k;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<MFSleepDay>>> {
        @DexIgnore
        public /* final */ /* synthetic */ SleepOverviewMonthPresenter a;

        @DexIgnore
        public d(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
            this.a = sleepOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<MFSleepDay>> yx5) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(yx5 != null ? yx5.f() : null);
            sb.append(" -- data.size=");
            sb.append((yx5 == null || (list = (List) yx5.d()) == null) ? null : Integer.valueOf(list.size()));
            local.d("SleepOverviewMonthPresenter", sb.toString());
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                List list2 = yx5 != null ? (List) yx5.d() : null;
                if (list2 != null && (!wg6.a((Object) this.a.l, (Object) list2))) {
                    rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new rg5$d$a(this, list2, (xe6) null), 3, (Object) null);
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SleepOverviewMonthPresenter(qg5 qg5, UserRepository userRepository, SleepSummariesRepository sleepSummariesRepository, PortfolioApp portfolioApp) {
        wg6.b(qg5, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(sleepSummariesRepository, "mSummariesRepository");
        wg6.b(portfolioApp, "mApp");
        this.o = qg5;
        this.p = userRepository;
        this.q = sleepSummariesRepository;
        this.r = portfolioApp;
        LiveData<yx5<List<MFSleepDay>>> b2 = sd.b(this.f, new c(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026    mSleepSummaries\n    }");
        this.m = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date e(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date h(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.i;
        if (date != null) {
            return date;
        }
        wg6.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date j(SleepOverviewMonthPresenter sleepOverviewMonthPresenter) {
        Date date = sleepOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", "start");
        i();
        LiveData<yx5<List<MFSleepDay>>> liveData = this.m;
        qg5 qg5 = this.o;
        if (qg5 != null) {
            liveData.a((SleepOverviewMonthFragment) qg5, new d(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", "stop");
        try {
            LiveData<yx5<List<MFSleepDay>>> liveData = this.m;
            qg5 qg5 = this.o;
            if (qg5 != null) {
                liveData.a((SleepOverviewMonthFragment) qg5);
                this.k.a(this.o);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewMonthFragment");
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SleepOverviewMonthPresenter", "stop - e=" + e2);
        }
    }

    @DexIgnore
    public FossilDeviceSerialPatternUtil.DEVICE h() {
        FossilDeviceSerialPatternUtil.DEVICE device = this.e;
        wg6.a((Object) device, "mCurrentDeviceType");
        return device;
    }

    @DexIgnore
    public void i() {
        FLogger.INSTANCE.getLocal().d("SleepOverviewMonthPresenter", "loadData");
        Date date = this.g;
        if (date != null) {
            if (date == null) {
                wg6.d("mCurrentDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.g;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("SleepOverviewMonthPresenter", sb.toString());
                    return;
                }
                wg6.d("mCurrentDate");
                throw null;
            }
        }
        this.g = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.g;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("SleepOverviewMonthPresenter", sb2.toString());
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
            return;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void j() {
        this.o.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.j;
        if (date3 == null) {
            date3 = new Date();
        }
        this.h = date3;
        Date date4 = this.h;
        if (date4 != null) {
            if (!bk4.a(date4.getTime(), date.getTime())) {
                Calendar p2 = bk4.p(date);
                wg6.a((Object) p2, "DateHelper.getStartOfMonth(date)");
                Date time = p2.getTime();
                wg6.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.h = time;
            }
            Boolean s = bk4.s(date);
            wg6.a((Object) s, "DateHelper.isThisMonth(date)");
            if (s.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar k2 = bk4.k(date);
                wg6.a((Object) k2, "DateHelper.getEndOfMonth(date)");
                date2 = k2.getTime();
                wg6.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.i = date2;
            Date date5 = this.i;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.h;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wg6.d("mStartDate");
                throw null;
            }
            wg6.d("mEndDate");
            throw null;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        if (this.f.a() == null || !bk4.d((Date) this.f.a(), date)) {
            this.f.a(date);
        }
    }

    @DexIgnore
    public final TreeMap<Long, Float> a(Date date, List<MFSleepDay> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("transferSummariesToDetailChart - date=");
        sb.append(date);
        sb.append(", summaries=");
        sb.append(list != null ? Integer.valueOf(list.size()) : null);
        local.d("SleepOverviewMonthPresenter", sb.toString());
        TreeMap<Long, Float> treeMap = new TreeMap<>();
        Calendar instance = Calendar.getInstance();
        if (list != null) {
            for (MFSleepDay next : list) {
                Date component1 = next.component1();
                int component2 = next.component2();
                int component3 = next.component3();
                wg6.a((Object) instance, "calendar");
                instance.setTime(component1);
                if (component2 > 0) {
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(((float) component3) / ((float) component2)));
                } else {
                    treeMap.put(Long.valueOf(instance.getTimeInMillis()), Float.valueOf(0.0f));
                }
            }
        }
        return treeMap;
    }
}
