package com.portfolio.platform.uirenew.home.details.sleep;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.zi5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepDetailActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a D; // = new a((qg6) null);
    @DexIgnore
    public SleepDetailPresenter B;
    @DexIgnore
    public Date C; // = new Date();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Date date, Context context) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(context, "context");
            Intent intent = new Intent(context, SleepDetailActivity.class);
            intent.putExtra("KEY_LONG_TIME", date.getTime());
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.home.details.sleep.SleepDetailActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        SleepDetailFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        if (intent != null) {
            this.C = new Date(intent.getLongExtra("KEY_LONG_TIME", new Date().getTime()));
        }
        if (b == null) {
            b = SleepDetailFragment.B.a(this.C);
            a((Fragment) b, 2131362119);
        }
        PortfolioApp.get.instance().g().a(new zi5(b)).a(this);
    }
}
