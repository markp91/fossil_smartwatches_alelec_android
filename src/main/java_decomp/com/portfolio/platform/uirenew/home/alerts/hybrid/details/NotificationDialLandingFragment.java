package com.portfolio.platform.uirenew.home.alerts.hybrid.details;

import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.bc4;
import com.fossil.jh6;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.w15;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.wg6;
import com.fossil.x15;
import com.portfolio.platform.uirenew.BaseFragment;
import com.portfolio.platform.view.NotificationSummaryDialView;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationDialLandingFragment extends BaseFragment implements x15 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public w15 f;
    @DexIgnore
    public ax5<bc4> g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationDialLandingFragment.i;
        }

        @DexIgnore
        public final NotificationDialLandingFragment b() {
            return new NotificationDialLandingFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationDialLandingFragment a;

        @DexIgnore
        public b(NotificationDialLandingFragment notificationDialLandingFragment) {
            this.a = notificationDialLandingFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements NotificationSummaryDialView.b {
        @DexIgnore
        public /* final */ /* synthetic */ NotificationDialLandingFragment a;

        @DexIgnore
        public c(NotificationDialLandingFragment notificationDialLandingFragment) {
            this.a = notificationDialLandingFragment;
        }

        @DexIgnore
        public void a(int i) {
            NotificationContactsAndAppsAssignedActivity.C.a(this.a, i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements ViewTreeObserver.OnGlobalLayoutListener {
        @DexIgnore
        public /* final */ /* synthetic */ bc4 a;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 b;

        @DexIgnore
        public d(bc4 bc4, jh6 jh6) {
            this.a = bc4;
            this.b = jh6;
        }

        @DexIgnore
        public final void onGlobalLayout() {
            this.a.r.requestLayout();
            NotificationSummaryDialView notificationSummaryDialView = this.a.r;
            wg6.a((Object) notificationSummaryDialView, "binding.nsdv");
            notificationSummaryDialView.getViewTreeObserver().removeOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) this.b.element);
        }
    }

    /*
    static {
        String simpleName = NotificationDialLandingFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationDialLandingF\u2026nt::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        bc4 a2 = kb.a(layoutInflater, 2131558581, viewGroup, false, e1());
        a2.q.setOnClickListener(new b(this));
        a2.r.setOnItemClickListener(new c(this));
        jh6 jh6 = new jh6();
        jh6.element = null;
        jh6.element = new d(a2, jh6);
        NotificationSummaryDialView notificationSummaryDialView = a2.r;
        wg6.a((Object) notificationSummaryDialView, "binding.nsdv");
        notificationSummaryDialView.getViewTreeObserver().addOnGlobalLayoutListener((ViewTreeObserver.OnGlobalLayoutListener) jh6.element);
        this.g = new ax5<>(this, a2);
        wg6.a((Object) a2, "binding");
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onPause() {
        w15 w15 = this.f;
        if (w15 != null) {
            w15.g();
            NotificationDialLandingFragment.super.onPause();
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void onResume() {
        NotificationDialLandingFragment.super.onResume();
        w15 w15 = this.f;
        if (w15 != null) {
            w15.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(w15 w15) {
        wg6.b(w15, "presenter");
        this.f = w15;
    }

    @DexIgnore
    public void a(SparseArray<List<BaseFeatureModel>> sparseArray) {
        NotificationSummaryDialView notificationSummaryDialView;
        wg6.b(sparseArray, "data");
        ax5<bc4> ax5 = this.g;
        if (ax5 != null) {
            bc4 a2 = ax5.a();
            if (a2 != null && (notificationSummaryDialView = a2.r) != null) {
                notificationSummaryDialView.setDataAsync(sparseArray);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
