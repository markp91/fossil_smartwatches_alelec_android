package com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings;

import android.content.Context;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.c55;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.d55;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.g55$b$a;
import com.fossil.g55$b$b;
import com.fossil.g55$b$c;
import com.fossil.g55$b$d;
import com.fossil.g55$c$a;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jm4;
import com.fossil.kc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeSettingsDefaultAddressPresenter extends c55 {
    @DexIgnore
    public static /* final */ String n;
    @DexIgnore
    public PlacesClient e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g;
    @DexIgnore
    public /* final */ String h; // = jm4.a((Context) PortfolioApp.get.instance(), 2131886188);
    @DexIgnore
    public /* final */ String i; // = jm4.a((Context) PortfolioApp.get.instance(), 2131886189);
    @DexIgnore
    public /* final */ ArrayList<String> j; // = new ArrayList<>();
    @DexIgnore
    public /* final */ d55 k;
    @DexIgnore
    public /* final */ an4 l;
    @DexIgnore
    public /* final */ UserRepository m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $address$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ fh6 $isAddressSaved$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ fh6 $isChanged$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ String $it;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(String str, xe6 xe6, CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, String str2, fh6 fh6, fh6 fh62) {
            super(2, xe6);
            this.$it = str;
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
            this.$address$inlined = str2;
            this.$isChanged$inlined = fh6;
            this.$isAddressSaved$inlined = fh62;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.$it, xe6, this.this$0, this.$address$inlined, this.$isChanged$inlined, this.$isAddressSaved$inlined);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00d3 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:48:0x015d  */
        /* JADX WARNING: Removed duplicated region for block: B:49:0x0190  */
        /* JADX WARNING: Removed duplicated region for block: B:57:0x01e6  */
        /* JADX WARNING: Removed duplicated region for block: B:58:0x0215  */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x025e  */
        /* JADX WARNING: Removed duplicated region for block: B:68:0x026a  */
        public final Object invokeSuspend(Object obj) {
            MFUser mFUser;
            ap4 ap4;
            il6 il6;
            MFUser mFUser2;
            MFUser mFUser3;
            ap4 ap42;
            il6 il62;
            Object a = ff6.a();
            int i = this.label;
            boolean z = false;
            if (i == 0) {
                nc6.a(obj);
                il62 = this.p$;
                if (!(this.$it.length() == 0) && !this.this$0.j.contains(this.$it)) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String l = CommuteTimeSettingsDefaultAddressPresenter.n;
                    local.d(l, "add " + this.$address$inlined + " to recent search list");
                    this.this$0.j.add(0, this.$it);
                    dl6 a2 = this.this$0.c();
                    g55$b$a g55_b_a = new g55$b$a(this, (xe6) null);
                    this.L$0 = il62;
                    this.label = 1;
                    if (gk6.a(a2, g55_b_a, this) == a) {
                        return a;
                    }
                }
                dl6 a3 = this.this$0.c();
                g55$b$b g55_b_b = new g55$b$b(this, (xe6) null);
                this.L$0 = il62;
                this.label = 2;
                obj = gk6.a(a3, g55_b_b, this);
                if (obj == a) {
                }
            } else if (i == 1) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
                dl6 a32 = this.this$0.c();
                g55$b$b g55_b_b2 = new g55$b$b(this, (xe6) null);
                this.L$0 = il62;
                this.label = 2;
                obj = gk6.a(a32, g55_b_b2, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 2) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 3) {
                mFUser3 = (MFUser) this.L$2;
                mFUser2 = (MFUser) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                ap42 = (ap4) obj;
                if (!(ap42 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String l2 = CommuteTimeSettingsDefaultAddressPresenter.n;
                    local2.d(l2, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " successfully");
                    this.$isAddressSaved$inlined.element = true;
                } else if (ap42 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String l3 = CommuteTimeSettingsDefaultAddressPresenter.n;
                    local3.d(l3, "update UserRepository to remote DB success - home = " + mFUser3.getHome() + " - work = " + mFUser3.getWork() + " failed");
                    dl6 a4 = this.this$0.c();
                    g55$b$d g55_b_d = new g55$b$d(mFUser3, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = mFUser2;
                    this.L$2 = mFUser3;
                    this.label = 4;
                    obj = gk6.a(a4, g55_b_d, this);
                    if (obj == a) {
                        return a;
                    }
                    mFUser = mFUser3;
                    ap4 = (ap4) obj;
                    fh6 fh6 = this.$isAddressSaved$inlined;
                    if (!(ap4 instanceof cp4)) {
                    }
                    fh6.element = z;
                }
                this.this$0.j().a();
                if (this.$isAddressSaved$inlined.element) {
                }
                return cd6.a;
            } else if (i == 4) {
                mFUser = (MFUser) this.L$2;
                MFUser mFUser4 = (MFUser) this.L$1;
                il6 il63 = (il6) this.L$0;
                nc6.a(obj);
                ap4 = (ap4) obj;
                fh6 fh62 = this.$isAddressSaved$inlined;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String l4 = CommuteTimeSettingsDefaultAddressPresenter.n;
                    local4.d(l4, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " successfully");
                    z = true;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String l5 = CommuteTimeSettingsDefaultAddressPresenter.n;
                    local5.d(l5, "update UserRepository to local DB success - home = " + mFUser.getHome() + " - work = " + mFUser.getWork() + " failed");
                } else {
                    throw new kc6();
                }
                fh62.element = z;
                this.this$0.j().a();
                if (this.$isAddressSaved$inlined.element) {
                    this.this$0.j().E(this.$address$inlined);
                } else {
                    this.this$0.j().E((String) null);
                }
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            il6 = il62;
            MFUser mFUser5 = (MFUser) obj;
            if (mFUser5 != null) {
                String b = this.this$0.g;
                if (b != null) {
                    int hashCode = b.hashCode();
                    if (hashCode != 2255103) {
                        if (hashCode == 76517104 && b.equals("Other")) {
                            this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getWork(), this.$address$inlined);
                            mFUser5.setWork(this.$address$inlined);
                        }
                    } else if (b.equals("Home")) {
                        this.$isChanged$inlined.element = !TextUtils.equals(mFUser5.getHome(), this.$address$inlined);
                        mFUser5.setHome(this.$address$inlined);
                    }
                }
                if (this.$isChanged$inlined.element) {
                    this.this$0.j().b();
                    dl6 a5 = this.this$0.c();
                    g55$b$c g55_b_c = new g55$b$c(mFUser5, (xe6) null, this);
                    this.L$0 = il6;
                    this.L$1 = mFUser5;
                    this.L$2 = mFUser5;
                    this.label = 3;
                    Object a6 = gk6.a(a5, g55_b_c, this);
                    if (a6 == a) {
                        return a;
                    }
                    mFUser2 = mFUser5;
                    obj = a6;
                    mFUser3 = mFUser2;
                    ap42 = (ap4) obj;
                    if (!(ap42 instanceof cp4)) {
                    }
                    this.this$0.j().a();
                }
                if (this.$isAddressSaved$inlined.element) {
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.commutetime.settings.CommuteTimeSettingsDefaultAddressPresenter$start$1", f = "CommuteTimeSettingsDefaultAddressPresenter.kt", l = {40}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CommuteTimeSettingsDefaultAddressPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(CommuteTimeSettingsDefaultAddressPresenter commuteTimeSettingsDefaultAddressPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = commuteTimeSettingsDefaultAddressPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                g55$c$a g55_c_a = new g55$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(a2, g55_c_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            wg6.a(obj, "withContext(IO) { mShare\u2026r.addressSearchedRecent }");
            this.this$0.j.clear();
            this.this$0.j.addAll((List) obj);
            this.this$0.e = Places.createClient(PortfolioApp.get.instance());
            this.this$0.j().a(this.this$0.e);
            if (wg6.a((Object) this.this$0.g, (Object) "Home")) {
                d55 j = this.this$0.j();
                String d = this.this$0.h;
                wg6.a((Object) d, "mHomeTitle");
                j.setTitle(d);
            } else {
                d55 j2 = this.this$0.j();
                String g = this.this$0.i;
                wg6.a((Object) g, "mWorkTitle");
                j2.setTitle(g);
            }
            this.this$0.j().y(this.this$0.f);
            this.this$0.j().j(this.this$0.j);
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = CommuteTimeSettingsDefaultAddressPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "CommuteTimeSettingsDefau\u2026er::class.java.simpleName");
        n = simpleName;
    }
    */

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public CommuteTimeSettingsDefaultAddressPresenter(d55 d55, an4 an4, UserRepository userRepository) {
        wg6.b(d55, "mView");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(userRepository, "mUserRepository");
        this.k = d55;
        this.l = an4;
        this.m = userRepository;
    }

    @DexIgnore
    public final an4 h() {
        return this.l;
    }

    @DexIgnore
    public final UserRepository i() {
        return this.m;
    }

    @DexIgnore
    public final d55 j() {
        return this.k;
    }

    @DexIgnore
    public void k() {
        this.k.a(this);
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        this.e = null;
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "addressType");
        wg6.b(str2, "defaultPlace");
        this.f = str2;
        this.g = str;
    }

    @DexIgnore
    public void a(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = n;
        local.d(str2, "onUserExit with address " + str);
        fh6 fh6 = new fh6();
        fh6.element = false;
        fh6 fh62 = new fh6();
        fh62.element = false;
        if (str == null || ik6.b(e(), (af6) null, (ll6) null, new b(str, (xe6) null, this, str, fh6, fh62), 3, (Object) null) == null) {
            this.k.E((String) null);
            cd6 cd6 = cd6.a;
        }
    }
}
