package com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders;

import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.p05;
import com.fossil.q05;
import com.fossil.qg6;
import com.fossil.r05$b$a;
import com.fossil.r05$c$a;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.RemindTimeModel;
import com.portfolio.platform.data.source.local.reminders.RemindersSettingsDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RemindTimePresenter extends p05 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public int e;
    @DexIgnore
    public RemindTimeModel f;
    @DexIgnore
    public /* final */ q05 g;
    @DexIgnore
    public /* final */ RemindersSettingsDatabase h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter$save$1", f = "RemindTimePresenter.kt", l = {42}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ RemindTimePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(RemindTimePresenter remindTimePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = remindTimePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 a2 = this.this$0.c();
                r05$b$a r05_b_a = new r05$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(a2, r05_b_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            this.this$0.g.close();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.RemindTimePresenter$start$1", f = "RemindTimePresenter.kt", l = {29}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ RemindTimePresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(RemindTimePresenter remindTimePresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = remindTimePresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            RemindTimePresenter remindTimePresenter;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                RemindTimePresenter remindTimePresenter2 = this.this$0;
                dl6 a2 = remindTimePresenter2.c();
                r05$c$a r05_c_a = new r05$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = remindTimePresenter2;
                this.label = 1;
                obj = gk6.a(a2, r05_c_a, this);
                if (obj == a) {
                    return a;
                }
                remindTimePresenter = remindTimePresenter2;
            } else if (i == 1) {
                remindTimePresenter = (RemindTimePresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            remindTimePresenter.f = (RemindTimeModel) obj;
            q05 d = this.this$0.g;
            RemindTimeModel b = this.this$0.f;
            if (b != null) {
                d.b(b.getMinutes());
                return cd6.a;
            }
            wg6.a();
            throw null;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = RemindTimePresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "RemindTimePresenter::class.java.simpleName");
        i = simpleName;
    }
    */

    @DexIgnore
    public RemindTimePresenter(q05 q05, RemindersSettingsDatabase remindersSettingsDatabase) {
        wg6.b(q05, "mView");
        wg6.b(remindersSettingsDatabase, "mRemindersSettingsDatabase");
        this.g = q05;
        this.h = remindersSettingsDatabase;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(i, "start");
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(i, "stop");
    }

    @DexIgnore
    public void h() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = i;
        local.d(str, "save(), minutes = " + this.e);
        RemindTimeModel remindTimeModel = this.f;
        if (remindTimeModel != null) {
            remindTimeModel.setMinutes(this.e);
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public void i() {
        this.g.a(this);
    }

    @DexIgnore
    public void a(int i2) {
        this.e = i2 * 20;
    }
}
