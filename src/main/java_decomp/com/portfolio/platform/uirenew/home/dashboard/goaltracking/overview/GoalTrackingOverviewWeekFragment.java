package com.portfolio.platform.uirenew.home.dashboard.goaltracking.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import com.fossil.ax5;
import com.fossil.b06;
import com.fossil.fa4;
import com.fossil.kb;
import com.fossil.qg6;
import com.fossil.ue5;
import com.fossil.ut4;
import com.fossil.vd;
import com.fossil.ve5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.ui.view.chart.base.BarChart;
import com.portfolio.platform.ui.view.chart.overview.OverviewWeekChart;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingOverviewWeekFragment extends BaseFragment implements ve5 {
    @DexIgnore
    public b06 f;
    @DexIgnore
    public ax5<fa4> g;
    @DexIgnore
    public ue5 h;
    @DexIgnore
    public HashMap i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingOverviewWeekFragment a;

        @DexIgnore
        public b(GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment, fa4 fa4) {
            this.a = goalTrackingOverviewWeekFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            GoalTrackingOverviewWeekFragment.a(this.a).a().a(1);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public static final /* synthetic */ b06 a(GoalTrackingOverviewWeekFragment goalTrackingOverviewWeekFragment) {
        b06 b06 = goalTrackingOverviewWeekFragment.f;
        if (b06 != null) {
            return b06;
        }
        wg6.d("mHomeDashboardViewModel");
        throw null;
    }

    @DexIgnore
    public void c(boolean z) {
        fa4 a2;
        ax5<fa4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null) {
            if (z) {
                OverviewWeekChart overviewWeekChart = a2.s;
                wg6.a((Object) overviewWeekChart, "binding.weekChart");
                overviewWeekChart.setVisibility(4);
                ConstraintLayout constraintLayout = a2.q;
                wg6.a((Object) constraintLayout, "binding.clTracking");
                constraintLayout.setVisibility(0);
                return;
            }
            OverviewWeekChart overviewWeekChart2 = a2.s;
            wg6.a((Object) overviewWeekChart2, "binding.weekChart");
            overviewWeekChart2.setVisibility(0);
            ConstraintLayout constraintLayout2 = a2.q;
            wg6.a((Object) constraintLayout2, "binding.clTracking");
            constraintLayout2.setVisibility(4);
        }
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.i;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public String h1() {
        return "GoalTrackingOverviewWeekFragment";
    }

    @DexIgnore
    public boolean i1() {
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onActivityBackPressed");
        return false;
    }

    @DexIgnore
    public final void j1() {
        fa4 a2;
        OverviewWeekChart overviewWeekChart;
        ax5<fa4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.s) != null) {
            overviewWeekChart.a("hybridGoalTrackingTab", "nonBrandNonReachGoal");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        fa4 a2;
        wg6.b(layoutInflater, "inflater");
        GoalTrackingOverviewWeekFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onCreateView");
        fa4 a3 = kb.a(layoutInflater, 2131558556, viewGroup, false, e1());
        FragmentActivity activity = getActivity();
        if (activity != null) {
            b06 a4 = vd.a(activity).a(b06.class);
            wg6.a((Object) a4, "ViewModelProviders.of(it\u2026ardViewModel::class.java)");
            this.f = a4;
            a3.r.setOnClickListener(new b(this, a3));
        }
        this.g = new ax5<>(this, a3);
        j1();
        ax5<fa4> ax5 = this.g;
        if (ax5 == null || (a2 = ax5.a()) == null) {
            return null;
        }
        return a2.d();
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        GoalTrackingOverviewWeekFragment.super.onResume();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onResume");
        j1();
        ue5 ue5 = this.h;
        if (ue5 != null) {
            ue5.f();
        }
    }

    @DexIgnore
    public void onStop() {
        GoalTrackingOverviewWeekFragment.super.onStop();
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onStop");
        ue5 ue5 = this.h;
        if (ue5 != null) {
            ue5.g();
        }
    }

    @DexIgnore
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "onViewCreated");
    }

    @DexIgnore
    public void a(ut4 ut4) {
        fa4 a2;
        OverviewWeekChart overviewWeekChart;
        wg6.b(ut4, "baseModel");
        FLogger.INSTANCE.getLocal().d("GoalTrackingOverviewWeekFragment", "showWeekDetails");
        ax5<fa4> ax5 = this.g;
        if (ax5 != null && (a2 = ax5.a()) != null && (overviewWeekChart = a2.s) != null) {
            new ArrayList();
            BarChart.c cVar = (BarChart.c) ut4;
            cVar.b(ut4.a.a(cVar.c()));
            ut4.a aVar = ut4.a;
            wg6.a((Object) overviewWeekChart, "it");
            Context context = overviewWeekChart.getContext();
            wg6.a((Object) context, "it.context");
            BarChart.a((BarChart) overviewWeekChart, (ArrayList) aVar.a(context, cVar), false, 2, (Object) null);
            overviewWeekChart.a(ut4);
        }
    }

    @DexIgnore
    public void a(ue5 ue5) {
        wg6.b(ue5, "presenter");
        this.h = ue5;
    }
}
