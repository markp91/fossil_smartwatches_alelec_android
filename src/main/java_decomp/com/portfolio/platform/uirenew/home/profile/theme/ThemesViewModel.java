package com.portfolio.platform.uirenew.home.profile.theme;

import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.nm5;
import com.fossil.nm5$c$a;
import com.fossil.nm5$e$a;
import com.fossil.nm5$f$a;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.td;
import com.fossil.ud;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemesViewModel extends td {
    @DexIgnore
    public static /* final */ String h;
    @DexIgnore
    public ArrayList<Theme> a; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<Style> b; // = new ArrayList<>();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public MutableLiveData<nm5.b> e; // = new MutableLiveData<>();
    @DexIgnore
    public b f; // = new b((ArrayList) null, (List) null, (String) null, (String) null, false, 31, (qg6) null);
    @DexIgnore
    public /* final */ ThemeRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public ArrayList<Theme> a;
        @DexIgnore
        public List<Style> b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public boolean e;

        @DexIgnore
        public b() {
            this((ArrayList) null, (List) null, (String) null, (String) null, false, 31, (qg6) null);
        }

        @DexIgnore
        public b(ArrayList<Theme> arrayList, List<Style> list, String str, String str2, boolean z) {
            this.a = arrayList;
            this.b = list;
            this.c = str;
            this.d = str2;
            this.e = z;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final boolean b() {
            return this.e;
        }

        @DexIgnore
        public final ArrayList<Theme> c() {
            return this.a;
        }

        @DexIgnore
        public final List<Style> d() {
            return this.b;
        }

        @DexIgnore
        public final String e() {
            return this.d;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(ArrayList arrayList, List list, String str, String str2, boolean z, int i, qg6 qg6) {
            this(r10, (i & 2) != 0 ? null : list, (i & 4) != 0 ? null : str, (i & 8) == 0 ? str2 : r0, (i & 16) != 0 ? false : z);
            String str3 = null;
            ArrayList arrayList2 = (i & 1) != 0 ? null : arrayList;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.e = z;
        }

        @DexIgnore
        public final void a(ArrayList<Theme> arrayList, String str, String str2, List<Style> list) {
            this.a = arrayList;
            this.d = str2;
            this.c = str;
            this.e = false;
            this.b = list;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$applyTheme$1", f = "ThemesViewModel.kt", l = {66}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ThemesViewModel themesViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                nm5$c$a nm5_c_a = new nm5$c$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, nm5_c_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$deleteTheme$1", f = "ThemesViewModel.kt", l = {79, 80, 82, 83, 85}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $id;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(ThemesViewModel themesViewModel, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
            this.$id = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, this.$id, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:25:0x00b1 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x00b2  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x00b9  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00d9  */
        /* JADX WARNING: Removed duplicated region for block: B:37:0x0101 A[RETURN] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x0102  */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x0109  */
        /* JADX WARNING: Removed duplicated region for block: B:44:0x0170  */
        public final Object invokeSuspend(Object obj) {
            ThemesViewModel themesViewModel;
            ArrayList arrayList;
            il6 il6;
            Theme theme;
            ThemesViewModel themesViewModel2;
            String str;
            Object listStyleById;
            Theme theme2;
            ThemesViewModel themesViewModel3;
            Theme theme3;
            il6 il62;
            Object listTheme;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il63 = this.p$;
                ThemeRepository e = this.this$0.g;
                String str2 = this.$id;
                this.L$0 = il63;
                this.label = 1;
                Object themeById = e.getThemeById(str2, this);
                if (themeById == a) {
                    return a;
                }
                Object obj2 = themeById;
                il62 = il63;
                obj = obj2;
            } else if (i == 1) {
                il62 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2) {
                nc6.a(obj);
                theme3 = (Theme) this.L$1;
                il62 = (il6) this.L$0;
                ThemesViewModel themesViewModel4 = this.this$0;
                ThemeRepository e2 = themesViewModel4.g;
                this.L$0 = il62;
                this.L$1 = theme3;
                this.L$2 = themesViewModel4;
                this.label = 3;
                listTheme = e2.getListTheme(this);
                if (listTheme != a) {
                    return a;
                }
                ThemesViewModel themesViewModel5 = themesViewModel4;
                theme2 = theme3;
                obj = listTheme;
                il6 = il62;
                themesViewModel3 = themesViewModel5;
                if (obj == null) {
                }
            } else if (i == 3) {
                themesViewModel3 = (ThemesViewModel) this.L$2;
                nc6.a(obj);
                il6 il64 = (il6) this.L$0;
                theme2 = (Theme) this.L$1;
                il6 = il64;
                if (obj == null) {
                    themesViewModel3.a = (ArrayList) obj;
                    themesViewModel2 = this.this$0;
                    ThemeRepository e3 = themesViewModel2.g;
                    this.L$0 = il6;
                    this.L$1 = theme2;
                    this.L$2 = themesViewModel2;
                    this.label = 4;
                    obj = e3.getCurrentThemeId(this);
                    if (obj == a) {
                        return a;
                    }
                    theme = theme2;
                    str = (String) obj;
                    if (str == null) {
                    }
                    themesViewModel2.c = str;
                    ThemesViewModel themesViewModel6 = this.this$0;
                    themesViewModel6.d = themesViewModel6.c;
                    ThemesViewModel themesViewModel7 = this.this$0;
                    ThemeRepository e4 = themesViewModel7.g;
                    String b = this.this$0.c;
                    this.L$0 = il6;
                    this.L$1 = theme;
                    this.L$2 = themesViewModel7;
                    this.label = 5;
                    listStyleById = e4.getListStyleById(b, this);
                    if (listStyleById != a) {
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.collections.ArrayList<com.portfolio.platform.data.model.Theme> /* = java.util.ArrayList<com.portfolio.platform.data.model.Theme> */");
                }
            } else if (i == 4) {
                themesViewModel2 = (ThemesViewModel) this.L$2;
                theme = (Theme) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
                str = (String) obj;
                if (str == null) {
                    str = "default";
                }
                themesViewModel2.c = str;
                ThemesViewModel themesViewModel62 = this.this$0;
                themesViewModel62.d = themesViewModel62.c;
                ThemesViewModel themesViewModel72 = this.this$0;
                ThemeRepository e42 = themesViewModel72.g;
                String b2 = this.this$0.c;
                this.L$0 = il6;
                this.L$1 = theme;
                this.L$2 = themesViewModel72;
                this.label = 5;
                listStyleById = e42.getListStyleById(b2, this);
                if (listStyleById != a) {
                    return a;
                }
                themesViewModel = themesViewModel72;
                obj = listStyleById;
                arrayList = (ArrayList) obj;
                if (arrayList == null) {
                }
                themesViewModel.b = arrayList;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String e5 = ThemesViewModel.h;
                local.d(e5, "mCurrentThemeId=" + this.this$0.c + " mThemes.size=" + this.this$0.a.size());
                this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
                this.this$0.b();
                return cd6.a;
            } else if (i == 5) {
                themesViewModel = (ThemesViewModel) this.L$2;
                Theme theme4 = (Theme) this.L$1;
                il6 il65 = (il6) this.L$0;
                nc6.a(obj);
                arrayList = (ArrayList) obj;
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                themesViewModel.b = arrayList;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String e52 = ThemesViewModel.h;
                local2.d(e52, "mCurrentThemeId=" + this.this$0.c + " mThemes.size=" + this.this$0.a.size());
                this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
                this.this$0.b();
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            theme3 = (Theme) obj;
            ThemeRepository e6 = this.this$0.g;
            if (theme3 != null) {
                this.L$0 = il62;
                this.L$1 = theme3;
                this.label = 2;
                if (e6.deleteTheme(theme3, this) == a) {
                    return a;
                }
                ThemesViewModel themesViewModel42 = this.this$0;
                ThemeRepository e22 = themesViewModel42.g;
                this.L$0 = il62;
                this.L$1 = theme3;
                this.L$2 = themesViewModel42;
                this.label = 3;
                listTheme = e22.getListTheme(this);
                if (listTheme != a) {
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1", f = "ThemesViewModel.kt", l = {50}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $themeId;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(ThemesViewModel themesViewModel, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
            this.$themeId = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, this.$themeId, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ThemesViewModel themesViewModel;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                this.this$0.d = this.$themeId;
                ThemesViewModel themesViewModel2 = this.this$0;
                dl6 b = zl6.b();
                nm5$e$a nm5_e_a = new nm5$e$a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = themesViewModel2;
                this.label = 1;
                obj = gk6.a(b, nm5_e_a, this);
                if (obj == a) {
                    return a;
                }
                themesViewModel = themesViewModel2;
            } else if (i == 1) {
                themesViewModel = (ThemesViewModel) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            themesViewModel.b = (ArrayList) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = ThemesViewModel.h;
            local.d(e, "userSelectedId " + this.this$0.d + " currentThemeId " + this.this$0.c);
            this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.b();
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$start$1", f = "ThemesViewModel.kt", l = {27}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ ThemesViewModel this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(ThemesViewModel themesViewModel, xe6 xe6) {
            super(2, xe6);
            this.this$0 = themesViewModel;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = zl6.b();
                nm5$f$a nm5_f_a = new nm5$f$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                if (gk6.a(b, nm5_f_a, this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String e = ThemesViewModel.h;
            local.d(e, "mCurrentThemeId=" + this.this$0.c);
            this.this$0.f.a(this.this$0.a, this.this$0.c, this.this$0.d, this.this$0.b);
            this.this$0.b();
            return cd6.a;
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ThemesViewModel.class.getSimpleName();
        wg6.a((Object) simpleName, "ThemesViewModel::class.java.simpleName");
        h = simpleName;
    }
    */

    @DexIgnore
    public ThemesViewModel(ThemeRepository themeRepository) {
        wg6.b(themeRepository, "mThemesRepository");
        this.g = themeRepository;
    }

    @DexIgnore
    public final MutableLiveData<nm5.b> c() {
        return this.e;
    }

    @DexIgnore
    public final void d() {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void a() {
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b() {
        this.e.a(this.f);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "id");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = h;
        local.d(str2, "deleteTheme id=" + str);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(this, str, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "themeId");
        rm6 unused = ik6.b(ud.a(this), (af6) null, (ll6) null, new e(this, str, (xe6) null), 3, (Object) null);
    }
}
