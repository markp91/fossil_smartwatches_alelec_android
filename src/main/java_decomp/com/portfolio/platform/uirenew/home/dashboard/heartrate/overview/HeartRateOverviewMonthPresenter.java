package com.portfolio.platform.uirenew.home.dashboard.heartrate.overview;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.ld;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.pf5;
import com.fossil.qf5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rf5$b$a;
import com.fossil.rf5$d$a;
import com.fossil.rm6;
import com.fossil.sd;
import com.fossil.sf6;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.wh4;
import com.fossil.xe6;
import com.fossil.yx5;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.log.MFLogger;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateOverviewMonthPresenter extends pf5 {
    @DexIgnore
    public MutableLiveData<Date> e; // = new MutableLiveData<>();
    @DexIgnore
    public Date f;
    @DexIgnore
    public Date g;
    @DexIgnore
    public Date h;
    @DexIgnore
    public Date i;
    @DexIgnore
    public LiveData<yx5<List<DailyHeartRateSummary>>> j; // = new MutableLiveData();
    @DexIgnore
    public List<DailyHeartRateSummary> k; // = new ArrayList();
    @DexIgnore
    public LiveData<yx5<List<DailyHeartRateSummary>>> l;
    @DexIgnore
    public /* final */ qf5 m;
    @DexIgnore
    public /* final */ UserRepository n;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthPresenter$loadData$2", f = "HeartRateOverviewMonthPresenter.kt", l = {95}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                dl6 b = this.this$0.c();
                rf5$b$a rf5_b_a = new rf5$b$a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = gk6.a(b, rf5_b_a, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            MFUser mFUser = (MFUser) obj;
            if (mFUser != null) {
                this.this$0.i = bk4.d(mFUser.getCreatedAt());
                qf5 l = this.this$0.m;
                Date d = HeartRateOverviewMonthPresenter.d(this.this$0);
                Date c = this.this$0.i;
                if (c == null) {
                    c = new Date();
                }
                l.a(d, c);
                this.this$0.e.a(new Date());
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<I, O> implements v3<X, LiveData<Y>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter a;

        @DexIgnore
        public c(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            this.a = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final LiveData<yx5<List<DailyHeartRateSummary>>> apply(Date date) {
            HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter = this.a;
            wg6.a((Object) date, "it");
            if (heartRateOverviewMonthPresenter.b(date)) {
                HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter2 = this.a;
                heartRateOverviewMonthPresenter2.j = heartRateOverviewMonthPresenter2.o.getHeartRateSummaries(HeartRateOverviewMonthPresenter.j(this.a), HeartRateOverviewMonthPresenter.g(this.a), true);
            }
            return this.a.j;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d<T> implements ld<yx5<? extends List<DailyHeartRateSummary>>> {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateOverviewMonthPresenter a;

        @DexIgnore
        public d(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
            this.a = heartRateOverviewMonthPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(yx5<? extends List<DailyHeartRateSummary>> yx5) {
            List list;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("mDateTransformations - status=");
            sb.append(yx5 != null ? yx5.f() : null);
            sb.append(" -- data.size=");
            sb.append((yx5 == null || (list = (List) yx5.d()) == null) ? null : Integer.valueOf(list.size()));
            local.d("HeartRateOverviewMonthPresenter", sb.toString());
            if ((yx5 != null ? yx5.f() : null) != wh4.DATABASE_LOADING) {
                List list2 = yx5 != null ? (List) yx5.d() : null;
                if (list2 != null && (!wg6.a((Object) this.a.k, (Object) list2))) {
                    rm6 unused = ik6.b(this.a.e(), (af6) null, (ll6) null, new rf5$d$a(this, list2, yx5, (xe6) null), 3, (Object) null);
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public HeartRateOverviewMonthPresenter(qf5 qf5, UserRepository userRepository, HeartRateSummaryRepository heartRateSummaryRepository) {
        wg6.b(qf5, "mView");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(heartRateSummaryRepository, "mHeartRateSummaryRepository");
        this.m = qf5;
        this.n = userRepository;
        this.o = heartRateSummaryRepository;
        LiveData<yx5<List<DailyHeartRateSummary>>> b2 = sd.b(this.e, new c(this));
        wg6.a((Object) b2, "Transformations.switchMa\u2026mHeartRateSummaries\n    }");
        this.l = b2;
    }

    @DexIgnore
    public static final /* synthetic */ Date d(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.f;
        if (date != null) {
            return date;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date g(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.h;
        if (date != null) {
            return date;
        }
        wg6.d("mEndDate");
        throw null;
    }

    @DexIgnore
    public static final /* synthetic */ Date j(HeartRateOverviewMonthPresenter heartRateOverviewMonthPresenter) {
        Date date = heartRateOverviewMonthPresenter.g;
        if (date != null) {
            return date;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d("HeartRateOverviewMonthPresenter", "start");
        h();
        LiveData<yx5<List<DailyHeartRateSummary>>> liveData = this.l;
        qf5 qf5 = this.m;
        if (qf5 != null) {
            liveData.a((HeartRateOverviewMonthFragment) qf5, new d(this));
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    public void g() {
        MFLogger.d("HeartRateOverviewMonthPresenter", "stop");
        LiveData<yx5<List<DailyHeartRateSummary>>> liveData = this.l;
        qf5 qf5 = this.m;
        if (qf5 != null) {
            liveData.a((HeartRateOverviewMonthFragment) qf5);
            this.j.a(this.m);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.dashboard.heartrate.overview.HeartRateOverviewMonthFragment");
    }

    @DexIgnore
    public void h() {
        Date date = this.f;
        if (date != null) {
            if (date == null) {
                wg6.d("mCurrentDate");
                throw null;
            } else if (bk4.t(date).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("loadData - mDate=");
                Date date2 = this.f;
                if (date2 != null) {
                    sb.append(date2);
                    local.d("HeartRateOverviewMonthPresenter", sb.toString());
                    return;
                }
                wg6.d("mCurrentDate");
                throw null;
            }
        }
        this.f = new Date();
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        StringBuilder sb2 = new StringBuilder();
        sb2.append("loadData - mDate=");
        Date date3 = this.f;
        if (date3 != null) {
            sb2.append(date3);
            local2.d("HeartRateOverviewMonthPresenter", sb2.toString());
            rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new b(this, (xe6) null), 3, (Object) null);
            return;
        }
        wg6.d("mCurrentDate");
        throw null;
    }

    @DexIgnore
    public void i() {
        this.m.a(this);
    }

    @DexIgnore
    public final boolean b(Date date) {
        Date date2;
        Date date3 = this.i;
        if (date3 == null) {
            date3 = new Date();
        }
        this.g = date3;
        Date date4 = this.g;
        if (date4 != null) {
            if (!bk4.a(date4.getTime(), date.getTime())) {
                Calendar p = bk4.p(date);
                wg6.a((Object) p, "DateHelper.getStartOfMonth(date)");
                Date time = p.getTime();
                wg6.a((Object) time, "DateHelper.getStartOfMonth(date).time");
                this.g = time;
            }
            Boolean s = bk4.s(date);
            wg6.a((Object) s, "DateHelper.isThisMonth(date)");
            if (s.booleanValue()) {
                date2 = new Date();
            } else {
                Calendar k2 = bk4.k(date);
                wg6.a((Object) k2, "DateHelper.getEndOfMonth(date)");
                date2 = k2.getTime();
                wg6.a((Object) date2, "DateHelper.getEndOfMonth(date).time");
            }
            this.h = date2;
            Date date5 = this.h;
            if (date5 != null) {
                long time2 = date5.getTime();
                Date date6 = this.g;
                if (date6 != null) {
                    return time2 >= date6.getTime();
                }
                wg6.d("mStartDate");
                throw null;
            }
            wg6.d("mEndDate");
            throw null;
        }
        wg6.d("mStartDate");
        throw null;
    }

    @DexIgnore
    public void a(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        if (this.e.a() == null || !bk4.d((Date) this.e.a(), date)) {
            this.e.a(date);
        }
    }
}
