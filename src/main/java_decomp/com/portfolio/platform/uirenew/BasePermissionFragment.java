package com.portfolio.platform.uirenew;

import android.content.Intent;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uh4;
import com.fossil.wg6;
import com.fossil.xm4;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BasePermissionFragment extends BaseFragment implements AlertDialogFragment.g {
    @DexIgnore
    public HashMap f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        wg6.a((Object) BasePermissionFragment.class.getSimpleName(), "BasePermissionFragment::class.java.simpleName");
    }
    */

    @DexIgnore
    public final void a(uh4... uh4Arr) {
        wg6.b(uh4Arr, "permissionCodes");
        ArrayList arrayList = new ArrayList();
        for (uh4 add : uh4Arr) {
            arrayList.add(add);
        }
        xm4.d.a(getContext(), (ArrayList<uh4>) arrayList);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.f;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void a(String str, int i, Intent intent) {
        wg6.b(str, "tag");
        if (getActivity() instanceof BaseActivity) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.a(str, i, intent);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
    }
}
