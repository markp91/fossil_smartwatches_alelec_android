package com.portfolio.platform.uirenew.onboarding.forgotPassword;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import androidx.fragment.app.Fragment;
import com.fossil.iq5;
import com.fossil.kq5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.ForgotPasswordFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForgotPasswordActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public kq5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wg6.b(context, "context");
            wg6.b(str, "email");
            Intent intent = new Intent(context, ForgotPasswordActivity.class);
            intent.putExtra("EMAIL", str);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.BaseActivity, android.app.Activity, com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        Intent intent = getIntent();
        String stringExtra = intent != null ? intent.getStringExtra("EMAIL") : null;
        ForgotPasswordFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = ForgotPasswordFragment.j.b();
            a((Fragment) b, ForgotPasswordFragment.j.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new iq5(b)).a(this);
            if (bundle == null) {
                kq5 kq5 = this.B;
                if (kq5 != null) {
                    if (stringExtra == null) {
                        stringExtra = "";
                    }
                    kq5.c(stringExtra);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            } else if (bundle.containsKey("EMAIL")) {
                kq5 kq52 = this.B;
                if (kq52 != null) {
                    String string = bundle.getString("EMAIL");
                    if (string == null) {
                        string = "";
                    }
                    kq52.c(string);
                    return;
                }
                wg6.d("mPresenter");
                throw null;
            }
        } else {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordContract.View");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.app.Activity, com.portfolio.platform.uirenew.onboarding.forgotPassword.ForgotPasswordActivity] */
    public void onSaveInstanceState(Bundle bundle, PersistableBundle persistableBundle) {
        if (bundle != null) {
            kq5 kq5 = this.B;
            if (kq5 != null) {
                kq5.a("EMAIL", bundle);
            } else {
                wg6.d("mPresenter");
                throw null;
            }
        }
        ForgotPasswordActivity.super.onSaveInstanceState(bundle, persistableBundle);
    }
}
