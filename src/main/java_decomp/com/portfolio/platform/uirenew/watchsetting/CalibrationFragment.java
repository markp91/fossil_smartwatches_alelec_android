package com.portfolio.platform.uirenew.watchsetting;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.dk4;
import com.fossil.gv5;
import com.fossil.hm4;
import com.fossil.hv5;
import com.fossil.jj4;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lj4;
import com.fossil.lx5;
import com.fossil.mj4;
import com.fossil.mt;
import com.fossil.mz;
import com.fossil.pr;
import com.fossil.qg6;
import com.fossil.r64;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.yz;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.ui.BasePermissionFragment;
import com.portfolio.platform.view.AlertDialogFragment;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalibrationFragment extends BasePermissionFragment implements hv5, AlertDialogFragment.g, hm4.b, AlertDialogFragment.h {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public gv5 i;
    @DexIgnore
    public ax5<r64> j;
    @DexIgnore
    public mj4 o;
    @DexIgnore
    public List<dk4.b> p; // = new ArrayList();
    @DexIgnore
    public HashMap q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final CalibrationFragment a() {
            return new CalibrationFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationFragment a;

        @DexIgnore
        public b(CalibrationFragment calibrationFragment) {
            this.a = calibrationFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CalibrationFragment.b(this.a).h();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationFragment a;

        @DexIgnore
        public c(CalibrationFragment calibrationFragment) {
            this.a = calibrationFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            CalibrationFragment.b(this.a).j();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r64 a;
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements mz<Drawable> {
            @DexIgnore
            public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String z1 = CalibrationFragment.r;
                local.d(z1, "showHour onLoadFail e=" + mtVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
                wg6.b(drawable, "resource");
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                wg6.b(prVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(CalibrationFragment.r, "showHour onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public d(r64 r64, CalibrationFragment calibrationFragment, String str) {
            this.a = r64;
            this.b = calibrationFragment;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            mj4 a2;
            lj4 a3;
            lj4 b2;
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String z1 = CalibrationFragment.r;
            local.d(z1, "showHour onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive() && (a2 = this.b.o) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b(new a())) != null) {
                b2.a(this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r64 a;
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements mz<Drawable> {
            @DexIgnore
            public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String z1 = CalibrationFragment.r;
                local.d(z1, "showMinute onLoadFail e=" + mtVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
                wg6.b(drawable, "resource");
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                wg6.b(prVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(CalibrationFragment.r, "showMinute onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public e(r64 r64, CalibrationFragment calibrationFragment, String str) {
            this.a = r64;
            this.b = calibrationFragment;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            mj4 a2;
            lj4 a3;
            lj4 b2;
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String z1 = CalibrationFragment.r;
            local.d(z1, "showMinute onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive() && (a2 = this.b.o) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b(new a())) != null) {
                b2.a(this.a.r);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements CloudImageHelper.OnImageCallbackListener {
        @DexIgnore
        public /* final */ /* synthetic */ r64 a;
        @DexIgnore
        public /* final */ /* synthetic */ CalibrationFragment b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements mz<Drawable> {
            @DexIgnore
            public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String z1 = CalibrationFragment.r;
                local.d(z1, "showSubeye onLoadFail e=" + mtVar);
                return true;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
                wg6.b(drawable, "resource");
                wg6.b(obj, "model");
                wg6.b(yzVar, RemoteFLogger.MESSAGE_TARGET_KEY);
                wg6.b(prVar, "dataSource");
                FLogger.INSTANCE.getLocal().d(CalibrationFragment.r, "showSubeye onResourceReady");
                return false;
            }
        }

        @DexIgnore
        public f(r64 r64, CalibrationFragment calibrationFragment, String str) {
            this.a = r64;
            this.b = calibrationFragment;
        }

        @DexIgnore
        public void onImageCallback(String str, String str2) {
            mj4 a2;
            lj4 a3;
            lj4 b2;
            wg6.b(str, "serial");
            wg6.b(str2, "filePath");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String z1 = CalibrationFragment.r;
            local.d(z1, "showSubeye onImageCallback serial=" + str + ' ' + "filepath=" + str2 + " isActive=" + this.b.isActive() + " request=" + this.b.o);
            if (this.b.isActive() && (a2 = this.b.o) != null && (a3 = a2.a(str2)) != null && (b2 = a3.b(new a())) != null) {
                b2.a(this.a.r);
            }
        }
    }

    /*
    static {
        String simpleName = CalibrationFragment.class.getSimpleName();
        wg6.a((Object) simpleName, "CalibrationFragment::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gv5 b(CalibrationFragment calibrationFragment) {
        gv5 gv5 = calibrationFragment.i;
        if (gv5 != null) {
            return gv5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void D() {
        FLogger.INSTANCE.getLocal().d(r, "showBluetoothError");
        if (isActive()) {
            gv5 gv5 = this.i;
            if (gv5 != null) {
                gv5.a(true);
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.e(childFragmentManager);
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void R() {
        FLogger.INSTANCE.getLocal().d(r, "showGeneralError");
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.a(true);
            if (isActive()) {
                lx5 lx5 = lx5.c;
                FragmentManager childFragmentManager = getChildFragmentManager();
                wg6.a((Object) childFragmentManager, "childFragmentManager");
                lx5.B(childFragmentManager);
                return;
            }
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public void T(String str) {
        if (!TextUtils.isEmpty(str) && str != null && str.hashCode() == 1925385819 && str.equals("DEVICE_CONNECT_FAILED")) {
            gv5 gv5 = this.i;
            if (gv5 != null) {
                gv5.a(false);
                FragmentActivity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                    return;
                }
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view, Boolean bool) {
    }

    @DexIgnore
    public void c(View view) {
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onPress");
        view.setPressed(true);
    }

    @DexIgnore
    public void d(View view) {
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onRelease");
        view.setPressed(false);
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.q;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    public boolean i1() {
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.h();
            return true;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r3v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void j(String str) {
        wg6.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showSubeye: deviceId = " + str);
            if (this.p.size() > 2) {
                ax5<r64> ax5 = this.j;
                if (ax5 != null) {
                    r64 a2 = ax5.a();
                    if (a2 != null) {
                        Object r2 = a2.w;
                        wg6.a((Object) r2, "binding.ftvTitle");
                        r2.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886902));
                        Object r22 = a2.u;
                        wg6.a((Object) r22, "binding.ftvDescription");
                        r22.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886901));
                        CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_SUB_EYE);
                        AppCompatImageView appCompatImageView = a2.r;
                        wg6.a((Object) appCompatImageView, "binding.acivDevice");
                        type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(2))).setImageCallback(new f(a2, this, str)).downloadForCalibration();
                        return;
                    }
                    return;
                }
                wg6.d("mBinding");
                throw null;
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void k(String str) {
        wg6.b(str, "deviceId");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = r;
        local.d(str2, "showHour: deviceId = " + str);
        if (isActive()) {
            ax5<r64> ax5 = this.j;
            if (ax5 != null) {
                r64 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.w;
                    wg6.a((Object) r1, "binding.ftvTitle");
                    r1.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886897));
                    Object r12 = a2.u;
                    wg6.a((Object) r12, "binding.ftvDescription");
                    r12.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886896));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_HOUR);
                    AppCompatImageView appCompatImageView = a2.r;
                    wg6.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(0))).setImageCallback(new d(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void o() {
        FLogger.INSTANCE.getLocal().d(r, "finish");
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    @DexIgnore
    public void onClick(View view) {
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onClick");
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.b(view.getId() == 2131361855);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v1, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r4v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v5, types: [com.portfolio.platform.view.FlexibleImageButton, android.view.View] */
    /* JADX WARNING: type inference failed for: r5v6, types: [com.portfolio.platform.view.FlexibleImageButton, android.view.View] */
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        r64 a2 = kb.a(layoutInflater, 2131558508, viewGroup, false, e1());
        a2.q.setOnClickListener(new b(this));
        a2.v.setOnClickListener(new c(this));
        new hm4().a(a2.s, this);
        new hm4().a(a2.t, this);
        this.j = new ax5<>(this, a2);
        this.o = jj4.a((Fragment) this);
        gv5 gv5 = this.i;
        if (gv5 != null) {
            this.p = gv5.i();
            wg6.a((Object) a2, "binding");
            return a2.d();
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        CalibrationFragment.super.onResume();
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onStop() {
        CalibrationFragment.super.onStop();
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.g();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void q1() {
        o();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void r(String str) {
        wg6.b(str, "deviceId");
        if (isActive()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local.d(str2, "showMinute: deviceId = " + str);
            ax5<r64> ax5 = this.j;
            if (ax5 != null) {
                r64 a2 = ax5.a();
                if (a2 != null) {
                    Object r1 = a2.w;
                    wg6.a((Object) r1, "binding.ftvTitle");
                    r1.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886900));
                    Object r12 = a2.u;
                    wg6.a((Object) r12, "binding.ftvDescription");
                    r12.setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886899));
                    CloudImageHelper.ItemImage type = CloudImageHelper.Companion.getInstance().with().setSerialNumber(str).setSerialPrefix(DeviceHelper.o.b(str)).setType(Constants.CalibrationType.TYPE_MINUTE);
                    AppCompatImageView appCompatImageView = a2.r;
                    wg6.a((Object) appCompatImageView, "binding.acivDevice");
                    type.setPlaceHolder(appCompatImageView, DeviceHelper.o.b(str, this.p.get(1))).setImageCallback(new e(a2, this, str)).downloadForCalibration();
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void r1() {
        o();
    }

    @DexIgnore
    public void s1() {
        o();
    }

    @DexIgnore
    public void t1() {
    }

    @DexIgnore
    public void u1() {
    }

    @DexIgnore
    public void b(View view) {
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEnded");
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.k();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(gv5 gv5) {
        wg6.b(gv5, "presenter");
        this.i = gv5;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r5v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(int i2, int i3) {
        String str;
        if (isActive()) {
            ax5<r64> ax5 = this.j;
            if (ax5 != null) {
                r64 a2 = ax5.a();
                if (a2 != null) {
                    a2.x.setLength(i3);
                    int i4 = i2 + 1;
                    a2.x.setProgress((int) ((((float) i4) / ((float) i3)) * ((float) 100)));
                    if (i4 < i3) {
                        str = jm4.a((Context) PortfolioApp.get.instance(), 2131886895);
                    } else {
                        str = jm4.a((Context) PortfolioApp.get.instance(), 2131886898);
                    }
                    Object r6 = a2.v;
                    wg6.a((Object) r6, "it.ftvNext");
                    r6.setText(str);
                    return;
                }
                return;
            }
            wg6.d("mBinding");
            throw null;
        }
    }

    @DexIgnore
    public void a(View view) {
        wg6.b(view, "view");
        FLogger.INSTANCE.getLocal().d(r, "GestureDetectorCallback onLongPressEvent");
        gv5 gv5 = this.i;
        if (gv5 != null) {
            gv5.c(view.getId() == 2131361855);
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void a(String str, int i2, Intent intent) {
        wg6.b(str, "tag");
        super.a(str, i2, intent);
        int hashCode = str.hashCode();
        if (hashCode == -1391436191 ? !str.equals("SYNC_FAILED") : hashCode != 1178575340 || !str.equals("SERVER_ERROR")) {
            BaseActivity activity = getActivity();
            if (activity != null) {
                activity.a(str, i2, intent);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.ui.BaseActivity");
        }
        o();
    }
}
