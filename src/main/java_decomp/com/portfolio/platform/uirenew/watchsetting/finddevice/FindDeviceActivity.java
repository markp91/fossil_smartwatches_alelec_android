package com.portfolio.platform.uirenew.watchsetting.finddevice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.sv5;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.BaseActivity;
import com.portfolio.platform.uirenew.watchsetting.FindDeviceFragment;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FindDeviceActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public FindDevicePresenter B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context, String str) {
            wg6.b(context, "context");
            wg6.b(str, "serial");
            Intent intent = new Intent(context, FindDeviceActivity.class);
            intent.putExtra("SERIAL", str);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FindDeviceActivity", "start with " + str);
            intent.setFlags(536870912);
            context.startActivity(intent);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [com.portfolio.platform.uirenew.watchsetting.finddevice.FindDeviceActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        FindDeviceFragment b = getSupportFragmentManager().b(2131362119);
        if (b == null) {
            b = FindDeviceFragment.u.a();
            a((Fragment) b, "FindDeviceFragment", 2131362119);
        }
        PortfolioApp.get.instance().g().a(new sv5(b)).a(this);
        if (bundle != null && bundle.containsKey("SERIAL")) {
            String string = bundle.getString("SERIAL");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String f = f();
            local.d(f, "retrieve serial from savedInstanceState " + string);
            if (string != null) {
                FindDevicePresenter findDevicePresenter = this.B;
                if (findDevicePresenter != null) {
                    findDevicePresenter.b(string);
                } else {
                    wg6.d("mPresenter");
                    throw null;
                }
            }
        }
        if (getIntent() != null) {
            String stringExtra = getIntent().getStringExtra("SERIAL");
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String f2 = f();
            local2.d(f2, "retrieve serial from intent " + stringExtra);
            FindDevicePresenter findDevicePresenter2 = this.B;
            if (findDevicePresenter2 != null) {
                wg6.a((Object) stringExtra, "serial");
                findDevicePresenter2.b(stringExtra);
                return;
            }
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    public void onSaveInstanceState(Bundle bundle) {
        wg6.b(bundle, "outState");
        FindDevicePresenter findDevicePresenter = this.B;
        if (findDevicePresenter != null) {
            bundle.putString("SERIAL", findDevicePresenter.k());
            FindDeviceActivity.super.onSaveInstanceState(bundle);
            return;
        }
        wg6.d("mPresenter");
        throw null;
    }
}
