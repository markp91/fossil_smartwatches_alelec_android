package com.portfolio.platform.uirenew;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import com.fossil.ax5;
import com.fossil.gq5;
import com.fossil.hq5;
import com.fossil.jm4;
import com.fossil.kb;
import com.fossil.lx5;
import com.fossil.nk4;
import com.fossil.qg6;
import com.fossil.t94;
import com.fossil.wg6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.uirenew.signup.SignUpActivity;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ForgotPasswordFragment extends BaseFragment implements hq5 {
    @DexIgnore
    public static /* final */ String i;
    @DexIgnore
    public static /* final */ a j; // = new a((qg6) null);
    @DexIgnore
    public ax5<t94> f;
    @DexIgnore
    public gq5 g;
    @DexIgnore
    public HashMap h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return ForgotPasswordFragment.i;
        }

        @DexIgnore
        public final ForgotPasswordFragment b() {
            return new ForgotPasswordFragment();
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment a;

        @DexIgnore
        public b(ForgotPasswordFragment forgotPasswordFragment) {
            this.a = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t94 a;
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment b;

        @DexIgnore
        public c(t94 t94, ForgotPasswordFragment forgotPasswordFragment) {
            this.a = t94;
            this.b = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gq5 a2 = ForgotPasswordFragment.a(this.b);
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.s;
            wg6.a((Object) flexibleTextInputEditText, "binding.etEmail");
            a2.b(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ t94 a;
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment b;

        @DexIgnore
        public d(t94 t94, ForgotPasswordFragment forgotPasswordFragment) {
            this.a = t94;
            this.b = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            gq5 a2 = ForgotPasswordFragment.a(this.b);
            FlexibleTextInputEditText flexibleTextInputEditText = this.a.s;
            wg6.a((Object) flexibleTextInputEditText, "binding.etEmail");
            a2.b(String.valueOf(flexibleTextInputEditText.getText()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment a;

        @DexIgnore
        public e(ForgotPasswordFragment forgotPasswordFragment) {
            this.a = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment a;

        @DexIgnore
        public f(ForgotPasswordFragment forgotPasswordFragment) {
            this.a = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            SignUpActivity.a aVar = SignUpActivity.G;
            wg6.a((Object) view, "it");
            Context context = view.getContext();
            wg6.a((Object) context, "it.context");
            aVar.a(context);
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements View.OnClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment a;

        @DexIgnore
        public g(ForgotPasswordFragment forgotPasswordFragment) {
            this.a = forgotPasswordFragment;
        }

        @DexIgnore
        public final void onClick(View view) {
            FragmentActivity activity = this.a.getActivity();
            if (activity != null) {
                activity.finish();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h implements TextWatcher {
        @DexIgnore
        public /* final */ /* synthetic */ ForgotPasswordFragment a;

        @DexIgnore
        public h(ForgotPasswordFragment forgotPasswordFragment) {
            this.a = forgotPasswordFragment;
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            String str;
            gq5 a2 = ForgotPasswordFragment.a(this.a);
            if (charSequence == null || (str = charSequence.toString()) == null) {
                str = "";
            }
            a2.a(str);
        }
    }

    /*
    static {
        String simpleName = ForgotPasswordFragment.class.getSimpleName();
        if (simpleName != null) {
            wg6.a((Object) simpleName, "ForgotPasswordFragment::class.java.simpleName!!");
            i = simpleName;
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public static final /* synthetic */ gq5 a(ForgotPasswordFragment forgotPasswordFragment) {
        gq5 gq5 = forgotPasswordFragment.g;
        if (gq5 != null) {
            return gq5;
        }
        wg6.d("mPresenter");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void C0() {
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                ConstraintLayout constraintLayout = a2.r;
                wg6.a((Object) constraintLayout, "it.clResetPwSuccess");
                if (constraintLayout.getVisibility() == 0) {
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886738);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026ructionsResentRememberTo)");
                    Y(a3);
                    return;
                }
                ConstraintLayout constraintLayout2 = a2.q;
                wg6.a((Object) constraintLayout2, "it.clResetPw");
                constraintLayout2.setVisibility(8);
                ConstraintLayout constraintLayout3 = a2.r;
                wg6.a((Object) constraintLayout3, "it.clResetPwSuccess");
                constraintLayout3.setVisibility(0);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void J(boolean z) {
        Object r0;
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.w) != 0) {
                if (z) {
                    wg6.a((Object) r0, "it");
                    r0.setVisibility(0);
                    return;
                }
                wg6.a((Object) r0, "it");
                r0.setVisibility(8);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.view.View, java.lang.Object, android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public void Q(String str) {
        Object r0;
        wg6.b(str, "email");
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null && (r0 = a2.s) != 0) {
                r0.setText(str);
                r0.requestFocus();
                nk4 nk4 = nk4.a;
                wg6.a((Object) r0, "it");
                Context context = r0.getContext();
                wg6.a((Object) context, "it.context");
                nk4.b(r0, context);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void d1() {
        HashMap hashMap = this.h;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void g0() {
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.u;
                wg6.a((Object) r1, "it.fbSendLink");
                r1.setEnabled(true);
                Object r12 = a2.u;
                wg6.a((Object) r12, "it.fbSendLink");
                r12.setClickable(true);
                Object r13 = a2.u;
                wg6.a((Object) r13, "it.fbSendLink");
                r13.setFocusable(true);
                a2.u.a("flexible_button_primary");
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public String h1() {
        return i;
    }

    @DexIgnore
    public void i() {
        a();
    }

    @DexIgnore
    public boolean i1() {
        return false;
    }

    @DexIgnore
    public void k() {
        b();
    }

    @DexIgnore
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        wg6.b(layoutInflater, "inflater");
        ForgotPasswordFragment.super.onCreateView(layoutInflater, viewGroup, bundle);
        this.f = new ax5<>(this, kb.a(layoutInflater, 2131558550, viewGroup, false, e1()));
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                wg6.a((Object) a2, "mBinding.get()!!");
                return a2.d();
            }
            wg6.a();
            throw null;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public /* synthetic */ void onDestroyView() {
        super.onDestroyView();
        d1();
    }

    @DexIgnore
    public void onResume() {
        ForgotPasswordFragment.super.onResume();
        gq5 gq5 = this.g;
        if (gq5 != null) {
            gq5.f();
        } else {
            wg6.d("mPresenter");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r3v5, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r3v6, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public void onViewCreated(View view, Bundle bundle) {
        wg6.b(view, "view");
        super.onViewCreated(view, bundle);
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                a2.t.setOnClickListener(new b(this));
                a2.u.setOnClickListener(new c(a2, this));
                a2.v.setOnClickListener(new d(a2, this));
                a2.y.setOnClickListener(new e(this));
                a2.w.setOnClickListener(new f(this));
                a2.z.setOnClickListener(new g(this));
                a2.s.addTextChangedListener(new h(this));
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v1, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
    public void s0() {
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                Object r1 = a2.u;
                wg6.a((Object) r1, "it.fbSendLink");
                r1.setEnabled(false);
                Object r12 = a2.u;
                wg6.a((Object) r12, "it.fbSendLink");
                r12.setClickable(false);
                Object r13 = a2.u;
                wg6.a((Object) r13, "it.fbSendLink");
                r13.setFocusable(false);
                a2.u.a("flexible_button_disabled");
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }

    @DexIgnore
    public void a(gq5 gq5) {
        wg6.b(gq5, "presenter");
        this.g = gq5;
    }

    @DexIgnore
    public void a(int i2, String str) {
        wg6.b(str, "errorMessage");
        lx5 lx5 = lx5.c;
        FragmentManager childFragmentManager = getChildFragmentManager();
        wg6.a((Object) childFragmentManager, "childFragmentManager");
        lx5.a(i2, str, childFragmentManager);
    }

    @DexIgnore
    public void J(String str) {
        wg6.b(str, "message");
        ax5<t94> ax5 = this.f;
        if (ax5 != null) {
            t94 a2 = ax5.a();
            if (a2 != null) {
                FlexibleTextInputLayout flexibleTextInputLayout = a2.x;
                wg6.a((Object) flexibleTextInputLayout, "it.inputEmail");
                flexibleTextInputLayout.setErrorEnabled(false);
                FlexibleTextInputLayout flexibleTextInputLayout2 = a2.x;
                wg6.a((Object) flexibleTextInputLayout2, "it.inputEmail");
                flexibleTextInputLayout2.setError(str);
                return;
            }
            return;
        }
        wg6.d("mBinding");
        throw null;
    }
}
