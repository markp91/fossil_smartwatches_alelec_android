package com.portfolio.platform.uirenew.troubleshooting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.tu5;
import com.fossil.vu5;
import com.fossil.wg6;
import com.fossil.y04;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TroubleshootingActivity extends BaseActivity {
    @DexIgnore
    public static /* final */ a C; // = new a((qg6) null);
    @DexIgnore
    public vu5 B;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a(Context context) {
            wg6.b(context, "context");
            context.startActivity(new Intent(context, TroubleshootingActivity.class));
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public static /* synthetic */ void a(a aVar, Context context, String str, boolean z, int i, Object obj) {
            if ((i & 4) != 0) {
                z = false;
            }
            aVar.a(context, str, z);
        }

        @DexIgnore
        public final void a(Context context, String str, boolean z) {
            wg6.b(context, "context");
            wg6.b(str, "serial");
            Intent intent = new Intent(context, TroubleshootingActivity.class);
            intent.putExtra("DEVICE_TYPE", DeviceHelper.o.f(str));
            intent.putExtra("IS_PAIRING_FLOW", z);
            context.startActivity(intent);
        }
    }

    @DexIgnore
    public final boolean a(Intent intent) {
        if (intent.getExtras() == null) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras != null) {
            return extras.getBoolean("DEVICE_TYPE", false);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.uirenew.troubleshooting.TroubleshootingActivity, com.portfolio.platform.ui.BaseActivity, android.app.Activity, androidx.fragment.app.FragmentActivity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(2131558439);
        TroubleshootingFragment b = getSupportFragmentManager().b(2131362119);
        Intent intent = getIntent();
        wg6.a((Object) intent, "intent");
        boolean a2 = a(intent);
        Intent intent2 = getIntent();
        wg6.a((Object) intent2, "intent");
        Bundle extras = intent2.getExtras();
        boolean z = false;
        if (extras != null) {
            z = extras.getBoolean("IS_PAIRING_FLOW", false);
        }
        if (b == null) {
            b = TroubleshootingFragment.o.a(a2, z);
            a((Fragment) b, TroubleshootingFragment.o.a(), 2131362119);
        }
        y04 g = PortfolioApp.get.instance().g();
        if (b != null) {
            g.a(new tu5(b)).a(this);
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.troubleshooting.TroubleshootingContract.View");
    }
}
