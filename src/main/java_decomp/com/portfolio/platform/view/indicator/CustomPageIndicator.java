package com.portfolio.platform.view.indicator;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import com.fossil.bl;
import com.fossil.qg6;
import com.fossil.qz5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xk4;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomPageIndicator extends View implements qz5 {
    @DexIgnore
    public float a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ Paint c;
    @DexIgnore
    public /* final */ Paint d;
    @DexIgnore
    public RecyclerView e;
    @DexIgnore
    public ViewPager f;
    @DexIgnore
    public ViewPager.i g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public float r;
    @DexIgnore
    public int s;
    @DexIgnore
    public float t;
    @DexIgnore
    public int u;
    @DexIgnore
    public boolean v;
    @DexIgnore
    public List<a> w;
    @DexIgnore
    public Rect x;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ int b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.indicator.CustomPageIndicator$a$a")
        /* renamed from: com.portfolio.platform.view.indicator.CustomPageIndicator$a$a  reason: collision with other inner class name */
        public static final class C0069a {
            @DexIgnore
            public C0069a() {
            }

            @DexIgnore
            public /* synthetic */ C0069a(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            new C0069a((qg6) null);
        }
        */

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends View.BaseSavedState {
        @DexIgnore
        public static /* final */ a CREATOR; // = new a((qg6) null);
        @DexIgnore
        public int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements Parcelable.Creator<b> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
                this();
            }

            @DexIgnore
            public b createFromParcel(Parcel parcel) {
                wg6.b(parcel, "parcel");
                return new b(parcel);
            }

            @DexIgnore
            public b[] newArray(int i) {
                return new b[i];
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcelable parcelable) {
            super(parcelable);
            wg6.b(parcelable, "superState");
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public void writeToParcel(Parcel parcel, int i) {
            wg6.b(parcel, "dest");
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.a);
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Parcel parcel) {
            super(parcel);
            wg6.b(parcel, "in");
            this.a = parcel.readInt();
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context) {
        super(context);
        wg6.b(context, "context");
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.t = -1.0f;
        this.u = -1;
        this.w = new ArrayList();
        this.x = new Rect();
    }

    @DexIgnore
    public void a(ViewPager viewPager, int i2) {
        setViewPager(viewPager);
        setCurrentItem(i2);
    }

    @DexIgnore
    public void b(int i2) {
        int i3;
        if (this.q || this.j == 0) {
            if (xk4.a(getContext())) {
                ViewPager viewPager = this.f;
                bl adapter = viewPager != null ? viewPager.getAdapter() : null;
                if (adapter != null) {
                    wg6.a((Object) adapter, "mViewPager?.adapter!!");
                    i3 = (adapter.a() - i2) - 1;
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                i3 = i2;
            }
            this.h = i3;
            this.i = i2;
            invalidate();
        }
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.b(i2);
        }
    }

    @DexIgnore
    public final int c(int i2) {
        for (a next : this.w) {
            if (next.b() == i2) {
                return next.a();
            }
        }
        return 0;
    }

    @DexIgnore
    public final int d(int i2) {
        ViewPager viewPager;
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824 || (viewPager = this.f) == null) {
            return size;
        }
        if (viewPager != null) {
            bl adapter = viewPager.getAdapter();
            if (adapter != null) {
                wg6.a((Object) adapter, "mViewPager!!.adapter!!");
                int a2 = adapter.a();
                float f2 = this.a;
                int paddingLeft = (int) (((float) getPaddingLeft()) + ((float) getPaddingRight()) + (((float) a2) * 2.0f * f2) + (((float) (a2 - 1)) * f2) + 1.0f);
                return mode == Integer.MIN_VALUE ? Math.min(paddingLeft, size) : paddingLeft;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final int e(int i2) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        if (mode == 1073741824) {
            return size;
        }
        int paddingTop = (int) ((((float) 2) * this.a) + ((float) getPaddingTop()) + ((float) getPaddingBottom()) + 1.0f);
        return mode == Integer.MIN_VALUE ? Math.min(paddingTop, size) : paddingTop;
    }

    @DexIgnore
    public final int getMCurrentPage$app_fossilRelease() {
        return this.h;
    }

    @DexIgnore
    public final ViewPager.i getMListener$app_fossilRelease() {
        return this.g;
    }

    @DexIgnore
    public final RecyclerView getMRecyclerView$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0069 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x006a  */
    public void onDraw(Canvas canvas) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        float f2;
        int i7;
        int i8;
        Canvas canvas2 = canvas;
        wg6.b(canvas2, "canvas");
        super.onDraw(canvas);
        ViewPager viewPager = this.f;
        Throwable th = null;
        if (viewPager != null) {
            if (viewPager == null) {
                wg6.a();
                throw null;
            } else if (viewPager.getAdapter() != null) {
                ViewPager viewPager2 = this.f;
                if (viewPager2 != null) {
                    bl adapter = viewPager2.getAdapter();
                    if (adapter != null) {
                        wg6.a((Object) adapter, "mViewPager!!.adapter!!");
                        i2 = adapter.a();
                        if (i2 != 0) {
                            if (this.h >= i2) {
                                setCurrentItem(i2 - 1);
                                return;
                            }
                            if (this.o == 0) {
                                i6 = getWidth();
                                i5 = getPaddingLeft();
                                i4 = getPaddingRight();
                                i3 = getPaddingTop();
                            } else {
                                i6 = getHeight();
                                i5 = getPaddingTop();
                                i4 = getPaddingBottom();
                                i3 = getPaddingLeft();
                            }
                            float f3 = this.a;
                            float f4 = (((float) 2) * f3) + this.r;
                            float f5 = ((float) i3) + f3;
                            float f6 = ((float) i5) + f3;
                            if (this.p) {
                                f6 += (((float) ((i6 - i5) - i4)) / 2.0f) - ((((float) i2) * f4) / 2.0f);
                            }
                            float f7 = this.a;
                            if (this.c.getStrokeWidth() > ((float) 0)) {
                                f7 -= this.c.getStrokeWidth() / 2.0f;
                            }
                            int i9 = 0;
                            while (i9 < i2) {
                                float f8 = (((float) i9) * f4) + f6;
                                if (this.o == 0) {
                                    f2 = f5;
                                } else {
                                    f2 = f8;
                                    f8 = f5;
                                }
                                int i10 = this.q ? this.i : this.h;
                                RecyclerView recyclerView = this.e;
                                if (recyclerView == null) {
                                    i7 = 0;
                                } else if (recyclerView != null) {
                                    RecyclerView.g adapter2 = recyclerView.getAdapter();
                                    if (adapter2 != null) {
                                        i7 = c(adapter2.getItemViewType(i9));
                                    } else {
                                        wg6.a();
                                        throw th;
                                    }
                                } else {
                                    wg6.a();
                                    throw th;
                                }
                                if (i7 != 0) {
                                    Drawable c2 = w6.c(getContext(), i7);
                                    Rect rect = this.x;
                                    float f9 = this.a;
                                    i8 = i2;
                                    rect.set((int) (f8 - f9), (int) (f2 - f9), ((int) (f8 - f9)) + (((int) f9) * 2), ((int) (f2 - f9)) + (((int) f9) * 2));
                                    if (c2 != null) {
                                        c2.setBounds(this.x);
                                    }
                                    if (i9 != i10) {
                                        if (c2 != null) {
                                            c2.setColorFilter(this.b.getColor(), PorterDuff.Mode.SRC_IN);
                                        }
                                    } else if (c2 != null) {
                                        c2.setColorFilter(this.d.getColor(), PorterDuff.Mode.SRC_IN);
                                    }
                                    if (c2 != null) {
                                        c2.draw(canvas2);
                                    }
                                } else {
                                    i8 = i2;
                                    if (i9 == i10) {
                                        canvas2.drawCircle(f8, f2, this.a, this.d);
                                    } else if (this.b.getAlpha() > 0) {
                                        canvas2.drawCircle(f8, f2, f7, this.b);
                                    }
                                    float f10 = this.a;
                                    if (f7 != f10) {
                                        canvas2.drawCircle(f8, f2, f10, this.c);
                                    }
                                }
                                i9++;
                                i2 = i8;
                                th = null;
                            }
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
        }
        RecyclerView recyclerView2 = this.e;
        if (recyclerView2 != null) {
            if (recyclerView2 == null) {
                wg6.a();
                throw null;
            } else if (recyclerView2.getAdapter() != null) {
                RecyclerView recyclerView3 = this.e;
                if (recyclerView3 != null) {
                    RecyclerView.g adapter3 = recyclerView3.getAdapter();
                    if (adapter3 != null) {
                        wg6.a((Object) adapter3, "mRecyclerView!!.adapter!!");
                        i2 = adapter3.getItemCount();
                        if (i2 != 0) {
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        i2 = 0;
        if (i2 != 0) {
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        if (this.o == 0) {
            setMeasuredDimension(d(i2), e(i3));
        } else {
            setMeasuredDimension(e(i2), d(i3));
        }
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        wg6.b(parcelable, Constants.STATE);
        b bVar = (b) parcelable;
        super.onRestoreInstanceState(bVar.getSuperState());
        this.h = bVar.a();
        this.i = bVar.a();
        requestLayout();
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        if (onSaveInstanceState == null) {
            return null;
        }
        b bVar = new b(onSaveInstanceState);
        bVar.a(this.h);
        return bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0082, code lost:
        if (r11.a() != false) goto L_0x0089;
     */
    @DexIgnore
    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        wg6.b(motionEvent, "ev");
        if (super.onTouchEvent(motionEvent)) {
            return true;
        }
        ViewPager viewPager = this.f;
        int i2 = 0;
        if (viewPager != null) {
            if (viewPager == null) {
                wg6.a();
                throw null;
            } else if (viewPager.getAdapter() != null) {
                ViewPager viewPager2 = this.f;
                if (viewPager2 != null) {
                    bl adapter = viewPager2.getAdapter();
                    if (adapter != null) {
                        wg6.a((Object) adapter, "mViewPager!!.adapter!!");
                        if (adapter.a() != 0) {
                            int action = motionEvent.getAction() & 255;
                            if (action == 0) {
                                this.u = motionEvent.getPointerId(0);
                                this.t = motionEvent.getX();
                            } else if (action == 2) {
                                float x2 = motionEvent.getX(motionEvent.findPointerIndex(this.u));
                                float f2 = x2 - this.t;
                                if (!this.v && Math.abs(f2) > ((float) this.s)) {
                                    this.v = true;
                                }
                                if (this.v) {
                                    this.t = x2;
                                    ViewPager viewPager3 = this.f;
                                    if (viewPager3 != null) {
                                        if (!viewPager3.g()) {
                                            ViewPager viewPager4 = this.f;
                                            if (viewPager4 == null) {
                                                wg6.a();
                                                throw null;
                                            }
                                        }
                                        ViewPager viewPager5 = this.f;
                                        if (viewPager5 != null) {
                                            viewPager5.b(f2);
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else if (action == 3 || action == 1) {
                                if (!this.v) {
                                    ViewPager viewPager6 = this.f;
                                    if (viewPager6 != null) {
                                        bl adapter2 = viewPager6.getAdapter();
                                        if (adapter2 != null) {
                                            wg6.a((Object) adapter2, "mViewPager!!.adapter!!");
                                            int a2 = adapter2.a();
                                            float width = (float) getWidth();
                                            float f3 = width / 2.0f;
                                            float f4 = width / 6.0f;
                                            if (this.h > 0 && motionEvent.getX() < f3 - f4) {
                                                if (action != 3) {
                                                    ViewPager viewPager7 = this.f;
                                                    if (viewPager7 != null) {
                                                        viewPager7.setCurrentItem(this.h - 1);
                                                    } else {
                                                        wg6.a();
                                                        throw null;
                                                    }
                                                }
                                                return true;
                                            } else if (this.h < a2 - 1 && motionEvent.getX() > f3 + f4) {
                                                if (action != 3) {
                                                    ViewPager viewPager8 = this.f;
                                                    if (viewPager8 != null) {
                                                        viewPager8.setCurrentItem(this.h + 1);
                                                    } else {
                                                        wg6.a();
                                                        throw null;
                                                    }
                                                }
                                                return true;
                                            }
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                                this.v = false;
                                this.u = -1;
                                ViewPager viewPager9 = this.f;
                                if (viewPager9 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (viewPager9.g()) {
                                    ViewPager viewPager10 = this.f;
                                    if (viewPager10 != null) {
                                        viewPager10.d();
                                    } else {
                                        wg6.a();
                                        throw null;
                                    }
                                }
                            } else if (action == 5) {
                                int actionIndex = motionEvent.getActionIndex();
                                this.t = motionEvent.getX(actionIndex);
                                this.u = motionEvent.getPointerId(actionIndex);
                            } else if (action == 6) {
                                int actionIndex2 = motionEvent.getActionIndex();
                                if (motionEvent.getPointerId(actionIndex2) == this.u) {
                                    if (actionIndex2 == 0) {
                                        i2 = 1;
                                    }
                                    this.u = motionEvent.getPointerId(i2);
                                }
                                this.t = motionEvent.getX(motionEvent.findPointerIndex(this.u));
                            }
                            return true;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        return false;
    }

    @DexIgnore
    public void setCurrentItem(int i2) {
        if (this.f == null && this.e == null) {
            throw new IllegalStateException("ViewPager has not been bound.");
        }
        ViewPager viewPager = this.f;
        if (viewPager == null) {
            if (xk4.a(getContext())) {
                RecyclerView recyclerView = this.e;
                if (recyclerView != null) {
                    RecyclerView.g adapter = recyclerView.getAdapter();
                    if (adapter != null) {
                        wg6.a((Object) adapter, "mRecyclerView!!.adapter!!");
                        i2 = (adapter.getItemCount() - i2) - 1;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.h = i2;
        } else if (viewPager != null) {
            viewPager.setCurrentItem(i2);
            if (xk4.a(getContext())) {
                ViewPager viewPager2 = this.f;
                if (viewPager2 != null) {
                    bl adapter2 = viewPager2.getAdapter();
                    if (adapter2 != null) {
                        wg6.a((Object) adapter2, "mViewPager!!.adapter!!");
                        i2 = (adapter2.a() - i2) - 1;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.h = i2;
        } else {
            wg6.a();
            throw null;
        }
        invalidate();
    }

    @DexIgnore
    public final void setMCurrentPage$app_fossilRelease(int i2) {
        this.h = i2;
    }

    @DexIgnore
    public final void setMListener$app_fossilRelease(ViewPager.i iVar) {
        this.g = iVar;
    }

    @DexIgnore
    public final void setMRecyclerView$app_fossilRelease(RecyclerView recyclerView) {
        this.e = recyclerView;
    }

    @DexIgnore
    public void setOnPageChangeListener(ViewPager.i iVar) {
    }

    @DexIgnore
    public void setViewPager(ViewPager viewPager) {
        if (!wg6.a((Object) this.f, (Object) viewPager)) {
            ViewPager viewPager2 = this.f;
            if (viewPager2 != null) {
                viewPager2.b(this);
            }
            if ((viewPager != null ? viewPager.getAdapter() : null) != null) {
                this.f = viewPager;
                ViewPager viewPager3 = this.f;
                if (viewPager3 != null) {
                    viewPager3.a(this);
                }
                invalidate();
                return;
            }
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }
    }

    @DexIgnore
    public void a(int i2) {
        this.j = i2;
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.a(i2);
        }
    }

    @DexIgnore
    public void a(int i2, float f2, int i3) {
        this.h = i2;
        invalidate();
        ViewPager.i iVar = this.g;
        if (iVar != null) {
            iVar.a(i2, f2, i3);
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 2130970096);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CustomPageIndicator(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.b = new Paint(1);
        this.c = new Paint(1);
        this.d = new Paint(1);
        this.t = -1.0f;
        this.u = -1;
        this.w = new ArrayList();
        this.x = new Rect();
        if (!isInEditMode()) {
            Resources resources = getResources();
            int a2 = w6.a(context, 2131099857);
            int a3 = w6.a(context, 2131099834);
            int a4 = w6.a(context, R.color.transparent);
            float dimension = resources.getDimension(2131165424);
            float dimension2 = resources.getDimension(2131165438);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.CirclePageIndicator, i2, 0);
            TypedArray obtainStyledAttributes2 = context.obtainStyledAttributes(attributeSet, x24.LinePageIndicator, i2, 0);
            this.p = obtainStyledAttributes.getBoolean(3, true);
            this.o = obtainStyledAttributes.getInt(0, 0);
            this.b.setStyle(Paint.Style.FILL);
            this.b.setColor(obtainStyledAttributes.getColor(4, a2));
            this.c.setStyle(Paint.Style.STROKE);
            this.c.setColor(obtainStyledAttributes.getColor(7, a4));
            this.c.setStrokeWidth(obtainStyledAttributes.getDimension(8, 0.0f));
            this.d.setStyle(Paint.Style.FILL);
            this.d.setColor(obtainStyledAttributes.getColor(2, a3));
            String b2 = ThemeManager.l.a().b("primaryColor");
            if (!TextUtils.isEmpty(b2)) {
                this.d.setColor(Color.parseColor(b2));
            }
            this.a = obtainStyledAttributes.getDimension(5, dimension);
            this.q = obtainStyledAttributes.getBoolean(6, false);
            this.r = obtainStyledAttributes2.getDimension(1, dimension2);
            Drawable drawable = obtainStyledAttributes.getDrawable(1);
            if (drawable != null) {
                setBackground(drawable);
            }
            obtainStyledAttributes.recycle();
            obtainStyledAttributes2.recycle();
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            wg6.a((Object) viewConfiguration, "configuration");
            this.s = viewConfiguration.getScaledPagingTouchSlop();
        }
    }
}
