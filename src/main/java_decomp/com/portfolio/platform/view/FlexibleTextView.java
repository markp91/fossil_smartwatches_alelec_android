package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.fossil.cy5;
import com.fossil.jm4;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.uy5;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FlexibleTextView extends AppCompatTextView {
    @DexIgnore
    public static /* final */ ArrayList<String> q; // = qd6.a((T[]) new String[]{"nonBrandSurface", "primaryColor"});
    @DexIgnore
    public static /* final */ ArrayList<String> r; // = qd6.a((T[]) new String[]{"primaryText", "nonBrandSwitchDisabledGray"});
    @DexIgnore
    public static /* final */ int s; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public static /* final */ a t; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String e; // = "FlexibleTextView";
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public String g; // = "";
    @DexIgnore
    public String h; // = "";
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p; // = s;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final int a() {
            return FlexibleTextView.s;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [androidx.appcompat.widget.AppCompatTextView, com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    @SuppressLint({"ResourceType"})
    public final void a(AttributeSet attributeSet) {
        this.j = 0;
        CharSequence text = getText();
        CharSequence hint = getHint();
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleTextView);
            this.j = obtainStyledAttributes.getInt(5, 0);
            this.o = obtainStyledAttributes.getInt(2, 0);
            this.p = obtainStyledAttributes.getColor(6, s);
            String string = obtainStyledAttributes.getString(3);
            if (string == null) {
                string = "";
            }
            this.f = string;
            String string2 = obtainStyledAttributes.getString(4);
            if (string2 == null) {
                string2 = "";
            }
            this.g = string2;
            String string3 = obtainStyledAttributes.getString(0);
            if (string3 == null) {
                string3 = "";
            }
            this.h = string3;
            this.i = obtainStyledAttributes.getInt(1, 0);
            obtainStyledAttributes.recycle();
            TypedArray obtainStyledAttributes2 = getContext().obtainStyledAttributes(attributeSet, new int[]{16843087, 16843088}, 0, 0);
            int resourceId = obtainStyledAttributes2.getResourceId(0, -1);
            if (resourceId != -1) {
                text = a(resourceId);
            }
            int resourceId2 = obtainStyledAttributes2.getResourceId(1, -1);
            if (resourceId2 != -1) {
                hint = a(resourceId2);
            }
            obtainStyledAttributes2.recycle();
        }
        if (!TextUtils.isEmpty(text)) {
            setText(text);
        }
        if (!TextUtils.isEmpty(hint)) {
            wg6.a((Object) hint, "hint");
            setHint(a(hint, this.o));
        }
        int i2 = this.p;
        if (i2 != s) {
            uy5.a((TextView) this, i2);
        }
        if (!TextUtils.isEmpty(this.f) || !TextUtils.isEmpty(this.g) || !TextUtils.isEmpty(this.h)) {
            a(this.f, this.g, this.h, this.i);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        wg6.b(bufferType, "type");
        if (charSequence == null) {
            charSequence = "";
        }
        FlexibleTextView.super.setText(a(charSequence), bufferType);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, Constants.STATE);
        int hashCode = str.hashCode();
        if (hashCode != -227352252) {
            if (hashCode == 636108957 && str.equals("flexible_tv_unselected")) {
                String str2 = r.get(0);
                wg6.a((Object) str2, "UNSELECTED_TV_STYLES[0]");
                String str3 = this.g;
                String str4 = r.get(1);
                wg6.a((Object) str4, "UNSELECTED_TV_STYLES[1]");
                a(str2, str3, str4, this.i);
            }
        } else if (str.equals("flexible_tv_selected")) {
            String str5 = q.get(0);
            wg6.a((Object) str5, "SELECTED_TV_STYLES[0]");
            String str6 = this.g;
            String str7 = q.get(1);
            wg6.a((Object) str7, "SELECTED_TV_STYLES[1]");
            a(str5, str6, str7, this.i);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    public final void a(String str, String str2, String str3, int i2) {
        GradientDrawable gradientDrawable;
        FLogger.INSTANCE.getLocal().d(this.e, "setStyle colorNameStyle=" + str + " fontNameStyle=" + str2);
        String b = ThemeManager.l.a().b(str);
        Typeface c = ThemeManager.l.a().c(str2);
        String b2 = ThemeManager.l.a().b(str3);
        if (b != null) {
            setTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
        if (b2 != null) {
            LayerDrawable layerDrawable = null;
            if (i2 == 1) {
                Object c2 = w6.c(getContext(), 2131230864);
                if (c2 != null) {
                    layerDrawable = (LayerDrawable) c2;
                    gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(2131362942);
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            } else if (i2 != 2) {
                gradientDrawable = null;
            } else {
                Object c3 = w6.c(getContext(), 2131230882);
                if (c3 != null) {
                    layerDrawable = (LayerDrawable) c3;
                    gradientDrawable = (GradientDrawable) layerDrawable.findDrawableByLayerId(2131362942);
                } else {
                    throw new rc6("null cannot be cast to non-null type android.graphics.drawable.LayerDrawable");
                }
            }
            if (gradientDrawable != null && layerDrawable != null) {
                gradientDrawable.setColor(Color.parseColor(b2));
                gradientDrawable.setStroke(1, Color.parseColor(b2));
                setBackground(layerDrawable);
            }
        }
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence) {
        return a(charSequence, this.j);
    }

    @DexIgnore
    public final CharSequence a(CharSequence charSequence, int i2) {
        if (i2 == 1) {
            return cy5.a(charSequence);
        }
        if (i2 == 2) {
            return cy5.b(charSequence);
        }
        if (i2 == 3) {
            return cy5.d(charSequence);
        }
        if (i2 == 4) {
            return cy5.e(charSequence);
        }
        if (i2 != 5) {
            return charSequence;
        }
        return cy5.c(charSequence);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        String a2 = jm4.a((Context) PortfolioApp.get.instance(), i2);
        wg6.a((Object) a2, "LanguageHelper.getString\u2026ioApp.instance, stringId)");
        return a2;
    }
}
