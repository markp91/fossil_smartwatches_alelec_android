package com.portfolio.platform.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class EditTextCustom extends FlexibleEditText {
    @DexIgnore
    public int A;
    @DexIgnore
    public a B;
    @DexIgnore
    public Drawable u;
    @DexIgnore
    public Drawable v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public b y;
    @DexIgnore
    public int z;

    @DexIgnore
    public interface a {

        @DexIgnore
        /* renamed from: com.portfolio.platform.view.EditTextCustom$a$a  reason: collision with other inner class name */
        public enum C0066a {
            TOP,
            BOTTOM,
            LEFT,
            RIGHT
        }

        @DexIgnore
        void a(C0066a aVar);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public EditTextCustom(Context context) {
        super(context);
    }

    @DexIgnore
    public void finalize() throws Throwable {
        this.u = null;
        this.x = null;
        this.v = null;
        this.w = null;
        EditTextCustom.super.finalize();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public void onDraw(Canvas canvas) {
        EditTextCustom.super.onDraw(canvas);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public boolean onKeyPreIme(int i, KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1) {
            b bVar = this.y;
            if (bVar != null) {
                bVar.a();
            }
            clearFocus();
        }
        return true;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        EditTextCustom.super.onSizeChanged(i, i2, i3, i4);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public boolean onTouchEvent(MotionEvent motionEvent) {
        a aVar;
        a aVar2;
        if (motionEvent.getAction() == 0) {
            this.z = (int) motionEvent.getX();
            this.A = (int) motionEvent.getY();
            Drawable drawable = this.x;
            if (drawable == null || !drawable.getBounds().contains(this.z, this.A)) {
                Drawable drawable2 = this.w;
                if (drawable2 == null || !drawable2.getBounds().contains(this.z, this.A)) {
                    Drawable drawable3 = this.v;
                    if (drawable3 != null) {
                        Rect bounds = drawable3.getBounds();
                        int i = (int) (((double) (getResources().getDisplayMetrics().density * 13.0f)) + 0.5d);
                        int i2 = this.z;
                        int i3 = this.A;
                        if (!bounds.contains(i2, i3)) {
                            i2 = this.z;
                            int i4 = i2 - i;
                            int i5 = this.A - i;
                            if (i4 > 0) {
                                i2 = i4;
                            }
                            i3 = i5 <= 0 ? this.A : i5;
                        }
                        if (bounds.contains(i2, i3) && (aVar2 = this.B) != null) {
                            aVar2.a(a.C0066a.LEFT);
                            motionEvent.setAction(3);
                            return false;
                        }
                    }
                    Drawable drawable4 = this.u;
                    if (drawable4 != null) {
                        Rect bounds2 = drawable4.getBounds();
                        int i6 = this.A - 13;
                        int width = getWidth() - (this.z + 13);
                        if (width <= 0) {
                            width += 13;
                        }
                        if (i6 <= 0) {
                            i6 = this.A;
                        }
                        if (!bounds2.contains(width, i6) || (aVar = this.B) == null) {
                            return EditTextCustom.super.onTouchEvent(motionEvent);
                        }
                        aVar.a(a.C0066a.RIGHT);
                        motionEvent.setAction(3);
                        return false;
                    }
                } else {
                    this.B.a(a.C0066a.TOP);
                    return EditTextCustom.super.onTouchEvent(motionEvent);
                }
            } else {
                this.B.a(a.C0066a.BOTTOM);
                return EditTextCustom.super.onTouchEvent(motionEvent);
            }
        }
        return EditTextCustom.super.onTouchEvent(motionEvent);
    }

    @DexIgnore
    public void setBackPressListener(b bVar) {
        this.y = bVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            this.v = drawable;
        }
        if (drawable3 != null) {
            this.u = drawable3;
        }
        if (drawable2 != null) {
            this.w = drawable2;
        }
        if (drawable4 != null) {
            this.x = drawable4;
        }
        EditTextCustom.super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
    }

    @DexIgnore
    public void setDrawableClickListener(a aVar) {
        this.B = aVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.EditTextCustom, android.widget.EditText] */
    public void setTypeface(Typeface typeface) {
        EditTextCustom.super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Fossil_Scout-Regular_10.otf"));
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @DexIgnore
    public EditTextCustom(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }
}
