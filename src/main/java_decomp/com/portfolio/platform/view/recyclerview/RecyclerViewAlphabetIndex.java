package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.gy5;
import com.fossil.mj6;
import com.fossil.nd6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewAlphabetIndex extends RecyclerView {
    @DexIgnore
    public String[] a; // = {"#", "A", "B", "C", "D", "E", DeviceIdentityUtils.FLASH_SERIAL_NUMBER_PREFIX, "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX, "T", "U", "V", "W", "X", "Y", "Z"};
    @DexIgnore
    public float b;
    @DexIgnore
    public int c;
    @DexIgnore
    public boolean d;
    @DexIgnore
    public Typeface e;
    @DexIgnore
    public a f;
    @DexIgnore
    public LinearLayoutManager g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.g<C0071a> {
        @DexIgnore
        public /* final */ Typeface a;
        @DexIgnore
        public /* final */ Typeface b;
        @DexIgnore
        public String[] c;
        @DexIgnore
        public int d;
        @DexIgnore
        public b e;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a")
        /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a  reason: collision with other inner class name */
        public final class C0071a extends RecyclerView.ViewHolder {
            @DexIgnore
            public /* final */ FlexibleTextView a;
            @DexIgnore
            public /* final */ /* synthetic */ a b;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a")
            /* renamed from: com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex$a$a$a  reason: collision with other inner class name */
            public static final class C0072a implements View.OnClickListener {
                @DexIgnore
                public /* final */ /* synthetic */ C0071a a;

                @DexIgnore
                public C0072a(C0071a aVar) {
                    this.a = aVar;
                }

                @DexIgnore
                public final void onClick(View view) {
                    int adapterPosition = this.a.getAdapterPosition();
                    if (adapterPosition != -1) {
                        String str = "" + this.a.a().getText().toString();
                        if (RecyclerViewAlphabetIndex.this.c()) {
                            RecyclerViewAlphabetIndex.this.setLetterToBold(str);
                        }
                        b c = this.a.b.c();
                        if (c != null) {
                            wg6.a((Object) view, "it");
                            c.a(view, adapterPosition, str);
                        }
                    }
                }
            }

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0071a(a aVar, View view) {
                super(view);
                wg6.b(view, "itemView");
                this.b = aVar;
                View findViewById = view.findViewById(2131363063);
                if (findViewById != null) {
                    this.a = (FlexibleTextView) findViewById;
                    view.setOnClickListener(new C0072a(this));
                    return;
                }
                wg6.a();
                throw null;
            }

            @DexIgnore
            public final FlexibleTextView a() {
                return this.a;
            }
        }

        @DexIgnore
        public a() {
            Typeface defaultFromStyle = Typeface.defaultFromStyle(0);
            if (defaultFromStyle != null) {
                this.a = defaultFromStyle;
                this.b = Typeface.defaultFromStyle(1);
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* renamed from: a */
        public void onBindViewHolder(C0071a aVar, int i) {
            wg6.b(aVar, "holder");
            String[] strArr = this.c;
            if (strArr != null) {
                aVar.a().setText(strArr[i]);
                if (RecyclerViewAlphabetIndex.this.c()) {
                    aVar.a().setTypeface(i == this.d ? this.b : this.a);
                    aVar.a().setTextSize(2, RecyclerViewAlphabetIndex.this.getMFontSize$app_fossilRelease());
                    if (RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease() != 0) {
                        aVar.a().setTextColor(RecyclerViewAlphabetIndex.this.getMItemColor$app_fossilRelease());
                    }
                    if (RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease() != null) {
                        aVar.a().setTypeface(RecyclerViewAlphabetIndex.this.getTypeFaceBaseTheme$app_fossilRelease());
                        return;
                    }
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final b c() {
            return this.e;
        }

        @DexIgnore
        public int getItemCount() {
            String[] strArr = this.c;
            if (strArr != null) {
                return strArr.length;
            }
            return 0;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v2, types: [com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex, android.view.ViewGroup] */
        public C0071a onCreateViewHolder(ViewGroup viewGroup, int i) {
            wg6.b(viewGroup, "parent");
            View inflate = LayoutInflater.from(RecyclerViewAlphabetIndex.this.getContext()).inflate(2131558674, viewGroup, false);
            wg6.a((Object) inflate, "view");
            return new C0071a(this, inflate);
        }

        @DexIgnore
        public final void a(b bVar) {
            wg6.b(bVar, "sectionIndexClickListener");
            this.e = bVar;
        }

        @DexIgnore
        public final void a(int i) {
            this.d = i;
            notifyDataSetChanged();
        }

        @DexIgnore
        public final void a(String[] strArr) {
            wg6.b(strArr, "alphabet");
            this.c = strArr;
            notifyDataSetChanged();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view, int i, String str);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex, android.view.ViewGroup] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context) {
        super(context);
        wg6.b(context, "context");
        setOverScrollMode(2);
        b();
    }

    @DexIgnore
    private final void setAlphabet(String[] strArr) {
        Arrays.sort(strArr);
        a aVar = this.f;
        if (aVar != null) {
            aVar.a(strArr);
        }
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.RecyclerViewAlphabetIndex);
        obtainStyledAttributes.getDimensionPixelSize(4, 15);
        this.b = obtainStyledAttributes.getDimension(1, (float) gy5.b(3.0f));
        this.c = obtainStyledAttributes.getColor(2, w6.a(context, 2131100008));
        this.d = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        String b2 = ThemeManager.l.a().b("primaryText");
        this.e = ThemeManager.l.a().c("descriptionText2");
        if (!TextUtils.isEmpty(b2)) {
            this.c = Color.parseColor(b2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [androidx.recyclerview.widget.RecyclerView, com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex, android.view.ViewGroup] */
    public final void b() {
        this.f = new a();
        this.g = new LinearLayoutManager(getContext(), 1, false);
        setHasFixedSize(true);
        setAdapter(this.f);
        setLayoutManager(this.g);
    }

    @DexIgnore
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    public final float getMFontSize$app_fossilRelease() {
        return this.b;
    }

    @DexIgnore
    public final int getMItemColor$app_fossilRelease() {
        return this.c;
    }

    @DexIgnore
    public final Typeface getTypeFaceBaseTheme$app_fossilRelease() {
        return this.e;
    }

    @DexIgnore
    public final void setCustomizable$app_fossilRelease(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final void setLetterToBold(String str) {
        wg6.b(str, "letter");
        int b2 = nd6.b((T[]) this.a, str);
        if (new mj6("[0-9]+").matches(str)) {
            b2 = this.a.length - 1;
        }
        LinearLayoutManager linearLayoutManager = this.g;
        if (linearLayoutManager != null) {
            linearLayoutManager.f(b2, 0);
        }
        a aVar = this.f;
        if (aVar != null) {
            aVar.a(b2);
        }
    }

    @DexIgnore
    public final void setMFontSize$app_fossilRelease(float f2) {
        this.b = f2;
    }

    @DexIgnore
    public final void setMItemColor$app_fossilRelease(int i) {
        this.c = i;
    }

    @DexIgnore
    public final void setOnSectionIndexClickListener(b bVar) {
        wg6.b(bVar, "sectionIndexClickListener");
        a aVar = this.f;
        if (aVar != null) {
            aVar.a(bVar);
        }
    }

    @DexIgnore
    public final void setTypeFaceBaseTheme$app_fossilRelease(Typeface typeface) {
        this.e = typeface;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.recyclerview.RecyclerViewAlphabetIndex, android.view.ViewGroup] */
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewAlphabetIndex(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        setOverScrollMode(2);
        a(context, attributeSet);
        b();
    }

    @DexIgnore
    public final void a() {
        setAlphabet(this.a);
    }
}
