package com.portfolio.platform.view.recyclerview;

import android.content.Context;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.hx5;
import com.fossil.wg6;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RecyclerViewCalendar$init$Anon1 extends GridLayoutManager {
    @DexIgnore
    public /* final */ /* synthetic */ Context R;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RecyclerViewCalendar$init$Anon1(Context context, Context context2, int i, int i2, boolean z) {
        super(context2, i, i2, z);
        this.R = context;
    }

    @DexIgnore
    public boolean a() {
        return false;
    }

    @DexIgnore
    public int k(RecyclerView.State state) {
        wg6.b(state, Constants.STATE);
        return hx5.a(this.R);
    }
}
