package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import com.fossil.x24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class UnderlinedTextView extends FlexibleTextView {
    @DexIgnore
    public Rect u;
    @DexIgnore
    public Paint v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public /* final */ Rect y;

    @DexIgnore
    public UnderlinedTextView(Context context) {
        this(context, (AttributeSet) null, 0);
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet, int i) {
        float f = context.getResources().getDisplayMetrics().density;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.UnderlinedTextView, i, 0);
        int color = obtainStyledAttributes.getColor(3, -65536);
        this.w = obtainStyledAttributes.getDimension(2, -1.0f);
        float dimension = obtainStyledAttributes.getDimension(1, f * 1.0f);
        this.x = obtainStyledAttributes.getDimension(0, 4.0f * dimension);
        obtainStyledAttributes.recycle();
        this.u = new Rect();
        this.v = new Paint();
        this.v.setStyle(Paint.Style.STROKE);
        this.v.setColor(color);
        this.v.setStrokeWidth(dimension);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v0, types: [androidx.appcompat.widget.AppCompatTextView, android.widget.TextView, com.portfolio.platform.view.UnderlinedTextView] */
    public void onDraw(Canvas canvas) {
        int lineCount = getLineCount();
        CharSequence text = getText();
        getPaint().getTextBounds(text.toString(), 0, text.length(), this.y);
        if (this.w == -1.0f) {
            for (int i = 0; i < lineCount; i++) {
                int lineBounds = getLineBounds(i, this.u);
                Rect rect = this.u;
                float f = (float) rect.left;
                float f2 = (float) lineBounds;
                float f3 = this.x;
                canvas.drawLine(f, f2 + f3, (float) rect.right, f2 + f3, this.v);
            }
        } else {
            int lineBounds2 = getLineBounds(getLineCount() - 1, this.u);
            float width = (((float) this.u.width()) - this.w) / 2.0f;
            Rect rect2 = this.u;
            float f4 = ((float) rect2.left) + width;
            float f5 = (float) lineBounds2;
            float f6 = this.x;
            canvas.drawLine(f4, f5 + f6, ((float) rect2.right) - width, f5 + f6, this.v);
        }
        UnderlinedTextView.super.onDraw(canvas);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public UnderlinedTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.y = new Rect();
        a(context, attributeSet, i);
    }
}
