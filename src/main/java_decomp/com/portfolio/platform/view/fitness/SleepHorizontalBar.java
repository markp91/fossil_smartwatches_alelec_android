package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.fossil.d7;
import com.fossil.kc6;
import com.fossil.lc6;
import com.fossil.nz5;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepHorizontalBar extends BaseFitnessChart {
    @DexIgnore
    public boolean A;
    @DexIgnore
    public Typeface B;
    @DexIgnore
    public RectF C;
    @DexIgnore
    public float D;
    @DexIgnore
    public float E;
    @DexIgnore
    public float F;
    @DexIgnore
    public float G;
    @DexIgnore
    public float H;
    @DexIgnore
    public int I;
    @DexIgnore
    public Paint J;
    @DexIgnore
    public /* final */ Paint K;
    @DexIgnore
    public /* final */ Paint L;
    @DexIgnore
    public /* final */ Paint M;
    @DexIgnore
    public /* final */ Paint N;
    @DexIgnore
    public /* final */ Paint O;
    @DexIgnore
    public ArrayList<LinkedList<lc6<b, Integer>>> d;
    @DexIgnore
    public float e;
    @DexIgnore
    public float f;
    @DexIgnore
    public float g;
    @DexIgnore
    public int h;
    @DexIgnore
    public String i;
    @DexIgnore
    public int j;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public float r;
    @DexIgnore
    public float s;
    @DexIgnore
    public float t;
    @DexIgnore
    public float u;
    @DexIgnore
    public float v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum b {
        AWAKE,
        SLEEP,
        DEEP
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        TypedArray obtainStyledAttributes;
        wg6.b(context, "context");
        this.d = new ArrayList<>();
        this.i = "";
        this.r = 5.0f;
        this.s = 8.0f;
        this.t = 8.0f;
        this.u = 8.0f;
        this.v = 1.0f;
        this.w = 8;
        this.x = -1;
        this.y = 10;
        this.A = true;
        this.C = new RectF();
        this.H = 8.0f;
        this.K = new Paint(1);
        this.L = new Paint(1);
        this.M = new Paint(1);
        this.N = new Paint(1);
        this.O = new Paint(1);
        if (!(attributeSet == null || (obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, x24.SleepHorizontalBar, 0, 0)) == null)) {
            this.h = obtainStyledAttributes.getColor(0, 0);
            this.j = obtainStyledAttributes.getColor(3, 0);
            this.o = obtainStyledAttributes.getColor(2, 0);
            this.p = obtainStyledAttributes.getColor(7, 0);
            this.q = obtainStyledAttributes.getColor(4, 0);
            this.r = (float) obtainStyledAttributes.getDimensionPixelSize(6, 5);
            this.t = (float) obtainStyledAttributes.getDimensionPixelSize(8, 8);
            this.s = (float) obtainStyledAttributes.getDimensionPixelSize(9, 8);
            this.u = (float) obtainStyledAttributes.getDimensionPixelSize(5, 8);
            this.v = obtainStyledAttributes.getFloat(10, 1.0f);
            this.x = obtainStyledAttributes.getResourceId(11, -1);
            this.w = obtainStyledAttributes.getDimensionPixelSize(12, 8);
            String string = obtainStyledAttributes.getString(13);
            this.i = string == null ? "" : string;
            this.z = obtainStyledAttributes.getColor(14, 0);
            this.y = obtainStyledAttributes.getDimensionPixelSize(15, 10);
            try {
                this.B = d7.a(getContext(), obtainStyledAttributes.getResourceId(1, -1));
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("SleepHorizontalBar", "init - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        c();
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        float f3 = rectF.left + f2;
        float f4 = rectF.top + f2;
        float f5 = rectF.right - f2;
        float f6 = rectF.bottom - f2;
        float f7 = (float) 0;
        if (f5 <= f7 || f5 < f3) {
            f5 = f3 + f2;
        }
        if (f6 <= f7 || f6 < f4) {
            f6 = f4 + f2;
        }
        return new RectF(f3, f4, f5, f6);
    }

    @DexIgnore
    public final void c() {
        this.O.setAntiAlias(true);
        this.O.setStyle(Paint.Style.STROKE);
        this.K.setColor(this.j);
        this.K.setAntiAlias(true);
        this.K.setStyle(Paint.Style.FILL);
        this.J = new Paint(this.K);
        this.M.setAlpha((int) (this.v * ((float) 255)));
        this.M.setColorFilter(new PorterDuffColorFilter(this.j, PorterDuff.Mode.SRC_IN));
        this.M.setAntiAlias(true);
        this.N.setColor(-1);
        this.N.setAntiAlias(true);
        this.N.setStyle(Paint.Style.FILL);
        this.L.setColor(this.z);
        this.L.setAntiAlias(true);
        this.L.setStyle(Paint.Style.FILL);
        this.L.setTextSize((float) this.y);
        Typeface typeface = this.B;
        if (typeface != null) {
            this.L.setTypeface(typeface);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return this.x;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.w;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        int i2;
        Canvas canvas2 = canvas;
        super.onDraw(canvas);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SleepHorizontalBar", "onDraw - mTotalValue=" + this.f + ", mGoal=" + this.g);
        this.D = ((float) getWidth()) - this.u;
        if (canvas2 != null) {
            canvas2.drawColor(this.h);
            RectF rectF = this.C;
            float f2 = this.e;
            float f3 = (float) 0;
            float f4 = f2 > f3 ? (this.D * this.f) / f2 : 0.0f;
            float f5 = this.s;
            float f6 = (float) 2;
            rectF.set(0.0f, 0.0f, f4 + (f5 * f6), f5 * ((float) 3));
            float f7 = this.f;
            if (f7 > f3) {
                if (f7 >= this.g) {
                    Paint paint = this.J;
                    if (paint != null) {
                        paint.setAlpha((int) 15.299999999999999d);
                        RectF rectF2 = this.C;
                        float f8 = (float) 4;
                        float f9 = this.r;
                        float f10 = f8 * f9;
                        float f11 = f8 * f9;
                        Paint paint2 = this.J;
                        if (paint2 != null) {
                            canvas2.drawRoundRect(rectF2, f10, f11, paint2);
                            Paint paint3 = this.J;
                            if (paint3 != null) {
                                paint3.setAlpha((int) 40.800000000000004d);
                                RectF a2 = a(this.C, this.s / f6);
                                float f12 = this.r;
                                float f13 = f12 * 2.0f;
                                float f14 = f12 * 2.0f;
                                Paint paint4 = this.J;
                                if (paint4 != null) {
                                    canvas2.drawRoundRect(a2, f13, f14, paint4);
                                } else {
                                    wg6.d("mPaintProgressReach");
                                    throw null;
                                }
                            } else {
                                wg6.d("mPaintProgressReach");
                                throw null;
                            }
                        } else {
                            wg6.d("mPaintProgressReach");
                            throw null;
                        }
                    } else {
                        wg6.d("mPaintProgressReach");
                        throw null;
                    }
                }
                if (this.I != 0) {
                    RectF a3 = a(this.C, this.s);
                    float f15 = a3.right;
                    float f16 = a3.left;
                    float f17 = (f15 - f16) - this.H;
                    float f18 = a3.bottom - a3.top;
                    int i3 = this.I;
                    float f19 = f16;
                    for (int i4 = 0; i4 < i3; i4++) {
                        LinkedList<lc6<b, Integer>> linkedList = this.d.get(i4);
                        wg6.a((Object) linkedList, "mValues[k]");
                        LinkedList<lc6> linkedList2 = linkedList;
                        int i5 = 0;
                        for (lc6 second : linkedList2) {
                            i5 += ((Number) second.getSecond()).intValue();
                        }
                        float f20 = (((float) i5) * f17) / this.f;
                        int i6 = 1;
                        if (f20 < ((float) 1)) {
                            f20 = this.s;
                        } else {
                            float f21 = this.D;
                            if (f20 > f21) {
                                f20 = f21;
                            }
                        }
                        lc6<Bitmap, Canvas> a4 = a((int) f20, (int) f18);
                        Bitmap first = a4.getFirst();
                        Canvas second2 = a4.getSecond();
                        int size = linkedList2.size();
                        int i7 = 0;
                        float f22 = 0.0f;
                        while (i7 < size) {
                            int i8 = size;
                            int i9 = nz5.a[((b) ((lc6) linkedList2.get(i7)).getFirst()).ordinal()];
                            if (i9 == i6) {
                                i2 = this.o;
                            } else if (i9 == 2) {
                                i2 = this.p;
                            } else if (i9 == 3) {
                                i2 = this.q;
                            } else {
                                throw new kc6();
                            }
                            float floatValue = (((Number) ((lc6) linkedList2.get(i7)).getSecond()).floatValue() * f17) / this.f;
                            this.K.setColor(i2);
                            float f23 = f22 + floatValue;
                            second2.drawRect(f22, 0.0f, f23, f18, this.K);
                            i7++;
                            f19 = f19;
                            size = i8;
                            f22 = f23;
                            second2 = second2;
                            i6 = 1;
                        }
                        float f24 = f19;
                        Bitmap a5 = a(first, this.r);
                        canvas2.drawBitmap(a5, f24, a3.top, this.K);
                        first.recycle();
                        a5.recycle();
                        f19 = f24 + this.t + f22;
                    }
                }
            }
            float height = ((float) getHeight()) / 2.0f;
            float f25 = this.e;
            float f26 = f25 > f3 ? (this.D * this.g) / f25 : 0.0f;
            float f27 = this.s;
            this.E = f26 + f27;
            if (this.f <= this.g) {
                canvas2.drawBitmap(getStartBitmap(), this.E - ((float) getStartBitmap().getWidth()), height - ((float) (getStartBitmap().getHeight() / 2)), this.M);
            } else {
                canvas2.drawCircle(this.E - (((float) this.w) / 2.0f), height, f27 / 4.0f, this.N);
            }
            if (this.A) {
                canvas2.drawText(this.i, (((float) getWidth()) - this.F) - f6, height + (this.G / f6), this.L);
            }
        }
    }

    @DexIgnore
    public void onGlobalLayout() {
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i3);
        int size = View.MeasureSpec.getSize(i3);
        if (mode != Integer.MIN_VALUE) {
            if (mode == 0) {
                size = (int) (this.s * ((float) 3));
            } else if (mode != 1073741824) {
                size = 0;
            }
        }
        setMeasuredDimension(getWidth(), size);
    }

    @DexIgnore
    @Keep
    public final void setMax(float f2) {
        this.e = f2;
        invalidate();
    }

    @DexIgnore
    public final lc6<Bitmap, Canvas> a(int i2, int i3) {
        Bitmap createBitmap = Bitmap.createBitmap(i2, i3, Bitmap.Config.ARGB_8888);
        return new lc6<>(createBitmap, new Canvas(createBitmap));
    }

    @DexIgnore
    public final Bitmap a(Bitmap bitmap, float f2) {
        Bitmap createBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.drawARGB(0, 0, 0, 0);
        Paint paint = new Paint(1);
        canvas.drawRoundRect(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight(), f2, f2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        canvas.drawBitmap(bitmap, rect, rect, paint);
        wg6.a((Object) createBitmap, "output");
        return createBitmap;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepHorizontalBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }
}
