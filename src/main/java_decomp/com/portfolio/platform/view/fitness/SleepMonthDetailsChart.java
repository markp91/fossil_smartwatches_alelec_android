package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.fossil.cd6;
import com.fossil.d7;
import com.fossil.dx5;
import com.fossil.hg6;
import com.fossil.oz5;
import com.fossil.qg6;
import com.fossil.sh4;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.sleep.SleepDayData;
import com.portfolio.platform.data.model.sleep.SleepSessionData;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String F;
    @DexIgnore
    public sh4 A;
    @DexIgnore
    public /* final */ ArrayList<SleepDayData> B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public int E;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ Paint t;
    @DexIgnore
    public /* final */ Paint u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public /* final */ int y;
    @DexIgnore
    public /* final */ int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<LinkedList<Integer>, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ SleepMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, int i2, SleepMonthDetailsChart sleepMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = sleepMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((LinkedList<Integer>) (LinkedList) obj);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            wg6.b(linkedList, "centerXList");
            this.this$0.a(this.$canvas$inlined, linkedList);
            SleepMonthDetailsChart sleepMonthDetailsChart = this.this$0;
            sleepMonthDetailsChart.a(this.$canvas$inlined, (List<Integer>) linkedList, this.$chartWidth, this.$chartHeight, sleepMonthDetailsChart.x * 2, this.$chartHeight - 4);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = SleepMonthDetailsChart.class.getSimpleName();
        wg6.a((Object) simpleName, "SleepMonthDetailsChart::class.java.simpleName");
        F = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = new Paint(1);
        this.u = new Paint(1);
        this.v = context.getResources().getDimensionPixelSize(2131165384);
        this.w = context.getResources().getDimensionPixelSize(2131165372);
        this.x = context.getResources().getDimensionPixelSize(2131165418);
        this.y = context.getResources().getDimensionPixelSize(2131165372);
        this.z = context.getResources().getDimensionPixelSize(2131165405);
        this.A = sh4.TOTAL_SLEEP;
        this.B = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, x24.SleepMonthDetailsChart, 0, 0));
        }
        int a2 = w6.a(context, 2131099838);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(4, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(3, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(0, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(1, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(2, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.i = mTypedArray6 != null ? mTypedArray6.getColor(5, a2) : a2;
        TypedArray mTypedArray7 = getMTypedArray();
        this.j = mTypedArray7 != null ? mTypedArray7.getDimensionPixelSize(7, 40) : 40;
        TypedArray mTypedArray8 = getMTypedArray();
        this.o = mTypedArray8 != null ? mTypedArray8.getResourceId(6, 2131296268) : 2131296268;
        TypedArray mTypedArray9 = getMTypedArray();
        if (mTypedArray9 != null) {
            mTypedArray9.recycle();
        }
    }

    @DexIgnore
    private final int getMChartMax() {
        int i2;
        int i3 = oz5.b[this.A.ordinal()];
        if (i3 == 1) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "Max of awake: " + this.E);
            i2 = this.E;
        } else if (i3 == 2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = F;
            local2.d(str2, "Max of light: " + this.D);
            i2 = this.D;
        } else if (i3 != 3) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = F;
            local3.d(str3, "Max of total: " + getMMaxSleepMinutes());
            i2 = getMMaxSleepMinutes();
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = F;
            local4.d(str4, "Max of restful: " + this.C);
            i2 = this.C;
        }
        return Math.max(i2, getMMaxGoal());
    }

    @DexIgnore
    private final int getMMaxGoal() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int sleepGoal = ((SleepDayData) t2).getSleepGoal();
                do {
                    T next = it.next();
                    int sleepGoal2 = ((SleepDayData) next).getSleepGoal();
                    if (sleepGoal < sleepGoal2) {
                        t2 = next;
                        sleepGoal = sleepGoal2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getSleepGoal();
        }
        return 480;
    }

    @DexIgnore
    private final int getMMaxSleepMinutes() {
        T t2;
        Iterator<T> it = this.B.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                int totalSleepMinutes = ((SleepDayData) t2).getTotalSleepMinutes();
                do {
                    T next = it.next();
                    int totalSleepMinutes2 = ((SleepDayData) next).getTotalSleepMinutes();
                    if (totalSleepMinutes < totalSleepMinutes2) {
                        t2 = next;
                        totalSleepMinutes = totalSleepMinutes2;
                    }
                } while (it.hasNext());
            }
        }
        SleepDayData sleepDayData = (SleepDayData) t2;
        if (sleepDayData != null) {
            return sleepDayData.getTotalSleepMinutes();
        }
        return 0;
    }

    @DexIgnore
    private final int getMSleepMode() {
        int i2 = oz5.a[this.A.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 != 2) {
            return i2 != 3 ? 3 : 2;
        }
        return 1;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            canvas.drawColor(-1);
            Rect rect = new Rect();
            this.r.getTextBounds("1", 0, yj6.c("1") > 0 ? yj6.c("1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.x)) - ((float) this.z));
            int width = getWidth() - getStartBitmap().getWidth();
            int i2 = this.x;
            int i3 = this.y;
            int i4 = (width - (i2 * 2)) - i3;
            a(canvas, i4, height, i3, i2 * 2, (hg6<? super LinkedList<Integer>, cd6>) new b(i4, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.v;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.p.setColor(this.d);
        float f2 = (float) 4;
        this.p.setStrokeWidth(f2);
        this.r.setColor(this.i);
        this.r.setStyle(Paint.Style.FILL);
        this.r.setTextSize((float) this.j);
        this.r.setTypeface(d7.a(getContext(), this.o));
        this.q.setColor(this.e);
        this.q.setStyle(Paint.Style.STROKE);
        this.q.setStrokeWidth(f2);
        this.q.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, 0.0f));
        this.s.setColor(this.f);
        this.s.setStrokeWidth((float) this.w);
        this.s.setStyle(Paint.Style.FILL);
        this.t.setColor(this.g);
        this.t.setStrokeWidth((float) this.w);
        this.t.setStyle(Paint.Style.FILL);
        this.u.setColor(this.h);
        this.u.setStrokeWidth((float) this.w);
        this.u.setStyle(Paint.Style.FILL);
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, hg6<? super LinkedList<Integer>, cd6> hg6) {
        float f2;
        int i6 = i3;
        if (!this.B.isEmpty()) {
            int size = i2 / this.B.size();
            int i7 = this.w;
            int i8 = size < i7 ? size : i7;
            int i9 = i8 / 2;
            LinkedList linkedList = new LinkedList();
            float mChartMax = (float) getMChartMax();
            Iterator<SleepDayData> it = this.B.iterator();
            int i10 = i4;
            while (it.hasNext()) {
                SleepDayData next = it.next();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = F;
                local.d(str, "Actual sleep: " + next.getTotalSleepMinutes() + ", chart max: " + mChartMax);
                int i11 = i10 + (i9 / 2);
                int totalSleepMinutes = next.getTotalSleepMinutes();
                int mSleepMode = getMSleepMode();
                if (mSleepMode == 0) {
                    int i12 = totalSleepMinutes;
                    f2 = mChartMax;
                    int dayAwake = next.getDayAwake();
                    a(canvas, this.s, i11, i3, (int) ((((float) dayAwake) / f2) * ((float) i6)), i9, i5, dayAwake, i12);
                } else if (mSleepMode == 1) {
                    int i13 = totalSleepMinutes;
                    f2 = mChartMax;
                    int dayLight = next.getDayLight();
                    a(canvas, this.t, i11, i3, (int) ((((float) dayLight) / f2) * ((float) i6)), i9, i5, dayLight, i13);
                } else if (mSleepMode != 2) {
                    wg6.a((Object) next, "sleepDayData");
                    a(canvas, i11, i3, (int) ((((float) totalSleepMinutes) / mChartMax) * ((float) i6)), i9, i5, next);
                    f2 = mChartMax;
                } else {
                    int dayRestful = next.getDayRestful();
                    int i14 = totalSleepMinutes;
                    f2 = mChartMax;
                    a(canvas, this.u, i11, i3, (int) ((((float) dayRestful) / mChartMax) * ((float) i6)), i9, i5, dayRestful, i14);
                }
                i10 += i8;
                linkedList.add(Integer.valueOf(i11));
                mChartMax = f2;
            }
            hg6.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, Paint paint, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        int i9 = i2 - (i5 / 2);
        if (i9 < 0) {
            i9 = 0;
        }
        RectF rectF = new RectF((float) i9, (float) ((i3 - ((int) ((((float) i7) / ((float) i8)) * ((float) i4)))) + i6), (float) (i5 + i9), (float) i3);
        dx5.c(canvas, rectF, paint, rectF.width() / ((float) 3));
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, int i5, int i6, SleepDayData sleepDayData) {
        int i7;
        int i8;
        int i9;
        int i10;
        Paint paint;
        int i11 = i2 - (i5 / 2);
        if (i11 < 0) {
            i11 = 0;
        }
        int i12 = i11 + i5;
        int size = (i4 - i6) - (this.z * (sleepDayData.getSessionList().size() - 1 > 0 ? sleepDayData.getSessionList().size() - 1 : 0));
        Iterator<SleepSessionData> it = sleepDayData.getSessionList().iterator();
        int i13 = 0;
        while (it.hasNext()) {
            int component1 = it.next().component1();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = F;
            local.d(str, "session duration: " + component1);
            i13 += component1;
        }
        int size2 = sleepDayData.getSessionList().size();
        int i14 = i3;
        int i15 = 0;
        while (i15 < size2) {
            SleepSessionData sleepSessionData = sleepDayData.getSessionList().get(i15);
            wg6.a((Object) sleepSessionData, "sleepDayData.sessionList[sessionIndex]");
            SleepSessionData sleepSessionData2 = sleepSessionData;
            int durationInMinutes = sleepSessionData2.getDurationInMinutes();
            float f2 = (float) durationInMinutes;
            int i16 = (int) ((f2 / ((float) i13)) * ((float) size));
            List<WrapperSleepStateChange> sleepStates = sleepSessionData2.getSleepStates();
            int size3 = sleepStates.size();
            int i17 = i14;
            int i18 = 0;
            while (i18 < size3) {
                int i19 = sleepStates.get(i18).state;
                if (i18 < size3 - 1) {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = ((int) sleepStates.get(i18 + 1).index) - ((int) sleepStates.get(i18).index);
                } else {
                    i8 = size;
                    i7 = size2;
                    i9 = i13;
                    i10 = durationInMinutes - ((int) sleepStates.get(i18).index);
                }
                int i20 = (int) ((((float) i10) / f2) * ((float) i16));
                if (i20 < 1) {
                    i20 = 1;
                }
                int i21 = i17 - i20;
                int i22 = i11;
                RectF rectF = new RectF((float) i11, (float) i21, (float) i12, (float) i17);
                if (i19 == 1) {
                    paint = this.t;
                } else if (i19 != 2) {
                    paint = this.s;
                } else {
                    paint = this.u;
                }
                canvas.drawRect(rectF, paint);
                i18++;
                size = i8;
                size2 = i7;
                i17 = i21;
                i13 = i9;
                i11 = i22;
            }
            Canvas canvas2 = canvas;
            int i23 = size2;
            int i24 = i13;
            i14 = i17 - this.z;
            i15++;
            size = size;
            i11 = i11;
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public SleepMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        int i2;
        if (!list.isEmpty()) {
            int size = list.size();
            int i3 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                i2 = 0;
                while (i3 < length) {
                    try {
                        i2 = numArr[i3].intValue();
                        a(canvas, String.valueOf(i2 + 1), list.get(i2).intValue());
                        i3++;
                    } catch (Exception e2) {
                        e = e2;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = F;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e3) {
                e = e3;
                i2 = 0;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = F;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.r.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.z), this.r);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.p);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.p);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4, int i5) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.B.isEmpty()) && this.B.size() > 1) {
            float mChartMax = (float) getMChartMax();
            Path path = new Path();
            int size = list.size();
            float height = (float) getStartBitmap().getHeight();
            int i6 = 1;
            while (i6 < size) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = F;
                StringBuilder sb = new StringBuilder();
                sb.append("Previous sleep goal: ");
                int i7 = i6 - 1;
                sb.append(this.B.get(i7).getSleepGoal());
                sb.append(", current sleep goal: ");
                sb.append(this.B.get(i6).getSleepGoal());
                sb.append(", chart max: ");
                sb.append(mChartMax);
                local.d(str, sb.toString());
                float intValue = (float) list2.get(i6).intValue();
                float f2 = (float) i3;
                float f3 = (float) i4;
                float f4 = (float) i5;
                float min = Math.min(((1.0f - (((float) this.B.get(i6).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                float intValue2 = (float) list2.get(i7).intValue();
                float min2 = Math.min(((1.0f - (((float) this.B.get(i7).getSleepGoal()) / mChartMax)) * f2) + f3, f4);
                if (min == min2) {
                    path.moveTo(intValue2, min2);
                    if (i6 == list.size() - 1) {
                        path.lineTo((float) (i2 + this.x), min);
                    } else {
                        path.lineTo(intValue, min);
                    }
                    canvas2.drawPath(path, this.q);
                } else {
                    path.moveTo(intValue2, min2);
                    path.lineTo(intValue, min2);
                    canvas2.drawPath(path, this.q);
                    path.moveTo(intValue, min2);
                    path.lineTo(intValue, min);
                    canvas2.drawPath(path, this.q);
                    if (i6 == list.size() - 1) {
                        path.moveTo(intValue, min);
                        path.lineTo((float) (i2 + this.x), min);
                        canvas2.drawPath(path, this.q);
                    }
                }
                i6++;
                list2 = list;
                height = min;
            }
            canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.x), height - ((float) (getStartBitmap().getHeight() / 2)), this.r);
        }
    }
}
