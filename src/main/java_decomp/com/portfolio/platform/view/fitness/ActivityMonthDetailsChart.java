package com.portfolio.platform.view.fitness;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import com.fossil.cd6;
import com.fossil.d7;
import com.fossil.dx5;
import com.fossil.hg6;
import com.fossil.lc6;
import com.fossil.qg6;
import com.fossil.w6;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.xg6;
import com.fossil.yj6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityMonthDetailsChart extends BaseFitnessChart {
    @DexIgnore
    public static /* final */ String C;
    @DexIgnore
    public float A;
    @DexIgnore
    public float B;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ Paint o;
    @DexIgnore
    public /* final */ Paint p;
    @DexIgnore
    public /* final */ Paint q;
    @DexIgnore
    public /* final */ Paint r;
    @DexIgnore
    public /* final */ Paint s;
    @DexIgnore
    public /* final */ int t;
    @DexIgnore
    public /* final */ int u;
    @DexIgnore
    public /* final */ int v;
    @DexIgnore
    public /* final */ int w;
    @DexIgnore
    public /* final */ int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public /* final */ ArrayList<lc6<Float, Float>> z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<LinkedList<Integer>, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Canvas $canvas$inlined;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartHeight;
        @DexIgnore
        public /* final */ /* synthetic */ int $chartWidth;
        @DexIgnore
        public /* final */ /* synthetic */ ActivityMonthDetailsChart this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i, int i2, ActivityMonthDetailsChart activityMonthDetailsChart, Canvas canvas) {
            super(1);
            this.$chartWidth = i;
            this.$chartHeight = i2;
            this.this$0 = activityMonthDetailsChart;
            this.$canvas$inlined = canvas;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((LinkedList<Integer>) (LinkedList) obj);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(LinkedList<Integer> linkedList) {
            wg6.b(linkedList, "barCenterXList");
            this.this$0.a(this.$canvas$inlined, linkedList);
            ActivityMonthDetailsChart activityMonthDetailsChart = this.this$0;
            Canvas canvas = this.$canvas$inlined;
            int i = this.$chartWidth;
            int i2 = this.$chartHeight;
            activityMonthDetailsChart.a(canvas, (List<Integer>) linkedList, i, i2, i2 - 4);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = ActivityMonthDetailsChart.class.getSimpleName();
        wg6.a((Object) simpleName, "ActivityMonthDetailsChart::class.java.simpleName");
        C = simpleName;
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        wg6.b(context, "context");
        this.o = new Paint(1);
        this.p = new Paint(1);
        this.q = new Paint(1);
        this.r = new Paint(1);
        this.s = new Paint(1);
        this.t = context.getResources().getDimensionPixelSize(2131165384);
        this.u = context.getResources().getDimensionPixelSize(2131165372);
        this.v = context.getResources().getDimensionPixelSize(2131165418);
        this.w = context.getResources().getDimensionPixelSize(2131165405);
        this.x = context.getResources().getDimensionPixelSize(2131165372);
        this.y = 4;
        this.z = new ArrayList<>();
        getViewTreeObserver().addOnGlobalLayoutListener(this);
        if (attributeSet != null) {
            setMTypedArray(context.getTheme().obtainStyledAttributes(attributeSet, x24.ActivityMonthDetailsChart, 0, 0));
        }
        int a2 = w6.a(context, 2131099838);
        TypedArray mTypedArray = getMTypedArray();
        this.d = mTypedArray != null ? mTypedArray.getColor(3, a2) : a2;
        TypedArray mTypedArray2 = getMTypedArray();
        this.e = mTypedArray2 != null ? mTypedArray2.getColor(2, a2) : a2;
        TypedArray mTypedArray3 = getMTypedArray();
        this.f = mTypedArray3 != null ? mTypedArray3.getColor(1, a2) : a2;
        TypedArray mTypedArray4 = getMTypedArray();
        this.g = mTypedArray4 != null ? mTypedArray4.getColor(0, a2) : a2;
        TypedArray mTypedArray5 = getMTypedArray();
        this.h = mTypedArray5 != null ? mTypedArray5.getColor(4, a2) : a2;
        TypedArray mTypedArray6 = getMTypedArray();
        this.j = mTypedArray6 != null ? mTypedArray6.getDimensionPixelSize(6, 40) : 40;
        TypedArray mTypedArray7 = getMTypedArray();
        this.i = mTypedArray7 != null ? mTypedArray7.getResourceId(5, 2131296268) : 2131296268;
        TypedArray mTypedArray8 = getMTypedArray();
        if (mTypedArray8 != null) {
            mTypedArray8.recycle();
        }
    }

    @DexIgnore
    private final float getMChartMax() {
        return Math.max(getMMaxGoal(), getMMaxValues());
    }

    @DexIgnore
    private final float getMMaxGoal() {
        T t2;
        Float f2;
        Iterator<T> it = this.z.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((lc6) t2).getSecond()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((lc6) next).getSecond()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        lc6 lc6 = (lc6) t2;
        if (lc6 == null || (f2 = (Float) lc6.getSecond()) == null) {
            return this.A;
        }
        return f2.floatValue();
    }

    @DexIgnore
    private final float getMMaxValues() {
        T t2;
        Float f2;
        Iterator<T> it = this.z.iterator();
        if (!it.hasNext()) {
            t2 = null;
        } else {
            t2 = it.next();
            if (it.hasNext()) {
                float floatValue = ((Number) ((lc6) t2).getFirst()).floatValue();
                do {
                    T next = it.next();
                    float floatValue2 = ((Number) ((lc6) next).getFirst()).floatValue();
                    if (Float.compare(floatValue, floatValue2) < 0) {
                        t2 = next;
                        floatValue = floatValue2;
                    }
                } while (it.hasNext());
            }
        }
        lc6 lc6 = (lc6) t2;
        if (lc6 == null || (f2 = (Float) lc6.getFirst()) == null) {
            return this.B;
        }
        return f2.floatValue();
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        if (canvas != null) {
            Rect rect = new Rect();
            this.q.getTextBounds("1", 0, yj6.c("1") > 0 ? yj6.c("1") : 1, rect);
            int height = (int) (((((float) getHeight()) - ((float) rect.height())) - ((float) this.v)) - ((float) this.w));
            int i2 = this.x;
            int width = ((getWidth() - getStartBitmap().getWidth()) - (this.v * 2)) - i2;
            a(canvas, width, height, i2, (hg6<? super LinkedList<Integer>, cd6>) new b(width, height, this, canvas));
            a(canvas, 0, height);
        }
    }

    @DexIgnore
    public int getStarIconResId() {
        return 17301515;
    }

    @DexIgnore
    public int getStarSizeInPx() {
        return this.t;
    }

    @DexIgnore
    public void onGlobalLayout() {
        this.o.setColor(this.d);
        float f2 = (float) 4;
        this.o.setStrokeWidth(f2);
        this.q.setColor(this.h);
        this.q.setStyle(Paint.Style.FILL);
        this.q.setTextSize((float) this.j);
        this.q.setTypeface(d7.a(getContext(), this.i));
        this.p.setColor(this.e);
        this.p.setStyle(Paint.Style.STROKE);
        this.p.setStrokeWidth(f2);
        this.p.setPathEffect(new DashPathEffect(new float[]{10.0f, 10.0f}, 0.0f));
        this.r.setColor(this.f);
        this.r.setStrokeWidth((float) this.u);
        this.r.setStyle(Paint.Style.FILL);
        this.s.setColor(this.g);
        this.s.setStrokeWidth((float) this.u);
        this.s.setStyle(Paint.Style.FILL);
        this.y = getStartBitmap().getHeight() / 2;
        getViewTreeObserver().removeOnGlobalLayoutListener(this);
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3, int i4, hg6<? super LinkedList<Integer>, cd6> hg6) {
        if (!this.z.isEmpty()) {
            float mChartMax = getMChartMax();
            int size = i2 / this.z.size();
            int i5 = this.u;
            if (size < i5) {
                i5 = i2 / this.z.size();
            }
            int i6 = i5 / 2;
            int i7 = i6 / 3;
            LinkedList linkedList = new LinkedList();
            Iterator<lc6<Float, Float>> it = this.z.iterator();
            while (it.hasNext()) {
                float f2 = (float) i3;
                RectF rectF = new RectF((float) i4, Math.max(f2 - ((((Number) it.next().component1()).floatValue() / mChartMax) * f2), (float) this.y), (float) (i4 + i6), f2);
                linkedList.add(Integer.valueOf((int) (rectF.left + (rectF.width() / ((float) 2)))));
                i4 += i5;
                dx5.c(canvas, rectF, this.r, (float) i7);
            }
            hg6.invoke(linkedList);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, int i2, int i3) {
        canvas.drawRect(new Rect(0, i2, getWidth(), i2 + 4), this.o);
        canvas.drawRect(new Rect(0, i3 - 4, getWidth(), i3), this.o);
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list, int i2, int i3, int i4) {
        Canvas canvas2 = canvas;
        List<Integer> list2 = list;
        if ((!this.z.isEmpty()) && this.z.size() > 1) {
            float mChartMax = getMChartMax();
            if (mChartMax > ((float) 0)) {
                Path path = new Path();
                int size = list.size();
                float height = (float) getStartBitmap().getHeight();
                int i5 = 1;
                while (i5 < size) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = C;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Previous goal: ");
                    int i6 = i5 - 1;
                    sb.append(((Number) this.z.get(i6).getSecond()).floatValue());
                    sb.append(", current goal: ");
                    sb.append(((Number) this.z.get(i5).getSecond()).floatValue());
                    sb.append(", chart max: ");
                    sb.append(mChartMax);
                    local.d(str, sb.toString());
                    float intValue = (float) list2.get(i5).intValue();
                    float f2 = (float) i3;
                    float f3 = (float) i4;
                    float max = Math.max(Math.min((1.0f - (((Number) this.z.get(i5).getSecond()).floatValue() / mChartMax)) * f2, f3), (float) this.y);
                    float intValue2 = (float) list2.get(i6).intValue();
                    float max2 = Math.max(Math.min((1.0f - (((Number) this.z.get(i6).getSecond()).floatValue() / mChartMax)) * f2, f3), (float) this.y);
                    if (max == max2) {
                        path.moveTo(intValue2, max2);
                        if (i5 == list.size() - 1) {
                            path.lineTo((float) (i2 + this.v), max);
                        } else {
                            path.lineTo(intValue, max);
                        }
                        canvas2.drawPath(path, this.p);
                    } else {
                        path.moveTo(intValue2, max2);
                        path.lineTo(intValue, max2);
                        canvas2.drawPath(path, this.p);
                        path.moveTo(intValue, max2);
                        path.lineTo(intValue, max);
                        canvas2.drawPath(path, this.p);
                        if (i5 == list.size() - 1) {
                            path.moveTo(intValue, max);
                            path.lineTo((float) (i2 + this.v), max);
                            canvas2.drawPath(path, this.p);
                        }
                    }
                    i5++;
                    height = max;
                }
                canvas2.drawBitmap(getStartBitmap(), (float) (i2 + this.v), height - ((float) (getStartBitmap().getHeight() / 2)), this.q);
            }
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context) {
        this(context, (AttributeSet) null, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public ActivityMonthDetailsChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
        wg6.b(context, "context");
    }

    @DexIgnore
    public final void a(Canvas canvas, List<Integer> list) {
        Integer[] numArr;
        int i2;
        if (!list.isEmpty()) {
            int size = list.size();
            int i3 = 0;
            switch (size) {
                case 28:
                    numArr = new Integer[]{0, 7, 14, 21, 27};
                    break;
                case 29:
                    numArr = new Integer[]{0, 7, 14, 21, 28};
                    break;
                case 30:
                    numArr = new Integer[]{0, 7, 14, 21, 29};
                    break;
                default:
                    numArr = new Integer[]{0, 7, 14, 21, 30};
                    break;
            }
            try {
                int length = numArr.length;
                i2 = 0;
                while (i3 < length) {
                    try {
                        i2 = numArr[i3].intValue();
                        a(canvas, String.valueOf(i2 + 1), list.get(i2).intValue());
                        i3++;
                    } catch (Exception e2) {
                        e = e2;
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = C;
                        local.d(str, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
                    }
                }
            } catch (Exception e3) {
                e = e3;
                i2 = 0;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = C;
                local2.d(str2, "Try to draw item: " + i2 + " with list size: " + size + " cause exception: " + e);
            }
        }
    }

    @DexIgnore
    public final void a(Canvas canvas, String str, int i2) {
        canvas.drawText(str, ((float) i2) - (this.q.measureText(str, 0, str.length()) / ((float) 2)), (float) ((getHeight() - (new Rect().height() / 2)) - this.w), this.q);
    }
}
