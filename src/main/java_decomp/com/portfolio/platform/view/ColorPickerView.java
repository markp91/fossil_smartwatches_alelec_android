package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposeShader;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import com.fossil.py5;
import com.fossil.x24;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ColorPickerView extends View {
    @DexIgnore
    public String A;
    @DexIgnore
    public int B;
    @DexIgnore
    public int C;
    @DexIgnore
    public int D;
    @DexIgnore
    public Rect E;
    @DexIgnore
    public Rect F;
    @DexIgnore
    public Rect G;
    @DexIgnore
    public Rect H;
    @DexIgnore
    public Point I;
    @DexIgnore
    public c J;
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Paint g;
    @DexIgnore
    public Paint h;
    @DexIgnore
    public Paint i;
    @DexIgnore
    public Paint j;
    @DexIgnore
    public Paint o;
    @DexIgnore
    public Paint p;
    @DexIgnore
    public Shader q;
    @DexIgnore
    public Shader r;
    @DexIgnore
    public Shader s;
    @DexIgnore
    public b t;
    @DexIgnore
    public b u;
    @DexIgnore
    public int v;
    @DexIgnore
    public float w;
    @DexIgnore
    public float x;
    @DexIgnore
    public float y;
    @DexIgnore
    public boolean z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b {
        @DexIgnore
        public Canvas a;
        @DexIgnore
        public Bitmap b;
        @DexIgnore
        public float c;

        @DexIgnore
        public b(ColorPickerView colorPickerView) {
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void q(int i);
    }

    @DexIgnore
    public ColorPickerView(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    private int getPreferredHeight() {
        int a2 = py5.a(getContext(), 200.0f);
        return this.z ? a2 + this.c + this.b : a2;
    }

    @DexIgnore
    private int getPreferredWidth() {
        return py5.a(getContext(), 200.0f) + this.a + this.c;
    }

    @DexIgnore
    public final void a(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.ColorPickerView);
        this.z = obtainStyledAttributes.getBoolean(1, false);
        this.B = obtainStyledAttributes.getColor(3, -4342339);
        this.C = obtainStyledAttributes.getColor(2, -9539986);
        obtainStyledAttributes.recycle();
        a(context);
        this.a = py5.a(getContext(), 30.0f);
        this.b = py5.a(getContext(), 20.0f);
        this.c = py5.a(getContext(), 10.0f);
        this.d = py5.a(getContext(), 5.0f);
        this.f = py5.a(getContext(), 4.0f);
        this.e = py5.a(getContext(), 2.0f);
        this.D = getResources().getDimensionPixelSize(2131165316);
        a();
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    @DexIgnore
    public final void b(Canvas canvas) {
        Canvas canvas2 = canvas;
        Rect rect = this.G;
        this.p.setColor(this.C);
        canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.p);
        if (this.u == null) {
            this.u = new b();
            this.u.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            b bVar = this.u;
            bVar.a = new Canvas(bVar.b);
            int[] iArr = new int[((int) (((float) rect.height()) + 0.5f))];
            float f2 = 360.0f;
            for (int i2 = 0; i2 < iArr.length; i2++) {
                iArr[i2] = Color.HSVToColor(new float[]{f2, 1.0f, 1.0f});
                f2 -= 360.0f / ((float) iArr.length);
            }
            Paint paint = new Paint();
            paint.setStrokeWidth(0.0f);
            for (int i3 = 0; i3 < iArr.length; i3++) {
                paint.setColor(iArr[i3]);
                b bVar2 = this.u;
                float f3 = (float) i3;
                bVar2.a.drawLine(0.0f, f3, (float) bVar2.b.getWidth(), f3, paint);
            }
        }
        canvas2.drawBitmap(this.u.b, (Rect) null, rect, (Paint) null);
        Point a2 = a(this.w);
        RectF rectF = new RectF();
        int i4 = rect.left;
        int i5 = this.e;
        rectF.left = (float) (i4 - i5);
        rectF.right = (float) (rect.right + i5);
        int i6 = a2.y;
        int i7 = this.f;
        rectF.top = (float) (i6 - (i7 / 2));
        rectF.bottom = (float) (i6 + (i7 / 2));
        canvas2.drawRoundRect(rectF, 2.0f, 2.0f, this.o);
    }

    @DexIgnore
    public final void c(Canvas canvas) {
        Rect rect = this.F;
        this.p.setColor(this.C);
        Rect rect2 = this.E;
        canvas.drawRect((float) rect2.left, (float) rect2.top, (float) (rect.right + 1), (float) (rect.bottom + 1), this.p);
        if (this.q == null) {
            int i2 = rect.left;
            this.q = new LinearGradient((float) i2, (float) rect.top, (float) i2, (float) rect.bottom, -1, -16777216, Shader.TileMode.CLAMP);
        }
        b bVar = this.t;
        if (bVar == null || bVar.c != this.w) {
            if (this.t == null) {
                this.t = new b();
            }
            b bVar2 = this.t;
            if (bVar2.b == null) {
                bVar2.b = Bitmap.createBitmap(rect.width(), rect.height(), Bitmap.Config.ARGB_8888);
            }
            b bVar3 = this.t;
            if (bVar3.a == null) {
                bVar3.a = new Canvas(bVar3.b);
            }
            int HSVToColor = Color.HSVToColor(new float[]{this.w, 1.0f, 1.0f});
            float f2 = (float) rect.left;
            int i3 = rect.top;
            this.r = new LinearGradient(f2, (float) i3, (float) rect.right, (float) i3, -1, HSVToColor, Shader.TileMode.CLAMP);
            this.g.setShader(new ComposeShader(this.q, this.r, PorterDuff.Mode.MULTIPLY));
            b bVar4 = this.t;
            bVar4.a.drawRect(0.0f, 0.0f, (float) bVar4.b.getWidth(), (float) this.t.b.getHeight(), this.g);
            this.t.c = this.w;
        }
        canvas.drawBitmap(this.t.b, (Rect) null, rect, (Paint) null);
        Point b2 = b(this.x, this.y);
        this.h.setColor(-16777216);
        canvas.drawCircle((float) b2.x, (float) b2.y, (float) (this.d - py5.a(getContext(), 1.0f)), this.h);
        this.h.setColor(-2236963);
        canvas.drawCircle((float) b2.x, (float) b2.y, (float) this.d, this.h);
    }

    @DexIgnore
    public final void d() {
        Rect rect = this.E;
        int i2 = rect.left + 1;
        int i3 = rect.top + 1;
        int i4 = rect.bottom - 1;
        int i5 = this.c;
        int i6 = ((rect.right - 1) - i5) - this.a;
        if (this.z) {
            i4 -= this.b + i5;
        }
        this.F = new Rect(i2, i3, i6, i4);
    }

    @DexIgnore
    public String getAlphaSliderText() {
        return this.A;
    }

    @DexIgnore
    public int getBorderColor() {
        return this.C;
    }

    @DexIgnore
    public int getColor() {
        return Color.HSVToColor(this.v, new float[]{this.w, this.x, this.y});
    }

    @DexIgnore
    public int getPaddingBottom() {
        return Math.max(super.getPaddingBottom(), this.D);
    }

    @DexIgnore
    public int getPaddingLeft() {
        return Math.max(super.getPaddingLeft(), this.D);
    }

    @DexIgnore
    public int getPaddingRight() {
        return Math.max(super.getPaddingRight(), this.D);
    }

    @DexIgnore
    public int getPaddingTop() {
        return Math.max(super.getPaddingTop(), this.D);
    }

    @DexIgnore
    public int getSliderTrackerColor() {
        return this.B;
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        if (this.E.width() > 0 && this.E.height() > 0) {
            c(canvas);
            b(canvas);
            a(canvas);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0058, code lost:
        if (r0 != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0087, code lost:
        if (r1 > r6) goto L_0x0089;
     */
    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int i4;
        int mode = View.MeasureSpec.getMode(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size = (View.MeasureSpec.getSize(i2) - getPaddingLeft()) - getPaddingRight();
        int size2 = (View.MeasureSpec.getSize(i3) - getPaddingBottom()) - getPaddingTop();
        if (mode != 1073741824 && mode2 != 1073741824) {
            int i5 = this.c;
            int i6 = this.a;
            i4 = size2 + i5 + i6;
            int i7 = (size - i5) - i6;
            if (this.z) {
                int i8 = this.b;
                i4 -= i5 + i8;
                i7 += i5 + i8;
            }
            boolean z2 = true;
            boolean z3 = i4 <= size;
            if (i7 > size2) {
                z2 = false;
            }
            if (!z3 || !z2) {
                if (z2 || !z3) {
                    if (!z3) {
                    }
                    setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size2 + getPaddingTop() + getPaddingBottom());
                }
            }
            size2 = i7;
            setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size2 + getPaddingTop() + getPaddingBottom());
        } else if (mode != 1073741824 || mode2 == 1073741824) {
            if (mode2 == 1073741824 && mode != 1073741824) {
                int i9 = this.c;
                int i10 = size2 + i9 + this.a;
                if (this.z) {
                    i10 -= i9 + this.b;
                }
            }
            setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size2 + getPaddingTop() + getPaddingBottom());
        } else {
            int i11 = this.c;
            int i12 = (size - i11) - this.a;
            if (this.z) {
                i12 += i11 + this.b;
            }
            if (i12 <= size2) {
                size2 = i12;
            }
            setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size2 + getPaddingTop() + getPaddingBottom());
        }
        size = i4;
        setMeasuredDimension(size + getPaddingLeft() + getPaddingRight(), size2 + getPaddingTop() + getPaddingBottom());
    }

    @DexIgnore
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.v = bundle.getInt("alpha");
            this.w = bundle.getFloat("hue");
            this.x = bundle.getFloat("sat");
            this.y = bundle.getFloat("val");
            this.z = bundle.getBoolean("show_alpha");
            this.A = bundle.getString("alpha_text");
            parcelable = bundle.getParcelable("instanceState");
        }
        super.onRestoreInstanceState(parcelable);
    }

    @DexIgnore
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("alpha", this.v);
        bundle.putFloat("hue", this.w);
        bundle.putFloat("sat", this.x);
        bundle.putFloat("val", this.y);
        bundle.putBoolean("show_alpha", this.z);
        bundle.putString("alpha_text", this.A);
        return bundle;
    }

    @DexIgnore
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        this.E = new Rect();
        this.E.left = getPaddingLeft();
        this.E.right = i2 - getPaddingRight();
        this.E.top = getPaddingTop();
        this.E.bottom = i3 - getPaddingBottom();
        this.q = null;
        this.r = null;
        this.s = null;
        this.t = null;
        this.u = null;
        d();
        c();
        b();
    }

    @DexIgnore
    public boolean onTouchEvent(MotionEvent motionEvent) {
        boolean z2;
        int action = motionEvent.getAction();
        if (action == 0) {
            this.I = new Point((int) motionEvent.getX(), (int) motionEvent.getY());
            z2 = a(motionEvent);
        } else if (action == 1) {
            this.I = null;
            z2 = a(motionEvent);
        } else if (action != 2) {
            z2 = false;
        } else {
            z2 = a(motionEvent);
        }
        if (!z2) {
            return super.onTouchEvent(motionEvent);
        }
        c cVar = this.J;
        if (cVar != null) {
            cVar.q(Color.HSVToColor(this.v, new float[]{this.w, this.x, this.y}));
        }
        invalidate();
        return true;
    }

    @DexIgnore
    public void setAlphaSliderText(int i2) {
        setAlphaSliderText(getContext().getString(i2));
    }

    @DexIgnore
    public void setAlphaSliderVisible(boolean z2) {
        if (this.z != z2) {
            this.z = z2;
            this.q = null;
            this.r = null;
            this.s = null;
            this.u = null;
            this.t = null;
            requestLayout();
        }
    }

    @DexIgnore
    public void setBorderColor(int i2) {
        this.C = i2;
        invalidate();
    }

    @DexIgnore
    public void setColor(int i2) {
        a(i2, false);
    }

    @DexIgnore
    public void setOnColorChangedListener(c cVar) {
        this.J = cVar;
    }

    @DexIgnore
    public void setSliderTrackerColor(int i2) {
        this.B = i2;
        this.o.setColor(this.B);
        invalidate();
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public ColorPickerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.v = 255;
        this.w = 360.0f;
        this.x = 0.0f;
        this.y = 0.0f;
        this.z = false;
        this.A = null;
        this.B = -4342339;
        this.C = -9539986;
        this.I = null;
        a(context, attributeSet);
    }

    @DexIgnore
    public void setAlphaSliderText(String str) {
        this.A = str;
        invalidate();
    }

    @DexIgnore
    public final void a(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(new TypedValue().data, new int[]{16842808});
        if (this.C == -9539986) {
            this.C = obtainStyledAttributes.getColor(0, -9539986);
        }
        if (this.B == -4342339) {
            this.B = obtainStyledAttributes.getColor(0, -4342339);
        }
        obtainStyledAttributes.recycle();
    }

    @DexIgnore
    public final void a() {
        this.g = new Paint();
        this.h = new Paint();
        this.o = new Paint();
        this.i = new Paint();
        this.j = new Paint();
        this.p = new Paint();
        this.h.setStyle(Paint.Style.STROKE);
        this.h.setStrokeWidth((float) py5.a(getContext(), 2.0f));
        this.h.setAntiAlias(true);
        this.o.setColor(this.B);
        this.o.setStyle(Paint.Style.STROKE);
        this.o.setStrokeWidth((float) py5.a(getContext(), 2.0f));
        this.o.setAntiAlias(true);
        this.j.setColor(-14935012);
        this.j.setTextSize((float) py5.a(getContext(), 14.0f));
        this.j.setAntiAlias(true);
        this.j.setTextAlign(Paint.Align.CENTER);
        this.j.setFakeBoldText(true);
    }

    @DexIgnore
    public final Point b(float f2, float f3) {
        Rect rect = this.F;
        Point point = new Point();
        point.x = (int) ((f2 * ((float) rect.width())) + ((float) rect.left));
        point.y = (int) (((1.0f - f3) * ((float) rect.height())) + ((float) rect.top));
        return point;
    }

    @DexIgnore
    public final void c() {
        Rect rect = this.E;
        this.G = new Rect((rect.right - this.a) + 1, rect.top + 1, rect.right - 1, (rect.bottom - 1) - (this.z ? this.c + this.b : 0));
    }

    @DexIgnore
    public final float b(float f2) {
        float f3;
        Rect rect = this.G;
        float height = (float) rect.height();
        int i2 = rect.top;
        if (f2 < ((float) i2)) {
            f3 = 0.0f;
        } else {
            f3 = f2 > ((float) rect.bottom) ? height : f2 - ((float) i2);
        }
        return 360.0f - ((f3 * 360.0f) / height);
    }

    @DexIgnore
    public final int b(int i2) {
        int i3;
        Rect rect = this.H;
        int width = rect.width();
        int i4 = rect.left;
        if (i2 < i4) {
            i3 = 0;
        } else {
            i3 = i2 > rect.right ? width : i2 - i4;
        }
        return 255 - ((i3 * 255) / width);
    }

    @DexIgnore
    public final void b() {
        if (this.z) {
            Rect rect = this.E;
            int i2 = rect.bottom;
            this.H = new Rect(rect.left + 1, (i2 - this.b) + 1, rect.right - 1, i2 - 1);
        }
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        Rect rect;
        if (this.z && (rect = this.H) != null) {
            this.p.setColor(this.C);
            canvas.drawRect((float) (rect.left - 1), (float) (rect.top - 1), (float) (rect.right + 1), (float) (rect.bottom + 1), this.p);
            float[] fArr = {this.w, this.x, this.y};
            int HSVToColor = Color.HSVToColor(fArr);
            int HSVToColor2 = Color.HSVToColor(0, fArr);
            float f2 = (float) rect.left;
            int i2 = rect.top;
            this.s = new LinearGradient(f2, (float) i2, (float) rect.right, (float) i2, HSVToColor, HSVToColor2, Shader.TileMode.CLAMP);
            this.i.setShader(this.s);
            canvas.drawRect(rect, this.i);
            String str = this.A;
            if (str != null && !str.equals("")) {
                canvas.drawText(this.A, (float) rect.centerX(), (float) (rect.centerY() + py5.a(getContext(), 4.0f)), this.j);
            }
            Point a2 = a(this.v);
            RectF rectF = new RectF();
            int i3 = a2.x;
            int i4 = this.f;
            rectF.left = (float) (i3 - (i4 / 2));
            rectF.right = (float) (i3 + (i4 / 2));
            int i5 = rect.top;
            int i6 = this.e;
            rectF.top = (float) (i5 - i6);
            rectF.bottom = (float) (rect.bottom + i6);
            canvas.drawRoundRect(rectF, 2.0f, 2.0f, this.o);
        }
    }

    @DexIgnore
    public final Point a(float f2) {
        Rect rect = this.G;
        float height = (float) rect.height();
        Point point = new Point();
        point.y = (int) ((height - ((f2 * height) / 360.0f)) + ((float) rect.top));
        point.x = rect.left;
        return point;
    }

    @DexIgnore
    public final Point a(int i2) {
        Rect rect = this.H;
        float width = (float) rect.width();
        Point point = new Point();
        point.x = (int) ((width - ((((float) i2) * width) / 255.0f)) + ((float) rect.left));
        point.y = rect.top;
        return point;
    }

    @DexIgnore
    public final float[] a(float f2, float f3) {
        float f4;
        float f5;
        Rect rect = this.F;
        float[] fArr = new float[2];
        float width = (float) rect.width();
        float height = (float) rect.height();
        int i2 = rect.left;
        if (f2 < ((float) i2)) {
            f4 = 0.0f;
        } else {
            f4 = f2 > ((float) rect.right) ? width : f2 - ((float) i2);
        }
        int i3 = rect.top;
        if (f3 < ((float) i3)) {
            f5 = 0.0f;
        } else {
            f5 = f3 > ((float) rect.bottom) ? height : f3 - ((float) i3);
        }
        fArr[0] = (1.0f / width) * f4;
        fArr[1] = 1.0f - ((1.0f / height) * f5);
        return fArr;
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        Point point = this.I;
        if (point == null) {
            return false;
        }
        int i2 = point.x;
        int i3 = point.y;
        if (this.G.contains(i2, i3)) {
            this.w = b(motionEvent.getY());
            return true;
        } else if (this.F.contains(i2, i3)) {
            float[] a2 = a(motionEvent.getX(), motionEvent.getY());
            this.x = a2[0];
            this.y = a2[1];
            return true;
        } else {
            Rect rect = this.H;
            if (rect == null || !rect.contains(i2, i3)) {
                return false;
            }
            this.v = b((int) motionEvent.getX());
            return true;
        }
    }

    @DexIgnore
    public void a(int i2, boolean z2) {
        c cVar;
        int alpha = Color.alpha(i2);
        float[] fArr = new float[3];
        Color.RGBToHSV(Color.red(i2), Color.green(i2), Color.blue(i2), fArr);
        this.v = alpha;
        this.w = fArr[0];
        this.x = fArr[1];
        this.y = fArr[2];
        if (z2 && (cVar = this.J) != null) {
            cVar.q(Color.HSVToColor(this.v, new float[]{this.w, this.x, this.y}));
        }
        invalidate();
    }
}
