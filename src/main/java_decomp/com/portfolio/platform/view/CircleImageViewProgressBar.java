package com.portfolio.platform.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.x24;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CircleImageViewProgressBar extends AppCompatImageView {
    @DexIgnore
    public static /* final */ ImageView.ScaleType J; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public static /* final */ Bitmap.Config K; // = Bitmap.Config.ARGB_8888;
    @DexIgnore
    public float A;
    @DexIgnore
    public ValueAnimator B;
    @DexIgnore
    public ColorFilter C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public boolean E;
    @DexIgnore
    public boolean F;
    @DexIgnore
    public boolean G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public /* final */ Matrix f;
    @DexIgnore
    public /* final */ Paint g;
    @DexIgnore
    public /* final */ Paint h;
    @DexIgnore
    public /* final */ Paint i;
    @DexIgnore
    public /* final */ Paint j;
    @DexIgnore
    public float o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;
    @DexIgnore
    public int s;
    @DexIgnore
    public int t;
    @DexIgnore
    public Bitmap u;
    @DexIgnore
    public BitmapShader v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public float y;
    @DexIgnore
    public float z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v4, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            CircleImageViewProgressBar.this.A = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            CircleImageViewProgressBar.this.invalidate();
        }
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context) {
        super(context);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.p = -16777216;
        this.q = 0;
        this.r = 10;
        this.s = 0;
        this.t = -16776961;
        this.I = true;
        f();
    }

    @DexIgnore
    private int getBorderWidth() {
        return this.q;
    }

    @DexIgnore
    private int getProgressBorderWidth() {
        return this.r;
    }

    @DexIgnore
    public final void a() {
        this.g.setColorFilter(this.C);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        int i2 = this.r;
        int i3 = this.q;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft + ((float) i3), (((float) i2) * 1.5f) + paddingTop + ((float) i3), ((paddingLeft + f2) - (((float) i2) * 1.5f)) - ((float) i3), ((paddingTop + f2) - (((float) i2) * 1.5f)) - ((float) i3));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public final RectF e() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        int i2 = this.r;
        float f2 = (float) min;
        return new RectF((((float) i2) * 1.5f) + paddingLeft, (((float) i2) * 1.5f) + paddingTop, (paddingLeft + f2) - (((float) i2) * 1.5f), (paddingTop + f2) - (((float) i2) * 1.5f));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public final void f() {
        this.B = ValueAnimator.ofFloat(new float[]{0.0f, this.A});
        this.B.setDuration(800);
        this.B.addUpdateListener(new a());
        CircleImageViewProgressBar.super.setScaleType(J);
        this.D = true;
        if (this.E) {
            h();
            this.E = false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public final void g() {
        if (this.H) {
            this.u = null;
        } else {
            this.u = a(getDrawable());
        }
        h();
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.C;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.s;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return J;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public final void h() {
        int i2;
        if (!this.D) {
            this.E = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            Bitmap bitmap = this.u;
            if (bitmap == null) {
                invalidate();
                return;
            }
            Shader.TileMode tileMode = Shader.TileMode.CLAMP;
            this.v = new BitmapShader(bitmap, tileMode, tileMode);
            this.g.setAntiAlias(true);
            this.g.setShader(this.v);
            this.h.setStyle(Paint.Style.STROKE);
            this.h.setAntiAlias(true);
            this.h.setColor(this.p);
            this.h.setStrokeWidth((float) this.q);
            this.h.setStrokeCap(Paint.Cap.ROUND);
            this.i.setStyle(Paint.Style.STROKE);
            this.i.setAntiAlias(true);
            this.i.setColor(this.t);
            this.i.setStrokeWidth((float) this.r);
            this.i.setStrokeCap(Paint.Cap.ROUND);
            this.j.setStyle(Paint.Style.FILL);
            this.j.setAntiAlias(true);
            this.j.setColor(this.s);
            this.x = this.u.getHeight();
            this.w = this.u.getWidth();
            this.d.set(d());
            this.e.set(e());
            this.c.set(this.e);
            if (!this.F && (i2 = this.r) > 0) {
                this.c.inset((float) (i2 / 2), (float) (i2 / 2));
            }
            this.z = Math.min(this.c.height() / 2.0f, this.c.width() / 2.0f);
            if (this.y > 1.0f) {
                this.y = 1.0f;
            }
            this.z *= this.y;
            a();
            i();
            invalidate();
        }
    }

    @DexIgnore
    public final void i() {
        float f2;
        float f3;
        this.f.set((Matrix) null);
        float f4 = 0.0f;
        if (((float) this.w) * this.c.height() > this.c.width() * ((float) this.x)) {
            f3 = this.c.height() / ((float) this.x);
            f2 = (this.c.width() - (((float) this.w) * f3)) * 0.5f;
        } else {
            f3 = this.c.width() / ((float) this.w);
            f4 = (this.c.height() - (((float) this.x) * f3)) * 0.5f;
            f2 = 0.0f;
        }
        this.f.setScale(f3, f3);
        Matrix matrix = this.f;
        RectF rectF = this.c;
        matrix.postTranslate(((float) ((int) (f2 + 0.5f))) + rectF.left, ((float) ((int) (f4 + 0.5f))) + rectF.top);
        BitmapShader bitmapShader = this.v;
        if (bitmapShader != null) {
            bitmapShader.setLocalMatrix(this.f);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r8v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void onDraw(Canvas canvas) {
        if (this.H) {
            CircleImageViewProgressBar.super.onDraw(canvas);
        } else if (this.u != null) {
            canvas.save();
            canvas.rotate(this.o - 90.0f, this.c.centerX(), this.c.centerY());
            int i2 = this.q;
            if (i2 > 0 && i2 < 10) {
                this.h.setColor(this.p);
                canvas.drawArc(this.d, 0.0f, 360.0f, false, this.h);
            }
            this.h.setColor(this.p);
            this.i.setColor(this.t);
            float f2 = (this.A / 100.0f) * 360.0f;
            RectF rectF = this.e;
            if (this.G) {
                f2 = -f2;
            }
            canvas.drawArc(rectF, 0.0f, f2, false, this.i);
            canvas.restore();
            canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.z, this.g);
            if (this.s != 0) {
                canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.z, this.j);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        CircleImageViewProgressBar.super.onSizeChanged(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z2) {
        if (z2) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setBorderColor(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            this.h.setColor(this.p);
            invalidate();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setBorderProgressColor(int i2) {
        if (i2 != this.t) {
            this.t = i2;
            this.i.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.q) {
            this.q = i2;
            h();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.C)) {
            this.C = colorFilter;
            a();
            invalidate();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.s) {
            this.s = i2;
            this.j.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setImageBitmap(Bitmap bitmap) {
        CircleImageViewProgressBar.super.setImageBitmap(bitmap);
        g();
    }

    @DexIgnore
    public void setImageDrawable(Drawable drawable) {
        CircleImageViewProgressBar.super.setImageDrawable(drawable);
        g();
    }

    @DexIgnore
    public void setImageResource(int i2) {
        CircleImageViewProgressBar.super.setImageResource(i2);
        g();
    }

    @DexIgnore
    public void setImageURI(Uri uri) {
        CircleImageViewProgressBar.super.setImageURI(uri);
        g();
    }

    @DexIgnore
    public void setInnrCircleDiammeter(float f2) {
        this.y = f2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setPadding(int i2, int i3, int i4, int i5) {
        CircleImageViewProgressBar.super.setPadding(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        CircleImageViewProgressBar.super.setPaddingRelative(i2, i3, i4, i5);
        h();
    }

    @DexIgnore
    public void setProgressBorderWidth(int i2) {
        if (i2 != this.r) {
            this.r = i2;
            h();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.widget.ImageView, com.portfolio.platform.view.CircleImageViewProgressBar] */
    public void setProgressValue(float f2) {
        if (this.I) {
            if (this.B.isRunning()) {
                this.B.cancel();
            }
            this.B.setFloatValues(new float[]{this.A, f2});
            this.B.start();
            return;
        }
        this.A = f2;
        invalidate();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != J) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", new Object[]{scaleType}));
        }
    }

    @DexIgnore
    public final Bitmap a(Drawable drawable) {
        Bitmap bitmap;
        if (drawable == null) {
            return null;
        }
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }
        try {
            if (drawable instanceof ColorDrawable) {
                bitmap = Bitmap.createBitmap(2, 2, K);
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), K);
            }
            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return bitmap;
        } catch (Exception e2) {
            e2.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public CircleImageViewProgressBar(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint(1);
        this.h = new Paint(1);
        this.i = new Paint(1);
        this.j = new Paint(1);
        this.p = -16777216;
        this.q = 0;
        this.r = 10;
        this.s = 0;
        this.t = -16776961;
        this.I = true;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.CircleImageViewProgressBar, i2, 0);
        this.r = obtainStyledAttributes.getDimensionPixelSize(5, 0);
        this.q = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.p = obtainStyledAttributes.getColor(3, -16777216);
        this.F = obtainStyledAttributes.getBoolean(4, false);
        this.G = obtainStyledAttributes.getBoolean(1, false);
        this.s = obtainStyledAttributes.getColor(7, 0);
        this.y = obtainStyledAttributes.getFloat(0, 0.805f);
        this.t = obtainStyledAttributes.getColor(6, -16776961);
        this.o = obtainStyledAttributes.getFloat(8, 0.0f);
        obtainStyledAttributes.recycle();
        f();
    }
}
