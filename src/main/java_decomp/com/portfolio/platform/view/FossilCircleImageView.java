package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.AppCompatImageView;
import com.fossil.cx5;
import com.fossil.fr;
import com.fossil.ij4;
import com.fossil.jj4;
import com.fossil.mj4;
import com.fossil.mt;
import com.fossil.mz;
import com.fossil.nz;
import com.fossil.pr;
import com.fossil.x24;
import com.fossil.xj4;
import com.fossil.yz;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.Objects;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FossilCircleImageView extends AppCompatImageView {
    @DexIgnore
    public static /* final */ String v; // = FossilCircleImageView.class.getSimpleName();
    @DexIgnore
    public static /* final */ ImageView.ScaleType w; // = ImageView.ScaleType.CENTER_CROP;
    @DexIgnore
    public /* final */ RectF c;
    @DexIgnore
    public /* final */ RectF d;
    @DexIgnore
    public /* final */ Paint e;
    @DexIgnore
    public /* final */ Paint f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;
    @DexIgnore
    public String j;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public ColorFilter q;
    @DexIgnore
    public boolean r;
    @DexIgnore
    public boolean s;
    @DexIgnore
    public boolean t;
    @DexIgnore
    public boolean u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements mz<Drawable> {
        @DexIgnore
        public /* final */ /* synthetic */ mj4 a;
        @DexIgnore
        public /* final */ /* synthetic */ String b;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.view.FossilCircleImageView$a$a")
        /* renamed from: com.portfolio.platform.view.FossilCircleImageView$a$a  reason: collision with other inner class name */
        public class C0067a implements Runnable {
            @DexIgnore
            public C0067a() {
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r1v4, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
            public void run() {
                a aVar = a.this;
                aVar.a.a((Object) new ij4("", aVar.b)).a(new nz().a(new xj4())).a(FossilCircleImageView.this);
            }
        }

        @DexIgnore
        public a(mj4 mj4, String str) {
            this.a = mj4;
            this.b = str;
        }

        @DexIgnore
        public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
            return false;
        }

        @DexIgnore
        public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
            new Handler(Looper.getMainLooper()).post(new C0067a());
            return true;
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context) {
        super(context);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new Paint();
        this.f = new Paint();
        this.g = -16777216;
        this.h = 0;
        this.i = 0;
        e();
    }

    @DexIgnore
    public final void a() {
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2});
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public final RectF d() {
        int width = (getWidth() - getPaddingLeft()) - getPaddingRight();
        int height = (getHeight() - getPaddingTop()) - getPaddingBottom();
        int min = Math.min(width, height);
        float paddingLeft = ((float) getPaddingLeft()) + (((float) (width - min)) / 2.0f);
        float paddingTop = ((float) getPaddingTop()) + (((float) (height - min)) / 2.0f);
        float f2 = (float) min;
        return new RectF(paddingLeft, paddingTop, paddingLeft + f2, f2 + paddingTop);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public final void e() {
        FossilCircleImageView.super.setScaleType(w);
        this.r = true;
        if (this.s) {
            f();
            this.s = false;
        }
        FLogger.INSTANCE.getLocal().d(v, "init()");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public final void f() {
        int i2;
        if (!this.r) {
            this.s = true;
        } else if (getWidth() != 0 || getHeight() != 0) {
            this.e.setStyle(Paint.Style.STROKE);
            this.e.setAntiAlias(true);
            this.e.setColor(this.g);
            this.e.setStrokeWidth((float) this.h);
            this.f.setStyle(Paint.Style.FILL);
            this.f.setAntiAlias(true);
            this.f.setColor(this.i);
            this.f.setFilterBitmap(true);
            this.f.setDither(true);
            this.d.set(d());
            this.p = Math.min((this.d.height() - ((float) this.h)) / 2.0f, (this.d.width() - ((float) this.h)) / 2.0f);
            this.c.set(this.d);
            if (!this.t && (i2 = this.h) > 0) {
                this.c.inset(((float) i2) - 1.0f, ((float) i2) - 1.0f);
            }
            this.o = Math.min(this.c.height() / 2.0f, this.c.width() / 2.0f);
            a();
            g();
            invalidate();
        }
    }

    @DexIgnore
    public final void g() {
    }

    @DexIgnore
    public int getBorderColor() {
        return this.g;
    }

    @DexIgnore
    public int getBorderWidth() {
        return this.h;
    }

    @DexIgnore
    public ColorFilter getColorFilter() {
        return this.q;
    }

    @DexIgnore
    @Deprecated
    public int getFillColor() {
        return this.i;
    }

    @DexIgnore
    public String getHandNumber() {
        return this.j;
    }

    @DexIgnore
    public ImageView.ScaleType getScaleType() {
        return w;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void onDraw(Canvas canvas) {
        FossilCircleImageView.super.onDraw(canvas);
        if (this.i != 0) {
            canvas.drawCircle(this.c.centerX(), this.c.centerY(), this.o, this.f);
        }
        if (this.h > 0) {
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.p, this.e);
        }
        if (this.u) {
            this.e.setAntiAlias(true);
            this.e.setFilterBitmap(true);
            this.e.setDither(true);
            this.e.setColor(Color.parseColor("#EEEEEE"));
            canvas.drawCircle(this.d.centerX(), this.d.centerY(), this.p, this.e);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        FossilCircleImageView.super.onSizeChanged(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setAdjustViewBounds(boolean z) {
        if (z) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void setBorderColor(int i2) {
        if (i2 != this.g) {
            this.g = i2;
            this.e.setColor(this.g);
            invalidate();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    @Deprecated
    public void setBorderColorResource(int i2) {
        setBorderColor(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setBorderWidth(int i2) {
        if (i2 != this.h) {
            this.h = i2;
            f();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void setColorFilter(ColorFilter colorFilter) {
        if (!Objects.equals(colorFilter, this.q)) {
            this.q = colorFilter;
            a();
            invalidate();
        }
    }

    @DexIgnore
    public void setDefault(boolean z) {
        this.u = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    @Deprecated
    public void setFillColor(int i2) {
        if (i2 != this.i) {
            this.i = i2;
            this.f.setColor(i2);
            invalidate();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    @Deprecated
    public void setFillColorResource(int i2) {
        setColorFilter(getContext().getResources().getColor(i2));
    }

    @DexIgnore
    public void setHandNumber(String str) {
        this.j = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void setPadding(int i2, int i3, int i4, int i5) {
        FossilCircleImageView.super.setPadding(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void setPaddingRelative(int i2, int i3, int i4, int i5) {
        FossilCircleImageView.super.setPaddingRelative(i2, i3, i4, i5);
        f();
    }

    @DexIgnore
    public void setScaleType(ImageView.ScaleType scaleType) {
        if (scaleType != w) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", new Object[]{scaleType}));
        }
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2, bitmap3});
    }

    @DexIgnore
    public void a(Bitmap bitmap, Bitmap bitmap2, Bitmap bitmap3, Bitmap bitmap4) {
        new b(new WeakReference(this)).execute(new Bitmap[]{bitmap, bitmap2, bitmap3, bitmap4});
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void a(mj4 mj4, String str, String str2) {
        mj4.a(str).a(new nz().a(new xj4())).b(new a(mj4, str2)).a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void a(String str, fr frVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = v;
        local.d(str2, "setImageUrl without defaultName url=" + str);
        frVar.a(str).a(new nz().a(new xj4())).a(this);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends AsyncTask<Bitmap, Void, Bitmap> {
        @DexIgnore
        public WeakReference<FossilCircleImageView> a;

        @DexIgnore
        public b(WeakReference<FossilCircleImageView> weakReference) {
            this.a = weakReference;
        }

        @DexIgnore
        /* renamed from: a */
        public Bitmap doInBackground(Bitmap... bitmapArr) {
            int length = bitmapArr.length;
            if (length == 1) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 0");
                return bitmapArr[0];
            } else if (length == 2) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 2");
                return cx5.a(bitmapArr[0], bitmapArr[1], 140);
            } else if (length == 3) {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 3");
                return cx5.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], 140);
            } else if (length != 4) {
                return null;
            } else {
                FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "doInBackground bitmap list = 4");
                return cx5.a(bitmapArr[0], bitmapArr[1], bitmapArr[2], bitmapArr[3], 140);
            }
        }

        @DexIgnore
        public void onPreExecute() {
            super.onPreExecute();
        }

        @DexIgnore
        /* renamed from: a */
        public void onPostExecute(Bitmap bitmap) {
            FLogger.INSTANCE.getLocal().d(FossilCircleImageView.v, "onPostExecute");
            if (this.a.get() != null) {
                ((FossilCircleImageView) this.a.get()).setImageBitmap(bitmap);
            }
        }
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public FossilCircleImageView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.c = new RectF();
        this.d = new RectF();
        this.e = new Paint();
        this.f = new Paint();
        this.g = -16777216;
        this.h = 0;
        this.i = 0;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, x24.FossilCircleImageView, i2, 0);
        this.h = obtainStyledAttributes.getDimensionPixelSize(2, 0);
        this.g = obtainStyledAttributes.getColor(0, -16777216);
        this.t = obtainStyledAttributes.getBoolean(1, false);
        this.i = obtainStyledAttributes.getColor(3, 0);
        this.j = obtainStyledAttributes.getString(4);
        if (this.j == null) {
            this.j = "";
        }
        obtainStyledAttributes.recycle();
        FLogger.INSTANCE.getLocal().d(v, "FossilCircleImageView constructor");
        e();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.ImageView, android.view.View, com.portfolio.platform.view.FossilCircleImageView] */
    public void a(String str, String str2, fr frVar) {
        jj4.a((View) this).a((Object) new ij4(str, str2)).a(new nz().a(new xj4())).a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void a(Bitmap bitmap, fr frVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        frVar.a(byteArrayOutputStream.toByteArray()).a(new nz().a(new xj4()).c()).a(this);
        try {
            byteArrayOutputStream.close();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        this.u = false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.widget.ImageView, com.portfolio.platform.view.FossilCircleImageView] */
    public void a(int i2, fr frVar) {
        frVar.a(Integer.valueOf(i2)).a(new nz().a(new xj4())).a(this);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = v;
        local.d(str, "setImageResource resId = " + i2);
    }
}
