package com.portfolio.platform.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import com.fossil.wg6;
import com.fossil.x24;
import com.google.android.material.textfield.TextInputEditText;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FlexibleTextInputEditText extends TextInputEditText {
    @DexIgnore
    public String d; // = "";
    @DexIgnore
    public String e; // = "";

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context) {
        super(context);
        wg6.b(context, "context");
        a((AttributeSet) null);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public final void a(AttributeSet attributeSet) {
        if (attributeSet != null) {
            TypedArray obtainStyledAttributes = getContext().obtainStyledAttributes(attributeSet, x24.FlexibleTextInputEditText);
            String string = obtainStyledAttributes.getString(0);
            if (string == null) {
                string = "";
            }
            this.d = string;
            String string2 = obtainStyledAttributes.getString(1);
            if (string2 == null) {
                string2 = "";
            }
            this.e = string2;
            obtainStyledAttributes.recycle();
        }
        if (!TextUtils.isEmpty(this.d) || !TextUtils.isEmpty(this.e)) {
            a(this.d, this.e);
        }
        setElevation(0.0f);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public FlexibleTextInputEditText(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        wg6.b(context, "context");
        wg6.b(attributeSet, "attrs");
        a(attributeSet);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.widget.EditText, com.portfolio.platform.view.FlexibleTextInputEditText] */
    public final void a(String str, String str2) {
        String b = ThemeManager.l.a().b(str);
        Typeface c = ThemeManager.l.a().c(str2);
        if (b != null) {
            setTextColor(Color.parseColor(b));
            setHintTextColor(Color.parseColor(b));
            setLinkTextColor(Color.parseColor(b));
        }
        if (c != null) {
            setTypeface(c);
        }
    }
}
