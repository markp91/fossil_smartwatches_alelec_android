package com.portfolio.platform.workers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.microapp.CommuteTimeService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class TimeChangeReceiver extends BroadcastReceiver {
    @DexIgnore
    public AlarmHelper a;
    @DexIgnore
    public an4 b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ TimeChangeReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(xe6 xe6, TimeChangeReceiver timeChangeReceiver) {
            super(2, xe6);
            this.this$0 = timeChangeReceiver;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(xe6, this.this$0);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                String e = PortfolioApp.get.instance().e();
                if (e.length() == 0) {
                    return cd6.a;
                }
                long g = this.this$0.b().g(e);
                long e2 = this.this$0.b().e(e);
                long currentTimeMillis = System.currentTimeMillis();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("TimeChangeReceiver", "currentTime = " + currentTimeMillis + "; lastSyncTimeSuccess = " + g + "; " + "lastReminderSyncTimeSuccess = " + e2);
                if (e2 > currentTimeMillis || g > currentTimeMillis) {
                    an4 b = this.this$0.b();
                    long j = currentTimeMillis - ((long) CommuteTimeService.y);
                    b.c(j, e);
                    this.this$0.b().a(j, e);
                    return cd6.a;
                }
                AlarmHelper a = this.this$0.a();
                Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
                wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
                a.e(applicationContext);
                AlarmHelper a2 = this.this$0.a();
                Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
                wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
                a2.d(applicationContext2);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public TimeChangeReceiver() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final AlarmHelper a() {
        AlarmHelper alarmHelper = this.a;
        if (alarmHelper != null) {
            return alarmHelper;
        }
        wg6.d("mAlarmHelper");
        throw null;
    }

    @DexIgnore
    public final an4 b() {
        an4 an4 = this.b;
        if (an4 != null) {
            return an4;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        String action;
        FLogger.INSTANCE.getLocal().d("TimeChangeReceiver", "onReceive()");
        if (intent != null && (action = intent.getAction()) != null) {
            if (wg6.a((Object) action, (Object) "android.intent.action.TIME_SET") || wg6.a((Object) action, (Object) "android.intent.action.TIMEZONE_CHANGED") || wg6.a((Object) action, (Object) "android.intent.action.TIME_TICK")) {
                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b((xe6) null, this), 3, (Object) null);
            }
        }
    }
}
