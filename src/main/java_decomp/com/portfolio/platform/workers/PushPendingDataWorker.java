package com.portfolio.platform.workers;

import android.content.Context;
import androidx.work.CoroutineWorker;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.bm;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.e06;
import com.fossil.ef6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jh6;
import com.fossil.jl6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.lk6;
import com.fossil.ll6;
import com.fossil.mc6;
import com.fossil.mk6;
import com.fossil.nc6;
import com.fossil.nf6;
import com.fossil.nl;
import com.fossil.qg6;
import com.fossil.ql;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.ul;
import com.fossil.vl;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.FileRepository;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThirdPartyRepository;
import com.portfolio.platform.data.source.remote.ApiResponse;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.inject.Provider;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PushPendingDataWorker extends CoroutineWorker {
    @DexIgnore
    public static /* final */ a z; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ActivitiesRepository g;
    @DexIgnore
    public /* final */ SummariesRepository h;
    @DexIgnore
    public /* final */ SleepSessionsRepository i;
    @DexIgnore
    public /* final */ SleepSummariesRepository j;
    @DexIgnore
    public /* final */ GoalTrackingRepository o;
    @DexIgnore
    public /* final */ HeartRateSampleRepository p;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository q;
    @DexIgnore
    public /* final */ FitnessDataRepository r;
    @DexIgnore
    public /* final */ AlarmsRepository s;
    @DexIgnore
    public /* final */ an4 t;
    @DexIgnore
    public /* final */ DianaPresetRepository u;
    @DexIgnore
    public /* final */ HybridPresetRepository v;
    @DexIgnore
    public /* final */ ThirdPartyRepository w;
    @DexIgnore
    public /* final */ FileRepository x;
    @DexIgnore
    public /* final */ PortfolioApp y;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final void a() {
            vl.a aVar = new vl.a(PushPendingDataWorker.class);
            nl.a aVar2 = new nl.a();
            aVar2.a(ul.CONNECTED);
            nl a = aVar2.a();
            wg6.a((Object) a, "Constraints.Builder().se\u2026rkType.CONNECTED).build()");
            aVar.a(a);
            vl a2 = aVar.a();
            wg6.a((Object) a2, "uploadBuilder.build()");
            vl vlVar = a2;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("PushPendingDataWorker", "startScheduleUploadPendingData() - id = " + vlVar.a());
            bm.a().a("PushPendingDataWorker", ql.KEEP, vlVar);
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements e06<PushPendingDataWorker> {
        @DexIgnore
        public /* final */ Provider<ActivitiesRepository> a;
        @DexIgnore
        public /* final */ Provider<SummariesRepository> b;
        @DexIgnore
        public /* final */ Provider<SleepSessionsRepository> c;
        @DexIgnore
        public /* final */ Provider<SleepSummariesRepository> d;
        @DexIgnore
        public /* final */ Provider<GoalTrackingRepository> e;
        @DexIgnore
        public /* final */ Provider<HeartRateSampleRepository> f;
        @DexIgnore
        public /* final */ Provider<HeartRateSummaryRepository> g;
        @DexIgnore
        public /* final */ Provider<FitnessDataRepository> h;
        @DexIgnore
        public /* final */ Provider<AlarmsRepository> i;
        @DexIgnore
        public /* final */ Provider<an4> j;
        @DexIgnore
        public /* final */ Provider<DianaPresetRepository> k;
        @DexIgnore
        public /* final */ Provider<HybridPresetRepository> l;
        @DexIgnore
        public /* final */ Provider<ThirdPartyRepository> m;
        @DexIgnore
        public /* final */ Provider<FileRepository> n;
        @DexIgnore
        public /* final */ Provider<PortfolioApp> o;

        @DexIgnore
        public b(Provider<ActivitiesRepository> provider, Provider<SummariesRepository> provider2, Provider<SleepSessionsRepository> provider3, Provider<SleepSummariesRepository> provider4, Provider<GoalTrackingRepository> provider5, Provider<HeartRateSampleRepository> provider6, Provider<HeartRateSummaryRepository> provider7, Provider<FitnessDataRepository> provider8, Provider<AlarmsRepository> provider9, Provider<an4> provider10, Provider<DianaPresetRepository> provider11, Provider<HybridPresetRepository> provider12, Provider<ThirdPartyRepository> provider13, Provider<FileRepository> provider14, Provider<PortfolioApp> provider15) {
            Provider<ActivitiesRepository> provider16 = provider;
            Provider<SummariesRepository> provider17 = provider2;
            Provider<SleepSessionsRepository> provider18 = provider3;
            Provider<SleepSummariesRepository> provider19 = provider4;
            Provider<GoalTrackingRepository> provider20 = provider5;
            Provider<HeartRateSampleRepository> provider21 = provider6;
            Provider<HeartRateSummaryRepository> provider22 = provider7;
            Provider<FitnessDataRepository> provider23 = provider8;
            Provider<AlarmsRepository> provider24 = provider9;
            Provider<an4> provider25 = provider10;
            Provider<DianaPresetRepository> provider26 = provider11;
            Provider<HybridPresetRepository> provider27 = provider12;
            Provider<ThirdPartyRepository> provider28 = provider13;
            Provider<FileRepository> provider29 = provider14;
            Provider<PortfolioApp> provider30 = provider15;
            wg6.b(provider16, "mActivitiesRepository");
            wg6.b(provider17, "mSummariesRepository");
            wg6.b(provider18, "mSleepSessionRepository");
            wg6.b(provider19, "mSleepSummariesRepository");
            wg6.b(provider20, "mGoalTrackingRepository");
            wg6.b(provider21, "mHeartRateSampleRepository");
            wg6.b(provider22, "mHeartRateSummaryRepository");
            wg6.b(provider23, "mFitnessDataRepository");
            wg6.b(provider24, "mAlarmsRepository");
            wg6.b(provider25, "mSharedPreferencesManager");
            wg6.b(provider26, "mDianaPresetRepository");
            wg6.b(provider27, "mHybridPresetRepository");
            wg6.b(provider28, "mThirdPartyRepository");
            wg6.b(provider29, "mFileRepository");
            wg6.b(provider30, "mApp");
            this.a = provider16;
            this.b = provider17;
            this.c = provider18;
            this.d = provider19;
            this.e = provider20;
            this.f = provider21;
            this.g = provider22;
            this.h = provider23;
            this.i = provider24;
            this.j = provider25;
            this.k = provider26;
            this.l = provider27;
            this.m = provider28;
            this.n = provider29;
            this.o = provider30;
        }

        @DexIgnore
        public PushPendingDataWorker a(Context context, WorkerParameters workerParameters) {
            wg6.b(context, "context");
            wg6.b(workerParameters, "parameterName");
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "Factory - create()");
            ActivitiesRepository activitiesRepository = this.a.get();
            wg6.a((Object) activitiesRepository, "mActivitiesRepository.get()");
            SummariesRepository summariesRepository = this.b.get();
            wg6.a((Object) summariesRepository, "mSummariesRepository.get()");
            SleepSessionsRepository sleepSessionsRepository = this.c.get();
            wg6.a((Object) sleepSessionsRepository, "mSleepSessionRepository.get()");
            SleepSummariesRepository sleepSummariesRepository = this.d.get();
            wg6.a((Object) sleepSummariesRepository, "mSleepSummariesRepository.get()");
            GoalTrackingRepository goalTrackingRepository = this.e.get();
            wg6.a((Object) goalTrackingRepository, "mGoalTrackingRepository.get()");
            HeartRateSampleRepository heartRateSampleRepository = this.f.get();
            wg6.a((Object) heartRateSampleRepository, "mHeartRateSampleRepository.get()");
            HeartRateSummaryRepository heartRateSummaryRepository = this.g.get();
            wg6.a((Object) heartRateSummaryRepository, "mHeartRateSummaryRepository.get()");
            FitnessDataRepository fitnessDataRepository = this.h.get();
            wg6.a((Object) fitnessDataRepository, "mFitnessDataRepository.get()");
            AlarmsRepository alarmsRepository = this.i.get();
            wg6.a((Object) alarmsRepository, "mAlarmsRepository.get()");
            an4 an4 = this.j.get();
            wg6.a((Object) an4, "mSharedPreferencesManager.get()");
            DianaPresetRepository dianaPresetRepository = this.k.get();
            wg6.a((Object) dianaPresetRepository, "mDianaPresetRepository.get()");
            HybridPresetRepository hybridPresetRepository = this.l.get();
            wg6.a((Object) hybridPresetRepository, "mHybridPresetRepository.get()");
            ThirdPartyRepository thirdPartyRepository = this.m.get();
            wg6.a((Object) thirdPartyRepository, "mThirdPartyRepository.get()");
            FileRepository fileRepository = this.n.get();
            wg6.a((Object) fileRepository, "mFileRepository.get()");
            PortfolioApp portfolioApp = this.o.get();
            wg6.a((Object) portfolioApp, "mApp.get()");
            return new PushPendingDataWorker(workerParameters, activitiesRepository, summariesRepository, sleepSessionsRepository, sleepSummariesRepository, goalTrackingRepository, heartRateSampleRepository, heartRateSummaryRepository, fitnessDataRepository, alarmsRepository, an4, dianaPresetRepository, hybridPresetRepository, thirdPartyRepository, fileRepository, portfolioApp);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {77}, m = "doWork")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(PushPendingDataWorker pushPendingDataWorker, xe6 xe6) {
            super(xe6);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super ListenableWorker.a>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ lk6 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements ActivitiesRepository.PushPendingActivitiesCallback {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$d$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$d$a$a  reason: collision with other inner class name */
            public static final class C0073a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ jh6 $endDate;
                @DexIgnore
                public /* final */ /* synthetic */ jh6 $startDate;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0073a(a aVar, jh6 jh6, jh6 jh62, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$startDate = jh6;
                    this.$endDate = jh62;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0073a aVar = new C0073a(this.this$0, this.$startDate, this.$endDate, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0073a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        SummariesRepository h = this.this$0.a.this$0.h;
                        Date date = ((DateTime) this.$startDate.element).toLocalDateTime().toDate();
                        wg6.a((Object) date, "startDate.toLocalDateTime().toDate()");
                        Date date2 = ((DateTime) this.$endDate.element).toLocalDateTime().toDate();
                        wg6.a((Object) date2, "endDate.toLocalDateTime().toDate()");
                        this.L$0 = il6;
                        this.label = 1;
                        if (h.loadSummaries(date, date2, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    return cd6.a;
                }
            }

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    lk6 lk6 = this.a.$cancellableContinuation;
                    mc6.a aVar = mc6.Companion;
                    lk6.resumeWith(mc6.m1constructorimpl(false));
                }
            }

            @DexIgnore
            public void onSuccess(List<ActivitySample> list) {
                wg6.b(list, "activityList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "ActivitiesRepository.pushPendingActivities onSuccess, go to next");
                if (!list.isEmpty()) {
                    jh6 jh6 = new jh6();
                    jh6.element = list.get(0).getStartTime();
                    jh6 jh62 = new jh6();
                    jh62.element = list.get(0).getEndTime();
                    for (ActivitySample next : list) {
                        if (next.getStartTime().getMillis() < ((DateTime) jh6.element).getMillis()) {
                            jh6.element = next.getStartTime();
                        }
                        if (next.getEndTime().getMillis() < ((DateTime) jh62.element).getMillis()) {
                            jh62.element = next.getEndTime();
                        }
                    }
                    rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new C0073a(this, jh6, jh62, (xe6) null), 3, (Object) null);
                }
                if (this.a.$cancellableContinuation.isActive()) {
                    lk6 lk6 = this.a.$cancellableContinuation;
                    mc6.a aVar = mc6.Companion;
                    lk6.resumeWith(mc6.m1constructorimpl(true));
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(lk6 lk6, xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.$cancellableContinuation, xe6, this.this$0);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ActivitiesRepository a3 = this.this$0.g;
                a aVar = new a(this);
                this.L$0 = il6;
                this.label = 1;
                if (a3.pushPendingActivities(aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ lk6 $cancellableContinuation;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements SleepSessionsRepository.PushPendingSleepSessionsCallback {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a  reason: collision with other inner class name */
            public static final class C0074a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $sleepSessionList;
                @DexIgnore
                public int I$0;
                @DexIgnore
                public int I$1;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$e$a$a$a  reason: collision with other inner class name */
                public static final class C0075a extends sf6 implements ig6<il6, xe6<? super ap4<ku3>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $end;
                    @DexIgnore
                    public /* final */ /* synthetic */ Calendar $start;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0074a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0075a(C0074a aVar, Calendar calendar, Calendar calendar2, xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aVar;
                        this.$start = calendar;
                        this.$end = calendar2;
                    }

                    @DexIgnore
                    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                        wg6.b(xe6, "completion");
                        C0075a aVar = new C0075a(this.this$0, this.$start, this.$end, xe6);
                        aVar.p$ = (il6) obj;
                        return aVar;
                    }

                    @DexIgnore
                    public final Object invoke(Object obj, Object obj2) {
                        return ((C0075a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                    }

                    @DexIgnore
                    public final Object invokeSuspend(Object obj) {
                        Object a = ff6.a();
                        int i = this.label;
                        if (i == 0) {
                            nc6.a(obj);
                            il6 il6 = this.p$;
                            SleepSummariesRepository g = this.this$0.this$0.a.this$0.j;
                            Calendar calendar = this.$start;
                            wg6.a((Object) calendar, "start");
                            Date time = calendar.getTime();
                            wg6.a((Object) time, "start.time");
                            Calendar calendar2 = this.$end;
                            wg6.a((Object) calendar2, "end");
                            Date time2 = calendar2.getTime();
                            wg6.a((Object) time2, "end.time");
                            this.L$0 = il6;
                            this.label = 1;
                            obj = g.fetchSleepSummaries(time, time2, this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            il6 il62 = (il6) this.L$0;
                            nc6.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0074a(a aVar, List list, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$sleepSessionList = list;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0074a aVar = new C0074a(this.this$0, this.$sleepSessionList, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0074a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        if (!this.$sleepSessionList.isEmpty()) {
                            int realStartTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealStartTime();
                            int realEndTime = ((MFSleepSession) this.$sleepSessionList.get(0)).getRealEndTime();
                            for (MFSleepSession mFSleepSession : this.$sleepSessionList) {
                                if (mFSleepSession.getRealStartTime() < realStartTime) {
                                    realStartTime = mFSleepSession.getRealStartTime();
                                }
                                if (mFSleepSession.getRealEndTime() > realEndTime) {
                                    realEndTime = mFSleepSession.getRealEndTime();
                                }
                            }
                            Calendar instance = Calendar.getInstance();
                            wg6.a((Object) instance, "start");
                            instance.setTimeInMillis((long) realStartTime);
                            Calendar instance2 = Calendar.getInstance();
                            wg6.a((Object) instance2, "end");
                            instance2.setTimeInMillis((long) realEndTime);
                            dl6 b = zl6.b();
                            C0075a aVar = new C0075a(this, instance, instance2, (xe6) null);
                            this.L$0 = il6;
                            this.I$0 = realStartTime;
                            this.I$1 = realEndTime;
                            this.L$1 = instance;
                            this.L$2 = instance2;
                            this.label = 1;
                            if (gk6.a(b, aVar, this) == a) {
                                return a;
                            }
                        }
                    } else if (i == 1) {
                        Calendar calendar = (Calendar) this.L$2;
                        Calendar calendar2 = (Calendar) this.L$1;
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        lk6 lk6 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = hf6.a(true);
                        mc6.a aVar2 = mc6.Companion;
                        lk6.resumeWith(mc6.m1constructorimpl(a2));
                    }
                    return cd6.a;
                }
            }

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    lk6 lk6 = this.a.$cancellableContinuation;
                    mc6.a aVar = mc6.Companion;
                    lk6.resumeWith(mc6.m1constructorimpl(true));
                }
            }

            @DexIgnore
            public void onSuccess(List<MFSleepSession> list) {
                wg6.b(list, "sleepSessionList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingSleepSessions onSuccess, go to next");
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new C0074a(this, list, (xe6) null), 3, (Object) null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(lk6 lk6, xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.$cancellableContinuation, xe6, this.this$0);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.i.pushPendingSleepSessions(new a(this));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ lk6 $cancellableContinuation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements GoalTrackingRepository.PushPendingGoalTrackingDataListCallback {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a")
            /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a  reason: collision with other inner class name */
            public static final class C0076a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
                @DexIgnore
                public /* final */ /* synthetic */ List $goalTrackingList;
                @DexIgnore
                public Object L$0;
                @DexIgnore
                public Object L$1;
                @DexIgnore
                public Object L$2;
                @DexIgnore
                public int label;
                @DexIgnore
                public il6 p$;
                @DexIgnore
                public /* final */ /* synthetic */ a this$0;

                @DexEdit(defaultAction = DexAction.IGNORE, target = "com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a")
                /* renamed from: com.portfolio.platform.workers.PushPendingDataWorker$f$a$a$a  reason: collision with other inner class name */
                public static final class C0077a extends sf6 implements ig6<il6, xe6<? super ap4<ApiResponse<GoalDailySummary>>>, Object> {
                    @DexIgnore
                    public /* final */ /* synthetic */ jh6 $endDate;
                    @DexIgnore
                    public /* final */ /* synthetic */ jh6 $startDate;
                    @DexIgnore
                    public Object L$0;
                    @DexIgnore
                    public int label;
                    @DexIgnore
                    public il6 p$;
                    @DexIgnore
                    public /* final */ /* synthetic */ C0076a this$0;

                    @DexIgnore
                    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                    public C0077a(C0076a aVar, jh6 jh6, jh6 jh62, xe6 xe6) {
                        super(2, xe6);
                        this.this$0 = aVar;
                        this.$startDate = jh6;
                        this.$endDate = jh62;
                    }

                    @DexIgnore
                    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                        wg6.b(xe6, "completion");
                        C0077a aVar = new C0077a(this.this$0, this.$startDate, this.$endDate, xe6);
                        aVar.p$ = (il6) obj;
                        return aVar;
                    }

                    @DexIgnore
                    public final Object invoke(Object obj, Object obj2) {
                        return ((C0077a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                    }

                    @DexIgnore
                    public final Object invokeSuspend(Object obj) {
                        Object a = ff6.a();
                        int i = this.label;
                        if (i == 0) {
                            nc6.a(obj);
                            this.L$0 = this.p$;
                            this.label = 1;
                            obj = this.this$0.this$0.a.this$0.o.loadSummaries((Date) this.$startDate.element, (Date) this.$endDate.element, this);
                            if (obj == a) {
                                return a;
                            }
                        } else if (i == 1) {
                            il6 il6 = (il6) this.L$0;
                            nc6.a(obj);
                        } else {
                            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                        }
                        return obj;
                    }
                }

                @DexIgnore
                /* JADX INFO: super call moved to the top of the method (can break code semantics) */
                public C0076a(a aVar, List list, xe6 xe6) {
                    super(2, xe6);
                    this.this$0 = aVar;
                    this.$goalTrackingList = list;
                }

                @DexIgnore
                public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                    wg6.b(xe6, "completion");
                    C0076a aVar = new C0076a(this.this$0, this.$goalTrackingList, xe6);
                    aVar.p$ = (il6) obj;
                    return aVar;
                }

                @DexIgnore
                public final Object invoke(Object obj, Object obj2) {
                    return ((C0076a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
                }

                @DexIgnore
                public final Object invokeSuspend(Object obj) {
                    Object a = ff6.a();
                    int i = this.label;
                    if (i == 0) {
                        nc6.a(obj);
                        il6 il6 = this.p$;
                        jh6 jh6 = new jh6();
                        jh6.element = ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        jh6 jh62 = new jh6();
                        jh62.element = ((GoalTrackingData) this.$goalTrackingList.get(0)).getDate();
                        for (GoalTrackingData goalTrackingData : this.$goalTrackingList) {
                            if (goalTrackingData.getDate().getTime() < ((Date) jh6.element).getTime()) {
                                jh6.element = goalTrackingData.getDate();
                            }
                            if (goalTrackingData.getDate().getTime() > ((Date) jh62.element).getTime()) {
                                jh62.element = goalTrackingData.getDate();
                            }
                        }
                        dl6 b = zl6.b();
                        C0077a aVar = new C0077a(this, jh6, jh62, (xe6) null);
                        this.L$0 = il6;
                        this.L$1 = jh6;
                        this.L$2 = jh62;
                        this.label = 1;
                        if (gk6.a(b, aVar, this) == a) {
                            return a;
                        }
                    } else if (i == 1) {
                        jh6 jh63 = (jh6) this.L$2;
                        jh6 jh64 = (jh6) this.L$1;
                        il6 il62 = (il6) this.L$0;
                        nc6.a(obj);
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    if (this.this$0.a.$cancellableContinuation.isActive()) {
                        lk6 lk6 = this.this$0.a.$cancellableContinuation;
                        Boolean a2 = hf6.a(true);
                        mc6.a aVar2 = mc6.Companion;
                        lk6.resumeWith(mc6.m1constructorimpl(a2));
                    }
                    return cd6.a;
                }
            }

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            public void onFail(int i) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onFail, go to next, errorCode = " + i);
                if (this.a.$cancellableContinuation.isActive()) {
                    lk6 lk6 = this.a.$cancellableContinuation;
                    mc6.a aVar = mc6.Companion;
                    lk6.resumeWith(mc6.m1constructorimpl(false));
                }
            }

            @DexIgnore
            public void onSuccess(List<GoalTrackingData> list) {
                wg6.b(list, "goalTrackingList");
                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "GoalTrackingRepository.pushPendingGoalTrackingDataList onSuccess, go to next");
                rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new C0076a(this, list, (xe6) null), 3, (Object) null);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(lk6 lk6, xe6 xe6, PushPendingDataWorker pushPendingDataWorker) {
            super(2, xe6);
            this.$cancellableContinuation = lk6;
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.$cancellableContinuation, xe6, this.this$0);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                GoalTrackingRepository c = this.this$0.o;
                a aVar = new a(this);
                this.L$0 = il6;
                this.label = 1;
                if (c.pushPendingGoalTrackingDataList(aVar, this) == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker", f = "PushPendingDataWorker.kt", l = {277, 285, 293, 201, 235, 240, 243, 301}, m = "start")
    public static final class g extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(PushPendingDataWorker pushPendingDataWorker, xe6 xe6) {
            super(xe6);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.b((xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$5", f = "PushPendingDataWorker.kt", l = {217, 218, 220, 221, 223, 224, 225}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $endDate;
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $startDate;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PushPendingDataWorker pushPendingDataWorker, jh6 jh6, jh6 jh62, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pushPendingDataWorker;
            this.$startDate = jh6;
            this.$endDate = jh62;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, this.$startDate, this.$endDate, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:15:0x008a, code lost:
            r13 = com.portfolio.platform.workers.PushPendingDataWorker.h(r12.this$0);
            r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r4, "startDate.toDate()");
            r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r5, "endDate.toDate()");
            r12.L$0 = r1;
            r12.label = 2;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x00b3, code lost:
            if (r13.loadSummaries(r4, r5, r12) != r0) goto L_0x00b6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x00b5, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b6, code lost:
            r4 = com.portfolio.platform.workers.PushPendingDataWorker.f(r12.this$0);
            r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r5, "startDate.toDate()");
            r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r6, "endDate.toDate()");
            r12.L$0 = r1;
            r12.label = 3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x00e5, code lost:
            if (com.portfolio.platform.data.source.SleepSessionsRepository.fetchSleepSessions$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x00e8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x00e7, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00e8, code lost:
            r13 = com.portfolio.platform.workers.PushPendingDataWorker.g(r12.this$0);
            r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r4, "startDate.toDate()");
            r5 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r5, "endDate.toDate()");
            r12.L$0 = r1;
            r12.label = 4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x0111, code lost:
            if (r13.fetchSleepSummaries(r4, r5, r12) != r0) goto L_0x0114;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x0113, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:24:0x0114, code lost:
            r4 = com.portfolio.platform.workers.PushPendingDataWorker.d(r12.this$0);
            r5 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r5, "startDate.toDate()");
            r6 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r6, "endDate.toDate()");
            r12.L$0 = r1;
            r12.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x0143, code lost:
            if (com.portfolio.platform.data.source.HeartRateSampleRepository.fetchHeartRateSamples$default(r4, r5, r6, 0, 0, r12, 12, (java.lang.Object) null) != r0) goto L_0x0146;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x0145, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:27:0x0146, code lost:
            r13 = com.portfolio.platform.workers.PushPendingDataWorker.e(r12.this$0);
            r4 = ((org.joda.time.DateTime) r12.$startDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r4, "startDate.toDate()");
            r3 = ((org.joda.time.DateTime) r12.$endDate.element).toDate();
            com.fossil.wg6.a((java.lang.Object) r3, "endDate.toDate()");
            r12.L$0 = r1;
            r12.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x016f, code lost:
            if (r13.loadSummaries(r4, r3, r12) != r0) goto L_0x0172;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x0171, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x0172, code lost:
            r13 = com.portfolio.platform.workers.PushPendingDataWorker.h(r12.this$0);
            r12.L$0 = r1;
            r12.label = 7;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x0181, code lost:
            if (r13.fetchActivityStatistic(r12) != r0) goto L_0x0184;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x0183, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0186, code lost:
            return com.fossil.cd6.a;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il6 il62 = this.p$;
                    ActivitiesRepository a2 = this.this$0.g;
                    Date date = ((DateTime) this.$startDate.element).toDate();
                    wg6.a((Object) date, "startDate.toDate()");
                    Date date2 = ((DateTime) this.$endDate.element).toDate();
                    wg6.a((Object) date2, "endDate.toDate()");
                    this.L$0 = il62;
                    this.label = 1;
                    if (ActivitiesRepository.fetchActivitySamples$default(a2, date, date2, 0, 0, this, 12, (Object) null) != a) {
                        il6 = il62;
                        break;
                    } else {
                        return a;
                    }
                case 1:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 6:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 7:
                    il6 il63 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements ThirdPartyRepository.PushPendingThirdPartyDataCallback {
        @DexIgnore
        public /* final */ /* synthetic */ lk6 a;

        @DexIgnore
        public i(lk6 lk6) {
            this.a = lk6;
        }

        @DexIgnore
        public void onComplete() {
            FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "ThirdPartyRepository.pushPendingData - Complete");
            if (this.a.isActive()) {
                lk6 lk6 = this.a;
                mc6.a aVar = mc6.Companion;
                lk6.resumeWith(mc6.m1constructorimpl(true));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.workers.PushPendingDataWorker$start$7", f = "PushPendingDataWorker.kt", l = {}, m = "invokeSuspend")
    public static final class j extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PushPendingDataWorker this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PushPendingDataWorker pushPendingDataWorker, xe6 xe6) {
            super(2, xe6);
            this.this$0 = pushPendingDataWorker;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            j jVar = new j(this.this$0, xe6);
            jVar.p$ = (il6) obj;
            return jVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((j) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.x.downloadPendingFile();
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r32v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* JADX WARNING: Unknown variable types count: 1 */
    public PushPendingDataWorker(WorkerParameters workerParameters, ActivitiesRepository activitiesRepository, SummariesRepository summariesRepository, SleepSessionsRepository sleepSessionsRepository, SleepSummariesRepository sleepSummariesRepository, GoalTrackingRepository goalTrackingRepository, HeartRateSampleRepository heartRateSampleRepository, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, AlarmsRepository alarmsRepository, an4 an4, DianaPresetRepository dianaPresetRepository, HybridPresetRepository hybridPresetRepository, ThirdPartyRepository thirdPartyRepository, FileRepository fileRepository, PortfolioApp r32) {
        super(r0, r1);
        WorkerParameters workerParameters2 = workerParameters;
        ActivitiesRepository activitiesRepository2 = activitiesRepository;
        SummariesRepository summariesRepository2 = summariesRepository;
        SleepSessionsRepository sleepSessionsRepository2 = sleepSessionsRepository;
        SleepSummariesRepository sleepSummariesRepository2 = sleepSummariesRepository;
        GoalTrackingRepository goalTrackingRepository2 = goalTrackingRepository;
        HeartRateSampleRepository heartRateSampleRepository2 = heartRateSampleRepository;
        HeartRateSummaryRepository heartRateSummaryRepository2 = heartRateSummaryRepository;
        FitnessDataRepository fitnessDataRepository2 = fitnessDataRepository;
        AlarmsRepository alarmsRepository2 = alarmsRepository;
        an4 an42 = an4;
        DianaPresetRepository dianaPresetRepository2 = dianaPresetRepository;
        HybridPresetRepository hybridPresetRepository2 = hybridPresetRepository;
        ThirdPartyRepository thirdPartyRepository2 = thirdPartyRepository;
        wg6.b(workerParameters2, "mWorkerParameters");
        wg6.b(activitiesRepository2, "mActivitiesRepository");
        wg6.b(summariesRepository2, "mSummariesRepository");
        wg6.b(sleepSessionsRepository2, "mSleepSessionRepository");
        wg6.b(sleepSummariesRepository2, "mSleepSummariesRepository");
        wg6.b(goalTrackingRepository2, "mGoalTrackingRepository");
        wg6.b(heartRateSampleRepository2, "mHeartRateSampleRepository");
        wg6.b(heartRateSummaryRepository2, "mHeartRateSummaryRepository");
        wg6.b(fitnessDataRepository2, "mFitnessDataRepository");
        wg6.b(alarmsRepository2, "mAlarmsRepository");
        wg6.b(an42, "mSharedPreferencesManager");
        wg6.b(dianaPresetRepository2, "mDianaPresetRepository");
        wg6.b(hybridPresetRepository2, "mHybridPresetRepository");
        wg6.b(thirdPartyRepository2, "mThirdPartyRepository");
        wg6.b(fileRepository, "mFileRepository");
        wg6.b(r32, "mApp");
        Context applicationContext = r32.getApplicationContext();
        wg6.a((Object) applicationContext, "mApp.applicationContext");
        this.g = activitiesRepository2;
        this.h = summariesRepository2;
        this.i = sleepSessionsRepository2;
        this.j = sleepSummariesRepository2;
        this.o = goalTrackingRepository2;
        this.p = heartRateSampleRepository2;
        this.q = heartRateSummaryRepository2;
        this.r = fitnessDataRepository2;
        this.s = alarmsRepository2;
        this.t = an42;
        this.u = dianaPresetRepository2;
        this.v = hybridPresetRepository2;
        this.w = thirdPartyRepository2;
        this.x = fileRepository;
        this.y = r32;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(xe6<? super ListenableWorker.a> xe6) {
        c cVar;
        int i2;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i3 = cVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                cVar.label = i3 - Integer.MIN_VALUE;
                Object obj = cVar.result;
                Object a2 = ff6.a();
                i2 = cVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d("PushPendingDataWorker", "doWork() - id = " + c());
                    cVar.L$0 = this;
                    cVar.label = 1;
                    if (b((xe6<? super cd6>) cVar) == a2) {
                        return a2;
                    }
                } else if (i2 == 1) {
                    PushPendingDataWorker pushPendingDataWorker = (PushPendingDataWorker) cVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ListenableWorker.a c2 = ListenableWorker.a.c();
                wg6.a((Object) c2, "Result.success()");
                return c2;
            }
        }
        cVar = new c(this, xe6);
        Object obj2 = cVar.result;
        Object a22 = ff6.a();
        i2 = cVar.label;
        if (i2 != 0) {
        }
        ListenableWorker.a c22 = ListenableWorker.a.c();
        wg6.a((Object) c22, "Result.success()");
        return c22;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0068, code lost:
        r6 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x012a, code lost:
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r4;
        r2.label = 2;
        r1 = new com.fossil.mk6(com.fossil.ef6.a(r2), 1);
        com.fossil.rm6 unused = com.fossil.ik6.b(com.fossil.jl6.a(com.fossil.zl6.b()), (com.fossil.af6) null, (com.fossil.ll6) null, new com.portfolio.platform.workers.PushPendingDataWorker.e(r1, (com.fossil.xe6) null, r9), 3, (java.lang.Object) null);
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0159, code lost:
        if (r1 != com.fossil.ff6.a()) goto L_0x015e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x015b, code lost:
        com.fossil.nf6.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x015e, code lost:
        if (r1 != r3) goto L_0x0161;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0160, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0161, code lost:
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r4;
        r2.label = 3;
        r1 = new com.fossil.mk6(com.fossil.ef6.a(r2), 1);
        com.fossil.rm6 unused = com.fossil.ik6.b(com.fossil.jl6.a(com.fossil.zl6.b()), (com.fossil.af6) null, (com.fossil.ll6) null, new com.portfolio.platform.workers.PushPendingDataWorker.f(r1, (com.fossil.xe6) null, r9), 3, (java.lang.Object) null);
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0190, code lost:
        if (r1 != com.fossil.ff6.a()) goto L_0x0195;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0192, code lost:
        com.fossil.nf6.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0195, code lost:
        if (r1 != r3) goto L_0x0198;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0197, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0198, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData");
        r1 = r9.r;
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r4;
        r2.label = 4;
        r1 = r1.pushPendingFitnessData(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x01b2, code lost:
        if (r1 != r3) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x01b4, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x01b5, code lost:
        r1 = (com.fossil.ap4) r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x01b9, code lost:
        if ((r1 instanceof com.fossil.cp4) == false) goto L_0x024e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x01bb, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData - Success");
        r1 = (java.util.List) ((com.fossil.cp4) r1).a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x01ce, code lost:
        if (r1 == null) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x01d5, code lost:
        if ((!r1.isEmpty()) == false) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x01d7, code lost:
        r6 = new com.fossil.jh6();
        r6.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.get(0)).getStartTimeTZ();
        r11 = new com.fossil.jh6();
        r11.element = ((com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.get(0)).getEndTimeTZ();
        r1 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0202, code lost:
        if (r1.hasNext() == false) goto L_0x0237;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0204, code lost:
        r10 = (com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper) r1.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0218, code lost:
        if (r10.getStartLongTime() >= ((org.joda.time.DateTime) r6.element).getMillis()) goto L_0x0220;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x021a, code lost:
        r6.element = r10.getStartTimeTZ();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x022e, code lost:
        if (r10.getEndLongTime() <= ((org.joda.time.DateTime) r11.element).getMillis()) goto L_0x01fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x0230, code lost:
        r11.element = r10.getEndTimeTZ();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0237, code lost:
        com.fossil.rm6 unused = com.fossil.ik6.b(com.fossil.jl6.a(com.fossil.zl6.b()), (com.fossil.af6) null, (com.fossil.ll6) null, new com.portfolio.platform.workers.PushPendingDataWorker.h(r9, r6, r11, (com.fossil.xe6) null), 3, (java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0250, code lost:
        if ((r1 instanceof com.fossil.zo4) == false) goto L_0x025d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x0252, code lost:
        com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "SleepSessionRepository.pushPendingFitnessData - Failed ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x025d, code lost:
        r1 = r9.s;
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r4;
        r2.label = 5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x026c, code lost:
        if (r1.executePendingRequest(r2) != r3) goto L_0x0068;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x026e, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x026f, code lost:
        r4 = r9.y.e();
        r1 = com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil.getDeviceBySerial(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0279, code lost:
        if (r1 != null) goto L_0x027c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0284, code lost:
        if (com.fossil.f06.a[r1.ordinal()] == 1) goto L_0x029a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0286, code lost:
        r1 = r9.v;
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r6;
        r2.L$2 = r4;
        r2.label = 7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x0297, code lost:
        if (r1.executePendingRequest(r4, r2) != r3) goto L_0x02ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x0299, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x029a, code lost:
        r1 = r9.u;
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r6;
        r2.L$2 = r4;
        r2.label = 6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x02ab, code lost:
        if (r1.executePendingRequest(r4, r2) != r3) goto L_0x02ae;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x02ad, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x02ae, code lost:
        r2.L$0 = r9;
        r2.Z$0 = r8;
        r2.L$1 = r6;
        r2.L$2 = r4;
        r2.label = 8;
        r1 = new com.fossil.mk6(com.fossil.ef6.a(r2), 1);
        i(r9).pushPendingData(new com.portfolio.platform.workers.PushPendingDataWorker.i(r1));
        r1 = r1.e();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x02d7, code lost:
        if (r1 != com.fossil.ff6.a()) goto L_0x02dc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x02d9, code lost:
        com.fossil.nf6.c(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x02dc, code lost:
        if (r1 != r3) goto L_0x02df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x02de, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x02df, code lost:
        r2 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02e0, code lost:
        com.fossil.rm6 unused = com.fossil.ik6.b(com.fossil.jl6.a(com.fossil.zl6.b()), (com.fossil.af6) null, (com.fossil.ll6) null, new com.portfolio.platform.workers.PushPendingDataWorker.j(r2, (com.fossil.xe6) null), 3, (java.lang.Object) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x02f6, code lost:
        return com.fossil.cd6.a;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0033  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0059  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x007a  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final Object b(xe6<? super cd6> xe6) {
        g gVar;
        PushPendingDataWorker pushPendingDataWorker;
        boolean z2;
        MFUser mFUser;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof g) {
            gVar = (g) xe62;
            int i2 = gVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                gVar.label = i2 - Integer.MIN_VALUE;
                Object obj = gVar.result;
                Object a2 = ff6.a();
                switch (gVar.label) {
                    case 0:
                        nc6.a(obj);
                        boolean m = this.t.m(this.y.h());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        local.d("PushPendingDataWorker", "start push pending data, isMigrationComplete " + m);
                        if (m) {
                            mFUser = zm4.p.a().n().b();
                            if (mFUser != null) {
                                gVar.L$0 = this;
                                gVar.Z$0 = m;
                                gVar.L$1 = mFUser;
                                gVar.label = 1;
                                mk6 mk6 = new mk6(ef6.a(gVar), 1);
                                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(mk6, (xe6) null, this), 3, (Object) null);
                                Object e2 = mk6.e();
                                if (e2 == ff6.a()) {
                                    nf6.c(gVar);
                                }
                                if (e2 != a2) {
                                    pushPendingDataWorker = this;
                                    z2 = m;
                                    break;
                                } else {
                                    return a2;
                                }
                            } else {
                                FLogger.INSTANCE.getLocal().d("PushPendingDataWorker", "User is null, no need to push any pending data here");
                                return cd6.a;
                            }
                        } else {
                            return cd6.a;
                        }
                    case 1:
                        mFUser = (MFUser) gVar.L$1;
                        z2 = gVar.Z$0;
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    case 2:
                        mFUser = (MFUser) gVar.L$1;
                        z2 = gVar.Z$0;
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    case 3:
                        mFUser = (MFUser) gVar.L$1;
                        z2 = gVar.Z$0;
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    case 4:
                        mFUser = (MFUser) gVar.L$1;
                        z2 = gVar.Z$0;
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    case 5:
                        mFUser = (MFUser) gVar.L$1;
                        boolean z3 = gVar.Z$0;
                        nc6.a(obj);
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        z2 = z3;
                        break;
                    case 6:
                    case 7:
                        String str = (String) gVar.L$2;
                        MFUser mFUser2 = (MFUser) gVar.L$1;
                        z2 = gVar.Z$0;
                        pushPendingDataWorker = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    case 8:
                        String str2 = (String) gVar.L$2;
                        MFUser mFUser3 = (MFUser) gVar.L$1;
                        boolean z4 = gVar.Z$0;
                        PushPendingDataWorker pushPendingDataWorker2 = (PushPendingDataWorker) gVar.L$0;
                        nc6.a(obj);
                        break;
                    default:
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
            }
        }
        gVar = new g(this, xe62);
        Object obj2 = gVar.result;
        Object a22 = ff6.a();
        switch (gVar.label) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                break;
            case 3:
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
            case 7:
                break;
            case 8:
                break;
        }
    }
}
