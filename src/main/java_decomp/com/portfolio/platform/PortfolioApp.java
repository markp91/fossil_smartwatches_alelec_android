package com.portfolio.platform;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Process;
import android.os.StrictMode;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import com.facebook.FacebookSdk;
import com.facebook.stetho.Stetho;
import com.fossil.a8;
import com.fossil.af6;
import com.fossil.al4;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.b14;
import com.fossil.bk4;
import com.fossil.bm;
import com.fossil.bs4;
import com.fossil.c86;
import com.fossil.cd6;
import com.fossil.ce;
import com.fossil.cj4;
import com.fossil.cp4;
import com.fossil.d06;
import com.fossil.dl6;
import com.fossil.do1;
import com.fossil.ex5;
import com.fossil.ff6;
import com.fossil.ge0;
import com.fossil.gk6;
import com.fossil.hg6;
import com.fossil.hl4;
import com.fossil.ht4;
import com.fossil.ig6;
import com.fossil.ik4;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.j06;
import com.fossil.jf6;
import com.fossil.jl4;
import com.fossil.jl6;
import com.fossil.jm4;
import com.fossil.kl4;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.mj6;
import com.fossil.ml;
import com.fossil.nc6;
import com.fossil.nd;
import com.fossil.nd6;
import com.fossil.nh6;
import com.fossil.o24;
import com.fossil.q06;
import com.fossil.q24;
import com.fossil.qd6;
import com.fossil.qe;
import com.fossil.qg6;
import com.fossil.qh4;
import com.fossil.rc6;
import com.fossil.re;
import com.fossil.rh4;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.so4;
import com.fossil.ti4;
import com.fossil.tj4;
import com.fossil.u04;
import com.fossil.v00;
import com.fossil.w24;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xi4;
import com.fossil.xj6;
import com.fossil.xm4;
import com.fossil.xx5;
import com.fossil.y04;
import com.fossil.yd6;
import com.fossil.yj6;
import com.fossil.yo4;
import com.fossil.z24;
import com.fossil.zj4;
import com.fossil.zk4;
import com.fossil.zl6;
import com.fossil.zm4;
import com.google.android.libraries.places.api.Places;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.IButtonConnectivity;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.InactiveNudgeData;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserBiometricData;
import com.misfit.frameworks.buttonservice.model.UserDisplayUnit;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.customrequest.CustomRequest;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.misfit.frameworks.buttonservice.model.pairing.PairingResponse;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.response.MusicResponse;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.misfit.frameworks.buttonservice.utils.MicroAppEventLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.cloudimage.ResolutionHelper;
import com.portfolio.platform.data.Access;
import com.portfolio.platform.data.model.Installation;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.room.fitness.ActivitySettings;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepDay;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.localization.LocalizationManager;
import com.portfolio.platform.manager.SoLibraryLoader;
import com.portfolio.platform.manager.WeatherManager;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.news.notifications.FossilNotificationBar;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import com.portfolio.platform.receiver.NetworkChangedReceiver;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.response.ResponseKt;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.portfolio.platform.service.MFDeviceService;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.splash.SplashScreenActivity;
import com.portfolio.platform.uirenew.welcome.WelcomeActivity;
import com.portfolio.platform.workers.PushPendingDataWorker;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.io.File;
import java.io.IOException;
import java.lang.Thread;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioApp extends re implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public static /* final */ String R;
    @DexIgnore
    public static boolean S;
    @DexIgnore
    public static PortfolioApp T;
    @DexIgnore
    public static boolean U;
    @DexIgnore
    public static j06 V;
    @DexIgnore
    public static IButtonConnectivity W;
    @DexIgnore
    public static MFDeviceService.b X;
    @DexIgnore
    public static /* final */ inner get; // = new inner((qg6) null);
    @DexIgnore
    public WatchFaceRepository A;
    @DexIgnore
    public MutableLiveData<String> B; // = new MutableLiveData<>();
    @DexIgnore
    public boolean C;
    @DexIgnore
    public boolean D; // = true;
    @DexIgnore
    public /* final */ Handler E; // = new Handler();
    @DexIgnore
    public Runnable F;
    @DexIgnore
    public y04 G;
    @DexIgnore
    public Thread.UncaughtExceptionHandler H;
    @DexIgnore
    public boolean I;
    @DexIgnore
    public ServerError J;
    @DexIgnore
    public jl4 K;
    @DexIgnore
    public kl4 L;
    @DexIgnore
    public NetworkChangedReceiver M;
    @DexIgnore
    public /* final */ yo4 N; // = new yo4();
    @DexIgnore
    public so4 O;
    @DexIgnore
    public SmsMmsReceiver P;
    @DexIgnore
    public ThemeRepository Q;
    @DexIgnore
    public LocalizationManager a;
    @DexIgnore
    public AppPackageInstallReceiver b;
    @DexIgnore
    public an4 c;
    @DexIgnore
    public UserRepository d;
    @DexIgnore
    public SummariesRepository e;
    @DexIgnore
    public SleepSummariesRepository f;
    @DexIgnore
    public AlarmHelper g;
    @DexIgnore
    public GuestApiService h;
    @DexIgnore
    public u04 i;
    @DexIgnore
    public ApiServiceV2 j;
    @DexIgnore
    public ex5 o;
    @DexIgnore
    public z24 p;
    @DexIgnore
    public DeleteLogoutUserUseCase q;
    @DexIgnore
    public AnalyticsHelper r;
    @DexIgnore
    public ApplicationEventListener s;
    @DexIgnore
    public DeviceRepository t;
    @DexIgnore
    public ik4 u;
    @DexIgnore
    public ShakeFeedbackService v;
    @DexIgnore
    public DianaPresetRepository w;
    @DexIgnore
    public d06 x;
    @DexIgnore
    public WatchLocalizationRepository y;
    @DexIgnore
    public DataValidationManager z;

    @DexIgnore
    public enum ActivityState {
        CREATE,
        START,
        RESUME,
        PAUSE,
        STOP,
        DESTROY
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Thread {
        @DexIgnore
        public void run() {
            try {
                FirebaseInstanceId.l().a();
            } catch (IOException e) {
                FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), e.toString());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp$executeSetLocalization$1", f = "PortfolioApp.kt", l = {1217}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(PortfolioApp portfolioApp, xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            IButtonConnectivity b;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                WatchLocalizationRepository v = this.this$0.v();
                this.L$0 = il6;
                this.label = 1;
                obj = v.getWatchLocalizationFromServer(true, this);
                if (obj == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            String str = (String) obj;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.get.d();
            local.d(d, "path of localization file: " + str);
            if (!(str == null || (b = PortfolioApp.get.b()) == null)) {
                b.setLocalizationData(new LocalizationData(str, (String) null, 2, (qg6) null));
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e implements m24.e<ht4.d, ht4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;
        @DexIgnore
        public /* final */ /* synthetic */ ServerError b;

        @DexIgnore
        public e(PortfolioApp portfolioApp, ServerError serverError) {
            this.a = portfolioApp;
            this.b = serverError;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* renamed from: a */
        public void onSuccess(DeleteLogoutUserUseCase.d dVar) {
            Integer code;
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout successfully - start welcome activity");
            Intent intent = new Intent(this.a, WelcomeActivity.class);
            intent.addFlags(268468224);
            ServerError serverError = this.b;
            if (!(serverError == null || (code = serverError.getCode()) == null)) {
                int intValue = code.intValue();
            }
            this.a.startActivity(intent);
            this.a.b((ServerError) null);
            this.a.b(false);
            zm4.p.a().o();
        }

        @DexIgnore
        public void a(DeleteLogoutUserUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "logout unsuccessfully");
            this.a.b((ServerError) null);
            this.a.b(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp$onActiveDeviceStealed$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class f extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(PortfolioApp portfolioApp, xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            f fVar = new f(this.this$0, xe6);
            fVar.p$ = (il6) obj;
            return fVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((f) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.get.d();
                local.d(d, "activeDevice=" + this.this$0.e() + ", was stealed remove it!!!");
                if (DeviceHelper.o.f(this.this$0.e())) {
                    this.this$0.x().deleteWatchFacesWithSerial(this.this$0.e());
                }
                PortfolioApp portfolioApp = this.this$0;
                portfolioApp.s(portfolioApp.e());
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public g(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.B() || !this.a.D) {
                PortfolioApp.get.a(false);
                FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "still foreground");
                return;
            }
            this.a.a(false);
            PortfolioApp.get.a(true);
            FLogger.INSTANCE.getLocal().d(PortfolioApp.get.d(), "went background");
            this.a.G();
            jl4 a2 = this.a.K;
            if (a2 != null) {
                a2.a("");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp$onCreate$1", f = "PortfolioApp.kt", l = {465}, m = "invokeSuspend")
    public static final class h extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(PortfolioApp portfolioApp, xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            h hVar = new h(this.this$0, xe6);
            hVar.p$ = (il6) obj;
            return hVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((h) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r11v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final Object invokeSuspend(Object obj) {
            String str;
            String str2;
            String str3;
            String str4;
            String str5;
            String str6;
            String str7;
            String n;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                ThemeRepository t = this.this$0.t();
                this.L$0 = il6;
                this.label = 1;
                if (t.initializeLocalTheme(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            Access a2 = new SoLibraryLoader().a((Context) this.this$0);
            do1 do1 = do1.b;
            String s = w24.y.s();
            if (a2 == null || (str = a2.getL()) == null) {
                str = "";
            }
            if (a2 == null || (str2 = a2.getM()) == null) {
                str2 = "";
            }
            do1.a(new ge0(s, str, str2));
            String h = w24.y.h();
            String e = w24.y.e();
            if (a2 == null || (str3 = a2.getI()) == null) {
                str3 = "";
            }
            if (a2 == null || (str4 = a2.getK()) == null) {
                str4 = "";
            }
            FLogger.INSTANCE.init("App", tj4.f.b().b(), tj4.f.b().a(), new CloudLogConfig(h, e, str3, str4), this.this$0, true, "App");
            FLogger.INSTANCE.getRemote().flush();
            String str8 = "0000000000000";
            if (a2 == null || (str5 = a2.getO()) == null) {
                str5 = str8;
            }
            FacebookSdk.setApplicationId(str5);
            if (!FacebookSdk.isInitialized()) {
                FacebookSdk.sdkInitialize(this.this$0);
            }
            if (!Places.isInitialized()) {
                Object r1 = this.this$0;
                if (!(a2 == null || (n = a2.getN()) == null)) {
                    str8 = n;
                }
                Places.initialize(r1, str8);
            }
            AnalyticsHelper.f.c().a(AnalyticsHelper.f.c().b());
            tj4.f.d();
            ZendeskConfig zendeskConfig = ZendeskConfig.INSTANCE;
            Object r12 = this.this$0;
            String w = w24.y.w();
            if (a2 == null || (str6 = a2.getG()) == null) {
                str6 = "";
            }
            if (a2 == null || (str7 = a2.getH()) == null) {
                str7 = "";
            }
            zendeskConfig.init(r12, w, str6, str7);
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i implements Thread.UncaughtExceptionHandler {
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp a;

        @DexIgnore
        public i(PortfolioApp portfolioApp) {
            this.a = portfolioApp;
        }

        @DexIgnore
        public final void uncaughtException(Thread thread, Throwable th) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String d = PortfolioApp.get.d();
            local.e(d, "uncaughtException - ex=" + th);
            th.printStackTrace();
            wg6.a((Object) th, "e");
            StackTraceElement[] stackTrace = th.getStackTrace();
            int length = stackTrace != null ? stackTrace.length : 0;
            if (length > 0) {
                int i = 0;
                while (true) {
                    if (i >= length) {
                        break;
                    } else if (stackTrace != null) {
                        StackTraceElement stackTraceElement = stackTrace[i];
                        wg6.a((Object) stackTraceElement, "element");
                        String className = stackTraceElement.getClassName();
                        wg6.a((Object) className, "element.className");
                        String simpleName = ButtonService.class.getSimpleName();
                        wg6.a((Object) simpleName, "ButtonService::class.java.simpleName");
                        if (yj6.a((CharSequence) className, (CharSequence) simpleName, false, 2, (Object) null)) {
                            FLogger.INSTANCE.getLocal().e(PortfolioApp.get.d(), "uncaughtException - stopLogService");
                            this.a.b((int) FailureCode.APP_CRASH_FROM_BUTTON_SERVICE);
                            break;
                        }
                        i++;
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            long currentTimeMillis = System.currentTimeMillis();
            this.a.w().c(currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String d2 = PortfolioApp.get.d();
            local2.d(d2, "Inside .uncaughtException - currentTime = " + currentTimeMillis);
            Thread.UncaughtExceptionHandler b = this.a.H;
            if (b != null) {
                b.uncaughtException(thread, th);
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class inner {
        @DexIgnore
        public inner() {
        }

        @DexIgnore
        public final void a(boolean z) {
            PortfolioApp.d(z);
        }

        @DexIgnore
        public final IButtonConnectivity b() {
            return PortfolioApp.W;
        }

        @DexIgnore
        public final String d() {
            return PortfolioApp.R;
        }

        @DexIgnore
        public final boolean e() {
            return PortfolioApp.U;
        }

        @DexIgnore
        public final boolean f() {
            return PortfolioApp.S;
        }

        @DexIgnore
        public final PortfolioApp instance() {
            PortfolioApp Z = PortfolioApp.T;
            if (Z != null) {
                return Z;
            }
            wg6.d("instance");
            throw null;
        }

        @DexIgnore
        public /* synthetic */ inner(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final j06 a() {
            return PortfolioApp.V;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Removed duplicated region for block: B:13:0x0039 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:14:0x003a A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[Catch:{ Exception -> 0x0093 }] */
        /* JADX WARNING: Removed duplicated region for block: B:33:0x008e A[Catch:{ Exception -> 0x0093 }] */
        public final void b(IButtonConnectivity iButtonConnectivity) {
            String str;
            String str2;
            IButtonConnectivity b;
            String str3;
            String str4;
            wg6.b(iButtonConnectivity, Constants.SERVICE);
            a(iButtonConnectivity);
            try {
                Access a = new SoLibraryLoader().a((Context) instance());
                String h = w24.y.h();
                String e = w24.y.e();
                if (a != null) {
                    str = a.getI();
                    if (str != null) {
                        if (a == null || (str2 = a.getK()) == null) {
                            str2 = "";
                        }
                        CloudLogConfig cloudLogConfig = new CloudLogConfig(h, e, str, str2);
                        b = b();
                        if (b == null) {
                            String s = w24.y.s();
                            if (a == null || (str3 = a.getL()) == null) {
                                str3 = "";
                            }
                            if (a == null || (str4 = a.getM()) == null) {
                                str4 = "";
                            }
                            b.init(s, str3, str4, (wg6.a((Object) "release", (Object) "release") ? instance().n() : FossilDeviceSerialPatternUtil.BRAND.UNKNOWN).getPrefix(), tj4.f.b().b(), tj4.f.b().a(), cloudLogConfig);
                            return;
                        }
                        wg6.a();
                        throw null;
                    }
                }
                str = "";
                if (a == null || (str2 = a.getK()) == null) {
                }
                CloudLogConfig cloudLogConfig2 = new CloudLogConfig(h, e, str, str2);
                b = b();
                if (b == null) {
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = d();
                local.e(d, ".updateButtonService(), ex=" + e2);
            }
        }

        @DexIgnore
        public final void c(Object obj) {
            wg6.b(obj, "o");
            try {
                j06 a = a();
                if (a != null) {
                    a.c(obj);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e) {
                String d = d();
                v00.a(0, d, "Exception when unregister bus events for object=" + obj + ",exception=" + e);
            }
        }

        @DexIgnore
        public final void a(IButtonConnectivity iButtonConnectivity) {
            PortfolioApp.W = iButtonConnectivity;
        }

        @DexIgnore
        public final void a(MFDeviceService.b bVar) {
            PortfolioApp.X = bVar;
        }

        @DexIgnore
        public final void a(Object obj) {
            wg6.b(obj, Constants.EVENT);
            j06 a = a();
            if (a != null) {
                a.a(obj);
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public final void b(MFDeviceService.b bVar) {
            wg6.b(bVar, Constants.SERVICE);
            a(bVar);
        }

        @DexIgnore
        public final void b(Object obj) {
            wg6.b(obj, "o");
            try {
                j06 a = a();
                if (a != null) {
                    a.b(obj);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e) {
                String d = d();
                v00.a(0, d, "Exception when register bus events for object=" + obj + ",exception=" + e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp", f = "PortfolioApp.kt", l = {1053}, m = "saveInstallation")
    public static final class j extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(PortfolioApp portfolioApp, xe6 xe6) {
            super(xe6);
            this.this$0 = portfolioApp;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((xe6<? super cd6>) this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2", f = "PortfolioApp.kt", l = {1054}, m = "invokeSuspend")
    public static final class k extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ Installation $installation;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ PortfolioApp this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.PortfolioApp$saveInstallation$2$response$1", f = "PortfolioApp.kt", l = {1054}, m = "invokeSuspend")
        public static final class a extends sf6 implements hg6<xe6<? super rx6<Installation>>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public /* final */ /* synthetic */ k this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(k kVar, xe6 xe6) {
                super(1, xe6);
                this.this$0 = kVar;
            }

            @DexIgnore
            public final xe6<cd6> create(xe6<?> xe6) {
                wg6.b(xe6, "completion");
                return new a(this.this$0, xe6);
            }

            @DexIgnore
            public final Object invoke(Object obj) {
                return ((a) create((xe6) obj)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                Object a = ff6.a();
                int i = this.label;
                if (i == 0) {
                    nc6.a(obj);
                    ApiServiceV2 q = this.this$0.this$0.q();
                    Installation installation = this.this$0.$installation;
                    this.label = 1;
                    obj = q.insertInstallation(installation, this);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return obj;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(PortfolioApp portfolioApp, Installation installation, xe6 xe6) {
            super(2, xe6);
            this.this$0 = portfolioApp;
            this.$installation = installation;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            k kVar = new k(this.this$0, this.$installation, xe6);
            kVar.p$ = (il6) obj;
            return kVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((k) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.label = 1;
                obj = ResponseKt.a(aVar, this);
                if (obj == a2) {
                    return a2;
                }
            } else if (i == 1) {
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap4 = (ap4) obj;
            if (ap4 instanceof cp4) {
                cp4 cp4 = (cp4) ap4;
                if (cp4.a() != null) {
                    this.this$0.w().p(((Installation) cp4.a()).getInstallationId());
                }
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.PortfolioApp$updateActiveDeviceInfoLog$1", f = "PortfolioApp.kt", l = {}, m = "invokeSuspend")
    public static final class l extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;

        @DexIgnore
        public l(xe6 xe6) {
            super(2, xe6);
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            l lVar = new l(xe6);
            lVar.p$ = (il6) obj;
            return lVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((l) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ActiveDeviceInfo a = tj4.f.b().a();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String d = PortfolioApp.get.d();
                local.d(d, ".updateActiveDeviceInfoLog(), " + new Gson().a(a));
                FLogger.INSTANCE.getRemote().updateActiveDeviceInfo(a);
                try {
                    IButtonConnectivity b = PortfolioApp.get.b();
                    if (b != null) {
                        b.updateActiveDeviceInfoLog(a);
                    }
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String d2 = PortfolioApp.get.d();
                    local2.e(d2, ".updateActiveDeviceInfoToButtonService(), error=" + e);
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        String simpleName = PortfolioApp.class.getSimpleName();
        wg6.a((Object) simpleName, "PortfolioApp::class.java.simpleName");
        R = simpleName;
    }
    */

    @DexIgnore
    public static final /* synthetic */ void d(boolean z2) {
    }

    @DexIgnore
    public static final IButtonConnectivity d0() {
        return W;
    }

    @DexIgnore
    public final void A() {
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.interruptCurrentSession(e());
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.e(str, "Error while interruptCurrentSession - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean B() {
        return this.C;
    }

    @DexIgnore
    public final boolean C() {
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                int[] listActiveCommunicator = iButtonConnectivity.getListActiveCommunicator();
                wg6.a((Object) listActiveCommunicator, "listActiveCommunicator");
                List<Integer> b2 = nd6.b(listActiveCommunicator);
                if (!(!b2.isEmpty()) || !b2.contains(Integer.valueOf(CommunicateMode.OTA.getValue()))) {
                    return false;
                }
                return true;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final boolean D() {
        return wg6.a((Object) getPackageName(), (Object) s());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final boolean E() {
        String string = Settings.Secure.getString(getContentResolver(), "enabled_notification_listeners");
        String str = getPackageName() + ZendeskConfig.SLASH + FossilNotificationListenerService.class.getCanonicalName();
        FLogger.INSTANCE.getLocal().d(R, "isNotificationListenerEnabled() - notificationServicePath = " + str);
        if (!TextUtils.isEmpty(string)) {
            FLogger.INSTANCE.getLocal().d(R, "isNotificationListenerEnabled() enabledNotificationListeners = " + string);
        }
        if (TextUtils.isEmpty(string)) {
            return false;
        }
        wg6.a((Object) string, "enabledNotificationListeners");
        return yj6.a((CharSequence) string, (CharSequence) str, false, 2, (Object) null);
    }

    @DexIgnore
    public final boolean F() {
        return xj6.b("release", "release", true);
    }

    @DexIgnore
    public final void G() {
        jl4 c2 = AnalyticsHelper.f.c("ota_session");
        jl4 c3 = AnalyticsHelper.f.c("sync_session");
        jl4 c4 = AnalyticsHelper.f.c("setup_device_session");
        if (c2 != null && c2.b()) {
            c2.a(AnalyticsHelper.f.a("ota_session_go_to_background"));
        } else if (c3 != null && c3.b()) {
            c3.a(AnalyticsHelper.f.a("sync_session_go_to_background"));
        } else if (c4 != null && c4.b()) {
            c4.a(AnalyticsHelper.f.a("setup_device_session_go_to_background"));
        }
    }

    @DexIgnore
    public final void H() {
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.logOut();
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final rm6 I() {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new f(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void J() {
        T();
        S();
        synchronized (PortfolioApp.class) {
            b14 b14 = new b14(this);
            o24.i e0 = o24.e0();
            e0.a(b14);
            y04 a2 = e0.a();
            wg6.a((Object) a2, "DaggerApplicationCompone\u2026                 .build()");
            this.G = a2;
            y04 y04 = this.G;
            if (y04 != null) {
                y04.a(this);
                cd6 cd6 = cd6.a;
            } else {
                wg6.d("applicationComponent");
                throw null;
            }
        }
        MFDeviceService.b bVar = X;
        if (bVar != null) {
            if (bVar != null) {
                bVar.a().w();
            } else {
                wg6.a();
                throw null;
            }
        }
        O();
        M();
        WeatherManager.n.a().g();
        WatchAppCommuteTimeManager.t.a().c();
        MusicControlComponent musicControlComponent = (MusicControlComponent) MusicControlComponent.o.a(this);
        DianaPresetRepository dianaPresetRepository = this.w;
        if (dianaPresetRepository != null) {
            musicControlComponent.a(dianaPresetRepository);
            xm4 xm4 = xm4.d;
            an4 an4 = this.c;
            if (an4 != null) {
                xm4.a(an4);
            } else {
                wg6.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            wg6.d("mDianaPresetRepository");
            throw null;
        }
    }

    @DexIgnore
    public final long K() {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceReadRealTimeStep(e());
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void L() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REPLACED");
        intentFilter.addDataScheme("package");
        registerReceiver(this.b, intentFilter);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void M() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = R;
                local.d(str, "registerContactObserver currentUser=" + currentUser);
                boolean e2 = xx5.a.e(this);
                if (e2) {
                    ex5 ex5 = this.o;
                    if (ex5 != null) {
                        ex5.b();
                        ContentResolver contentResolver = getContentResolver();
                        Uri uri = ContactsContract.Contacts.CONTENT_URI;
                        ex5 ex52 = this.o;
                        if (ex52 != null) {
                            contentResolver.registerContentObserver(uri, true, ex52);
                            FLogger.INSTANCE.getLocal().d(R, "registerContactObserver success");
                            an4 an4 = this.c;
                            if (an4 != null) {
                                an4.n(true);
                            } else {
                                wg6.d("sharedPreferencesManager");
                                throw null;
                            }
                        } else {
                            wg6.d("mContactObserver");
                            throw null;
                        }
                    } else {
                        wg6.d("mContactObserver");
                        throw null;
                    }
                } else {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = R;
                    local2.d(str2, "registerContactObserver fail due to enough Permission =" + e2);
                }
            }
        } else {
            wg6.d("mUserRepository");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void N() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.WAP_PUSH_RECEIVED");
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        SmsMmsReceiver smsMmsReceiver = this.P;
        if (smsMmsReceiver != null) {
            registerReceiver(smsMmsReceiver, intentFilter);
        } else {
            wg6.d("mMessageReceiver");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void O() {
        try {
            so4 so4 = this.O;
            if (so4 != null) {
                registerReceiver(so4, new IntentFilter("android.intent.action.PHONE_STATE"));
                N();
                return;
            }
            wg6.d("mPhoneCallReceiver");
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.e(str, "registerTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void P() {
        AlarmHelper alarmHelper = this.g;
        if (alarmHelper != null) {
            alarmHelper.a((Context) this);
            AlarmHelper alarmHelper2 = this.g;
            if (alarmHelper2 != null) {
                alarmHelper2.d(this);
            } else {
                wg6.d("mAlarmHelper");
                throw null;
            }
        } else {
            wg6.d("mAlarmHelper");
            throw null;
        }
    }

    @DexIgnore
    public final void Q() {
        String e2 = e();
        try {
            FLogger.INSTANCE.getLocal().d(R, "Inside resetDeviceSettingToDefault");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.resetDeviceSettingToDefault(e2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e3) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.e(str, "Error inside " + R + ".resetDeviceSettingToDefault - e=" + e3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0009, code lost:
        r1 = r1.getDeviceProfile((java.lang.String) r9.B.a());
     */
    @DexIgnore
    public final void R() {
        MisfitDeviceProfile deviceProfile;
        MisfitDeviceProfile deviceProfile2;
        try {
            String p2 = p();
            IButtonConnectivity iButtonConnectivity = W;
            String locale = (iButtonConnectivity == null || deviceProfile2 == null) ? null : deviceProfile2.getLocale();
            an4 an4 = this.c;
            if (an4 != null) {
                String h2 = an4.h(p2);
                IButtonConnectivity iButtonConnectivity2 = W;
                String localeVersion = (iButtonConnectivity2 == null || (deviceProfile = iButtonConnectivity2.getDeviceProfile((String) this.B.a())) == null) ? null : deviceProfile.getLocaleVersion();
                an4 an42 = this.c;
                if (an42 != null) {
                    String q2 = an42.q();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = R;
                    local.d(str, "phoneLocale " + p2 + " - watchLocale: " + locale + " - standardLocale: " + h2 + "\nwatchLocaleVersion: " + localeVersion + " - standardLocaleVersion: " + q2);
                    if (!wg6.a((Object) h2, (Object) locale)) {
                        c(p2);
                    } else if (!wg6.a((Object) localeVersion, (Object) q2)) {
                        c(p2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(R, "don't need to set to watch");
                    }
                } else {
                    wg6.d("sharedPreferencesManager");
                    throw null;
                }
            } else {
                wg6.d("sharedPreferencesManager");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void S() {
        FLogger.INSTANCE.getLocal().d(R, "unregisterContactObserver");
        try {
            ex5 ex5 = this.o;
            if (ex5 != null) {
                ex5.c();
                ContentResolver contentResolver = getContentResolver();
                ex5 ex52 = this.o;
                if (ex52 != null) {
                    contentResolver.unregisterContentObserver(ex52);
                    an4 an4 = this.c;
                    if (an4 != null) {
                        an4.n(false);
                    } else {
                        wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    wg6.d("mContactObserver");
                    throw null;
                }
            } else {
                wg6.d("mContactObserver");
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.d(str, "unregisterContactObserver e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void T() {
        try {
            so4 so4 = this.O;
            if (so4 != null) {
                unregisterReceiver(so4);
                SmsMmsReceiver smsMmsReceiver = this.P;
                if (smsMmsReceiver != null) {
                    unregisterReceiver(smsMmsReceiver);
                } else {
                    wg6.d("mMessageReceiver");
                    throw null;
                }
            } else {
                wg6.d("mPhoneCallReceiver");
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.e(str, "unregisterTelephonyReceiver throw exception: " + e2.getMessage());
        }
    }

    @DexIgnore
    public final rm6 U() {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new l((xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final void V() {
        AppLogInfo b2 = tj4.f.b().b();
        FLogger.INSTANCE.updateAppLogInfo(b2);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateAppLogInfo(b2);
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.e(str, ".updateAppLogInfo(), error=" + e2);
        }
    }

    @DexIgnore
    public final void W() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = R;
                local.d(str, "updateFabricUserInformation currentUser=" + currentUser);
                v00.o().g.c(currentUser.getUserId());
                v00.o().g.a("UserId", currentUser.getUserId());
                if (U && !TextUtils.isEmpty(currentUser.getEmail())) {
                    v00.o().g.b(currentUser.getEmail());
                    return;
                }
                return;
            }
            return;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final void a(int i2) {
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, com.fossil.re] */
    public void attachBaseContext(Context context) {
        wg6.b(context, "base");
        PortfolioApp.super.attachBaseContext(context);
        qe.d(this);
    }

    @DexIgnore
    public final String e() {
        an4 an4 = this.c;
        if (an4 != null) {
            String b2 = an4.b();
            return b2 != null ? b2 : "";
        }
        wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final MutableLiveData<String> f() {
        an4 an4 = this.c;
        if (an4 != null) {
            String b2 = an4.b();
            if (!wg6.a((Object) b2, (Object) (String) this.B.a())) {
                this.B.a(b2);
            }
            return this.B;
        }
        wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final y04 g() {
        y04 y04 = this.G;
        if (y04 != null) {
            return y04;
        }
        wg6.d("applicationComponent");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x005c  */
    public final String h() {
        List<T> list;
        Object[] array;
        boolean z2;
        if (!U) {
            return "4.3.0";
        }
        List<String> split = new mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split("4.3.0", 0);
        if (!split.isEmpty()) {
            ListIterator<String> listIterator = split.listIterator(split.size());
            while (true) {
                if (!listIterator.hasPrevious()) {
                    break;
                }
                if (listIterator.previous().length() == 0) {
                    z2 = true;
                    continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (!z2) {
                    list = yd6.c(split, listIterator.nextIndex() + 1);
                    break;
                }
            }
            array = list.toArray(new String[0]);
            if (array == null) {
                String[] strArr = (String[]) array;
                if (!(strArr.length == 0)) {
                    return strArr[0];
                }
                return "4.3.0";
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
        list = qd6.a();
        array = list.toArray(new String[0]);
        if (array == null) {
        }
    }

    @DexIgnore
    public final String i() {
        String name = qh4.fromInt(Integer.parseInt(w24.y.d())).getName();
        wg6.a((Object) name, "FossilBrand.fromInt(Inte\u2026onfig.brandId)).getName()");
        return name;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007e  */
    public final UserProfile j() {
        int i2;
        int i3;
        double d2;
        int i4;
        int i5;
        int i6;
        int i7;
        UserBiometricData.BiometricWearingPosition biometricWearingPosition;
        int i8;
        int i9;
        List<T> list;
        Object[] array;
        boolean z2;
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            MFUser currentUser = userRepository.getCurrentUser();
            if (currentUser == null) {
                return null;
            }
            String birthday = currentUser.getBirthday();
            int i10 = 0;
            if (TextUtils.isEmpty(birthday)) {
                i2 = 0;
            } else if (birthday != null) {
                List<String> split = new mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(birthday, 0);
                if (!split.isEmpty()) {
                    ListIterator<String> listIterator = split.listIterator(split.size());
                    while (true) {
                        if (!listIterator.hasPrevious()) {
                            break;
                        }
                        if (listIterator.previous().length() == 0) {
                            z2 = true;
                            continue;
                        } else {
                            z2 = false;
                            continue;
                        }
                        if (!z2) {
                            list = yd6.c(split, listIterator.nextIndex() + 1);
                            break;
                        }
                    }
                    array = list.toArray(new String[0]);
                    if (array == null) {
                        int i11 = Calendar.getInstance().get(1);
                        Integer valueOf = Integer.valueOf(((String[]) array)[0]);
                        wg6.a((Object) valueOf, "Integer.valueOf(s[0])");
                        i2 = i11 - valueOf.intValue();
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                list = qd6.a();
                array = list.toArray(new String[0]);
                if (array == null) {
                }
            } else {
                wg6.a();
                throw null;
            }
            float e2 = zj4.e((float) (currentUser.getWeightInGrams() > 0 ? currentUser.getWeightInGrams() : 68039));
            float d3 = zj4.d((float) (currentUser.getHeightInCentimeters() > 0 ? currentUser.getHeightInCentimeters() : 170));
            SummariesRepository summariesRepository = this.e;
            if (summariesRepository != null) {
                ActivitySettings currentActivitySettings = summariesRepository.getCurrentActivitySettings();
                int component1 = currentActivitySettings.component1();
                int component2 = currentActivitySettings.component2();
                int component3 = currentActivitySettings.component3();
                SummariesRepository summariesRepository2 = this.e;
                if (summariesRepository2 != null) {
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "Calendar.getInstance()");
                    ActivitySummary summary = summariesRepository2.getSummary(instance);
                    SleepSummariesRepository sleepSummariesRepository = this.f;
                    if (sleepSummariesRepository != null) {
                        Calendar instance2 = Calendar.getInstance();
                        wg6.a((Object) instance2, "Calendar.getInstance()");
                        Date time = instance2.getTime();
                        wg6.a((Object) time, "Calendar.getInstance().time");
                        MFSleepDay sleepSummaryFromDb = sleepSummariesRepository.getSleepSummaryFromDb(time);
                        ik4 ik4 = this.u;
                        if (ik4 != null) {
                            float a2 = ik4.a(new Date(), true);
                            double d4 = 0.0d;
                            if (summary != null) {
                                i3 = summary.getActiveTime();
                                d2 = summary.getCalories();
                                d4 = ((double) 100) * summary.getDistance();
                            } else {
                                d2 = 0.0d;
                                i3 = 0;
                            }
                            if (sleepSummaryFromDb != null) {
                                int sleepMinutes = sleepSummaryFromDb.getSleepMinutes() + 0;
                                SleepDistribution sleepStateDistInMinute = sleepSummaryFromDb.getSleepStateDistInMinute();
                                if (sleepStateDistInMinute != null) {
                                    int component12 = sleepStateDistInMinute.component1();
                                    i8 = sleepStateDistInMinute.component2() + 0;
                                    i9 = 0 + sleepStateDistInMinute.component3();
                                    i10 = component12 + 0;
                                } else {
                                    i9 = 0;
                                    i8 = 0;
                                }
                                i7 = sleepMinutes;
                                i6 = i10;
                                i4 = i9;
                                i5 = i8;
                            } else {
                                i7 = 0;
                                i6 = 0;
                                i5 = 0;
                                i4 = 0;
                            }
                            UserBiometricData.BiometricGender biometricGender = UserBiometricData.BiometricGender.UNSPECIFIED;
                            if (currentUser.getGender() == rh4.MALE) {
                                biometricGender = UserBiometricData.BiometricGender.MALE;
                            } else if (currentUser.getGender() == rh4.FEMALE) {
                                biometricGender = UserBiometricData.BiometricGender.FEMALE;
                            }
                            an4 an4 = this.c;
                            if (an4 != null) {
                                String l2 = an4.l();
                                if (l2 != null) {
                                    int hashCode = l2.hashCode();
                                    if (hashCode != -1867896629) {
                                        if (hashCode != 647131558) {
                                            if (hashCode == 660843126 && l2.equals("left wrist")) {
                                                biometricWearingPosition = UserBiometricData.BiometricWearingPosition.LEFT_WRIST;
                                                return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, xi4.a(currentUser), i7, i6, i5, i4, (InactiveNudgeData) null, false, -1, -1, System.currentTimeMillis());
                                            }
                                        } else if (l2.equals("unspecified wrist")) {
                                            biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED_WRIST;
                                            return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, xi4.a(currentUser), i7, i6, i5, i4, (InactiveNudgeData) null, false, -1, -1, System.currentTimeMillis());
                                        }
                                    } else if (l2.equals("right wrist")) {
                                        biometricWearingPosition = UserBiometricData.BiometricWearingPosition.RIGHT_WRIST;
                                        return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, xi4.a(currentUser), i7, i6, i5, i4, (InactiveNudgeData) null, false, -1, -1, System.currentTimeMillis());
                                    }
                                }
                                biometricWearingPosition = UserBiometricData.BiometricWearingPosition.UNSPECIFIED;
                                return new UserProfile(new UserBiometricData(i2, biometricGender, d3, e2, biometricWearingPosition), (long) component1, (long) a2, component3, i3, (long) component2, (long) d2, (long) d4, xi4.a(currentUser), i7, i6, i5, i4, (InactiveNudgeData) null, false, -1, -1, System.currentTimeMillis());
                            }
                            wg6.d("sharedPreferencesManager");
                            throw null;
                        }
                        wg6.d("mFitnessHelper");
                        throw null;
                    }
                    wg6.d("mSleepSummariesRepository");
                    throw null;
                }
                wg6.d("mSummariesRepository");
                throw null;
            }
            wg6.d("mSummariesRepository");
            throw null;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final Date k() {
        MFUser b2 = zm4.p.a().n().b();
        if (b2 != null) {
            String createdAt = b2.getCreatedAt();
            if (!TextUtils.isEmpty(createdAt)) {
                Date a2 = zk4.a(bk4.d(createdAt));
                wg6.a((Object) a2, "TimeHelper.getStartOfDay(registeredDate)");
                return a2;
            }
        }
        FLogger.INSTANCE.getLocal().d(R, "Fail to getCurrentUserRegisteringDate, return new Date(1, 1, 1)");
        return new Date(1, 1, 1);
    }

    @DexIgnore
    public final long l(String str) {
        wg6.b(str, "serial");
        return a(str, 1, 1, true);
    }

    @DexIgnore
    public final String m() {
        String i2 = i();
        if (wg6.a((Object) i2, (Object) qh4.CITIZEN.getName())) {
            return "citizen";
        }
        return wg6.a((Object) i2, (Object) qh4.UNIVERSAL.getName()) ? "universal" : "default";
    }

    @DexIgnore
    public final FossilDeviceSerialPatternUtil.BRAND n() {
        String i2 = i();
        if (wg6.a((Object) i2, (Object) qh4.CHAPS.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.CHAPS;
        }
        if (wg6.a((Object) i2, (Object) qh4.DIESEL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.DIESEL;
        }
        if (wg6.a((Object) i2, (Object) qh4.EA.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.EA;
        }
        if (wg6.a((Object) i2, (Object) qh4.KATESPADE.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.KATE_SPADE;
        }
        if (wg6.a((Object) i2, (Object) qh4.MICHAELKORS.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.MICHAEL_KORS;
        }
        if (wg6.a((Object) i2, (Object) qh4.SKAGEN.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.SKAGEN;
        }
        if (wg6.a((Object) i2, (Object) qh4.AX.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.ARMANI_EXCHANGE;
        }
        if (wg6.a((Object) i2, (Object) qh4.RELIC.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.RELIC;
        }
        if (wg6.a((Object) i2, (Object) qh4.MJ.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.MARC_JACOBS;
        }
        if (wg6.a((Object) i2, (Object) qh4.FOSSIL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.FOSSIL;
        }
        if (wg6.a((Object) i2, (Object) qh4.UNIVERSAL.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.UNIVERSAL;
        }
        if (wg6.a((Object) i2, (Object) qh4.CITIZEN.getName())) {
            return FossilDeviceSerialPatternUtil.BRAND.CITIZEN;
        }
        return FossilDeviceSerialPatternUtil.BRAND.UNKNOWN;
    }

    @DexIgnore
    public final String o() {
        Locale locale = Locale.getDefault();
        wg6.a((Object) locale, "locale");
        String language = locale.getLanguage();
        String country = locale.getCountry();
        if (TextUtils.isEmpty(language)) {
            return "";
        }
        if (wg6.a((Object) language, (Object) "iw")) {
            language = "he";
        }
        if (wg6.a((Object) language, (Object) "in")) {
            language = "id";
        }
        if (wg6.a((Object) language, (Object) "ji")) {
            language = "yi";
        }
        if (!TextUtils.isEmpty(country)) {
            nh6 nh6 = nh6.a;
            Locale locale2 = Locale.US;
            wg6.a((Object) locale2, "Locale.US");
            Object[] objArr = {language, country};
            language = String.format(locale2, "%s-%s", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) language, "java.lang.String.format(locale, format, *args)");
        }
        wg6.a((Object) language, "localeString");
        return language;
    }

    @DexIgnore
    public void onActivityCreated(Activity activity, Bundle bundle) {
        wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.CREATE;
    }

    @DexIgnore
    public void onActivityDestroyed(Activity activity) {
        wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.DESTROY;
    }

    @DexIgnore
    public void onActivityPaused(Activity activity) {
        wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.PAUSE;
        if (U) {
            ShakeFeedbackService shakeFeedbackService = this.v;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.e();
            } else {
                wg6.d("mShakeFeedbackService");
                throw null;
            }
        }
        this.D = true;
        Runnable runnable = this.F;
        if (runnable != null) {
            this.E.removeCallbacks(runnable);
        }
        this.E.postDelayed(new g(this), 500);
    }

    @DexIgnore
    public void onActivityResumed(Activity activity) {
        wg6.b(activity, Constants.ACTIVITY);
        if (U) {
            ShakeFeedbackService shakeFeedbackService = this.v;
            if (shakeFeedbackService != null) {
                shakeFeedbackService.a((Context) activity);
            } else {
                wg6.d("mShakeFeedbackService");
                throw null;
            }
        }
        ActivityState activityState = ActivityState.RESUME;
        this.D = false;
        boolean z2 = !this.C;
        this.C = true;
        Runnable runnable = this.F;
        if (runnable != null) {
            this.E.removeCallbacks(runnable);
        }
        if (z2) {
            S = true;
            FLogger.INSTANCE.getLocal().d(R, "from background");
            jl4 jl4 = this.K;
            if (jl4 != null) {
                jl4.d();
            }
        } else {
            S = false;
            FLogger.INSTANCE.getLocal().d(R, "still foreground");
        }
        if (this.I) {
            a(this.J);
        }
    }

    @DexIgnore
    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        wg6.b(activity, Constants.ACTIVITY);
    }

    @DexIgnore
    public void onActivityStarted(Activity activity) {
        wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.START;
    }

    @DexIgnore
    public void onActivityStopped(Activity activity) {
        wg6.b(activity, Constants.ACTIVITY);
        ActivityState activityState = ActivityState.STOP;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r10v0, types: [android.app.Application$ActivityLifecycleCallbacks, android.content.Context, com.portfolio.platform.PortfolioApp, android.app.Application, java.lang.Object] */
    public void onCreate() {
        PortfolioApp.super.onCreate();
        T = this;
        U = (getApplicationContext().getApplicationInfo().flags & 2) != 0;
        if (D()) {
            b14 b14 = new b14(this);
            o24.i e0 = o24.e0();
            e0.a(b14);
            y04 a2 = e0.a();
            wg6.a((Object) a2, "DaggerApplicationCompone\u2026\n                .build()");
            this.G = a2;
            y04 y04 = this.G;
            if (y04 != null) {
                y04.a((PortfolioApp) this);
                System.loadLibrary("FitnessAlgorithm");
                LifecycleOwner g2 = nd.g();
                wg6.a((Object) g2, "ProcessLifecycleOwner.get()");
                Lifecycle lifecycle = g2.getLifecycle();
                ApplicationEventListener applicationEventListener = this.s;
                if (applicationEventListener != null) {
                    lifecycle.a(applicationEventListener);
                    MicroAppEventLogger.initialize(this);
                    z();
                    if (!F()) {
                        Stetho.initializeWithDefaults(this);
                    }
                    rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new h(this, (xe6) null), 3, (Object) null);
                    FLogger.INSTANCE.getLocal().d(R, "On app create");
                    V = new ti4(q06.a);
                    MutableLiveData<String> mutableLiveData = this.B;
                    an4 an4 = this.c;
                    if (an4 != null) {
                        mutableLiveData.b(an4.b());
                        registerActivityLifecycleCallbacks(this);
                        MusicControlComponent musicControlComponent = (MusicControlComponent) MusicControlComponent.o.a(this);
                        DianaPresetRepository dianaPresetRepository = this.w;
                        if (dianaPresetRepository != null) {
                            musicControlComponent.a(dianaPresetRepository);
                            String str = "4.3.0";
                            if (U) {
                                Object[] array = new mj6(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR).split(str, 2).toArray(new String[0]);
                                if (array != null) {
                                    str = ((String[]) array)[0];
                                } else {
                                    throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                                }
                            }
                            GuestApiService guestApiService = this.h;
                            if (guestApiService != null) {
                                this.a = new LocalizationManager(this, str, guestApiService);
                                this.b = new AppPackageInstallReceiver();
                                q24.a();
                                registerReceiver(this.a, new IntentFilter("android.intent.action.LOCALE_CHANGED"));
                                L();
                                LocalizationManager localizationManager = this.a;
                                if (localizationManager != null) {
                                    registerActivityLifecycleCallbacks(localizationManager.a());
                                    LocalizationManager localizationManager2 = this.a;
                                    if (localizationManager2 != null) {
                                        String simpleName = SplashScreenActivity.class.getSimpleName();
                                        wg6.a((Object) simpleName, "SplashScreenActivity::class.java.simpleName");
                                        localizationManager2.a(simpleName);
                                        LocalizationManager localizationManager3 = this.a;
                                        if (localizationManager3 != null) {
                                            localizationManager3.i();
                                            ResolutionHelper.INSTANCE.initDeviceDensity(this);
                                            xm4 xm4 = xm4.d;
                                            an4 an42 = this.c;
                                            if (an42 != null) {
                                                xm4.a(an42);
                                                if (U) {
                                                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().detectCustomSlowCalls().penaltyLog().build());
                                                    StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects().detectActivityLeaks().detectLeakedRegistrationObjects().penaltyLog().build());
                                                }
                                                try {
                                                    if (Build.VERSION.SDK_INT >= 24) {
                                                        this.M = new NetworkChangedReceiver();
                                                        registerReceiver(this.M, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                                                    }
                                                } catch (Exception e2) {
                                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                                    String str2 = R;
                                                    local.e(str2, "onCreate - jobScheduler - ex=" + e2);
                                                }
                                                M();
                                                IntentFilter intentFilter = new IntentFilter();
                                                intentFilter.addAction("android.intent.action.TIME_TICK");
                                                intentFilter.addAction("android.intent.action.TIMEZONE_CHANGED");
                                                registerReceiver(this.N, intentFilter);
                                                O();
                                                this.H = Thread.getDefaultUncaughtExceptionHandler();
                                                Thread.setDefaultUncaughtExceptionHandler(new i(this));
                                                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                                                StrictMode.setVmPolicy(builder.build());
                                                builder.detectFileUriExposure();
                                                AlarmHelper alarmHelper = this.g;
                                                if (alarmHelper != null) {
                                                    alarmHelper.c(this);
                                                    AlarmHelper alarmHelper2 = this.g;
                                                    if (alarmHelper2 != null) {
                                                        alarmHelper2.d(this);
                                                        this.K = AnalyticsHelper.f.b("app_appearance");
                                                        AnalyticsHelper analyticsHelper = this.r;
                                                        if (analyticsHelper != null) {
                                                            analyticsHelper.b("diana_sdk_version", ButtonService.Companion.getSdkVersionV2());
                                                            b();
                                                            return;
                                                        }
                                                        wg6.d("mAnalyticsHelper");
                                                        throw null;
                                                    }
                                                    wg6.d("mAlarmHelper");
                                                    throw null;
                                                }
                                                wg6.d("mAlarmHelper");
                                                throw null;
                                            }
                                            wg6.d("sharedPreferencesManager");
                                            throw null;
                                        }
                                        wg6.a();
                                        throw null;
                                    }
                                    wg6.a();
                                    throw null;
                                }
                                wg6.a();
                                throw null;
                            }
                            wg6.d("mGuestApiService");
                            throw null;
                        }
                        wg6.d("mDianaPresetRepository");
                        throw null;
                    }
                    wg6.d("sharedPreferencesManager");
                    throw null;
                }
                wg6.d("mAppEventManager");
                throw null;
            }
            wg6.d("applicationComponent");
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void onLowMemory() {
        PortfolioApp.super.onLowMemory();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "Inside " + R + ".onLowMemory");
        System.runFinalization();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void onTerminate() {
        PortfolioApp.super.onTerminate();
        FLogger.INSTANCE.getLocal().d(R, "---Inside .onTerminate of Application");
        T();
        unregisterReceiver(this.a);
        S();
        LocalizationManager localizationManager = this.a;
        unregisterActivityLifecycleCallbacks(localizationManager != null ? localizationManager.a() : null);
        if (Build.VERSION.SDK_INT >= 24) {
            FLogger.INSTANCE.getLocal().d(R, "unregister NetworkChangedReceiver");
            unregisterReceiver(this.M);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void onTrimMemory(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "Inside " + R + ".onTrimMemory");
        PortfolioApp.super.onTrimMemory(i2);
        System.runFinalization();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String p() {
        Resources resources = getResources();
        wg6.a((Object) resources, "resources");
        String languageTag = a8.a(resources.getConfiguration()).a(0).toLanguageTag();
        wg6.a((Object) languageTag, "ConfigurationCompat.getL\u2026ation)[0].toLanguageTag()");
        return languageTag;
    }

    @DexIgnore
    public final ApiServiceV2 q() {
        ApiServiceV2 apiServiceV2 = this.j;
        if (apiServiceV2 != null) {
            return apiServiceV2;
        }
        wg6.d("mApiService");
        throw null;
    }

    @DexIgnore
    public final LocalizationManager r() {
        return this.a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String s() {
        int myPid = Process.myPid();
        Object systemService = getSystemService(Constants.ACTIVITY);
        if (systemService != null) {
            for (ActivityManager.RunningAppProcessInfo next : ((ActivityManager) systemService).getRunningAppProcesses()) {
                if (next.pid == myPid) {
                    return next.processName;
                }
            }
            return null;
        }
        throw new rc6("null cannot be cast to non-null type android.app.ActivityManager");
    }

    @DexIgnore
    public final ThemeRepository t() {
        ThemeRepository themeRepository = this.Q;
        if (themeRepository != null) {
            return themeRepository;
        }
        wg6.d("mThemeRepository");
        throw null;
    }

    @DexIgnore
    public final UserRepository u() {
        UserRepository userRepository = this.d;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    public final WatchLocalizationRepository v() {
        WatchLocalizationRepository watchLocalizationRepository = this.y;
        if (watchLocalizationRepository != null) {
            return watchLocalizationRepository;
        }
        wg6.d("mWatchLocalizationRepository");
        throw null;
    }

    @DexIgnore
    public final an4 w() {
        an4 an4 = this.c;
        if (an4 != null) {
            return an4;
        }
        wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final WatchFaceRepository x() {
        WatchFaceRepository watchFaceRepository = this.A;
        if (watchFaceRepository != null) {
            return watchFaceRepository;
        }
        wg6.d("watchFaceRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final boolean y() {
        Object systemService = getSystemService("connectivity");
        if (systemService != null) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) systemService).getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        }
        throw new rc6("null cannot be cast to non-null type android.net.ConnectivityManager");
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void z() {
        FLogger.INSTANCE.getLocal().d(R, "initFabric");
        c86.c cVar = new c86.c(this);
        cVar.a(new v00());
        cVar.a(true);
        c86.d(cVar.a());
        v00.o().g.a("SDK Version V2", ButtonService.Companion.getSdkVersionV2());
        v00.o().g.a("Locale", Locale.getDefault().toString());
        W();
    }

    @DexIgnore
    public final void b(boolean z2) {
        this.I = z2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void c(String str, String str2) {
        wg6.b(str, "serial");
        if (TextUtils.isEmpty(str) || DeviceHelper.o.e(str)) {
            String e2 = e();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, "Inside " + R + ".setActiveDeviceSerial - current=" + e2 + ", new=" + str + ", newDevice mac address=" + str2);
            if (str2 == null) {
                str2 = "";
            }
            an4 an4 = this.c;
            if (an4 != null) {
                an4.o(str);
                if (e2 != str) {
                    this.B.a(str);
                }
                U();
                try {
                    IButtonConnectivity iButtonConnectivity = W;
                    if (iButtonConnectivity != null) {
                        iButtonConnectivity.setActiveSerial(str, str2);
                        FossilNotificationBar.c.a(this);
                        return;
                    }
                    wg6.a();
                    throw null;
                } catch (Exception e3) {
                    e3.printStackTrace();
                }
            } else {
                wg6.d("sharedPreferencesManager");
                throw null;
            }
        } else {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.d(str4, "Ignore legacy device serial=" + str);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String d() {
        String str;
        String e2 = e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "generateDeviceNotificationContent activeSerial=" + e2);
        if (TextUtils.isEmpty(e2)) {
            Object r0 = T;
            if (r0 != 0) {
                String a2 = jm4.a((Context) r0, 2131886813);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026Dashboard_CTA__PairWatch)");
                return a2;
            }
            wg6.d("instance");
            throw null;
        }
        String a3 = jm4.a((Context) this, 2131886926);
        DeviceRepository deviceRepository = this.t;
        if (deviceRepository != null) {
            String deviceNameBySerial = deviceRepository.getDeviceNameBySerial(e2);
            try {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = R;
                StringBuilder sb = new StringBuilder();
                sb.append("generateDeviceNotificationContent gattState=");
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    sb.append(iButtonConnectivity.getGattState(e2));
                    local2.d(str3, sb.toString());
                    IButtonConnectivity iButtonConnectivity2 = W;
                    if (iButtonConnectivity2 != null) {
                        if (iButtonConnectivity2.getGattState(e2) == 2) {
                            PortfolioApp portfolioApp = T;
                            if (portfolioApp == null) {
                                wg6.d("instance");
                                throw null;
                            } else if (portfolioApp.i(e2)) {
                                Object r02 = T;
                                if (r02 != 0) {
                                    a3 = jm4.a((Context) r02, 2131886551);
                                } else {
                                    wg6.d("instance");
                                    throw null;
                                }
                            } else {
                                an4 an4 = this.c;
                                if (an4 != null) {
                                    long g2 = an4.g(e2);
                                    if (g2 == 0) {
                                        str = "";
                                    } else if (System.currentTimeMillis() - g2 < 60000) {
                                        nh6 nh6 = nh6.a;
                                        Object r5 = T;
                                        if (r5 != 0) {
                                            String a4 = jm4.a((Context) r5, 2131886942);
                                            wg6.a((Object) a4, "LanguageHelper.getString\u2026ttings_Label__NumbermAgo)");
                                            Object[] objArr = {1};
                                            str = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                                            wg6.a((Object) str, "java.lang.String.format(format, *args)");
                                        } else {
                                            wg6.d("instance");
                                            throw null;
                                        }
                                    } else {
                                        str = bk4.a(g2);
                                    }
                                    nh6 nh62 = nh6.a;
                                    Object r6 = T;
                                    if (r6 != 0) {
                                        String a5 = jm4.a((Context) r6, 2131886932);
                                        wg6.a((Object) a5, "LanguageHelper.getString\u2026_Text__LastSyncedDayTime)");
                                        Object[] objArr2 = {str};
                                        String format = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                                        wg6.a((Object) format, "java.lang.String.format(format, *args)");
                                        a3 = format;
                                    } else {
                                        wg6.d("instance");
                                        throw null;
                                    }
                                } else {
                                    wg6.d("sharedPreferencesManager");
                                    throw null;
                                }
                            }
                        }
                        return deviceNameBySerial + " : " + a3;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            } catch (Exception e3) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str4 = R;
                local3.d(str4, "generateDeviceNotificationContent e=" + e3);
            }
        } else {
            wg6.d("mDeviceRepository");
            throw null;
        }
    }

    @DexIgnore
    public final boolean e(String str) {
        wg6.b(str, "newActiveDeviceSerial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                boolean forceSwitchDeviceWithoutErase = iButtonConnectivity.forceSwitchDeviceWithoutErase(str);
                if (!forceSwitchDeviceWithoutErase) {
                    return forceSwitchDeviceWithoutErase;
                }
                k(str);
                return forceSwitchDeviceWithoutErase;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final int g(String str) {
        wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getCommunicatorModeBySerial(str);
            }
            wg6.a();
            throw null;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    public final boolean i(String str) {
        wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.isSyncing(str);
            }
            wg6.a();
            throw null;
        } catch (Exception unused) {
            return false;
        }
    }

    @DexIgnore
    public final DataValidationManager l() {
        DataValidationManager dataValidationManager = this.z;
        if (dataValidationManager != null) {
            return dataValidationManager;
        }
        wg6.d("mDataValidationManager");
        throw null;
    }

    @DexIgnore
    public final void p(String str) {
        wg6.b(str, "serial");
        UserProfile j2 = j();
        if (j2 != null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "setImplicitDeviceConfig - currentUserProfile=" + j2);
            try {
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.setImplicitDeviceConfig(j2, str);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = R;
                local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
                e2.printStackTrace();
            }
        } else {
            FLogger.INSTANCE.getLocal().e(R, "setImplicitDeviceConfig - currentUserProfile is NULL");
        }
    }

    @DexIgnore
    public final void q(String str) {
        wg6.b(str, ButtonService.USER_ID);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.updateUserId(str);
            }
        } catch (Exception unused) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "exception when set userId to SDK " + str);
        }
    }

    @DexIgnore
    public final boolean r(String str) {
        wg6.b(str, "newActiveDeviceSerial");
        UserProfile j2 = j();
        if (j2 == null) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.e(str2, "Error inside " + R + ".switchActiveDevice - user is null");
            return false;
        }
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                boolean switchActiveDevice = iButtonConnectivity.switchActiveDevice(str, j2);
                if (!switchActiveDevice) {
                    return switchActiveDevice;
                }
                if (!(str.length() == 0)) {
                    return switchActiveDevice;
                }
                k(str);
                return switchActiveDevice;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    @DexIgnore
    public final void b(ServerError serverError) {
        this.J = serverError;
    }

    @DexIgnore
    public final void b(String str, NotificationBaseObj notificationBaseObj) {
        wg6.b(str, "serial");
        wg6.b(notificationBaseObj, "notification");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "sendNotificationToDevice packageName " + notificationBaseObj);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSendNotification(str, notificationBaseObj);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".sendDianaNotification() - e=" + e2);
        }
    }

    @DexIgnore
    public final void m(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, ".removeActivePreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removeActiveSerial(str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "removeActivePreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(boolean z2) {
        this.C = z2;
    }

    @DexIgnore
    public final void a(kl4 kl4) {
        jl4 jl4;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "Active view tracer old: " + this.L + " \n new: " + kl4);
        if (kl4 != null && wg6.a((Object) kl4.a(), (Object) "view_appearance")) {
            kl4 kl42 = this.L;
            if (!(kl42 == null || kl42 == null || kl42.a(kl4) || (jl4 = this.K) == null)) {
                hl4 a2 = AnalyticsHelper.f.a("app_appearance_view_navigate");
                kl4 kl43 = this.L;
                if (kl43 != null) {
                    String e2 = kl43.e();
                    if (e2 != null) {
                        a2.a("prev_view", e2);
                        String e3 = kl4.e();
                        if (e3 != null) {
                            a2.a("next_view", e3);
                            jl4.a(a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            this.L = kl4;
        }
    }

    @DexIgnore
    public final void e(String str, String str2) {
        wg6.b(str2, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, ".setSecretKeyToDevice(), secretKey=" + str);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setSecretKey(str2, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, ".setSecretKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String f(String str) {
        wg6.b(str, "packageName");
        String str2 = (String) yd6.f(yj6.a((CharSequence) str, new String[]{"."}, false, 0, 6, (Object) null));
        Object r1 = T;
        if (r1 != 0) {
            PackageManager packageManager = r1.getPackageManager();
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
                return applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo).toString() : str2;
            } catch (PackageManager.NameNotFoundException unused) {
                return str2;
            }
        } else {
            wg6.d("instance");
            throw null;
        }
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "stopLogService - failureCode=" + i2);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.stopLogService(i2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local2.e(str2, "stopLogService - ex=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r5v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void k(String str) {
        wg6.b(str, "newActiveDeviceSerial");
        String e2 = e();
        an4 an4 = this.c;
        if (an4 != null) {
            an4.o(str);
            this.B.a(str);
            FossilNotificationBar.c.a(this);
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "onSwitchActiveDeviceSuccess - current=" + e2 + ", new=" + str);
            return;
        }
        wg6.d("sharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    public final void s(String str) {
        wg6.b(str, "serial");
        if (!TextUtils.isEmpty(str)) {
            try {
                if (xj6.b(str, e(), true)) {
                    an4 an4 = this.c;
                    if (an4 != null) {
                        an4.o("");
                        this.B.a("");
                    } else {
                        wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                }
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.deviceUnlink(str);
                    an4 an42 = this.c;
                    if (an42 != null) {
                        an42.a(str, 0, false);
                    } else {
                        wg6.d("sharedPreferencesManager");
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = R;
                local.e(str2, "Inside " + R + ".unlinkDevice - serial=" + str + ", ex=" + e2);
            }
        }
    }

    @DexIgnore
    public final boolean b(String str, String str2, int i2) {
        wg6.b(str, "serial");
        wg6.b(str2, "serverSecretKey");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, ".sendServerSecretKeyToDevice(), serverSecretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendServerSecretKey(str, str2, i2);
                return true;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, ".sendServerSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void o(String str) {
        String str2;
        int i2;
        if (str == null) {
            i2 = 1024;
            str2 = "";
        } else {
            try {
                i2 = al4.a(str);
                str2 = str;
            } catch (Exception e2) {
                e2.printStackTrace();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = R;
                local.e(str3, "Inside " + R + ".setAutoSecondTimezone - ex=" + e2);
                return;
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str4 = R;
        local2.d(str4, "Inside " + R + ".setAutoSecondTimezone - offsetMinutes=" + i2 + ", mSecondTimezoneId=" + str2);
        IButtonConnectivity iButtonConnectivity = W;
        if (iButtonConnectivity != null) {
            if (str == null) {
                str = "";
            }
            iButtonConnectivity.deviceSetAutoSecondTimezone(str);
            return;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final boolean b(String str, String str2) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, ".sendCurrentSecretKeyToDevice(), secretKey=" + str2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCurrentSecretKey(str, str2);
                return true;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, ".sendCurrentSecretKeyToDevice() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r12v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void c(String str) {
        String str2;
        String str3 = "localization_" + str + '.';
        File[] listFiles = new File(getFilesDir() + "/localization").listFiles();
        int length = listFiles.length;
        int i2 = 0;
        while (true) {
            if (i2 >= length) {
                str2 = "";
                break;
            }
            File file = listFiles[i2];
            wg6.a((Object) file, "file");
            String path = file.getPath();
            wg6.a((Object) path, "file.path");
            if (yj6.a((CharSequence) path, (CharSequence) str3, false, 2, (Object) null)) {
                str2 = file.getPath();
                wg6.a((Object) str2, "file.path");
                break;
            }
            i2++;
        }
        FLogger.INSTANCE.getLocal().d(R, "setLocalization - neededLocalizationFilePath: " + str2);
        if (TextUtils.isEmpty(str2)) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
            return;
        }
        IButtonConnectivity iButtonConnectivity = W;
        if (iButtonConnectivity != null) {
            iButtonConnectivity.setLocalizationData(new LocalizationData(str2, (String) null, 2, (qg6) null));
        }
    }

    @DexIgnore
    public final int h(String str) {
        wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.getGattState(str);
            }
            return 0;
        } catch (Exception unused) {
            return 0;
        }
    }

    @DexIgnore
    public final void n(String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, ".removePairedPreferenceSerial - serial=" + str);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.removePairedSerial(str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "removePairedPreferenceSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, UserProfile userProfile) {
        wg6.b(str, "serial");
        wg6.b(userProfile, "userProfile");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        boolean z2 = false;
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "startDeviceSyncInButtonService - serial=" + str);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    time_stamp_for_non_executable_method = iButtonConnectivity.deviceStartSync(str, userProfile);
                    z2 = true;
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str3 = R;
                local2.e(str3, "startDeviceSyncInButtonService - serial " + str);
            }
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local3.e(str4, "startDeviceSyncInButtonService - serial " + str + " exception " + e2);
        }
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str5 = R;
        local4.d(str5, "put timestamp " + time_stamp_for_non_executable_method + " for sync session");
        return z2;
    }

    @DexIgnore
    public final void b(String str, boolean z2) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, ".onUpdateSecretKeyToServerResponse(), isSuccess=" + z2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendPushSecretKeyResponse(str, z2);
                cd6 cd6 = cd6.a;
                return;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".onUpdateSecretKeyToServerResponse() - e=" + e2);
            cd6 cd62 = cd6.a;
        }
    }

    @DexIgnore
    public final void b(String str, List<? extends BLEMapping> list) {
        wg6.b(str, "serial");
        wg6.b(list, "mappings");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setAutoMapping serial=" + str + "mappingsSize=" + list.size());
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoMapping(str, list);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long b(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wg6.b(appNotificationFilterSettings, "notificationFilterSettings");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setNotificationFilterSettings - notificationFilterSettings=" + appNotificationFilterSettings);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setNotificationFilterSettings(appNotificationFilterSettings, str);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".setNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r4v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(cj4 cj4, boolean z2, int i2) {
        wg6.b(cj4, "factory");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "startDeviceSync, isNewDevice=" + z2 + ", syncMode=" + i2);
        Intent intent = new Intent("BROADCAST_SYNC_COMPLETE");
        intent.putExtra("sync_result", 0);
        intent.putExtra("SERIAL", e());
        ce.a(this).a(intent);
        String e2 = e();
        if (i(e2)) {
            FLogger.INSTANCE.getLocal().d(R, "Device is syncing, Skip this sync.");
        } else {
            cj4.b(e2).a(new bs4(i2, e2, z2), (CoroutineUseCase.e) null);
        }
    }

    @DexIgnore
    public final void c(boolean z2) {
        try {
            String e2 = e();
            if (!z2) {
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    iButtonConnectivity.updatePercentageGoalProgress(e2, false, j());
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                IButtonConnectivity iButtonConnectivity2 = W;
                if (iButtonConnectivity2 != null) {
                    iButtonConnectivity2.updatePercentageGoalProgress(e2, true, j());
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    @DexIgnore
    public final void d(String str, String str2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = R;
        local.d(str3, ".setPairedSerial - serial=" + str + ", macaddress=" + str2);
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "";
        }
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setPairedSerial(str, str2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, "setPairedSerial - serial=" + str + ", ex=" + e2);
        }
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deleteDataFiles(str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.e(str2, "Error while deleting data file - e=" + e2);
        }
    }

    @DexIgnore
    public final long c(String str, boolean z2) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setFrontLightEnable() - serial=" + str + ", isFrontLightEnable=" + z2);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setFrontLightEnable(str, z2);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void d(String str) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "Inside " + R + ".forceReconnectDevice");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceForceReconnect(str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "Error inside " + R + ".deviceReconnect - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void b() {
        ml.a aVar = new ml.a();
        d06 d06 = this.x;
        if (d06 != null) {
            aVar.a(d06);
            ml a2 = aVar.a();
            wg6.a((Object) a2, "Configuration.Builder()\n\u2026\n                .build()");
            bm.a(this, a2);
            PushPendingDataWorker.z.a();
            return;
        }
        wg6.d("mDaggerAwareWorkerFactory");
        throw null;
    }

    @DexIgnore
    public final long c(String str, List<? extends MicroAppMapping> list) {
        wg6.b(str, "serial");
        wg6.b(list, "mappings");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            for (MicroAppMapping microAppMapping : list) {
                if (microAppMapping != null) {
                    StringBuilder sb = new StringBuilder("Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", declarationFiles=");
                    for (String append : microAppMapping.getDeclarationFiles()) {
                        sb.append("||");
                        sb.append(append);
                    }
                    FLogger.INSTANCE.getLocal().d(R, sb.toString());
                    FLogger.INSTANCE.getLocal().d(R, "Inside .setMappings set mapping of deviceId=" + str + ", gesture=" + microAppMapping.getGesture() + ", appID=" + microAppMapping.getMicroAppId() + ", extraInfo=" + Arrays.toString(microAppMapping.getDeclarationFiles()));
                }
            }
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetMapping(str, MicroAppMapping.convertToBLEMapping(list));
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "confirmStopWorkout -serial =" + str + ", stopWorkout=" + z2);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.confirmStopWorkout(str, z2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "confirmStopWorkout - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final void a(String str, boolean z2, WatchParamsFileMapping watchParamsFileMapping) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "onGetWatchParamResponse -serial =" + str + ", content=" + watchParamsFileMapping);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onSetWatchParamResponse(str, z2, watchParamsFileMapping);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "onSetWatchParamResponse - serial " + str + " exception " + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z2, int i2) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, ".switchDeviceResponse(), isSuccess=" + z2 + ", failureCode=" + i2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.switchDeviceResponse(str, z2, i2);
                return true;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r13v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application, java.lang.Object] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object a(xe6<? super cd6> xe6) {
        j jVar;
        int i2;
        if (xe6 instanceof j) {
            jVar = (j) xe6;
            int i3 = jVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                jVar.label = i3 - Integer.MIN_VALUE;
                Object obj = jVar.result;
                Object a2 = ff6.a();
                i2 = jVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    MFUser b2 = zm4.p.a().n().b();
                    if (b2 != null) {
                        Installation installation = new Installation();
                        installation.setAppMarketingVersion("4.3.0");
                        installation.setAppBuildNumber(25287);
                        installation.setModel(Build.MODEL);
                        installation.setOsVersion(Build.VERSION.RELEASE);
                        tj4.a aVar = tj4.f;
                        String userId = b2.getUserId();
                        wg6.a((Object) userId, "currentUser.userId");
                        installation.setInstallationId(aVar.a(userId));
                        installation.setAppPermissions(xx5.a.a());
                        an4 an4 = this.c;
                        if (an4 != null) {
                            installation.setDeviceToken(an4.f());
                            TimeZone timeZone = TimeZone.getDefault();
                            wg6.a((Object) timeZone, "TimeZone.getDefault()");
                            String id = timeZone.getID();
                            wg6.a((Object) id, "zone");
                            if (yj6.a((CharSequence) id, '/', 0, false, 6, (Object) null) > 0) {
                                installation.setTimeZone(id);
                            }
                            try {
                                String packageName = getPackageName();
                                PackageManager packageManager = getPackageManager();
                                String str = packageManager.getPackageInfo(packageName, 0).versionName;
                                String obj2 = packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, 0)).toString();
                                if (!TextUtils.isEmpty(obj2)) {
                                    installation.setAppName(obj2);
                                }
                                if (!TextUtils.isEmpty(str)) {
                                    installation.setAppVersion(str);
                                }
                            } catch (Exception unused) {
                                FLogger.INSTANCE.getLocal().e(R, "Cannot load package info; will not be saved to installation");
                            }
                            installation.setDeviceType("android");
                            a(installation);
                            dl6 b3 = zl6.b();
                            k kVar = new k(this, installation, (xe6) null);
                            jVar.L$0 = this;
                            jVar.L$1 = b2;
                            jVar.L$2 = installation;
                            jVar.L$3 = id;
                            jVar.label = 1;
                            if (gk6.a(b3, kVar, jVar) == a2) {
                                return a2;
                            }
                        } else {
                            wg6.d("sharedPreferencesManager");
                            throw null;
                        }
                    }
                } else if (i2 == 1) {
                    String str2 = (String) jVar.L$3;
                    Installation installation2 = (Installation) jVar.L$2;
                    MFUser mFUser = (MFUser) jVar.L$1;
                    PortfolioApp portfolioApp = (PortfolioApp) jVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        jVar = new j(this, xe6);
        Object obj3 = jVar.result;
        Object a22 = ff6.a();
        i2 = jVar.label;
        if (i2 != 0) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final void c() {
        new c().start();
    }

    @DexIgnore
    public final void j(String str) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, ".onPing(), serial=" + str);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.onPing(str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".onPing() - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(Installation installation) {
        installation.setLocaleIdentifier(o());
    }

    @DexIgnore
    public final long a(String str, int i2) {
        wg6.b(str, "serial");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "Inside " + R + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            if (!TextUtils.isEmpty(str)) {
                IButtonConnectivity iButtonConnectivity = W;
                if (iButtonConnectivity != null) {
                    return iButtonConnectivity.deviceUpdateGoalStep(str, i2);
                }
                wg6.a();
                throw null;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "Error Inside " + R + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2);
            return time_stamp_for_non_executable_method;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local3.e(str4, "Error Inside " + R + ".setUpdateGoalSteps - serial=" + str + ", goalSteps=" + i2 + ", ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, int i2, int i3, boolean z2) {
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "Inside " + R + ".playVibeNotification");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.playVibration(str, i2, i3, true);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "Error inside " + R + ".playVibeNotification - e=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, List<? extends Alarm> list) {
        wg6.b(str, "serial");
        wg6.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setMultipleAlarms - alarms size=" + list.size());
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetListAlarm(str, list);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "setMultipleAlarms - ex=" + e2);
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(List<? extends Alarm> list) {
        wg6.b(list, "alarms");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.e(str, "setAutoAlarms - alarms=" + list);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetAutoListAlarm(list);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local2.e(str2, "setAutoAlarms - e=" + e2);
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final void a() {
        an4 an4 = this.c;
        if (an4 != null) {
            an4.a(53);
            File cacheDir = getCacheDir();
            wg6.a((Object) cacheDir, "cacheDirectory");
            File file = new File(cacheDir.getParent());
            if (file.exists()) {
                for (String str : file.list()) {
                    if (!wg6.a((Object) str, (Object) "lib")) {
                        a(new File(file, str));
                    }
                }
            }
            PendingIntent activity = PendingIntent.getActivity(this, 123456, new Intent(this, SplashScreenActivity.class), 268435456);
            Object systemService = getSystemService(Alarm.TABLE_NAME);
            if (systemService != null) {
                ((AlarmManager) systemService).set(1, System.currentTimeMillis() + ((long) 1000), activity);
                if (19 <= Build.VERSION.SDK_INT) {
                    Object systemService2 = getSystemService(Constants.ACTIVITY);
                    if (systemService2 != null) {
                        ((ActivityManager) systemService2).clearApplicationUserData();
                        return;
                    }
                    throw new rc6("null cannot be cast to non-null type android.app.ActivityManager");
                }
                try {
                    Runtime.getRuntime().exec("pm clear " + getPackageName());
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                throw new rc6("null cannot be cast to non-null type android.app.AlarmManager");
            }
        } else {
            wg6.d("sharedPreferencesManager");
            throw null;
        }
    }

    @DexIgnore
    public final boolean a(File file) {
        if (file == null) {
            return true;
        }
        if (!file.isDirectory()) {
            return file.delete();
        }
        String[] list = file.list();
        int length = list.length;
        boolean z2 = true;
        for (int i2 = 0; i2 < length; i2++) {
            z2 = a(new File(file, list[i2])) && z2;
        }
        return z2;
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, String str2) {
        wg6.b(communicateMode, "communicateMode");
        wg6.b(str, "serial");
        wg6.b(str2, "message");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = R;
        local.d(str3, "addLog - communicateMode=" + communicateMode + ", serial=" + str + ", message=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.addLog(communicateMode.ordinal(), str, str2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, "addLog - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(CommunicateMode communicateMode, String str, CommunicateMode communicateMode2, String str2) {
        wg6.b(communicateMode, "curCommunicateMode");
        wg6.b(str, "curSerial");
        wg6.b(communicateMode2, "newCommunicateMode");
        wg6.b(str2, "newSerial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = R;
        local.d(str3, "changePendingLogKey - curCommunicateMode=" + communicateMode + ", curSerial=" + str + ", newCommunicateMode=" + communicateMode2 + ", newSerial=" + str2);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.changePendingLogKey(communicateMode.ordinal(), str, communicateMode2.ordinal(), str2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, "changePendingLogKey - ex=" + e2);
        }
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5) {
        String e2 = e();
        int min = Math.min(i2, 65535);
        int min2 = Math.min(i3, 65535);
        int min3 = Math.min(i4, 65535);
        int min4 = Math.min(i5, 4294967);
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = R;
            local.d(str, "Inside simulateDisconnection - delay=" + min + ", duration=" + min2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.simulateDisconnection(e2, min, min2, min3, min4);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e3) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local2.e(str2, "Error inside " + R + ".simulateDisconnection - e=" + e3);
        }
    }

    @DexIgnore
    public final long a(String str, CustomRequest customRequest) {
        wg6.b(str, "serial");
        wg6.b(customRequest, Constants.COMMAND);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "sendCustomCommand() - serial=" + str + ", command=" + customRequest);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendCustomCommand(str, customRequest);
                return time_stamp_for_non_executable_method;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005a, code lost:
        if (com.fossil.yj6.a((java.lang.CharSequence) r6, (java.lang.CharSequence) com.misfit.frameworks.common.constants.Constants.PROFILE_KEY_IMAGE, false, 2, (java.lang.Object) null) == false) goto L_0x0061;
     */
    @DexIgnore
    public final boolean a(Intent intent, Uri uri) {
        wg6.b(uri, "fileUri");
        String a2 = a(uri);
        String type = intent != null ? intent.getType() : "";
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "isImageFile intentMimeType=" + type + ", uriMimeType=" + a2);
        String path = uri.getPath();
        if (path != null) {
            wg6.a((Object) path, "fileUri.path!!");
            if (!yj6.a((CharSequence) path, (CharSequence) "pickerImage", false, 2, (Object) null)) {
                if (!TextUtils.isEmpty(type)) {
                    if (type == null) {
                        wg6.a();
                        throw null;
                    }
                }
                if (TextUtils.isEmpty(a2)) {
                    return false;
                }
                if (a2 == null) {
                    wg6.a();
                    throw null;
                } else if (yj6.a((CharSequence) a2, (CharSequence) Constants.PROFILE_KEY_IMAGE, false, 2, (Object) null)) {
                    return true;
                } else {
                    return false;
                }
            }
            return true;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String a(Uri uri) {
        if (wg6.a((Object) uri.getScheme(), (Object) "content")) {
            return getContentResolver().getType(uri);
        }
        String path = uri.getPath();
        if (path != null) {
            return MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(path)).toString()));
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final void a(String str, String str2) {
        wg6.b(str, "serial");
        wg6.b(str2, "macAddress");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, "Inside " + R + ".pairDevice");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDevice(str, str2, j());
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, "Error inside " + R + ".pairDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final boolean a(String str, PairingResponse pairingResponse) {
        wg6.b(str, "serial");
        wg6.b(pairingResponse, "response");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, ".pairDeviceResponse(), response=" + pairingResponse);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.pairDeviceResponse(str, pairingResponse);
                return true;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".pairDeviceResponse() - e=" + e2);
            return false;
        }
    }

    @DexIgnore
    public final void a(String str, String str2, int i2) {
        wg6.b(str, "serial");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local.d(str3, ".sendRandomKeyToDevice(), randomKey=" + str2);
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendRandomKey(str, str2, i2);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str4 = R;
            local2.e(str4, ".sendRandomKeyToDevice() - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "serial");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.cancelPairDevice(str);
                cd6 cd6 = cd6.a;
                return;
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.e(str2, ".cancelPairDevice() - e=" + e2);
            cd6 cd62 = cd6.a;
        }
    }

    @DexIgnore
    public final void a(String str, VibrationStrengthObj vibrationStrengthObj) {
        wg6.b(str, "serial");
        wg6.b(vibrationStrengthObj, "vibrationStrengthObj");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "Inside " + R + ".setVibrationStrength");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceSetVibrationStrength(str, vibrationStrengthObj);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "Error inside " + R + ".setVibrationStrength - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(String str, FirmwareData firmwareData, UserProfile userProfile) {
        wg6.b(str, "serial");
        wg6.b(firmwareData, "firmwareData");
        wg6.b(userProfile, "userProfile");
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "Inside " + R + ".otaDevice");
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.deviceOta(str, firmwareData, userProfile);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, "Error inside " + R + ".otaDevice - e=" + e2);
        }
    }

    @DexIgnore
    public final void a(ComplicationAppMappingSettings complicationAppMappingSettings, String str) {
        wg6.b(complicationAppMappingSettings, "complicationAppMappingSettings");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setComplicationApps - complicationAppsSettings=" + complicationAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoComplicationAppSettings(complicationAppMappingSettings, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(DeviceAppResponse deviceAppResponse, String str) {
        wg6.b(deviceAppResponse, "deviceAppResponse");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "sendComplicationAppInfo - deviceAppResponse=" + deviceAppResponse);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendDeviceAppResponse(deviceAppResponse, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(MusicResponse musicResponse, String str) {
        wg6.b(musicResponse, "musicAppResponse");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "sendMusicAppResponse - musicAppResponse=" + musicResponse);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMusicAppResponse(musicResponse, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final long a(WatchAppMappingSettings watchAppMappingSettings, ComplicationAppMappingSettings complicationAppMappingSettings, BackgroundConfig backgroundConfig, String str) {
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setPresetApps - watchAppMappingSettings=" + watchAppMappingSettings + ", complicationAppsSettings=" + complicationAppMappingSettings + ", backgroundConfig=" + backgroundConfig);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setPresetApps(watchAppMappingSettings, complicationAppMappingSettings, backgroundConfig, str);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(ReplyMessageMappingGroup replyMessageMappingGroup, String str) {
        wg6.b(replyMessageMappingGroup, "replyMessageGroup");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setReplyMessageMapping - replyMessageGroup=" + replyMessageMappingGroup);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.setReplyMessageMappingSetting(replyMessageMappingGroup, str);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".setReplyMessageMappingSetting(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final long a(String str, NotificationBaseObj notificationBaseObj) {
        wg6.b(str, "serial");
        wg6.b(notificationBaseObj, "notification");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "notifyNotificationControlEvent - notification=" + notificationBaseObj);
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.notifyNotificationEvent(notificationBaseObj, str);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".notifyNotificationControlEvent(), e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    public final void a(WatchAppMappingSettings watchAppMappingSettings, String str) {
        wg6.b(watchAppMappingSettings, "watchAppMappingSettings");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setAutoWatchAppSettings - watchAppMappingSettings=" + watchAppMappingSettings);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoWatchAppSettings(watchAppMappingSettings, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(BackgroundConfig backgroundConfig, String str) {
        wg6.b(backgroundConfig, "backgroundConfig");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setAutoBackgroundImageConfig - backgroundConfig=" + backgroundConfig);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoBackgroundImageConfig(backgroundConfig, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(AppNotificationFilterSettings appNotificationFilterSettings, String str) {
        wg6.b(appNotificationFilterSettings, "notificationFilterSettings");
        wg6.b(str, "serial");
        for (AppNotificationFilter appNotificationFilter : appNotificationFilterSettings.getNotificationFilters()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.d(str2, "setAutoNotificationFilterSettings " + appNotificationFilter);
        }
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoNotificationFilterSettings(appNotificationFilterSettings, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".setAutoNotificationFilterSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserDisplayUnit userDisplayUnit, String str) {
        wg6.b(userDisplayUnit, "userDisplayUnit");
        wg6.b(str, "serial");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = R;
        local.d(str2, "setImplicitDisplayUnitSettings - userDisplayUnit=" + userDisplayUnit);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setImplicitDisplayUnitSettings(userDisplayUnit, str);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = R;
            local2.e(str3, ".setImplicitDisplayUnitSettings(), e=" + e2);
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(UserProfile userProfile) {
        wg6.b(userProfile, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = R;
        local.d(str, "setAutoUserBiometricData - userProfile=" + userProfile);
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.setAutoUserBiometricData(userProfile);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public final void a(String str, DeviceAppResponse deviceAppResponse) {
        wg6.b(str, "serial");
        wg6.b(deviceAppResponse, "deviceAppResponse");
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                iButtonConnectivity.sendMicroAppRemoteActivity(str, deviceAppResponse);
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.e(str2, "Error inside " + R + ".sendMicroAppRemoteActivity - e=" + e2);
        }
    }

    @DexIgnore
    public final long a(String str, InactiveNudgeData inactiveNudgeData) {
        wg6.b(str, "serial");
        wg6.b(inactiveNudgeData, "inactiveNudgeData");
        long time_stamp_for_non_executable_method = ButtonService.Companion.getTIME_STAMP_FOR_NON_EXECUTABLE_METHOD();
        try {
            IButtonConnectivity iButtonConnectivity = W;
            if (iButtonConnectivity != null) {
                return iButtonConnectivity.deviceSetInactiveNudgeConfig(str, inactiveNudgeData);
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = R;
            local.e(str2, ".setInactiveNudgeConfig - e=" + e2);
            e2.printStackTrace();
            return time_stamp_for_non_executable_method;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.CoroutineUseCase$e, com.portfolio.platform.PortfolioApp$e] */
    public final void a(ServerError serverError) {
        FLogger.INSTANCE.getLocal().d(R, "forceLogout");
        if (this.C) {
            Object r0 = this.q;
            if (r0 != 0) {
                r0.a(new DeleteLogoutUserUseCase.b(2, (WeakReference<Activity>) null), new e(this, serverError));
            } else {
                wg6.d("mDeleteLogoutUserUseCase");
                throw null;
            }
        } else {
            this.J = serverError;
            this.I = true;
        }
    }
}
