package com.portfolio.platform.ui.device.domain.usecase;

import android.content.Intent;
import com.fossil.af6;
import com.fossil.hf6;
import com.fossil.ik6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.rm6;
import com.fossil.ur4;
import com.fossil.ur4$e$a;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMapping;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.service.BleCommandResultManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetReplyMessageMappingUseCase extends m24<ur4.b, ur4.d, ur4.c> {
    @DexIgnore
    public boolean d;
    @DexIgnore
    public /* final */ e e; // = new e();
    @DexIgnore
    public /* final */ List<QuickResponseMessage> f; // = new ArrayList();
    @DexIgnore
    public /* final */ QuickResponseRepository g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ List<QuickResponseMessage> a;

        @DexIgnore
        public b(List<QuickResponseMessage> list) {
            wg6.b(list, "quickResponseMessageList");
            this.a = list;
        }

        @DexIgnore
        public final List<QuickResponseMessage> a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, int i2, ArrayList<Integer> arrayList) {
            wg6.b(arrayList, "errorCodes");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements BleCommandResultManager.b {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.ui.device.domain.usecase.SetReplyMessageMappingUseCase, com.portfolio.platform.CoroutineUseCase] */
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("SetReplyMessageMappingUseCase", "Inside .bleReceiver communicateMode= " + communicateMode);
            if (communicateMode == CommunicateMode.SET_REPLY_MESSAGE_MAPPING && SetReplyMessageMappingUseCase.this.d) {
                boolean z = false;
                SetReplyMessageMappingUseCase.this.d = false;
                if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                    z = true;
                }
                SetReplyMessageMappingUseCase.this.e();
                if (z) {
                    rm6 unused = ik6.b(SetReplyMessageMappingUseCase.this.b(), (af6) null, (ll6) null, new ur4$e$a(this, (xe6) null), 3, (Object) null);
                    return;
                }
                FLogger.INSTANCE.getLocal().d("SetReplyMessageMappingUseCase", "onReceive failed");
                int intExtra = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
                ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
                if (integerArrayListExtra == null) {
                    integerArrayListExtra = new ArrayList<>(intExtra);
                }
                SetReplyMessageMappingUseCase.this.a(new c(FailureCode.FAILED_TO_CONNECT, intExtra, integerArrayListExtra));
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public SetReplyMessageMappingUseCase(QuickResponseRepository quickResponseRepository) {
        wg6.b(quickResponseRepository, "mQuickResponseRepository");
        this.g = quickResponseRepository;
    }

    @DexIgnore
    public String c() {
        return "SetReplyMessageMappingUseCase";
    }

    @DexIgnore
    public final void e() {
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }

    @DexIgnore
    public final void d() {
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.e, CommunicateMode.SET_REPLY_MESSAGE_MAPPING);
    }

    @DexIgnore
    public Object a(ur4.b bVar, xe6<Object> xe6) {
        this.d = true;
        d();
        if (bVar != null) {
            List<QuickResponseMessage> a2 = bVar.a();
            ArrayList<QuickResponseMessage> arrayList = new ArrayList<>();
            for (T next : a2) {
                if (!hf6.a(((QuickResponseMessage) next).getResponse().length() == 0).booleanValue()) {
                    arrayList.add(next);
                }
            }
            ArrayList arrayList2 = new ArrayList(rd6.a(arrayList, 10));
            for (QuickResponseMessage quickResponseMessage : arrayList) {
                arrayList2.add(new ReplyMessageMapping(String.valueOf(quickResponseMessage.getId()), quickResponseMessage.getResponse()));
            }
            hf6.a(PortfolioApp.get.instance().a(new ReplyMessageMappingGroup(yd6.d(arrayList2), "icMessage.icon"), PortfolioApp.get.instance().e()));
        }
        return new Object();
    }
}
