package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.gs4;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.zm4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.FirmwareFactory;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.Firmware;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.util.DeviceUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateFirmwareUsecase extends m24<gs4.b, gs4.d, gs4.c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public static /* final */ a g; // = new a((qg6) null);
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ an4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UpdateFirmwareUsecase.f;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final FirmwareData a(an4 an4, String str) {
            wg6.b(an4, "sharedPreferencesManager");
            wg6.b(str, "deviceSKU");
            if (xj6.b("release", "release", true) || !an4.D()) {
                Firmware a = zm4.p.a().e().a(str);
                if (a != null) {
                    return FirmwareFactory.getInstance().createFirmwareData(a.getVersionNumber(), a.getDeviceModel(), a.getChecksum());
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a2 = a();
                local.e(a2, "Error when update firmware, can't find latest fw of model=" + str);
                return null;
            }
            Firmware a3 = an4.a(str);
            if (a3 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a3.getVersionNumber(), a3.getDeviceModel(), a3.getChecksum());
            }
            Firmware a4 = zm4.p.a().e().a(str);
            if (a4 != null) {
                return FirmwareFactory.getInstance().createFirmwareData(a4.getVersionNumber(), a4.getDeviceModel(), a4.getChecksum());
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a5 = a();
            local2.e(a5, "Error when update firmware, can't find latest fw of model=" + str);
            return null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(String str, boolean z) {
            wg6.b(str, "deviceSerial");
            this.a = str;
            this.b = z;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ b(String str, boolean z, int i, qg6 qg6) {
            this(str, (i & 2) != 0 ? false : z);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase", f = "UpdateFirmwareUsecase.kt", l = {45}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ UpdateFirmwareUsecase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(UpdateFirmwareUsecase updateFirmwareUsecase, xe6 xe6) {
            super(xe6);
            this.this$0 = updateFirmwareUsecase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((gs4.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        wg6.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public UpdateFirmwareUsecase(DeviceRepository deviceRepository, an4 an4) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = an4;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f0 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f1 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00f8 A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x016d A[Catch:{ Exception -> 0x0173 }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0186  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0188  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public Object a(gs4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        String str;
        String a2;
        Device device;
        UpdateFirmwareUsecase updateFirmwareUsecase;
        String sku;
        FirmwareData a3;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a4 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(f, "running UseCase");
                    if (bVar == null) {
                        try {
                            FLogger.INSTANCE.getLocal().e(f, "Error when update firmware, requestValues is NULL");
                            return new c();
                        } catch (Exception e2) {
                            e = e2;
                            IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
                            FLogger.Component component = FLogger.Component.APP;
                            FLogger.Session session = FLogger.Session.OTHER;
                            if (bVar == null || (a2 = bVar.a()) == null) {
                            }
                            String str2 = f;
                            remote.i(component, session, str, str2, "[OTA Start] Exception " + e);
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str3 = f;
                            local.d(str3, "Inside .run failed with exception=" + e);
                            e.printStackTrace();
                            return new c();
                        }
                    } else {
                        device = this.d.getDeviceBySerial(bVar.a());
                        if (device == null) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str4 = f;
                            local2.e(str4, "Error when update firmware, can't find latest device on db serial=" + bVar.a());
                            return new c();
                        }
                        if (bVar.b()) {
                            FLogger.INSTANCE.getLocal().d(f, "force download latest fw before udpate fw");
                            FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, PortfolioApp.get.instance().e(), ApplicationEventListener.v.a(), "[OTA Start] Download FW");
                            DeviceUtils a5 = DeviceUtils.g.a();
                            eVar.L$0 = this;
                            eVar.L$1 = bVar;
                            eVar.L$2 = device;
                            eVar.label = 1;
                            if (a5.a((xe6<? super cd6>) eVar) == a4) {
                                return a4;
                            }
                        }
                        updateFirmwareUsecase = this;
                    }
                } else if (i == 1) {
                    Device device2 = (Device) eVar.L$2;
                    gs4.b bVar2 = (b) eVar.L$1;
                    updateFirmwareUsecase = (UpdateFirmwareUsecase) eVar.L$0;
                    try {
                        nc6.a(obj);
                        device = device2;
                        bVar = bVar2;
                    } catch (Exception e3) {
                        e = e3;
                        bVar = bVar2;
                        IRemoteFLogger remote2 = FLogger.INSTANCE.getRemote();
                        FLogger.Component component2 = FLogger.Component.APP;
                        FLogger.Session session2 = FLogger.Session.OTHER;
                        if (bVar == null || (a2 = bVar.a()) == null) {
                            str = "";
                        } else {
                            str = a2;
                        }
                        String str22 = f;
                        remote2.i(component2, session2, str, str22, "[OTA Start] Exception " + e);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str32 = f;
                        local3.d(str32, "Inside .run failed with exception=" + e);
                        e.printStackTrace();
                        return new c();
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                a aVar = g;
                an4 an4 = updateFirmwareUsecase.e;
                sku = device.getSku();
                if (sku != null) {
                    sku = "";
                }
                a3 = aVar.a(an4, sku);
                if (a3 != null) {
                    return new c();
                }
                updateFirmwareUsecase.e.a(device.getDeviceId(), device.getFirmwareRevision());
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                String str5 = f;
                local4.d(str5, "Start update firmware with version=" + a3.getFirmwareVersion() + ", currentVersion=" + device.getFirmwareRevision());
                UserProfile j = PortfolioApp.get.instance().j();
                if (j != null) {
                    FLogger.INSTANCE.getRemote().i(FLogger.Component.APP, FLogger.Session.OTHER, bVar.a(), f, "[OTA Start] Calling OTA from SDK");
                    PortfolioApp.get.instance().a(bVar.a(), a3, j);
                    cd6 cd6 = cd6.a;
                } else {
                    new c();
                }
                return new d();
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a42 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        a aVar2 = g;
        an4 an42 = updateFirmwareUsecase.e;
        sku = device.getSku();
        if (sku != null) {
        }
        a3 = aVar2.a(an42, sku);
        if (a3 != null) {
        }
    }
}
