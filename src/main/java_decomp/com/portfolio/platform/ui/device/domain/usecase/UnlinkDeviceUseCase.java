package com.portfolio.platform.ui.device.domain.usecase;

import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ds4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UnlinkDeviceUseCase extends m24<ds4.b, ds4.d, ds4.c> {
    @DexIgnore
    public static /* final */ String j;
    @DexIgnore
    public static /* final */ a k; // = new a((qg6) null);
    @DexIgnore
    public b d;
    @DexIgnore
    public /* final */ DeviceRepository e;
    @DexIgnore
    public /* final */ HybridPresetRepository f;
    @DexIgnore
    public /* final */ DianaPresetRepository g;
    @DexIgnore
    public /* final */ PortfolioApp h;
    @DexIgnore
    public /* final */ WatchFaceRepository i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return UnlinkDeviceUseCase.j;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;

        @DexIgnore
        public b(String str) {
            wg6.b(str, "deviceSerial");
            this.a = str;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ ArrayList<Integer> b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public c(String str, ArrayList<Integer> arrayList, String str2) {
            wg6.b(str, "lastErrorCode");
            this.a = str;
            this.b = arrayList;
            this.c = str2;
        }

        @DexIgnore
        public final String a() {
            return this.c;
        }

        @DexIgnore
        public final String b() {
            return this.a;
        }

        @DexIgnore
        public final ArrayList<Integer> c() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase$doRemoveDevice$1", f = "UnlinkDeviceUseCase.kt", l = {60, 67, 69}, m = "invokeSuspend")
    public static final class e extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ UnlinkDeviceUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(UnlinkDeviceUseCase unlinkDeviceUseCase, xe6 xe6) {
            super(2, xe6);
            this.this$0 = unlinkDeviceUseCase;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            e eVar = new e(this.this$0, xe6);
            eVar.p$ = (il6) obj;
            return eVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((e) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r14v2, types: [com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        /* JADX WARNING: type inference failed for: r14v16, types: [com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Device device;
            String str;
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 = this.p$;
                DeviceRepository a2 = this.this$0.e;
                b d = this.this$0.d;
                String str2 = null;
                String a3 = d != null ? d.a() : null;
                if (a3 != null) {
                    device = a2.getDeviceBySerial(a3);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String a4 = UnlinkDeviceUseCase.k.a();
                    StringBuilder sb = new StringBuilder();
                    sb.append("doRemoveDevice ");
                    if (device != null) {
                        str2 = device.getDeviceId();
                    }
                    sb.append(str2);
                    local.d(a4, sb.toString());
                    if (device != null) {
                        DeviceRepository a5 = this.this$0.e;
                        this.L$0 = il6;
                        this.L$1 = device;
                        this.label = 1;
                        obj = a5.removeDevice(device, this);
                        if (obj == a) {
                            return a;
                        }
                    } else {
                        this.this$0.a(new c("UNLINK_FAIL_ON_SERVER", qd6.a((T[]) new Integer[]{hf6.a(600)}), ""));
                        return cd6.a;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else if (i == 1) {
                device = (Device) this.L$1;
                il6 = (il6) this.L$0;
                nc6.a(obj);
            } else if (i == 2 || i == 3) {
                String str3 = (String) this.L$3;
                ap4 ap4 = (ap4) this.L$2;
                Device device2 = (Device) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
                this.this$0.a(new d());
                return cd6.a;
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            ap4 ap42 = (ap4) obj;
            if (ap42 instanceof cp4) {
                String deviceId = device.getDeviceId();
                PortfolioApp.get.instance().s(deviceId);
                if (DeviceHelper.o.f(deviceId)) {
                    this.this$0.i.deleteWatchFacesWithSerial(deviceId);
                    DianaPresetRepository b = this.this$0.g;
                    this.L$0 = il6;
                    this.L$1 = device;
                    this.L$2 = ap42;
                    this.L$3 = deviceId;
                    this.label = 2;
                    if (b.deleteAllPresetBySerial(deviceId, this) == a) {
                        return a;
                    }
                } else {
                    HybridPresetRepository c = this.this$0.f;
                    this.L$0 = il6;
                    this.L$1 = device;
                    this.L$2 = ap42;
                    this.L$3 = deviceId;
                    this.label = 3;
                    if (c.deleteAllPresetBySerial(deviceId, this) == a) {
                        return a;
                    }
                }
                this.this$0.a(new d());
                return cd6.a;
            }
            if (ap42 instanceof zo4) {
                Object r0 = this.this$0;
                zo4 zo4 = (zo4) ap42;
                ArrayList a6 = qd6.a((T[]) new Integer[]{hf6.a(zo4.a())});
                ServerError c2 = zo4.c();
                if (c2 == null || (str = c2.getUserMessage()) == null) {
                    str = "";
                }
                r0.a(new c("UNLINK_FAIL_ON_SERVER", a6, str));
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = UnlinkDeviceUseCase.class.getSimpleName();
        wg6.a((Object) simpleName, "UnlinkDeviceUseCase::class.java.simpleName");
        j = simpleName;
    }
    */

    @DexIgnore
    public UnlinkDeviceUseCase(DeviceRepository deviceRepository, HybridPresetRepository hybridPresetRepository, DianaPresetRepository dianaPresetRepository, PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(hybridPresetRepository, "mHybridPresetRepository");
        wg6.b(dianaPresetRepository, "mDianaPresetRepository");
        wg6.b(portfolioApp, "mApp");
        wg6.b(watchFaceRepository, "watchFaceRepository");
        this.e = deviceRepository;
        this.f = hybridPresetRepository;
        this.g = dianaPresetRepository;
        this.h = portfolioApp;
        this.i = watchFaceRepository;
    }

    @DexIgnore
    public String c() {
        return j;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v0, types: [com.portfolio.platform.ui.device.domain.usecase.UnlinkDeviceUseCase, com.portfolio.platform.CoroutineUseCase] */
    public final rm6 d() {
        return ik6.b(b(), (af6) null, (ll6) null, new e(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public Object a(ds4.b bVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(j, "running UseCase");
        if (bVar == null) {
            return new c("", (ArrayList<Integer>) null, "");
        }
        this.d = bVar;
        String a2 = bVar.a();
        String e2 = this.h.e();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = j;
        local.d(str, "remove device " + a2 + " currentActive " + e2);
        d();
        return new Object();
    }
}
