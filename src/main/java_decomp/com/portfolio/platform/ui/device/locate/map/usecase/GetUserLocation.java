package com.portfolio.platform.ui.device.locate.map.usecase;

import android.location.Location;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.ls4;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetUserLocation extends m24<ls4.b, ls4.d, ls4.c> {
    @DexIgnore
    public LocationSource d;
    @DexIgnore
    public PortfolioApp e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ double a;
        @DexIgnore
        public /* final */ double b;

        @DexIgnore
        public d(double d, double d2) {
            this.a = d;
            this.b = d2;
        }

        @DexIgnore
        public final double a() {
            return this.a;
        }

        @DexIgnore
        public final double b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.device.locate.map.usecase.GetUserLocation", f = "GetUserLocation.kt", l = {16}, m = "run")
    public static final class e extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ GetUserLocation this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(GetUserLocation getUserLocation, xe6 xe6) {
            super(xe6);
            this.this$0 = getUserLocation;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ls4.b) null, (xe6<Object>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public GetUserLocation(LocationSource locationSource, PortfolioApp portfolioApp) {
        wg6.b(locationSource, "mLocationSource");
        wg6.b(portfolioApp, "mPortfolioApp");
        this.d = locationSource;
        this.e = portfolioApp;
    }

    @DexIgnore
    public String c() {
        return "GetUserLocation";
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(ls4.b bVar, xe6<Object> xe6) {
        e eVar;
        int i;
        LocationSource.Result result;
        if (xe6 instanceof e) {
            eVar = (e) xe6;
            int i2 = eVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                eVar.label = i2 - Integer.MIN_VALUE;
                Object obj = eVar.result;
                Object a2 = ff6.a();
                i = eVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("GetUserLocation", "running UseCase");
                    LocationSource locationSource = this.d;
                    Object r2 = this.e;
                    eVar.L$0 = this;
                    eVar.L$1 = bVar;
                    eVar.label = 1;
                    obj = locationSource.getLocation(r2, false, eVar);
                    if (obj == a2) {
                        return a2;
                    }
                } else if (i == 1) {
                    b bVar2 = (b) eVar.L$1;
                    GetUserLocation getUserLocation = (GetUserLocation) eVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                result = (LocationSource.Result) obj;
                if (result.getErrorState() == LocationSource.ErrorState.SUCCESS) {
                    return new c();
                }
                Location location = result.getLocation();
                if (location != null) {
                    return new d(location.getLatitude(), location.getLongitude());
                }
                return null;
            }
        }
        eVar = new e(this, xe6);
        Object obj2 = eVar.result;
        Object a22 = ff6.a();
        i = eVar.label;
        if (i != 0) {
        }
        result = (LocationSource.Result) obj2;
        if (result.getErrorState() == LocationSource.ErrorState.SUCCESS) {
        }
    }
}
