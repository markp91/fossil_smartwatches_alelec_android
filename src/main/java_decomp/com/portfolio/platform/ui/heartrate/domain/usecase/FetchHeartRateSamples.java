package com.portfolio.platform.ui.heartrate.domain.usecase;

import android.text.TextUtils;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.ss4;
import com.fossil.uy5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSampleRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FetchHeartRateSamples extends m24<ss4.b, m24.d, m24.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSampleRepository d;
    @DexIgnore
    public /* final */ FitnessDataRepository e;
    @DexIgnore
    public /* final */ UserRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchHeartRateSamples", f = "FetchHeartRateSamples.kt", l = {46}, m = "run")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FetchHeartRateSamples this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FetchHeartRateSamples fetchHeartRateSamples, xe6 xe6) {
            super(xe6);
            this.this$0 = fetchHeartRateSamples;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((ss4.b) null, (xe6<? super cd6>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = FetchHeartRateSamples.class.getSimpleName();
        wg6.a((Object) simpleName, "FetchHeartRateSamples::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchHeartRateSamples(HeartRateSampleRepository heartRateSampleRepository, FitnessDataRepository fitnessDataRepository, UserRepository userRepository) {
        wg6.b(heartRateSampleRepository, "mHeartRateSampleRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(userRepository, "mUserRepository");
        this.d = heartRateSampleRepository;
        this.e = fitnessDataRepository;
        this.f = userRepository;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public Object a(ss4.b bVar, xe6<? super cd6> xe6) {
        c cVar;
        int i;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i2 = cVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cVar.label = i2 - Integer.MIN_VALUE;
                c cVar2 = cVar;
                Object obj = cVar2.result;
                Object a2 = ff6.a();
                i = cVar2.label;
                if (i != 0) {
                    nc6.a(obj);
                    if (bVar == null) {
                        return cd6.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = g;
                    local.d(str, "executeUseCase - date=" + uy5.a(a3));
                    MFUser currentUser = this.f.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cd6.a;
                    }
                    Date d2 = bk4.d(currentUser.getCreatedAt());
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = g;
                    StringBuilder sb = new StringBuilder();
                    sb.append("executeUseCase - createdDate=");
                    wg6.a((Object) d2, "createdDate");
                    sb.append(uy5.a(d2));
                    local3.d(str3, sb.toString());
                    if (bk4.b(d2, a3) || bk4.b(a3, new Date())) {
                        return cd6.a;
                    }
                    Calendar q = bk4.q(a3);
                    wg6.a((Object) q, "DateHelper.getStartOfWeek(date)");
                    Date time = q.getTime();
                    if (bk4.c(d2, time)) {
                        time = d2;
                    }
                    FitnessDataRepository fitnessDataRepository = this.e;
                    wg6.a((Object) time, "startDate");
                    List<FitnessDataWrapper> fitnessData = fitnessDataRepository.getFitnessData(time, a3);
                    if (fitnessData.isEmpty()) {
                        HeartRateSampleRepository heartRateSampleRepository = this.d;
                        cVar2.L$0 = this;
                        cVar2.L$1 = bVar;
                        cVar2.L$2 = a3;
                        cVar2.L$3 = currentUser;
                        cVar2.L$4 = d2;
                        cVar2.L$5 = time;
                        cVar2.L$6 = fitnessData;
                        cVar2.label = 1;
                        if (HeartRateSampleRepository.fetchHeartRateSamples$default(heartRateSampleRepository, time, a3, 0, 0, cVar2, 12, (Object) null) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) cVar2.L$6;
                    Date date = (Date) cVar2.L$5;
                    Date date2 = (Date) cVar2.L$4;
                    MFUser mFUser = (MFUser) cVar2.L$3;
                    Date date3 = (Date) cVar2.L$2;
                    b bVar2 = (b) cVar2.L$1;
                    FetchHeartRateSamples fetchHeartRateSamples = (FetchHeartRateSamples) cVar2.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        cVar = new c(this, xe6);
        c cVar22 = cVar;
        Object obj2 = cVar22.result;
        Object a22 = ff6.a();
        i = cVar22.label;
        if (i != 0) {
        }
        return cd6.a;
    }
}
