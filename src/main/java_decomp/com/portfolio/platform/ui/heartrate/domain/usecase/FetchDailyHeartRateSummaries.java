package com.portfolio.platform.ui.heartrate.domain.usecase;

import android.text.TextUtils;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rs4;
import com.fossil.uy5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FetchDailyHeartRateSummaries extends m24<rs4.b, m24.d, m24.a> {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public /* final */ HeartRateSummaryRepository d;
    @DexIgnore
    public /* final */ UserRepository e;
    @DexIgnore
    public /* final */ FitnessDataRepository f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ Date a;

        @DexIgnore
        public b(Date date) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            this.a = date;
        }

        @DexIgnore
        public final Date a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.heartrate.domain.usecase.FetchDailyHeartRateSummaries", f = "FetchDailyHeartRateSummaries.kt", l = {57}, m = "run")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public Object L$8;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ FetchDailyHeartRateSummaries this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries, xe6 xe6) {
            super(xe6);
            this.this$0 = fetchDailyHeartRateSummaries;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((rs4.b) null, (xe6<? super cd6>) this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = FetchDailyHeartRateSummaries.class.getSimpleName();
        wg6.a((Object) simpleName, "FetchDailyHeartRateSumma\u2026es::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public FetchDailyHeartRateSummaries(HeartRateSummaryRepository heartRateSummaryRepository, UserRepository userRepository, FitnessDataRepository fitnessDataRepository) {
        wg6.b(heartRateSummaryRepository, "mHeartRateSummariesRepository");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        this.d = heartRateSummaryRepository;
        this.e = userRepository;
        this.f = fitnessDataRepository;
    }

    @DexIgnore
    public String c() {
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public Object a(rs4.b bVar, xe6<? super cd6> xe6) {
        c cVar;
        int i;
        Date date;
        Date date2;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i2 = cVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cVar.label = i2 - Integer.MIN_VALUE;
                Object obj = cVar.result;
                Object a2 = ff6.a();
                i = cVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    if (bVar == null) {
                        return cd6.a;
                    }
                    Date a3 = bVar.a();
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = g;
                    local.d(str, "executeUseCase - date=" + uy5.a(a3));
                    MFUser currentUser = this.e.getCurrentUser();
                    if (currentUser == null || TextUtils.isEmpty(currentUser.getCreatedAt())) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = g;
                        local2.d(str2, "executeUseCase - FAILED!!! with user=" + currentUser);
                        return cd6.a;
                    }
                    Date d2 = bk4.d(currentUser.getCreatedAt());
                    Calendar instance = Calendar.getInstance();
                    wg6.a((Object) instance, "calendar");
                    instance.setTime(a3);
                    instance.set(5, 1);
                    long timeInMillis = instance.getTimeInMillis();
                    wg6.a((Object) d2, "createdDate");
                    if (bk4.a(timeInMillis, d2.getTime())) {
                        date = d2;
                    } else {
                        Calendar e2 = bk4.e(instance);
                        wg6.a((Object) e2, "DateHelper.getStartOfMonth(calendar)");
                        date = e2.getTime();
                        wg6.a((Object) date, "DateHelper.getStartOfMonth(calendar).time");
                        if (bk4.b(d2, date)) {
                            return cd6.a;
                        }
                    }
                    Boolean s = bk4.s(date);
                    wg6.a((Object) s, "DateHelper.isThisMonth(startDate)");
                    if (s.booleanValue()) {
                        date2 = new Date();
                    } else {
                        Calendar k = bk4.k(date);
                        wg6.a((Object) k, "DateHelper.getEndOfMonth(startDate)");
                        date2 = k.getTime();
                        wg6.a((Object) date2, "DateHelper.getEndOfMonth(startDate).time");
                    }
                    List<FitnessDataWrapper> fitnessData = this.f.getFitnessData(date, date2);
                    if (fitnessData.isEmpty()) {
                        HeartRateSummaryRepository heartRateSummaryRepository = this.d;
                        cVar.L$0 = this;
                        cVar.L$1 = bVar;
                        cVar.L$2 = a3;
                        cVar.L$3 = currentUser;
                        cVar.L$4 = d2;
                        cVar.L$5 = instance;
                        cVar.L$6 = date2;
                        cVar.L$7 = date;
                        cVar.L$8 = fitnessData;
                        cVar.label = 1;
                        if (heartRateSummaryRepository.loadSummaries(date, date2, cVar) == a2) {
                            return a2;
                        }
                    }
                } else if (i == 1) {
                    List list = (List) cVar.L$8;
                    Date date3 = (Date) cVar.L$7;
                    Date date4 = (Date) cVar.L$6;
                    Calendar calendar = (Calendar) cVar.L$5;
                    Date date5 = (Date) cVar.L$4;
                    MFUser mFUser = (MFUser) cVar.L$3;
                    Date date6 = (Date) cVar.L$2;
                    b bVar2 = (b) cVar.L$1;
                    FetchDailyHeartRateSummaries fetchDailyHeartRateSummaries = (FetchDailyHeartRateSummaries) cVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        cVar = new c(this, xe6);
        Object obj2 = cVar.result;
        Object a22 = ff6.a();
        i = cVar.label;
        if (i != 0) {
        }
        return cd6.a;
    }
}
