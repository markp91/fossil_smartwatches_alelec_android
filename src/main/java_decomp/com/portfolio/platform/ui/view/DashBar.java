package com.portfolio.platform.ui.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Shader;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import androidx.annotation.Keep;
import com.fossil.qg6;
import com.fossil.x24;
import com.portfolio.platform.manager.ThemeManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DashBar extends View {
    @DexIgnore
    public Paint a; // = new Paint(1);
    @DexIgnore
    public Paint b; // = new Paint(1);
    @DexIgnore
    public float c; // = 10.0f;
    @DexIgnore
    public float d; // = 6.0f;
    @DexIgnore
    public int e; // = 4;
    @DexIgnore
    public String f; // = "primaryText";
    @DexIgnore
    public String g; // = "secondaryText";
    @DexIgnore
    public String h; // = "primaryText";
    @DexIgnore
    public int i; // = 50;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public DashBar(Context context) {
        super(context, (AttributeSet) null);
    }

    @DexIgnore
    public final void a(float f2) {
        this.a.setDither(true);
        this.a.setStyle(Paint.Style.STROKE);
        this.a.setStrokeWidth(f2);
        this.a.setAntiAlias(true);
        this.a.setStrokeCap(Paint.Cap.ROUND);
        this.a.setStrokeJoin(Paint.Join.ROUND);
    }

    @DexIgnore
    public final void b(float f2) {
        if (!TextUtils.isEmpty(this.f) && !TextUtils.isEmpty(this.g) && !TextUtils.isEmpty(this.h)) {
            String b2 = ThemeManager.l.a().b(this.f);
            String b3 = ThemeManager.l.a().b(this.g);
            String b4 = ThemeManager.l.a().b(this.h);
            int parseColor = b2 != null ? Color.parseColor(b2) : -16777216;
            int parseColor2 = b3 != null ? Color.parseColor(b3) : -16777216;
            int parseColor3 = b4 != null ? Color.parseColor(b4) : -16777216;
            this.a.setShader(new LinearGradient(0.0f, 0.0f, f2, 0.0f, parseColor, parseColor2, Shader.TileMode.CLAMP));
            Paint paint = this.a;
            int i2 = this.e;
            float f3 = this.d;
            paint.setPathEffect(new DashPathEffect(new float[]{((f2 - 20.0f) - (((float) (i2 - 1)) * f3)) / ((float) i2), f3}, 0.0f));
            this.b = new Paint(this.a);
            this.b.setShader(new LinearGradient(0.0f, 0.0f, f2, 0.0f, parseColor3, parseColor3, Shader.TileMode.CLAMP));
        }
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        b((float) getWidth());
        if (canvas != null) {
            float height = ((float) canvas.getHeight()) / 2.0f;
            float width = (float) canvas.getWidth();
            float f2 = height;
            float f3 = height;
            canvas.drawLine(10.0f, f2, width - 10.0f, f3, this.b);
            canvas.drawLine(10.0f, f2, ((width * ((float) this.i)) / ((float) 100)) - 10.0f, f3, this.a);
        }
    }

    @DexIgnore
    public void onMeasure(int i2, int i3) {
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int mode2 = View.MeasureSpec.getMode(i3);
        int size2 = View.MeasureSpec.getSize(i3);
        if (!(mode == Integer.MIN_VALUE || mode == 0 || mode == 1073741824)) {
            size = 0;
        }
        if (mode2 != Integer.MIN_VALUE) {
            if (mode2 == 0) {
                size2 = ((int) this.c) * 2;
            } else if (mode2 != 1073741824) {
                size2 = 0;
            }
        }
        setMeasuredDimension(size, size2);
    }

    @DexIgnore
    @Keep
    public final void setLength(int i2) {
        this.e = i2;
        invalidate();
    }

    @DexIgnore
    @Keep
    public final void setProgress(int i2) {
        this.i = i2;
        invalidate();
    }

    @DexIgnore
    public DashBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context != null ? context.obtainStyledAttributes(attributeSet, x24.DashBar) : null;
        if (obtainStyledAttributes != null) {
            this.c = obtainStyledAttributes.getDimension(6, 10.0f);
            this.d = obtainStyledAttributes.getDimension(3, 6.0f);
            this.e = obtainStyledAttributes.getInteger(4, 4);
            String string = obtainStyledAttributes.getString(2);
            String str = "";
            this.f = string == null ? str : string;
            String string2 = obtainStyledAttributes.getString(1);
            this.g = string2 == null ? str : string2;
            String string3 = obtainStyledAttributes.getString(0);
            this.h = string3 != null ? string3 : str;
            this.i = obtainStyledAttributes.getInt(5, 0);
        }
        a(this.c);
        setLayerType(1, (Paint) null);
        if (obtainStyledAttributes != null) {
            obtainStyledAttributes.recycle();
        }
    }
}
