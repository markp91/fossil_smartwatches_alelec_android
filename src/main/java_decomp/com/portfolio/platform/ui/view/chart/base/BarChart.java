package com.portfolio.platform.ui.view.chart.base;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.annotation.Keep;
import com.fossil.dx5;
import com.fossil.kc6;
import com.fossil.lc6;
import com.fossil.nd6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.sh4;
import com.fossil.tt4;
import com.fossil.ue6;
import com.fossil.ut4;
import com.fossil.wg6;
import com.fossil.x24;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BarChart extends BaseChart {
    @DexIgnore
    public int A;
    @DexIgnore
    public int B;
    @DexIgnore
    public PointF C;
    @DexIgnore
    public boolean D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public ArrayList<PointF> G;
    @DexIgnore
    public boolean H;
    @DexIgnore
    public float I;
    @DexIgnore
    public int J;
    @DexIgnore
    public float K;
    @DexIgnore
    public float L;
    @DexIgnore
    public String M;
    @DexIgnore
    public Path N;
    @DexIgnore
    public String O;
    @DexIgnore
    public float P;
    @DexIgnore
    public ArrayList<String> Q;
    @DexIgnore
    public int R;
    @DexIgnore
    public float S;
    @DexIgnore
    public String T;
    @DexIgnore
    public int U;
    @DexIgnore
    public String V;
    @DexIgnore
    public ArrayList<lc6<String, PointF>> W;
    @DexIgnore
    public float a0;
    @DexIgnore
    public ArrayList<lc6<Integer, PointF>> b0;
    @DexIgnore
    public float c0;
    @DexIgnore
    public float d0;
    @DexIgnore
    public float e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public float h0;
    @DexIgnore
    public float i0;
    @DexIgnore
    public int j0;
    @DexIgnore
    public int k0;
    @DexIgnore
    public int l0;
    @DexIgnore
    public boolean m0;
    @DexIgnore
    public String n0;
    @DexIgnore
    public c o0;
    @DexIgnore
    public sh4 p0;
    @DexIgnore
    public Paint q0;
    @DexIgnore
    public Paint r0;
    @DexIgnore
    public Paint s0;
    @DexIgnore
    public Paint t0;
    @DexIgnore
    public e u;
    @DexIgnore
    public Paint u0;
    @DexIgnore
    public int v;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public int a;
        @DexIgnore
        public ArrayList<ArrayList<b>> b;
        @DexIgnore
        public long c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public a() {
            this(0, (ArrayList) null, 0, false, 15, (qg6) null);
        }

        @DexIgnore
        public a(int i, ArrayList<ArrayList<b>> arrayList, long j, boolean z) {
            wg6.b(arrayList, "mListOfBarPoints");
            this.a = i;
            this.b = arrayList;
            this.c = j;
            this.d = z;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final long b() {
            return this.c;
        }

        @DexIgnore
        public final ArrayList<ArrayList<b>> c() {
            return this.b;
        }

        @DexIgnore
        public final boolean d() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a == aVar.a && this.c == aVar.c && this.d == aVar.d && a(aVar.b)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((this.a * 31) + this.b.hashCode()) * 31) + com.fossil.e.a(this.c)) * 31) + com.fossil.a.a(this.d);
        }

        @DexIgnore
        public String toString() {
            return "{goal=" + this.a + ", index=" + this.c + ", points=" + this.b + '}';
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ a(int i, ArrayList arrayList, long j, boolean z, int i2, qg6 qg6) {
            this(r0, (i2 & 2) != 0 ? qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(0, (f) null, 0, 0, (RectF) null, 23, (qg6) null)})}) : arrayList, (i2 & 4) != 0 ? -1 : j, (i2 & 8) == 0 ? z : false);
            int i3 = (i2 & 1) != 0 ? -1 : i;
        }

        @DexIgnore
        public final boolean a(ArrayList<ArrayList<b>> arrayList) {
            if (this.b.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T next : this.b) {
                int i2 = i + 1;
                if (i >= 0) {
                    ArrayList arrayList2 = (ArrayList) next;
                    if (arrayList2.size() != arrayList.get(i).size()) {
                        return false;
                    }
                    int i3 = 0;
                    for (Object next2 : arrayList2) {
                        int i4 = i3 + 1;
                        if (i3 < 0) {
                            qd6.c();
                            throw null;
                        } else if (!wg6.a((Object) (b) next2, (Object) (b) arrayList.get(i).get(i3))) {
                            return false;
                        } else {
                            i3 = i4;
                        }
                    }
                    i = i2;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public int a;
        @DexIgnore
        public f b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public RectF e;

        @DexIgnore
        public b() {
            this(0, (f) null, 0, 0, (RectF) null, 31, (qg6) null);
        }

        @DexIgnore
        public b(int i, f fVar, int i2, int i3, RectF rectF) {
            wg6.b(fVar, "mState");
            wg6.b(rectF, "mBound");
            this.a = i;
            this.b = fVar;
            this.c = i2;
            this.d = i3;
            this.e = rectF;
        }

        @DexIgnore
        public final RectF a() {
            return this.e;
        }

        @DexIgnore
        public final int b() {
            return this.a;
        }

        @DexIgnore
        public final f c() {
            return this.b;
        }

        @DexIgnore
        public final int d() {
            return this.c;
        }

        @DexIgnore
        public final int e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.d == bVar.d && this.c == bVar.c && this.a == bVar.a && this.b.getMValue() == bVar.b.getMValue()) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((((((this.a * 31) + this.b.hashCode()) * 31) + this.c) * 31) + this.d) * 31) + this.e.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "{index=" + this.a + ", state=" + this.b + ", mTotal=" + this.c + ", value=" + this.d + ", bound=" + this.e + '}';
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(int i, f fVar, int i2, int i3, RectF rectF, int i4, qg6 qg6) {
            this(i, r10, (i4 & 4) != 0 ? 0 : i2, (i4 & 8) == 0 ? i3 : 0, (i4 & 16) != 0 ? new RectF() : rectF);
            i = (i4 & 1) != 0 ? -1 : i;
            f fVar2 = (i4 & 2) != 0 ? f.DEFAULT : fVar;
        }

        @DexIgnore
        public final void a(RectF rectF) {
            wg6.b(rectF, "<set-?>");
            this.e = rectF;
        }

        @DexIgnore
        public final void b(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void c(int i) {
            this.d = i;
        }

        @DexIgnore
        public final void a(int i) {
            this.a = i;
        }

        @DexIgnore
        public final void a(f fVar) {
            wg6.b(fVar, "<set-?>");
            this.b = fVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends ut4 {
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public ArrayList<a> d;

        @DexIgnore
        public c() {
            this(0, 0, (ArrayList) null, 7, (qg6) null);
        }

        @DexIgnore
        public c(int i, int i2, ArrayList<a> arrayList) {
            wg6.b(arrayList, "mData");
            this.b = i;
            this.c = i2;
            this.d = arrayList;
        }

        @DexIgnore
        public final ArrayList<a> a() {
            return this.d;
        }

        @DexIgnore
        public final int b() {
            return this.c;
        }

        @DexIgnore
        public final int c() {
            return this.b;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            if (this.b == cVar.b && this.c == cVar.c && a(cVar.d)) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return (((this.b * 31) + this.c) * 31) + this.d.hashCode();
        }

        @DexIgnore
        public String toString() {
            return "ChartModel:{max=" + this.b + ", goal=" + this.c + ", data=" + this.d + '}';
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ c(int i, int i2, ArrayList arrayList, int i3, qg6 qg6) {
            this((i3 & 1) != 0 ? -1 : i, (i3 & 2) != 0 ? -1 : i2, (i3 & 4) != 0 ? new ArrayList() : arrayList);
        }

        @DexIgnore
        public final void a(int i) {
            this.c = i;
        }

        @DexIgnore
        public final void b(int i) {
            this.b = i;
        }

        @DexIgnore
        public final boolean a(ArrayList<a> arrayList) {
            if (this.d.size() != arrayList.size()) {
                return false;
            }
            int i = 0;
            for (T next : this.d) {
                int i2 = i + 1;
                if (i < 0) {
                    qd6.c();
                    throw null;
                } else if (!wg6.a((Object) (a) next, (Object) arrayList.get(i))) {
                    return false;
                } else {
                    i = i2;
                }
            }
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public /* synthetic */ d(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(long j);
    }

    @DexIgnore
    public enum f {
        LOWEST(0),
        DEFAULT(1),
        HIGHEST(2);
        
        @DexIgnore
        public static /* final */ a Companion; // = null;
        @DexIgnore
        public int mValue;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new a((qg6) null);
        }
        */

        @DexIgnore
        public f(int i) {
            this.mValue = i;
        }

        @DexIgnore
        public final int getMValue() {
            return this.mValue;
        }

        @DexIgnore
        public final void setMValue(int i) {
            this.mValue = i;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((b) t2).c(), ((b) t).c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((b) t2).c(), ((b) t).c());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(((b) t).c(), ((b) t2).c());
        }
    }

    /*
    static {
        new d((qg6) null);
    }
    */

    @DexIgnore
    public BarChart(Context context) {
        this(context, (AttributeSet) null);
    }

    @DexIgnore
    public void a() {
        super.a();
        g();
        if (isInEditMode()) {
            this.o0 = new c(MFNetworkReturnCode.RESPONSE_OK, MFNetworkReturnCode.RESPONSE_OK, qd6.a((T[]) new a[]{new a(MFNetworkReturnCode.RESPONSE_OK, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 40, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, (qg6) null), new a(MFNetworkReturnCode.RESPONSE_OK, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 80, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, (qg6) null), new a(150, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 60, new RectF()), new b(-1, f.LOWEST, 0, 100, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, (qg6) null), new a(150, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 80, new RectF()), new b(-1, f.LOWEST, 0, 30, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, (qg6) null), new a(150, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 90, new RectF()), new b(-1, f.HIGHEST, 0, 20, new RectF())})}), 0, false, 12, (qg6) null), new a(180, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 40, new RectF()), new b(-1, f.LOWEST, 0, 40, new RectF()), new b(-1, f.HIGHEST, 0, 40, new RectF())})}), 0, false, 12, (qg6) null), new a(180, qd6.a((T[]) new ArrayList[]{qd6.a((T[]) new b[]{new b(-1, f.DEFAULT, 0, 70, new RectF()), new b(-1, f.LOWEST, 0, 30, new RectF()), new b(-1, f.HIGHEST, 0, 50, new RectF())})}), 0, false, 12, (qg6) null)}));
            return;
        }
    }

    @DexIgnore
    public void b(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.b(canvas);
        d();
        e();
        f(canvas);
        e(canvas);
        h(canvas);
    }

    @DexIgnore
    public void d(Canvas canvas) {
        wg6.b(canvas, "canvas");
        super.d(canvas);
        f();
        i(canvas);
        j(canvas);
    }

    @DexIgnore
    public void e(Canvas canvas) {
        int i2;
        wg6.b(canvas, "canvas");
        Iterator<a> it = this.o0.a().iterator();
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            Iterator it2 = yd6.a(arrayList, new i()).iterator();
            while (true) {
                if (it2.hasNext()) {
                    b bVar = (b) it2.next();
                    if (bVar.e() != 0) {
                        Paint paint = this.q0;
                        if (paint != null) {
                            int i3 = tt4.a[bVar.c().ordinal()];
                            if (i3 == 1) {
                                i2 = this.x;
                            } else if (i3 == 2) {
                                i2 = this.y;
                            } else if (i3 == 3) {
                                i2 = this.z;
                            } else {
                                throw new kc6();
                            }
                            paint.setColor(i2);
                            float f2 = bVar.a().left;
                            float f3 = bVar.a().top;
                            float f4 = bVar.a().right;
                            float f5 = bVar.a().bottom;
                            float f6 = this.f0;
                            Paint paint2 = this.q0;
                            if (paint2 != null) {
                                dx5.a(canvas, f2, f3, f4, f5, f6, f6, true, true, false, false, paint2);
                            } else {
                                wg6.d("mGraphPaint");
                                throw null;
                            }
                        } else {
                            wg6.d("mGraphPaint");
                            throw null;
                        }
                    }
                }
            }
        }
    }

    @DexIgnore
    public final void f(Canvas canvas) {
        wg6.b(canvas, "canvas");
        g(canvas);
        k(canvas);
    }

    @DexIgnore
    public final void g() {
        String b2;
        Typeface c2;
        String b3;
        this.q0 = new Paint(1);
        Paint paint = this.q0;
        if (paint != null) {
            paint.setAntiAlias(true);
            Paint paint2 = this.q0;
            if (paint2 != null) {
                paint2.setStyle(Paint.Style.FILL);
                Paint paint3 = this.q0;
                if (paint3 != null) {
                    this.r0 = new Paint(paint3);
                    Paint paint4 = this.r0;
                    if (paint4 != null) {
                        paint4.setStrokeWidth(this.I);
                        Paint paint5 = this.r0;
                        if (paint5 != null) {
                            paint5.setColor(this.J);
                            Paint paint6 = this.r0;
                            if (paint6 != null) {
                                paint6.setStyle(Paint.Style.STROKE);
                                Paint paint7 = this.r0;
                                if (paint7 != null) {
                                    paint7.setStrokeCap(Paint.Cap.ROUND);
                                    Paint paint8 = this.r0;
                                    if (paint8 != null) {
                                        paint8.setStrokeJoin(Paint.Join.ROUND);
                                        Paint paint9 = this.r0;
                                        if (paint9 != null) {
                                            paint9.setPathEffect(new DashPathEffect(new float[]{this.K, this.L}, 0.0f));
                                            this.s0 = new Paint(1);
                                            Paint paint10 = this.s0;
                                            if (paint10 != null) {
                                                paint10.setColorFilter(new PorterDuffColorFilter(this.v, PorterDuff.Mode.SRC_IN));
                                                this.t0 = new Paint(1);
                                                if (!TextUtils.isEmpty(this.T) && (b3 = ThemeManager.l.a().b(this.T)) != null) {
                                                    Paint paint11 = this.t0;
                                                    if (paint11 != null) {
                                                        paint11.setColor(Color.parseColor(b3));
                                                    } else {
                                                        wg6.d("mLegendPaint");
                                                        throw null;
                                                    }
                                                }
                                                Paint paint12 = this.t0;
                                                if (paint12 != null) {
                                                    paint12.setAntiAlias(true);
                                                    Paint paint13 = this.t0;
                                                    if (paint13 != null) {
                                                        paint13.setStyle(Paint.Style.FILL);
                                                        Paint paint14 = this.t0;
                                                        if (paint14 != null) {
                                                            paint14.setStrokeWidth(this.P);
                                                            Paint paint15 = this.t0;
                                                            if (paint15 != null) {
                                                                paint15.setTextSize(this.S);
                                                                if (!TextUtils.isEmpty(this.V) && (c2 = ThemeManager.l.a().c(this.V)) != null) {
                                                                    Paint paint16 = this.t0;
                                                                    if (paint16 != null) {
                                                                        paint16.setTypeface(c2);
                                                                    } else {
                                                                        wg6.d("mLegendPaint");
                                                                        throw null;
                                                                    }
                                                                }
                                                                Paint paint17 = this.t0;
                                                                if (paint17 != null) {
                                                                    this.u0 = new Paint(paint17);
                                                                    if (!TextUtils.isEmpty(this.O) && (b2 = ThemeManager.l.a().b(this.O)) != null) {
                                                                        Paint paint18 = this.u0;
                                                                        if (paint18 != null) {
                                                                            paint18.setColor(Color.parseColor(b2));
                                                                        } else {
                                                                            wg6.d("mLegendLinePaint");
                                                                            throw null;
                                                                        }
                                                                    }
                                                                } else {
                                                                    wg6.d("mLegendPaint");
                                                                    throw null;
                                                                }
                                                            } else {
                                                                wg6.d("mLegendPaint");
                                                                throw null;
                                                            }
                                                        } else {
                                                            wg6.d("mLegendPaint");
                                                            throw null;
                                                        }
                                                    } else {
                                                        wg6.d("mLegendPaint");
                                                        throw null;
                                                    }
                                                } else {
                                                    wg6.d("mLegendPaint");
                                                    throw null;
                                                }
                                            } else {
                                                wg6.d("mGraphIconPaint");
                                                throw null;
                                            }
                                        } else {
                                            wg6.d("mGraphGoalLinePaint");
                                            throw null;
                                        }
                                    } else {
                                        wg6.d("mGraphGoalLinePaint");
                                        throw null;
                                    }
                                } else {
                                    wg6.d("mGraphGoalLinePaint");
                                    throw null;
                                }
                            } else {
                                wg6.d("mGraphGoalLinePaint");
                                throw null;
                            }
                        } else {
                            wg6.d("mGraphGoalLinePaint");
                            throw null;
                        }
                    } else {
                        wg6.d("mGraphGoalLinePaint");
                        throw null;
                    }
                } else {
                    wg6.d("mGraphPaint");
                    throw null;
                }
            } else {
                wg6.d("mGraphPaint");
                throw null;
            }
        } else {
            wg6.d("mGraphPaint");
            throw null;
        }
    }

    @DexIgnore
    public final int getMActiveColor() {
        return this.v;
    }

    @DexIgnore
    public final String getMBackgroundColor() {
        return this.n0;
    }

    @DexIgnore
    public final int getMBarAlpha() {
        return this.l0;
    }

    @DexIgnore
    public final float getMBarMargin() {
        return this.h0;
    }

    @DexIgnore
    public final float getMBarMarginEnd() {
        return this.i0;
    }

    @DexIgnore
    public final float getMBarRadius() {
        return this.f0;
    }

    @DexIgnore
    public final float getMBarSpace() {
        return this.g0;
    }

    @DexIgnore
    public final e getMBarTouchListener() {
        return this.u;
    }

    @DexIgnore
    public final float getMBarWidth() {
        return this.e0;
    }

    @DexIgnore
    public final c getMChartModel() {
        return this.o0;
    }

    @DexIgnore
    public final int getMDefaultColor() {
        return this.y;
    }

    @DexIgnore
    public final PointF getMGoalIconPoint() {
        return this.C;
    }

    @DexIgnore
    public final boolean getMGoalIconShow() {
        return this.D;
    }

    @DexIgnore
    public final int getMGoalIconSize() {
        return this.B;
    }

    @DexIgnore
    public final Path getMGoalLinePath() {
        return this.N;
    }

    @DexIgnore
    public final sh4 getMGoalType() {
        return this.p0;
    }

    @DexIgnore
    public final int getMGoalnonBrandLineColor() {
        return this.J;
    }

    @DexIgnore
    public final Paint getMGraphGoalLinePaint() {
        Paint paint = this.r0;
        if (paint != null) {
            return paint;
        }
        wg6.d("mGraphGoalLinePaint");
        throw null;
    }

    @DexIgnore
    public final float getMGraphLegendMargin() {
        return this.a0;
    }

    @DexIgnore
    public final ArrayList<lc6<Integer, PointF>> getMGraphLegendPoint() {
        return this.b0;
    }

    @DexIgnore
    public final Paint getMGraphPaint() {
        Paint paint = this.q0;
        if (paint != null) {
            return paint;
        }
        wg6.d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final int getMHighestColor() {
        return this.z;
    }

    @DexIgnore
    public final int getMInActiveColor() {
        return this.w;
    }

    @DexIgnore
    public final boolean getMIsFlexibleSize() {
        return this.m0;
    }

    @DexIgnore
    public final int getMLegendIconRes() {
        return this.R;
    }

    @DexIgnore
    public final Paint getMLegendLinePaint() {
        Paint paint = this.u0;
        if (paint != null) {
            return paint;
        }
        wg6.d("mLegendLinePaint");
        throw null;
    }

    @DexIgnore
    public final Paint getMLegendPaint() {
        Paint paint = this.t0;
        if (paint != null) {
            return paint;
        }
        wg6.d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public final ArrayList<String> getMLegendTexts() {
        return this.Q;
    }

    @DexIgnore
    public final int getMLowestColor() {
        return this.x;
    }

    @DexIgnore
    public final int getMMaxValue() {
        return this.k0;
    }

    @DexIgnore
    public final int getMNumberBar() {
        return this.j0;
    }

    @DexIgnore
    public final float getMSafeAreaHeight() {
        return this.c0;
    }

    @DexIgnore
    public final ArrayList<PointF> getMStarIconPoint() {
        return this.G;
    }

    @DexIgnore
    public final boolean getMStarIconShow() {
        return this.H;
    }

    @DexIgnore
    public final int getMStarIconSize() {
        return this.F;
    }

    @DexIgnore
    public final String getMTextColor() {
        return this.T;
    }

    @DexIgnore
    public final int getMTextMargin() {
        return this.U;
    }

    @DexIgnore
    public final ArrayList<lc6<String, PointF>> getMTextPoint() {
        return this.W;
    }

    @DexIgnore
    public final float getMTextSize() {
        return this.S;
    }

    @DexIgnore
    public void h(Canvas canvas) {
        wg6.b(canvas, "canvas");
        Iterator<lc6<Integer, PointF>> it = this.b0.iterator();
        while (it.hasNext()) {
            lc6 next = it.next();
            String valueOf = String.valueOf(((Number) next.getFirst()).intValue());
            float f2 = ((PointF) next.getSecond()).x;
            float f3 = ((PointF) next.getSecond()).y;
            Paint paint = this.t0;
            if (paint != null) {
                canvas.drawText(valueOf, f2, f3, paint);
            } else {
                wg6.d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public void i(Canvas canvas) {
        wg6.b(canvas, "canvas");
        float f2 = this.P * 0.5f;
        float width = (float) canvas.getWidth();
        float f3 = this.P * 0.5f;
        Paint paint = this.u0;
        if (paint != null) {
            canvas.drawLine(0.0f, f2, width, f3, paint);
        } else {
            wg6.d("mLegendLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public void j(Canvas canvas) {
        wg6.b(canvas, "canvas");
        Iterator<lc6<String, PointF>> it = this.W.iterator();
        while (it.hasNext()) {
            lc6 next = it.next();
            String str = (String) next.getFirst();
            float f2 = ((PointF) next.getSecond()).x;
            float f3 = ((PointF) next.getSecond()).y;
            Paint paint = this.t0;
            if (paint != null) {
                canvas.drawText(str, f2, f3, paint);
            } else {
                wg6.d("mLegendPaint");
                throw null;
            }
        }
    }

    @DexIgnore
    public void k(Canvas canvas) {
        Bitmap a2;
        wg6.b(canvas, "canvas");
        if (this.H && (a2 = a(this.E, this.F)) != null) {
            for (PointF pointF : this.G) {
                float f2 = pointF.x;
                float f3 = pointF.y;
                Paint paint = this.s0;
                if (paint != null) {
                    canvas.drawBitmap(a2, f2, f3, paint);
                } else {
                    wg6.d("mGraphIconPaint");
                    throw null;
                }
            }
            a2.recycle();
        }
    }

    @DexIgnore
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
    }

    @DexIgnore
    @Keep
    public final void setBarAlpha(int i2) {
        this.l0 = i2;
        Paint paint = this.q0;
        if (paint != null) {
            paint.setAlpha(this.l0);
            Paint paint2 = this.r0;
            if (paint2 != null) {
                paint2.setAlpha(this.l0);
                Paint paint3 = this.s0;
                if (paint3 != null) {
                    paint3.setAlpha(this.l0);
                    c();
                    return;
                }
                wg6.d("mGraphIconPaint");
                throw null;
            }
            wg6.d("mGraphGoalLinePaint");
            throw null;
        }
        wg6.d("mGraphPaint");
        throw null;
    }

    @DexIgnore
    public final void setMActiveColor(int i2) {
        this.v = i2;
    }

    @DexIgnore
    public final void setMBackgroundColor(String str) {
        wg6.b(str, "<set-?>");
        this.n0 = str;
    }

    @DexIgnore
    public final void setMBarAlpha(int i2) {
        this.l0 = i2;
    }

    @DexIgnore
    public final void setMBarMargin(float f2) {
        this.h0 = f2;
    }

    @DexIgnore
    public final void setMBarMarginEnd(float f2) {
        this.i0 = f2;
    }

    @DexIgnore
    public final void setMBarRadius(float f2) {
        this.f0 = f2;
    }

    @DexIgnore
    public final void setMBarSpace(float f2) {
        this.g0 = f2;
    }

    @DexIgnore
    public final void setMBarTouchListener(e eVar) {
        this.u = eVar;
    }

    @DexIgnore
    public final void setMBarWidth(float f2) {
        this.e0 = f2;
    }

    @DexIgnore
    public final void setMChartModel(c cVar) {
        wg6.b(cVar, "<set-?>");
        this.o0 = cVar;
    }

    @DexIgnore
    public final void setMDefaultColor(int i2) {
        this.y = i2;
    }

    @DexIgnore
    public final void setMGoalIconPoint(PointF pointF) {
        wg6.b(pointF, "<set-?>");
        this.C = pointF;
    }

    @DexIgnore
    public final void setMGoalIconShow(boolean z2) {
        this.D = z2;
    }

    @DexIgnore
    public final void setMGoalIconSize(int i2) {
        this.B = i2;
    }

    @DexIgnore
    public final void setMGoalLinePath(Path path) {
        wg6.b(path, "<set-?>");
        this.N = path;
    }

    @DexIgnore
    public final void setMGoalType(sh4 sh4) {
        wg6.b(sh4, "<set-?>");
        this.p0 = sh4;
    }

    @DexIgnore
    public final void setMGoalnonBrandLineColor(int i2) {
        this.J = i2;
    }

    @DexIgnore
    public final void setMGraphGoalLinePaint(Paint paint) {
        wg6.b(paint, "<set-?>");
        this.r0 = paint;
    }

    @DexIgnore
    public final void setMGraphLegendMargin(float f2) {
        this.a0 = f2;
    }

    @DexIgnore
    public final void setMGraphLegendPoint(ArrayList<lc6<Integer, PointF>> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.b0 = arrayList;
    }

    @DexIgnore
    public final void setMGraphPaint(Paint paint) {
        wg6.b(paint, "<set-?>");
        this.q0 = paint;
    }

    @DexIgnore
    public final void setMHighestColor(int i2) {
        this.z = i2;
    }

    @DexIgnore
    public final void setMInActiveColor(int i2) {
        this.w = i2;
    }

    @DexIgnore
    public final void setMIsFlexibleSize(boolean z2) {
        this.m0 = z2;
    }

    @DexIgnore
    public final void setMLegendIconRes(int i2) {
        this.R = i2;
    }

    @DexIgnore
    public final void setMLegendLinePaint(Paint paint) {
        wg6.b(paint, "<set-?>");
        this.u0 = paint;
    }

    @DexIgnore
    public final void setMLegendPaint(Paint paint) {
        wg6.b(paint, "<set-?>");
        this.t0 = paint;
    }

    @DexIgnore
    public final void setMLegendTexts(ArrayList<String> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.Q = arrayList;
    }

    @DexIgnore
    public final void setMLowestColor(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public final void setMMaxValue(int i2) {
        this.k0 = i2;
    }

    @DexIgnore
    public final void setMNumberBar(int i2) {
        this.j0 = i2;
    }

    @DexIgnore
    public final void setMSafeAreaHeight(float f2) {
        this.c0 = f2;
    }

    @DexIgnore
    public final void setMStarIconPoint(ArrayList<PointF> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.G = arrayList;
    }

    @DexIgnore
    public final void setMStarIconShow(boolean z2) {
        this.H = z2;
    }

    @DexIgnore
    public final void setMStarIconSize(int i2) {
        this.F = i2;
    }

    @DexIgnore
    public final void setMTextColor(String str) {
        wg6.b(str, "<set-?>");
        this.T = str;
    }

    @DexIgnore
    public final void setMTextMargin(int i2) {
        this.U = i2;
    }

    @DexIgnore
    public final void setMTextPoint(ArrayList<lc6<String, PointF>> arrayList) {
        wg6.b(arrayList, "<set-?>");
        this.W = arrayList;
    }

    @DexIgnore
    public final void setMTextSize(float f2) {
        this.S = f2;
    }

    @DexIgnore
    @Keep
    public final void setMaxValue(int i2) {
        this.k0 = i2;
        c();
    }

    @DexIgnore
    public final void setOnTouchListener(e eVar) {
        wg6.b(eVar, "barTouchListener");
        this.u = eVar;
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i2) {
        this(context, attributeSet, i2, 0);
    }

    @DexIgnore
    public final void f() {
        RectF rectF = new RectF(this.h0, this.c0, ((float) getMGraphWidth()) - this.i0, (float) getMGraphHeight());
        this.W.clear();
        a(rectF.left, rectF.right);
    }

    @DexIgnore
    public BarChart(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        Resources.Theme theme;
        TypedArray obtainStyledAttributes;
        String[] strArr;
        this.A = -1;
        this.B = 10;
        this.C = new PointF();
        this.E = -1;
        this.F = 10;
        this.G = new ArrayList<>();
        this.I = 2.0f;
        this.K = 2.0f;
        this.L = 2.0f;
        this.M = "";
        this.N = new Path();
        this.O = "";
        this.P = 2.0f;
        this.Q = new ArrayList<>();
        this.R = -1;
        this.S = 14.0f;
        this.T = "";
        this.U = 10;
        this.V = "";
        this.W = new ArrayList<>();
        this.b0 = new ArrayList<>();
        this.c0 = 50.0f;
        this.e0 = 10.0f;
        this.f0 = 5.0f;
        this.g0 = 30.0f;
        this.h0 = 30.0f;
        this.i0 = 30.0f;
        this.j0 = -1;
        this.l0 = 255;
        this.n0 = "";
        this.o0 = new c(0, 0, (ArrayList) null, 7, (qg6) null);
        this.p0 = sh4.ACTIVE_TIME;
        if (!(attributeSet == null || context == null || (theme = context.getTheme()) == null || (obtainStyledAttributes = theme.obtainStyledAttributes(attributeSet, x24.BaseChart, 0, 0)) == null)) {
            try {
                this.v = obtainStyledAttributes.getColor(0, 0);
                this.w = obtainStyledAttributes.getColor(14, 0);
                this.x = obtainStyledAttributes.getColor(20, 0);
                this.y = obtainStyledAttributes.getColor(2, 0);
                this.z = obtainStyledAttributes.getColor(13, 0);
                this.A = obtainStyledAttributes.getResourceId(5, 10);
                this.B = obtainStyledAttributes.getDimensionPixelSize(6, 10);
                this.E = obtainStyledAttributes.getResourceId(27, 10);
                this.F = obtainStyledAttributes.getDimensionPixelSize(28, 10);
                this.I = (float) obtainStyledAttributes.getDimensionPixelSize(10, 2);
                this.J = obtainStyledAttributes.getColor(7, 0);
                this.K = (float) obtainStyledAttributes.getDimensionPixelSize(9, 2);
                this.L = (float) obtainStyledAttributes.getDimensionPixelSize(8, 2);
                String string = obtainStyledAttributes.getString(11);
                if (string == null) {
                    string = "";
                }
                this.M = string;
                this.S = (float) obtainStyledAttributes.getDimensionPixelSize(31, 14);
                String string2 = obtainStyledAttributes.getString(29);
                if (string2 == null) {
                    string2 = "";
                }
                this.T = string2;
                this.U = obtainStyledAttributes.getDimensionPixelSize(30, 10);
                this.R = obtainStyledAttributes.getResourceId(16, -1);
                String string3 = obtainStyledAttributes.getString(17);
                if (string3 == null) {
                    string3 = "";
                }
                this.O = string3;
                this.P = (float) obtainStyledAttributes.getDimensionPixelSize(18, 2);
                this.a0 = (float) obtainStyledAttributes.getDimensionPixelSize(12, 0);
                obtainStyledAttributes.getDimensionPixelSize(25, -1);
                this.c0 = (float) obtainStyledAttributes.getDimensionPixelSize(24, 50);
                this.d0 = (float) obtainStyledAttributes.getDimensionPixelSize(32, 0);
                this.e0 = (float) obtainStyledAttributes.getDimensionPixelSize(33, 10);
                this.f0 = (float) obtainStyledAttributes.getDimensionPixelSize(23, 5);
                this.h0 = (float) obtainStyledAttributes.getDimensionPixelSize(21, 30);
                this.i0 = (float) obtainStyledAttributes.getDimensionPixelSize(22, (int) this.h0);
                this.g0 = (float) obtainStyledAttributes.getDimensionPixelSize(26, 30);
                setMLegendHeight(obtainStyledAttributes.getDimensionPixelSize(15, 30));
                this.m0 = obtainStyledAttributes.getBoolean(3, false);
                String string4 = obtainStyledAttributes.getString(4);
                if (string4 == null) {
                    string4 = "";
                }
                this.V = string4;
                String string5 = obtainStyledAttributes.getString(1);
                if (string5 == null) {
                    string5 = "";
                }
                this.n0 = string5;
                int resourceId = obtainStyledAttributes.getResourceId(19, -1);
                if (resourceId != -1) {
                    Resources resources = getResources();
                    if (resources == null || (strArr = resources.getStringArray(resourceId)) == null) {
                        strArr = new String[0];
                    }
                    wg6.a((Object) strArr, "(resources?.getStringArr\u2026         ?: emptyArray())");
                    ArrayList<String> arrayList = new ArrayList<>();
                    nd6.b((T[]) strArr, arrayList);
                    this.Q = arrayList;
                }
            } catch (Exception e2) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String tag = getTAG();
                local.d(tag, "constructor - e=" + e2);
            } catch (Throwable th) {
                obtainStyledAttributes.recycle();
                throw th;
            }
            obtainStyledAttributes.recycle();
        }
        a();
    }

    @DexIgnore
    public void d() {
        float f2;
        ArrayList<a> a2 = this.o0.a();
        RectF rectF = new RectF(this.h0, this.c0, ((float) getMGraphWidth()) - this.i0, (float) getMGraphHeight());
        float height = rectF.height();
        float width = rectF.width();
        if (this.m0) {
            float f3 = this.g0;
            int i2 = this.j0;
            this.e0 = (width - (f3 * ((float) (i2 - 1)))) / ((float) i2);
        } else {
            this.g0 = a(width);
        }
        float f4 = this.d0;
        float f5 = this.g0;
        if (f4 > f5 * 0.5f) {
            this.d0 = f5 * 0.5f;
        }
        float f6 = rectF.left;
        float f7 = (float) 10;
        PointF pointF = new PointF(rectF.right + f7, rectF.top);
        PointF pointF2 = new PointF(rectF.right + f7, (rectF.top + rectF.bottom) * 0.5f);
        this.N = new Path();
        this.C = new PointF();
        int i3 = 0;
        this.D = false;
        this.G.clear();
        this.H = false;
        this.b0.clear();
        Iterator<a> it = a2.iterator();
        boolean z2 = true;
        boolean z3 = true;
        float f8 = 0.0f;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(i3);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List<b> a3 = yd6.a(arrayList, new g());
            float f9 = this.e0 + f6;
            float f10 = f8;
            float f11 = 0.0f;
            for (b bVar : a3) {
                f11 += (float) bVar.e();
                float f12 = (f11 * height) / ((float) this.k0);
                if (f12 != 0.0f) {
                    f2 = this.f0;
                    if (f12 < f2) {
                        float mGraphHeight = ((float) getMGraphHeight()) - f2;
                        bVar.a(new RectF(f6, mGraphHeight, f9, (float) getMGraphHeight()));
                        f10 = mGraphHeight;
                    }
                }
                f2 = f12;
                float mGraphHeight2 = ((float) getMGraphHeight()) - f2;
                bVar.a(new RectF(f6, mGraphHeight2, f9, (float) getMGraphHeight()));
                f10 = mGraphHeight2;
            }
            if (f9 > pointF2.x && f10 < pointF2.y) {
                z2 = false;
            }
            if (f9 > pointF.x && f10 < pointF.y) {
                z3 = false;
            }
            f6 = f9 + this.g0;
            f8 = f10;
            i3 = 0;
        }
        if (z2) {
            this.b0.add(new lc6(Integer.valueOf(this.k0 / 2), pointF2));
        }
        if (z3) {
            this.b0.add(new lc6(Integer.valueOf(this.k0), pointF));
            return;
        }
        pointF.set(pointF.x, this.c0 - 10.0f);
        this.b0.add(new lc6(Integer.valueOf(this.k0), pointF));
    }

    @DexIgnore
    public void b(ut4 ut4) {
        wg6.b(ut4, "model");
        this.o0 = (c) ut4;
        this.k0 = this.o0.c();
        if (this.j0 == -1) {
            this.j0 = this.o0.a().size();
        }
    }

    @DexIgnore
    public void e() {
        ArrayList<a> a2 = this.o0.a();
        int b2 = this.o0.b();
        RectF rectF = new RectF(this.h0, this.c0, ((float) getMGraphWidth()) - this.i0, (float) getMGraphHeight());
        float height = rectF.height();
        float f2 = rectF.left;
        Iterator<a> it = a2.iterator();
        float f3 = 0.0f;
        boolean z2 = false;
        float f4 = 0.0f;
        while (it.hasNext()) {
            ArrayList<b> arrayList = it.next().c().get(0);
            wg6.a((Object) arrayList, "item.mListOfBarPoints[0]");
            List<b> a3 = yd6.a(arrayList, new h());
            float f5 = this.e0 + f2;
            float f6 = f4;
            float f7 = 0.0f;
            for (b bVar : a3) {
                f7 += (float) bVar.e();
                float f8 = (f7 * height) / ((float) this.k0);
                if (f8 != 0.0f) {
                    float f9 = this.f0;
                    if (f8 < f9) {
                        f8 = f9;
                    }
                }
                float mGraphHeight = ((float) getMGraphHeight()) - f8;
                bVar.a(new RectF(f2, mGraphHeight, f5, (float) getMGraphHeight()));
                f6 = mGraphHeight;
            }
            f3 += f7;
            if (f3 >= ((float) b2) && !z2) {
                float f10 = (f2 + f5) * 0.5f;
                this.N.moveTo(f10, this.c0 - 10.0f);
                this.N.lineTo(f10, f6);
                this.C.set(f10 - (((float) this.B) * 0.5f), (this.c0 * 0.5f) - 20.0f);
                this.D = true;
                z2 = true;
            }
            f2 = this.g0 + f5;
            f4 = f6;
        }
    }

    @DexIgnore
    public void a(String str, String str2) {
        wg6.b(str, "reachGoal");
        wg6.b(str2, "nonBrandNonReachGoal");
        String b2 = ThemeManager.l.a().b(str);
        String b3 = ThemeManager.l.a().b(str2);
        if (b2 != null) {
            int parseColor = Color.parseColor(b2);
            this.y = parseColor;
            this.J = parseColor;
            this.v = parseColor;
            Paint paint = this.r0;
            if (paint != null) {
                paint.setColor(this.J);
            } else {
                wg6.d("mGraphGoalLinePaint");
                throw null;
            }
        }
        if (b3 != null) {
            this.w = Color.parseColor(b3);
        }
    }

    @DexIgnore
    public void g(Canvas canvas) {
        wg6.b(canvas, "canvas");
        if (this.D) {
            Path path = this.N;
            Paint paint = this.r0;
            if (paint != null) {
                canvas.drawPath(path, paint);
                Bitmap a2 = a(this.A, this.B);
                if (a2 != null) {
                    PointF pointF = this.C;
                    float f2 = pointF.x;
                    float f3 = pointF.y;
                    Paint paint2 = this.s0;
                    if (paint2 != null) {
                        canvas.drawBitmap(a2, f2, f3, paint2);
                        a2.recycle();
                        return;
                    }
                    wg6.d("mGraphIconPaint");
                    throw null;
                }
                return;
            }
            wg6.d("mGraphGoalLinePaint");
            throw null;
        }
    }

    @DexIgnore
    public static /* synthetic */ Bitmap a(BarChart barChart, int i2, int i3, int i4, Object obj) {
        if (obj == null) {
            if ((i4 & 2) != 0) {
                i3 = -1;
            }
            return barChart.a(i2, i3);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: createBitmapByRes");
    }

    @DexIgnore
    public Bitmap a(int i2, int i3) {
        int i4;
        if (i2 == -1) {
            return null;
        }
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), i2);
            if (decodeResource == null) {
                return decodeResource;
            }
            if (i3 == -1) {
                i3 = decodeResource.getWidth();
                i4 = decodeResource.getHeight();
            } else {
                i4 = i3;
            }
            return Bitmap.createScaledBitmap(decodeResource, i3, i4, false);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "createBitmapByRes - e=" + e2);
            return null;
        }
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        wg6.b(motionEvent, Constants.EVENT);
        PointF pointF = new PointF(motionEvent.getX(), motionEvent.getY());
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onGraphOverlayTouchEvent - pointF=" + pointF);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                return false;
            }
            Iterator<a> it = this.o0.a().iterator();
            while (it.hasNext()) {
                a next = it.next();
                Iterator<ArrayList<b>> it2 = next.c().iterator();
                while (it2.hasNext()) {
                    ArrayList next2 = it2.next();
                    if (this.p0 == sh4.TOTAL_SLEEP) {
                        if (BaseChart.t.a(pointF, a(new RectF(((b) next2.get(0)).a().left, ((b) next2.get(next2.size() - 1)).a().top, ((b) next2.get(0)).a().right, ((b) next2.get(0)).a().bottom), this.d0))) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String tag2 = getTAG();
                            local2.d(tag2, "onGraphOverlayTouchEvent for total sleep - ACTION_UP = TRUE, index=" + next.b());
                            e eVar = this.u;
                            if (eVar != null) {
                                eVar.a(next.b());
                            }
                        }
                    } else {
                        Iterator it3 = next2.iterator();
                        while (it3.hasNext()) {
                            if (BaseChart.t.a(pointF, a(((b) it3.next()).a(), this.d0))) {
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String tag3 = getTAG();
                                local3.d(tag3, "onGraphOverlayTouchEvent for normal chart item- ACTION_UP = TRUE, index=" + next.b());
                                e eVar2 = this.u;
                                if (eVar2 != null) {
                                    eVar2.a(next.b());
                                }
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    @DexIgnore
    public float a(float f2) {
        float f3 = this.e0;
        int i2 = this.j0;
        return (f2 - (f3 * ((float) i2))) / ((float) (i2 - 1));
    }

    @DexIgnore
    public void a(float f2, float f3) {
        Rect rect = new Rect();
        Paint paint = this.t0;
        if (paint != null) {
            paint.getTextBounds("12 am", 0, 5, rect);
            float height = ((float) this.U) + ((float) rect.height());
            this.W.add(new lc6("12 am", new PointF(f2, height)));
            this.W.add(new lc6("12 am", new PointF(f3 - ((float) rect.width()), height)));
            Paint paint2 = this.t0;
            if (paint2 != null) {
                paint2.getTextBounds("12 pm", 0, 5, rect);
                this.W.add(new lc6("12 pm", new PointF(((f2 + f3) - ((float) rect.width())) * 0.5f, height)));
                return;
            }
            wg6.d("mLegendPaint");
            throw null;
        }
        wg6.d("mLegendPaint");
        throw null;
    }

    @DexIgnore
    public synchronized void a(ut4 ut4) {
        wg6.b(ut4, "model");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "changeModel - model=" + ut4);
        if (!wg6.a((Object) this.o0, (Object) ut4)) {
            b(ut4);
            c();
        }
    }

    @DexIgnore
    public static /* synthetic */ void a(BarChart barChart, int i2, int i3, int i4, int i5, String str, sh4 sh4, int i6, Object obj) {
        if (obj == null) {
            if ((i6 & 1) != 0) {
                i2 = -1;
            }
            if ((i6 & 2) != 0) {
                i3 = -1;
            }
            if ((i6 & 4) != 0) {
                i4 = -1;
            }
            if ((i6 & 8) != 0) {
                i5 = -1;
            }
            if ((i6 & 16) != 0) {
                str = null;
            }
            if ((i6 & 32) != 0) {
                sh4 = sh4.ACTIVE_TIME;
            }
            barChart.a(i2, i3, i4, i5, str, sh4);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setDataRes");
    }

    @DexIgnore
    public final void a(int i2, int i3, int i4, int i5, String str, sh4 sh4) {
        String str2;
        wg6.b(sh4, "goalType");
        if (i2 == -1) {
            i2 = this.v;
        }
        this.v = i2;
        if (i3 == -1) {
            i3 = this.x;
        }
        this.x = i3;
        if (i4 == -1) {
            i4 = this.y;
        }
        this.y = i4;
        if (i5 == -1) {
            i5 = this.z;
        }
        this.z = i5;
        if (str != null) {
            str2 = str;
        } else {
            str2 = this.M;
        }
        this.M = str2;
        if (sh4 == sh4.ACTIVE_TIME) {
            sh4 = this.p0;
        }
        this.p0 = sh4;
        if (str == null) {
            str = this.M;
        }
        this.M = str;
    }

    @DexIgnore
    public static /* synthetic */ void a(BarChart barChart, ArrayList arrayList, boolean z2, int i2, Object obj) {
        if (obj == null) {
            if ((i2 & 2) != 0) {
                z2 = false;
            }
            barChart.a((ArrayList<String>) arrayList, z2);
            return;
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: setLegendTexts");
    }

    @DexIgnore
    public final void a(ArrayList<String> arrayList, boolean z2) {
        wg6.b(arrayList, "array");
        this.Q.clear();
        this.Q.addAll(arrayList);
        if (z2) {
            getMLegend().invalidate();
        }
    }

    @DexIgnore
    public final RectF a(RectF rectF, float f2) {
        return new RectF(rectF.left - f2, rectF.top - f2, rectF.right + f2, rectF.bottom + f2);
    }
}
