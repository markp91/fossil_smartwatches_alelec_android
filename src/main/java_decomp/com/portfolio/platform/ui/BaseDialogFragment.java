package com.portfolio.platform.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.hc;
import com.fossil.o34;
import com.fossil.pw6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseDialogFragment extends DialogFragment implements pw6.a, DialogInterface.OnKeyListener {
    @DexIgnore
    public /* final */ String a; // = BaseDialogFragment.class.getSimpleName();

    @DexIgnore
    public BaseDialogFragment() {
        new o34(this);
    }

    @DexIgnore
    public void a(int i, List<String> list) {
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local.d(str2, "onPermissionsDenied: perm = " + str);
        }
    }

    @DexIgnore
    public void b(int i, List<String> list) {
        for (String str : list) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local.d(str2, "onPermissionsGranted: perm = " + str);
        }
    }

    @DexIgnore
    public boolean d1() {
        dismiss();
        return true;
    }

    @DexIgnore
    public void dismiss() {
        if (getFragmentManager() != null) {
            hc b = getFragmentManager().b();
            b.d(this);
            b.b();
        }
    }

    @DexIgnore
    public void onCreate(Bundle bundle) {
        BaseDialogFragment.super.onCreate(bundle);
        setStyle(1, 2131951629);
        AnalyticsHelper.f.c();
    }

    @DexIgnore
    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i != 4) {
            return false;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "onKey KEYCODE_BACK");
        return d1();
    }

    @DexIgnore
    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        BaseDialogFragment.super.onRequestPermissionsResult(i, strArr, iArr);
        pw6.a(i, strArr, iArr, new Object[]{this});
    }

    @DexIgnore
    public void setupDialog(Dialog dialog, int i) {
        dialog.requestWindowFeature(1);
        if (dialog.getWindow() != null) {
            dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
            dialog.getWindow().setLayout(-1, -1);
            dialog.getWindow().getDecorView().setSystemUiVisibility(3328);
            dialog.setOnKeyListener(this);
        }
    }

    @DexIgnore
    public void show(FragmentManager fragmentManager, String str) {
        if (!isAdded()) {
            hc b = fragmentManager.b();
            Fragment b2 = fragmentManager.b(str);
            if (b2 != null) {
                b.d(b2);
            }
            b.a((String) null);
            b.a(this, str);
            b.b();
            fragmentManager.r();
        }
    }
}
