package com.portfolio.platform.ui.user.usecase;

import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.m24;
import com.fossil.nc6;
import com.fossil.rt4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.Auth;
import com.portfolio.platform.data.SignUpEmailAuth;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SignUpEmailUseCase extends m24<rt4.a, rt4.c, rt4.b> {
    @DexIgnore
    public /* final */ UserRepository d;
    @DexIgnore
    public /* final */ an4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ SignUpEmailAuth a;

        @DexIgnore
        public a(SignUpEmailAuth signUpEmailAuth) {
            wg6.b(signUpEmailAuth, "emailAuth");
            this.a = signUpEmailAuth;
        }

        @DexIgnore
        public final SignUpEmailAuth a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.a {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(int i, String str) {
            wg6.b(str, "errorMesagge");
            this.a = i;
            this.b = str;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.d {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.ui.user.usecase.SignUpEmailUseCase", f = "SignUpEmailUseCase.kt", l = {29}, m = "run")
    public static final class d extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ SignUpEmailUseCase this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(SignUpEmailUseCase signUpEmailUseCase, xe6 xe6) {
            super(xe6);
            this.this$0 = signUpEmailUseCase;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((rt4.a) null, (xe6<Object>) this);
        }
    }

    @DexIgnore
    public SignUpEmailUseCase(UserRepository userRepository, an4 an4) {
        wg6.b(userRepository, "mUserRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = userRepository;
        this.e = an4;
    }

    @DexIgnore
    public String c() {
        return "SignUpEmailUseCase";
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00a4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0029  */
    public Object a(rt4.a aVar, xe6<Object> xe6) {
        d dVar;
        int i;
        SignUpEmailUseCase signUpEmailUseCase;
        ap4 ap4;
        String str;
        if (xe6 instanceof d) {
            dVar = (d) xe6;
            int i2 = dVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                dVar.label = i2 - Integer.MIN_VALUE;
                Object obj = dVar.result;
                Object a2 = ff6.a();
                i = dVar.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "running UseCase");
                    if (aVar == null) {
                        return new b(600, "");
                    }
                    if (this.d.getCurrentUser() != null) {
                        return new c();
                    }
                    UserRepository userRepository = this.d;
                    SignUpEmailAuth a3 = aVar.a();
                    dVar.L$0 = this;
                    dVar.L$1 = aVar;
                    dVar.label = 1;
                    obj = userRepository.signUpEmail(a3, dVar);
                    if (obj == a2) {
                        return a2;
                    }
                    signUpEmailUseCase = this;
                } else if (i == 1) {
                    a aVar2 = (a) dVar.L$1;
                    signUpEmailUseCase = (SignUpEmailUseCase) dVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                String str2 = null;
                if (!(ap4 instanceof cp4)) {
                    FLogger.INSTANCE.getLocal().d("SignUpEmailUseCase", "signUpEmail success");
                    an4 an4 = signUpEmailUseCase.e;
                    Auth auth = (Auth) ((cp4) ap4).a();
                    if (auth != null) {
                        str2 = auth.getAccessToken();
                    }
                    an4.w(str2);
                    return new c();
                } else if (!(ap4 instanceof zo4)) {
                    return new b(600, "");
                } else {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("signUpEmail failed code=");
                    zo4 zo4 = (zo4) ap4;
                    ServerError c2 = zo4.c();
                    if (c2 != null) {
                        str2 = c2.getMessage();
                    }
                    sb.append(str2);
                    local.d("SignUpEmailUseCase", sb.toString());
                    int a4 = zo4.a();
                    ServerError c3 = zo4.c();
                    if (c3 == null || (str = c3.getMessage()) == null) {
                        str = "";
                    }
                    return new b(a4, str);
                }
            }
        }
        dVar = new d(this, xe6);
        Object obj2 = dVar.result;
        Object a22 = ff6.a();
        i = dVar.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        String str22 = null;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
