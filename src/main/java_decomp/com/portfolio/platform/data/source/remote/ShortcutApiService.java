package com.portfolio.platform.data.source.remote;

import com.fossil.ny6;
import com.fossil.zy6;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.RecommendedPreset;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ShortcutApiService {
    @DexIgnore
    @ny6("default-presets")
    Call<ApiResponse<RecommendedPreset>> getDefaultPreset(@zy6("offset") int i, @zy6("size") int i2, @zy6("serialNumber") String str);

    @DexIgnore
    @ny6("micro-apps")
    Call<ApiResponse<MicroApp>> getMicroAppGallery(@zy6("page") int i, @zy6("size") int i2, @zy6("serialNumber") String str);
}
