package com.portfolio.platform.data.source;

import android.content.Context;
import androidx.lifecycle.LiveData;
import com.fossil.cd6;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.jm4;
import com.fossil.nc6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.QuickResponseMessage;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseMessageDao;
import com.portfolio.platform.data.source.local.quickresponse.QuickResponseSenderDao;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ QuickResponseMessageDao mQRMessageDao;
    @DexIgnore
    public /* final */ QuickResponseSenderDao mQuickResponseSenderDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return QuickResponseRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = QuickResponseRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "QuickResponseRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public QuickResponseRepository(QuickResponseMessageDao quickResponseMessageDao, QuickResponseSenderDao quickResponseSenderDao) {
        wg6.b(quickResponseMessageDao, "mQRMessageDao");
        wg6.b(quickResponseSenderDao, "mQuickResponseSenderDao");
        this.mQRMessageDao = quickResponseMessageDao;
        this.mQuickResponseSenderDao = quickResponseSenderDao;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r5v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x00c7 A[LOOP:0: B:20:0x00c1->B:22:0x00c7, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getAllQuickResponse(xe6<? super List<QuickResponseMessage>> xe6) {
        QuickResponseRepository$getAllQuickResponse$Anon1 quickResponseRepository$getAllQuickResponse$Anon1;
        int i;
        List<QuickResponseMessage> list;
        if (xe6 instanceof QuickResponseRepository$getAllQuickResponse$Anon1) {
            quickResponseRepository$getAllQuickResponse$Anon1 = (QuickResponseRepository$getAllQuickResponse$Anon1) xe6;
            int i2 = quickResponseRepository$getAllQuickResponse$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                quickResponseRepository$getAllQuickResponse$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = quickResponseRepository$getAllQuickResponse$Anon1.result;
                Object a = ff6.a();
                i = quickResponseRepository$getAllQuickResponse$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    List<QuickResponseMessage> allRawResponse = this.mQRMessageDao.getAllRawResponse();
                    if (!allRawResponse.isEmpty()) {
                        return allRawResponse;
                    }
                    String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131887257);
                    wg6.a((Object) a2, "LanguageHelper.getString\u2026.string.qr_call_you_back)");
                    String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887259);
                    wg6.a((Object) a3, "LanguageHelper.getString\u2026e, R.string.qr_on_my_way)");
                    String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131887258);
                    wg6.a((Object) a4, "LanguageHelper.getString\u2026.string.qr_cant_talk_now)");
                    List c = qd6.c(new QuickResponseMessage(a2), new QuickResponseMessage(a3), new QuickResponseMessage(a4));
                    dl6 b = zl6.b();
                    QuickResponseRepository$getAllQuickResponse$Anon2 quickResponseRepository$getAllQuickResponse$Anon2 = new QuickResponseRepository$getAllQuickResponse$Anon2(this, c, (xe6) null);
                    quickResponseRepository$getAllQuickResponse$Anon1.L$0 = this;
                    quickResponseRepository$getAllQuickResponse$Anon1.L$1 = allRawResponse;
                    quickResponseRepository$getAllQuickResponse$Anon1.L$2 = c;
                    quickResponseRepository$getAllQuickResponse$Anon1.label = 1;
                    if (gk6.a(b, quickResponseRepository$getAllQuickResponse$Anon2, quickResponseRepository$getAllQuickResponse$Anon1) == a) {
                        return a;
                    }
                    list = c;
                } else if (i == 1) {
                    list = (List) quickResponseRepository$getAllQuickResponse$Anon1.L$2;
                    List list2 = (List) quickResponseRepository$getAllQuickResponse$Anon1.L$1;
                    QuickResponseRepository quickResponseRepository = (QuickResponseRepository) quickResponseRepository$getAllQuickResponse$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                for (QuickResponseMessage quickResponseMessage : list) {
                    quickResponseMessage.setId(list.indexOf(quickResponseMessage) + 1);
                }
                return list;
            }
        }
        quickResponseRepository$getAllQuickResponse$Anon1 = new QuickResponseRepository$getAllQuickResponse$Anon1(this, xe6);
        Object obj2 = quickResponseRepository$getAllQuickResponse$Anon1.result;
        Object a5 = ff6.a();
        i = quickResponseRepository$getAllQuickResponse$Anon1.label;
        if (i != 0) {
        }
        while (r9.hasNext()) {
        }
        return list;
    }

    @DexIgnore
    public final LiveData<List<QuickResponseMessage>> getAllQuickResponseLiveData() {
        return this.mQRMessageDao.getAllResponse();
    }

    @DexIgnore
    public final Object getQuickResponseSender(int i, xe6<? super QuickResponseSender> xe6) {
        return this.mQuickResponseSenderDao.getQuickResponseSenderById(i);
    }

    @DexIgnore
    public final Object insertQR(QuickResponseMessage quickResponseMessage, xe6<? super cd6> xe6) {
        this.mQRMessageDao.insertResponse(quickResponseMessage);
        return cd6.a;
    }

    @DexIgnore
    public final Object insertQRs(List<QuickResponseMessage> list, xe6<? super cd6> xe6) {
        this.mQRMessageDao.insertResponses(list);
        return cd6.a;
    }

    @DexIgnore
    public final Object removeAll(xe6<? super cd6> xe6) {
        this.mQRMessageDao.removeAll();
        return cd6.a;
    }

    @DexIgnore
    public final Object removeQRById(int i, xe6<? super cd6> xe6) {
        this.mQRMessageDao.removeById(i);
        return cd6.a;
    }

    @DexIgnore
    public final Object updateQR(QuickResponseMessage quickResponseMessage, xe6<? super cd6> xe6) {
        this.mQRMessageDao.update(quickResponseMessage);
        return cd6.a;
    }

    @DexIgnore
    public final Object upsertQuickResponseSender(QuickResponseSender quickResponseSender, xe6<? super Long> xe6) {
        return hf6.a(this.mQuickResponseSenderDao.insertQuickResponseSender(quickResponseSender));
    }

    @DexIgnore
    public final Object updateQR(List<QuickResponseMessage> list, xe6<? super cd6> xe6) {
        this.mQRMessageDao.update(list);
        return cd6.a;
    }
}
