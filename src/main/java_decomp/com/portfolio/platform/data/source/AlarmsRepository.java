package com.portfolio.platform.data.source;

import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.dl6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.nd6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ AlarmsRemoteDataSource mAlarmRemoteDataSource;
    @DexIgnore
    public /* final */ AlarmsLocalDataSource mAlarmsLocalDataSource;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = AlarmsRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmsRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public AlarmsRepository(AlarmsLocalDataSource alarmsLocalDataSource, AlarmsRemoteDataSource alarmsRemoteDataSource) {
        wg6.b(alarmsLocalDataSource, "mAlarmsLocalDataSource");
        wg6.b(alarmsRemoteDataSource, "mAlarmRemoteDataSource");
        this.mAlarmsLocalDataSource = alarmsLocalDataSource;
        this.mAlarmRemoteDataSource = alarmsRemoteDataSource;
    }

    @DexIgnore
    public final void cleanUp() {
        this.mAlarmsLocalDataSource.cleanUp();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00bf  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object deleteAlarm(Alarm alarm, xe6<? super ap4<Void>> xe6) {
        AlarmsRepository$deleteAlarm$Anon1 alarmsRepository$deleteAlarm$Anon1;
        int i;
        ap4 ap4;
        AlarmsRepository alarmsRepository;
        ap4 ap42;
        if (xe6 instanceof AlarmsRepository$deleteAlarm$Anon1) {
            alarmsRepository$deleteAlarm$Anon1 = (AlarmsRepository$deleteAlarm$Anon1) xe6;
            int i2 = alarmsRepository$deleteAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$deleteAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$deleteAlarm$Anon1.result;
                Object a = ff6.a();
                i = alarmsRepository$deleteAlarm$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "deleteAlarm - alarm= " + alarm.getTitle() + " - " + alarm.getId() + " - " + alarm.getPinType());
                    this.mAlarmsLocalDataSource.deleteAlarm(alarm);
                    if (alarm.getPinType() == 1 || TextUtils.isEmpty(alarm.getId())) {
                        return new cp4((Object) null, false, 2, (qg6) null);
                    }
                    AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                    String id = alarm.getId();
                    if (id != null) {
                        alarmsRepository$deleteAlarm$Anon1.L$0 = this;
                        alarmsRepository$deleteAlarm$Anon1.L$1 = alarm;
                        alarmsRepository$deleteAlarm$Anon1.label = 1;
                        obj = alarmsRemoteDataSource.deleteAlarm(id, alarmsRepository$deleteAlarm$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        alarmsRepository = this;
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else if (i == 1) {
                    alarm = (Alarm) alarmsRepository$deleteAlarm$Anon1.L$1;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$deleteAlarm$Anon1.L$0;
                    nc6.a(obj);
                } else if (i == 2) {
                    ap4 = (ap4) alarmsRepository$deleteAlarm$Anon1.L$2;
                    Alarm alarm2 = (Alarm) alarmsRepository$deleteAlarm$Anon1.L$1;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$deleteAlarm$Anon1.L$0;
                    nc6.a(obj);
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap42 = (ap4) obj;
                if (!(ap42 instanceof cp4)) {
                    return new cp4((Object) null, false, 2, (qg6) null);
                }
                if (ap42 instanceof zo4) {
                    dl6 b = zl6.b();
                    AlarmsRepository$deleteAlarm$Anon2 alarmsRepository$deleteAlarm$Anon2 = new AlarmsRepository$deleteAlarm$Anon2(alarmsRepository, alarm, (xe6) null);
                    alarmsRepository$deleteAlarm$Anon1.L$0 = alarmsRepository;
                    alarmsRepository$deleteAlarm$Anon1.L$1 = alarm;
                    alarmsRepository$deleteAlarm$Anon1.L$2 = ap42;
                    alarmsRepository$deleteAlarm$Anon1.label = 2;
                    if (gk6.a(b, alarmsRepository$deleteAlarm$Anon2, alarmsRepository$deleteAlarm$Anon1) == a) {
                        return a;
                    }
                    ap4 = ap42;
                    zo4 zo42 = (zo4) ap4;
                    return new zo4(zo42.a(), zo42.c(), zo42.d(), (String) null, 8, (qg6) null);
                }
                throw new kc6();
            }
        }
        alarmsRepository$deleteAlarm$Anon1 = new AlarmsRepository$deleteAlarm$Anon1(this, xe6);
        Object obj2 = alarmsRepository$deleteAlarm$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRepository$deleteAlarm$Anon1.label;
        if (i != 0) {
        }
        ap42 = (ap4) obj2;
        if (!(ap42 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0093  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00cf  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final Object downloadAlarms(xe6<? super cd6> xe6) {
        AlarmsRepository$downloadAlarms$Anon1 alarmsRepository$downloadAlarms$Anon1;
        int i;
        boolean z;
        AlarmsRepository alarmsRepository;
        ap4 ap4;
        boolean booleanValue;
        if (xe6 instanceof AlarmsRepository$downloadAlarms$Anon1) {
            alarmsRepository$downloadAlarms$Anon1 = (AlarmsRepository$downloadAlarms$Anon1) xe6;
            int i2 = alarmsRepository$downloadAlarms$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$downloadAlarms$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$downloadAlarms$Anon1.result;
                Object a = ff6.a();
                i = alarmsRepository$downloadAlarms$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "start download");
                    alarmsRepository$downloadAlarms$Anon1.L$0 = this;
                    alarmsRepository$downloadAlarms$Anon1.label = 1;
                    obj = executePendingRequest(alarmsRepository$downloadAlarms$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    alarmsRepository = this;
                } else if (i == 1) {
                    alarmsRepository = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$0;
                    nc6.a(obj);
                } else if (i == 2) {
                    boolean z2 = alarmsRepository$downloadAlarms$Anon1.Z$0;
                    nc6.a(obj);
                    z = z2;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$0;
                    ap4 = (ap4) obj;
                    if (!(ap4 instanceof cp4)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.d(str, "downloadAlarms success isFromCache " + ((cp4) ap4).b());
                        dl6 b = zl6.b();
                        AlarmsRepository$downloadAlarms$Anon2 alarmsRepository$downloadAlarms$Anon2 = new AlarmsRepository$downloadAlarms$Anon2(alarmsRepository, ap4, (xe6) null);
                        alarmsRepository$downloadAlarms$Anon1.L$0 = alarmsRepository;
                        alarmsRepository$downloadAlarms$Anon1.Z$0 = z;
                        alarmsRepository$downloadAlarms$Anon1.L$1 = ap4;
                        alarmsRepository$downloadAlarms$Anon1.label = 3;
                        if (gk6.a(b, alarmsRepository$downloadAlarms$Anon2, alarmsRepository$downloadAlarms$Anon1) == a) {
                            return a;
                        }
                    } else if (ap4 instanceof zo4) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = TAG;
                        local2.d(str2, "downloadAlarms fail!! " + ((zo4) ap4).a());
                    }
                    return cd6.a;
                } else if (i == 3) {
                    ap4 ap42 = (ap4) alarmsRepository$downloadAlarms$Anon1.L$1;
                    boolean z3 = alarmsRepository$downloadAlarms$Anon1.Z$0;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$downloadAlarms$Anon1.L$0;
                    nc6.a(obj);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                booleanValue = ((Boolean) obj).booleanValue();
                if (booleanValue) {
                    AlarmsRemoteDataSource alarmsRemoteDataSource = alarmsRepository.mAlarmRemoteDataSource;
                    alarmsRepository$downloadAlarms$Anon1.L$0 = alarmsRepository;
                    alarmsRepository$downloadAlarms$Anon1.Z$0 = booleanValue;
                    alarmsRepository$downloadAlarms$Anon1.label = 2;
                    Object alarms = alarmsRemoteDataSource.getAlarms(alarmsRepository$downloadAlarms$Anon1);
                    if (alarms == a) {
                        return a;
                    }
                    Object obj2 = alarms;
                    z = booleanValue;
                    obj = obj2;
                    ap4 = (ap4) obj;
                    if (!(ap4 instanceof cp4)) {
                    }
                    return cd6.a;
                }
                FLogger.INSTANCE.getLocal().d(TAG, "downloadAlarms not execute due to pending data");
                return cd6.a;
            }
        }
        alarmsRepository$downloadAlarms$Anon1 = new AlarmsRepository$downloadAlarms$Anon1(this, xe6);
        Object obj3 = alarmsRepository$downloadAlarms$Anon1.result;
        Object a2 = ff6.a();
        i = alarmsRepository$downloadAlarms$Anon1.label;
        if (i != 0) {
        }
        booleanValue = ((Boolean) obj3).booleanValue();
        if (booleanValue) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x01c5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x01ea A[SYNTHETIC, Splitter:B:66:0x01ea] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x021f A[LOOP:3: B:75:0x0219->B:77:0x021f, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x026f  */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x028a  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x02a9 A[SYNTHETIC, Splitter:B:92:0x02a9] */
    public final synchronized Object executePendingRequest(xe6<? super Boolean> xe6) {
        AlarmsRepository$executePendingRequest$Anon1 alarmsRepository$executePendingRequest$Anon1;
        int i;
        AlarmsRepository alarmsRepository;
        List<Alarm> list;
        List<Alarm> list2;
        List list3;
        ap4 ap4;
        AlarmsRepository alarmsRepository2;
        List<Alarm> list4;
        ArrayList arrayList;
        List<Alarm> list5;
        ap4 ap42;
        xe6<? super Boolean> xe62 = xe6;
        synchronized (this) {
            if (xe62 instanceof AlarmsRepository$executePendingRequest$Anon1) {
                alarmsRepository$executePendingRequest$Anon1 = (AlarmsRepository$executePendingRequest$Anon1) xe62;
                if ((alarmsRepository$executePendingRequest$Anon1.label & Integer.MIN_VALUE) != 0) {
                    alarmsRepository$executePendingRequest$Anon1.label -= Integer.MIN_VALUE;
                    Object obj = alarmsRepository$executePendingRequest$Anon1.result;
                    Object a = ff6.a();
                    i = alarmsRepository$executePendingRequest$Anon1.label;
                    if (i != 0) {
                        nc6.a(obj);
                        List<Alarm> allPendingAlarm = this.mAlarmsLocalDataSource.getAllPendingAlarm();
                        FLogger.INSTANCE.getLocal().d(TAG, "executePendingRequest allPendingAlarm " + allPendingAlarm.size());
                        if (allPendingAlarm.isEmpty()) {
                            Boolean a2 = hf6.a(false);
                            return a2;
                        }
                        list5 = new ArrayList<>();
                        for (T next : allPendingAlarm) {
                            if (hf6.a(((Alarm) next).getPinType() != 3).booleanValue()) {
                                list5.add(next);
                            }
                        }
                        ArrayList arrayList2 = new ArrayList();
                        for (T next2 : allPendingAlarm) {
                            if (hf6.a(((Alarm) next2).getPinType() == 3).booleanValue()) {
                                arrayList2.add(next2);
                            }
                        }
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("executePendingRequest upsertAlarmPendingList ");
                        ArrayList arrayList3 = new ArrayList(rd6.a(list5, 10));
                        for (Alarm alarm : list5) {
                            arrayList3.add(alarm.getUri() + " - " + alarm.getId() + " - " + alarm.getPinType() + " - " + alarm.getTitle());
                        }
                        sb.append(arrayList3);
                        local.d(str, sb.toString());
                        if (!list5.isEmpty()) {
                            AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                            alarmsRepository$executePendingRequest$Anon1.L$0 = this;
                            alarmsRepository$executePendingRequest$Anon1.L$1 = allPendingAlarm;
                            alarmsRepository$executePendingRequest$Anon1.L$2 = list5;
                            alarmsRepository$executePendingRequest$Anon1.L$3 = arrayList2;
                            alarmsRepository$executePendingRequest$Anon1.label = 1;
                            Object upsertAlarms = alarmsRemoteDataSource.upsertAlarms(list5, alarmsRepository$executePendingRequest$Anon1);
                            if (upsertAlarms == a) {
                                return a;
                            }
                            alarmsRepository2 = this;
                            ArrayList arrayList4 = arrayList2;
                            list4 = allPendingAlarm;
                            obj = upsertAlarms;
                            arrayList = arrayList4;
                        } else {
                            list = allPendingAlarm;
                            alarmsRepository = this;
                            list2 = list5;
                            list3 = arrayList2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("executePendingRequest deleteAlarmPendingList ");
                            ArrayList arrayList5 = new ArrayList(rd6.a(list2, 10));
                            while (r13.hasNext()) {
                            }
                            sb2.append(arrayList5);
                            local2.d(str2, sb2.toString());
                            if (!list3.isEmpty()) {
                            }
                            Boolean a3 = hf6.a(false);
                            return a3;
                        }
                    } else if (i == 1) {
                        alarmsRepository2 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$0;
                        nc6.a(obj);
                        arrayList = (List) alarmsRepository$executePendingRequest$Anon1.L$3;
                        list5 = (List) alarmsRepository$executePendingRequest$Anon1.L$2;
                        list4 = (List) alarmsRepository$executePendingRequest$Anon1.L$1;
                    } else if (i == 2) {
                        ap4 ap43 = (ap4) alarmsRepository$executePendingRequest$Anon1.L$4;
                        list4 = (List) alarmsRepository$executePendingRequest$Anon1.L$1;
                        nc6.a(obj);
                        arrayList = (List) alarmsRepository$executePendingRequest$Anon1.L$3;
                        list5 = (List) alarmsRepository$executePendingRequest$Anon1.L$2;
                        alarmsRepository2 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$0;
                        list = list4;
                        alarmsRepository = alarmsRepository2;
                        List list6 = arrayList;
                        list2 = list5;
                        list3 = list6;
                        ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
                        String str22 = TAG;
                        StringBuilder sb22 = new StringBuilder();
                        sb22.append("executePendingRequest deleteAlarmPendingList ");
                        ArrayList arrayList52 = new ArrayList(rd6.a(list2, 10));
                        for (Alarm alarm2 : list2) {
                            arrayList52.add(alarm2.getUri() + " - " + alarm2.getId() + " - " + alarm2.getPinType() + " - " + alarm2.getTitle());
                        }
                        sb22.append(arrayList52);
                        local22.d(str22, sb22.toString());
                        if (!list3.isEmpty()) {
                            AlarmsRemoteDataSource alarmsRemoteDataSource2 = alarmsRepository.mAlarmRemoteDataSource;
                            alarmsRepository$executePendingRequest$Anon1.L$0 = alarmsRepository;
                            alarmsRepository$executePendingRequest$Anon1.L$1 = list;
                            alarmsRepository$executePendingRequest$Anon1.L$2 = list2;
                            alarmsRepository$executePendingRequest$Anon1.L$3 = list3;
                            alarmsRepository$executePendingRequest$Anon1.label = 3;
                            obj = alarmsRemoteDataSource2.deleteAlarms(list3, alarmsRepository$executePendingRequest$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            ap4 = (ap4) obj;
                            if (!(ap4 instanceof cp4)) {
                            }
                        }
                        Boolean a32 = hf6.a(false);
                        return a32;
                    } else if (i == 3) {
                        list3 = (List) alarmsRepository$executePendingRequest$Anon1.L$3;
                        list2 = (List) alarmsRepository$executePendingRequest$Anon1.L$2;
                        list = (List) alarmsRepository$executePendingRequest$Anon1.L$1;
                        alarmsRepository = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$0;
                        nc6.a(obj);
                        ap4 = (ap4) obj;
                        if (!(ap4 instanceof cp4)) {
                            dl6 b = zl6.b();
                            AlarmsRepository$executePendingRequest$Anon5 alarmsRepository$executePendingRequest$Anon5 = new AlarmsRepository$executePendingRequest$Anon5(alarmsRepository, list3, (xe6) null);
                            alarmsRepository$executePendingRequest$Anon1.L$0 = alarmsRepository;
                            alarmsRepository$executePendingRequest$Anon1.L$1 = list;
                            alarmsRepository$executePendingRequest$Anon1.L$2 = list2;
                            alarmsRepository$executePendingRequest$Anon1.L$3 = list3;
                            alarmsRepository$executePendingRequest$Anon1.label = 4;
                            if (gk6.a(b, alarmsRepository$executePendingRequest$Anon5, alarmsRepository$executePendingRequest$Anon1) == a) {
                                return a;
                            }
                        } else if (ap4 instanceof zo4) {
                            Boolean a4 = hf6.a(true);
                            return a4;
                        }
                        Boolean a322 = hf6.a(false);
                        return a322;
                    } else if (i == 4) {
                        List list7 = (List) alarmsRepository$executePendingRequest$Anon1.L$3;
                        List list8 = (List) alarmsRepository$executePendingRequest$Anon1.L$2;
                        List list9 = (List) alarmsRepository$executePendingRequest$Anon1.L$1;
                        AlarmsRepository alarmsRepository3 = (AlarmsRepository) alarmsRepository$executePendingRequest$Anon1.L$0;
                        nc6.a(obj);
                        Boolean a3222 = hf6.a(false);
                        return a3222;
                    } else {
                        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                    }
                    ap42 = (ap4) obj;
                    if (!(ap42 instanceof cp4)) {
                        dl6 b2 = zl6.b();
                        AlarmsRepository$executePendingRequest$Anon3 alarmsRepository$executePendingRequest$Anon3 = new AlarmsRepository$executePendingRequest$Anon3(alarmsRepository2, ap42, (xe6) null);
                        alarmsRepository$executePendingRequest$Anon1.L$0 = alarmsRepository2;
                        alarmsRepository$executePendingRequest$Anon1.L$1 = list4;
                        alarmsRepository$executePendingRequest$Anon1.L$2 = list5;
                        alarmsRepository$executePendingRequest$Anon1.L$3 = arrayList;
                        alarmsRepository$executePendingRequest$Anon1.L$4 = ap42;
                        alarmsRepository$executePendingRequest$Anon1.label = 2;
                        if (gk6.a(b2, alarmsRepository$executePendingRequest$Anon3, alarmsRepository$executePendingRequest$Anon1) == a) {
                            return a;
                        }
                    } else if (ap42 instanceof zo4) {
                        Boolean a5 = hf6.a(true);
                        return a5;
                    }
                    list = list4;
                    alarmsRepository = alarmsRepository2;
                    List list62 = arrayList;
                    list2 = list5;
                    list3 = list62;
                    ILocalFLogger local222 = FLogger.INSTANCE.getLocal();
                    String str222 = TAG;
                    StringBuilder sb222 = new StringBuilder();
                    sb222.append("executePendingRequest deleteAlarmPendingList ");
                    ArrayList arrayList522 = new ArrayList(rd6.a(list2, 10));
                    while (r13.hasNext()) {
                    }
                    sb222.append(arrayList522);
                    local222.d(str222, sb222.toString());
                    if (!list3.isEmpty()) {
                    }
                    Boolean a32222 = hf6.a(false);
                    return a32222;
                }
            }
            alarmsRepository$executePendingRequest$Anon1 = new AlarmsRepository$executePendingRequest$Anon1(this, xe62);
            Object obj2 = alarmsRepository$executePendingRequest$Anon1.result;
            Object a6 = ff6.a();
            i = alarmsRepository$executePendingRequest$Anon1.label;
            if (i != 0) {
            }
            ap42 = (ap4) obj2;
            if (!(ap42 instanceof cp4)) {
            }
            list = list4;
            alarmsRepository = alarmsRepository2;
            List list622 = arrayList;
            list2 = list5;
            list3 = list622;
            ILocalFLogger local2222 = FLogger.INSTANCE.getLocal();
            String str2222 = TAG;
            StringBuilder sb2222 = new StringBuilder();
            sb2222.append("executePendingRequest deleteAlarmPendingList ");
            ArrayList arrayList5222 = new ArrayList(rd6.a(list2, 10));
            while (r13.hasNext()) {
            }
            sb2222.append(arrayList5222);
            local2222.d(str2222, sb2222.toString());
            if (!list3.isEmpty()) {
            }
            Boolean a322222 = hf6.a(false);
            return a322222;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0060, code lost:
        if ((r8.length == 0) != false) goto L_0x0067;
     */
    @DexIgnore
    public final Alarm findIncomingActiveAlarm() {
        Alarm alarm;
        FLogger.INSTANCE.getLocal().d(TAG, "findIncomingActiveAlarm()");
        Calendar instance = Calendar.getInstance();
        int i = (instance.get(11) * 60) + instance.get(12);
        int i2 = instance.get(7);
        List<Alarm> activeAlarms = this.mAlarmsLocalDataSource.getActiveAlarms();
        Alarm alarm2 = null;
        if (activeAlarms != null) {
            Alarm alarm3 = null;
            alarm = null;
            for (Alarm alarm4 : activeAlarms) {
                if (alarm4.getDays() != null) {
                    int[] days = alarm4.getDays();
                    if (days != null) {
                        if (!nd6.a(days, i2)) {
                            int[] days2 = alarm4.getDays();
                            if (days2 == null) {
                                wg6.a();
                                throw null;
                            }
                        }
                        if (alarm4.getTotalMinutes() > i) {
                            continue;
                        } else {
                            if (alarm3 != null) {
                                if (alarm3 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (alarm3.getTotalMinutes() <= alarm4.getTotalMinutes()) {
                                }
                            }
                            alarm3 = alarm4;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
                if (alarm4.getTotalMinutes() > i) {
                    int[] iArr = new int[1];
                    if (i2 < 7) {
                        iArr[0] = i2 + 1;
                    } else {
                        iArr[0] = 1;
                    }
                    alarm4.setDays(iArr);
                    if (alarm != null) {
                        if (alarm == null) {
                            wg6.a();
                            throw null;
                        } else if (alarm.getTotalMinutes() <= alarm4.getTotalMinutes()) {
                        }
                    }
                    alarm = alarm4;
                } else {
                    continue;
                }
            }
            alarm2 = alarm3;
        } else {
            alarm = null;
        }
        return alarm2 != null ? alarm2 : alarm;
    }

    @DexIgnore
    public final Alarm findNextActiveAlarm() {
        return this.mAlarmsLocalDataSource.findNexActiveAlarm();
    }

    @DexIgnore
    public final List<Alarm> getActiveAlarms() {
        return this.mAlarmsLocalDataSource.getActiveAlarms();
    }

    @DexIgnore
    public final Alarm getAlarmById(String str) {
        wg6.b(str, "id");
        return this.mAlarmsLocalDataSource.getAlarmById(str);
    }

    @DexIgnore
    public final List<Alarm> getAllAlarmIgnoreDeletePinType() {
        return this.mAlarmsLocalDataSource.getAllAlarmIgnoreDeletePinType();
    }

    @DexIgnore
    public final List<Alarm> getInComingActiveAlarms() {
        return this.mAlarmsLocalDataSource.getInComingActiveAlarms();
    }

    @DexIgnore
    public final Object insertAlarm(Alarm alarm, xe6<? super ap4<Alarm>> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertAlarm - alarmId=" + alarm.getId());
        alarm.setPinType(1);
        this.mAlarmsLocalDataSource.upsertAlarm(alarm);
        return upsertAlarm(alarm, xe6);
    }

    @DexIgnore
    public final Object updateAlarm(Alarm alarm, xe6<? super ap4<Alarm>> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "updateAlarm - alarmId=" + alarm.getId());
        if (alarm.getPinType() != 1) {
            alarm.setPinType(2);
        }
        this.mAlarmsLocalDataSource.upsertAlarm(alarm);
        return upsertAlarm(alarm, xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.util.List} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x008b  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00d4  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a  */
    public final /* synthetic */ Object upsertAlarm(Alarm alarm, xe6<? super ap4<Alarm>> xe6) {
        AlarmsRepository$upsertAlarm$Anon1 alarmsRepository$upsertAlarm$Anon1;
        int i;
        List list;
        ArrayList arrayList;
        AlarmsRepository alarmsRepository;
        ap4 ap4;
        Alarm alarm2 = alarm;
        xe6<? super ap4<Alarm>> xe62 = xe6;
        if (xe62 instanceof AlarmsRepository$upsertAlarm$Anon1) {
            alarmsRepository$upsertAlarm$Anon1 = (AlarmsRepository$upsertAlarm$Anon1) xe62;
            int i2 = alarmsRepository$upsertAlarm$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                alarmsRepository$upsertAlarm$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = alarmsRepository$upsertAlarm$Anon1.result;
                Object a = ff6.a();
                i = alarmsRepository$upsertAlarm$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.add(alarm2);
                    AlarmsRemoteDataSource alarmsRemoteDataSource = this.mAlarmRemoteDataSource;
                    alarmsRepository$upsertAlarm$Anon1.L$0 = this;
                    alarmsRepository$upsertAlarm$Anon1.L$1 = alarm2;
                    alarmsRepository$upsertAlarm$Anon1.L$2 = arrayList2;
                    alarmsRepository$upsertAlarm$Anon1.label = 1;
                    Object upsertAlarms = alarmsRemoteDataSource.upsertAlarms(arrayList2, alarmsRepository$upsertAlarm$Anon1);
                    if (upsertAlarms == a) {
                        return a;
                    }
                    alarmsRepository = this;
                    arrayList = arrayList2;
                    obj = upsertAlarms;
                } else if (i == 1) {
                    nc6.a(obj);
                    arrayList = (ArrayList) alarmsRepository$upsertAlarm$Anon1.L$2;
                    alarm2 = (Alarm) alarmsRepository$upsertAlarm$Anon1.L$1;
                    alarmsRepository = (AlarmsRepository) alarmsRepository$upsertAlarm$Anon1.L$0;
                } else if (i == 2) {
                    List list2 = (List) alarmsRepository$upsertAlarm$Anon1.L$5;
                    list = (List) alarmsRepository$upsertAlarm$Anon1.L$4;
                    ap4 ap42 = (ap4) alarmsRepository$upsertAlarm$Anon1.L$3;
                    ArrayList arrayList3 = (ArrayList) alarmsRepository$upsertAlarm$Anon1.L$2;
                    Alarm alarm3 = (Alarm) alarmsRepository$upsertAlarm$Anon1.L$1;
                    AlarmsRepository alarmsRepository2 = (AlarmsRepository) alarmsRepository$upsertAlarm$Anon1.L$0;
                    nc6.a(obj);
                    return new cp4(list.get(0), false, 2, (qg6) null);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    List a2 = ((cp4) ap4).a();
                    if (a2 == null) {
                        return new cp4(alarm2, false, 2, (qg6) null);
                    }
                    AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 = r4;
                    dl6 b = zl6.b();
                    AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1 alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon12 = new AlarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1(a2, (xe6) null, alarmsRepository, alarmsRepository$upsertAlarm$Anon1, a2);
                    alarmsRepository$upsertAlarm$Anon1.L$0 = alarmsRepository;
                    alarmsRepository$upsertAlarm$Anon1.L$1 = alarm2;
                    alarmsRepository$upsertAlarm$Anon1.L$2 = arrayList;
                    alarmsRepository$upsertAlarm$Anon1.L$3 = ap4;
                    list = a2;
                    alarmsRepository$upsertAlarm$Anon1.L$4 = list;
                    alarmsRepository$upsertAlarm$Anon1.L$5 = list;
                    alarmsRepository$upsertAlarm$Anon1.label = 2;
                    if (gk6.a(b, alarmsRepository$upsertAlarm$$inlined$let$lambda$Anon1, alarmsRepository$upsertAlarm$Anon1) == a) {
                        return a;
                    }
                    return new cp4(list.get(0), false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        alarmsRepository$upsertAlarm$Anon1 = new AlarmsRepository$upsertAlarm$Anon1(this, xe62);
        Object obj2 = alarmsRepository$upsertAlarm$Anon1.result;
        Object a3 = ff6.a();
        i = alarmsRepository$upsertAlarm$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
