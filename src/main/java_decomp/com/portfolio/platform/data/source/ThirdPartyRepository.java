package com.portfolio.platform.data.source;

import com.fossil.af6;
import com.fossil.ci6;
import com.fossil.ef6;
import com.fossil.f72;
import com.fossil.ff6;
import com.fossil.fitness.WorkoutType;
import com.fossil.g72;
import com.fossil.gv1;
import com.fossil.h72;
import com.fossil.h82;
import com.fossil.hh6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.k72;
import com.fossil.kk4;
import com.fossil.ll6;
import com.fossil.mc6;
import com.fossil.mk6;
import com.fossil.mn6;
import com.fossil.nf6;
import com.fossil.ot1;
import com.fossil.qc3;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.uh6;
import com.fossil.wg6;
import com.fossil.wv1;
import com.fossil.x62;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yv1;
import com.fossil.zl6;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitActiveTime;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOCalorie;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWOStep;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWorkoutSession;
import com.portfolio.platform.data.model.thirdparty.ua.UASample;
import com.portfolio.platform.data.model.ua.UAActivityTimeSeries;
import com.portfolio.platform.data.source.local.thirdparty.ThirdPartyDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import kotlinx.coroutines.CoroutineExceptionHandler;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThirdPartyRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "ThirdPartyRepository";
    @DexIgnore
    public ActivitiesRepository mActivitiesRepository;
    @DexIgnore
    public /* final */ CoroutineExceptionHandler mExceptionHandling; // = new ThirdPartyRepository$$special$$inlined$CoroutineExceptionHandler$Anon1(CoroutineExceptionHandler.m);
    @DexIgnore
    public kk4 mGoogleFitHelper;
    @DexIgnore
    public PortfolioApp mPortfolioApp;
    @DexIgnore
    public /* final */ il6 mPushDataSupervisorJob; // = jl6.a(mn6.a((rm6) null, 1, (Object) null).plus(zl6.b()).plus(this.mExceptionHandling));
    @DexIgnore
    public ThirdPartyDatabase mThirdPartyDatabase;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingThirdPartyDataCallback {
        @DexIgnore
        void onComplete();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[WorkoutType.values().length];

        /*
        static {
            $EnumSwitchMapping$0[WorkoutType.RUNNING.ordinal()] = 1;
            $EnumSwitchMapping$0[WorkoutType.CYCLING.ordinal()] = 2;
            $EnumSwitchMapping$0[WorkoutType.TREADMILL.ordinal()] = 3;
            $EnumSwitchMapping$0[WorkoutType.ELLIPTICAL.ordinal()] = 4;
            $EnumSwitchMapping$0[WorkoutType.WEIGHTS.ordinal()] = 5;
            $EnumSwitchMapping$0[WorkoutType.UNKNOWN.ordinal()] = 6;
        }
        */
    }

    @DexIgnore
    public ThirdPartyRepository(kk4 kk4, ThirdPartyDatabase thirdPartyDatabase, ActivitiesRepository activitiesRepository, PortfolioApp portfolioApp) {
        wg6.b(kk4, "mGoogleFitHelper");
        wg6.b(thirdPartyDatabase, "mThirdPartyDatabase");
        wg6.b(activitiesRepository, "mActivitiesRepository");
        wg6.b(portfolioApp, "mPortfolioApp");
        this.mGoogleFitHelper = kk4;
        this.mThirdPartyDatabase = thirdPartyDatabase;
        this.mActivitiesRepository = activitiesRepository;
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    private final List<GFitSleep> convertListMFSleepSessionToListGFitSleep(List<MFSleepSession> list) {
        ArrayList arrayList = new ArrayList();
        for (MFSleepSession next : list) {
            long j = (long) 1000;
            arrayList.add(new GFitSleep(next.getRealSleepMinutes(), ((long) next.getRealStartTime()) * j, j * ((long) next.getRealEndTime())));
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForActiveMinutes(GFitActiveTime gFitActiveTime, String str) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.j);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        List<T> j = yd6.j(gFitActiveTime.getActiveTimes());
        uh6 a2 = ci6.a((uh6) ci6.d(0, j.size()), 2);
        int a3 = a2.a();
        int b = a2.b();
        int c = a2.c();
        if (c < 0 ? a3 >= b : a3 <= b) {
            while (true) {
                DataPoint p = a.p();
                p.a(j.get(a3).longValue(), j.get(a3 + 1).longValue(), TimeUnit.MILLISECONDS);
                p.a(h72.d).e("unknown");
                a.a(p);
                if (a3 == b) {
                    break;
                }
                a3 += c;
            }
        }
        wg6.a((Object) a, "dataSetForActiveMinutes");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForCalories(List<GFitSample> list, String str) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component3 = next.component3();
            long component4 = next.component4();
            long component5 = next.component5();
            try {
                DataPoint p = a.p();
                p.a(component4, component5, TimeUnit.MILLISECONDS);
                p.a(h72.F).a(component3);
                a.a(p);
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.e(TAG, "createDataSetForCalories() fall into error=" + e.getMessage());
            }
        }
        wg6.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForDistances(List<GFitSample> list, String str) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.y);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            float component2 = next.component2();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint p = a.p();
            p.a(component4, component5, TimeUnit.MILLISECONDS);
            p.a(h72.u).a(component2);
            a.a(p);
        }
        wg6.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForHeartRates(List<GFitHeartRate> list, String str) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.v);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitHeartRate next : list) {
            float component1 = next.component1();
            long component2 = next.component2();
            long component3 = next.component3();
            DataPoint p = a.p();
            p.a(component2, component3, TimeUnit.MILLISECONDS);
            p.a(h72.p).a(component1);
            a.a(p);
        }
        wg6.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r4v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForSteps(List<GFitSample> list, String str) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.e);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitSample next : list) {
            int component1 = next.component1();
            long component4 = next.component4();
            long component5 = next.component5();
            DataPoint p = a.p();
            p.a(component4, component5, TimeUnit.MILLISECONDS);
            p.a(h72.g).c(component1);
            a.a(p);
        }
        wg6.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForWOCalories(List<GFitWOCalorie> list, String str, long j, long j2) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.p);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        String str2 = str;
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOCalorie next : list) {
                if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                    DataPoint p = a.p();
                    p.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                    p.a(h72.F).a(next.getCalorie());
                    a.a(p);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when add workout calories data point " + e);
        }
        wg6.a((Object) a, "dataSetForCalories");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForWODistances(List<GFitWODistance> list, String str, long j, long j2) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.y);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        String str2 = str;
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        for (GFitWODistance next : list) {
            if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                DataPoint p = a.p();
                p.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                p.a(h72.u).a(next.getDistance());
                a.a(p);
            }
        }
        wg6.a((Object) a, "dataSetForDistances");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForWOHeartRates(List<GFitWOHeartRate> list, String str, long j, long j2) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.v);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        String str2 = str;
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOHeartRate next : list) {
                if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                    DataPoint p = a.p();
                    p.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                    p.a(h72.p).a(next.getHeartRate());
                    a.a(p);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when add workout heartrate data point " + e);
        }
        wg6.a((Object) a, "dataSet");
        return a;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r5v0, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private final DataSet createDataSetForWOSteps(List<GFitWOStep> list, String str, long j, long j2) {
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.e);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        String str2 = str;
        aVar.a(new g72(this.mPortfolioApp.getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str, 3));
        DataSet a = DataSet.a(aVar.a());
        try {
            for (GFitWOStep next : list) {
                if (next.getStartTime() >= j && next.getEndTime() <= j2) {
                    DataPoint p = a.p();
                    p.a(next.getStartTime(), next.getEndTime(), TimeUnit.MILLISECONDS);
                    p.a(h72.g).c(next.getStep());
                    a.a(p);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(TAG, "exception when add workout steps data point " + e);
        }
        wg6.a((Object) a, "dataSetForSteps");
        return a;
    }

    @DexIgnore
    private final h82 createWorkoutSession(GFitWorkoutSession gFitWorkoutSession, String str) {
        DataSet createDataSetForWOSteps = createDataSetForWOSteps(gFitWorkoutSession.getSteps(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOCalories = createDataSetForWOCalories(gFitWorkoutSession.getCalories(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWODistances = createDataSetForWODistances(gFitWorkoutSession.getDistances(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        DataSet createDataSetForWOHeartRates = createDataSetForWOHeartRates(gFitWorkoutSession.getHeartRates(), str, gFitWorkoutSession.getStartTime(), gFitWorkoutSession.getEndTime());
        WorkoutType workoutType = getWorkoutType(gFitWorkoutSession.getWorkoutType());
        k72.a aVar = new k72.a();
        aVar.d(workoutType.name());
        aVar.a(getFitnessActivitiesType(workoutType));
        aVar.b(gFitWorkoutSession.getStartTime(), TimeUnit.MILLISECONDS);
        aVar.a(gFitWorkoutSession.getEndTime(), TimeUnit.MILLISECONDS);
        k72 a = aVar.a();
        h82.a aVar2 = new h82.a();
        aVar2.a(a);
        if (!createDataSetForWOSteps.isEmpty()) {
            aVar2.a(createDataSetForWOSteps);
        }
        if (!createDataSetForWOCalories.isEmpty()) {
            aVar2.a(createDataSetForWOCalories);
        }
        if (!createDataSetForWODistances.isEmpty()) {
            aVar2.a(createDataSetForWODistances);
        }
        if (!createDataSetForWOHeartRates.isEmpty()) {
            aVar2.a(createDataSetForWOHeartRates);
        }
        h82 a2 = aVar2.a();
        wg6.a((Object) a2, "sessionBuilder.build()");
        return a2;
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeries(List<UASample> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (UASample next : list) {
            int component1 = next.component1();
            double component2 = next.component2();
            double component3 = next.component3();
            long component4 = next.component4();
            builder.addSteps(component4, component1);
            builder.addDistance(component4, component2);
            builder.addCalories(component4, component3);
        }
        return builder.build();
    }

    @DexIgnore
    private final UAActivityTimeSeries generateTimeSeriesOldFlow(List<SampleRaw> list) {
        UAActivityTimeSeries.Builder builder = new UAActivityTimeSeries.Builder();
        for (SampleRaw next : list) {
            long time = next.getStartTime().getTime() / ((long) 1000);
            builder.addSteps(time, (int) next.getSteps());
            builder.addDistance(time, next.getDistance());
            builder.addCalories(time, next.getCalories());
        }
        return builder.build();
    }

    @DexIgnore
    private final String getFitnessActivitiesType(WorkoutType workoutType) {
        switch (WhenMappings.$EnumSwitchMapping$0[workoutType.ordinal()]) {
            case 1:
                return "running";
            case 2:
                return "biking";
            case 3:
                return "walking.treadmill";
            case 4:
                return "elliptical";
            case 5:
                return "weightlifting";
            case 6:
                return "unknown";
            default:
                return "other";
        }
    }

    @DexIgnore
    private final WorkoutType getWorkoutType(int i) {
        if (i < WorkoutType.values().length) {
            return WorkoutType.values()[i];
        }
        return WorkoutType.UNKNOWN;
    }

    @DexIgnore
    public static /* synthetic */ void saveData$default(ThirdPartyRepository thirdPartyRepository, List list, List list2, GFitActiveTime gFitActiveTime, List list3, List list4, List list5, int i, Object obj) {
        if ((i & 1) != 0) {
            list = qd6.a();
        }
        if ((i & 2) != 0) {
            list2 = qd6.a();
        }
        List list6 = list2;
        if ((i & 4) != 0) {
            gFitActiveTime = null;
        }
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        if ((i & 8) != 0) {
            list3 = qd6.a();
        }
        List list7 = list3;
        if ((i & 16) != 0) {
            list4 = qd6.a();
        }
        List list8 = list4;
        if ((i & 32) != 0) {
            list5 = qd6.a();
        }
        thirdPartyRepository.saveData(list, list6, gFitActiveTime2, list7, list8, list5);
    }

    @DexIgnore
    public static /* synthetic */ rm6 uploadData$default(ThirdPartyRepository thirdPartyRepository, PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback, int i, Object obj) {
        if ((i & 1) != 0) {
            pushPendingThirdPartyDataCallback = null;
        }
        return thirdPartyRepository.uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final DataSet createDataSetForSleepData(GFitSleep gFitSleep, g72 g72) {
        wg6.b(gFitSleep, "gFitSleep");
        wg6.b(g72, "device");
        f72.a aVar = new f72.a();
        aVar.a(this.mPortfolioApp);
        aVar.a(DataType.j);
        aVar.b(this.mPortfolioApp.getString(2131887020));
        aVar.a(0);
        aVar.a(g72);
        DataSet a = DataSet.a(aVar.a());
        long startTime = gFitSleep.getStartTime();
        long endTime = gFitSleep.getEndTime();
        DataPoint p = a.p();
        p.a(startTime, endTime, TimeUnit.MILLISECONDS);
        p.a(h72.d).c(gFitSleep.getSleepMins());
        a.a(p);
        wg6.a((Object) a, "dataSetForSleep");
        return a;
    }

    @DexIgnore
    public final ActivitiesRepository getMActivitiesRepository() {
        return this.mActivitiesRepository;
    }

    @DexIgnore
    public final PortfolioApp getMPortfolioApp() {
        return this.mPortfolioApp;
    }

    @DexIgnore
    public final ThirdPartyDatabase getMThirdPartyDatabase() {
        return this.mThirdPartyDatabase;
    }

    @DexIgnore
    public final void pushPendingData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        wg6.b(pushPendingThirdPartyDataCallback, "pushPendingThirdPartyDataCallback");
        uploadData(pushPendingThirdPartyDataCallback);
    }

    @DexIgnore
    public final void saveData(List<GFitSample> list, List<UASample> list2, GFitActiveTime gFitActiveTime, List<GFitHeartRate> list3, List<GFitWorkoutSession> list4, List<MFSleepSession> list5) {
        List<GFitWorkoutSession> list6 = list4;
        List<MFSleepSession> list7 = list5;
        wg6.b(list, "listGFitSample");
        wg6.b(list2, "listUASample");
        wg6.b(list3, "listGFitHeartRate");
        wg6.b(list6, "listGFitWorkoutSession");
        wg6.b(list7, "listMFSleepSession");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("saveData = listGFitSample=");
        sb.append(list);
        sb.append(", listUASample=");
        sb.append(list2);
        sb.append(", gFitActiveTime=");
        GFitActiveTime gFitActiveTime2 = gFitActiveTime;
        sb.append(gFitActiveTime);
        sb.append(", listGFitHeartRate=");
        sb.append(list3);
        sb.append(", listGFitWorkoutSession=");
        sb.append(list6);
        sb.append(", listMFSleepSession= ");
        sb.append(list7);
        local.d(TAG, sb.toString());
        this.mThirdPartyDatabase.runInTransaction(new ThirdPartyRepository$saveData$Anon1(this, list, gFitActiveTime2, list3, list6, list2, list7));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00e5  */
    public final /* synthetic */ Object saveGFitActiveTimeToGoogleFit(List<GFitActiveTime> list, String str, xe6<Object> xe6) {
        Object e;
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitActiveTimeToGoogleFit");
        wv1.a aVar = new wv1.a(getMPortfolioApp());
        aVar.a(x62.b, new Scope[0]);
        aVar.a(x62.a, new Scope[0]);
        aVar.a(x62.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write"});
        wv1 a = aVar.a();
        gv1 a2 = a.a();
        wg6.a((Object) a2, Constants.RESULT);
        if (a2.E()) {
            wg6.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitActiveTime to Google Fit");
                int size = list.size();
                hh6 hh6 = new hh6();
                hh6.element = 0;
                for (GFitActiveTime next : list) {
                    yv1 a3 = x62.c.a(a, createDataSetForActiveMinutes(next, str));
                    wv1 wv1 = a;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    int i = size;
                    ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(next, a, hh6, size, mk6, this, list, str);
                    a3.a(thirdPartyRepository$saveGFitActiveTimeToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    a = wv1;
                    size = i;
                }
                e = mk6.e();
                if (e == ff6.a()) {
                    nf6.c(xe6);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitActiveTime to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitActiveTimeToGoogleFit");
        if (mk6.isActive()) {
            mc6.a aVar2 = mc6.Companion;
            mk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
        e = mk6.e();
        if (e == ff6.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00ea  */
    public final /* synthetic */ Object saveGFitHeartRateToGoogleFit(List<GFitHeartRate> list, String str, xe6<Object> xe6) {
        Object e;
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitHeartRateToGoogleFit");
        wv1.a aVar = new wv1.a(getMPortfolioApp());
        aVar.a(x62.b, new Scope[0]);
        aVar.a(x62.a, new Scope[0]);
        aVar.a(x62.d, new Scope[0]);
        aVar.a(new Scope("https://www.googleapis.com/auth/fitness.body.write"));
        wv1 a = aVar.a();
        gv1 a2 = a.a();
        wg6.a((Object) a2, Constants.RESULT);
        if (a2.E()) {
            wg6.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitHeartRate to Google Fit");
                List<List<T>> b = yd6.b(list, 500);
                int size = b.size();
                hh6 hh6 = new hh6();
                hh6.element = 0;
                for (List next : b) {
                    String str2 = str;
                    yv1 a3 = x62.c.a(a, createDataSetForHeartRates(next, str2));
                    wv1 wv1 = a;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                    ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(next, a, hh6, size, mk6, this, list, str2);
                    a3.a(thirdPartyRepository$saveGFitHeartRateToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                    List<GFitHeartRate> list2 = list;
                    a = wv1;
                }
                e = mk6.e();
                if (e == ff6.a()) {
                    nf6.c(xe6);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitHeartRate to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitHeartRateToGoogleFit");
        if (mk6.isActive()) {
            mc6.a aVar2 = mc6.Companion;
            mk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
        e = mk6.e();
        if (e == ff6.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0156  */
    public final /* synthetic */ Object saveGFitSampleToGoogleFit(List<GFitSample> list, String str, xe6<Object> xe6) {
        Object e;
        wv1 wv1;
        List list2;
        hh6 hh6;
        hh6 hh62;
        ThirdPartyRepository thirdPartyRepository = this;
        String str2 = str;
        boolean z = true;
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSampleToGoogleFit");
        wv1.a aVar = new wv1.a(getMPortfolioApp());
        int i = 0;
        aVar.a(x62.b, new Scope[0]);
        aVar.a(x62.a, new Scope[0]);
        aVar.a(x62.d, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write"});
        wv1 a = aVar.a();
        gv1 a2 = a.a();
        wg6.a((Object) a2, Constants.RESULT);
        if (a2.E()) {
            wg6.a((Object) a, "googleApiClient");
            if (a.g()) {
                FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitSample to Google Fit");
                List<List<T>> b = yd6.b(list, 500);
                int size = b.size();
                hh6 hh63 = new hh6();
                hh63.element = 0;
                Iterator<T> it = b.iterator();
                while (it.hasNext()) {
                    List next = it.next();
                    ArrayList arrayList = new ArrayList();
                    DataSet access$createDataSetForSteps = thirdPartyRepository.createDataSetForSteps(next, str2);
                    DataSet access$createDataSetForDistances = thirdPartyRepository.createDataSetForDistances(next, str2);
                    DataSet access$createDataSetForCalories = thirdPartyRepository.createDataSetForCalories(next, str2);
                    arrayList.add(access$createDataSetForSteps);
                    arrayList.add(access$createDataSetForDistances);
                    arrayList.add(access$createDataSetForCalories);
                    int size2 = arrayList.size();
                    hh6 hh64 = new hh6();
                    hh64.element = i;
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        DataSet dataSet = (DataSet) it2.next();
                        wg6.a((Object) dataSet, "dataSet");
                        List B = dataSet.B();
                        wg6.a((Object) B, "dataSet.dataPoints");
                        if (B.isEmpty() ^ z) {
                            ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                            hh6 = hh64;
                            list2 = next;
                            hh62 = hh63;
                            wv1 = a;
                            ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(hh64, size2, next, a, hh63, mk6, size, this, list, str);
                            x62.c.a(a, dataSet).a(thirdPartyRepository$saveGFitSampleToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                        } else {
                            hh6 = hh64;
                            list2 = next;
                            hh62 = hh63;
                            wv1 = a;
                            hh62.element++;
                        }
                        List<GFitSample> list3 = list;
                        hh63 = hh62;
                        hh64 = hh6;
                        next = list2;
                        a = wv1;
                        z = true;
                        i = 0;
                    }
                    thirdPartyRepository = this;
                    List<GFitSample> list4 = list;
                }
                e = mk6.e();
                if (e == ff6.a()) {
                    nf6.c(xe6);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitSample to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSampleToGoogleFit");
        if (mk6.isActive()) {
            mc6.a aVar2 = mc6.Companion;
            mk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
        e = mk6.e();
        if (e == ff6.a()) {
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r3v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r2v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final Object saveGFitSleepDataToGoogleFit(List<GFitSleep> list, String str, xe6<Object> xe6) {
        String str2 = str;
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitSleepDataToGoogleFit");
        GoogleSignInAccount a = ot1.a(getMPortfolioApp());
        if (a != null) {
            int size = list.size();
            hh6 hh6 = new hh6();
            hh6.element = 0;
            int i = 2131887020;
            g72 g72 = new g72(getMPortfolioApp().getString(2131887020), DeviceIdentityUtils.getNameBySerial(str), str2, 3);
            ArrayList arrayList = new ArrayList();
            for (GFitSleep next : list) {
                DataSet createDataSetForSleepData = createDataSetForSleepData(next, g72);
                k72.a aVar = new k72.a();
                aVar.c(str2 + new DateTime());
                aVar.d(getMPortfolioApp().getString(i));
                aVar.b("User Sleep");
                aVar.b(next.getStartTime(), TimeUnit.MILLISECONDS);
                aVar.a(next.getEndTime(), TimeUnit.MILLISECONDS);
                aVar.a("sleep");
                k72 a2 = aVar.a();
                h82.a aVar2 = new h82.a();
                aVar2.a(a2);
                aVar2.a(createDataSetForSleepData);
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r0;
                int i2 = size;
                int i3 = size;
                qc3 a3 = x62.a(getMPortfolioApp(), a).a(aVar2.a());
                g72 g722 = g72;
                ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(next, g72, a, hh6, arrayList, i2, mk6, this, list, str);
                a3.a(thirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                a3.a(new ThirdPartyRepository$saveGFitSleepDataToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(g722, a, hh6, arrayList, i3, mk6, this, list, str));
                str2 = str;
                size = i3;
                g72 = g722;
                i = 2131887020;
            }
        } else {
            FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitSleepDataToGoogleFit");
            if (mk6.isActive()) {
                mc6.a aVar3 = mc6.Companion;
                mk6.resumeWith(mc6.m1constructorimpl((Object) null));
            }
        }
        Object e = mk6.e();
        if (e == ff6.a()) {
            nf6.c(xe6);
        }
        return e;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0154  */
    public final /* synthetic */ Object saveGFitWorkoutSessionToGoogleFit(List<GFitWorkoutSession> list, String str, xe6<Object> xe6) {
        Object e;
        hh6 hh6;
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        FLogger.INSTANCE.getLocal().d(TAG, "Start saveGFitWorkoutSessionToGoogleFit");
        wv1.a aVar = new wv1.a(getMPortfolioApp());
        aVar.a(x62.b, new Scope[0]);
        aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.location.write", "https://www.googleapis.com/auth/fitness.body.write"});
        wv1 a = aVar.a();
        gv1 a2 = a.a();
        wg6.a((Object) a2, Constants.RESULT);
        if (a2.E()) {
            wg6.a((Object) a, "googleApiClient");
            if (a.g()) {
                GoogleSignInAccount a3 = ot1.a(getMPortfolioApp());
                if (a3 != null) {
                    FLogger.INSTANCE.getLocal().d(TAG, "Sending GFitWorkoutSession to Google Fit");
                    int size = list.size();
                    hh6 hh62 = new hh6();
                    hh62.element = 0;
                    for (GFitWorkoutSession next : list) {
                        try {
                            qc3 a4 = x62.a(getMPortfolioApp(), a3).a(createWorkoutSession(next, str));
                            ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 = r1;
                            hh6 = hh62;
                            try {
                                ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1 thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon12 = new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1(next, a3, hh62, size, mk6, this, list, str);
                                a4.a(thirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon1);
                                a4.a(new ThirdPartyRepository$saveGFitWorkoutSessionToGoogleFit$$inlined$suspendCancellableCoroutine$lambda$Anon2(a3, hh6, size, mk6, this, list, str));
                            } catch (Exception e2) {
                                e = e2;
                            }
                        } catch (Exception e3) {
                            e = e3;
                            hh6 = hh62;
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            local.d(TAG, "exception when create workout session " + e);
                            FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
                            if (mk6.isActive()) {
                                mc6.a aVar2 = mc6.Companion;
                                mk6.resumeWith(mc6.m1constructorimpl((Object) null));
                            }
                            hh62 = hh6;
                        }
                        hh62 = hh6;
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(TAG, "GoogleSignInAccount is null");
                    FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
                    if (mk6.isActive()) {
                        mc6.a aVar3 = mc6.Companion;
                        mk6.resumeWith(mc6.m1constructorimpl((Object) null));
                    }
                }
                e = mk6.e();
                if (e == ff6.a()) {
                    nf6.c(xe6);
                }
                return e;
            }
        }
        FLogger.INSTANCE.getLocal().d(TAG, "Not sending GFitWorkoutSession to Google Fit");
        FLogger.INSTANCE.getLocal().d(TAG, "End saveGFitWorkoutSessionToGoogleFit");
        if (mk6.isActive()) {
            mc6.a aVar4 = mc6.Companion;
            mk6.resumeWith(mc6.m1constructorimpl((Object) null));
        }
        e = mk6.e();
        if (e == ff6.a()) {
        }
        return e;
    }

    @DexIgnore
    public final void setMActivitiesRepository(ActivitiesRepository activitiesRepository) {
        wg6.b(activitiesRepository, "<set-?>");
        this.mActivitiesRepository = activitiesRepository;
    }

    @DexIgnore
    public final void setMPortfolioApp(PortfolioApp portfolioApp) {
        wg6.b(portfolioApp, "<set-?>");
        this.mPortfolioApp = portfolioApp;
    }

    @DexIgnore
    public final void setMThirdPartyDatabase(ThirdPartyDatabase thirdPartyDatabase) {
        wg6.b(thirdPartyDatabase, "<set-?>");
        this.mThirdPartyDatabase = thirdPartyDatabase;
    }

    @DexIgnore
    public final rm6 uploadData(PushPendingThirdPartyDataCallback pushPendingThirdPartyDataCallback) {
        return ik6.b(this.mPushDataSupervisorJob, (af6) null, (ll6) null, new ThirdPartyRepository$uploadData$Anon1(this, pushPendingThirdPartyDataCallback, (xe6) null), 3, (Object) null);
    }
}
