package com.portfolio.platform.data.source;

import com.google.gson.reflect.TypeToken;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository$delete$type$Anon1 extends TypeToken<UpsertApiResponse<ServerError>> {
}
