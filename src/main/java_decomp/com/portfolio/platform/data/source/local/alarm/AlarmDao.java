package com.portfolio.platform.data.source.local.alarm;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface AlarmDao {
    @DexIgnore
    void cleanUp();

    @DexIgnore
    List<Alarm> getActiveAlarms();

    @DexIgnore
    Alarm getAlarmWithUri(String str);

    @DexIgnore
    List<Alarm> getAlarms();

    @DexIgnore
    List<Alarm> getAlarmsIgnoreDeleted();

    @DexIgnore
    Alarm getInComingActiveAlarm(int i);

    @DexIgnore
    List<Alarm> getInComingActiveAlarms();

    @DexIgnore
    List<Alarm> getPendingAlarms();

    @DexIgnore
    long insertAlarm(Alarm alarm);

    @DexIgnore
    Long[] insertAlarms(List<Alarm> list);

    @DexIgnore
    int removeAlarm(String str);
}
