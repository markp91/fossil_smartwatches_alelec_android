package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.du3;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.rd6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yx5;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDateTime;
import com.portfolio.platform.helper.GsonConvertDateTimeToLong;
import com.portfolio.platform.helper.GsonConverterShortDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository$getHeartRateSummaries$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateSummaryRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<DailyHeartRateSummary>, ApiResponse<ku3>> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryRepository$getHeartRateSummaries$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(HeartRateSummaryRepository$getHeartRateSummaries$Anon1 heartRateSummaryRepository$getHeartRateSummaries$Anon1, lc6 lc6) {
            this.this$0 = heartRateSummaryRepository$getHeartRateSummaries$Anon1;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ApiResponse<ku3>>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiService$p = this.this$0.this$0.mApiService;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiService$p.getDailyHeartRateSummaries(e, e2, 0, 100, xe6);
        }

        @DexIgnore
        public LiveData<List<DailyHeartRateSummary>> loadFromDb() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "getHeartRateSummaries isNotToday startDate = " + this.this$0.$startDate + ", endDate = " + this.this$0.$endDate);
            HeartRateDailySummaryDao access$getMHeartRateSummaryDao$p = this.this$0.this$0.mHeartRateSummaryDao;
            HeartRateSummaryRepository$getHeartRateSummaries$Anon1 heartRateSummaryRepository$getHeartRateSummaries$Anon1 = this.this$0;
            return access$getMHeartRateSummaryDao$p.getDailyHeartRateSummariesLiveData(heartRateSummaryRepository$getHeartRateSummaries$Anon1.$startDate, heartRateSummaryRepository$getHeartRateSummaries$Anon1.$endDate);
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease(), "getHeartRateSummaries onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(ApiResponse<ku3> apiResponse) {
            Date date;
            Date date2;
            wg6.b(apiResponse, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
            local.d(tAG$app_fossilRelease, "saveCallResult onResponse: response = " + apiResponse);
            try {
                if (!apiResponse.get_items().isEmpty()) {
                    du3 du3 = new du3();
                    du3.a(Long.TYPE, new GsonConvertDateTimeToLong());
                    du3.a(DateTime.class, new GsonConvertDateTime());
                    du3.a(Date.class, new GsonConverterShortDate());
                    Gson a = du3.a();
                    List<ku3> list = apiResponse.get_items();
                    ArrayList arrayList = new ArrayList(rd6.a(list, 10));
                    for (ku3 a2 : list) {
                        arrayList.add((DailyHeartRateSummary) a.a(a2, new HeartRateSummaryRepository$getHeartRateSummaries$Anon1$Anon1_Level2$saveCallResult$summaries$Anon1_Level3$Anon1_Level4().getType()));
                    }
                    List d = yd6.d(arrayList);
                    FitnessDataDao access$getMFitnessDataDao$p = this.this$0.this$0.mFitnessDataDao;
                    lc6 lc6 = this.$downloadingDate;
                    if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                        date = this.this$0.$startDate;
                    }
                    lc6 lc62 = this.$downloadingDate;
                    if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                        date2 = this.this$0.$endDate;
                    }
                    List<FitnessDataWrapper> fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String tAG$app_fossilRelease2 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                    local2.d(tAG$app_fossilRelease2, "heartrate summary " + d + " fitnessDataSize " + fitnessData.size());
                    if (fitnessData.isEmpty()) {
                        this.this$0.this$0.mHeartRateSummaryDao.insertListDailyHeartRateSummary(d);
                    }
                }
            } catch (Exception e) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String tAG$app_fossilRelease3 = HeartRateSummaryRepository.Companion.getTAG$app_fossilRelease();
                StringBuilder sb = new StringBuilder();
                sb.append("saveCallResult exception=");
                e.printStackTrace();
                sb.append(cd6.a);
                local3.e(tAG$app_fossilRelease3, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<DailyHeartRateSummary> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public HeartRateSummaryRepository$getHeartRateSummaries$Anon1(HeartRateSummaryRepository heartRateSummaryRepository, Date date, Date date2, boolean z) {
        this.this$0 = heartRateSummaryRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.HeartRateSummaryRepository$getHeartRateSummaries$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<DailyHeartRateSummary>>> apply(List<FitnessDataWrapper> list) {
        wg6.a((Object) list, "fitnessDataList");
        return new Anon1_Level2(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
