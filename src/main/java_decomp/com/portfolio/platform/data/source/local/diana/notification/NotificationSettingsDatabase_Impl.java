package com.portfolio.platform.data.source.local.diana.notification;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDatabase_Impl extends NotificationSettingsDatabase {
    @DexIgnore
    public volatile NotificationSettingsDao _notificationSettingsDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `notificationSettings` (`settingsName` TEXT NOT NULL, `settingsType` INTEGER NOT NULL, `isCall` INTEGER NOT NULL, PRIMARY KEY(`settingsName`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '563d2dcf0170d812960699bb88bdb35d')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `notificationSettings`");
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = NotificationSettingsDatabase_Impl.this.mDatabase = iiVar;
            NotificationSettingsDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (NotificationSettingsDatabase_Impl.this.mCallbacks != null) {
                int size = NotificationSettingsDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) NotificationSettingsDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("settingsName", new fi.a("settingsName", "TEXT", true, 1, (String) null, 1));
            hashMap.put("settingsType", new fi.a("settingsType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("isCall", new fi.a("isCall", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("notificationSettings", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "notificationSettings");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "notificationSettings(com.portfolio.platform.data.model.NotificationSettingsModel).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        NotificationSettingsDatabase_Impl.super.assertNotMainThread();
        ii a = NotificationSettingsDatabase_Impl.super.getOpenHelper().a();
        try {
            NotificationSettingsDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `notificationSettings`");
            NotificationSettingsDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            NotificationSettingsDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"notificationSettings"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "563d2dcf0170d812960699bb88bdb35d", "d29e13e7c48fbcf2227b41f4f512e273");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public NotificationSettingsDao getNotificationSettingsDao() {
        NotificationSettingsDao notificationSettingsDao;
        if (this._notificationSettingsDao != null) {
            return this._notificationSettingsDao;
        }
        synchronized (this) {
            if (this._notificationSettingsDao == null) {
                this._notificationSettingsDao = new NotificationSettingsDao_Impl(this);
            }
            notificationSettingsDao = this._notificationSettingsDao;
        }
        return notificationSettingsDao;
    }
}
