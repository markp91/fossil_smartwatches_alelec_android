package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaPresetRepository_Factory implements Factory<DianaPresetRepository> {
    @DexIgnore
    public /* final */ Provider<DianaPresetDao> mDianaPresetDaoProvider;
    @DexIgnore
    public /* final */ Provider<DianaPresetRemoteDataSource> mDianaPresetRemoteDataSourceProvider;

    @DexIgnore
    public DianaPresetRepository_Factory(Provider<DianaPresetDao> provider, Provider<DianaPresetRemoteDataSource> provider2) {
        this.mDianaPresetDaoProvider = provider;
        this.mDianaPresetRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static DianaPresetRepository_Factory create(Provider<DianaPresetDao> provider, Provider<DianaPresetRemoteDataSource> provider2) {
        return new DianaPresetRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static DianaPresetRepository newDianaPresetRepository(DianaPresetDao dianaPresetDao, DianaPresetRemoteDataSource dianaPresetRemoteDataSource) {
        return new DianaPresetRepository(dianaPresetDao, dianaPresetRemoteDataSource);
    }

    @DexIgnore
    public static DianaPresetRepository provideInstance(Provider<DianaPresetDao> provider, Provider<DianaPresetRemoteDataSource> provider2) {
        return new DianaPresetRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public DianaPresetRepository get() {
        return provideInstance(this.mDianaPresetDaoProvider, this.mDianaPresetRemoteDataSourceProvider);
    }
}
