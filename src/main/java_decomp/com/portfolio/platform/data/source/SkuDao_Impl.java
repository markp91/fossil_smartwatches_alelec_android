package com.portfolio.platform.data.source;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.WatchParam;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SkuDao_Impl implements SkuDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<SKUModel> __insertionAdapterOfSKUModel;
    @DexIgnore
    public /* final */ hh<WatchParam> __insertionAdapterOfWatchParam;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUpSku;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUpWatchParam;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<SKUModel> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `SKU` (`createdAt`,`updatedAt`,`serialNumberPrefix`,`sku`,`deviceName`,`groupName`,`gender`,`deviceType`,`fastPairId`) VALUES (?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, SKUModel sKUModel) {
            if (sKUModel.getCreatedAt() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, sKUModel.getCreatedAt());
            }
            if (sKUModel.getUpdatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, sKUModel.getUpdatedAt());
            }
            if (sKUModel.getSerialNumberPrefix() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, sKUModel.getSerialNumberPrefix());
            }
            if (sKUModel.getSku() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, sKUModel.getSku());
            }
            if (sKUModel.getDeviceName() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, sKUModel.getDeviceName());
            }
            if (sKUModel.getGroupName() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, sKUModel.getGroupName());
            }
            if (sKUModel.getGender() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, sKUModel.getGender());
            }
            if (sKUModel.getDeviceType() == null) {
                miVar.a(8);
            } else {
                miVar.a(8, sKUModel.getDeviceType());
            }
            if (sKUModel.getFastPairId() == null) {
                miVar.a(9);
            } else {
                miVar.a(9, sKUModel.getFastPairId());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<WatchParam> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchParam` (`prefixSerial`,`versionMajor`,`versionMinor`,`data`) VALUES (?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, WatchParam watchParam) {
            if (watchParam.getPrefixSerial() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, watchParam.getPrefixSerial());
            }
            if (watchParam.getVersionMajor() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, watchParam.getVersionMajor());
            }
            if (watchParam.getVersionMinor() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, watchParam.getVersionMinor());
            }
            if (watchParam.getData() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, watchParam.getData());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM SKU";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchParam";
        }
    }

    @DexIgnore
    public SkuDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfSKUModel = new Anon1(ohVar);
        this.__insertionAdapterOfWatchParam = new Anon2(ohVar);
        this.__preparedStmtOfCleanUpSku = new Anon3(ohVar);
        this.__preparedStmtOfCleanUpWatchParam = new Anon4(ohVar);
    }

    @DexIgnore
    public void addOrUpdateSkuList(List<SKUModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfSKUModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void addOrUpdateWatchParam(WatchParam watchParam) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchParam.insert(watchParam);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void cleanUpSku() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUpSku.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpSku.release(acquire);
        }
    }

    @DexIgnore
    public void cleanUpWatchParam() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUpWatchParam.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUpWatchParam.release(acquire);
        }
    }

    @DexIgnore
    public List<SKUModel> getAllSkus() {
        rh b = rh.b("SELECT * FROM SKU", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "serialNumberPrefix");
            int b5 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = ai.b(a, "deviceName");
            int b7 = ai.b(a, "groupName");
            int b8 = ai.b(a, "gender");
            int b9 = ai.b(a, "deviceType");
            int b10 = ai.b(a, "fastPairId");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                SKUModel sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
                arrayList.add(sKUModel);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public SKUModel getSkuByDeviceIdPrefix(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM SKU WHERE serialNumberPrefix=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        SKUModel sKUModel = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "createdAt");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, "serialNumberPrefix");
            int b5 = ai.b(a, LegacyDeviceModel.COLUMN_DEVICE_MODEL);
            int b6 = ai.b(a, "deviceName");
            int b7 = ai.b(a, "groupName");
            int b8 = ai.b(a, "gender");
            int b9 = ai.b(a, "deviceType");
            int b10 = ai.b(a, "fastPairId");
            if (a.moveToFirst()) {
                sKUModel = new SKUModel(a.getString(b4), a.getString(b5), a.getString(b6), a.getString(b7), a.getString(b8), a.getString(b9), a.getString(b10));
                sKUModel.setCreatedAt(a.getString(b2));
                sKUModel.setUpdatedAt(a.getString(b3));
            }
            return sKUModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public WatchParam getWatchParamById(String str) {
        rh b = rh.b("SELECT * FROM watchParam WHERE prefixSerial =?", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchParam watchParam = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "prefixSerial");
            int b3 = ai.b(a, "versionMajor");
            int b4 = ai.b(a, "versionMinor");
            int b5 = ai.b(a, "data");
            if (a.moveToFirst()) {
                watchParam = new WatchParam(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5));
            }
            return watchParam;
        } finally {
            a.close();
            b.c();
        }
    }
}
