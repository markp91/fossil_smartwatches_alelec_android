package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ju3;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.google.gson.JsonElement;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.WatchParameterResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = DeviceRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "DeviceRemoteDataSource::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public DeviceRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiService");
        this.mApiService = apiServiceV2;
    }

    @DexIgnore
    public final Object forceLinkDevice(Device device, xe6<? super ap4<Void>> xe6) {
        return ResponseKt.a(new DeviceRemoteDataSource$forceLinkDevice$Anon2(this, device, (xe6) null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0089  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c2  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object generatePairingKey(String str, xe6<? super ap4<String>> xe6) {
        DeviceRemoteDataSource$generatePairingKey$Anon1 deviceRemoteDataSource$generatePairingKey$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DeviceRemoteDataSource$generatePairingKey$Anon1) {
            deviceRemoteDataSource$generatePairingKey$Anon1 = (DeviceRemoteDataSource$generatePairingKey$Anon1) xe6;
            int i2 = deviceRemoteDataSource$generatePairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$generatePairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$generatePairingKey$Anon1.result;
                Object a = ff6.a();
                i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    ku3.a("serialNumber", str);
                    DeviceRemoteDataSource$generatePairingKey$response$Anon1 deviceRemoteDataSource$generatePairingKey$response$Anon1 = new DeviceRemoteDataSource$generatePairingKey$response$Anon1(this, ku3, (xe6) null);
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$0 = this;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$1 = str;
                    deviceRemoteDataSource$generatePairingKey$Anon1.L$2 = ku3;
                    deviceRemoteDataSource$generatePairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$generatePairingKey$response$Anon1, deviceRemoteDataSource$generatePairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) deviceRemoteDataSource$generatePairingKey$Anon1.L$2;
                    str = (String) deviceRemoteDataSource$generatePairingKey$Anon1.L$1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$generatePairingKey$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "generatePairingKey of " + str + " response " + ap4);
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!((ku3) a2).d("randomKey")) {
                        return new zo4(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((ku3) cp4.a()).a("randomKey");
                        wg6.a((Object) a3, "response.response.get(Co\u2026ants.JSON_KEY_RANDOM_KEY)");
                        return new cp4(a3.f(), false, 2, (qg6) null);
                    }
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        deviceRemoteDataSource$generatePairingKey$Anon1 = new DeviceRemoteDataSource$generatePairingKey$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$generatePairingKey$Anon1.result;
        Object a4 = ff6.a();
        i = deviceRemoteDataSource$generatePairingKey$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "generatePairingKey of " + str + " response " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final Object getAllDevice(xe6<? super ap4<ApiResponse<Device>>> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "getAllDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$getAllDevice$Anon2(this, (xe6) null), xe6);
    }

    @DexIgnore
    public final Object getDeviceBySerial(String str, xe6<? super ap4<Device>> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "getDeviceBySerial");
        return ResponseKt.a(new DeviceRemoteDataSource$getDeviceBySerial$Anon2(this, str, (xe6) null), xe6);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getDeviceSecretKey(String str, xe6<? super ap4<String>> xe6) {
        DeviceRemoteDataSource$getDeviceSecretKey$Anon1 deviceRemoteDataSource$getDeviceSecretKey$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DeviceRemoteDataSource$getDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$getDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$getDeviceSecretKey$Anon1) xe6;
            int i2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
                Object a = ff6.a();
                i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1 deviceRemoteDataSource$getDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$response$Anon1(this, str, (xe6) null);
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$0 = this;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$1 = str;
                    deviceRemoteDataSource$getDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getDeviceSecretKey$response$Anon1, deviceRemoteDataSource$getDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getDeviceSecretKey$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getDeviceSecretKey of " + str + " response " + ap4);
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!((ku3) a2).d("secretKey")) {
                        return new zo4(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((ku3) cp4.a()).a("secretKey");
                        if (a3 instanceof ju3) {
                            return new cp4("", false, 2, (qg6) null);
                        }
                        wg6.a((Object) a3, "secretKey");
                        return new cp4(a3.f(), false, 2, (qg6) null);
                    }
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        deviceRemoteDataSource$getDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$getDeviceSecretKey$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$getDeviceSecretKey$Anon1.result;
        Object a4 = ff6.a();
        i = deviceRemoteDataSource$getDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local2.d(str22, "getDeviceSecretKey of " + str + " response " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getLatestWatchParamFromServer(String str, int i, xe6<? super WatchParameterResponse> xe6) {
        DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1;
        int i2;
        ap4 ap4;
        if (xe6 instanceof DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) {
            deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = (DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1) xe6;
            int i3 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
                Object a = ff6.a();
                i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1(this, str, i, (xe6) null);
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$0 = this;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$1 = str;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$0 = i;
                    deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getLatestWatchParamFromServer$response$Anon1, deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    i = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.I$0;
                    str = (String) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local.d(str2, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
                if (!(ap4 instanceof cp4)) {
                    ApiResponse apiResponse = (ApiResponse) ((cp4) ap4).a();
                    List list = apiResponse != null ? apiResponse.get_items() : null;
                    if (list == null || !(!list.isEmpty())) {
                        return null;
                    }
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local2.d(str3, "getLatestWatchParamFromServer success, response = " + ((WatchParameterResponse) list.get(0)));
                    return (WatchParameterResponse) list.get(0);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.d(str4, "getLatestWatchParamFromServer failed, errorCode = " + ((zo4) ap4).a());
                    return null;
                } else {
                    throw new kc6();
                }
            }
        }
        deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1 = new DeviceRemoteDataSource$getLatestWatchParamFromServer$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.result;
        Object a2 = ff6.a();
        i2 = deviceRemoteDataSource$getLatestWatchParamFromServer$Anon1.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str22 = TAG;
        local4.d(str22, "getLatestWatchParamFromServer, serial =" + str + ", majorVersion= " + i);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0038  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0074  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object getSupportedSku(int i, xe6<? super ap4<ApiResponse<SKUModel>>> xe6) {
        DeviceRemoteDataSource$getSupportedSku$Anon1 deviceRemoteDataSource$getSupportedSku$Anon1;
        int i2;
        ap4 ap4;
        if (xe6 instanceof DeviceRemoteDataSource$getSupportedSku$Anon1) {
            deviceRemoteDataSource$getSupportedSku$Anon1 = (DeviceRemoteDataSource$getSupportedSku$Anon1) xe6;
            int i3 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$getSupportedSku$Anon1.label = i3 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$getSupportedSku$Anon1.result;
                Object a = ff6.a();
                i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchSupportedSkus");
                    DeviceRemoteDataSource$getSupportedSku$response$Anon1 deviceRemoteDataSource$getSupportedSku$response$Anon1 = new DeviceRemoteDataSource$getSupportedSku$response$Anon1(this, i, (xe6) null);
                    deviceRemoteDataSource$getSupportedSku$Anon1.L$0 = this;
                    deviceRemoteDataSource$getSupportedSku$Anon1.I$0 = i;
                    deviceRemoteDataSource$getSupportedSku$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$getSupportedSku$response$Anon1, deviceRemoteDataSource$getSupportedSku$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i2 == 1) {
                    int i4 = deviceRemoteDataSource$getSupportedSku$Anon1.I$0;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$getSupportedSku$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    Object a2 = ((cp4) ap4).a();
                    if (a2 != null) {
                        return new cp4(a2, false, 2, (qg6) null);
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), (Throwable) null, (String) null);
                } else {
                    throw new kc6();
                }
            }
        }
        deviceRemoteDataSource$getSupportedSku$Anon1 = new DeviceRemoteDataSource$getSupportedSku$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$getSupportedSku$Anon1.result;
        Object a3 = ff6.a();
        i2 = deviceRemoteDataSource$getSupportedSku$Anon1.label;
        if (i2 != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final Object removeDevice(Device device, xe6<? super ap4<Void>> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "removeDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$removeDevice$Anon2(this, device, (xe6) null), xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v22, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v6, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x009d  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x00dd  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object swapPairingKey(String str, String str2, xe6<? super ap4<String>> xe6) {
        DeviceRemoteDataSource$swapPairingKey$Anon1 deviceRemoteDataSource$swapPairingKey$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof DeviceRemoteDataSource$swapPairingKey$Anon1) {
            deviceRemoteDataSource$swapPairingKey$Anon1 = (DeviceRemoteDataSource$swapPairingKey$Anon1) xe6;
            int i2 = deviceRemoteDataSource$swapPairingKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$swapPairingKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$swapPairingKey$Anon1.result;
                Object a = ff6.a();
                i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    ku3.a("serialNumber", str2);
                    ku3.a("encryptedData", str);
                    DeviceRemoteDataSource$swapPairingKey$response$Anon1 deviceRemoteDataSource$swapPairingKey$response$Anon1 = new DeviceRemoteDataSource$swapPairingKey$response$Anon1(this, ku3, (xe6) null);
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$0 = this;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$1 = str;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$2 = str2;
                    deviceRemoteDataSource$swapPairingKey$Anon1.L$3 = ku3;
                    deviceRemoteDataSource$swapPairingKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$swapPairingKey$response$Anon1, deviceRemoteDataSource$swapPairingKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) deviceRemoteDataSource$swapPairingKey$Anon1.L$3;
                    str2 = deviceRemoteDataSource$swapPairingKey$Anon1.L$2;
                    str = (String) deviceRemoteDataSource$swapPairingKey$Anon1.L$1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$swapPairingKey$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "swapPairingKey " + str + " of " + str2 + " response " + ap4);
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 == null) {
                        wg6.a();
                        throw null;
                    } else if (!((ku3) a2).d("encryptedData")) {
                        return new zo4(600, (ServerError) null, (Throwable) null, (String) null);
                    } else {
                        JsonElement a3 = ((ku3) cp4.a()).a("encryptedData");
                        if (a3 instanceof ju3) {
                            return new zo4(600, (ServerError) null, (Throwable) null, (String) null);
                        }
                        wg6.a((Object) a3, "encryptedData");
                        return new cp4(a3.f(), false, 2, (qg6) null);
                    }
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        deviceRemoteDataSource$swapPairingKey$Anon1 = new DeviceRemoteDataSource$swapPairingKey$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$swapPairingKey$Anon1.result;
        Object a4 = ff6.a();
        i = deviceRemoteDataSource$swapPairingKey$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "swapPairingKey " + str + " of " + str2 + " response " + ap4);
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final Object updateDevice(Device device, xe6<? super ap4<Void>> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "updateDevice");
        return ResponseKt.a(new DeviceRemoteDataSource$updateDevice$Anon2(this, device, (xe6) null), xe6);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object updateDeviceSecretKey(String str, String str2, xe6<? super ap4<ku3>> xe6) {
        DeviceRemoteDataSource$updateDeviceSecretKey$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$Anon1;
        int i;
        if (xe6 instanceof DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) {
            deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = (DeviceRemoteDataSource$updateDeviceSecretKey$Anon1) xe6;
            int i2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
                Object a = ff6.a();
                i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku3 = new ku3();
                    ku3.a("secretKey", str2);
                    DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$response$Anon1(this, str, ku3, (xe6) null);
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$0 = this;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$1 = str;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$2 = str2;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$3 = ku3;
                    deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label = 1;
                    obj = ResponseKt.a(deviceRemoteDataSource$updateDeviceSecretKey$response$Anon1, deviceRemoteDataSource$updateDeviceSecretKey$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    ku3 ku32 = (ku3) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$3;
                    str2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$2;
                    str = (String) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$1;
                    DeviceRemoteDataSource deviceRemoteDataSource = (DeviceRemoteDataSource) deviceRemoteDataSource$updateDeviceSecretKey$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 ap4 = (ap4) obj;
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local.d(str3, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ap4);
                return ap4;
            }
        }
        deviceRemoteDataSource$updateDeviceSecretKey$Anon1 = new DeviceRemoteDataSource$updateDeviceSecretKey$Anon1(this, xe6);
        Object obj2 = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.result;
        Object a2 = ff6.a();
        i = deviceRemoteDataSource$updateDeviceSecretKey$Anon1.label;
        if (i != 0) {
        }
        ap4 ap42 = (ap4) obj2;
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str32 = TAG;
        local2.d(str32, "updateDeviceSecretKey " + str2 + " of " + str + " response " + ap42);
        return ap42;
    }
}
