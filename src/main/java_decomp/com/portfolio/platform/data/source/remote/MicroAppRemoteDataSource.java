package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.DeclarationFile;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppRemoteDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG; // = "MicroAppRemoteDataSource";
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public MicroAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003c  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0071  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object getAllMicroApp(String str, xe6<? super ap4<List<MicroApp>>> xe6) {
        MicroAppRemoteDataSource$getAllMicroApp$Anon1 microAppRemoteDataSource$getAllMicroApp$Anon1;
        int i;
        ap4 ap4;
        if (xe6 instanceof MicroAppRemoteDataSource$getAllMicroApp$Anon1) {
            microAppRemoteDataSource$getAllMicroApp$Anon1 = (MicroAppRemoteDataSource$getAllMicroApp$Anon1) xe6;
            int i2 = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRemoteDataSource$getAllMicroApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = microAppRemoteDataSource$getAllMicroApp$Anon1.result;
                Object a = ff6.a();
                i = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "getAllMicroApp " + str);
                    MicroAppRemoteDataSource$getAllMicroApp$response$Anon1 microAppRemoteDataSource$getAllMicroApp$response$Anon1 = new MicroAppRemoteDataSource$getAllMicroApp$response$Anon1(this, str, (xe6) null);
                    microAppRemoteDataSource$getAllMicroApp$Anon1.L$0 = this;
                    microAppRemoteDataSource$getAllMicroApp$Anon1.L$1 = str;
                    microAppRemoteDataSource$getAllMicroApp$Anon1.label = 1;
                    obj = ResponseKt.a(microAppRemoteDataSource$getAllMicroApp$response$Anon1, microAppRemoteDataSource$getAllMicroApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    str = (String) microAppRemoteDataSource$getAllMicroApp$Anon1.L$1;
                    MicroAppRemoteDataSource microAppRemoteDataSource = (MicroAppRemoteDataSource) microAppRemoteDataSource$getAllMicroApp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        for (MicroApp microApp : ((ApiResponse) a2).get_items()) {
                            microApp.setSerialNumber(str);
                            arrayList.add(microApp);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "getAllMicroApp success isFromCache " + cp4.b());
                        return new cp4(arrayList, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getAllMicroApp fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" serverError ");
                    sb.append(zo4.c());
                    local3.d(TAG, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        microAppRemoteDataSource$getAllMicroApp$Anon1 = new MicroAppRemoteDataSource$getAllMicroApp$Anon1(this, xe6);
        Object obj2 = microAppRemoteDataSource$getAllMicroApp$Anon1.result;
        Object a3 = ff6.a();
        i = microAppRemoteDataSource$getAllMicroApp$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x004e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x00a2  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    public final Object getAllMicroAppVariant(String str, String str2, String str3, xe6<? super ap4<List<MicroAppVariant>>> xe6) {
        MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
        int i;
        String str4;
        ap4 ap4;
        String str5 = str;
        String str6 = str2;
        String str7 = str3;
        xe6<? super ap4<List<MicroAppVariant>>> xe62 = xe6;
        if (xe62 instanceof MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1) {
            microAppRemoteDataSource$getAllMicroAppVariant$Anon1 = (MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1) xe62;
            int i2 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                microAppRemoteDataSource$getAllMicroAppVariant$Anon1.label = i2 - Integer.MIN_VALUE;
                MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon12 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
                Object obj = microAppRemoteDataSource$getAllMicroAppVariant$Anon12.result;
                Object a = ff6.a();
                i = microAppRemoteDataSource$getAllMicroAppVariant$Anon12.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    local.d(TAG, "getAllMicroAppVariant " + str5 + " major " + str6 + " minor " + str7);
                    MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 = new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this, str, str2, str3, (xe6) null);
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$0 = this;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$1 = str5;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$2 = str6;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$3 = str7;
                    microAppRemoteDataSource$getAllMicroAppVariant$Anon12.label = 1;
                    obj = ResponseKt.a(microAppRemoteDataSource$getAllMicroAppVariant$response$Anon1, microAppRemoteDataSource$getAllMicroAppVariant$Anon12);
                    if (obj == a) {
                        return a;
                    }
                    str4 = str5;
                } else if (i == 1) {
                    String str8 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$3;
                    String str9 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$2;
                    str4 = (String) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$1;
                    MicroAppRemoteDataSource microAppRemoteDataSource = (MicroAppRemoteDataSource) microAppRemoteDataSource$getAllMicroAppVariant$Anon12.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    cp4 cp4 = (cp4) ap4;
                    Object a2 = cp4.a();
                    if (a2 != null) {
                        for (MicroAppVariant microAppVariant : ((ApiResponse) a2).get_items()) {
                            microAppVariant.setSerialNumber(str4);
                            for (DeclarationFile declarationFile : microAppVariant.getDeclarationFileList()) {
                                declarationFile.setAppId(microAppVariant.getAppId());
                                declarationFile.setSerialNumber(str4);
                                declarationFile.setVariantName(microAppVariant.getName());
                            }
                            arrayList.add(microAppVariant);
                        }
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d(TAG, "getAllMicroApp success isFromCache " + cp4.b());
                        return new cp4(arrayList, cp4.b());
                    }
                    wg6.a();
                    throw null;
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    StringBuilder sb = new StringBuilder();
                    sb.append("getAllMicroApp fail code ");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" serverError ");
                    sb.append(zo4.c());
                    local3.d(TAG, sb.toString());
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        microAppRemoteDataSource$getAllMicroAppVariant$Anon1 = new MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1(this, xe62);
        MicroAppRemoteDataSource$getAllMicroAppVariant$Anon1 microAppRemoteDataSource$getAllMicroAppVariant$Anon122 = microAppRemoteDataSource$getAllMicroAppVariant$Anon1;
        Object obj2 = microAppRemoteDataSource$getAllMicroAppVariant$Anon122.result;
        Object a3 = ff6.a();
        i = microAppRemoteDataSource$getAllMicroAppVariant$Anon122.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
