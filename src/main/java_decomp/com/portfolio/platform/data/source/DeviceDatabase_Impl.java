package com.portfolio.platform.data.source;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.portfolio.platform.data.legacy.onedotfive.LegacyDeviceModel;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceDatabase_Impl extends DeviceDatabase {
    @DexIgnore
    public volatile DeviceDao _deviceDao;
    @DexIgnore
    public volatile SkuDao _skuDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `device` (`major` INTEGER NOT NULL, `minor` INTEGER NOT NULL, `createdAt` TEXT, `updatedAt` TEXT, `owner` TEXT, `productDisplayName` TEXT, `manufacturer` TEXT, `softwareRevision` TEXT, `hardwareRevision` TEXT, `deviceId` TEXT NOT NULL, `macAddress` TEXT, `sku` TEXT, `firmwareRevision` TEXT, `batteryLevel` INTEGER NOT NULL, `vibrationStrength` INTEGER, `isActive` INTEGER NOT NULL, PRIMARY KEY(`deviceId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `SKU` (`createdAt` TEXT, `updatedAt` TEXT, `serialNumberPrefix` TEXT NOT NULL, `sku` TEXT, `deviceName` TEXT, `groupName` TEXT, `gender` TEXT, `deviceType` TEXT, `fastPairId` TEXT, PRIMARY KEY(`serialNumberPrefix`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `watchParam` (`prefixSerial` TEXT NOT NULL, `versionMajor` TEXT, `versionMinor` TEXT, `data` TEXT, PRIMARY KEY(`prefixSerial`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '5396f1b60549f2c34ac181bb35df2a0f')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `device`");
            iiVar.b("DROP TABLE IF EXISTS `SKU`");
            iiVar.b("DROP TABLE IF EXISTS `watchParam`");
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = DeviceDatabase_Impl.this.mDatabase = iiVar;
            DeviceDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (DeviceDatabase_Impl.this.mCallbacks != null) {
                int size = DeviceDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DeviceDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(16);
            hashMap.put("major", new fi.a("major", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("minor", new fi.a("minor", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "TEXT", false, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "TEXT", false, 0, (String) null, 1));
            hashMap.put("owner", new fi.a("owner", "TEXT", false, 0, (String) null, 1));
            hashMap.put("productDisplayName", new fi.a("productDisplayName", "TEXT", false, 0, (String) null, 1));
            hashMap.put("manufacturer", new fi.a("manufacturer", "TEXT", false, 0, (String) null, 1));
            hashMap.put("softwareRevision", new fi.a("softwareRevision", "TEXT", false, 0, (String) null, 1));
            hashMap.put("hardwareRevision", new fi.a("hardwareRevision", "TEXT", false, 0, (String) null, 1));
            hashMap.put("deviceId", new fi.a("deviceId", "TEXT", true, 1, (String) null, 1));
            hashMap.put("macAddress", new fi.a("macAddress", "TEXT", false, 0, (String) null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new fi.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, (String) null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, new fi.a(LegacyDeviceModel.COLUMN_FIRMWARE_VERSION, "TEXT", false, 0, (String) null, 1));
            hashMap.put(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, new fi.a(LegacyDeviceModel.COLUMN_BATTERY_LEVEL, "INTEGER", true, 0, (String) null, 1));
            hashMap.put("vibrationStrength", new fi.a("vibrationStrength", "INTEGER", false, 0, (String) null, 1));
            hashMap.put("isActive", new fi.a("isActive", "INTEGER", true, 0, (String) null, 1));
            fi fiVar = new fi("device", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "device");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "device(com.portfolio.platform.data.model.Device).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("createdAt", new fi.a("createdAt", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("serialNumberPrefix", new fi.a("serialNumberPrefix", "TEXT", true, 1, (String) null, 1));
            hashMap2.put(LegacyDeviceModel.COLUMN_DEVICE_MODEL, new fi.a(LegacyDeviceModel.COLUMN_DEVICE_MODEL, "TEXT", false, 0, (String) null, 1));
            hashMap2.put("deviceName", new fi.a("deviceName", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("groupName", new fi.a("groupName", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("gender", new fi.a("gender", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("deviceType", new fi.a("deviceType", "TEXT", false, 0, (String) null, 1));
            hashMap2.put("fastPairId", new fi.a("fastPairId", "TEXT", false, 0, (String) null, 1));
            fi fiVar2 = new fi("SKU", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "SKU");
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "SKU(com.portfolio.platform.data.model.SKUModel).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(4);
            hashMap3.put("prefixSerial", new fi.a("prefixSerial", "TEXT", true, 1, (String) null, 1));
            hashMap3.put("versionMajor", new fi.a("versionMajor", "TEXT", false, 0, (String) null, 1));
            hashMap3.put("versionMinor", new fi.a("versionMinor", "TEXT", false, 0, (String) null, 1));
            hashMap3.put("data", new fi.a("data", "TEXT", false, 0, (String) null, 1));
            fi fiVar3 = new fi("watchParam", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "watchParam");
            if (fiVar3.equals(a3)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "watchParam(com.portfolio.platform.data.model.WatchParam).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        DeviceDatabase_Impl.super.assertNotMainThread();
        ii a = DeviceDatabase_Impl.super.getOpenHelper().a();
        try {
            DeviceDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `device`");
            a.b("DELETE FROM `SKU`");
            a.b("DELETE FROM `watchParam`");
            DeviceDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            DeviceDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"device", "SKU", "watchParam"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(3), "5396f1b60549f2c34ac181bb35df2a0f", "8e8d5273d687d9dce08dcd027801930e");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public DeviceDao deviceDao() {
        DeviceDao deviceDao;
        if (this._deviceDao != null) {
            return this._deviceDao;
        }
        synchronized (this) {
            if (this._deviceDao == null) {
                this._deviceDao = new DeviceDao_Impl(this);
            }
            deviceDao = this._deviceDao;
        }
        return deviceDao;
    }

    @DexIgnore
    public SkuDao skuDao() {
        SkuDao skuDao;
        if (this._skuDao != null) {
            return this._skuDao;
        }
        synchronized (this) {
            if (this._skuDao == null) {
                this._skuDao = new SkuDao_Impl(this);
            }
            skuDao = this._skuDao;
        }
        return skuDao;
    }
}
