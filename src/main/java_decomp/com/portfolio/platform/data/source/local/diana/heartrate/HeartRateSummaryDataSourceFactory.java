package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.MutableLiveData;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryDataSourceFactory extends xe.b<Date, DailyHeartRateSummary> {
    @DexIgnore
    public /* final */ u04 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao heartRateSummaryDao;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public HeartRateSummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<HeartRateSummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository summariesRepository;

    @DexIgnore
    public HeartRateSummaryDataSourceFactory(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository2, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase2, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(heartRateSummaryRepository, "summariesRepository");
        wg6.b(fitnessDataRepository2, "fitnessDataRepository");
        wg6.b(heartRateDailySummaryDao, "heartRateSummaryDao");
        wg6.b(fitnessDatabase2, "fitnessDatabase");
        wg6.b(date, "createdDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "mStartCalendar");
        this.summariesRepository = heartRateSummaryRepository;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.heartRateSummaryDao = heartRateDailySummaryDao;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = u04;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public xe<Date, DailyHeartRateSummary> create() {
        this.localDataSource = new HeartRateSummaryLocalDataSource(this.summariesRepository, this.fitnessDataRepository, this.heartRateSummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource = this.localDataSource;
        if (heartRateSummaryLocalDataSource != null) {
            return heartRateSummaryLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final HeartRateSummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<HeartRateSummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource) {
        this.localDataSource = heartRateSummaryLocalDataSource;
    }
}
