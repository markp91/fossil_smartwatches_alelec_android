package com.portfolio.platform.data.source.remote;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRemoteDataSource_Factory implements Factory<FileRemoteDataSource> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mServiceProvider;

    @DexIgnore
    public FileRemoteDataSource_Factory(Provider<ApiServiceV2> provider) {
        this.mServiceProvider = provider;
    }

    @DexIgnore
    public static FileRemoteDataSource_Factory create(Provider<ApiServiceV2> provider) {
        return new FileRemoteDataSource_Factory(provider);
    }

    @DexIgnore
    public static FileRemoteDataSource newFileRemoteDataSource(ApiServiceV2 apiServiceV2) {
        return new FileRemoteDataSource(apiServiceV2);
    }

    @DexIgnore
    public static FileRemoteDataSource provideInstance(Provider<ApiServiceV2> provider) {
        return new FileRemoteDataSource(provider.get());
    }

    @DexIgnore
    public FileRemoteDataSource get() {
        return provideInstance(this.mServiceProvider);
    }
}
