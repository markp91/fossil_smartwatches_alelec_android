package com.portfolio.platform.data.source.local.hybrid.goaltracking;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.ye;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.source.GoalTrackingRepository;
import java.util.Date;
import java.util.List;
import java.util.Set;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingDataLocalDataSource extends ye<Long, GoalTrackingData> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public DateTime latestTrackedTime;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public /* final */ Date mCurrentDate;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ GoalTrackingRepository mGoalTrackingRepository;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public int mOffset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ GoalTrackingDataLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = goalTrackingDataLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingDataLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = GoalTrackingDataLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "GoalTrackingDataLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingDataLocalDataSource(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(goalTrackingRepository, "mGoalTrackingRepository");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(date, "mCurrentDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        this.mGoalTrackingRepository = goalTrackingRepository;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mCurrentDate = date;
        this.listener = aVar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "goalTrackingRaw", new String[0]);
        this.mGoalTrackingDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final List<GoalTrackingData> getDataInDatabase(int i) {
        return this.mGoalTrackingDao.getGoalTrackingDataListInitInDate(this.mCurrentDate, i);
    }

    @DexIgnore
    private final rm6 loadData(vk4.d dVar, vk4.b.a aVar, int i) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new GoalTrackingDataLocalDataSource$loadData$Anon1(this, i, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ rm6 loadData$default(GoalTrackingDataLocalDataSource goalTrackingDataLocalDataSource, vk4.d dVar, vk4.b.a aVar, int i, int i2, Object obj) {
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return goalTrackingDataLocalDataSource.loadData(dVar, aVar, i);
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mGoalTrackingDatabase.getInvalidationTracker().b();
        return GoalTrackingDataLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(ye.f<Long> fVar, ye.a<GoalTrackingData> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - currentDate = " + this.mCurrentDate);
        GoalTrackingDao goalTrackingDao = this.mGoalTrackingDao;
        Date date = this.mCurrentDate;
        DateTime dateTime = this.latestTrackedTime;
        if (dateTime != null) {
            Object obj = fVar.a;
            wg6.a(obj, "params.key");
            List<GoalTrackingData> goalTrackingDataListAfterInDate = goalTrackingDao.getGoalTrackingDataListAfterInDate(date, dateTime, ((Number) obj).longValue(), fVar.b);
            if (!goalTrackingDataListAfterInDate.isEmpty()) {
                this.latestTrackedTime = ((GoalTrackingData) yd6.f(goalTrackingDataListAfterInDate)).getTrackedAt();
            }
            aVar.a(goalTrackingDataListAfterInDate);
            this.mHelper.a(vk4.d.AFTER, (vk4.b) new GoalTrackingDataLocalDataSource$loadAfter$Anon1(this));
            return;
        }
        wg6.d("latestTrackedTime");
        throw null;
    }

    @DexIgnore
    public void loadBefore(ye.f<Long> fVar, ye.a<GoalTrackingData> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(ye.e<Long> eVar, ye.c<GoalTrackingData> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - mCurrentDate = " + this.mCurrentDate + ' ');
        List<GoalTrackingData> dataInDatabase = getDataInDatabase(eVar.a);
        if (!dataInDatabase.isEmpty()) {
            this.latestTrackedTime = ((GoalTrackingData) yd6.f(dataInDatabase)).getTrackedAt();
        }
        cVar.a(dataInDatabase);
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new GoalTrackingDataLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mGoalTrackingDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public Long getKey(GoalTrackingData goalTrackingData) {
        wg6.b(goalTrackingData, "item");
        return Long.valueOf(goalTrackingData.getUpdatedAt());
    }
}
