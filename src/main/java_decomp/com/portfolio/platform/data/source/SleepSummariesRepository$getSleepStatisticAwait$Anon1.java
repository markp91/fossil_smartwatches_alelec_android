package com.portfolio.platform.data.source;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.SleepSummariesRepository", f = "SleepSummariesRepository.kt", l = {394}, m = "getSleepStatisticAwait")
public final class SleepSummariesRepository$getSleepStatisticAwait$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummariesRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummariesRepository$getSleepStatisticAwait$Anon1(SleepSummariesRepository sleepSummariesRepository, xe6 xe6) {
        super(xe6);
        this.this$0 = sleepSummariesRepository;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.getSleepStatisticAwait(this);
    }
}
