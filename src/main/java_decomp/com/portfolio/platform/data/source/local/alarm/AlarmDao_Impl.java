package com.portfolio.platform.data.source.local.alarm;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.v34;
import com.fossil.vh;
import com.misfit.frameworks.buttonservice.model.Alarm;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDao_Impl implements AlarmDao {
    @DexIgnore
    public /* final */ v34 __alarmConverter; // = new v34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<Alarm> __insertionAdapterOfAlarm;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveAlarm;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Alarm> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `alarm` (`id`,`uri`,`title`,`message`,`hour`,`minute`,`days`,`isActive`,`isRepeated`,`createdAt`,`updatedAt`,`pinType`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Alarm alarm) {
            if (alarm.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, alarm.getId());
            }
            if (alarm.getUri() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, alarm.getUri());
            }
            if (alarm.getTitle() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, alarm.getTitle());
            }
            if (alarm.getMessage() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, alarm.getMessage());
            }
            miVar.a(5, (long) alarm.getHour());
            miVar.a(6, (long) alarm.getMinute());
            String a = AlarmDao_Impl.this.__alarmConverter.a(alarm.getDays());
            if (a == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a);
            }
            miVar.a(8, alarm.isActive() ? 1 : 0);
            miVar.a(9, alarm.isRepeated() ? 1 : 0);
            if (alarm.getCreatedAt() == null) {
                miVar.a(10);
            } else {
                miVar.a(10, alarm.getCreatedAt());
            }
            if (alarm.getUpdatedAt() == null) {
                miVar.a(11);
            } else {
                miVar.a(11, alarm.getUpdatedAt());
            }
            miVar.a(12, (long) alarm.getPinType());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM alarm WHERE uri =?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM alarm";
        }
    }

    @DexIgnore
    public AlarmDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfAlarm = new Anon1(ohVar);
        this.__preparedStmtOfRemoveAlarm = new Anon2(ohVar);
        this.__preparedStmtOfCleanUp = new Anon3(ohVar);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public List<Alarm> getActiveAlarms() {
        rh rhVar;
        rh b = rh.b("SELECT *FROM alarm WHERE isActive = 1 and pinType <> 3", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            rhVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b2;
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = i;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public Alarm getAlarmWithUri(String str) {
        Alarm alarm;
        String str2 = str;
        rh b = rh.b("SELECT * FROM alarm WHERE uri =?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Alarm> getAlarms() {
        rh rhVar;
        rh b = rh.b("SELECT * FROM alarm", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            rhVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b2;
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = i;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<Alarm> getAlarmsIgnoreDeleted() {
        rh rhVar;
        rh b = rh.b("SELECT * FROM alarm WHERE pinType <> 3 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            rhVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b2;
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = i;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public Alarm getInComingActiveAlarm(int i) {
        Alarm alarm;
        rh b = rh.b("SELECT * FROM alarm WHERE isActive = 1 and minute + hour * 60 > ? ORDER BY hour, minute ASC LIMIT 1", 1);
        b.a(1, (long) i);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            if (a.moveToFirst()) {
                alarm = new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13));
            } else {
                alarm = null;
            }
            return alarm;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<Alarm> getInComingActiveAlarms() {
        rh rhVar;
        rh b = rh.b("SELECT * FROM alarm WHERE isActive = 1 and pinType <> 3 ORDER BY hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            rhVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b2;
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = i;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<Alarm> getPendingAlarms() {
        rh rhVar;
        rh b = rh.b("SELECT*FROM alarm where pinType <> 0 ORDER BY isActive DESC, hour, minute ASC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "uri");
            int b4 = ai.b(a, Explore.COLUMN_TITLE);
            int b5 = ai.b(a, "message");
            int b6 = ai.b(a, "hour");
            int b7 = ai.b(a, "minute");
            int b8 = ai.b(a, Alarm.COLUMN_DAYS);
            int b9 = ai.b(a, "isActive");
            int b10 = ai.b(a, "isRepeated");
            int b11 = ai.b(a, "createdAt");
            int b12 = ai.b(a, "updatedAt");
            int b13 = ai.b(a, "pinType");
            rhVar = b;
            try {
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b2;
                    arrayList.add(new Alarm(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getInt(b6), a.getInt(b7), this.__alarmConverter.a(a.getString(b8)), a.getInt(b9) != 0, a.getInt(b10) != 0, a.getString(b11), a.getString(b12), a.getInt(b13)));
                    b2 = i;
                }
                a.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b;
            a.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public long insertAlarm(Alarm alarm) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            long insertAndReturnId = this.__insertionAdapterOfAlarm.insertAndReturnId(alarm);
            this.__db.setTransactionSuccessful();
            return insertAndReturnId;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public Long[] insertAlarms(List<Alarm> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            Long[] insertAndReturnIdsArrayBox = this.__insertionAdapterOfAlarm.insertAndReturnIdsArrayBox(list);
            this.__db.setTransactionSuccessful();
            return insertAndReturnIdsArrayBox;
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public int removeAlarm(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveAlarm.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            int s = acquire.s();
            this.__db.setTransactionSuccessful();
            return s;
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAlarm.release(acquire);
        }
    }
}
