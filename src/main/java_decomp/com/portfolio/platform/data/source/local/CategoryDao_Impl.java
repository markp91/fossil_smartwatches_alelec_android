package com.portfolio.platform.data.source.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.Category;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CategoryDao_Impl implements CategoryDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<Category> __insertionAdapterOfCategory;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearData;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<Category> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `category` (`id`,`englishName`,`name`,`updatedAt`,`createdAt`,`priority`) VALUES (?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, Category category) {
            if (category.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, category.getId());
            }
            if (category.getEnglishName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, category.getEnglishName());
            }
            if (category.getName() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, category.getName());
            }
            if (category.getUpdatedAt() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, category.getUpdatedAt());
            }
            if (category.getCreatedAt() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, category.getCreatedAt());
            }
            miVar.a(6, (long) category.getPriority());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM category";
        }
    }

    @DexIgnore
    public CategoryDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfCategory = new Anon1(ohVar);
        this.__preparedStmtOfClearData = new Anon2(ohVar);
    }

    @DexIgnore
    public void clearData() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearData.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearData.release(acquire);
        }
    }

    @DexIgnore
    public List<Category> getAllCategory() {
        rh b = rh.b("SELECT * FROM category ORDER BY priority DESC", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "englishName");
            int b4 = ai.b(a, "name");
            int b5 = ai.b(a, "updatedAt");
            int b6 = ai.b(a, "createdAt");
            int b7 = ai.b(a, "priority");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new Category(a.getString(b2), a.getString(b3), a.getString(b4), a.getString(b5), a.getString(b6), a.getInt(b7)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public Category getCategoryById(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM category WHERE id=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            return a.moveToFirst() ? new Category(a.getString(ai.b(a, "id")), a.getString(ai.b(a, "englishName")), a.getString(ai.b(a, "name")), a.getString(ai.b(a, "updatedAt")), a.getString(ai.b(a, "createdAt")), a.getInt(ai.b(a, "priority"))) : null;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertCategoryList(List<Category> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfCategory.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
