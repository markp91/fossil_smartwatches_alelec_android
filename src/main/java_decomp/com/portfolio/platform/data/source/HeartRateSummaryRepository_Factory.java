package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryRepository_Factory implements Factory<HeartRateSummaryRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<HeartRateDailySummaryDao> mHeartRateSummaryDaoProvider;

    @DexIgnore
    public HeartRateSummaryRepository_Factory(Provider<HeartRateDailySummaryDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        this.mHeartRateSummaryDaoProvider = provider;
        this.mFitnessDataDaoProvider = provider2;
        this.mApiServiceProvider = provider3;
    }

    @DexIgnore
    public static HeartRateSummaryRepository_Factory create(Provider<HeartRateDailySummaryDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new HeartRateSummaryRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static HeartRateSummaryRepository newHeartRateSummaryRepository(HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        return new HeartRateSummaryRepository(heartRateDailySummaryDao, fitnessDataDao, apiServiceV2);
    }

    @DexIgnore
    public static HeartRateSummaryRepository provideInstance(Provider<HeartRateDailySummaryDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new HeartRateSummaryRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public HeartRateSummaryRepository get() {
        return provideInstance(this.mHeartRateSummaryDaoProvider, this.mFitnessDataDaoProvider, this.mApiServiceProvider);
    }
}
