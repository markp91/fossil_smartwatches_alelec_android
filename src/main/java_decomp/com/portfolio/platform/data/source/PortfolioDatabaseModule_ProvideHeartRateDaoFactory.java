package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateSampleDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideHeartRateDaoFactory implements Factory<HeartRateSampleDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideHeartRateDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideHeartRateDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideHeartRateDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static HeartRateSampleDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideHeartRateDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static HeartRateSampleDao proxyProvideHeartRateDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        HeartRateSampleDao provideHeartRateDao = portfolioDatabaseModule.provideHeartRateDao(fitnessDatabase);
        z76.a(provideHeartRateDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideHeartRateDao;
    }

    @DexIgnore
    public HeartRateSampleDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
