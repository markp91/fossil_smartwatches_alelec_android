package com.portfolio.platform.data.source.local.diana.heartrate;

import androidx.lifecycle.LiveData;
import com.fossil.af;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.lc6;
import com.fossil.lh;
import com.fossil.ll6;
import com.fossil.qd6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.wk4;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.model.diana.heartrate.DailyHeartRateSummary;
import com.portfolio.platform.data.model.diana.heartrate.Resting;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSummaryLocalDataSource extends af<Date, DailyHeartRateSummary> {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ Calendar key;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public /* final */ Date mCreatedDate;
    @DexIgnore
    public Date mEndDate; // = new Date();
    @DexIgnore
    public /* final */ FitnessDataRepository mFitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase mFitnessDatabase;
    @DexIgnore
    public /* final */ HeartRateDailySummaryDao mHeartRateSummaryDao;
    @DexIgnore
    public vk4 mHelper;
    @DexIgnore
    public LiveData<NetworkState> mNetworkState; // = wk4.a(this.mHelper);
    @DexIgnore
    public /* final */ lh.c mObserver;
    @DexIgnore
    public List<lc6<Date, Date>> mRequestAfterQueue; // = new ArrayList();
    @DexIgnore
    public Date mStartDate; // = new Date();
    @DexIgnore
    public /* final */ HeartRateSummaryRepository mSummariesRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends lh.c {
        @DexIgnore
        public /* final */ /* synthetic */ HeartRateSummaryLocalDataSource this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1(HeartRateSummaryLocalDataSource heartRateSummaryLocalDataSource, String str, String[] strArr) {
            super(str, strArr);
            this.this$0 = heartRateSummaryLocalDataSource;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            wg6.b(set, "tables");
            this.this$0.invalidate();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final Date calculateNextKey(Date date, Date date2) {
            wg6.b(date, HardwareLog.COLUMN_DATE);
            wg6.b(date2, "createdDate");
            FLogger.INSTANCE.getLocal().d(getTAG$app_fossilRelease(), "calculateNextKey");
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "nextPagedKey");
            instance.setTime(date);
            instance.add(3, -7);
            Calendar c = bk4.c(instance);
            if (bk4.b(date2, c.getTime())) {
                c.setTime(date2);
            }
            Date time = c.getTime();
            wg6.a((Object) time, "nextPagedKey.time");
            return time;
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return HeartRateSummaryLocalDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = HeartRateSummaryLocalDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "HeartRateSummaryLocalDat\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public HeartRateSummaryLocalDataSource(HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(heartRateSummaryRepository, "mSummariesRepository");
        wg6.b(fitnessDataRepository, "mFitnessDataRepository");
        wg6.b(heartRateDailySummaryDao, "mHeartRateSummaryDao");
        wg6.b(fitnessDatabase, "mFitnessDatabase");
        wg6.b(date, "mCreatedDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "key");
        this.mSummariesRepository = heartRateSummaryRepository;
        this.mFitnessDataRepository = fitnessDataRepository;
        this.mHeartRateSummaryDao = heartRateDailySummaryDao;
        this.mFitnessDatabase = fitnessDatabase;
        this.mCreatedDate = date;
        this.listener = aVar;
        this.key = calendar;
        this.mHelper = new vk4(u04.a());
        this.mHelper.a(this.listener);
        this.mObserver = new Anon1(this, "daily_heart_rate_summary", new String[0]);
        this.mFitnessDatabase.getInvalidationTracker().b(this.mObserver);
    }

    @DexIgnore
    private final void calculateStartDate(Date date) {
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "calendar");
        instance.setTime(this.mEndDate);
        instance.add(3, -14);
        instance.set(10, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        Calendar c = bk4.c(instance);
        c.add(5, 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "calculateStartDate endDate=" + this.mEndDate + ", startDate=" + c.getTime());
        Date time = c.getTime();
        wg6.a((Object) time, "calendar.time");
        this.mStartDate = time;
        if (bk4.b(date, this.mStartDate)) {
            this.mStartDate = date;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0028, code lost:
        r1 = r1.getResting();
     */
    @DexIgnore
    private final List<DailyHeartRateSummary> calculateSummaries(List<DailyHeartRateSummary> list) {
        int value;
        Resting resting;
        List<DailyHeartRateSummary> list2 = list;
        int i = 1;
        if (!list.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) yd6.f(list);
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "endCalendar");
            instance.setTime(dailyHeartRateSummary.getDate());
            int value2 = (instance.get(7) == 1 || resting == null) ? 0 : resting.getValue();
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "calendar");
            instance2.setTime(((DailyHeartRateSummary) yd6.d(list)).getDate());
            Calendar q = bk4.q(instance2.getTime());
            wg6.a((Object) q, "DateHelper.getStartOfWeek(calendar.time)");
            q.add(5, -1);
            Calendar instance3 = Calendar.getInstance();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            for (T next : list) {
                int i6 = i5 + 1;
                if (i5 >= 0) {
                    DailyHeartRateSummary dailyHeartRateSummary2 = (DailyHeartRateSummary) next;
                    wg6.a((Object) instance3, "mSummaryCalendar");
                    instance3.setTime(dailyHeartRateSummary2.getDate());
                    if (instance3.get(5) == q.get(5)) {
                        DailyHeartRateSummary dailyHeartRateSummary3 = list2.get(i2);
                        if (i4 <= 0) {
                            i4 = 1;
                        }
                        dailyHeartRateSummary3.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i4));
                        q.add(5, -7);
                        i2 = i5;
                        i3 = 0;
                        i4 = 0;
                    }
                    Resting resting2 = dailyHeartRateSummary2.getResting();
                    if (resting2 != null && (value = resting2.getValue()) > 0) {
                        i3 += value;
                        i4++;
                    }
                    if (i5 == list.size() - 1 && value2 > 0) {
                        i3 += value2;
                        i4++;
                    }
                    i5 = i6;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary4 = list2.get(i2);
            if (i4 > 0) {
                i = i4;
            }
            dailyHeartRateSummary4.setAvgRestingHeartRateOfWeek(Integer.valueOf(i3 / i));
        }
        return list2;
    }

    @DexIgnore
    private final DailyHeartRateSummary dummySummary(Date date) {
        DailyHeartRateSummary dailyHeartRateSummary = new DailyHeartRateSummary(0.0f, date, System.currentTimeMillis(), System.currentTimeMillis(), 0, 0, 0, (Resting) null);
        dailyHeartRateSummary.setCreatedAt(System.currentTimeMillis());
        dailyHeartRateSummary.setUpdatedAt(System.currentTimeMillis());
        return dailyHeartRateSummary;
    }

    @DexIgnore
    private final List<DailyHeartRateSummary> getDataInDatabase(Date date, Date date2) {
        Object obj;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getDataInDatabase - startDate=" + date + ", endDate=" + date2);
        List<DailyHeartRateSummary> calculateSummaries = calculateSummaries(this.mHeartRateSummaryDao.getDailyHeartRateSummariesDesc(bk4.b(date, date2) ? date2 : date, date2));
        ArrayList arrayList = new ArrayList();
        Date lastDate = this.mHeartRateSummaryDao.getLastDate();
        if (lastDate == null) {
            lastDate = date;
        }
        ArrayList arrayList2 = new ArrayList();
        arrayList2.addAll(calculateSummaries);
        if (!bk4.b(date, lastDate)) {
            date = lastDate;
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "getDataInDatabase - summaries.size=" + calculateSummaries.size() + ", " + "lastDate=" + lastDate + ", startDateToFill=" + date);
        while (bk4.c(date2, date)) {
            Iterator it = arrayList2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                if (bk4.d(((DailyHeartRateSummary) obj).getDate(), date2)) {
                    break;
                }
            }
            DailyHeartRateSummary dailyHeartRateSummary = (DailyHeartRateSummary) obj;
            if (dailyHeartRateSummary == null) {
                arrayList.add(dummySummary(date2));
            } else {
                arrayList.add(dailyHeartRateSummary);
                arrayList2.remove(dailyHeartRateSummary);
            }
            date2 = bk4.n(date2);
            wg6.a((Object) date2, "DateHelper.getPrevDate(endDateToFill)");
        }
        if (!arrayList.isEmpty()) {
            DailyHeartRateSummary dailyHeartRateSummary2 = (DailyHeartRateSummary) yd6.d(arrayList);
            Boolean t = bk4.t(dailyHeartRateSummary2.getDate());
            wg6.a((Object) t, "DateHelper.isToday(todaySummary.date)");
            if (t.booleanValue()) {
                arrayList.add(0, new DailyHeartRateSummary(dailyHeartRateSummary2));
            }
        }
        return arrayList;
    }

    @DexIgnore
    private final rm6 loadData(Date date, Date date2, vk4.b.a aVar) {
        return ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new HeartRateSummaryLocalDataSource$loadData$Anon1(this, date, date2, aVar, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public final Date getMEndDate() {
        return this.mEndDate;
    }

    @DexIgnore
    public final vk4 getMHelper() {
        return this.mHelper;
    }

    @DexIgnore
    public final LiveData<NetworkState> getMNetworkState() {
        return this.mNetworkState;
    }

    @DexIgnore
    public final Date getMStartDate() {
        return this.mStartDate;
    }

    @DexIgnore
    public boolean isInvalid() {
        this.mFitnessDatabase.getInvalidationTracker().b();
        return HeartRateSummaryLocalDataSource.super.isInvalid();
    }

    @DexIgnore
    public void loadAfter(af.f<Date> fVar, af.a<Date, DailyHeartRateSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadAfter - createdDate=" + this.mCreatedDate + ", param.key=" + ((Date) fVar.a));
        if (bk4.b((Date) fVar.a, this.mCreatedDate)) {
            Object obj = fVar.a;
            wg6.a(obj, "params.key");
            Date date = (Date) obj;
            Companion companion = Companion;
            Object obj2 = fVar.a;
            wg6.a(obj2, "params.key");
            Date calculateNextKey = companion.calculateNextKey((Date) obj2, this.mCreatedDate);
            this.key.setTime(calculateNextKey);
            Date m = bk4.d(this.mCreatedDate, calculateNextKey) ? this.mCreatedDate : bk4.m(calculateNextKey);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "loadAfter - nextKey=" + calculateNextKey + ", startQueryDate=" + m + ", endQueryDate=" + date);
            wg6.a((Object) m, "startQueryDate");
            aVar.a(getDataInDatabase(m, date), calculateNextKey);
            if (bk4.b(this.mStartDate, date)) {
                this.mEndDate = date;
                calculateStartDate(this.mCreatedDate);
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = TAG;
                local3.d(str3, "loadAfter startDate=" + this.mStartDate + ", endDate=" + this.mEndDate);
                this.mRequestAfterQueue.add(new lc6(this.mStartDate, this.mEndDate));
                this.mHelper.a(vk4.d.AFTER, (vk4.b) new HeartRateSummaryLocalDataSource$loadAfter$Anon1(this));
            }
        }
    }

    @DexIgnore
    public void loadBefore(af.f<Date> fVar, af.a<Date, DailyHeartRateSummary> aVar) {
        wg6.b(fVar, "params");
        wg6.b(aVar, Constants.CALLBACK);
    }

    @DexIgnore
    public void loadInitial(af.e<Date> eVar, af.c<Date, DailyHeartRateSummary> cVar) {
        wg6.b(eVar, "params");
        wg6.b(cVar, Constants.CALLBACK);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "loadInitial - createdDate=" + this.mCreatedDate + ", key.time=" + this.key.getTime());
        Date date = this.mStartDate;
        Date m = bk4.d(this.mCreatedDate, this.key.getTime()) ? this.mCreatedDate : bk4.m(this.key.getTime());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "loadInitial - nextKey=" + this.key.getTime() + ", startQueryDate=" + m + ", endQueryDate=" + date);
        wg6.a((Object) m, "startQueryDate");
        cVar.a(getDataInDatabase(m, date), (Object) null, this.key.getTime());
        this.mHelper.a(vk4.d.INITIAL, (vk4.b) new HeartRateSummaryLocalDataSource$loadInitial$Anon1(this));
    }

    @DexIgnore
    public final void removePagingObserver() {
        this.mHelper.b(this.listener);
        this.mFitnessDatabase.getInvalidationTracker().c(this.mObserver);
    }

    @DexIgnore
    public final void setMEndDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mEndDate = date;
    }

    @DexIgnore
    public final void setMHelper(vk4 vk4) {
        wg6.b(vk4, "<set-?>");
        this.mHelper = vk4;
    }

    @DexIgnore
    public final void setMNetworkState(LiveData<NetworkState> liveData) {
        wg6.b(liveData, "<set-?>");
        this.mNetworkState = liveData;
    }

    @DexIgnore
    public final void setMStartDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mStartDate = date;
    }
}
