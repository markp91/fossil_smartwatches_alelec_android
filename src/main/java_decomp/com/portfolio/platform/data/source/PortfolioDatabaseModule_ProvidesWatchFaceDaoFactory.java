package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.WatchFaceDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory implements Factory<WatchFaceDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvidesWatchFaceDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static WatchFaceDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return proxyProvidesWatchFaceDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static WatchFaceDao proxyProvidesWatchFaceDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        WatchFaceDao providesWatchFaceDao = portfolioDatabaseModule.providesWatchFaceDao(dianaCustomizeDatabase);
        z76.a(providesWatchFaceDao, "Cannot return null from a non-@Nullable @Provides method");
        return providesWatchFaceDao;
    }

    @DexIgnore
    public WatchFaceDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
