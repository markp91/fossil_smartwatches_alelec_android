package com.portfolio.platform.data.source.local;

import com.portfolio.platform.data.model.Category;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface CategoryDao {
    @DexIgnore
    void clearData();

    @DexIgnore
    List<Category> getAllCategory();

    @DexIgnore
    Category getCategoryById(String str);

    @DexIgnore
    void upsertCategoryList(List<Category> list);
}
