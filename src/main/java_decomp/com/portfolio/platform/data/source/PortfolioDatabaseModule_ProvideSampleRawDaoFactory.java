package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.data.source.local.fitness.SampleRawDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideSampleRawDaoFactory implements Factory<SampleRawDao> {
    @DexIgnore
    public /* final */ Provider<FitnessDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideSampleRawDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideSampleRawDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideSampleRawDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static SampleRawDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<FitnessDatabase> provider) {
        return proxyProvideSampleRawDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static SampleRawDao proxyProvideSampleRawDao(PortfolioDatabaseModule portfolioDatabaseModule, FitnessDatabase fitnessDatabase) {
        SampleRawDao provideSampleRawDao = portfolioDatabaseModule.provideSampleRawDao(fitnessDatabase);
        z76.a(provideSampleRawDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideSampleRawDao;
    }

    @DexIgnore
    public SampleRawDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
