package com.portfolio.platform.data.source;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory implements Factory<NotificationsDataSource> {
    @DexIgnore
    public /* final */ NotificationsRepositoryModule module;

    @DexIgnore
    public NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory(NotificationsRepositoryModule notificationsRepositoryModule) {
        this.module = notificationsRepositoryModule;
    }

    @DexIgnore
    public static NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory create(NotificationsRepositoryModule notificationsRepositoryModule) {
        return new NotificationsRepositoryModule_ProvideRemoteNotificationsDataSourceFactory(notificationsRepositoryModule);
    }

    @DexIgnore
    public static NotificationsDataSource provideInstance(NotificationsRepositoryModule notificationsRepositoryModule) {
        return proxyProvideRemoteNotificationsDataSource(notificationsRepositoryModule);
    }

    @DexIgnore
    public static NotificationsDataSource proxyProvideRemoteNotificationsDataSource(NotificationsRepositoryModule notificationsRepositoryModule) {
        NotificationsDataSource provideRemoteNotificationsDataSource = notificationsRepositoryModule.provideRemoteNotificationsDataSource();
        z76.a(provideRemoteNotificationsDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideRemoteNotificationsDataSource;
    }

    @DexIgnore
    public NotificationsDataSource get() {
        return provideInstance(this.module);
    }
}
