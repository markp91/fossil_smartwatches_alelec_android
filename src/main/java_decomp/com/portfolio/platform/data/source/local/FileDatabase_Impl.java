package com.portfolio.platform.data.source.local;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileDatabase_Impl extends FileDatabase {
    @DexIgnore
    public volatile FileDao _fileDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `localfile` (`pinType` INTEGER NOT NULL, `fileName` TEXT NOT NULL, `localUri` TEXT NOT NULL, `remoteUrl` TEXT NOT NULL, `checksum` TEXT, PRIMARY KEY(`remoteUrl`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '86bae3c920c99bffd3cf337bd7d2de03')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `localfile`");
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FileDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FileDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = FileDatabase_Impl.this.mDatabase = iiVar;
            FileDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (FileDatabase_Impl.this.mCallbacks != null) {
                int size = FileDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) FileDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(5);
            hashMap.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("fileName", new fi.a("fileName", "TEXT", true, 0, (String) null, 1));
            hashMap.put("localUri", new fi.a("localUri", "TEXT", true, 0, (String) null, 1));
            hashMap.put("remoteUrl", new fi.a("remoteUrl", "TEXT", true, 1, (String) null, 1));
            hashMap.put("checksum", new fi.a("checksum", "TEXT", false, 0, (String) null, 1));
            fi fiVar = new fi("localfile", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "localfile");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "localfile(com.portfolio.platform.data.model.LocalFile).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        FileDatabase_Impl.super.assertNotMainThread();
        ii a = FileDatabase_Impl.super.getOpenHelper().a();
        try {
            FileDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `localfile`");
            FileDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            FileDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"localfile"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "86bae3c920c99bffd3cf337bd7d2de03", "bd3a6e5fde50d6c5a98cb60164fc5f85");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public FileDao fileDao() {
        FileDao fileDao;
        if (this._fileDao != null) {
            return this._fileDao;
        }
        synchronized (this) {
            if (this._fileDao == null) {
                this._fileDao = new FileDao_Impl(this);
            }
            fileDao = this._fileDao;
        }
        return fileDao;
    }
}
