package com.portfolio.platform.data.source.remote;

import com.fossil.wg6;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppSettingRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    public final List<WatchAppLastSetting> fetchAllWatchAppSetting() {
        return new ArrayList();
    }

    @DexIgnore
    public final void upsertWatchAppSetting(WatchAppLastSetting watchAppLastSetting) {
        wg6.b(watchAppLastSetting, "watchAppLastSetting");
    }
}
