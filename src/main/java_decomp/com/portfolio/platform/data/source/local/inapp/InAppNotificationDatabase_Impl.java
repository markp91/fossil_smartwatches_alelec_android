package com.portfolio.platform.data.source.local.inapp;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.portfolio.platform.data.model.Explore;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class InAppNotificationDatabase_Impl extends InAppNotificationDatabase {
    @DexIgnore
    public volatile InAppNotificationDao _inAppNotificationDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `inAppNotification` (`id` TEXT NOT NULL, `title` TEXT NOT NULL, `content` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b9f81264aeec3206295ee3fe096e0c84')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `inAppNotification`");
            if (InAppNotificationDatabase_Impl.this.mCallbacks != null) {
                int size = InAppNotificationDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) InAppNotificationDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (InAppNotificationDatabase_Impl.this.mCallbacks != null) {
                int size = InAppNotificationDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) InAppNotificationDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = InAppNotificationDatabase_Impl.this.mDatabase = iiVar;
            InAppNotificationDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (InAppNotificationDatabase_Impl.this.mCallbacks != null) {
                int size = InAppNotificationDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) InAppNotificationDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(3);
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put(Explore.COLUMN_TITLE, new fi.a(Explore.COLUMN_TITLE, "TEXT", true, 0, (String) null, 1));
            hashMap.put("content", new fi.a("content", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("inAppNotification", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "inAppNotification");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "inAppNotification(com.portfolio.platform.data.InAppNotification).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        InAppNotificationDatabase_Impl.super.assertNotMainThread();
        ii a = InAppNotificationDatabase_Impl.super.getOpenHelper().a();
        try {
            InAppNotificationDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `inAppNotification`");
            InAppNotificationDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            InAppNotificationDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"inAppNotification"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "b9f81264aeec3206295ee3fe096e0c84", "9dd4de0568e77b393b85859b42ea3ad5");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public InAppNotificationDao inAppNotificationDao() {
        InAppNotificationDao inAppNotificationDao;
        if (this._inAppNotificationDao != null) {
            return this._inAppNotificationDao;
        }
        synchronized (this) {
            if (this._inAppNotificationDao == null) {
                this._inAppNotificationDao = new InAppNotificationDao_Impl(this);
            }
            inAppNotificationDao = this._inAppNotificationDao;
        }
        return inAppNotificationDao;
    }
}
