package com.portfolio.platform.data.source.local.diana;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import com.portfolio.platform.data.legacy.threedotzero.MicroApp;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.RingStyleDao;
import com.portfolio.platform.data.source.local.RingStyleDao_Impl;
import com.zendesk.sdk.model.helpcenter.help.HelpRequest;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DianaCustomizeDatabase_Impl extends DianaCustomizeDatabase {
    @DexIgnore
    public volatile ComplicationDao _complicationDao;
    @DexIgnore
    public volatile ComplicationLastSettingDao _complicationLastSettingDao;
    @DexIgnore
    public volatile DianaPresetDao _dianaPresetDao;
    @DexIgnore
    public volatile RingStyleDao _ringStyleDao;
    @DexIgnore
    public volatile WatchAppDao _watchAppDao;
    @DexIgnore
    public volatile WatchAppLastSettingDao _watchAppLastSettingDao;
    @DexIgnore
    public volatile WatchFaceDao _watchFaceDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `complication` (`complicationId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `icon` TEXT, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `watchApp` (`watchappId` TEXT NOT NULL, `name` TEXT NOT NULL, `nameKey` TEXT NOT NULL, `description` TEXT NOT NULL, `descriptionKey` TEXT NOT NULL, `categories` TEXT NOT NULL, `icon` TEXT, `updatedAt` TEXT NOT NULL, `createdAt` TEXT NOT NULL, PRIMARY KEY(`watchappId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `complicationLastSetting` (`complicationId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`complicationId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `watchAppLastSetting` (`watchAppId` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, `setting` TEXT NOT NULL, PRIMARY KEY(`watchAppId`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `dianaPreset` (`createdAt` TEXT, `updatedAt` TEXT, `pinType` INTEGER NOT NULL, `id` TEXT NOT NULL, `serialNumber` TEXT NOT NULL, `name` TEXT NOT NULL, `isActive` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `dianaRecommendPreset` (`serialNumber` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `isDefault` INTEGER NOT NULL, `complications` TEXT NOT NULL, `watchapps` TEXT NOT NULL, `watchFaceId` TEXT NOT NULL, `createdAt` TEXT NOT NULL, `updatedAt` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `watch_face` (`id` TEXT NOT NULL, `name` TEXT NOT NULL, `ringStyleItems` TEXT, `background` TEXT NOT NULL, `previewUrl` TEXT NOT NULL, `serial` TEXT NOT NULL, `watchFaceType` INTEGER NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS `DianaComplicationRingStyle` (`id` TEXT NOT NULL, `category` TEXT NOT NULL, `name` TEXT NOT NULL, `data` TEXT NOT NULL, `metaData` TEXT NOT NULL, `serial` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, 'b6ffc006c1b31b2f7c026c60e1a5da30')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `complication`");
            iiVar.b("DROP TABLE IF EXISTS `watchApp`");
            iiVar.b("DROP TABLE IF EXISTS `complicationLastSetting`");
            iiVar.b("DROP TABLE IF EXISTS `watchAppLastSetting`");
            iiVar.b("DROP TABLE IF EXISTS `dianaPreset`");
            iiVar.b("DROP TABLE IF EXISTS `dianaRecommendPreset`");
            iiVar.b("DROP TABLE IF EXISTS `watch_face`");
            iiVar.b("DROP TABLE IF EXISTS `DianaComplicationRingStyle`");
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = DianaCustomizeDatabase_Impl.this.mDatabase = iiVar;
            DianaCustomizeDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (DianaCustomizeDatabase_Impl.this.mCallbacks != null) {
                int size = DianaCustomizeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) DianaCustomizeDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            ii iiVar2 = iiVar;
            HashMap hashMap = new HashMap(9);
            hashMap.put("complicationId", new fi.a("complicationId", "TEXT", true, 1, (String) null, 1));
            hashMap.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap.put("nameKey", new fi.a("nameKey", "TEXT", true, 0, (String) null, 1));
            hashMap.put(HelpRequest.INCLUDE_CATEGORIES, new fi.a(HelpRequest.INCLUDE_CATEGORIES, "TEXT", true, 0, (String) null, 1));
            hashMap.put("description", new fi.a("description", "TEXT", true, 0, (String) null, 1));
            hashMap.put("descriptionKey", new fi.a("descriptionKey", "TEXT", true, 0, (String) null, 1));
            hashMap.put(MicroApp.COLUMN_ICON, new fi.a(MicroApp.COLUMN_ICON, "TEXT", false, 0, (String) null, 1));
            hashMap.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            hashMap.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("complication", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar2, "complication");
            if (!fiVar.equals(a)) {
                return new qh.b(false, "complication(com.portfolio.platform.data.model.diana.Complication).\n Expected:\n" + fiVar + "\n Found:\n" + a);
            }
            HashMap hashMap2 = new HashMap(9);
            hashMap2.put("watchappId", new fi.a("watchappId", "TEXT", true, 1, (String) null, 1));
            hashMap2.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("nameKey", new fi.a("nameKey", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("description", new fi.a("description", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("descriptionKey", new fi.a("descriptionKey", "TEXT", true, 0, (String) null, 1));
            hashMap2.put(HelpRequest.INCLUDE_CATEGORIES, new fi.a(HelpRequest.INCLUDE_CATEGORIES, "TEXT", true, 0, (String) null, 1));
            hashMap2.put(MicroApp.COLUMN_ICON, new fi.a(MicroApp.COLUMN_ICON, "TEXT", false, 0, (String) null, 1));
            hashMap2.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap2.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            fi fiVar2 = new fi("watchApp", hashMap2, new HashSet(0), new HashSet(0));
            fi a2 = fi.a(iiVar2, "watchApp");
            if (!fiVar2.equals(a2)) {
                return new qh.b(false, "watchApp(com.portfolio.platform.data.model.diana.WatchApp).\n Expected:\n" + fiVar2 + "\n Found:\n" + a2);
            }
            HashMap hashMap3 = new HashMap(3);
            hashMap3.put("complicationId", new fi.a("complicationId", "TEXT", true, 1, (String) null, 1));
            hashMap3.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap3.put(MicroAppSetting.SETTING, new fi.a(MicroAppSetting.SETTING, "TEXT", true, 0, (String) null, 1));
            fi fiVar3 = new fi("complicationLastSetting", hashMap3, new HashSet(0), new HashSet(0));
            fi a3 = fi.a(iiVar2, "complicationLastSetting");
            if (!fiVar3.equals(a3)) {
                return new qh.b(false, "complicationLastSetting(com.portfolio.platform.data.model.diana.ComplicationLastSetting).\n Expected:\n" + fiVar3 + "\n Found:\n" + a3);
            }
            HashMap hashMap4 = new HashMap(3);
            hashMap4.put("watchAppId", new fi.a("watchAppId", "TEXT", true, 1, (String) null, 1));
            hashMap4.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            hashMap4.put(MicroAppSetting.SETTING, new fi.a(MicroAppSetting.SETTING, "TEXT", true, 0, (String) null, 1));
            fi fiVar4 = new fi("watchAppLastSetting", hashMap4, new HashSet(0), new HashSet(0));
            fi a4 = fi.a(iiVar2, "watchAppLastSetting");
            if (!fiVar4.equals(a4)) {
                return new qh.b(false, "watchAppLastSetting(com.portfolio.platform.data.model.diana.WatchAppLastSetting).\n Expected:\n" + fiVar4 + "\n Found:\n" + a4);
            }
            HashMap hashMap5 = new HashMap(10);
            hashMap5.put("createdAt", new fi.a("createdAt", "TEXT", false, 0, (String) null, 1));
            hashMap5.put("updatedAt", new fi.a("updatedAt", "TEXT", false, 0, (String) null, 1));
            hashMap5.put("pinType", new fi.a("pinType", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap5.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("isActive", new fi.a("isActive", "INTEGER", true, 0, (String) null, 1));
            hashMap5.put("complications", new fi.a("complications", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("watchapps", new fi.a("watchapps", "TEXT", true, 0, (String) null, 1));
            hashMap5.put("watchFaceId", new fi.a("watchFaceId", "TEXT", true, 0, (String) null, 1));
            fi fiVar5 = new fi("dianaPreset", hashMap5, new HashSet(0), new HashSet(0));
            fi a5 = fi.a(iiVar2, "dianaPreset");
            if (!fiVar5.equals(a5)) {
                return new qh.b(false, "dianaPreset(com.portfolio.platform.data.model.diana.preset.DianaPreset).\n Expected:\n" + fiVar5 + "\n Found:\n" + a5);
            }
            HashMap hashMap6 = new HashMap(9);
            hashMap6.put("serialNumber", new fi.a("serialNumber", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap6.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("isDefault", new fi.a("isDefault", "INTEGER", true, 0, (String) null, 1));
            hashMap6.put("complications", new fi.a("complications", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("watchapps", new fi.a("watchapps", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("watchFaceId", new fi.a("watchFaceId", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("createdAt", new fi.a("createdAt", "TEXT", true, 0, (String) null, 1));
            hashMap6.put("updatedAt", new fi.a("updatedAt", "TEXT", true, 0, (String) null, 1));
            fi fiVar6 = new fi("dianaRecommendPreset", hashMap6, new HashSet(0), new HashSet(0));
            fi a6 = fi.a(iiVar2, "dianaRecommendPreset");
            if (!fiVar6.equals(a6)) {
                return new qh.b(false, "dianaRecommendPreset(com.portfolio.platform.data.model.diana.preset.DianaRecommendPreset).\n Expected:\n" + fiVar6 + "\n Found:\n" + a6);
            }
            HashMap hashMap7 = new HashMap(7);
            hashMap7.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap7.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("ringStyleItems", new fi.a("ringStyleItems", "TEXT", false, 0, (String) null, 1));
            hashMap7.put(Explore.COLUMN_BACKGROUND, new fi.a(Explore.COLUMN_BACKGROUND, "TEXT", true, 0, (String) null, 1));
            hashMap7.put("previewUrl", new fi.a("previewUrl", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("serial", new fi.a("serial", "TEXT", true, 0, (String) null, 1));
            hashMap7.put("watchFaceType", new fi.a("watchFaceType", "INTEGER", true, 0, (String) null, 1));
            fi fiVar7 = new fi("watch_face", hashMap7, new HashSet(0), new HashSet(0));
            fi a7 = fi.a(iiVar2, "watch_face");
            if (!fiVar7.equals(a7)) {
                return new qh.b(false, "watch_face(com.portfolio.platform.data.model.diana.preset.WatchFace).\n Expected:\n" + fiVar7 + "\n Found:\n" + a7);
            }
            HashMap hashMap8 = new HashMap(6);
            hashMap8.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap8.put("category", new fi.a("category", "TEXT", true, 0, (String) null, 1));
            hashMap8.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap8.put("data", new fi.a("data", "TEXT", true, 0, (String) null, 1));
            hashMap8.put("metaData", new fi.a("metaData", "TEXT", true, 0, (String) null, 1));
            hashMap8.put("serial", new fi.a("serial", "TEXT", true, 0, (String) null, 1));
            fi fiVar8 = new fi("DianaComplicationRingStyle", hashMap8, new HashSet(0), new HashSet(0));
            fi a8 = fi.a(iiVar2, "DianaComplicationRingStyle");
            if (fiVar8.equals(a8)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "DianaComplicationRingStyle(com.portfolio.platform.data.model.diana.preset.DianaComplicationRingStyle).\n Expected:\n" + fiVar8 + "\n Found:\n" + a8);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        DianaCustomizeDatabase_Impl.super.assertNotMainThread();
        ii a = DianaCustomizeDatabase_Impl.super.getOpenHelper().a();
        try {
            DianaCustomizeDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `complication`");
            a.b("DELETE FROM `watchApp`");
            a.b("DELETE FROM `complicationLastSetting`");
            a.b("DELETE FROM `watchAppLastSetting`");
            a.b("DELETE FROM `dianaPreset`");
            a.b("DELETE FROM `dianaRecommendPreset`");
            a.b("DELETE FROM `watch_face`");
            a.b("DELETE FROM `DianaComplicationRingStyle`");
            DianaCustomizeDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            DianaCustomizeDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"complication", "watchApp", "complicationLastSetting", "watchAppLastSetting", "dianaPreset", "dianaRecommendPreset", "watch_face", "DianaComplicationRingStyle"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(14), "b6ffc006c1b31b2f7c026c60e1a5da30", "68d551717ecc33a44128455c32e84262");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public ComplicationDao getComplicationDao() {
        ComplicationDao complicationDao;
        if (this._complicationDao != null) {
            return this._complicationDao;
        }
        synchronized (this) {
            if (this._complicationDao == null) {
                this._complicationDao = new ComplicationDao_Impl(this);
            }
            complicationDao = this._complicationDao;
        }
        return complicationDao;
    }

    @DexIgnore
    public ComplicationLastSettingDao getComplicationSettingDao() {
        ComplicationLastSettingDao complicationLastSettingDao;
        if (this._complicationLastSettingDao != null) {
            return this._complicationLastSettingDao;
        }
        synchronized (this) {
            if (this._complicationLastSettingDao == null) {
                this._complicationLastSettingDao = new ComplicationLastSettingDao_Impl(this);
            }
            complicationLastSettingDao = this._complicationLastSettingDao;
        }
        return complicationLastSettingDao;
    }

    @DexIgnore
    public DianaPresetDao getPresetDao() {
        DianaPresetDao dianaPresetDao;
        if (this._dianaPresetDao != null) {
            return this._dianaPresetDao;
        }
        synchronized (this) {
            if (this._dianaPresetDao == null) {
                this._dianaPresetDao = new DianaPresetDao_Impl(this);
            }
            dianaPresetDao = this._dianaPresetDao;
        }
        return dianaPresetDao;
    }

    @DexIgnore
    public RingStyleDao getRingStyleDao() {
        RingStyleDao ringStyleDao;
        if (this._ringStyleDao != null) {
            return this._ringStyleDao;
        }
        synchronized (this) {
            if (this._ringStyleDao == null) {
                this._ringStyleDao = new RingStyleDao_Impl(this);
            }
            ringStyleDao = this._ringStyleDao;
        }
        return ringStyleDao;
    }

    @DexIgnore
    public WatchAppDao getWatchAppDao() {
        WatchAppDao watchAppDao;
        if (this._watchAppDao != null) {
            return this._watchAppDao;
        }
        synchronized (this) {
            if (this._watchAppDao == null) {
                this._watchAppDao = new WatchAppDao_Impl(this);
            }
            watchAppDao = this._watchAppDao;
        }
        return watchAppDao;
    }

    @DexIgnore
    public WatchAppLastSettingDao getWatchAppSettingDao() {
        WatchAppLastSettingDao watchAppLastSettingDao;
        if (this._watchAppLastSettingDao != null) {
            return this._watchAppLastSettingDao;
        }
        synchronized (this) {
            if (this._watchAppLastSettingDao == null) {
                this._watchAppLastSettingDao = new WatchAppLastSettingDao_Impl(this);
            }
            watchAppLastSettingDao = this._watchAppLastSettingDao;
        }
        return watchAppLastSettingDao;
    }

    @DexIgnore
    public WatchFaceDao getWatchFaceDao() {
        WatchFaceDao watchFaceDao;
        if (this._watchFaceDao != null) {
            return this._watchFaceDao;
        }
        synchronized (this) {
            if (this._watchFaceDao == null) {
                this._watchFaceDao = new WatchFaceDao_Impl(this);
            }
            watchFaceDao = this._watchFaceDao;
        }
        return watchFaceDao;
    }
}
