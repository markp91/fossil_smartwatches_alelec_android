package com.portfolio.platform.data.source.local.fitness;

import androidx.lifecycle.MutableLiveData;
import com.fossil.ik4;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryDataSourceFactory extends xe.b<Date, ActivitySummary> {
    @DexIgnore
    public /* final */ ActivitySummaryDao activitySummaryDao;
    @DexIgnore
    public /* final */ u04 appExecutors;
    @DexIgnore
    public /* final */ Date createdDate;
    @DexIgnore
    public /* final */ FitnessDataRepository fitnessDataRepository;
    @DexIgnore
    public /* final */ FitnessDatabase fitnessDatabase;
    @DexIgnore
    public /* final */ ik4 fitnessHelper;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public ActivitySummaryLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ Calendar mStartCalendar;
    @DexIgnore
    public /* final */ MutableLiveData<ActivitySummaryLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ SummariesRepository summariesRepository;

    @DexIgnore
    public ActivitySummaryDataSourceFactory(SummariesRepository summariesRepository2, ik4 ik4, FitnessDataRepository fitnessDataRepository2, ActivitySummaryDao activitySummaryDao2, FitnessDatabase fitnessDatabase2, Date date, u04 u04, vk4.a aVar, Calendar calendar) {
        wg6.b(summariesRepository2, "summariesRepository");
        wg6.b(ik4, "fitnessHelper");
        wg6.b(fitnessDataRepository2, "fitnessDataRepository");
        wg6.b(activitySummaryDao2, "activitySummaryDao");
        wg6.b(fitnessDatabase2, "fitnessDatabase");
        wg6.b(date, "createdDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        wg6.b(calendar, "mStartCalendar");
        this.summariesRepository = summariesRepository2;
        this.fitnessHelper = ik4;
        this.fitnessDataRepository = fitnessDataRepository2;
        this.activitySummaryDao = activitySummaryDao2;
        this.fitnessDatabase = fitnessDatabase2;
        this.createdDate = date;
        this.appExecutors = u04;
        this.listener = aVar;
        this.mStartCalendar = calendar;
    }

    @DexIgnore
    public xe<Date, ActivitySummary> create() {
        this.localDataSource = new ActivitySummaryLocalDataSource(this.summariesRepository, this.fitnessHelper, this.fitnessDataRepository, this.activitySummaryDao, this.fitnessDatabase, this.createdDate, this.appExecutors, this.listener, this.mStartCalendar);
        this.sourceLiveData.a(this.localDataSource);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.localDataSource;
        if (activitySummaryLocalDataSource != null) {
            return activitySummaryLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final ActivitySummaryLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<ActivitySummaryLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.localDataSource = activitySummaryLocalDataSource;
    }
}
