package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.du3;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ku3;
import com.fossil.lc6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.sd;
import com.fossil.tx5;
import com.fossil.v3;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yx5;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.FitnessDayData;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.ActivitySummaryDao;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.helper.GsonConvertDate;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getSummaries$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1_Level2 extends tx5<List<ActivitySummary>, ku3> {
        @DexIgnore
        public /* final */ /* synthetic */ lc6 $downloadingDate;
        @DexIgnore
        public /* final */ /* synthetic */ SummariesRepository$getSummaries$Anon1 this$0;

        @DexIgnore
        public Anon1_Level2(SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon1, lc6 lc6) {
            this.this$0 = summariesRepository$getSummaries$Anon1;
            this.$downloadingDate = lc6;
        }

        @DexIgnore
        public Object createCall(xe6<? super rx6<ku3>> xe6) {
            Date date;
            Date date2;
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.this$0.mApiServiceV2;
            lc6 lc6 = this.$downloadingDate;
            if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                date = this.this$0.$startDate;
            }
            String e = bk4.e(date);
            wg6.a((Object) e, "DateHelper.formatShortDa\u2026            ?: startDate)");
            lc6 lc62 = this.$downloadingDate;
            if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                date2 = this.this$0.$endDate;
            }
            String e2 = bk4.e(date2);
            wg6.a((Object) e2, "DateHelper.formatShortDa\u2026              ?: endDate)");
            return access$getMApiServiceV2$p.getSummaries(e, e2, 0, 100, xe6);
        }

        @DexIgnore
        public LiveData<List<ActivitySummary>> loadFromDb() {
            if (!bk4.t(this.this$0.$endDate).booleanValue()) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isNotToday - startDate=" + this.this$0.$startDate + ", endDate=" + this.this$0.$endDate);
                ActivitySummaryDao access$getMActivitySummaryDao$p = this.this$0.this$0.mActivitySummaryDao;
                SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon1 = this.this$0;
                return access$getMActivitySummaryDao$p.getActivitySummariesLiveData(summariesRepository$getSummaries$Anon1.$startDate, summariesRepository$getSummaries$Anon1.$endDate);
            }
            FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - loadFromDb -- isToday");
            ActivitySummaryDao access$getMActivitySummaryDao$p2 = this.this$0.this$0.mActivitySummaryDao;
            SummariesRepository$getSummaries$Anon1 summariesRepository$getSummaries$Anon12 = this.this$0;
            LiveData<List<ActivitySummary>> activitySummariesLiveData = access$getMActivitySummaryDao$p2.getActivitySummariesLiveData(summariesRepository$getSummaries$Anon12.$startDate, summariesRepository$getSummaries$Anon12.$endDate);
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon1_Level3(this, (xe6) null), 3, (Object) null);
            LiveData<List<ActivitySummary>> a = sd.a(activitySummariesLiveData, new SummariesRepository$getSummaries$Anon1$Anon1_Level2$loadFromDb$Anon2_Level3(this, activitySummariesLiveData));
            wg6.a((Object) a, "Transformations.map(resu\u2026                        }");
            return a;
        }

        @DexIgnore
        public void onFetchFailed(Throwable th) {
            FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getSummaries - onFetchFailed");
        }

        @DexIgnore
        public void saveCallResult(ku3 ku3) {
            Date date;
            Date date2;
            List<FitnessDayData> list;
            wg6.b(ku3, "item");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SummariesRepository.TAG, "getSummaries - saveCallResult -- item=" + ku3);
            try {
                ArrayList arrayList = new ArrayList();
                du3 du3 = new du3();
                du3.a(Date.class, new GsonConvertDate());
                du3.a(DateTime.class, new GsonConvertDateTime());
                ApiResponse apiResponse = (ApiResponse) du3.a().a(ku3.toString(), new SummariesRepository$getSummaries$Anon1$Anon1_Level2$saveCallResult$Anon1_Level3().getType());
                if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                    for (FitnessDayData activitySummary : list) {
                        ActivitySummary activitySummary2 = activitySummary.toActivitySummary();
                        wg6.a((Object) activitySummary2, "it.toActivitySummary()");
                        arrayList.add(activitySummary2);
                    }
                }
                FitnessDataDao access$getMFitnessDataDao$p = this.this$0.this$0.mFitnessDataDao;
                lc6 lc6 = this.$downloadingDate;
                if (lc6 == null || (date = (Date) lc6.getFirst()) == null) {
                    date = this.this$0.$startDate;
                }
                lc6 lc62 = this.$downloadingDate;
                if (lc62 == null || (date2 = (Date) lc62.getSecond()) == null) {
                    date2 = this.this$0.$endDate;
                }
                List<FitnessDataWrapper> fitnessData = access$getMFitnessDataDao$p.getFitnessData(date, date2);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d(SummariesRepository.TAG, "fitnessDatasSize " + fitnessData.size());
                if (fitnessData.isEmpty()) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    local3.d(SummariesRepository.TAG, "upsert 1 list " + arrayList);
                    this.this$0.this$0.mActivitySummaryDao.upsertActivitySummaries(arrayList);
                }
            } catch (Exception e) {
                ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                StringBuilder sb = new StringBuilder();
                sb.append("getSummaries - saveCallResult -- e=");
                e.printStackTrace();
                sb.append(cd6.a);
                local4.e(SummariesRepository.TAG, sb.toString());
            }
        }

        @DexIgnore
        public boolean shouldFetch(List<ActivitySummary> list) {
            return this.this$0.$shouldFetch && this.$downloadingDate != null;
        }
    }

    @DexIgnore
    public SummariesRepository$getSummaries$Anon1(SummariesRepository summariesRepository, Date date, Date date2, boolean z) {
        this.this$0 = summariesRepository;
        this.$startDate = date;
        this.$endDate = date2;
        this.$shouldFetch = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.SummariesRepository$getSummaries$Anon1$Anon1_Level2] */
    public final LiveData<yx5<List<ActivitySummary>>> apply(List<FitnessDataWrapper> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getSummaries - startDate=" + this.$startDate + ", endDate=" + this.$endDate + " fitnessDataList=" + list.size());
        wg6.a((Object) list, "fitnessDataList");
        return new Anon1_Level2(this, FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate)).asLiveData();
    }
}
