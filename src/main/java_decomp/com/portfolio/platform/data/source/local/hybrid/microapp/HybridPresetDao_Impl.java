package com.portfolio.platform.data.source.local.hybrid.microapp;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.h44;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridRecommendPreset;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HybridPresetDao_Impl implements HybridPresetDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ h44 __hybridAppSettingTypeConverter; // = new h44();
    @DexIgnore
    public /* final */ hh<HybridPreset> __insertionAdapterOfHybridPreset;
    @DexIgnore
    public /* final */ hh<HybridRecommendPreset> __insertionAdapterOfHybridRecommendPreset;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllPresetBySerial;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllPresetTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAllRecommendPresetTable;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeletePreset;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveAllDeletePinTypePreset;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<HybridRecommendPreset> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridRecommendPreset` (`id`,`name`,`serialNumber`,`buttons`,`isDefault`,`createdAt`,`updatedAt`) VALUES (?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, HybridRecommendPreset hybridRecommendPreset) {
            if (hybridRecommendPreset.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, hybridRecommendPreset.getId());
            }
            if (hybridRecommendPreset.getName() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, hybridRecommendPreset.getName());
            }
            if (hybridRecommendPreset.getSerialNumber() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, hybridRecommendPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridRecommendPreset.getButtons());
            if (a == null) {
                miVar.a(4);
            } else {
                miVar.a(4, a);
            }
            miVar.a(5, hybridRecommendPreset.isDefault() ? 1 : 0);
            if (hybridRecommendPreset.getCreatedAt() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, hybridRecommendPreset.getCreatedAt());
            }
            if (hybridRecommendPreset.getUpdatedAt() == null) {
                miVar.a(7);
            } else {
                miVar.a(7, hybridRecommendPreset.getUpdatedAt());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends hh<HybridPreset> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `hybridPreset` (`pinType`,`createdAt`,`updatedAt`,`id`,`name`,`serialNumber`,`buttons`,`isActive`) VALUES (?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, HybridPreset hybridPreset) {
            miVar.a(1, (long) hybridPreset.getPinType());
            if (hybridPreset.getCreatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, hybridPreset.getCreatedAt());
            }
            if (hybridPreset.getUpdatedAt() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, hybridPreset.getUpdatedAt());
            }
            if (hybridPreset.getId() == null) {
                miVar.a(4);
            } else {
                miVar.a(4, hybridPreset.getId());
            }
            if (hybridPreset.getName() == null) {
                miVar.a(5);
            } else {
                miVar.a(5, hybridPreset.getName());
            }
            if (hybridPreset.getSerialNumber() == null) {
                miVar.a(6);
            } else {
                miVar.a(6, hybridPreset.getSerialNumber());
            }
            String a = HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(hybridPreset.getButtons());
            if (a == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a);
            }
            miVar.a(8, hybridPreset.isActive() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 extends vh {
        @DexIgnore
        public Anon4(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE serialNumber=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon5 extends vh {
        @DexIgnore
        public Anon5(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridRecommendPreset";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon6 extends vh {
        @DexIgnore
        public Anon6(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon7 extends vh {
        @DexIgnore
        public Anon7(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM hybridPreset WHERE pinType = 3";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon8 implements Callable<List<HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon8(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<HybridPreset> call() throws Exception {
            Cursor a = bi.a(HybridPresetDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "pinType");
                int b2 = ai.b(a, "createdAt");
                int b3 = ai.b(a, "updatedAt");
                int b4 = ai.b(a, "id");
                int b5 = ai.b(a, "name");
                int b6 = ai.b(a, "serialNumber");
                int b7 = ai.b(a, "buttons");
                int b8 = ai.b(a, "isActive");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    HybridPreset hybridPreset = new HybridPreset(a.getString(b4), a.getString(b5), a.getString(b6), HybridPresetDao_Impl.this.__hybridAppSettingTypeConverter.a(a.getString(b7)), a.getInt(b8) != 0);
                    hybridPreset.setPinType(a.getInt(b));
                    hybridPreset.setCreatedAt(a.getString(b2));
                    hybridPreset.setUpdatedAt(a.getString(b3));
                    arrayList.add(hybridPreset);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HybridPresetDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfHybridRecommendPreset = new Anon1(ohVar);
        this.__insertionAdapterOfHybridPreset = new Anon2(ohVar);
        this.__preparedStmtOfClearAllPresetTable = new Anon3(ohVar);
        this.__preparedStmtOfClearAllPresetBySerial = new Anon4(ohVar);
        this.__preparedStmtOfClearAllRecommendPresetTable = new Anon5(ohVar);
        this.__preparedStmtOfDeletePreset = new Anon6(ohVar);
        this.__preparedStmtOfRemoveAllDeletePinTypePreset = new Anon7(ohVar);
    }

    @DexIgnore
    public void clearAllPresetBySerial(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllPresetBySerial.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetBySerial.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void clearAllRecommendPresetTable() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAllRecommendPresetTable.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAllRecommendPresetTable.release(acquire);
        }
    }

    @DexIgnore
    public void deletePreset(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeletePreset.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeletePreset.release(acquire);
        }
    }

    @DexIgnore
    public HybridPreset getActivePresetBySerial(String str) {
        HybridPreset hybridPreset;
        String str2 = str;
        rh b = rh.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND isActive = 1 AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "createdAt");
            int b4 = ai.b(a, "updatedAt");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "name");
            int b7 = ai.b(a, "serialNumber");
            int b8 = ai.b(a, "buttons");
            int b9 = ai.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            } else {
                hybridPreset = null;
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridPreset> getAllPendingPreset(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 0", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "createdAt");
            int b4 = ai.b(a, "updatedAt");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "name");
            int b7 = ai.b(a, "serialNumber");
            int b8 = ai.b(a, "buttons");
            int b9 = ai.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridPreset> getAllPreset(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC ", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "createdAt");
            int b4 = ai.b(a, "updatedAt");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "name");
            int b7 = ai.b(a, "serialNumber");
            int b8 = ai.b(a, "buttons");
            int b9 = ai.b(a, "isActive");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                HybridPreset hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
                arrayList.add(hybridPreset);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<HybridPreset>> getAllPresetAsLiveData(String str) {
        rh b = rh.b("SELECT * FROM hybridPreset WHERE serialNumber=? AND pinType != 3 ORDER BY createdAt ASC", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"hybridPreset"}, false, new Anon8(b));
    }

    @DexIgnore
    public HybridPreset getPresetById(String str) {
        HybridPreset hybridPreset;
        String str2 = str;
        rh b = rh.b("SELECT * FROM hybridPreset WHERE id=? AND pinType != 3", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "pinType");
            int b3 = ai.b(a, "createdAt");
            int b4 = ai.b(a, "updatedAt");
            int b5 = ai.b(a, "id");
            int b6 = ai.b(a, "name");
            int b7 = ai.b(a, "serialNumber");
            int b8 = ai.b(a, "buttons");
            int b9 = ai.b(a, "isActive");
            if (a.moveToFirst()) {
                hybridPreset = new HybridPreset(a.getString(b5), a.getString(b6), a.getString(b7), this.__hybridAppSettingTypeConverter.a(a.getString(b8)), a.getInt(b9) != 0);
                hybridPreset.setPinType(a.getInt(b2));
                hybridPreset.setCreatedAt(a.getString(b3));
                hybridPreset.setUpdatedAt(a.getString(b4));
            } else {
                hybridPreset = null;
            }
            return hybridPreset;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public List<HybridRecommendPreset> getRecommendPresetList(String str) {
        String str2 = str;
        rh b = rh.b("SELECT * FROM hybridRecommendPreset WHERE serialNumber=?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "name");
            int b4 = ai.b(a, "serialNumber");
            int b5 = ai.b(a, "buttons");
            int b6 = ai.b(a, "isDefault");
            int b7 = ai.b(a, "createdAt");
            int b8 = ai.b(a, "updatedAt");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new HybridRecommendPreset(a.getString(b2), a.getString(b3), a.getString(b4), this.__hybridAppSettingTypeConverter.a(a.getString(b5)), a.getInt(b6) != 0, a.getString(b7), a.getString(b8)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void removeAllDeletePinTypePreset() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveAllDeletePinTypePreset.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAllDeletePinTypePreset.release(acquire);
        }
    }

    @DexIgnore
    public void upsertPreset(HybridPreset hybridPreset) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(hybridPreset);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertPresetList(List<HybridPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertRecommendPresetList(List<HybridRecommendPreset> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHybridRecommendPreset.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
