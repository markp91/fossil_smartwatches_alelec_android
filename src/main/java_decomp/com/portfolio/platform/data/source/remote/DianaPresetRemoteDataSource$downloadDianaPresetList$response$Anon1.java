package com.portfolio.platform.data.source.remote;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.DianaPresetRemoteDataSource$downloadDianaPresetList$response$1", f = "DianaPresetRemoteDataSource.kt", l = {106}, m = "invokeSuspend")
public final class DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<DianaPreset>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPresetRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1(DianaPresetRemoteDataSource dianaPresetRemoteDataSource, String str, xe6 xe6) {
        super(1, xe6);
        this.this$0 = dianaPresetRemoteDataSource;
        this.$serial = str;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1(this.this$0, this.$serial, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((DianaPresetRemoteDataSource$downloadDianaPresetList$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiServiceV2Dot1$p = this.this$0.mApiServiceV2Dot1;
            String str = this.$serial;
            this.label = 1;
            obj = access$getMApiServiceV2Dot1$p.getDianaPresetList(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
