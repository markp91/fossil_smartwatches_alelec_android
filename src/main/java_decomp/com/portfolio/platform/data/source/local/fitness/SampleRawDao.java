package com.portfolio.platform.data.source.local.fitness;

import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class SampleRawDao {
    @DexIgnore
    public abstract void deleteAllActivitySamples();

    @DexIgnore
    public abstract List<SampleRaw> getListActivitySampleByUaType(int i);

    @DexIgnore
    public abstract List<SampleRaw> getPendingActivitySamples();

    @DexIgnore
    public abstract List<SampleRaw> getPendingActivitySamples(Date date, Date date2);

    @DexIgnore
    public abstract void upsertListActivitySample(List<SampleRaw> list);
}
