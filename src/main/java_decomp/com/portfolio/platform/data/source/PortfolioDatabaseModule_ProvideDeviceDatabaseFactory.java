package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDeviceDatabaseFactory implements Factory<DeviceDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDeviceDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDeviceDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideDeviceDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DeviceDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideDeviceDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static DeviceDatabase proxyProvideDeviceDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        DeviceDatabase provideDeviceDatabase = portfolioDatabaseModule.provideDeviceDatabase(portfolioApp);
        z76.a(provideDeviceDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideDeviceDatabase;
    }

    @DexIgnore
    public DeviceDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
