package com.portfolio.platform.data.source.local.diana.notification;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationSettingsDao_Impl implements NotificationSettingsDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<NotificationSettingsModel> __insertionAdapterOfNotificationSettingsModel;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDelete;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<NotificationSettingsModel> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `notificationSettings` (`settingsName`,`settingsType`,`isCall`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, NotificationSettingsModel notificationSettingsModel) {
            if (notificationSettingsModel.getSettingsName() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, notificationSettingsModel.getSettingsName());
            }
            miVar.a(2, (long) notificationSettingsModel.getSettingsType());
            miVar.a(3, notificationSettingsModel.isCall() ? 1 : 0);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM notificationSettings";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<NotificationSettingsModel>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<NotificationSettingsModel> call() throws Exception {
            Cursor a = bi.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "settingsName");
                int b2 = ai.b(a, "settingsType");
                int b3 = ai.b(a, "isCall");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new NotificationSettingsModel(a.getString(b), a.getInt(b2), a.getInt(b3) != 0));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<NotificationSettingsModel> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public NotificationSettingsModel call() throws Exception {
            NotificationSettingsModel notificationSettingsModel = null;
            boolean z = false;
            Cursor a = bi.a(NotificationSettingsDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "settingsName");
                int b2 = ai.b(a, "settingsType");
                int b3 = ai.b(a, "isCall");
                if (a.moveToFirst()) {
                    String string = a.getString(b);
                    int i = a.getInt(b2);
                    if (a.getInt(b3) != 0) {
                        z = true;
                    }
                    notificationSettingsModel = new NotificationSettingsModel(string, i, z);
                }
                return notificationSettingsModel;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public NotificationSettingsDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfNotificationSettingsModel = new Anon1(ohVar);
        this.__preparedStmtOfDelete = new Anon2(ohVar);
    }

    @DexIgnore
    public void delete() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDelete.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDelete.release(acquire);
        }
    }

    @DexIgnore
    public LiveData<List<NotificationSettingsModel>> getListNotificationSettings() {
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, new Anon3(rh.b("SELECT * FROM notificationSettings", 0)));
    }

    @DexIgnore
    public List<NotificationSettingsModel> getListNotificationSettingsNoLiveData() {
        rh b = rh.b("SELECT * FROM notificationSettings", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "settingsName");
            int b3 = ai.b(a, "settingsType");
            int b4 = ai.b(a, "isCall");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new NotificationSettingsModel(a.getString(b2), a.getInt(b3), a.getInt(b4) != 0));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<NotificationSettingsModel> getNotificationSettingsWithFieldIsCall(boolean z) {
        rh b = rh.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.a(1, z ? 1 : 0);
        return this.__db.getInvalidationTracker().a(new String[]{"notificationSettings"}, false, new Anon4(b));
    }

    @DexIgnore
    public NotificationSettingsModel getNotificationSettingsWithIsCallNoLiveData(boolean z) {
        boolean z2 = true;
        rh b = rh.b("SELECT * FROM notificationSettings WHERE isCall = ?", 1);
        b.a(1, z ? 1 : 0);
        this.__db.assertNotSuspendingTransaction();
        NotificationSettingsModel notificationSettingsModel = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "settingsName");
            int b3 = ai.b(a, "settingsType");
            int b4 = ai.b(a, "isCall");
            if (a.moveToFirst()) {
                String string = a.getString(b2);
                int i = a.getInt(b3);
                if (a.getInt(b4) == 0) {
                    z2 = false;
                }
                notificationSettingsModel = new NotificationSettingsModel(string, i, z2);
            }
            return notificationSettingsModel;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertListNotificationSettings(List<NotificationSettingsModel> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertNotificationSettings(NotificationSettingsModel notificationSettingsModel) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfNotificationSettingsModel.insert(notificationSettingsModel);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
