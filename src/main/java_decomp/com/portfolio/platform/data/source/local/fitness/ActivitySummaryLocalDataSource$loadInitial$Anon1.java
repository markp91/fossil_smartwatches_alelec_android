package com.portfolio.platform.data.source.local.fitness;

import com.fossil.vk4;
import com.fossil.wg6;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivitySummaryLocalDataSource$loadInitial$Anon1 implements vk4.b {
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    public ActivitySummaryLocalDataSource$loadInitial$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource) {
        this.this$0 = activitySummaryLocalDataSource;
    }

    @DexIgnore
    public final void run(vk4.b.a aVar) {
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource = this.this$0;
        activitySummaryLocalDataSource.calculateStartDate(activitySummaryLocalDataSource.mCreatedDate);
        ActivitySummaryLocalDataSource activitySummaryLocalDataSource2 = this.this$0;
        vk4.d dVar = vk4.d.INITIAL;
        Date mStartDate = activitySummaryLocalDataSource2.getMStartDate();
        Date mEndDate = this.this$0.getMEndDate();
        wg6.a((Object) aVar, "helperCallback");
        activitySummaryLocalDataSource2.loadData(dVar, mStartDate, mEndDate, aVar);
    }
}
