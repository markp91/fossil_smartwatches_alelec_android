package com.portfolio.platform.data.source.local.diana.notification;

import com.fossil.oh;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class NotificationSettingsDatabase extends oh {
    @DexIgnore
    public abstract NotificationSettingsDao getNotificationSettingsDao();
}
