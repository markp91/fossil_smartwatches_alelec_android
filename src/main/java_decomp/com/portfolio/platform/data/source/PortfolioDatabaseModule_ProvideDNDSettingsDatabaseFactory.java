package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory implements Factory<DNDSettingsDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideDNDSettingsDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DNDSettingsDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideDNDSettingsDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static DNDSettingsDatabase proxyProvideDNDSettingsDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        DNDSettingsDatabase provideDNDSettingsDatabase = portfolioDatabaseModule.provideDNDSettingsDatabase(portfolioApp);
        z76.a(provideDNDSettingsDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideDNDSettingsDatabase;
    }

    @DexIgnore
    public DNDSettingsDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
