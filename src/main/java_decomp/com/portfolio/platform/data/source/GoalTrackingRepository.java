package com.portfolio.platform.data.source;

import android.text.TextUtils;
import androidx.lifecycle.LiveData;
import com.fossil.aj6;
import com.fossil.an4;
import com.fossil.ap4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.cf;
import com.fossil.cn6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.fu3;
import com.fossil.gk6;
import com.fossil.hf6;
import com.fossil.kc6;
import com.fossil.ku3;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rd6;
import com.fossil.sd;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.fossil.yx5;
import com.fossil.ze;
import com.fossil.zl6;
import com.fossil.zo4;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.cloud.CloudLogWriter;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.Listing;
import com.portfolio.platform.data.model.GoalSetting;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.Range;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingData;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.model.goaltracking.response.GoalEvent;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDao;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataLocalDataSource;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingDatabase;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryDataSourceFactory;
import com.portfolio.platform.data.source.local.hybrid.goaltracking.GoalTrackingSummaryLocalDataSource;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.UpsertApiResponse;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingRepository {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;
    @DexIgnore
    public /* final */ GoalTrackingDao mGoalTrackingDao;
    @DexIgnore
    public /* final */ GoalTrackingDatabase mGoalTrackingDatabase;
    @DexIgnore
    public /* final */ an4 mSharedPreferencesManager;
    @DexIgnore
    public List<GoalTrackingDataSourceFactory> mSourceDataFactoryList; // = new ArrayList();
    @DexIgnore
    public List<GoalTrackingSummaryDataSourceFactory> mSourceFactoryList; // = new ArrayList();
    @DexIgnore
    public /* final */ UserRepository mUserRepository;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG() {
            return GoalTrackingRepository.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface PushPendingGoalTrackingDataListCallback {
        @DexIgnore
        void onFail(int i);

        @DexIgnore
        void onSuccess(List<GoalTrackingData> list);
    }

    @DexIgnore
    public interface UpdateGoalSettingCallback {
        @DexIgnore
        void onFail(zo4<GoalSetting> zo4);

        @DexIgnore
        void onSuccess(cp4<GoalSetting> cp4);
    }

    /*
    static {
        String simpleName = GoalTrackingRepository.class.getSimpleName();
        wg6.a((Object) simpleName, "GoalTrackingRepository::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public GoalTrackingRepository(GoalTrackingDatabase goalTrackingDatabase, GoalTrackingDao goalTrackingDao, UserRepository userRepository, an4 an4, ApiServiceV2 apiServiceV2) {
        wg6.b(goalTrackingDatabase, "mGoalTrackingDatabase");
        wg6.b(goalTrackingDao, "mGoalTrackingDao");
        wg6.b(userRepository, "mUserRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mGoalTrackingDatabase = goalTrackingDatabase;
        this.mGoalTrackingDao = goalTrackingDao;
        this.mUserRepository = userRepository;
        this.mSharedPreferencesManager = an4;
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    private final LiveData<List<GoalTrackingData>> getPendingGoalTrackingDataListLiveData(Date date, Date date2) {
        return this.mGoalTrackingDao.getPendingGoalTrackingDataListLiveData(date, date2);
    }

    @DexIgnore
    public static /* synthetic */ Object loadGoalTrackingDataList$default(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, int i, int i2, xe6 xe6, int i3, Object obj) {
        return goalTrackingRepository.loadGoalTrackingDataList(date, date2, (i3 & 4) != 0 ? 0 : i, (i3 & 8) != 0 ? 100 : i2, xe6);
    }

    @DexIgnore
    private final void removeDeletedGoalTrackingList(List<GoalTrackingData> list) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "removeDeletedGoalTrackingList goalTrackingDataList size= " + list.size());
        for (GoalTrackingData component1 : list) {
            this.mGoalTrackingDao.removeDeletedGoalTrackingData(component1.component1());
        }
    }

    @DexIgnore
    private final void updateGoalTrackingPinType(List<GoalTrackingData> list, int i) {
        for (GoalTrackingData pinType : list) {
            pinType.setPinType(i);
        }
        this.mGoalTrackingDao.upsertListGoalTrackingData(list);
    }

    @DexIgnore
    public final void cleanUp() {
        FLogger.INSTANCE.getLocal().d(TAG, "cleanUp");
        removePagingListener();
        this.mGoalTrackingDao.deleteAllGoalTrackingSummaries();
        this.mGoalTrackingDao.deleteAllGoalTrackingData();
        this.mGoalTrackingDao.deleteGoalSetting();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00cd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object delete(List<GoalTrackingData> list, xe6<? super ap4<List<GoalTrackingData>>> xe6) {
        GoalTrackingRepository$delete$Anon1 goalTrackingRepository$delete$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        ap4 ap4;
        if (xe6 instanceof GoalTrackingRepository$delete$Anon1) {
            goalTrackingRepository$delete$Anon1 = (GoalTrackingRepository$delete$Anon1) xe6;
            int i2 = goalTrackingRepository$delete$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$delete$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$delete$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$delete$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "deleteGoalTrackingDataList: sampleRawList =" + list.size());
                    fu3 fu3 = new fu3();
                    for (GoalTrackingData component1 : list) {
                        try {
                            fu3.a(component1.component1());
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = TAG;
                            local2.e(str2, "updateGoalSetting exception=" + e);
                            e.printStackTrace();
                        }
                    }
                    ku3 ku3 = new ku3();
                    ku3.a("_ids", fu3);
                    GoalTrackingRepository$delete$repoResponse$Anon1 goalTrackingRepository$delete$repoResponse$Anon1 = new GoalTrackingRepository$delete$repoResponse$Anon1(this, ku3, (xe6) null);
                    goalTrackingRepository$delete$Anon1.L$0 = this;
                    goalTrackingRepository$delete$Anon1.L$1 = list;
                    goalTrackingRepository$delete$Anon1.L$2 = fu3;
                    goalTrackingRepository$delete$Anon1.L$3 = ku3;
                    goalTrackingRepository$delete$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$delete$repoResponse$Anon1, goalTrackingRepository$delete$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    ku3 ku32 = (ku3) goalTrackingRepository$delete$Anon1.L$3;
                    fu3 fu32 = (fu3) goalTrackingRepository$delete$Anon1.L$2;
                    list = (List) goalTrackingRepository$delete$Anon1.L$1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$delete$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    local3.d(str3, "deleteGoalTrackingDataList onResponse: response = " + ap4);
                    goalTrackingRepository.removeDeletedGoalTrackingList(list);
                    return new cp4(list, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("deleteGoalTrackingDataList Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local4.d(str4, sb.toString());
                    if (zo4.a() == 422) {
                        ArrayList arrayList = new ArrayList();
                        if (!TextUtils.isEmpty(zo4.b())) {
                            try {
                                Object a2 = new Gson().a(((zo4) ap4).b(), new GoalTrackingRepository$delete$type$Anon1().getType());
                                wg6.a(a2, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list2 = ((UpsertApiResponse) a2).get_items();
                                if (true ^ list2.isEmpty()) {
                                    int size = list2.size();
                                    for (int i3 = 0; i3 < size; i3++) {
                                        Integer code = ((ServerError) list2.get(i3)).getCode();
                                        if (code != null) {
                                            if (code.intValue() == 404001) {
                                                GoalTrackingData goalTrackingData = list.get(i3);
                                                goalTrackingData.setPinType(0);
                                                arrayList.add(goalTrackingData);
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e2) {
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String str5 = TAG;
                                local5.e(str5, "insertGoalTrackingDataList ex=" + e2);
                                e2.printStackTrace();
                            }
                            goalTrackingRepository.removeDeletedGoalTrackingList(arrayList);
                        }
                        new cp4(list, false, 2, (qg6) null);
                    }
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        goalTrackingRepository$delete$Anon1 = new GoalTrackingRepository$delete$Anon1(this, xe6);
        Object obj2 = goalTrackingRepository$delete$Anon1.result;
        Object a3 = ff6.a();
        i = goalTrackingRepository$delete$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final Object deleteGoalTracking(GoalTrackingData goalTrackingData, xe6<? super cd6> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "deleteGoalTracking " + goalTrackingData);
        this.mGoalTrackingDao.deleteGoalTrackingRawData(goalTrackingData);
        Object pushPendingGoalTrackingDataList = pushPendingGoalTrackingDataList(xe6);
        if (pushPendingGoalTrackingDataList == ff6.a()) {
            return pushPendingGoalTrackingDataList;
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object fetchGoalSetting(xe6<? super ap4<GoalSetting>> xe6) {
        GoalTrackingRepository$fetchGoalSetting$Anon1 goalTrackingRepository$fetchGoalSetting$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        ap4 ap4;
        String message;
        if (xe6 instanceof GoalTrackingRepository$fetchGoalSetting$Anon1) {
            goalTrackingRepository$fetchGoalSetting$Anon1 = (GoalTrackingRepository$fetchGoalSetting$Anon1) xe6;
            int i2 = goalTrackingRepository$fetchGoalSetting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$fetchGoalSetting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$fetchGoalSetting$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$fetchGoalSetting$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    FLogger.INSTANCE.getLocal().d(TAG, "fetchGoalSetting");
                    GoalTrackingRepository$fetchGoalSetting$response$Anon1 goalTrackingRepository$fetchGoalSetting$response$Anon1 = new GoalTrackingRepository$fetchGoalSetting$response$Anon1(this, (xe6) null);
                    goalTrackingRepository$fetchGoalSetting$Anon1.L$0 = this;
                    goalTrackingRepository$fetchGoalSetting$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$fetchGoalSetting$response$Anon1, goalTrackingRepository$fetchGoalSetting$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$fetchGoalSetting$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null) {
                        goalTrackingRepository.saveSettingToDB((GoalSetting) cp4.a());
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("fetchGoalSettings Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb.append(str);
                    local.e(str2, sb.toString());
                }
                return ap4;
            }
        }
        goalTrackingRepository$fetchGoalSetting$Anon1 = new GoalTrackingRepository$fetchGoalSetting$Anon1(this, xe6);
        Object obj2 = goalTrackingRepository$fetchGoalSetting$Anon1.result;
        Object a2 = ff6.a();
        i = goalTrackingRepository$fetchGoalSetting$Anon1.label;
        String str3 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final List<GoalTrackingData> getGoalTrackingDataInDate(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        return this.mGoalTrackingDao.getGoalTrackingDataInDate(date);
    }

    @DexIgnore
    public final LiveData<yx5<List<GoalTrackingData>>> getGoalTrackingDataList(Date date, Date date2, boolean z) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getGoalTrackingDataList startDate=" + date + ", endDate=" + date2);
        LiveData<yx5<List<GoalTrackingData>>> b = sd.b(getPendingGoalTrackingDataListLiveData(date, date2), new GoalTrackingRepository$getGoalTrackingDataList$Anon1(this, date, date2, z));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<GoalTrackingData> getGoalTrackingDataPaging(Date date, GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, u04 u04, vk4.a aVar) {
        wg6.b(date, "currentDate");
        wg6.b(goalTrackingRepository, "goalTrackingRepository");
        wg6.b(goalTrackingDao, "goalTrackingDao");
        wg6.b(goalTrackingDatabase, "goalTrackingDatabase");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        FLogger.INSTANCE.getLocal().d(TAG, "getGoalTrackingDataPaging");
        GoalTrackingDataSourceFactory goalTrackingDataSourceFactory = new GoalTrackingDataSourceFactory(goalTrackingRepository, goalTrackingDao, goalTrackingDatabase, date, u04, aVar);
        this.mSourceDataFactoryList.add(goalTrackingDataSourceFactory);
        cf.h.a aVar2 = new cf.h.a();
        aVar2.a(100);
        aVar2.a(false);
        aVar2.b(100);
        aVar2.c(5);
        cf.h a = aVar2.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(goalTrackingDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(goalTrackingDataSourceFactory.getSourceLiveData(), GoalTrackingRepository$getGoalTrackingDataPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new GoalTrackingRepository$getGoalTrackingDataPaging$Anon2(goalTrackingDataSourceFactory), new GoalTrackingRepository$getGoalTrackingDataPaging$Anon3(goalTrackingDataSourceFactory));
    }

    @DexIgnore
    public final List<GoalTrackingSummary> getGoalTrackingSummaries(Date date, Date date2) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        return this.mGoalTrackingDao.getGoalTrackingSummaries(date, date2);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.util.NetworkBoundResource, com.portfolio.platform.data.source.GoalTrackingRepository$getLastGoalSetting$Anon1] */
    public final LiveData<yx5<Integer>> getLastGoalSetting() {
        return new GoalTrackingRepository$getLastGoalSetting$Anon1(this).asLiveData();
    }

    @DexIgnore
    public final List<GoalTrackingData> getPendingGoalTrackingDataList(Date date, Date date2) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        return this.mGoalTrackingDao.getPendingGoalTrackingDataList(date, date2);
    }

    @DexIgnore
    public final LiveData<yx5<List<GoalTrackingSummary>>> getSummaries(Date date, Date date2, boolean z) {
        wg6.b(date, "startDate");
        wg6.b(date2, "endDate");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummaries startDate=" + date + ", endDate=" + date2);
        LiveData<yx5<List<GoalTrackingSummary>>> b = sd.b(getPendingGoalTrackingDataListLiveData(date, date2), new GoalTrackingRepository$getSummaries$Anon1(this, date, date2, z));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    @DexIgnore
    public final Listing<GoalTrackingSummary> getSummariesPaging(GoalTrackingRepository goalTrackingRepository, GoalTrackingDao goalTrackingDao, GoalTrackingDatabase goalTrackingDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(goalTrackingRepository, "goalTrackingRepository");
        wg6.b(goalTrackingDao, "goalTrackingDao");
        wg6.b(goalTrackingDatabase, "goalTrackingDatabase");
        wg6.b(date, "createdDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        FLogger.INSTANCE.getLocal().d(TAG, "getSummariesPaging");
        GoalTrackingSummaryLocalDataSource.Companion companion = GoalTrackingSummaryLocalDataSource.Companion;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        Date time = instance.getTime();
        wg6.a((Object) time, "Calendar.getInstance().time");
        Date calculateNextKey = companion.calculateNextKey(time, date);
        Calendar instance2 = Calendar.getInstance();
        wg6.a((Object) instance2, "calendar");
        instance2.setTime(calculateNextKey);
        GoalTrackingSummaryDataSourceFactory goalTrackingSummaryDataSourceFactory = new GoalTrackingSummaryDataSourceFactory(goalTrackingRepository, goalTrackingDao, goalTrackingDatabase, date, u04, aVar, instance2);
        this.mSourceFactoryList.add(goalTrackingSummaryDataSourceFactory);
        cf.h.a aVar2 = new cf.h.a();
        aVar2.a(30);
        aVar2.a(false);
        aVar2.b(30);
        aVar2.c(5);
        cf.h a = aVar2.a();
        wg6.a((Object) a, "PagedList.Config.Builder\u2026\n                .build()");
        LiveData a2 = new ze(goalTrackingSummaryDataSourceFactory, a).a();
        wg6.a((Object) a2, "LivePagedListBuilder(sou\u2026eFactory, config).build()");
        LiveData b = sd.b(goalTrackingSummaryDataSourceFactory.getSourceLiveData(), GoalTrackingRepository$getSummariesPaging$Anon1.INSTANCE);
        wg6.a((Object) b, "Transformations.switchMa\u2026rkState\n                }");
        return new Listing<>(a2, b, new GoalTrackingRepository$getSummariesPaging$Anon2(goalTrackingSummaryDataSourceFactory), new GoalTrackingRepository$getSummariesPaging$Anon3(goalTrackingSummaryDataSourceFactory));
    }

    @DexIgnore
    public final LiveData<yx5<GoalTrackingSummary>> getSummary(Date date) {
        wg6.b(date, HardwareLog.COLUMN_DATE);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "getSummary date=" + date);
        LiveData<yx5<GoalTrackingSummary>> b = sd.b(getPendingGoalTrackingDataListLiveData(date, date), new GoalTrackingRepository$getSummary$Anon1(this, date));
        wg6.a((Object) b, "Transformations.switchMa\u2026 }.asLiveData()\n        }");
        return b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01d5, code lost:
        if (r7.intValue() != 409000) goto L_0x01d7;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0047  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0118  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object insert(List<GoalTrackingData> list, xe6<? super ap4<List<GoalTrackingData>>> xe6) {
        GoalTrackingRepository$insert$Anon1 goalTrackingRepository$insert$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        ap4 ap4;
        if (xe6 instanceof GoalTrackingRepository$insert$Anon1) {
            goalTrackingRepository$insert$Anon1 = (GoalTrackingRepository$insert$Anon1) xe6;
            int i2 = goalTrackingRepository$insert$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$insert$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$insert$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$insert$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "insertGoalTrackingDataList: sampleRawList =" + list.size());
                    MFUser currentUser = this.mUserRepository.getCurrentUser();
                    String userId = currentUser != null ? currentUser.getUserId() : null;
                    if (!TextUtils.isEmpty(userId)) {
                        fu3 fu3 = new fu3();
                        for (GoalTrackingData next : list) {
                            String component1 = next.component1();
                            DateTime component2 = next.component2();
                            int component3 = next.component3();
                            Date component4 = next.component4();
                            try {
                                ku3 ku3 = new ku3();
                                ku3.a("id", component1);
                                ku3.a(HardwareLog.COLUMN_DATE, bk4.f(component4));
                                ku3.a("trackedAt", bk4.b(component2));
                                ku3.a("timezoneOffset", hf6.a(component3));
                                fu3.a(ku3);
                            } catch (Exception e) {
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String str2 = TAG;
                                local2.e(str2, "updateGoalSetting exception=" + e);
                                e.printStackTrace();
                            }
                        }
                        ku3 ku32 = new ku3();
                        ku32.a(CloudLogWriter.ITEMS_PARAM, fu3);
                        GoalTrackingRepository$insert$repoResponse$Anon1 goalTrackingRepository$insert$repoResponse$Anon1 = new GoalTrackingRepository$insert$repoResponse$Anon1(this, ku32, (xe6) null);
                        goalTrackingRepository$insert$Anon1.L$0 = this;
                        goalTrackingRepository$insert$Anon1.L$1 = list;
                        goalTrackingRepository$insert$Anon1.L$2 = userId;
                        goalTrackingRepository$insert$Anon1.L$3 = fu3;
                        goalTrackingRepository$insert$Anon1.L$4 = ku32;
                        goalTrackingRepository$insert$Anon1.label = 1;
                        obj = ResponseKt.a(goalTrackingRepository$insert$repoResponse$Anon1, goalTrackingRepository$insert$Anon1);
                        if (obj == a) {
                            return a;
                        }
                        goalTrackingRepository = this;
                    } else {
                        FLogger.INSTANCE.getLocal().d(TAG, "insertGoalTrackingDataList userId is null");
                        return new zo4(600, new ServerError(600, ""), (Throwable) null, (String) null, 8, (qg6) null);
                    }
                } else if (i == 1) {
                    ku3 ku33 = (ku3) goalTrackingRepository$insert$Anon1.L$4;
                    fu3 fu32 = (fu3) goalTrackingRepository$insert$Anon1.L$3;
                    String str3 = (String) goalTrackingRepository$insert$Anon1.L$2;
                    list = (List) goalTrackingRepository$insert$Anon1.L$1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$insert$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    local3.d(str4, "insertGoalTrackingDataList onResponse: response = " + ap4);
                    goalTrackingRepository.updateGoalTrackingPinType(list, 0);
                    return new cp4(list, false, 2, (qg6) null);
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                    String str5 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("insertGoalTrackingDataList Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    sb.append(c != null ? c.getMessage() : null);
                    local4.d(str5, sb.toString());
                    if (zo4.a() == 422) {
                        ArrayList arrayList = new ArrayList();
                        if (!TextUtils.isEmpty(zo4.b())) {
                            try {
                                Object a2 = new Gson().a(((zo4) ap4).b(), new GoalTrackingRepository$insert$type$Anon1().getType());
                                wg6.a(a2, "Gson().fromJson(repoResponse.errorItems, type)");
                                List list2 = ((UpsertApiResponse) a2).get_items();
                                if (true ^ list2.isEmpty()) {
                                    int size = list2.size();
                                    for (int i3 = 0; i3 < size; i3++) {
                                        Integer code = ((ServerError) list2.get(i3)).getCode();
                                        if (code == null) {
                                        }
                                        Integer code2 = ((ServerError) list2.get(i3)).getCode();
                                        if (code2 != null) {
                                            if (code2.intValue() != 409001) {
                                            }
                                            GoalTrackingData goalTrackingData = list.get(i3);
                                            goalTrackingData.setPinType(0);
                                            arrayList.add(goalTrackingData);
                                        }
                                    }
                                }
                            } catch (Exception e2) {
                                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                                String str6 = TAG;
                                local5.e(str6, "insertGoalTrackingDataList ex=" + e2);
                                e2.printStackTrace();
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertListGoalTrackingData(arrayList);
                        }
                        new cp4(list, false, 2, (qg6) null);
                    }
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), zo4.b());
                } else {
                    throw new kc6();
                }
            }
        }
        goalTrackingRepository$insert$Anon1 = new GoalTrackingRepository$insert$Anon1(this, xe6);
        Object obj2 = goalTrackingRepository$insert$Anon1.result;
        Object a3 = ff6.a();
        i = goalTrackingRepository$insert$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }

    @DexIgnore
    public final Object insertFromDevice(List<GoalTrackingData> list, xe6<? super cd6> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "insertFromDevice: goalTrackingDataList = " + list.size());
        this.mGoalTrackingDao.addGoalTrackingRawDataList(list);
        if (!list.isEmpty()) {
            this.mSharedPreferencesManager.j(true);
        }
        Object pushPendingGoalTrackingDataList = pushPendingGoalTrackingDataList(xe6);
        if (pushPendingGoalTrackingDataList == ff6.a()) {
            return pushPendingGoalTrackingDataList;
        }
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0072  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00c9  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002c  */
    public final Object loadGoalTrackingDataList(Date date, Date date2, int i, int i2, xe6<? super ap4<ApiResponse<GoalEvent>>> xe6) {
        GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon1;
        int i3;
        ap4 ap4;
        ap4 ap42;
        Object obj;
        int i4;
        GoalTrackingRepository goalTrackingRepository;
        Date date3;
        int i5;
        String message;
        Date date4 = date;
        Date date5 = date2;
        xe6<? super ap4<ApiResponse<GoalEvent>>> xe62 = xe6;
        if (xe62 instanceof GoalTrackingRepository$loadGoalTrackingDataList$Anon1) {
            goalTrackingRepository$loadGoalTrackingDataList$Anon1 = (GoalTrackingRepository$loadGoalTrackingDataList$Anon1) xe62;
            int i6 = goalTrackingRepository$loadGoalTrackingDataList$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$loadGoalTrackingDataList$Anon1.label = i6 - Integer.MIN_VALUE;
                GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon12 = goalTrackingRepository$loadGoalTrackingDataList$Anon1;
                Object obj2 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.result;
                Object a = ff6.a();
                i3 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.label;
                if (i3 != 0) {
                    nc6.a(obj2);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = TAG;
                    local.d(str, "loadGoalTrackingDataList startDate=" + date4 + ", endDate=" + date5);
                    GoalTrackingRepository$loadGoalTrackingDataList$response$Anon1 goalTrackingRepository$loadGoalTrackingDataList$response$Anon1 = new GoalTrackingRepository$loadGoalTrackingDataList$response$Anon1(this, date, date2, i, (xe6) null);
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$0 = this;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$1 = date4;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$2 = date5;
                    i5 = i;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$0 = i5;
                    int i7 = i2;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$1 = i7;
                    goalTrackingRepository$loadGoalTrackingDataList$Anon12.label = 1;
                    Object a2 = ResponseKt.a(goalTrackingRepository$loadGoalTrackingDataList$response$Anon1, goalTrackingRepository$loadGoalTrackingDataList$Anon12);
                    if (a2 == a) {
                        return a;
                    }
                    i4 = i7;
                    obj2 = a2;
                    date3 = date5;
                    goalTrackingRepository = this;
                } else if (i3 == 1) {
                    int i8 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$1;
                    i5 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$0;
                    date3 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$2;
                    nc6.a(obj2);
                    i4 = i8;
                    date4 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$0;
                } else if (i3 == 2) {
                    List list = (List) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$4;
                    ap42 = (ap4) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$3;
                    int i9 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$1;
                    int i10 = goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$0;
                    Date date6 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$2;
                    Date date7 = (Date) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$1;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$0;
                    try {
                        nc6.a(obj2);
                        obj = obj2;
                        return (ap4) obj;
                    } catch (Exception e) {
                        e = e;
                        ap4 = ap42;
                    }
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj2;
                String str2 = null;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        try {
                            List<GoalEvent> list2 = ((ApiResponse) ((cp4) ap4).a()).get_items();
                            ArrayList arrayList = new ArrayList(rd6.a(list2, 10));
                            for (GoalEvent goalTrackingData : list2) {
                                GoalTrackingData goalTrackingData2 = goalTrackingData.toGoalTrackingData();
                                if (goalTrackingData2 != null) {
                                    arrayList.add(goalTrackingData2);
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertGoalTrackingDataList(yd6.d(arrayList));
                            if (!((ApiResponse) ((cp4) ap4).a()).get_items().isEmpty()) {
                                goalTrackingRepository.mSharedPreferencesManager.j(true);
                            }
                            if (((ApiResponse) ((cp4) ap4).a()).get_range() == null) {
                                return ap4;
                            }
                            Range range = ((ApiResponse) ((cp4) ap4).a()).get_range();
                            if (range == null) {
                                wg6.a();
                                throw null;
                            } else if (!range.isHasNext()) {
                                return ap4;
                            } else {
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$0 = goalTrackingRepository;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$1 = date4;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$2 = date3;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$0 = i5;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.I$1 = i4;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$3 = ap4;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.L$4 = arrayList;
                                goalTrackingRepository$loadGoalTrackingDataList$Anon12.label = 2;
                                obj = goalTrackingRepository.loadGoalTrackingDataList(date4, date3, i5 + i4, i4, goalTrackingRepository$loadGoalTrackingDataList$Anon12);
                                if (obj == a) {
                                    return a;
                                }
                                ap42 = ap4;
                                return (ap4) obj;
                            }
                        } catch (Exception e2) {
                            e = e2;
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.e(str3, "loadGoalTrackingDataList exception=" + e);
                            e.printStackTrace();
                            return ap4;
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadGoalTrackingDataList Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str2 = c2.getUserMessage();
                        }
                    } else {
                        str2 = message;
                    }
                    if (str2 == null) {
                        str2 = "";
                    }
                    sb.append(str2);
                    local3.d(str4, sb.toString());
                }
                return ap4;
            }
        }
        goalTrackingRepository$loadGoalTrackingDataList$Anon1 = new GoalTrackingRepository$loadGoalTrackingDataList$Anon1(this, xe62);
        GoalTrackingRepository$loadGoalTrackingDataList$Anon1 goalTrackingRepository$loadGoalTrackingDataList$Anon122 = goalTrackingRepository$loadGoalTrackingDataList$Anon1;
        Object obj22 = goalTrackingRepository$loadGoalTrackingDataList$Anon122.result;
        Object a3 = ff6.a();
        i3 = goalTrackingRepository$loadGoalTrackingDataList$Anon122.label;
        if (i3 != 0) {
        }
        ap4 = (ap4) obj22;
        String str22 = null;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x003e  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0080  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0136  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final Object loadSummaries(Date date, Date date2, xe6<? super ap4<ApiResponse<GoalDailySummary>>> xe6) {
        GoalTrackingRepository$loadSummaries$Anon1 goalTrackingRepository$loadSummaries$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        ap4 ap4;
        String message;
        if (xe6 instanceof GoalTrackingRepository$loadSummaries$Anon1) {
            goalTrackingRepository$loadSummaries$Anon1 = (GoalTrackingRepository$loadSummaries$Anon1) xe6;
            int i2 = goalTrackingRepository$loadSummaries$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$loadSummaries$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$loadSummaries$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$loadSummaries$Anon1.label;
                String str = null;
                if (i != 0) {
                    nc6.a(obj);
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local.d(str2, "loadSummaries startDate=" + date + ", endDate=" + date2);
                    GoalTrackingRepository$loadSummaries$response$Anon1 goalTrackingRepository$loadSummaries$response$Anon1 = new GoalTrackingRepository$loadSummaries$response$Anon1(this, date, date2, (xe6) null);
                    goalTrackingRepository$loadSummaries$Anon1.L$0 = this;
                    goalTrackingRepository$loadSummaries$Anon1.L$1 = date;
                    goalTrackingRepository$loadSummaries$Anon1.L$2 = date2;
                    goalTrackingRepository$loadSummaries$Anon1.label = 1;
                    obj = ResponseKt.a(goalTrackingRepository$loadSummaries$response$Anon1, goalTrackingRepository$loadSummaries$Anon1);
                    if (obj == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                } else if (i == 1) {
                    Date date3 = (Date) goalTrackingRepository$loadSummaries$Anon1.L$2;
                    Date date4 = (Date) goalTrackingRepository$loadSummaries$Anon1.L$1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$loadSummaries$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    cp4 cp4 = (cp4) ap4;
                    if (cp4.a() != null && !cp4.b()) {
                        try {
                            List<GoalDailySummary> list = ((ApiResponse) ((cp4) ap4).a()).get_items();
                            ArrayList arrayList = new ArrayList(rd6.a(list, 10));
                            for (GoalDailySummary goalTrackingSummary : list) {
                                GoalTrackingSummary goalTrackingSummary2 = goalTrackingSummary.toGoalTrackingSummary();
                                if (goalTrackingSummary2 != null) {
                                    arrayList.add(goalTrackingSummary2);
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            goalTrackingRepository.mGoalTrackingDao.upsertGoalTrackingSummaries(yd6.d(arrayList));
                            List list2 = ((ApiResponse) ((cp4) ap4).a()).get_items();
                            ArrayList arrayList2 = new ArrayList();
                            for (Object next : list2) {
                                if (hf6.a(((GoalDailySummary) next).getMTotalTracked() > 0).booleanValue()) {
                                    arrayList2.add(next);
                                }
                            }
                            if (!arrayList2.isEmpty()) {
                                goalTrackingRepository.mSharedPreferencesManager.j(true);
                            }
                        } catch (Exception e) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str3 = TAG;
                            local2.e(str3, "loadSummaries exception=" + e);
                            e.printStackTrace();
                        }
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("loadSummaries Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c = zo4.c();
                    if (c == null || (message = c.getMessage()) == null) {
                        ServerError c2 = zo4.c();
                        if (c2 != null) {
                            str = c2.getUserMessage();
                        }
                    } else {
                        str = message;
                    }
                    if (str == null) {
                        str = "";
                    }
                    sb.append(str);
                    local3.d(str4, sb.toString());
                }
                return ap4;
            }
        }
        goalTrackingRepository$loadSummaries$Anon1 = new GoalTrackingRepository$loadSummaries$Anon1(this, xe6);
        Object obj2 = goalTrackingRepository$loadSummaries$Anon1.result;
        Object a2 = ff6.a();
        i = goalTrackingRepository$loadSummaries$Anon1.label;
        String str5 = null;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
        return ap4;
    }

    @DexIgnore
    public final Object pushPendingGoalTrackingDataList(PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, xe6<? super cd6> xe6) {
        cd6 cd6;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "pushPendingGoalTrackingDataList mGoalTrackingDatabase=" + this.mGoalTrackingDatabase);
        List<GoalTrackingData> pendingGoalTrackingDataList = this.mGoalTrackingDao.getPendingGoalTrackingDataList();
        if (pendingGoalTrackingDataList.size() > 0) {
            Object saveGoalTrackingDataListToServer = saveGoalTrackingDataListToServer(pendingGoalTrackingDataList, pushPendingGoalTrackingDataListCallback, xe6);
            if (saveGoalTrackingDataListToServer == ff6.a()) {
                return saveGoalTrackingDataListToServer;
            }
        } else {
            if (pushPendingGoalTrackingDataListCallback != null) {
                pushPendingGoalTrackingDataListCallback.onFail(MFNetworkReturnCode.NOT_FOUND);
                cd6 = cd6.a;
            } else {
                cd6 = null;
            }
            if (cd6 == ff6.a()) {
                return cd6;
            }
        }
        return cd6.a;
    }

    @DexIgnore
    public final void removePagingListener() {
        for (GoalTrackingDataSourceFactory localDataSource : this.mSourceDataFactoryList) {
            GoalTrackingDataLocalDataSource localDataSource2 = localDataSource.getLocalDataSource();
            if (localDataSource2 != null) {
                localDataSource2.removePagingObserver();
            }
        }
        this.mSourceDataFactoryList.clear();
        for (GoalTrackingSummaryDataSourceFactory localDataSource3 : this.mSourceFactoryList) {
            GoalTrackingSummaryLocalDataSource localDataSource4 = localDataSource3.getLocalDataSource();
            if (localDataSource4 != null) {
                localDataSource4.removePagingObserver();
            }
        }
        this.mSourceFactoryList.clear();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x00a5  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0156  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x01b2  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x01d4  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0254  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x025a  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0262  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0032  */
    public final /* synthetic */ Object saveGoalTrackingDataListToServer(List<GoalTrackingData> list, PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback, xe6<? super cd6> xe6) {
        GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
        int i;
        PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback2;
        int i2;
        List list2;
        List<GoalTrackingData> list3;
        GoalTrackingRepository goalTrackingRepository;
        String str;
        GoalTrackingRepository goalTrackingRepository2;
        List<GoalTrackingData> list4;
        PushPendingGoalTrackingDataListCallback pushPendingGoalTrackingDataListCallback3;
        int i3;
        List list5;
        Object obj;
        int i4;
        ap4 ap4;
        List<GoalTrackingData> list6;
        List<T> list7;
        List<T> list8;
        String str2;
        int i5;
        List<T> list9;
        ap4 ap42;
        GoalTrackingRepository goalTrackingRepository3;
        GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
        xe6<? super cd6> xe62 = xe6;
        if (xe62 instanceof GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1) {
            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = (GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1) xe62;
            int i6 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
            if ((i6 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = i6 - Integer.MIN_VALUE;
                Object obj2 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
                String str3 = "startIndex=";
                if (i != 0) {
                    nc6.a(obj2);
                    list4 = list;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback;
                    list5 = new ArrayList();
                    i3 = 0;
                    goalTrackingRepository2 = this;
                } else if (i == 1) {
                    list8 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$6;
                    list9 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$5;
                    list6 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$4;
                    i5 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$1;
                    list5 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$3;
                    i3 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$0;
                    pushPendingGoalTrackingDataListCallback3 = (PushPendingGoalTrackingDataListCallback) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$2;
                    list4 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$1;
                    goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$0;
                    nc6.a(obj2);
                    str2 = str3;
                    ap42 = (ap4) obj2;
                    if (!(ap42 instanceof cp4)) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
                        String str4 = TAG;
                        goalTrackingRepository3 = goalTrackingRepository2;
                        local.d(str4, "saveGoalTrackingDataListToServer success, bravo!!! startIndex=" + i3 + " endIndex=" + i5);
                        Object a2 = ((cp4) ap42).a();
                        if (a2 != null) {
                            list5.addAll((List) a2);
                        }
                        wg6.a();
                        throw null;
                    }
                    goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1;
                    goalTrackingRepository3 = goalTrackingRepository2;
                    if (ap42 instanceof zo4) {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str5 = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append("saveGoalTrackingDataListToServer failed, errorCode=");
                        sb.append(((zo4) ap42).a());
                        sb.append(' ');
                        str = str2;
                        sb.append(str);
                        sb.append(i3);
                        sb.append(" endIndex=");
                        sb.append(i5);
                        local2.d(str5, sb.toString());
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                        list7 = list9;
                        i4 = i5;
                        list2 = list5;
                        i2 = i3;
                        goalTrackingRepository = goalTrackingRepository3;
                        pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                        List<GoalTrackingData> list10 = list6;
                        list3 = list4;
                        if (!list8.isEmpty()) {
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$0 = goalTrackingRepository;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$1 = list3;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$2 = pushPendingGoalTrackingDataListCallback2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$0 = i2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$3 = list2;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$1 = i4;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$4 = list10;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$5 = list7;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$6 = list8;
                            goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = 2;
                            obj = goalTrackingRepository.delete(list8, goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1);
                            if (obj == a) {
                                return a;
                            }
                            ap4 = (ap4) obj;
                            if (ap4 instanceof cp4) {
                            }
                        }
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                        }
                        if (!(!list2.isEmpty())) {
                        }
                        return cd6.a;
                    }
                    str = str2;
                    goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                    list7 = list9;
                    i4 = i5;
                    list2 = list5;
                    i2 = i3;
                    goalTrackingRepository = goalTrackingRepository3;
                    pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                    List<GoalTrackingData> list102 = list6;
                    list3 = list4;
                    if (!list8.isEmpty()) {
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                    return cd6.a;
                } else if (i == 2) {
                    List list11 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$6;
                    List list12 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$5;
                    List list13 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$4;
                    int i7 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$1;
                    list2 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$3;
                    i2 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$0;
                    pushPendingGoalTrackingDataListCallback2 = (PushPendingGoalTrackingDataListCallback) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$2;
                    list3 = (List) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$1;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$0;
                    nc6.a(obj2);
                    i4 = i7;
                    obj = obj2;
                    str = str3;
                    ap4 = (ap4) obj;
                    if (ap4 instanceof cp4) {
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String str6 = TAG;
                        local3.d(str6, "saveGoalTrackingDataListToServer success, bravo!!! startIndex=" + i2 + " endIndex=" + i4);
                        Object a3 = ((cp4) ap4).a();
                        if (a3 != null) {
                            list2.addAll((List) a3);
                        }
                        wg6.a();
                        throw null;
                    } else if (ap4 instanceof zo4) {
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String str7 = TAG;
                        local4.d(str7, "saveGoalTrackingDataListToServer failed, errorCode=" + ((zo4) ap4).a() + ' ' + str + i2 + " endIndex=" + i4);
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                            str3 = str;
                            list5 = list2;
                        }
                        if (!(!list2.isEmpty())) {
                            if (pushPendingGoalTrackingDataListCallback3 != null) {
                                pushPendingGoalTrackingDataListCallback3.onSuccess(list2);
                            }
                        } else if (pushPendingGoalTrackingDataListCallback3 != null) {
                            pushPendingGoalTrackingDataListCallback3.onFail(600);
                        }
                        return cd6.a;
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                if (i3 < list4.size()) {
                    int i8 = i3 + 100;
                    if (i8 > list4.size()) {
                        i8 = list4.size();
                    }
                    i5 = i8;
                    ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                    String str8 = TAG;
                    local5.d(str8, "saveGoalTrackingDataListToServer startIndex=" + i3 + " endIndex=" + i5);
                    list6 = list4.subList(i3, i5);
                    list9 = aj6.g(aj6.a(yd6.b(list6), GoalTrackingRepository$saveGoalTrackingDataListToServer$subAddedGoalTrackingDataListList$Anon1.INSTANCE));
                    list8 = aj6.g(aj6.a(yd6.b(list6), GoalTrackingRepository$saveGoalTrackingDataListToServer$subDeletedGoalTrackingDataListList$Anon1.INSTANCE));
                    str2 = str3;
                    if (!list9.isEmpty()) {
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$0 = goalTrackingRepository2;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$1 = list4;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$2 = pushPendingGoalTrackingDataListCallback3;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$0 = i3;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$3 = list5;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.I$1 = i5;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$4 = list6;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$5 = list9;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.L$6 = list8;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label = 1;
                        obj2 = goalTrackingRepository2.insert(list9, goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1);
                        if (obj2 == a) {
                            return a;
                        }
                        ap42 = (ap4) obj2;
                        if (!(ap42 instanceof cp4)) {
                        }
                        str = str2;
                        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon12;
                        list7 = list9;
                        i4 = i5;
                        list2 = list5;
                        i2 = i3;
                        goalTrackingRepository = goalTrackingRepository3;
                        pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                        List<GoalTrackingData> list1022 = list6;
                        list3 = list4;
                        if (!list8.isEmpty()) {
                        }
                        goalTrackingRepository2 = goalTrackingRepository;
                        list4 = list3;
                        pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                        i3 = i2 + 100;
                        if (i3 >= list4.size()) {
                        }
                        if (!(!list2.isEmpty())) {
                        }
                    }
                    str = str2;
                    list7 = list9;
                    goalTrackingRepository = goalTrackingRepository2;
                    i4 = i5;
                    list2 = list5;
                    i2 = i3;
                    pushPendingGoalTrackingDataListCallback2 = pushPendingGoalTrackingDataListCallback3;
                    List<GoalTrackingData> list10222 = list6;
                    list3 = list4;
                    if (!list8.isEmpty()) {
                    }
                    goalTrackingRepository2 = goalTrackingRepository;
                    list4 = list3;
                    pushPendingGoalTrackingDataListCallback3 = pushPendingGoalTrackingDataListCallback2;
                    i3 = i2 + 100;
                    if (i3 >= list4.size()) {
                    }
                    if (!(!list2.isEmpty())) {
                    }
                }
                return cd6.a;
            }
        }
        goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1 = new GoalTrackingRepository$saveGoalTrackingDataListToServer$Anon1(this, xe62);
        Object obj22 = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.result;
        Object a4 = ff6.a();
        i = goalTrackingRepository$saveGoalTrackingDataListToServer$Anon1.label;
        String str32 = "startIndex=";
        if (i != 0) {
        }
        if (i3 < list4.size()) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final void saveSettingToDB(GoalSetting goalSetting) {
        wg6.b(goalSetting, "goalSetting");
        this.mGoalTrackingDao.upsertGoalSettings(goalSetting);
        GoalTrackingSummary goalTrackingSummary = this.mGoalTrackingDao.getGoalTrackingSummary(new Date());
        if (goalTrackingSummary == null) {
            goalTrackingSummary = new GoalTrackingSummary(new Date(), 0, goalSetting.getCurrentTarget(), new Date().getTime(), new Date().getTime());
        } else {
            goalTrackingSummary.setGoalTarget(goalSetting.getCurrentTarget());
        }
        this.mGoalTrackingDao.upsertGoalTrackingSummary(goalTrackingSummary);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00d3  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0026  */
    public final Object updateGoalSetting(GoalSetting goalSetting, UpdateGoalSettingCallback updateGoalSettingCallback, xe6<? super cd6> xe6) {
        GoalTrackingRepository$updateGoalSetting$Anon1 goalTrackingRepository$updateGoalSetting$Anon1;
        int i;
        GoalTrackingRepository goalTrackingRepository;
        ku3 ku3;
        ap4 ap4;
        if (xe6 instanceof GoalTrackingRepository$updateGoalSetting$Anon1) {
            goalTrackingRepository$updateGoalSetting$Anon1 = (GoalTrackingRepository$updateGoalSetting$Anon1) xe6;
            int i2 = goalTrackingRepository$updateGoalSetting$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                goalTrackingRepository$updateGoalSetting$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = goalTrackingRepository$updateGoalSetting$Anon1.result;
                Object a = ff6.a();
                i = goalTrackingRepository$updateGoalSetting$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    ku3 ku32 = new ku3();
                    try {
                        ku32.a("currentTarget", hf6.a(goalSetting.getCurrentTarget()));
                        TimeZone timeZone = TimeZone.getDefault();
                        wg6.a((Object) timeZone, "TimeZone.getDefault()");
                        ku32.a("timezoneOffset", hf6.a(timeZone.getRawOffset() / 1000));
                    } catch (Exception e) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = TAG;
                        local.e(str, "updateGoalSetting exception=" + e);
                        e.printStackTrace();
                    }
                    GoalTrackingRepository$updateGoalSetting$repoResponse$Anon1 goalTrackingRepository$updateGoalSetting$repoResponse$Anon1 = new GoalTrackingRepository$updateGoalSetting$repoResponse$Anon1(this, ku32, (xe6) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$0 = this;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$3 = ku32;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 1;
                    Object a2 = ResponseKt.a(goalTrackingRepository$updateGoalSetting$repoResponse$Anon1, goalTrackingRepository$updateGoalSetting$Anon1);
                    if (a2 == a) {
                        return a;
                    }
                    goalTrackingRepository = this;
                    Object obj2 = a2;
                    ku3 = ku32;
                    obj = obj2;
                } else if (i == 1) {
                    updateGoalSettingCallback = (UpdateGoalSettingCallback) goalTrackingRepository$updateGoalSetting$Anon1.L$2;
                    goalTrackingRepository = (GoalTrackingRepository) goalTrackingRepository$updateGoalSetting$Anon1.L$0;
                    nc6.a(obj);
                    GoalSetting goalSetting2 = (GoalSetting) goalTrackingRepository$updateGoalSetting$Anon1.L$1;
                    ku3 = (ku3) goalTrackingRepository$updateGoalSetting$Anon1.L$3;
                    goalSetting = goalSetting2;
                } else if (i == 2 || i == 3) {
                    ap4 ap42 = (ap4) goalTrackingRepository$updateGoalSetting$Anon1.L$4;
                    ku3 ku33 = (ku3) goalTrackingRepository$updateGoalSetting$Anon1.L$3;
                    UpdateGoalSettingCallback updateGoalSettingCallback2 = (UpdateGoalSettingCallback) goalTrackingRepository$updateGoalSetting$Anon1.L$2;
                    GoalSetting goalSetting3 = (GoalSetting) goalTrackingRepository$updateGoalSetting$Anon1.L$1;
                    GoalTrackingRepository goalTrackingRepository2 = (GoalTrackingRepository) goalTrackingRepository$updateGoalSetting$Anon1.L$0;
                    nc6.a(obj);
                    return cd6.a;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = TAG;
                    local2.d(str2, "updateGoalSetting onResponse: response = " + ap4);
                    goalTrackingRepository.saveSettingToDB(goalSetting);
                    cn6 c = zl6.c();
                    GoalTrackingRepository$updateGoalSetting$Anon2 goalTrackingRepository$updateGoalSetting$Anon2 = new GoalTrackingRepository$updateGoalSetting$Anon2(updateGoalSettingCallback, goalSetting, (xe6) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$0 = goalTrackingRepository;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$3 = ku3;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$4 = ap4;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 2;
                    if (gk6.a(c, goalTrackingRepository$updateGoalSetting$Anon2, goalTrackingRepository$updateGoalSetting$Anon1) == a) {
                        return a;
                    }
                } else if (ap4 instanceof zo4) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = TAG;
                    StringBuilder sb = new StringBuilder();
                    sb.append("updateGoalSetting Failure code=");
                    zo4 zo4 = (zo4) ap4;
                    sb.append(zo4.a());
                    sb.append(" message=");
                    ServerError c2 = zo4.c();
                    sb.append(c2 != null ? c2.getMessage() : null);
                    local3.d(str3, sb.toString());
                    cn6 c3 = zl6.c();
                    GoalTrackingRepository$updateGoalSetting$Anon3 goalTrackingRepository$updateGoalSetting$Anon3 = new GoalTrackingRepository$updateGoalSetting$Anon3(updateGoalSettingCallback, ap4, (xe6) null);
                    goalTrackingRepository$updateGoalSetting$Anon1.L$0 = goalTrackingRepository;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$1 = goalSetting;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$2 = updateGoalSettingCallback;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$3 = ku3;
                    goalTrackingRepository$updateGoalSetting$Anon1.L$4 = ap4;
                    goalTrackingRepository$updateGoalSetting$Anon1.label = 3;
                    if (gk6.a(c3, goalTrackingRepository$updateGoalSetting$Anon3, goalTrackingRepository$updateGoalSetting$Anon1) == a) {
                        return a;
                    }
                }
                return cd6.a;
            }
        }
        goalTrackingRepository$updateGoalSetting$Anon1 = new GoalTrackingRepository$updateGoalSetting$Anon1(this, xe6);
        Object obj3 = goalTrackingRepository$updateGoalSetting$Anon1.result;
        Object a3 = ff6.a();
        i = goalTrackingRepository$updateGoalSetting$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj3;
        if (!(ap4 instanceof cp4)) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final /* synthetic */ Object pushPendingGoalTrackingDataList(xe6<? super cd6> xe6) {
        FLogger.INSTANCE.getLocal().d(TAG, "pushPendingGoalTrackingDataList");
        Object pushPendingGoalTrackingDataList = pushPendingGoalTrackingDataList(new GoalTrackingRepository$pushPendingGoalTrackingDataList$Anon3(this), xe6);
        if (pushPendingGoalTrackingDataList == ff6.a()) {
            return pushPendingGoalTrackingDataList;
        }
        return cd6.a;
    }
}
