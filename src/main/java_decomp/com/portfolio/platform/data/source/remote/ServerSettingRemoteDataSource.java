package com.portfolio.platform.data.source.remote;

import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerSettingRemoteDataSource implements ServerSettingDataSource {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ApiServiceV2 apiService;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final String getTAG$app_fossilRelease() {
            return ServerSettingRemoteDataSource.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = ServerSettingRemoteDataSource.class.getSimpleName();
        wg6.a((Object) simpleName, "ServerSettingRemoteDataS\u2026ce::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public ServerSettingRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "apiService");
        this.apiService = apiServiceV2;
    }

    @DexIgnore
    public void addOrUpdateServerSetting(ServerSetting serverSetting) {
    }

    @DexIgnore
    public void addOrUpdateServerSettingList(List<ServerSetting> list) {
    }

    @DexIgnore
    public void clearData() {
    }

    @DexIgnore
    public ServerSetting getServerSettingByKey(String str) {
        wg6.b(str, "key");
        return null;
    }

    @DexIgnore
    public void getServerSettingList(ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList) {
        wg6.b(onGetServerSettingList, Constants.CALLBACK);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this, onGetServerSettingList, (xe6) null), 3, (Object) null);
    }
}
