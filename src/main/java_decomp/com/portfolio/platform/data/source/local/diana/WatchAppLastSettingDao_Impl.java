package com.portfolio.platform.data.source.local.diana;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;
import com.portfolio.platform.data.model.diana.WatchAppLastSetting;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppLastSettingDao_Impl implements WatchAppLastSettingDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<WatchAppLastSetting> __insertionAdapterOfWatchAppLastSetting;
    @DexIgnore
    public /* final */ vh __preparedStmtOfCleanUp;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteWatchAppLastSettingById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<WatchAppLastSetting> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `watchAppLastSetting` (`watchAppId`,`updatedAt`,`setting`) VALUES (?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, WatchAppLastSetting watchAppLastSetting) {
            if (watchAppLastSetting.getWatchAppId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, watchAppLastSetting.getWatchAppId());
            }
            if (watchAppLastSetting.getUpdatedAt() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, watchAppLastSetting.getUpdatedAt());
            }
            if (watchAppLastSetting.getSetting() == null) {
                miVar.a(3);
            } else {
                miVar.a(3, watchAppLastSetting.getSetting());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting WHERE watchAppId=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM watchAppLastSetting";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<WatchAppLastSetting>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<WatchAppLastSetting> call() throws Exception {
            Cursor a = bi.a(WatchAppLastSettingDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "watchAppId");
                int b2 = ai.b(a, "updatedAt");
                int b3 = ai.b(a, MicroAppSetting.SETTING);
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    arrayList.add(new WatchAppLastSetting(a.getString(b), a.getString(b2), a.getString(b3)));
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public WatchAppLastSettingDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfWatchAppLastSetting = new Anon1(ohVar);
        this.__preparedStmtOfDeleteWatchAppLastSettingById = new Anon2(ohVar);
        this.__preparedStmtOfCleanUp = new Anon3(ohVar);
    }

    @DexIgnore
    public void cleanUp() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfCleanUp.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfCleanUp.release(acquire);
        }
    }

    @DexIgnore
    public void deleteWatchAppLastSettingById(String str) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteWatchAppLastSettingById.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteWatchAppLastSettingById.release(acquire);
        }
    }

    @DexIgnore
    public List<WatchAppLastSetting> getAllWatchAppLastSetting() {
        rh b = rh.b("SELECT * FROM watchAppLastSetting", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "watchAppId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                arrayList.add(new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4)));
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<WatchAppLastSetting>> getAllWatchAppLastSettingAsLiveData() {
        return this.__db.getInvalidationTracker().a(new String[]{"watchAppLastSetting"}, false, new Anon4(rh.b("SELECT * FROM watchAppLastSetting", 0)));
    }

    @DexIgnore
    public WatchAppLastSetting getWatchAppLastSetting(String str) {
        rh b = rh.b("SELECT * FROM watchAppLastSetting WHERE watchAppId=? ", 1);
        if (str == null) {
            b.a(1);
        } else {
            b.a(1, str);
        }
        this.__db.assertNotSuspendingTransaction();
        WatchAppLastSetting watchAppLastSetting = null;
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "watchAppId");
            int b3 = ai.b(a, "updatedAt");
            int b4 = ai.b(a, MicroAppSetting.SETTING);
            if (a.moveToFirst()) {
                watchAppLastSetting = new WatchAppLastSetting(a.getString(b2), a.getString(b3), a.getString(b4));
            }
            return watchAppLastSetting;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void upsertWatchAppLastSetting(WatchAppLastSetting watchAppLastSetting) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfWatchAppLastSetting.insert(watchAppLastSetting);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
