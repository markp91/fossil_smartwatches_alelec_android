package com.portfolio.platform.data.source;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory implements Factory<NotificationsDataSource> {
    @DexIgnore
    public /* final */ NotificationsRepositoryModule module;

    @DexIgnore
    public NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory(NotificationsRepositoryModule notificationsRepositoryModule) {
        this.module = notificationsRepositoryModule;
    }

    @DexIgnore
    public static NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory create(NotificationsRepositoryModule notificationsRepositoryModule) {
        return new NotificationsRepositoryModule_ProvideLocalNotificationsDataSourceFactory(notificationsRepositoryModule);
    }

    @DexIgnore
    public static NotificationsDataSource provideInstance(NotificationsRepositoryModule notificationsRepositoryModule) {
        return proxyProvideLocalNotificationsDataSource(notificationsRepositoryModule);
    }

    @DexIgnore
    public static NotificationsDataSource proxyProvideLocalNotificationsDataSource(NotificationsRepositoryModule notificationsRepositoryModule) {
        NotificationsDataSource provideLocalNotificationsDataSource = notificationsRepositoryModule.provideLocalNotificationsDataSource();
        z76.a(provideLocalNotificationsDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideLocalNotificationsDataSource;
    }

    @DexIgnore
    public NotificationsDataSource get() {
        return provideInstance(this.module);
    }
}
