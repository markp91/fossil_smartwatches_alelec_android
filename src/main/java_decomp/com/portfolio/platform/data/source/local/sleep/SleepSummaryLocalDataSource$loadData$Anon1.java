package com.portfolio.platform.data.source.local.sleep;

import com.fossil.af6;
import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rl6;
import com.fossil.sf6;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource$loadData$1", f = "SleepSummaryLocalDataSource.kt", l = {179, 179}, m = "invokeSuspend")
public final class SleepSummaryLocalDataSource$loadData$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ vk4.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ vk4.d $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public Object L$7;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepSummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SleepSummaryLocalDataSource$loadData$Anon1(SleepSummaryLocalDataSource sleepSummaryLocalDataSource, Date date, Date date2, vk4.d dVar, vk4.b.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = sleepSummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = dVar;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SleepSummaryLocalDataSource$loadData$Anon1 sleepSummaryLocalDataSource$loadData$Anon1 = new SleepSummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, xe6);
        sleepSummaryLocalDataSource$loadData$Anon1.p$ = (il6) obj;
        return sleepSummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SleepSummaryLocalDataSource$loadData$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        SleepSummaryLocalDataSource sleepSummaryLocalDataSource;
        Object obj2;
        vk4.d dVar;
        ap4 ap4;
        rl6 rl6;
        List<FitnessDataWrapper> list;
        lc6<Date, Date> lc6;
        il6 il6;
        rl6 rl62;
        Object obj3;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d(SleepSummaryLocalDataSource.TAG, "loadData start=" + this.$startDate + ", end=" + this.$endDate);
            list = this.this$0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            lc6 = FitnessDataWrapperKt.calculateRangeDownload(list, this.$startDate, this.$endDate);
            if (lc6 != null) {
                il6 il63 = il62;
                rl6 a2 = ik6.a(il63, (af6) null, (ll6) null, new SleepSummaryLocalDataSource$loadData$Anon1$summariesDeferred$Anon1_Level2(this, lc6, (xe6) null), 3, (Object) null);
                rl62 = ik6.a(il63, (af6) null, (ll6) null, new SleepSummaryLocalDataSource$loadData$Anon1$sleepSessionsDeferred$Anon1_Level2(this, lc6, (xe6) null), 3, (Object) null);
                sleepSummaryLocalDataSource = this.this$0;
                vk4.d dVar2 = this.$requestType;
                this.L$0 = il62;
                this.L$1 = list;
                this.L$2 = lc6;
                this.L$3 = a2;
                this.L$4 = rl62;
                this.L$5 = sleepSummaryLocalDataSource;
                this.L$6 = dVar2;
                this.label = 1;
                obj3 = a2.a(this);
                if (obj3 == a) {
                    return a;
                }
                rl6 rl63 = a2;
                il6 = il62;
                dVar = dVar2;
                rl6 = rl63;
            } else {
                this.$helperCallback.a();
                return cd6.a;
            }
        } else if (i == 1) {
            dVar = (vk4.d) this.L$6;
            rl62 = (rl6) this.L$4;
            lc6 = (lc6) this.L$2;
            list = (List) this.L$1;
            nc6.a(obj);
            rl6 = (rl6) this.L$3;
            il6 = (il6) this.L$0;
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$5;
            obj3 = obj;
        } else if (i == 2) {
            ap4 = (ap4) this.L$7;
            dVar = (vk4.d) this.L$6;
            rl6 rl64 = (rl6) this.L$4;
            rl6 rl65 = (rl6) this.L$3;
            lc6 lc62 = (lc6) this.L$2;
            List list2 = (List) this.L$1;
            il6 il64 = (il6) this.L$0;
            nc6.a(obj);
            sleepSummaryLocalDataSource = (SleepSummaryLocalDataSource) this.L$5;
            obj2 = obj;
            sleepSummaryLocalDataSource.combineData(dVar, ap4, (ap4) obj2, this.$helperCallback);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap42 = (ap4) obj3;
        this.L$0 = il6;
        this.L$1 = list;
        this.L$2 = lc6;
        this.L$3 = rl6;
        this.L$4 = rl62;
        this.L$5 = sleepSummaryLocalDataSource;
        this.L$6 = dVar;
        this.L$7 = ap42;
        this.label = 2;
        obj2 = rl62.a(this);
        if (obj2 == a) {
            return a;
        }
        ap4 = ap42;
        sleepSummaryLocalDataSource.combineData(dVar, ap4, (ap4) obj2, this.$helperCallback);
        return cd6.a;
    }
}
