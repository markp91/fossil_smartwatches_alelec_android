package com.portfolio.platform.data.source.local.diana.workout;

import androidx.lifecycle.MutableLiveData;
import com.fossil.u04;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.WorkoutSessionRepository;
import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionDataSourceFactory extends xe.b<Long, WorkoutSession> {
    @DexIgnore
    public /* final */ u04 appExecutors;
    @DexIgnore
    public /* final */ Date currentDate;
    @DexIgnore
    public /* final */ FitnessDataDao fitnessDataDao;
    @DexIgnore
    public /* final */ vk4.a listener;
    @DexIgnore
    public WorkoutSessionLocalDataSource localDataSource;
    @DexIgnore
    public /* final */ MutableLiveData<WorkoutSessionLocalDataSource> sourceLiveData; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ WorkoutDao workoutDao;
    @DexIgnore
    public /* final */ FitnessDatabase workoutDatabase;
    @DexIgnore
    public /* final */ WorkoutSessionRepository workoutSessionRepository;

    @DexIgnore
    public WorkoutSessionDataSourceFactory(WorkoutSessionRepository workoutSessionRepository2, FitnessDataDao fitnessDataDao2, WorkoutDao workoutDao2, FitnessDatabase fitnessDatabase, Date date, u04 u04, vk4.a aVar) {
        wg6.b(workoutSessionRepository2, "workoutSessionRepository");
        wg6.b(fitnessDataDao2, "fitnessDataDao");
        wg6.b(workoutDao2, "workoutDao");
        wg6.b(fitnessDatabase, "workoutDatabase");
        wg6.b(date, "currentDate");
        wg6.b(u04, "appExecutors");
        wg6.b(aVar, "listener");
        this.workoutSessionRepository = workoutSessionRepository2;
        this.fitnessDataDao = fitnessDataDao2;
        this.workoutDao = workoutDao2;
        this.workoutDatabase = fitnessDatabase;
        this.currentDate = date;
        this.appExecutors = u04;
        this.listener = aVar;
    }

    @DexIgnore
    public xe<Long, WorkoutSession> create() {
        this.localDataSource = new WorkoutSessionLocalDataSource(this.workoutSessionRepository, this.fitnessDataDao, this.workoutDao, this.workoutDatabase, this.currentDate, this.appExecutors, this.listener);
        this.sourceLiveData.a(this.localDataSource);
        WorkoutSessionLocalDataSource workoutSessionLocalDataSource = this.localDataSource;
        if (workoutSessionLocalDataSource != null) {
            return workoutSessionLocalDataSource;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public final WorkoutSessionLocalDataSource getLocalDataSource() {
        return this.localDataSource;
    }

    @DexIgnore
    public final MutableLiveData<WorkoutSessionLocalDataSource> getSourceLiveData() {
        return this.sourceLiveData;
    }

    @DexIgnore
    public final void setLocalDataSource(WorkoutSessionLocalDataSource workoutSessionLocalDataSource) {
        this.localDataSource = workoutSessionLocalDataSource;
    }
}
