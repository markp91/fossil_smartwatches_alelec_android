package com.portfolio.platform.data.source;

import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.LocalFile;
import com.portfolio.platform.manager.FileDownloadManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class FileRepository$downloadPendingFile$$inlined$forEach$lambda$Anon1 implements FileDownloadManager.c {
    @DexIgnore
    public /* final */ /* synthetic */ LocalFile $it;
    @DexIgnore
    public /* final */ /* synthetic */ FileRepository this$0;

    @DexIgnore
    public FileRepository$downloadPendingFile$$inlined$forEach$lambda$Anon1(LocalFile localFile, FileRepository fileRepository) {
        this.$it = localFile;
        this.this$0 = fileRepository;
    }

    @DexIgnore
    public void onComplete(boolean z, LocalFile localFile) {
        wg6.b(localFile, "file");
        if (z) {
            this.this$0.mFileDao.upsertLocalFile(localFile);
            return;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = FileRepository.Companion.getTAG();
        local.d(tag, "downloadPendingFile with remoteUrl " + this.$it.getRemoteUrl() + " false");
    }
}
