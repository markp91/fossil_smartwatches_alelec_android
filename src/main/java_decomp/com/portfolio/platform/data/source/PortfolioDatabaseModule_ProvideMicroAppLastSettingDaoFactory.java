package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.hybrid.microapp.HybridCustomizeDatabase;
import com.portfolio.platform.data.source.local.hybrid.microapp.MicroAppLastSettingDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory implements Factory<MicroAppLastSettingDao> {
    @DexIgnore
    public /* final */ Provider<HybridCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideMicroAppLastSettingDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static MicroAppLastSettingDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<HybridCustomizeDatabase> provider) {
        return proxyProvideMicroAppLastSettingDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static MicroAppLastSettingDao proxyProvideMicroAppLastSettingDao(PortfolioDatabaseModule portfolioDatabaseModule, HybridCustomizeDatabase hybridCustomizeDatabase) {
        MicroAppLastSettingDao provideMicroAppLastSettingDao = portfolioDatabaseModule.provideMicroAppLastSettingDao(hybridCustomizeDatabase);
        z76.a(provideMicroAppLastSettingDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppLastSettingDao;
    }

    @DexIgnore
    public MicroAppLastSettingDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
