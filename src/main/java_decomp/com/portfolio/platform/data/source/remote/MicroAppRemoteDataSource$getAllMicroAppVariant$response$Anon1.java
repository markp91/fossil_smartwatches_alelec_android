package com.portfolio.platform.data.source.remote;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.room.microapp.MicroAppVariant;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource$getAllMicroAppVariant$response$1", f = "MicroAppRemoteDataSource.kt", l = {40}, m = "invokeSuspend")
public final class MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<MicroAppVariant>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $majorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $minorNumber;
    @DexIgnore
    public /* final */ /* synthetic */ String $serial;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, String str, String str2, String str3, xe6 xe6) {
        super(1, xe6);
        this.this$0 = microAppRemoteDataSource;
        this.$serial = str;
        this.$majorNumber = str2;
        this.$minorNumber = str3;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1(this.this$0, this.$serial, this.$majorNumber, this.$minorNumber, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((MicroAppRemoteDataSource$getAllMicroAppVariant$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            String str = this.$serial;
            String str2 = this.$majorNumber;
            String str3 = this.$minorNumber;
            this.label = 1;
            obj = access$getMApiServiceV2$p.getAllMicroAppVariant(str, str2, str3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
