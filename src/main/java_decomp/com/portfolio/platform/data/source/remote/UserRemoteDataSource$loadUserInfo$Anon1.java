package com.portfolio.platform.data.source.remote;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource", f = "UserRemoteDataSource.kt", l = {122}, m = "loadUserInfo")
public final class UserRemoteDataSource$loadUserInfo$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$loadUserInfo$Anon1(UserRemoteDataSource userRemoteDataSource, xe6 xe6) {
        super(xe6);
        this.this$0 = userRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.loadUserInfo((MFUser) null, this);
    }
}
