package com.portfolio.platform.data.source.local.quickresponse;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.QuickResponseMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseMessageDao_Impl extends QuickResponseMessageDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<QuickResponseMessage> __insertionAdapterOfQuickResponseMessage;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveAll;
    @DexIgnore
    public /* final */ vh __preparedStmtOfRemoveById;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<QuickResponseMessage> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `quickResponseMessage` (`id`,`response`) VALUES (nullif(?, 0),?)";
        }

        @DexIgnore
        public void bind(mi miVar, QuickResponseMessage quickResponseMessage) {
            miVar.a(1, (long) quickResponseMessage.getId());
            if (quickResponseMessage.getResponse() == null) {
                miVar.a(2);
            } else {
                miVar.a(2, quickResponseMessage.getResponse());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM quickResponseMessage WHERE id = ?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM quickResponseMessage";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon4 implements Callable<List<QuickResponseMessage>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon4(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<QuickResponseMessage> call() throws Exception {
            Cursor a = bi.a(QuickResponseMessageDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "response");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    QuickResponseMessage quickResponseMessage = new QuickResponseMessage(a.getString(b2));
                    quickResponseMessage.setId(a.getInt(b));
                    arrayList.add(quickResponseMessage);
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public QuickResponseMessageDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfQuickResponseMessage = new Anon1(ohVar);
        this.__preparedStmtOfRemoveById = new Anon2(ohVar);
        this.__preparedStmtOfRemoveAll = new Anon3(ohVar);
    }

    @DexIgnore
    public List<QuickResponseMessage> getAllRawResponse() {
        rh b = rh.b("SELECT * FROM quickResponseMessage", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "response");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                QuickResponseMessage quickResponseMessage = new QuickResponseMessage(a.getString(b3));
                quickResponseMessage.setId(a.getInt(b2));
                arrayList.add(quickResponseMessage);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<QuickResponseMessage>> getAllResponse() {
        return this.__db.getInvalidationTracker().a(new String[]{"quickResponseMessage"}, false, new Anon4(rh.b("SELECT * FROM quickResponseMessage", 0)));
    }

    @DexIgnore
    public void insertResponse(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertResponses(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void removeAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveAll.release(acquire);
        }
    }

    @DexIgnore
    public void removeById(int i) {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfRemoveById.acquire();
        acquire.a(1, (long) i);
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfRemoveById.release(acquire);
        }
    }

    @DexIgnore
    public void update(QuickResponseMessage quickResponseMessage) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(quickResponseMessage);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void update(List<QuickResponseMessage> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfQuickResponseMessage.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
