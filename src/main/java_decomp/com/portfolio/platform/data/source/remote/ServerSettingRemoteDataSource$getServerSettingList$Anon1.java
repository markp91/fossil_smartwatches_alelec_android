package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.MFNetworkReturnCode;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.response.ResponseKt;
import java.net.SocketTimeoutException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.ServerSettingRemoteDataSource$getServerSettingList$1", f = "ServerSettingRemoteDataSource.kt", l = {38}, m = "invokeSuspend")
public final class ServerSettingRemoteDataSource$getServerSettingList$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingDataSource.OnGetServerSettingList $callback;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ServerSettingRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ServerSettingRemoteDataSource$getServerSettingList$Anon1(ServerSettingRemoteDataSource serverSettingRemoteDataSource, ServerSettingDataSource.OnGetServerSettingList onGetServerSettingList, xe6 xe6) {
        super(2, xe6);
        this.this$0 = serverSettingRemoteDataSource;
        this.$callback = onGetServerSettingList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ServerSettingRemoteDataSource$getServerSettingList$Anon1 serverSettingRemoteDataSource$getServerSettingList$Anon1 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1(this.this$0, this.$callback, xe6);
        serverSettingRemoteDataSource$getServerSettingList$Anon1.p$ = (il6) obj;
        return serverSettingRemoteDataSource$getServerSettingList$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ServerSettingRemoteDataSource$getServerSettingList$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2 = new ServerSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = ResponseKt.a(serverSettingRemoteDataSource$getServerSettingList$Anon1$response$Anon1_Level2, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap4 = (ap4) obj;
        if (ap4 instanceof cp4) {
            FLogger.INSTANCE.getLocal().e(ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease(), "getServerSettingList- is successful");
            this.$callback.onSuccess((ServerSettingList) ((cp4) ap4).a());
        } else if (ap4 instanceof zo4) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tAG$app_fossilRelease = ServerSettingRemoteDataSource.Companion.getTAG$app_fossilRelease();
            StringBuilder sb = new StringBuilder();
            sb.append("getServerSettingList - Failure, code=");
            zo4 zo4 = (zo4) ap4;
            sb.append(zo4.a());
            local.e(tAG$app_fossilRelease, sb.toString());
            if (zo4.d() instanceof SocketTimeoutException) {
                this.$callback.onFailed(MFNetworkReturnCode.CLIENT_TIMEOUT);
            } else {
                this.$callback.onFailed(601);
            }
        }
        return cd6.a;
    }
}
