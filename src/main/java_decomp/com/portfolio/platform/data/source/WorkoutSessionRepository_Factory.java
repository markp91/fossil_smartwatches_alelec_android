package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.FitnessDataDao;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutDao;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSessionRepository_Factory implements Factory<WorkoutSessionRepository> {
    @DexIgnore
    public /* final */ Provider<ApiServiceV2> mApiServiceProvider;
    @DexIgnore
    public /* final */ Provider<FitnessDataDao> mFitnessDataDaoProvider;
    @DexIgnore
    public /* final */ Provider<WorkoutDao> mWorkoutDaoProvider;

    @DexIgnore
    public WorkoutSessionRepository_Factory(Provider<WorkoutDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        this.mWorkoutDaoProvider = provider;
        this.mFitnessDataDaoProvider = provider2;
        this.mApiServiceProvider = provider3;
    }

    @DexIgnore
    public static WorkoutSessionRepository_Factory create(Provider<WorkoutDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new WorkoutSessionRepository_Factory(provider, provider2, provider3);
    }

    @DexIgnore
    public static WorkoutSessionRepository newWorkoutSessionRepository(WorkoutDao workoutDao, FitnessDataDao fitnessDataDao, ApiServiceV2 apiServiceV2) {
        return new WorkoutSessionRepository(workoutDao, fitnessDataDao, apiServiceV2);
    }

    @DexIgnore
    public static WorkoutSessionRepository provideInstance(Provider<WorkoutDao> provider, Provider<FitnessDataDao> provider2, Provider<ApiServiceV2> provider3) {
        return new WorkoutSessionRepository(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public WorkoutSessionRepository get() {
        return provideInstance(this.mWorkoutDaoProvider, this.mFitnessDataDaoProvider, this.mApiServiceProvider);
    }
}
