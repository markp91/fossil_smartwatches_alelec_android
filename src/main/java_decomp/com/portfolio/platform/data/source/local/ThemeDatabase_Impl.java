package com.portfolio.platform.data.source.local;

import com.fossil.bi;
import com.fossil.fh;
import com.fossil.fi;
import com.fossil.ii;
import com.fossil.ji;
import com.fossil.lh;
import com.fossil.oh;
import com.fossil.qh;
import java.util.HashMap;
import java.util.HashSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ThemeDatabase_Impl extends ThemeDatabase {
    @DexIgnore
    public volatile ThemeDao _themeDao;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends qh.a {
        @DexIgnore
        public Anon1(int i) {
            super(i);
        }

        @DexIgnore
        public void createAllTables(ii iiVar) {
            iiVar.b("CREATE TABLE IF NOT EXISTS `theme` (`isCurrentTheme` INTEGER NOT NULL, `type` TEXT NOT NULL, `id` TEXT NOT NULL, `name` TEXT NOT NULL, `styles` TEXT NOT NULL, PRIMARY KEY(`id`))");
            iiVar.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
            iiVar.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '3bbfe40ee4044998d625f23932959275')");
        }

        @DexIgnore
        public void dropAllTables(ii iiVar) {
            iiVar.b("DROP TABLE IF EXISTS `theme`");
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThemeDatabase_Impl.this.mCallbacks.get(i)).b(iiVar);
                }
            }
        }

        @DexIgnore
        public void onCreate(ii iiVar) {
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThemeDatabase_Impl.this.mCallbacks.get(i)).a(iiVar);
                }
            }
        }

        @DexIgnore
        public void onOpen(ii iiVar) {
            ii unused = ThemeDatabase_Impl.this.mDatabase = iiVar;
            ThemeDatabase_Impl.this.internalInitInvalidationTracker(iiVar);
            if (ThemeDatabase_Impl.this.mCallbacks != null) {
                int size = ThemeDatabase_Impl.this.mCallbacks.size();
                for (int i = 0; i < size; i++) {
                    ((oh.b) ThemeDatabase_Impl.this.mCallbacks.get(i)).c(iiVar);
                }
            }
        }

        @DexIgnore
        public void onPostMigrate(ii iiVar) {
        }

        @DexIgnore
        public void onPreMigrate(ii iiVar) {
            bi.a(iiVar);
        }

        @DexIgnore
        public qh.b onValidateSchema(ii iiVar) {
            HashMap hashMap = new HashMap(5);
            hashMap.put("isCurrentTheme", new fi.a("isCurrentTheme", "INTEGER", true, 0, (String) null, 1));
            hashMap.put("type", new fi.a("type", "TEXT", true, 0, (String) null, 1));
            hashMap.put("id", new fi.a("id", "TEXT", true, 1, (String) null, 1));
            hashMap.put("name", new fi.a("name", "TEXT", true, 0, (String) null, 1));
            hashMap.put("styles", new fi.a("styles", "TEXT", true, 0, (String) null, 1));
            fi fiVar = new fi("theme", hashMap, new HashSet(0), new HashSet(0));
            fi a = fi.a(iiVar, "theme");
            if (fiVar.equals(a)) {
                return new qh.b(true, (String) null);
            }
            return new qh.b(false, "theme(com.portfolio.platform.data.model.Theme).\n Expected:\n" + fiVar + "\n Found:\n" + a);
        }
    }

    @DexIgnore
    public void clearAllTables() {
        ThemeDatabase_Impl.super.assertNotMainThread();
        ii a = ThemeDatabase_Impl.super.getOpenHelper().a();
        try {
            ThemeDatabase_Impl.super.beginTransaction();
            a.b("DELETE FROM `theme`");
            ThemeDatabase_Impl.super.setTransactionSuccessful();
        } finally {
            ThemeDatabase_Impl.super.endTransaction();
            a.d("PRAGMA wal_checkpoint(FULL)").close();
            if (!a.A()) {
                a.b("VACUUM");
            }
        }
    }

    @DexIgnore
    public lh createInvalidationTracker() {
        return new lh(this, new HashMap(0), new HashMap(0), new String[]{"theme"});
    }

    @DexIgnore
    public ji createOpenHelper(fh fhVar) {
        qh qhVar = new qh(fhVar, new Anon1(1), "3bbfe40ee4044998d625f23932959275", "68ccf1dee92254a5e6e37fa35b38bb4e");
        ji.b.a a = ji.b.a(fhVar.b);
        a.a(fhVar.c);
        a.a(qhVar);
        return fhVar.a.a(a.a());
    }

    @DexIgnore
    public ThemeDao themeDao() {
        ThemeDao themeDao;
        if (this._themeDao != null) {
            return this._themeDao;
        }
        synchronized (this) {
            if (this._themeDao == null) {
                this._themeDao = new ThemeDao_Impl(this);
            }
            themeDao = this._themeDao;
        }
        return themeDao;
    }
}
