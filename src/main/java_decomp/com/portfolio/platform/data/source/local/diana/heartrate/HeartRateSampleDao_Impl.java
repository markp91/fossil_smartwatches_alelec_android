package com.portfolio.platform.data.source.local.diana.heartrate;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.lifecycle.LiveData;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.n44;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.fossil.x34;
import com.fossil.z34;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HeartRateSampleDao_Impl extends HeartRateSampleDao {
    @DexIgnore
    public /* final */ x34 __dateShortStringConverter; // = new x34();
    @DexIgnore
    public /* final */ z34 __dateTimeISOStringConverter; // = new z34();
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ hh<HeartRateSample> __insertionAdapterOfHeartRateSample;
    @DexIgnore
    public /* final */ vh __preparedStmtOfDeleteAllHeartRateSamples;
    @DexIgnore
    public /* final */ n44 __restingConverter; // = new n44();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<HeartRateSample> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `heart_rate_sample` (`id`,`average`,`date`,`createdAt`,`updatedAt`,`endTime`,`startTime`,`timezoneOffset`,`min`,`max`,`minuteCount`,`resting`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, HeartRateSample heartRateSample) {
            if (heartRateSample.getId() == null) {
                miVar.a(1);
            } else {
                miVar.a(1, heartRateSample.getId());
            }
            miVar.a(2, (double) heartRateSample.getAverage());
            String a = HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(heartRateSample.getDate());
            if (a == null) {
                miVar.a(3);
            } else {
                miVar.a(3, a);
            }
            miVar.a(4, heartRateSample.getCreatedAt());
            miVar.a(5, heartRateSample.getUpdatedAt());
            String a2 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getEndTime());
            if (a2 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a2);
            }
            String a3 = HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(heartRateSample.getStartTime());
            if (a3 == null) {
                miVar.a(7);
            } else {
                miVar.a(7, a3);
            }
            miVar.a(8, (long) heartRateSample.getTimezoneOffsetInSecond());
            miVar.a(9, (long) heartRateSample.getMin());
            miVar.a(10, (long) heartRateSample.getMax());
            miVar.a(11, (long) heartRateSample.getMinuteCount());
            String a4 = HeartRateSampleDao_Impl.this.__restingConverter.a(heartRateSample.getResting());
            if (a4 == null) {
                miVar.a(12);
            } else {
                miVar.a(12, a4);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends vh {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM heart_rate_sample";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 implements Callable<List<HeartRateSample>> {
        @DexIgnore
        public /* final */ /* synthetic */ rh val$_statement;

        @DexIgnore
        public Anon3(rh rhVar) {
            this.val$_statement = rhVar;
        }

        @DexIgnore
        public void finalize() {
            this.val$_statement.c();
        }

        @DexIgnore
        public List<HeartRateSample> call() throws Exception {
            Cursor a = bi.a(HeartRateSampleDao_Impl.this.__db, this.val$_statement, false, (CancellationSignal) null);
            try {
                int b = ai.b(a, "id");
                int b2 = ai.b(a, "average");
                int b3 = ai.b(a, HardwareLog.COLUMN_DATE);
                int b4 = ai.b(a, "createdAt");
                int b5 = ai.b(a, "updatedAt");
                int b6 = ai.b(a, "endTime");
                int b7 = ai.b(a, "startTime");
                int b8 = ai.b(a, "timezoneOffset");
                int b9 = ai.b(a, "min");
                int b10 = ai.b(a, "max");
                int b11 = ai.b(a, "minuteCount");
                int b12 = ai.b(a, "resting");
                ArrayList arrayList = new ArrayList(a.getCount());
                while (a.moveToNext()) {
                    int i = b;
                    arrayList.add(new HeartRateSample(a.getString(b), a.getFloat(b2), HeartRateSampleDao_Impl.this.__dateShortStringConverter.a(a.getString(b3)), a.getLong(b4), a.getLong(b5), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b6)), HeartRateSampleDao_Impl.this.__dateTimeISOStringConverter.a(a.getString(b7)), a.getInt(b8), a.getInt(b9), a.getInt(b10), a.getInt(b11), HeartRateSampleDao_Impl.this.__restingConverter.a(a.getString(b12))));
                    b = i;
                }
                return arrayList;
            } finally {
                a.close();
            }
        }
    }

    @DexIgnore
    public HeartRateSampleDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfHeartRateSample = new Anon1(ohVar);
        this.__preparedStmtOfDeleteAllHeartRateSamples = new Anon2(ohVar);
    }

    @DexIgnore
    public void deleteAllHeartRateSamples() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfDeleteAllHeartRateSamples.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfDeleteAllHeartRateSamples.release(acquire);
        }
    }

    @DexIgnore
    public HeartRateSample getHeartRateSample(String str) {
        HeartRateSample heartRateSample;
        String str2 = str;
        rh b = rh.b("SELECT * FROM heart_rate_sample WHERE id = ?", 1);
        if (str2 == null) {
            b.a(1);
        } else {
            b.a(1, str2);
        }
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "average");
            int b4 = ai.b(a, HardwareLog.COLUMN_DATE);
            int b5 = ai.b(a, "createdAt");
            int b6 = ai.b(a, "updatedAt");
            int b7 = ai.b(a, "endTime");
            int b8 = ai.b(a, "startTime");
            int b9 = ai.b(a, "timezoneOffset");
            int b10 = ai.b(a, "min");
            int b11 = ai.b(a, "max");
            int b12 = ai.b(a, "minuteCount");
            int b13 = ai.b(a, "resting");
            if (a.moveToFirst()) {
                heartRateSample = new HeartRateSample(a.getString(b2), a.getFloat(b3), this.__dateShortStringConverter.a(a.getString(b4)), a.getLong(b5), a.getLong(b6), this.__dateTimeISOStringConverter.a(a.getString(b7)), this.__dateTimeISOStringConverter.a(a.getString(b8)), a.getInt(b9), a.getInt(b10), a.getInt(b11), a.getInt(b12), this.__restingConverter.a(a.getString(b13)));
            } else {
                heartRateSample = null;
            }
            return heartRateSample;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public LiveData<List<HeartRateSample>> getHeartRateSamples(Date date, Date date2) {
        rh b = rh.b("SELECT * FROM heart_rate_sample WHERE date >= ? AND date <= ? ORDER BY startTime ASC", 2);
        String a = this.__dateShortStringConverter.a(date);
        if (a == null) {
            b.a(1);
        } else {
            b.a(1, a);
        }
        String a2 = this.__dateShortStringConverter.a(date2);
        if (a2 == null) {
            b.a(2);
        } else {
            b.a(2, a2);
        }
        return this.__db.getInvalidationTracker().a(new String[]{"heart_rate_sample"}, false, new Anon3(b));
    }

    @DexIgnore
    public void insertHeartRateSample(HeartRateSample heartRateSample) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(heartRateSample);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void upsertHeartRateSampleList(List<HeartRateSample> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfHeartRateSample.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
