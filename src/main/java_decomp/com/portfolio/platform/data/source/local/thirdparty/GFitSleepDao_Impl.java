package com.portfolio.platform.data.source.local.thirdparty;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.ai;
import com.fossil.bi;
import com.fossil.gh;
import com.fossil.hh;
import com.fossil.mi;
import com.fossil.oh;
import com.fossil.rh;
import com.fossil.vh;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSleep;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GFitSleepDao_Impl implements GFitSleepDao {
    @DexIgnore
    public /* final */ oh __db;
    @DexIgnore
    public /* final */ gh<GFitSleep> __deletionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ hh<GFitSleep> __insertionAdapterOfGFitSleep;
    @DexIgnore
    public /* final */ vh __preparedStmtOfClearAll;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends hh<GFitSleep> {
        @DexIgnore
        public Anon1(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR REPLACE INTO `gFitSleep` (`id`,`sleepMins`,`startTime`,`endTime`) VALUES (nullif(?, 0),?,?,?)";
        }

        @DexIgnore
        public void bind(mi miVar, GFitSleep gFitSleep) {
            miVar.a(1, (long) gFitSleep.getId());
            miVar.a(2, (long) gFitSleep.getSleepMins());
            miVar.a(3, gFitSleep.getStartTime());
            miVar.a(4, gFitSleep.getEndTime());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon2 extends gh<GFitSleep> {
        @DexIgnore
        public Anon2(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM `gFitSleep` WHERE `id` = ?";
        }

        @DexIgnore
        public void bind(mi miVar, GFitSleep gFitSleep) {
            miVar.a(1, (long) gFitSleep.getId());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon3 extends vh {
        @DexIgnore
        public Anon3(oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM GFitSleep";
        }
    }

    @DexIgnore
    public GFitSleepDao_Impl(oh ohVar) {
        this.__db = ohVar;
        this.__insertionAdapterOfGFitSleep = new Anon1(ohVar);
        this.__deletionAdapterOfGFitSleep = new Anon2(ohVar);
        this.__preparedStmtOfClearAll = new Anon3(ohVar);
    }

    @DexIgnore
    public void clearAll() {
        this.__db.assertNotSuspendingTransaction();
        mi acquire = this.__preparedStmtOfClearAll.acquire();
        this.__db.beginTransaction();
        try {
            acquire.s();
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
            this.__preparedStmtOfClearAll.release(acquire);
        }
    }

    @DexIgnore
    public void deleteListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__deletionAdapterOfGFitSleep.handleMultiple(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public List<GFitSleep> getAllGFitSleep() {
        rh b = rh.b("SELECT * FROM gFitSleep", 0);
        this.__db.assertNotSuspendingTransaction();
        Cursor a = bi.a(this.__db, b, false, (CancellationSignal) null);
        try {
            int b2 = ai.b(a, "id");
            int b3 = ai.b(a, "sleepMins");
            int b4 = ai.b(a, "startTime");
            int b5 = ai.b(a, "endTime");
            ArrayList arrayList = new ArrayList(a.getCount());
            while (a.moveToNext()) {
                GFitSleep gFitSleep = new GFitSleep(a.getInt(b3), a.getLong(b4), a.getLong(b5));
                gFitSleep.setId(a.getInt(b2));
                arrayList.add(gFitSleep);
            }
            return arrayList;
        } finally {
            a.close();
            b.c();
        }
    }

    @DexIgnore
    public void insertGFitSleep(GFitSleep gFitSleep) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(gFitSleep);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }

    @DexIgnore
    public void insertListGFitSleep(List<GFitSleep> list) {
        this.__db.assertNotSuspendingTransaction();
        this.__db.beginTransaction();
        try {
            this.__insertionAdapterOfGFitSleep.insert(list);
            this.__db.setTransactionSuccessful();
        } finally {
            this.__db.endTransaction();
        }
    }
}
