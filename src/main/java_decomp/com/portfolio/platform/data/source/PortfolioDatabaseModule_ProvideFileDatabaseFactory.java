package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.local.FileDatabase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideFileDatabaseFactory implements Factory<FileDatabase> {
    @DexIgnore
    public /* final */ Provider<PortfolioApp> appProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideFileDatabaseFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        this.module = portfolioDatabaseModule;
        this.appProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideFileDatabaseFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return new PortfolioDatabaseModule_ProvideFileDatabaseFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static FileDatabase provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<PortfolioApp> provider) {
        return proxyProvideFileDatabase(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static FileDatabase proxyProvideFileDatabase(PortfolioDatabaseModule portfolioDatabaseModule, PortfolioApp portfolioApp) {
        FileDatabase provideFileDatabase = portfolioDatabaseModule.provideFileDatabase(portfolioApp);
        z76.a(provideFileDatabase, "Cannot return null from a non-@Nullable @Provides method");
        return provideFileDatabase;
    }

    @DexIgnore
    public FileDatabase get() {
        return provideInstance(this.module, this.appProvider);
    }
}
