package com.portfolio.platform.data.source;

import com.portfolio.platform.data.source.local.AlarmsLocalDataSource;
import com.portfolio.platform.data.source.remote.AlarmsRemoteDataSource;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmsRepository_Factory implements Factory<AlarmsRepository> {
    @DexIgnore
    public /* final */ Provider<AlarmsRemoteDataSource> mAlarmRemoteDataSourceProvider;
    @DexIgnore
    public /* final */ Provider<AlarmsLocalDataSource> mAlarmsLocalDataSourceProvider;

    @DexIgnore
    public AlarmsRepository_Factory(Provider<AlarmsLocalDataSource> provider, Provider<AlarmsRemoteDataSource> provider2) {
        this.mAlarmsLocalDataSourceProvider = provider;
        this.mAlarmRemoteDataSourceProvider = provider2;
    }

    @DexIgnore
    public static AlarmsRepository_Factory create(Provider<AlarmsLocalDataSource> provider, Provider<AlarmsRemoteDataSource> provider2) {
        return new AlarmsRepository_Factory(provider, provider2);
    }

    @DexIgnore
    public static AlarmsRepository newAlarmsRepository(AlarmsLocalDataSource alarmsLocalDataSource, AlarmsRemoteDataSource alarmsRemoteDataSource) {
        return new AlarmsRepository(alarmsLocalDataSource, alarmsRemoteDataSource);
    }

    @DexIgnore
    public static AlarmsRepository provideInstance(Provider<AlarmsLocalDataSource> provider, Provider<AlarmsRemoteDataSource> provider2) {
        return new AlarmsRepository(provider.get(), provider2.get());
    }

    @DexIgnore
    public AlarmsRepository get() {
        return provideInstance(this.mAlarmsLocalDataSourceProvider, this.mAlarmRemoteDataSourceProvider);
    }
}
