package com.portfolio.platform.data.source.local.fitness;

import com.fossil.ap4;
import com.fossil.cd6;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lc6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.vk4;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.portfolio.platform.data.model.ServerError;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapperKt;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.local.fitness.ActivitySummaryLocalDataSource$loadData$1", f = "ActivitySummaryLocalDataSource.kt", l = {185}, m = "invokeSuspend")
public final class ActivitySummaryLocalDataSource$loadData$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ vk4.b.a $helperCallback;
    @DexIgnore
    public /* final */ /* synthetic */ vk4.d $requestType;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActivitySummaryLocalDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ActivitySummaryLocalDataSource$loadData$Anon1(ActivitySummaryLocalDataSource activitySummaryLocalDataSource, Date date, Date date2, vk4.d dVar, vk4.b.a aVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = activitySummaryLocalDataSource;
        this.$startDate = date;
        this.$endDate = date2;
        this.$requestType = dVar;
        this.$helperCallback = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        ActivitySummaryLocalDataSource$loadData$Anon1 activitySummaryLocalDataSource$loadData$Anon1 = new ActivitySummaryLocalDataSource$loadData$Anon1(this.this$0, this.$startDate, this.$endDate, this.$requestType, this.$helperCallback, xe6);
        activitySummaryLocalDataSource$loadData$Anon1.p$ = (il6) obj;
        return activitySummaryLocalDataSource$loadData$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((ActivitySummaryLocalDataSource$loadData$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            List<FitnessDataWrapper> fitnessData = this.this$0.mFitnessDataRepository.getFitnessData(this.$startDate, this.$endDate);
            lc6<Date, Date> calculateRangeDownload = FitnessDataWrapperKt.calculateRangeDownload(fitnessData, this.$startDate, this.$endDate);
            if (calculateRangeDownload != null) {
                this.L$0 = il6;
                this.L$1 = fitnessData;
                this.L$2 = calculateRangeDownload;
                this.label = 1;
                obj = this.this$0.mSummariesRepository.loadSummaries(calculateRangeDownload.getFirst(), calculateRangeDownload.getSecond(), this);
                if (obj == a) {
                    return a;
                }
            } else {
                this.$helperCallback.a();
                return cd6.a;
            }
        } else if (i == 1) {
            lc6 lc6 = (lc6) this.L$2;
            List list = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ap4 ap4 = (ap4) obj;
        if (ap4 instanceof cp4) {
            if (this.$requestType == vk4.d.AFTER && (!this.this$0.mRequestAfterQueue.isEmpty())) {
                this.this$0.mRequestAfterQueue.remove(0);
            }
            this.$helperCallback.a();
        } else if (ap4 instanceof zo4) {
            zo4 zo4 = (zo4) ap4;
            if (zo4.d() != null) {
                this.$helperCallback.a(zo4.d());
            } else if (zo4.c() != null) {
                ServerError c = zo4.c();
                vk4.b.a aVar = this.$helperCallback;
                String userMessage = c.getUserMessage();
                if (userMessage == null) {
                    userMessage = c.getMessage();
                }
                if (userMessage == null) {
                    userMessage = "";
                }
                aVar.a(new Throwable(userMessage));
            }
        }
        return cd6.a;
    }
}
