package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.v3;
import com.portfolio.platform.data.NetworkState;
import com.portfolio.platform.data.source.local.sleep.SleepSummaryLocalDataSource;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSummariesRepository$getSummariesPaging$Anon1<I, O> implements v3<X, LiveData<Y>> {
    @DexIgnore
    public static /* final */ SleepSummariesRepository$getSummariesPaging$Anon1 INSTANCE; // = new SleepSummariesRepository$getSummariesPaging$Anon1();

    @DexIgnore
    public final LiveData<NetworkState> apply(SleepSummaryLocalDataSource sleepSummaryLocalDataSource) {
        return sleepSummaryLocalDataSource.getMNetworkState();
    }
}
