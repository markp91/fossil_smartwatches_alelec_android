package com.portfolio.platform.data.source.remote;

import com.fossil.ap4;
import com.fossil.cp4;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.kc6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zo4;
import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.response.ResponseKt;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppRemoteDataSource {
    @DexIgnore
    public /* final */ ApiServiceV2 mApiServiceV2;

    @DexIgnore
    public WatchAppRemoteDataSource(ApiServiceV2 apiServiceV2) {
        wg6.b(apiServiceV2, "mApiServiceV2");
        this.mApiServiceV2 = apiServiceV2;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0039  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x007c  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final Object getAllWatchApp(String str, xe6<? super ap4<List<WatchApp>>> xe6) {
        WatchAppRemoteDataSource$getAllWatchApp$Anon1 watchAppRemoteDataSource$getAllWatchApp$Anon1;
        int i;
        ap4 ap4;
        List list;
        if (xe6 instanceof WatchAppRemoteDataSource$getAllWatchApp$Anon1) {
            watchAppRemoteDataSource$getAllWatchApp$Anon1 = (WatchAppRemoteDataSource$getAllWatchApp$Anon1) xe6;
            int i2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                watchAppRemoteDataSource$getAllWatchApp$Anon1.label = i2 - Integer.MIN_VALUE;
                Object obj = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
                Object a = ff6.a();
                i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
                if (i != 0) {
                    nc6.a(obj);
                    WatchAppRemoteDataSource$getAllWatchApp$response$Anon1 watchAppRemoteDataSource$getAllWatchApp$response$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$response$Anon1(this, str, (xe6) null);
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$0 = this;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.L$1 = str;
                    watchAppRemoteDataSource$getAllWatchApp$Anon1.label = 1;
                    obj = ResponseKt.a(watchAppRemoteDataSource$getAllWatchApp$response$Anon1, watchAppRemoteDataSource$getAllWatchApp$Anon1);
                    if (obj == a) {
                        return a;
                    }
                } else if (i == 1) {
                    String str2 = (String) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$1;
                    WatchAppRemoteDataSource watchAppRemoteDataSource = (WatchAppRemoteDataSource) watchAppRemoteDataSource$getAllWatchApp$Anon1.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                ap4 = (ap4) obj;
                if (!(ap4 instanceof cp4)) {
                    ArrayList arrayList = new ArrayList();
                    cp4 cp4 = (cp4) ap4;
                    ApiResponse apiResponse = (ApiResponse) cp4.a();
                    if (!(apiResponse == null || (list = apiResponse.get_items()) == null)) {
                        hf6.a(arrayList.addAll(list));
                    }
                    return new cp4(arrayList, cp4.b());
                } else if (ap4 instanceof zo4) {
                    zo4 zo4 = (zo4) ap4;
                    return new zo4(zo4.a(), zo4.c(), zo4.d(), (String) null, 8, (qg6) null);
                } else {
                    throw new kc6();
                }
            }
        }
        watchAppRemoteDataSource$getAllWatchApp$Anon1 = new WatchAppRemoteDataSource$getAllWatchApp$Anon1(this, xe6);
        Object obj2 = watchAppRemoteDataSource$getAllWatchApp$Anon1.result;
        Object a2 = ff6.a();
        i = watchAppRemoteDataSource$getAllWatchApp$Anon1.label;
        if (i != 0) {
        }
        ap4 = (ap4) obj2;
        if (!(ap4 instanceof cp4)) {
        }
    }
}
