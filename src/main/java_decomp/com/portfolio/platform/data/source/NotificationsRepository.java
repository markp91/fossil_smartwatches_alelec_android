package com.portfolio.platform.data.source;

import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.SparseArray;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.fossil.z76;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.scope.Local;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationsRepository implements NotificationsDataSource {
    @DexIgnore
    public static /* final */ String TAG; // = "NotificationsRepository";
    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> mCachedNotificationsData;
    @DexIgnore
    public boolean mCachedNotificationsDirty;
    @DexIgnore
    public /* final */ NotificationsDataSource mNotificationLocalDataSource;
    @DexIgnore
    public NotificationsDataSource mNotificationRemoteDataSource;
    @DexIgnore
    public /* final */ List<NotificationRepositoryObserver> mObservers; // = new CopyOnWriteArrayList();

    @DexIgnore
    public interface NotificationRepositoryObserver {
        @DexIgnore
        void onNotificationsDataChanged();
    }

    @DexIgnore
    public NotificationsRepository(@Local NotificationsDataSource notificationsDataSource) {
        z76.a(notificationsDataSource, "mNotificationLocalDataSource cannot be null!");
        this.mNotificationLocalDataSource = notificationsDataSource;
    }

    @DexIgnore
    private void notifyNotificationsChanged() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .notifyNotificationsChanged observerList=" + this.mObservers);
        for (NotificationRepositoryObserver onNotificationsDataChanged : this.mObservers) {
            onNotificationsDataChanged.onNotificationsDataChanged();
        }
    }

    @DexIgnore
    private void processNotificationsList(SparseArray<List<BaseFeatureModel>> sparseArray) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .processNotificationsList notificationsList = " + sparseArray);
        if (sparseArray != null) {
            if (this.mCachedNotificationsData == null) {
                this.mCachedNotificationsData = new SparseArray<>();
            }
            this.mCachedNotificationsData.clear();
            for (int i = 1; i <= 12; i++) {
                if (sparseArray.get(i) != null && !sparseArray.get(i).isEmpty()) {
                    this.mCachedNotificationsData.put(i, sparseArray.get(i));
                }
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = TAG;
            local2.d(str2, "Inside .processNotificationsList add list notification to cache = " + sparseArray);
            this.mCachedNotificationsDirty = false;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    private List<Integer> queryContactOnPhone(List<Integer> list) {
        String str = "contact_id IN " + transformInCondition(list);
        FLogger.INSTANCE.getLocal().d(TAG, ".Inside step 2 - query contact on phone IN condition, selection = " + str);
        Cursor query = PortfolioApp.T.getContentResolver().query(ContactsContract.Data.CONTENT_URI, new String[]{"contact_id"}, str, (String[]) null, (String) null);
        ArrayList arrayList = new ArrayList();
        if (query != null) {
            query.moveToFirst();
            while (!query.isAfterLast()) {
                int i = query.getInt(0);
                FLogger.INSTANCE.getLocal().d(TAG, ".Inside step 2 - query contact on phone IN condition, contactId=" + i);
                arrayList.add(Integer.valueOf(i));
                query.moveToNext();
            }
            query.close();
        }
        return arrayList;
    }

    @DexIgnore
    private StringBuilder transformInCondition(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (Integer intValue : list) {
            int intValue2 = intValue.intValue();
            if (sb.length() > 1) {
                sb.append(',');
            }
            sb.append(intValue2);
        }
        sb.append(')');
        return sb;
    }

    @DexIgnore
    public void addContentObserver(NotificationRepositoryObserver notificationRepositoryObserver) {
        if (!this.mObservers.contains(notificationRepositoryObserver)) {
            this.mObservers.add(notificationRepositoryObserver);
        }
    }

    @DexIgnore
    public void clearAllNotificationSetting() {
        this.mNotificationLocalDataSource.clearAllNotificationSetting();
        SparseArray<List<BaseFeatureModel>> sparseArray = this.mCachedNotificationsData;
        if (sparseArray != null) {
            sparseArray.clear();
        }
        this.mCachedNotificationsDirty = true;
    }

    @DexIgnore
    public void clearAllPhoneFavoritesContacts() {
        this.mNotificationLocalDataSource.clearAllPhoneFavoritesContacts();
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByHour(int i, int i2) {
        return this.mNotificationLocalDataSource.getAllAppFilterByHour(i, i2);
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilterByVibration(int i) {
        return this.mNotificationLocalDataSource.getAllAppFilterByVibration(i);
    }

    @DexIgnore
    public List<AppFilter> getAllAppFilters(int i) {
        return this.mNotificationLocalDataSource.getAllAppFilters(i);
    }

    @DexIgnore
    public List<ContactGroup> getAllContactGroups(int i) {
        return this.mNotificationLocalDataSource.getAllContactGroups(i);
    }

    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> getAllNotificationsByHour(String str, int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "Inside .getAllNotificationsByHour isCachedAvailable=" + isCacheNotificationsAvailable());
        if (isCacheNotificationsAvailable()) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local2.d(str3, "Inside .getAllNotificationsByHour cachedNotifications=" + this.mCachedNotificationsData);
            return this.mCachedNotificationsData;
        }
        processNotificationsList(this.mNotificationLocalDataSource.getAllNotificationsByHour(str, i));
        return this.mCachedNotificationsData;
    }

    @DexIgnore
    public AppFilter getAppFilterByType(String str, int i) {
        return this.mNotificationLocalDataSource.getAppFilterByType(str, i);
    }

    @DexIgnore
    public SparseArray<List<BaseFeatureModel>> getCachedNotifications() {
        return this.mCachedNotificationsData;
    }

    @DexIgnore
    public Contact getContactById(int i) {
        return this.mNotificationLocalDataSource.getContactById(i);
    }

    @DexIgnore
    public List<Integer> getContactGroupId(List<Integer> list) {
        return this.mNotificationLocalDataSource.getContactGroupId(list);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingEmail(String str, int i) {
        return this.mNotificationLocalDataSource.getContactGroupsMatchingEmail(str, i);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingIncomingCall(String str, int i) {
        return this.mNotificationLocalDataSource.getContactGroupsMatchingIncomingCall(str, i);
    }

    @DexIgnore
    public List<ContactGroup> getContactGroupsMatchingSms(String str, int i) {
        return this.mNotificationLocalDataSource.getContactGroupsMatchingSms(str, i);
    }

    @DexIgnore
    public List<Integer> getLocalContactId() {
        return this.mNotificationLocalDataSource.getLocalContactId();
    }

    @DexIgnore
    public boolean isCacheNotificationsAvailable() {
        return this.mCachedNotificationsData != null && !this.mCachedNotificationsDirty;
    }

    @DexIgnore
    public void removeAllAppFilters() {
        FLogger.INSTANCE.getLocal().d(TAG, "Inside .removeAllAppFilters");
        this.mNotificationLocalDataSource.removeAllAppFilters();
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeAppFilter(AppFilter appFilter) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .removeListAppFilter appFilter=" + appFilter);
        this.mNotificationLocalDataSource.removeAppFilter(appFilter);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeContact(Contact contact) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .removeContact contact=" + contact);
        this.mNotificationLocalDataSource.removeContact(contact);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeContactGroup(ContactGroup contactGroup) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .saveContactGroup removeContactGroup=" + contactGroup);
        this.mNotificationLocalDataSource.removeContactGroup(contactGroup);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeContactGroupList(List<ContactGroup> list) {
        this.mNotificationLocalDataSource.removeContactGroupList(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeContentObserver(NotificationRepositoryObserver notificationRepositoryObserver) {
        this.mObservers.remove(notificationRepositoryObserver);
    }

    @DexIgnore
    public void removeListAppFilter(List<AppFilter> list) {
        this.mNotificationLocalDataSource.removeListAppFilter(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeListContact(List<Contact> list) {
        this.mNotificationLocalDataSource.removeListContact(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void removeLocalRedundantContact(List<Integer> list) {
        this.mNotificationLocalDataSource.removeLocalRedundantContact(list);
    }

    @DexIgnore
    public void removeLocalRedundantContactGroup(List<Integer> list) {
        this.mNotificationLocalDataSource.removeLocalRedundantContactGroup(list);
    }

    @DexIgnore
    public void removePhoneFavoritesContact(String str) {
        this.mNotificationLocalDataSource.removePhoneFavoritesContact(str);
    }

    @DexIgnore
    public void removePhoneNumber(List<Integer> list) {
        this.mNotificationLocalDataSource.removePhoneNumber(list);
    }

    @DexIgnore
    public void removePhoneNumberByContactGroupId(int i) {
        this.mNotificationLocalDataSource.removePhoneNumberByContactGroupId(i);
    }

    @DexIgnore
    public void removeRedundantContact() {
        FLogger.INSTANCE.getLocal().d(TAG, ".Inside removeRedundantContact");
        List<Integer> queryContactOnPhone = queryContactOnPhone(getLocalContactId());
        List<Integer> contactGroupId = getContactGroupId(queryContactOnPhone);
        removeLocalRedundantContact(queryContactOnPhone);
        removeLocalRedundantContactGroup(contactGroupId);
        removePhoneNumber(contactGroupId);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveAppFilter(AppFilter appFilter) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .saveAppFilter appFilter=" + appFilter);
        this.mNotificationLocalDataSource.saveAppFilter(appFilter);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveContact(Contact contact) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .saveContactGroup saveContact=" + contact);
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local2.d(str2, "Contact Id = " + contact.getContactId());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local3.d(str3, "Contact name = " + contact.getFirstName());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = TAG;
        local4.d(str4, "Contact db row = " + contact.getDbRowId());
        FLogger.INSTANCE.getLocal().d(TAG, "-----------------------------------------");
        this.mNotificationLocalDataSource.saveContact(contact);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveContactGroup(ContactGroup contactGroup) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .saveContactGroup contactGroup=" + contactGroup);
        this.mNotificationLocalDataSource.saveContactGroup(contactGroup);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveContactGroupList(List<ContactGroup> list) {
        this.mNotificationLocalDataSource.saveContactGroupList(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveEmailAddress(EmailAddress emailAddress) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .saveEmailAddress emailAddress=" + emailAddress);
        this.mNotificationLocalDataSource.saveEmailAddress(emailAddress);
    }

    @DexIgnore
    public void saveListAppFilters(List<AppFilter> list) {
        this.mNotificationLocalDataSource.saveListAppFilters(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveListContact(List<Contact> list) {
        this.mNotificationLocalDataSource.saveListContact(list);
        this.mCachedNotificationsDirty = true;
        notifyNotificationsChanged();
    }

    @DexIgnore
    public void saveListPhoneNumber(List<PhoneNumber> list) {
        this.mNotificationLocalDataSource.saveListPhoneNumber(list);
    }

    @DexIgnore
    public void savePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        this.mNotificationLocalDataSource.savePhoneFavoritesContact(phoneFavoritesContact);
    }

    @DexIgnore
    public void savePhoneNumber(PhoneNumber phoneNumber) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "Inside .savePhoneNumber phoneNumber=" + phoneNumber);
        this.mNotificationLocalDataSource.savePhoneNumber(phoneNumber);
    }

    @DexIgnore
    public void removePhoneFavoritesContact(PhoneFavoritesContact phoneFavoritesContact) {
        this.mNotificationLocalDataSource.removePhoneFavoritesContact(phoneFavoritesContact);
    }
}
