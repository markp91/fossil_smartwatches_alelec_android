package com.portfolio.platform.data.source.remote;

import com.fossil.jf6;
import com.fossil.lf6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.MicroAppRemoteDataSource", f = "MicroAppRemoteDataSource.kt", l = {18}, m = "getAllMicroApp")
public final class MicroAppRemoteDataSource$getAllMicroApp$Anon1 extends jf6 {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* synthetic */ Object result;
    @DexIgnore
    public /* final */ /* synthetic */ MicroAppRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MicroAppRemoteDataSource$getAllMicroApp$Anon1(MicroAppRemoteDataSource microAppRemoteDataSource, xe6 xe6) {
        super(xe6);
        this.this$0 = microAppRemoteDataSource;
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        this.result = obj;
        this.label |= Integer.MIN_VALUE;
        return this.this$0.getAllMicroApp((String) null, this);
    }
}
