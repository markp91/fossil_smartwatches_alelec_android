package com.portfolio.platform.data.source;

import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.model.goaltracking.response.GoalDailySummary;
import com.portfolio.platform.data.source.remote.ApiResponse;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.GoalTrackingRepository$loadSummaries$response$1", f = "GoalTrackingRepository.kt", l = {229}, m = "invokeSuspend")
public final class GoalTrackingRepository$loadSummaries$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<ApiResponse<GoalDailySummary>>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Date $endDate;
    @DexIgnore
    public /* final */ /* synthetic */ Date $startDate;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ GoalTrackingRepository this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GoalTrackingRepository$loadSummaries$response$Anon1(GoalTrackingRepository goalTrackingRepository, Date date, Date date2, xe6 xe6) {
        super(1, xe6);
        this.this$0 = goalTrackingRepository;
        this.$startDate = date;
        this.$endDate = date2;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new GoalTrackingRepository$loadSummaries$response$Anon1(this.this$0, this.$startDate, this.$endDate, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((GoalTrackingRepository$loadSummaries$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiServiceV2$p = this.this$0.mApiServiceV2;
            String e = bk4.e(this.$startDate);
            wg6.a((Object) e, "DateHelper.formatShortDate(startDate)");
            String e2 = bk4.e(this.$endDate);
            wg6.a((Object) e2, "DateHelper.formatShortDate(endDate)");
            this.label = 1;
            obj = access$getMApiServiceV2$p.getGoalTrackingSummaries(e, e2, 0, 100, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
