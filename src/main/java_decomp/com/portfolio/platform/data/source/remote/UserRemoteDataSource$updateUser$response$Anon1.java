package com.portfolio.platform.data.source.remote;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ku3;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rx6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.portfolio.platform.data.User;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.data.source.remote.UserRemoteDataSource$updateUser$response$1", f = "UserRemoteDataSource.kt", l = {78}, m = "invokeSuspend")
public final class UserRemoteDataSource$updateUser$response$Anon1 extends sf6 implements hg6<xe6<? super rx6<User>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ ku3 $userJsonObject;
    @DexIgnore
    public int label;
    @DexIgnore
    public /* final */ /* synthetic */ UserRemoteDataSource this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UserRemoteDataSource$updateUser$response$Anon1(UserRemoteDataSource userRemoteDataSource, ku3 ku3, xe6 xe6) {
        super(1, xe6);
        this.this$0 = userRemoteDataSource;
        this.$userJsonObject = ku3;
    }

    @DexIgnore
    public final xe6<cd6> create(xe6<?> xe6) {
        wg6.b(xe6, "completion");
        return new UserRemoteDataSource$updateUser$response$Anon1(this.this$0, this.$userJsonObject, xe6);
    }

    @DexIgnore
    public final Object invoke(Object obj) {
        return ((UserRemoteDataSource$updateUser$response$Anon1) create((xe6) obj)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            ApiServiceV2 access$getMApiService$p = this.this$0.mApiService;
            ku3 ku3 = this.$userJsonObject;
            this.label = 1;
            obj = access$getMApiService$p.updateUser(ku3, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
