package com.portfolio.platform.data.source;

import com.fossil.z76;
import com.portfolio.platform.data.source.local.diana.DianaCustomizeDatabase;
import com.portfolio.platform.data.source.local.diana.DianaPresetDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class PortfolioDatabaseModule_ProvideDianaPresetDaoFactory implements Factory<DianaPresetDao> {
    @DexIgnore
    public /* final */ Provider<DianaCustomizeDatabase> dbProvider;
    @DexIgnore
    public /* final */ PortfolioDatabaseModule module;

    @DexIgnore
    public PortfolioDatabaseModule_ProvideDianaPresetDaoFactory(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        this.module = portfolioDatabaseModule;
        this.dbProvider = provider;
    }

    @DexIgnore
    public static PortfolioDatabaseModule_ProvideDianaPresetDaoFactory create(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return new PortfolioDatabaseModule_ProvideDianaPresetDaoFactory(portfolioDatabaseModule, provider);
    }

    @DexIgnore
    public static DianaPresetDao provideInstance(PortfolioDatabaseModule portfolioDatabaseModule, Provider<DianaCustomizeDatabase> provider) {
        return proxyProvideDianaPresetDao(portfolioDatabaseModule, provider.get());
    }

    @DexIgnore
    public static DianaPresetDao proxyProvideDianaPresetDao(PortfolioDatabaseModule portfolioDatabaseModule, DianaCustomizeDatabase dianaCustomizeDatabase) {
        DianaPresetDao provideDianaPresetDao = portfolioDatabaseModule.provideDianaPresetDao(dianaCustomizeDatabase);
        z76.a(provideDianaPresetDao, "Cannot return null from a non-@Nullable @Provides method");
        return provideDianaPresetDao;
    }

    @DexIgnore
    public DianaPresetDao get() {
        return provideInstance(this.module, this.dbProvider);
    }
}
