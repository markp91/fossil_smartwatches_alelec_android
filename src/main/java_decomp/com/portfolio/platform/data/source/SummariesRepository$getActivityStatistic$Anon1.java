package com.portfolio.platform.data.source;

import androidx.lifecycle.LiveData;
import com.fossil.af6;
import com.fossil.ik6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.rm6;
import com.fossil.rx6;
import com.fossil.tx5;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityStatistic;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SummariesRepository$getActivityStatistic$Anon1 extends tx5<ActivityStatistic, ActivityStatistic> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $shouldFetch;
    @DexIgnore
    public /* final */ /* synthetic */ SummariesRepository this$0;

    @DexIgnore
    public SummariesRepository$getActivityStatistic$Anon1(SummariesRepository summariesRepository, boolean z) {
        this.this$0 = summariesRepository;
        this.$shouldFetch = z;
    }

    @DexIgnore
    public Object createCall(xe6<? super rx6<ActivityStatistic>> xe6) {
        return this.this$0.mApiServiceV2.getActivityStatistic(xe6);
    }

    @DexIgnore
    public LiveData<ActivityStatistic> loadFromDb() {
        return this.this$0.mActivitySummaryDao.getActivityStatisticLiveData();
    }

    @DexIgnore
    public void onFetchFailed(Throwable th) {
        FLogger.INSTANCE.getLocal().d(SummariesRepository.TAG, "getActivityStatistic - onFetchFailed");
    }

    @DexIgnore
    public void saveCallResult(ActivityStatistic activityStatistic) {
        wg6.b(activityStatistic, "item");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d(SummariesRepository.TAG, "getActivityStatistic - saveCallResult -- item=" + activityStatistic);
        rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new SummariesRepository$getActivityStatistic$Anon1$saveCallResult$Anon1_Level2(this, activityStatistic, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public boolean shouldFetch(ActivityStatistic activityStatistic) {
        return this.$shouldFetch;
    }
}
