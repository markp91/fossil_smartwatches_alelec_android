package com.portfolio.platform.data;

import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WechatAccessToken {
    @DexIgnore
    @vu3("access_token")
    public /* final */ String mAccessToken;
    @DexIgnore
    @vu3("expires_in")
    public /* final */ int mExpiresIn;
    @DexIgnore
    @vu3("openid")
    public /* final */ String mOpenId;
    @DexIgnore
    @vu3("refresh_token")
    public /* final */ String mRefreshToken;
    @DexIgnore
    @vu3("scope")
    public /* final */ String mScope;

    @DexIgnore
    public WechatAccessToken(String str, int i, String str2, String str3, String str4) {
        wg6.b(str, "mAccessToken");
        wg6.b(str2, "mRefreshToken");
        wg6.b(str3, "mOpenId");
        wg6.b(str4, "mScope");
        this.mAccessToken = str;
        this.mExpiresIn = i;
        this.mRefreshToken = str2;
        this.mOpenId = str3;
        this.mScope = str4;
    }

    @DexIgnore
    public final String getAccessToken() {
        return this.mAccessToken;
    }

    @DexIgnore
    public final int getExpiresIn() {
        return this.mExpiresIn;
    }

    @DexIgnore
    public final String getOpenId() {
        return this.mOpenId;
    }

    @DexIgnore
    public final String getRefreshToken() {
        return this.mRefreshToken;
    }

    @DexIgnore
    public final String getScope() {
        return this.mScope;
    }
}
