package com.portfolio.platform.data;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFDeviceModel {
    @DexIgnore
    public static /* final */ String DIANA; // = "DN.0.0";
    @DexIgnore
    public static /* final */ String MINI; // = "HM.0.0";
    @DexIgnore
    public static /* final */ String Q_MOTION; // = "B0.0.1";
    @DexIgnore
    public static /* final */ String RAY; // = "B0.0.0";
    @DexIgnore
    public static /* final */ String RMM; // = "C1.1.0";
    @DexIgnore
    public static /* final */ String RMM_FOSSIL; // = "C1.1.1";
    @DexIgnore
    public static /* final */ String SAM; // = "HW.0.0";

    @DexIgnore
    public static boolean isSame(String str, String str2) {
        return str2.startsWith(str);
    }
}
