package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory implements Factory<MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ MicroAppSettingRepositoryModule module;

    @DexIgnore
    public MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        this.module = microAppSettingRepositoryModule;
    }

    @DexIgnore
    public static MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory create(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        return new MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetLocalDataSourceFactory(microAppSettingRepositoryModule);
    }

    @DexIgnore
    public static MicroAppSettingDataSource provideInstance(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        return proxyProvideFavoriteMappingSetLocalDataSource(microAppSettingRepositoryModule);
    }

    @DexIgnore
    public static MicroAppSettingDataSource proxyProvideFavoriteMappingSetLocalDataSource(MicroAppSettingRepositoryModule microAppSettingRepositoryModule) {
        MicroAppSettingDataSource provideFavoriteMappingSetLocalDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetLocalDataSource();
        z76.a(provideFavoriteMappingSetLocalDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetLocalDataSource;
    }

    @DexIgnore
    public MicroAppSettingDataSource get() {
        return provideInstance(this.module);
    }
}
