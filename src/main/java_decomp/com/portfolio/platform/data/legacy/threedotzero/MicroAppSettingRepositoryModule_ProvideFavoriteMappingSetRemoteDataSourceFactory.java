package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.u04;
import com.fossil.z76;
import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory implements Factory<MicroAppSettingDataSource> {
    @DexIgnore
    public /* final */ Provider<u04> appExecutorsProvider;
    @DexIgnore
    public /* final */ MicroAppSettingRepositoryModule module;
    @DexIgnore
    public /* final */ Provider<ShortcutApiService> shortcutApiServiceProvider;

    @DexIgnore
    public MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<u04> provider2) {
        this.module = microAppSettingRepositoryModule;
        this.shortcutApiServiceProvider = provider;
        this.appExecutorsProvider = provider2;
    }

    @DexIgnore
    public static MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory create(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<u04> provider2) {
        return new MicroAppSettingRepositoryModule_ProvideFavoriteMappingSetRemoteDataSourceFactory(microAppSettingRepositoryModule, provider, provider2);
    }

    @DexIgnore
    public static MicroAppSettingDataSource provideInstance(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, Provider<ShortcutApiService> provider, Provider<u04> provider2) {
        return proxyProvideFavoriteMappingSetRemoteDataSource(microAppSettingRepositoryModule, provider.get(), provider2.get());
    }

    @DexIgnore
    public static MicroAppSettingDataSource proxyProvideFavoriteMappingSetRemoteDataSource(MicroAppSettingRepositoryModule microAppSettingRepositoryModule, ShortcutApiService shortcutApiService, u04 u04) {
        MicroAppSettingDataSource provideFavoriteMappingSetRemoteDataSource = microAppSettingRepositoryModule.provideFavoriteMappingSetRemoteDataSource(shortcutApiService, u04);
        z76.a(provideFavoriteMappingSetRemoteDataSource, "Cannot return null from a non-@Nullable @Provides method");
        return provideFavoriteMappingSetRemoteDataSource;
    }

    @DexIgnore
    public MicroAppSettingDataSource get() {
        return provideInstance(this.module, this.shortcutApiServiceProvider, this.appExecutorsProvider);
    }
}
