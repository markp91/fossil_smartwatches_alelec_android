package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.z76;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory implements Factory<MicroAppVariantDataSource> {
    @DexIgnore
    public /* final */ MicroAppVariantRepositoryModule module;

    @DexIgnore
    public MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        this.module = microAppVariantRepositoryModule;
    }

    @DexIgnore
    public static MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory create(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        return new MicroAppVariantRepositoryModule_ProvideMicroAppVariantLocalDataSource$app_fossilReleaseFactory(microAppVariantRepositoryModule);
    }

    @DexIgnore
    public static MicroAppVariantDataSource provideInstance(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        return proxyProvideMicroAppVariantLocalDataSource$app_fossilRelease(microAppVariantRepositoryModule);
    }

    @DexIgnore
    public static MicroAppVariantDataSource proxyProvideMicroAppVariantLocalDataSource$app_fossilRelease(MicroAppVariantRepositoryModule microAppVariantRepositoryModule) {
        MicroAppVariantDataSource provideMicroAppVariantLocalDataSource$app_fossilRelease = microAppVariantRepositoryModule.provideMicroAppVariantLocalDataSource$app_fossilRelease();
        z76.a(provideMicroAppVariantLocalDataSource$app_fossilRelease, "Cannot return null from a non-@Nullable @Provides method");
        return provideMicroAppVariantLocalDataSource$app_fossilRelease;
    }

    @DexIgnore
    public MicroAppVariantDataSource get() {
        return provideInstance(this.module);
    }
}
