package com.portfolio.platform.data.legacy.threedotzero;

import com.fossil.wearables.fsl.BaseProvider;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.portfolio.platform.data.legacy.threedotzero.MappingSet;
import com.portfolio.platform.data.model.SKUModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface DeviceProvider extends BaseProvider {
    @DexIgnore
    boolean addOrUpdateActivePreset(ActivePreset activePreset);

    @DexIgnore
    boolean addOrUpdateDeclarationFile(DeclarationFile declarationFile);

    @DexIgnore
    void addOrUpdateDevice(DeviceModel deviceModel);

    @DexIgnore
    boolean addOrUpdateMicroApp(MicroApp microApp);

    @DexIgnore
    boolean addOrUpdateMicroAppVariant(MicroAppVariant microAppVariant);

    @DexIgnore
    boolean addOrUpdateRecommendedPreset(RecommendedPreset recommendedPreset);

    @DexIgnore
    boolean addOrUpdateSavedPreset(SavedPreset savedPreset);

    @DexIgnore
    void addOrUpdateUAppSystemVersionModel(UAppSystemVersionModel uAppSystemVersionModel);

    @DexIgnore
    void clearActivePreset();

    @DexIgnore
    void clearAllDevices();

    @DexIgnore
    void clearDeclarationFiles();

    @DexIgnore
    boolean clearListSKUsSupported();

    @DexIgnore
    void clearMicroApp();

    @DexIgnore
    void clearRecommendedPreset();

    @DexIgnore
    void clearSavedPreset();

    @DexIgnore
    void clearVariant();

    @DexIgnore
    boolean deleteDeclarationFileByVariant(MicroAppVariant microAppVariant);

    @DexIgnore
    boolean deleteMicroAppList(String str);

    @DexIgnore
    void deleteMicroAppVariants(String str, int i, int i2);

    @DexIgnore
    boolean deleteRecommendedPreset(String str);

    @DexIgnore
    boolean deleteSavedPreset(String str);

    @DexIgnore
    MappingSet getActiveMappingSetByDeviceFamily(MFDeviceFamily mFDeviceFamily);

    @DexIgnore
    ActivePreset getActivePreset(String str);

    @DexIgnore
    List<ActivePreset> getAllActivePreset();

    @DexIgnore
    List<DeviceModel> getAllDevice();

    @DexIgnore
    List<MicroApp> getAllMicroApp();

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariant();

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariant(String str, int i);

    @DexIgnore
    List<MicroAppVariant> getAllMicroAppVariantUniqueBySerial();

    @DexIgnore
    List<RecommendedPreset> getAllRecommendedPreset();

    @DexIgnore
    List<SavedPreset> getAllSavedPreset();

    @DexIgnore
    List<SavedPreset> getAllSavedPresets();

    @DexIgnore
    List<SavedPreset> getAllSavedPresetsExcludeDeleteType();

    @DexIgnore
    RecommendedPreset getDefaultPreset(String str);

    @DexIgnore
    DeviceModel getDeviceById(String str);

    @DexIgnore
    List<MicroApp> getListMicroApp(String str);

    @DexIgnore
    List<RecommendedPreset> getListRecommendedPresetBySerial(String str);

    @DexIgnore
    List<SKUModel> getListSKUsSupported();

    @DexIgnore
    List<MappingSet> getMappingSetByType(MappingSet.MappingSetType mappingSetType);

    @DexIgnore
    MicroApp getMicroApp(String str, String str2);

    @DexIgnore
    MicroAppVariant getMicroAppVariant(String str, String str2, int i, String str3);

    @DexIgnore
    List<ActivePreset> getPendingActivePresets();

    @DexIgnore
    List<SavedPreset> getPendingDeletedUserPresets();

    @DexIgnore
    List<SavedPreset> getPendingUserPresets();

    @DexIgnore
    SKUModel getSKUModelBySerialNumberPrefix(String str);

    @DexIgnore
    SavedPreset getSavedPresetById(String str);

    @DexIgnore
    UAppSystemVersionModel getUAppSystemVersionModel(String str);

    @DexIgnore
    void removeDevice(String str);

    @DexIgnore
    void setListSKUsSupported(List<SKUModel> list);

    @DexIgnore
    void updateBatteryLevel(String str, int i);

    @DexIgnore
    void updateListMicroApp(List<MicroApp> list);
}
