package com.portfolio.platform.data;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class AppFilterHistory implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -2158356172743946820L;
    @DexIgnore
    public String color;
    @DexIgnore
    public String haptic;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ String senderName;
    @DexIgnore
    public String subtitle;
    @DexIgnore
    public String title;
    @DexIgnore
    public String type;

    @DexIgnore
    public AppFilterHistory(String str) {
        this.senderName = str;
    }

    @DexIgnore
    public String getColor() {
        return this.color;
    }

    @DexIgnore
    public String getHaptic() {
        return this.haptic;
    }

    @DexIgnore
    public int getId() {
        return this.id;
    }

    @DexIgnore
    public String getSenderName() {
        return this.senderName;
    }

    @DexIgnore
    public String getSubTitle() {
        return this.subtitle;
    }

    @DexIgnore
    public String getTitle() {
        return this.title;
    }

    @DexIgnore
    public String getType() {
        return this.type;
    }

    @DexIgnore
    public void setColor(String str) {
        this.color = str;
    }

    @DexIgnore
    public void setHaptic(String str) {
        this.haptic = str;
    }

    @DexIgnore
    public void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public void setSubTitle(String str) {
        this.subtitle = str;
    }

    @DexIgnore
    public void setTitle(String str) {
        this.title = str;
    }

    @DexIgnore
    public void setType(String str) {
        this.type = str;
    }
}
