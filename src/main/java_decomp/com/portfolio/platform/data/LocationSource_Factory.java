package com.portfolio.platform.data;

import com.portfolio.platform.data.source.local.AddressDao;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocationSource_Factory implements Factory<LocationSource> {
    @DexIgnore
    public /* final */ Provider<AddressDao> addressDaoProvider;

    @DexIgnore
    public LocationSource_Factory(Provider<AddressDao> provider) {
        this.addressDaoProvider = provider;
    }

    @DexIgnore
    public static LocationSource_Factory create(Provider<AddressDao> provider) {
        return new LocationSource_Factory(provider);
    }

    @DexIgnore
    public static LocationSource newLocationSource() {
        return new LocationSource();
    }

    @DexIgnore
    public static LocationSource provideInstance(Provider<AddressDao> provider) {
        LocationSource locationSource = new LocationSource();
        LocationSource_MembersInjector.injectAddressDao(locationSource, provider.get());
        return locationSource;
    }

    @DexIgnore
    public LocationSource get() {
        return provideInstance(this.addressDaoProvider);
    }
}
