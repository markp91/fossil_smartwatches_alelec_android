package com.portfolio.platform.data;

import com.fossil.b;
import com.fossil.d;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.util.Date;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ActivityStatistic {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String ID; // = "id";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "activity_statistic";
    @DexIgnore
    @vu3("activeTimeBestDay")
    public /* final */ ActivityDailyBest activeTimeBestDay;
    @DexIgnore
    @vu3("activeTimeBestStreak")
    public /* final */ ActivityDailyBest activeTimeBestStreak;
    @DexIgnore
    @vu3("caloriesBestDay")
    public /* final */ CaloriesBestDay caloriesBestDay;
    @DexIgnore
    @vu3("caloriesBestStreak")
    public /* final */ ActivityDailyBest caloriesBestStreak;
    @DexIgnore
    @vu3("createdAt")
    public /* final */ DateTime createdAt;
    @DexIgnore
    @vu3("id")
    public /* final */ String id;
    @DexIgnore
    @vu3("stepsBestDay")
    public /* final */ ActivityDailyBest stepsBestDay;
    @DexIgnore
    @vu3("stepsBestStreak")
    public /* final */ ActivityDailyBest stepsBestStreak;
    @DexIgnore
    @vu3("totalActiveTime")
    public /* final */ int totalActiveTime;
    @DexIgnore
    @vu3("totalCalories")
    public /* final */ double totalCalories;
    @DexIgnore
    @vu3("totalDays")
    public /* final */ int totalDays;
    @DexIgnore
    @vu3("totalDistance")
    public /* final */ double totalDistance;
    @DexIgnore
    @vu3("totalIntensityDistInStep")
    public /* final */ List<Integer> totalIntensityDistInStep;
    @DexIgnore
    @vu3("totalSteps")
    public /* final */ int totalSteps;
    @DexIgnore
    @vu3("uid")
    public /* final */ String uid;
    @DexIgnore
    @vu3("updatedAt")
    public /* final */ DateTime updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class ActivityDailyBest {
        @DexIgnore
        public /* final */ String activityDailySummaryId;
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore
        public ActivityDailyBest(String str, Date date2, int i) {
            wg6.b(str, "activityDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            this.activityDailySummaryId = str;
            this.date = date2;
            this.value = i;
        }

        @DexIgnore
        public static /* synthetic */ ActivityDailyBest copy$default(ActivityDailyBest activityDailyBest, String str, Date date2, int i, int i2, Object obj) {
            if ((i2 & 1) != 0) {
                str = activityDailyBest.activityDailySummaryId;
            }
            if ((i2 & 2) != 0) {
                date2 = activityDailyBest.date;
            }
            if ((i2 & 4) != 0) {
                i = activityDailyBest.value;
            }
            return activityDailyBest.copy(str, date2, i);
        }

        @DexIgnore
        public final String component1() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final int component3() {
            return this.value;
        }

        @DexIgnore
        public final ActivityDailyBest copy(String str, Date date2, int i) {
            wg6.b(str, "activityDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            return new ActivityDailyBest(str, date2, i);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof ActivityDailyBest)) {
                return false;
            }
            ActivityDailyBest activityDailyBest = (ActivityDailyBest) obj;
            return wg6.a((Object) this.activityDailySummaryId, (Object) activityDailyBest.activityDailySummaryId) && wg6.a((Object) this.date, (Object) activityDailyBest.date) && this.value == activityDailyBest.value;
        }

        @DexIgnore
        public final String getActivityDailySummaryId() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.activityDailySummaryId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return ((hashCode + i) * 31) + d.a(this.value);
        }

        @DexIgnore
        public String toString() {
            return "ActivityDailyBest(activityDailySummaryId=" + this.activityDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CaloriesBestDay {
        @DexIgnore
        public /* final */ String activityDailySummaryId;
        @DexIgnore
        public /* final */ Date date;
        @DexIgnore
        public /* final */ double value;

        @DexIgnore
        public CaloriesBestDay(String str, Date date2, double d) {
            wg6.b(str, "activityDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            this.activityDailySummaryId = str;
            this.date = date2;
            this.value = d;
        }

        @DexIgnore
        public static /* synthetic */ CaloriesBestDay copy$default(CaloriesBestDay caloriesBestDay, String str, Date date2, double d, int i, Object obj) {
            if ((i & 1) != 0) {
                str = caloriesBestDay.activityDailySummaryId;
            }
            if ((i & 2) != 0) {
                date2 = caloriesBestDay.date;
            }
            if ((i & 4) != 0) {
                d = caloriesBestDay.value;
            }
            return caloriesBestDay.copy(str, date2, d);
        }

        @DexIgnore
        public final String component1() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date component2() {
            return this.date;
        }

        @DexIgnore
        public final double component3() {
            return this.value;
        }

        @DexIgnore
        public final CaloriesBestDay copy(String str, Date date2, double d) {
            wg6.b(str, "activityDailySummaryId");
            wg6.b(date2, HardwareLog.COLUMN_DATE);
            return new CaloriesBestDay(str, date2, d);
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof CaloriesBestDay)) {
                return false;
            }
            CaloriesBestDay caloriesBestDay = (CaloriesBestDay) obj;
            return wg6.a((Object) this.activityDailySummaryId, (Object) caloriesBestDay.activityDailySummaryId) && wg6.a((Object) this.date, (Object) caloriesBestDay.date) && Double.compare(this.value, caloriesBestDay.value) == 0;
        }

        @DexIgnore
        public final String getActivityDailySummaryId() {
            return this.activityDailySummaryId;
        }

        @DexIgnore
        public final Date getDate() {
            return this.date;
        }

        @DexIgnore
        public final double getValue() {
            return this.value;
        }

        @DexIgnore
        public int hashCode() {
            String str = this.activityDailySummaryId;
            int i = 0;
            int hashCode = (str != null ? str.hashCode() : 0) * 31;
            Date date2 = this.date;
            if (date2 != null) {
                i = date2.hashCode();
            }
            return ((hashCode + i) * 31) + b.a(this.value);
        }

        @DexIgnore
        public String toString() {
            return "CaloriesBestDay(activityDailySummaryId=" + this.activityDailySummaryId + ", date=" + this.date + ", value=" + this.value + ")";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public ActivityStatistic(String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        List<Integer> list2 = list;
        DateTime dateTime3 = dateTime;
        DateTime dateTime4 = dateTime2;
        wg6.b(str, "id");
        wg6.b(str2, "uid");
        wg6.b(list2, "totalIntensityDistInStep");
        wg6.b(dateTime3, "createdAt");
        wg6.b(dateTime4, "updatedAt");
        this.id = str;
        this.uid = str2;
        this.activeTimeBestDay = activityDailyBest;
        this.activeTimeBestStreak = activityDailyBest2;
        this.caloriesBestDay = caloriesBestDay2;
        this.caloriesBestStreak = activityDailyBest3;
        this.stepsBestDay = activityDailyBest4;
        this.stepsBestStreak = activityDailyBest5;
        this.totalActiveTime = i;
        this.totalCalories = d;
        this.totalDays = i2;
        this.totalDistance = d2;
        this.totalSteps = i3;
        this.totalIntensityDistInStep = list2;
        this.createdAt = dateTime3;
        this.updatedAt = dateTime4;
    }

    @DexIgnore
    public static /* synthetic */ ActivityStatistic copy$default(ActivityStatistic activityStatistic, String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List list, DateTime dateTime, DateTime dateTime2, int i4, Object obj) {
        ActivityStatistic activityStatistic2 = activityStatistic;
        int i5 = i4;
        return activityStatistic.copy((i5 & 1) != 0 ? activityStatistic2.id : str, (i5 & 2) != 0 ? activityStatistic2.uid : str2, (i5 & 4) != 0 ? activityStatistic2.activeTimeBestDay : activityDailyBest, (i5 & 8) != 0 ? activityStatistic2.activeTimeBestStreak : activityDailyBest2, (i5 & 16) != 0 ? activityStatistic2.caloriesBestDay : caloriesBestDay2, (i5 & 32) != 0 ? activityStatistic2.caloriesBestStreak : activityDailyBest3, (i5 & 64) != 0 ? activityStatistic2.stepsBestDay : activityDailyBest4, (i5 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? activityStatistic2.stepsBestStreak : activityDailyBest5, (i5 & 256) != 0 ? activityStatistic2.totalActiveTime : i, (i5 & 512) != 0 ? activityStatistic2.totalCalories : d, (i5 & 1024) != 0 ? activityStatistic2.totalDays : i2, (i5 & 2048) != 0 ? activityStatistic2.totalDistance : d2, (i5 & 4096) != 0 ? activityStatistic2.totalSteps : i3, (i5 & 8192) != 0 ? activityStatistic2.totalIntensityDistInStep : list, (i5 & 16384) != 0 ? activityStatistic2.createdAt : dateTime, (i5 & 32768) != 0 ? activityStatistic2.updatedAt : dateTime2);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final double component10() {
        return this.totalCalories;
    }

    @DexIgnore
    public final int component11() {
        return this.totalDays;
    }

    @DexIgnore
    public final double component12() {
        return this.totalDistance;
    }

    @DexIgnore
    public final int component13() {
        return this.totalSteps;
    }

    @DexIgnore
    public final List<Integer> component14() {
        return this.totalIntensityDistInStep;
    }

    @DexIgnore
    public final DateTime component15() {
        return this.createdAt;
    }

    @DexIgnore
    public final DateTime component16() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component2() {
        return this.uid;
    }

    @DexIgnore
    public final ActivityDailyBest component3() {
        return this.activeTimeBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component4() {
        return this.activeTimeBestStreak;
    }

    @DexIgnore
    public final CaloriesBestDay component5() {
        return this.caloriesBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component6() {
        return this.caloriesBestStreak;
    }

    @DexIgnore
    public final ActivityDailyBest component7() {
        return this.stepsBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest component8() {
        return this.stepsBestStreak;
    }

    @DexIgnore
    public final int component9() {
        return this.totalActiveTime;
    }

    @DexIgnore
    public final ActivityStatistic copy(String str, String str2, ActivityDailyBest activityDailyBest, ActivityDailyBest activityDailyBest2, CaloriesBestDay caloriesBestDay2, ActivityDailyBest activityDailyBest3, ActivityDailyBest activityDailyBest4, ActivityDailyBest activityDailyBest5, int i, double d, int i2, double d2, int i3, List<Integer> list, DateTime dateTime, DateTime dateTime2) {
        String str3 = str;
        wg6.b(str3, "id");
        wg6.b(str2, "uid");
        wg6.b(list, "totalIntensityDistInStep");
        wg6.b(dateTime, "createdAt");
        wg6.b(dateTime2, "updatedAt");
        return new ActivityStatistic(str3, str2, activityDailyBest, activityDailyBest2, caloriesBestDay2, activityDailyBest3, activityDailyBest4, activityDailyBest5, i, d, i2, d2, i3, list, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityStatistic)) {
            return false;
        }
        ActivityStatistic activityStatistic = (ActivityStatistic) obj;
        return wg6.a((Object) this.id, (Object) activityStatistic.id) && wg6.a((Object) this.uid, (Object) activityStatistic.uid) && wg6.a((Object) this.activeTimeBestDay, (Object) activityStatistic.activeTimeBestDay) && wg6.a((Object) this.activeTimeBestStreak, (Object) activityStatistic.activeTimeBestStreak) && wg6.a((Object) this.caloriesBestDay, (Object) activityStatistic.caloriesBestDay) && wg6.a((Object) this.caloriesBestStreak, (Object) activityStatistic.caloriesBestStreak) && wg6.a((Object) this.stepsBestDay, (Object) activityStatistic.stepsBestDay) && wg6.a((Object) this.stepsBestStreak, (Object) activityStatistic.stepsBestStreak) && this.totalActiveTime == activityStatistic.totalActiveTime && Double.compare(this.totalCalories, activityStatistic.totalCalories) == 0 && this.totalDays == activityStatistic.totalDays && Double.compare(this.totalDistance, activityStatistic.totalDistance) == 0 && this.totalSteps == activityStatistic.totalSteps && wg6.a((Object) this.totalIntensityDistInStep, (Object) activityStatistic.totalIntensityDistInStep) && wg6.a((Object) this.createdAt, (Object) activityStatistic.createdAt) && wg6.a((Object) this.updatedAt, (Object) activityStatistic.updatedAt);
    }

    @DexIgnore
    public final ActivityDailyBest getActiveTimeBestDay() {
        return this.activeTimeBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getActiveTimeBestStreak() {
        return this.activeTimeBestStreak;
    }

    @DexIgnore
    public final CaloriesBestDay getCaloriesBestDay() {
        return this.caloriesBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getCaloriesBestStreak() {
        return this.caloriesBestStreak;
    }

    @DexIgnore
    public final DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final ActivityDailyBest getStepsBestDay() {
        return this.stepsBestDay;
    }

    @DexIgnore
    public final ActivityDailyBest getStepsBestStreak() {
        return this.stepsBestStreak;
    }

    @DexIgnore
    public final int getTotalActiveTime() {
        return this.totalActiveTime;
    }

    @DexIgnore
    public final double getTotalCalories() {
        return this.totalCalories;
    }

    @DexIgnore
    public final int getTotalDays() {
        return this.totalDays;
    }

    @DexIgnore
    public final double getTotalDistance() {
        return this.totalDistance;
    }

    @DexIgnore
    public final List<Integer> getTotalIntensityDistInStep() {
        return this.totalIntensityDistInStep;
    }

    @DexIgnore
    public final int getTotalSteps() {
        return this.totalSteps;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.uid;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        ActivityDailyBest activityDailyBest = this.activeTimeBestDay;
        int hashCode3 = (hashCode2 + (activityDailyBest != null ? activityDailyBest.hashCode() : 0)) * 31;
        ActivityDailyBest activityDailyBest2 = this.activeTimeBestStreak;
        int hashCode4 = (hashCode3 + (activityDailyBest2 != null ? activityDailyBest2.hashCode() : 0)) * 31;
        CaloriesBestDay caloriesBestDay2 = this.caloriesBestDay;
        int hashCode5 = (hashCode4 + (caloriesBestDay2 != null ? caloriesBestDay2.hashCode() : 0)) * 31;
        ActivityDailyBest activityDailyBest3 = this.caloriesBestStreak;
        int hashCode6 = (hashCode5 + (activityDailyBest3 != null ? activityDailyBest3.hashCode() : 0)) * 31;
        ActivityDailyBest activityDailyBest4 = this.stepsBestDay;
        int hashCode7 = (hashCode6 + (activityDailyBest4 != null ? activityDailyBest4.hashCode() : 0)) * 31;
        ActivityDailyBest activityDailyBest5 = this.stepsBestStreak;
        int hashCode8 = (((((((((((hashCode7 + (activityDailyBest5 != null ? activityDailyBest5.hashCode() : 0)) * 31) + d.a(this.totalActiveTime)) * 31) + b.a(this.totalCalories)) * 31) + d.a(this.totalDays)) * 31) + b.a(this.totalDistance)) * 31) + d.a(this.totalSteps)) * 31;
        List<Integer> list = this.totalIntensityDistInStep;
        int hashCode9 = (hashCode8 + (list != null ? list.hashCode() : 0)) * 31;
        DateTime dateTime = this.createdAt;
        int hashCode10 = (hashCode9 + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.updatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode10 + i;
    }

    @DexIgnore
    public String toString() {
        return "ActivityStatistic(id=" + this.id + ", uid=" + this.uid + ", activeTimeBestDay=" + this.activeTimeBestDay + ", activeTimeBestStreak=" + this.activeTimeBestStreak + ", caloriesBestDay=" + this.caloriesBestDay + ", caloriesBestStreak=" + this.caloriesBestStreak + ", stepsBestDay=" + this.stepsBestDay + ", stepsBestStreak=" + this.stepsBestStreak + ", totalActiveTime=" + this.totalActiveTime + ", totalCalories=" + this.totalCalories + ", totalDays=" + this.totalDays + ", totalDistance=" + this.totalDistance + ", totalSteps=" + this.totalSteps + ", totalIntensityDistInStep=" + this.totalIntensityDistInStep + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
