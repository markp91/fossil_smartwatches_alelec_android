package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.b;
import com.fossil.d;
import com.fossil.fitness.Distance;
import com.fossil.wg6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DistanceWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public double total;
    @DexIgnore
    public List<Double> values;

    @DexIgnore
    public DistanceWrapper(int i, List<Double> list, double d) {
        wg6.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = d;
    }

    @DexIgnore
    public static /* synthetic */ DistanceWrapper copy$default(DistanceWrapper distanceWrapper, int i, List<Double> list, double d, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = distanceWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = distanceWrapper.values;
        }
        if ((i2 & 4) != 0) {
            d = distanceWrapper.total;
        }
        return distanceWrapper.copy(i, list, d);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Double> component2() {
        return this.values;
    }

    @DexIgnore
    public final double component3() {
        return this.total;
    }

    @DexIgnore
    public final DistanceWrapper copy(int i, List<Double> list, double d) {
        wg6.b(list, "values");
        return new DistanceWrapper(i, list, d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DistanceWrapper)) {
            return false;
        }
        DistanceWrapper distanceWrapper = (DistanceWrapper) obj;
        return this.resolutionInSecond == distanceWrapper.resolutionInSecond && wg6.a((Object) this.values, (Object) distanceWrapper.values) && Double.compare(this.total, distanceWrapper.total) == 0;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final double getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Double> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolutionInSecond) * 31;
        List<Double> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + b.a(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(double d) {
        this.total = d;
    }

    @DexIgnore
    public final void setValues(List<Double> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "DistanceWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public DistanceWrapper(Distance distance) {
        this(r0, r1, distance.getTotal());
        wg6.b(distance, "distance");
        int resolutionInSecond2 = distance.getResolutionInSecond();
        ArrayList values2 = distance.getValues();
        wg6.a((Object) values2, "distance.values");
    }
}
