package com.portfolio.platform.data.model.goaltracking.response;

import com.fossil.d;
import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import java.util.Date;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalDailySummary {
    @DexIgnore
    @vu3("createdAt")
    public DateTime mCreatedAt;
    @DexIgnore
    @vu3("date")
    public Date mDate;
    @DexIgnore
    @vu3("goalTarget")
    public int mGoalTarget;
    @DexIgnore
    @vu3("totalTracked")
    public int mTotalTracked;
    @DexIgnore
    @vu3("updatedAt")
    public DateTime mUpdatedAt;

    @DexIgnore
    public GoalDailySummary(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        wg6.b(date, "mDate");
        wg6.b(dateTime, "mCreatedAt");
        wg6.b(dateTime2, "mUpdatedAt");
        this.mDate = date;
        this.mTotalTracked = i;
        this.mGoalTarget = i2;
        this.mCreatedAt = dateTime;
        this.mUpdatedAt = dateTime2;
    }

    @DexIgnore
    public static /* synthetic */ GoalDailySummary copy$default(GoalDailySummary goalDailySummary, Date date, int i, int i2, DateTime dateTime, DateTime dateTime2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            date = goalDailySummary.mDate;
        }
        if ((i3 & 2) != 0) {
            i = goalDailySummary.mTotalTracked;
        }
        int i4 = i;
        if ((i3 & 4) != 0) {
            i2 = goalDailySummary.mGoalTarget;
        }
        int i5 = i2;
        if ((i3 & 8) != 0) {
            dateTime = goalDailySummary.mCreatedAt;
        }
        DateTime dateTime3 = dateTime;
        if ((i3 & 16) != 0) {
            dateTime2 = goalDailySummary.mUpdatedAt;
        }
        return goalDailySummary.copy(date, i4, i5, dateTime3, dateTime2);
    }

    @DexIgnore
    public final Date component1() {
        return this.mDate;
    }

    @DexIgnore
    public final int component2() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final int component3() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final DateTime component4() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final DateTime component5() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public final GoalDailySummary copy(Date date, int i, int i2, DateTime dateTime, DateTime dateTime2) {
        wg6.b(date, "mDate");
        wg6.b(dateTime, "mCreatedAt");
        wg6.b(dateTime2, "mUpdatedAt");
        return new GoalDailySummary(date, i, i2, dateTime, dateTime2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GoalDailySummary)) {
            return false;
        }
        GoalDailySummary goalDailySummary = (GoalDailySummary) obj;
        return wg6.a((Object) this.mDate, (Object) goalDailySummary.mDate) && this.mTotalTracked == goalDailySummary.mTotalTracked && this.mGoalTarget == goalDailySummary.mGoalTarget && wg6.a((Object) this.mCreatedAt, (Object) goalDailySummary.mCreatedAt) && wg6.a((Object) this.mUpdatedAt, (Object) goalDailySummary.mUpdatedAt);
    }

    @DexIgnore
    public final DateTime getMCreatedAt() {
        return this.mCreatedAt;
    }

    @DexIgnore
    public final Date getMDate() {
        return this.mDate;
    }

    @DexIgnore
    public final int getMGoalTarget() {
        return this.mGoalTarget;
    }

    @DexIgnore
    public final int getMTotalTracked() {
        return this.mTotalTracked;
    }

    @DexIgnore
    public final DateTime getMUpdatedAt() {
        return this.mUpdatedAt;
    }

    @DexIgnore
    public int hashCode() {
        Date date = this.mDate;
        int i = 0;
        int hashCode = (((((date != null ? date.hashCode() : 0) * 31) + d.a(this.mTotalTracked)) * 31) + d.a(this.mGoalTarget)) * 31;
        DateTime dateTime = this.mCreatedAt;
        int hashCode2 = (hashCode + (dateTime != null ? dateTime.hashCode() : 0)) * 31;
        DateTime dateTime2 = this.mUpdatedAt;
        if (dateTime2 != null) {
            i = dateTime2.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setMCreatedAt(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.mCreatedAt = dateTime;
    }

    @DexIgnore
    public final void setMDate(Date date) {
        wg6.b(date, "<set-?>");
        this.mDate = date;
    }

    @DexIgnore
    public final void setMGoalTarget(int i) {
        this.mGoalTarget = i;
    }

    @DexIgnore
    public final void setMTotalTracked(int i) {
        this.mTotalTracked = i;
    }

    @DexIgnore
    public final void setMUpdatedAt(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.mUpdatedAt = dateTime;
    }

    @DexIgnore
    public final GoalTrackingSummary toGoalTrackingSummary() {
        try {
            return new GoalTrackingSummary(this.mDate, this.mTotalTracked, this.mGoalTarget, this.mCreatedAt.getMillis(), this.mUpdatedAt.getMillis());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.e("GoalDailySummary", "toGoalTrackingSummary exception=" + e);
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String toString() {
        return "GoalDailySummary(mDate=" + this.mDate + ", mTotalTracked=" + this.mTotalTracked + ", mGoalTarget=" + this.mGoalTarget + ", mCreatedAt=" + this.mCreatedAt + ", mUpdatedAt=" + this.mUpdatedAt + ")";
    }
}
