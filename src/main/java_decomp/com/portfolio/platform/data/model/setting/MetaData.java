package com.portfolio.platform.data.model.setting;

import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.model.watchparams.Version;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetaData {
    @DexIgnore
    @vu3("locale")
    public String locale;
    @DexIgnore
    @vu3("version")
    public Version version;

    @DexIgnore
    public MetaData(String str, Version version2) {
        wg6.b(str, "locale");
        wg6.b(version2, "version");
        this.locale = str;
        this.version = version2;
    }

    @DexIgnore
    public static /* synthetic */ MetaData copy$default(MetaData metaData, String str, Version version2, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metaData.locale;
        }
        if ((i & 2) != 0) {
            version2 = metaData.version;
        }
        return metaData.copy(str, version2);
    }

    @DexIgnore
    public final String component1() {
        return this.locale;
    }

    @DexIgnore
    public final Version component2() {
        return this.version;
    }

    @DexIgnore
    public final MetaData copy(String str, Version version2) {
        wg6.b(str, "locale");
        wg6.b(version2, "version");
        return new MetaData(str, version2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MetaData)) {
            return false;
        }
        MetaData metaData = (MetaData) obj;
        return wg6.a((Object) this.locale, (Object) metaData.locale) && wg6.a((Object) this.version, (Object) metaData.version);
    }

    @DexIgnore
    public final String getLocale() {
        return this.locale;
    }

    @DexIgnore
    public final Version getVersion() {
        return this.version;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.locale;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        Version version2 = this.version;
        if (version2 != null) {
            i = version2.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public final void setLocale(String str) {
        wg6.b(str, "<set-?>");
        this.locale = str;
    }

    @DexIgnore
    public final void setVersion(Version version2) {
        wg6.b(version2, "<set-?>");
        this.version = version2;
    }

    @DexIgnore
    public String toString() {
        return "MetaData(locale=" + this.locale + ", version=" + this.version + ")";
    }
}
