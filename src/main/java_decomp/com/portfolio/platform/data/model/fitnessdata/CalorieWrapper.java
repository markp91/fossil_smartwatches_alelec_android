package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.aj6;
import com.fossil.c;
import com.fossil.d;
import com.fossil.fitness.Calorie;
import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.fossil.yd6;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CalorieWrapper {
    @DexIgnore
    public int resolutionInSecond;
    @DexIgnore
    public float total;
    @DexIgnore
    public List<Float> values;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Anon1 extends xg6 implements hg6<Byte, Float> {
        @DexIgnore
        public static /* final */ Anon1 INSTANCE; // = new Anon1();

        @DexIgnore
        public Anon1() {
            super(1);
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Float.valueOf(invoke((Byte) obj));
        }

        @DexIgnore
        public final float invoke(Byte b) {
            return (float) b.byteValue();
        }
    }

    @DexIgnore
    public CalorieWrapper(int i, List<Float> list, float f) {
        wg6.b(list, "values");
        this.resolutionInSecond = i;
        this.values = list;
        this.total = f;
    }

    @DexIgnore
    public static /* synthetic */ CalorieWrapper copy$default(CalorieWrapper calorieWrapper, int i, List<Float> list, float f, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = calorieWrapper.resolutionInSecond;
        }
        if ((i2 & 2) != 0) {
            list = calorieWrapper.values;
        }
        if ((i2 & 4) != 0) {
            f = calorieWrapper.total;
        }
        return calorieWrapper.copy(i, list, f);
    }

    @DexIgnore
    public final int component1() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final List<Float> component2() {
        return this.values;
    }

    @DexIgnore
    public final float component3() {
        return this.total;
    }

    @DexIgnore
    public final CalorieWrapper copy(int i, List<Float> list, float f) {
        wg6.b(list, "values");
        return new CalorieWrapper(i, list, f);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CalorieWrapper)) {
            return false;
        }
        CalorieWrapper calorieWrapper = (CalorieWrapper) obj;
        return this.resolutionInSecond == calorieWrapper.resolutionInSecond && wg6.a((Object) this.values, (Object) calorieWrapper.values) && Float.compare(this.total, calorieWrapper.total) == 0;
    }

    @DexIgnore
    public final int getResolutionInSecond() {
        return this.resolutionInSecond;
    }

    @DexIgnore
    public final float getTotal() {
        return this.total;
    }

    @DexIgnore
    public final List<Float> getValues() {
        return this.values;
    }

    @DexIgnore
    public int hashCode() {
        int a = d.a(this.resolutionInSecond) * 31;
        List<Float> list = this.values;
        return ((a + (list != null ? list.hashCode() : 0)) * 31) + c.a(this.total);
    }

    @DexIgnore
    public final void setResolutionInSecond(int i) {
        this.resolutionInSecond = i;
    }

    @DexIgnore
    public final void setTotal(float f) {
        this.total = f;
    }

    @DexIgnore
    public final void setValues(List<Float> list) {
        wg6.b(list, "<set-?>");
        this.values = list;
    }

    @DexIgnore
    public String toString() {
        return "CalorieWrapper(resolutionInSecond=" + this.resolutionInSecond + ", values=" + this.values + ", total=" + this.total + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public CalorieWrapper(Calorie calorie) {
        this(r0, aj6.g(aj6.c(yd6.b(r1), Anon1.INSTANCE)), (float) calorie.getTotal());
        wg6.b(calorie, "calorie");
        int resolutionInSecond2 = calorie.getResolutionInSecond();
        ArrayList values2 = calorie.getValues();
        wg6.a((Object) values2, "calorie.values");
    }
}
