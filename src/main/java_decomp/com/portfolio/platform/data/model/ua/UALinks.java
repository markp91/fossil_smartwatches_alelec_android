package com.portfolio.platform.data.model.ua;

import com.fossil.vu3;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UALinks {
    @DexIgnore
    @vu3("device")
    public List<UALink> device;
    @DexIgnore
    @vu3("self")
    public List<UALink> self;

    @DexIgnore
    public final List<UALink> getDevice() {
        return this.device;
    }

    @DexIgnore
    public final List<UALink> getSelf() {
        return this.self;
    }

    @DexIgnore
    public final void setDevice(List<UALink> list) {
        this.device = list;
    }

    @DexIgnore
    public final void setSelf(List<UALink> list) {
        this.self = list;
    }
}
