package com.portfolio.platform.data.model;

import com.fossil.pj4;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UserSettings {
    @DexIgnore
    public List<String> acceptedPrivacies;
    @DexIgnore
    public List<String> acceptedTermsOfService;
    @DexIgnore
    @pj4
    public String createdAt;
    @DexIgnore
    @pj4
    public String id;
    @DexIgnore
    @pj4
    public boolean isEnableNotification;
    @DexIgnore
    @pj4
    public boolean isLatestPrivacyAccepted;
    @DexIgnore
    @pj4
    public boolean isLatestTermsOfServiceAccepted;
    @DexIgnore
    @pj4
    public String latestPrivacyVersion;
    @DexIgnore
    @pj4
    public String latestTermsOfServiceVersion;
    @DexIgnore
    @pj4
    public String uid;
    @DexIgnore
    @pj4
    public String updatedAt;

    @DexIgnore
    public final List<String> getAcceptedPrivacies() {
        return this.acceptedPrivacies;
    }

    @DexIgnore
    public final List<String> getAcceptedTermsOfService() {
        return this.acceptedTermsOfService;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final String getLatestPrivacyVersion() {
        return this.latestPrivacyVersion;
    }

    @DexIgnore
    public final String getLatestTermsOfServiceVersion() {
        return this.latestTermsOfServiceVersion;
    }

    @DexIgnore
    public final String getUid() {
        return this.uid;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isEnableNotification() {
        return this.isEnableNotification;
    }

    @DexIgnore
    public final boolean isLatestPrivacyAccepted() {
        return this.isLatestPrivacyAccepted;
    }

    @DexIgnore
    public final boolean isLatestTermsOfServiceAccepted() {
        return this.isLatestTermsOfServiceAccepted;
    }

    @DexIgnore
    public final void setAcceptedPrivacies(List<String> list) {
        this.acceptedPrivacies = list;
    }

    @DexIgnore
    public final void setAcceptedTermsOfService(List<String> list) {
        this.acceptedTermsOfService = list;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setEnableNotification(boolean z) {
        this.isEnableNotification = z;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setLatestPrivacyAccepted(boolean z) {
        this.isLatestPrivacyAccepted = z;
    }

    @DexIgnore
    public final void setLatestPrivacyVersion(String str) {
        this.latestPrivacyVersion = str;
    }

    @DexIgnore
    public final void setLatestTermsOfServiceAccepted(boolean z) {
        this.isLatestTermsOfServiceAccepted = z;
    }

    @DexIgnore
    public final void setLatestTermsOfServiceVersion(String str) {
        this.latestTermsOfServiceVersion = str;
    }

    @DexIgnore
    public final void setUid(String str) {
        this.uid = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        this.updatedAt = str;
    }
}
