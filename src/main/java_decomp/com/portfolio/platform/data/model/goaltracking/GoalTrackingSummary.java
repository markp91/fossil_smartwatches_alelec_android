package com.portfolio.platform.data.model.goaltracking;

import com.fossil.vu3;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GoalTrackingSummary {
    @DexIgnore
    @vu3("createdAt")
    public long createdAt;
    @DexIgnore
    @vu3("date")
    public Date date;
    @DexIgnore
    @vu3("goalTarget")
    public int goalTarget;
    @DexIgnore
    public int pinType;
    @DexIgnore
    public int totalTargetOfWeek;
    @DexIgnore
    @vu3("totalTracked")
    public int totalTracked;
    @DexIgnore
    public int totalValueOfWeek;
    @DexIgnore
    @vu3("updatedAt")
    public long updatedAt;

    @DexIgnore
    public GoalTrackingSummary(Date date2, int i, int i2, long j, long j2) {
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        this.date = date2;
        this.totalTracked = i;
        this.goalTarget = i2;
        this.createdAt = j;
        this.updatedAt = j2;
    }

    @DexIgnore
    public static /* synthetic */ GoalTrackingSummary copy$default(GoalTrackingSummary goalTrackingSummary, Date date2, int i, int i2, long j, long j2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            date2 = goalTrackingSummary.date;
        }
        if ((i3 & 2) != 0) {
            i = goalTrackingSummary.totalTracked;
        }
        int i4 = i;
        if ((i3 & 4) != 0) {
            i2 = goalTrackingSummary.goalTarget;
        }
        int i5 = i2;
        if ((i3 & 8) != 0) {
            j = goalTrackingSummary.createdAt;
        }
        long j3 = j;
        if ((i3 & 16) != 0) {
            j2 = goalTrackingSummary.updatedAt;
        }
        return goalTrackingSummary.copy(date2, i4, i5, j3, j2);
    }

    @DexIgnore
    public final Date component1() {
        return this.date;
    }

    @DexIgnore
    public final int component2() {
        return this.totalTracked;
    }

    @DexIgnore
    public final int component3() {
        return this.goalTarget;
    }

    @DexIgnore
    public final long component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final long component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final GoalTrackingSummary copy(Date date2, int i, int i2, long j, long j2) {
        wg6.b(date2, HardwareLog.COLUMN_DATE);
        return new GoalTrackingSummary(date2, i, i2, j, j2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof GoalTrackingSummary)) {
            return false;
        }
        GoalTrackingSummary goalTrackingSummary = (GoalTrackingSummary) obj;
        if (this.totalTracked == goalTrackingSummary.totalTracked && this.goalTarget == goalTrackingSummary.goalTarget && this.totalTargetOfWeek == goalTrackingSummary.totalTargetOfWeek && this.totalValueOfWeek == goalTrackingSummary.totalValueOfWeek) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final long getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final Date getDate() {
        return this.date;
    }

    @DexIgnore
    public final int getGoalTarget() {
        return this.goalTarget;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final int getTotalTargetOfWeek() {
        return this.totalTargetOfWeek;
    }

    @DexIgnore
    public final int getTotalTracked() {
        return this.totalTracked;
    }

    @DexIgnore
    public final int getTotalValueOfWeek() {
        return this.totalValueOfWeek;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        return 0;
    }

    @DexIgnore
    public final void setCreatedAt(long j) {
        this.createdAt = j;
    }

    @DexIgnore
    public final void setDate(Date date2) {
        wg6.b(date2, "<set-?>");
        this.date = date2;
    }

    @DexIgnore
    public final void setGoalTarget(int i) {
        this.goalTarget = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setTotalTargetOfWeek(int i) {
        this.totalTargetOfWeek = i;
    }

    @DexIgnore
    public final void setTotalTracked(int i) {
        this.totalTracked = i;
    }

    @DexIgnore
    public final void setTotalValueOfWeek(int i) {
        this.totalValueOfWeek = i;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public String toString() {
        return "GoalTrackingSummary(date=" + this.date + ", totalTracked=" + this.totalTracked + ", goalTarget=" + this.goalTarget + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ")";
    }
}
