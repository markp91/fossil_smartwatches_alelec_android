package com.portfolio.platform.data.model;

import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class QuickResponseSender {
    @DexIgnore
    public String content;
    @DexIgnore
    public int id;

    @DexIgnore
    public QuickResponseSender(String str) {
        wg6.b(str, "content");
        this.content = str;
    }

    @DexIgnore
    public static /* synthetic */ QuickResponseSender copy$default(QuickResponseSender quickResponseSender, String str, int i, Object obj) {
        if ((i & 1) != 0) {
            str = quickResponseSender.content;
        }
        return quickResponseSender.copy(str);
    }

    @DexIgnore
    public final String component1() {
        return this.content;
    }

    @DexIgnore
    public final QuickResponseSender copy(String str) {
        wg6.b(str, "content");
        return new QuickResponseSender(str);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            return (obj instanceof QuickResponseSender) && wg6.a((Object) this.content, (Object) ((QuickResponseSender) obj).content);
        }
        return true;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.content;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    @DexIgnore
    public final void setContent(String str) {
        wg6.b(str, "<set-?>");
        this.content = str;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        return "QuickResponseSender(content=" + this.content + ")";
    }
}
