package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;
import com.fossil.fitness.HeartRate;
import com.fossil.fitness.SleepSession;
import com.fossil.fitness.SleepStateChange;
import com.fossil.wg6;
import com.j256.ormlite.logger.Logger;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SleepSessionWrapper {
    @DexIgnore
    public /* final */ int awakeMinutes;
    @DexIgnore
    public /* final */ int deepSleepMinutes;
    @DexIgnore
    public /* final */ DateTime endTime;
    @DexIgnore
    public /* final */ HeartRateWrapper heartRate;
    @DexIgnore
    public /* final */ int lightSleepMinutes;
    @DexIgnore
    public /* final */ int normalizedSleepQuality;
    @DexIgnore
    public /* final */ DateTime startTime;
    @DexIgnore
    public /* final */ List<SleepStateChangeWrapper> stateChanges;
    @DexIgnore
    public /* final */ int timezoneOffsetInSecond;

    @DexIgnore
    public SleepSessionWrapper(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        wg6.b(dateTime, "startTime");
        wg6.b(dateTime2, "endTime");
        this.startTime = dateTime;
        this.endTime = dateTime2;
        this.awakeMinutes = i;
        this.lightSleepMinutes = i2;
        this.deepSleepMinutes = i3;
        this.heartRate = heartRateWrapper;
        this.normalizedSleepQuality = i4;
        this.timezoneOffsetInSecond = i5;
        this.stateChanges = new ArrayList();
    }

    @DexIgnore
    public static /* synthetic */ SleepSessionWrapper copy$default(SleepSessionWrapper sleepSessionWrapper, DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5, int i6, Object obj) {
        SleepSessionWrapper sleepSessionWrapper2 = sleepSessionWrapper;
        int i7 = i6;
        return sleepSessionWrapper.copy((i7 & 1) != 0 ? sleepSessionWrapper2.startTime : dateTime, (i7 & 2) != 0 ? sleepSessionWrapper2.endTime : dateTime2, (i7 & 4) != 0 ? sleepSessionWrapper2.awakeMinutes : i, (i7 & 8) != 0 ? sleepSessionWrapper2.lightSleepMinutes : i2, (i7 & 16) != 0 ? sleepSessionWrapper2.deepSleepMinutes : i3, (i7 & 32) != 0 ? sleepSessionWrapper2.heartRate : heartRateWrapper, (i7 & 64) != 0 ? sleepSessionWrapper2.normalizedSleepQuality : i4, (i7 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) != 0 ? sleepSessionWrapper2.timezoneOffsetInSecond : i5);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final DateTime component2() {
        return this.endTime;
    }

    @DexIgnore
    public final int component3() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int component4() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int component5() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final HeartRateWrapper component6() {
        return this.heartRate;
    }

    @DexIgnore
    public final int component7() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final int component8() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final SleepSessionWrapper copy(DateTime dateTime, DateTime dateTime2, int i, int i2, int i3, HeartRateWrapper heartRateWrapper, int i4, int i5) {
        wg6.b(dateTime, "startTime");
        wg6.b(dateTime2, "endTime");
        return new SleepSessionWrapper(dateTime, dateTime2, i, i2, i3, heartRateWrapper, i4, i5);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SleepSessionWrapper)) {
            return false;
        }
        SleepSessionWrapper sleepSessionWrapper = (SleepSessionWrapper) obj;
        return wg6.a((Object) this.startTime, (Object) sleepSessionWrapper.startTime) && wg6.a((Object) this.endTime, (Object) sleepSessionWrapper.endTime) && this.awakeMinutes == sleepSessionWrapper.awakeMinutes && this.lightSleepMinutes == sleepSessionWrapper.lightSleepMinutes && this.deepSleepMinutes == sleepSessionWrapper.deepSleepMinutes && wg6.a((Object) this.heartRate, (Object) sleepSessionWrapper.heartRate) && this.normalizedSleepQuality == sleepSessionWrapper.normalizedSleepQuality && this.timezoneOffsetInSecond == sleepSessionWrapper.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getAwakeMinutes() {
        return this.awakeMinutes;
    }

    @DexIgnore
    public final int getDeepSleepMinutes() {
        return this.deepSleepMinutes;
    }

    @DexIgnore
    public final DateTime getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public final HeartRateWrapper getHeartRate() {
        return this.heartRate;
    }

    @DexIgnore
    public final int getLightSleepMinutes() {
        return this.lightSleepMinutes;
    }

    @DexIgnore
    public final int getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final List<SleepStateChangeWrapper> getStateChanges() {
        return this.stateChanges;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        int i = 0;
        int hashCode = (dateTime != null ? dateTime.hashCode() : 0) * 31;
        DateTime dateTime2 = this.endTime;
        int hashCode2 = (((((((hashCode + (dateTime2 != null ? dateTime2.hashCode() : 0)) * 31) + d.a(this.awakeMinutes)) * 31) + d.a(this.lightSleepMinutes)) * 31) + d.a(this.deepSleepMinutes)) * 31;
        HeartRateWrapper heartRateWrapper = this.heartRate;
        if (heartRateWrapper != null) {
            i = heartRateWrapper.hashCode();
        }
        return ((((hashCode2 + i) * 31) + d.a(this.normalizedSleepQuality)) * 31) + d.a(this.timezoneOffsetInSecond);
    }

    @DexIgnore
    public String toString() {
        return "SleepSessionWrapper(startTime=" + this.startTime + ", endTime=" + this.endTime + ", awakeMinutes=" + this.awakeMinutes + ", lightSleepMinutes=" + this.lightSleepMinutes + ", deepSleepMinutes=" + this.deepSleepMinutes + ", heartRate=" + this.heartRate + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ")";
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public SleepSessionWrapper(SleepSession sleepSession) {
        this(r2, r0, r4, r5, r6, r1, sleepSession.getNormalizedSleepQuality(), sleepSession.getTimezoneOffsetInSecond());
        HeartRateWrapper heartRateWrapper;
        wg6.b(sleepSession, "sleepSession");
        DateTime dateTime = new DateTime(((long) sleepSession.getStartTime()) * 1000, DateTimeZone.forOffsetMillis(sleepSession.getTimezoneOffsetInSecond() * 1000));
        DateTime dateTime2 = new DateTime(((long) sleepSession.getEndTime()) * 1000, DateTimeZone.forOffsetMillis(sleepSession.getTimezoneOffsetInSecond() * 1000));
        int awakeMinutes2 = sleepSession.getAwakeMinutes();
        int lightSleepMinutes2 = sleepSession.getLightSleepMinutes();
        int deepSleepMinutes2 = sleepSession.getDeepSleepMinutes();
        if (sleepSession.getHeartrate() != null) {
            HeartRate heartrate = sleepSession.getHeartrate();
            wg6.a((Object) heartrate, "sleepSession.heartrate");
            heartRateWrapper = new HeartRateWrapper(heartrate);
        } else {
            heartRateWrapper = null;
        }
        ArrayList<SleepStateChange> stateChanges2 = sleepSession.getStateChanges();
        wg6.a((Object) stateChanges2, "sleepSession.stateChanges");
        for (SleepStateChange sleepStateChange : stateChanges2) {
            List<SleepStateChangeWrapper> list = this.stateChanges;
            wg6.a((Object) sleepStateChange, "it");
            list.add(new SleepStateChangeWrapper(sleepStateChange));
        }
    }
}
