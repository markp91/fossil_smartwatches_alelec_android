package com.portfolio.platform.data.model.diana.preset;

import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MetaData {
    @DexIgnore
    @vu3("selectedBackgroundColor")
    public String selectedBackgroundColor;
    @DexIgnore
    @vu3("selectedForegroundColor")
    public String selectedForegroundColor;
    @DexIgnore
    @vu3("unselectedBackgroundColor")
    public String unselectedBackgroundColor;
    @DexIgnore
    @vu3("unselectedForegroundColor")
    public String unselectedForegroundColor;

    @DexIgnore
    public MetaData(String str, String str2, String str3, String str4) {
        this.selectedForegroundColor = str;
        this.selectedBackgroundColor = str2;
        this.unselectedForegroundColor = str3;
        this.unselectedBackgroundColor = str4;
    }

    @DexIgnore
    public static /* synthetic */ MetaData copy$default(MetaData metaData, String str, String str2, String str3, String str4, int i, Object obj) {
        if ((i & 1) != 0) {
            str = metaData.selectedForegroundColor;
        }
        if ((i & 2) != 0) {
            str2 = metaData.selectedBackgroundColor;
        }
        if ((i & 4) != 0) {
            str3 = metaData.unselectedForegroundColor;
        }
        if ((i & 8) != 0) {
            str4 = metaData.unselectedBackgroundColor;
        }
        return metaData.copy(str, str2, str3, str4);
    }

    @DexIgnore
    public final String component1() {
        return this.selectedForegroundColor;
    }

    @DexIgnore
    public final String component2() {
        return this.selectedBackgroundColor;
    }

    @DexIgnore
    public final String component3() {
        return this.unselectedForegroundColor;
    }

    @DexIgnore
    public final String component4() {
        return this.unselectedBackgroundColor;
    }

    @DexIgnore
    public final MetaData copy(String str, String str2, String str3, String str4) {
        return new MetaData(str, str2, str3, str4);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MetaData)) {
            return false;
        }
        MetaData metaData = (MetaData) obj;
        return wg6.a((Object) this.selectedForegroundColor, (Object) metaData.selectedForegroundColor) && wg6.a((Object) this.selectedBackgroundColor, (Object) metaData.selectedBackgroundColor) && wg6.a((Object) this.unselectedForegroundColor, (Object) metaData.unselectedForegroundColor) && wg6.a((Object) this.unselectedBackgroundColor, (Object) metaData.unselectedBackgroundColor);
    }

    @DexIgnore
    public final String getSelectedBackgroundColor() {
        return this.selectedBackgroundColor;
    }

    @DexIgnore
    public final String getSelectedForegroundColor() {
        return this.selectedForegroundColor;
    }

    @DexIgnore
    public final String getUnselectedBackgroundColor() {
        return this.unselectedBackgroundColor;
    }

    @DexIgnore
    public final String getUnselectedForegroundColor() {
        return this.unselectedForegroundColor;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.selectedForegroundColor;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.selectedBackgroundColor;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.unselectedForegroundColor;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.unselectedBackgroundColor;
        if (str4 != null) {
            i = str4.hashCode();
        }
        return hashCode3 + i;
    }

    @DexIgnore
    public final void setSelectedBackgroundColor(String str) {
        this.selectedBackgroundColor = str;
    }

    @DexIgnore
    public final void setSelectedForegroundColor(String str) {
        this.selectedForegroundColor = str;
    }

    @DexIgnore
    public final void setUnselectedBackgroundColor(String str) {
        this.unselectedBackgroundColor = str;
    }

    @DexIgnore
    public final void setUnselectedForegroundColor(String str) {
        this.unselectedForegroundColor = str;
    }

    @DexIgnore
    public String toString() {
        return "MetaData(selectedForegroundColor=" + this.selectedForegroundColor + ", selectedBackgroundColor=" + this.selectedBackgroundColor + ", unselectedForegroundColor=" + this.unselectedForegroundColor + ", unselectedBackgroundColor=" + this.unselectedBackgroundColor + ")";
    }
}
