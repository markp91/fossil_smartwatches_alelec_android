package com.portfolio.platform.data.model.fitnessdata;

import com.fossil.d;
import com.fossil.wg6;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class RestingWrapper {
    @DexIgnore
    public DateTime startTime;
    @DexIgnore
    public int timezoneOffsetInSecond;
    @DexIgnore
    public int value;

    @DexIgnore
    public RestingWrapper(DateTime dateTime, int i, int i2) {
        wg6.b(dateTime, "startTime");
        this.startTime = dateTime;
        this.timezoneOffsetInSecond = i;
        this.value = i2;
    }

    @DexIgnore
    public static /* synthetic */ RestingWrapper copy$default(RestingWrapper restingWrapper, DateTime dateTime, int i, int i2, int i3, Object obj) {
        if ((i3 & 1) != 0) {
            dateTime = restingWrapper.startTime;
        }
        if ((i3 & 2) != 0) {
            i = restingWrapper.timezoneOffsetInSecond;
        }
        if ((i3 & 4) != 0) {
            i2 = restingWrapper.value;
        }
        return restingWrapper.copy(dateTime, i, i2);
    }

    @DexIgnore
    public final DateTime component1() {
        return this.startTime;
    }

    @DexIgnore
    public final int component2() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int component3() {
        return this.value;
    }

    @DexIgnore
    public final RestingWrapper copy(DateTime dateTime, int i, int i2) {
        wg6.b(dateTime, "startTime");
        return new RestingWrapper(dateTime, i, i2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof RestingWrapper)) {
            return false;
        }
        RestingWrapper restingWrapper = (RestingWrapper) obj;
        return wg6.a((Object) this.startTime, (Object) restingWrapper.startTime) && this.timezoneOffsetInSecond == restingWrapper.timezoneOffsetInSecond && this.value == restingWrapper.value;
    }

    @DexIgnore
    public final DateTime getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public final int getTimezoneOffsetInSecond() {
        return this.timezoneOffsetInSecond;
    }

    @DexIgnore
    public final int getValue() {
        return this.value;
    }

    @DexIgnore
    public int hashCode() {
        DateTime dateTime = this.startTime;
        return ((((dateTime != null ? dateTime.hashCode() : 0) * 31) + d.a(this.timezoneOffsetInSecond)) * 31) + d.a(this.value);
    }

    @DexIgnore
    public final void setStartTime(DateTime dateTime) {
        wg6.b(dateTime, "<set-?>");
        this.startTime = dateTime;
    }

    @DexIgnore
    public final void setTimezoneOffsetInSecond(int i) {
        this.timezoneOffsetInSecond = i;
    }

    @DexIgnore
    public final void setValue(int i) {
        this.value = i;
    }

    @DexIgnore
    public String toString() {
        return "RestingWrapper(startTime=" + this.startTime + ", timezoneOffsetInSecond=" + this.timezoneOffsetInSecond + ", value=" + this.value + ")";
    }
}
