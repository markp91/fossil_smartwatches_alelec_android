package com.portfolio.platform.data.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.tu3;
import com.fossil.vu3;
import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Explore implements Parcelable {
    @DexIgnore
    public static /* final */ String COLUMN_BACKGROUND; // = "background";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DESCRIPTION; // = "description";
    @DexIgnore
    public static /* final */ String COLUMN_ETAG; // = "_etag";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_LOCAL_BACKGROUND; // = "COLUMN_LOCAL_BACKGROUND";
    @DexIgnore
    public static /* final */ String COLUMN_TITLE; // = "title";
    @DexIgnore
    public static /* final */ String COLUMN_TYPE; // = "type";
    @DexIgnore
    public static /* final */ String COLUMN_UPDATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ Parcelable.Creator<Explore> CREATOR; // = new Anon1();
    @DexIgnore
    @tu3
    @vu3("background")
    @DatabaseField(columnName = "background")
    public int background;
    @DexIgnore
    @tu3
    @vu3("createdAt")
    @DatabaseField(columnName = "createdAt")
    public String createdAt;
    @DexIgnore
    @tu3
    @vu3("description")
    @DatabaseField(columnName = "description")
    public String description;
    @DexIgnore
    @tu3
    @vu3("_etag")
    @DatabaseField(columnName = "_etag")
    public String etag;
    @DexIgnore
    public ExploreType exploreType; // = ExploreType.WRIST_FLICK;
    @DexIgnore
    @tu3
    @vu3("id")
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @tu3
    @DatabaseField(columnName = "COLUMN_LOCAL_BACKGROUND")
    public int localBackground;
    @DexIgnore
    @tu3
    @vu3("title")
    @DatabaseField(columnName = "title")
    public String title;
    @DexIgnore
    @tu3
    @vu3("type")
    @DatabaseField(columnName = "type")
    public String type;
    @DexIgnore
    @tu3
    @vu3("updatedAt")
    @DatabaseField(columnName = "updatedAt")
    public String updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Explore> {
        @DexIgnore
        public Explore createFromParcel(Parcel parcel) {
            return new Explore(parcel);
        }

        @DexIgnore
        public Explore[] newArray(int i) {
            return new Explore[i];
        }
    }

    @DexIgnore
    public enum ExploreType {
        WRIST_FLICK,
        DOUBLE_TAP,
        PRESS_AND_HOLD
    }

    @DexIgnore
    public Explore() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int getBackground() {
        return this.background;
    }

    @DexIgnore
    public String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public String getDescription() {
        return this.description;
    }

    @DexIgnore
    public String getEtag() {
        return this.etag;
    }

    @DexIgnore
    public ExploreType getExploreType() {
        return this.exploreType;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public int getLocalBackground() {
        return this.localBackground;
    }

    @DexIgnore
    public String getTitle() {
        return this.title;
    }

    @DexIgnore
    public String getType() {
        return this.type;
    }

    @DexIgnore
    public String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public void setBackground(int i) {
        this.background = i;
    }

    @DexIgnore
    public void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public void setDescription(String str) {
        this.description = str;
    }

    @DexIgnore
    public void setEtag(String str) {
        this.etag = str;
    }

    @DexIgnore
    public void setExploreType(ExploreType exploreType2) {
        this.exploreType = exploreType2;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setTitle(String str) {
        this.title = str;
    }

    @DexIgnore
    public void setType(String str) {
        this.type = str;
    }

    @DexIgnore
    public void setUpdatedAt(String str) {
        this.updatedAt = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.id);
        parcel.writeString(this.type);
        parcel.writeInt(this.background);
        parcel.writeString(this.title);
        parcel.writeString(this.description);
        parcel.writeString(this.etag);
        parcel.writeString(this.createdAt);
        parcel.writeString(this.updatedAt);
    }

    @DexIgnore
    public Explore(String str, int i, String str2, String str3) {
        this.type = str;
        this.localBackground = i;
        this.title = str2;
        this.description = str3;
    }

    @DexIgnore
    public Explore(Parcel parcel) {
        this.id = parcel.readString();
        this.type = parcel.readString();
        this.background = parcel.readInt();
        this.title = parcel.readString();
        this.description = parcel.readString();
        this.etag = parcel.readString();
        this.createdAt = parcel.readString();
        this.updatedAt = parcel.readString();
    }
}
