package com.portfolio.platform.data.model.room.microapp;

import com.fossil.d;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppSetting {
    @DexIgnore
    @vu3("appId")
    public String appId;
    @DexIgnore
    @vu3("createdAt")
    public String createdAt;
    @DexIgnore
    @vu3("id")
    public String id;
    @DexIgnore
    public int pinType;
    @DexIgnore
    @vu3("settings")
    public String setting;
    @DexIgnore
    @vu3("updatedAt")
    public String updatedAt;

    @DexIgnore
    public MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i) {
        wg6.b(str, "id");
        wg6.b(str2, "appId");
        wg6.b(str4, "createdAt");
        wg6.b(str5, "updatedAt");
        this.id = str;
        this.appId = str2;
        this.setting = str3;
        this.createdAt = str4;
        this.updatedAt = str5;
        this.pinType = i;
    }

    @DexIgnore
    public static /* synthetic */ MicroAppSetting copy$default(MicroAppSetting microAppSetting, String str, String str2, String str3, String str4, String str5, int i, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = microAppSetting.id;
        }
        if ((i2 & 2) != 0) {
            str2 = microAppSetting.appId;
        }
        String str6 = str2;
        if ((i2 & 4) != 0) {
            str3 = microAppSetting.setting;
        }
        String str7 = str3;
        if ((i2 & 8) != 0) {
            str4 = microAppSetting.createdAt;
        }
        String str8 = str4;
        if ((i2 & 16) != 0) {
            str5 = microAppSetting.updatedAt;
        }
        String str9 = str5;
        if ((i2 & 32) != 0) {
            i = microAppSetting.pinType;
        }
        return microAppSetting.copy(str, str6, str7, str8, str9, i);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component2() {
        return this.appId;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final String component4() {
        return this.createdAt;
    }

    @DexIgnore
    public final String component5() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component6() {
        return this.pinType;
    }

    @DexIgnore
    public final MicroAppSetting copy(String str, String str2, String str3, String str4, String str5, int i) {
        wg6.b(str, "id");
        wg6.b(str2, "appId");
        wg6.b(str4, "createdAt");
        wg6.b(str5, "updatedAt");
        return new MicroAppSetting(str, str2, str3, str4, str5, i);
    }

    @DexIgnore
    public final MicroAppSetting deepCopy() {
        return new MicroAppSetting(this.id, this.appId, this.setting, this.createdAt, this.updatedAt, 0, 32, (qg6) null);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroAppSetting)) {
            return false;
        }
        MicroAppSetting microAppSetting = (MicroAppSetting) obj;
        return wg6.a((Object) this.id, (Object) microAppSetting.id) && wg6.a((Object) this.appId, (Object) microAppSetting.appId) && wg6.a((Object) this.setting, (Object) microAppSetting.setting) && wg6.a((Object) this.createdAt, (Object) microAppSetting.createdAt) && wg6.a((Object) this.updatedAt, (Object) microAppSetting.updatedAt) && this.pinType == microAppSetting.pinType;
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.appId;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.setting;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.createdAt;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.updatedAt;
        if (str5 != null) {
            i = str5.hashCode();
        }
        return ((hashCode4 + i) * 31) + d.a(this.pinType);
    }

    @DexIgnore
    public final void setAppId(String str) {
        wg6.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.createdAt = str;
    }

    @DexIgnore
    public final void setId(String str) {
        wg6.b(str, "<set-?>");
        this.id = str;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setSetting(String str) {
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppSetting(id=" + this.id + ", appId=" + this.appId + ", setting=" + this.setting + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ MicroAppSetting(String str, String str2, String str3, String str4, String str5, int i, int i2, qg6 qg6) {
        this(str, str2, str3, str4, str5, (i2 & 32) != 0 ? 1 : i);
    }
}
