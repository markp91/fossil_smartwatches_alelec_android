package com.portfolio.platform.data.model.microapp.weather;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.vu3;
import com.fossil.wg6;
import com.fossil.zj4;
import com.google.android.gms.maps.model.LatLng;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherSettings implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    @vu3("forecastType")
    public DISPLAY_FORMAT displayFormat;
    @DexIgnore
    @vu3("forecast")
    public String forecast;
    @DexIgnore
    @vu3("useCurrentLocation")
    public boolean isUseCurrentLocation;
    @DexIgnore
    public LatLng latLng;
    @DexIgnore
    @vu3("area")
    public String location;
    @DexIgnore
    @vu3("rainProbability")
    public float rainProbability;
    @DexIgnore
    @vu3("unit")
    public TEMP_UNIT tempUnit;
    @DexIgnore
    @vu3("temperature")
    public float temperature;
    @DexIgnore
    public long updatedAt;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<WeatherSettings> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public WeatherSettings createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new WeatherSettings(parcel);
        }

        @DexIgnore
        public WeatherSettings[] newArray(int i) {
            return new WeatherSettings[i];
        }
    }

    @DexIgnore
    public enum DISPLAY_FORMAT {
        MIN(2),
        MAX(1),
        RANGE(3),
        CURRENT_TEMP(0);
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ int value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final DISPLAY_FORMAT getDisplayFormat(int i) {
                for (DISPLAY_FORMAT display_format : DISPLAY_FORMAT.values()) {
                    if (display_format.getValue() == i) {
                        return display_format;
                    }
                }
                return DISPLAY_FORMAT.CURRENT_TEMP;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public DISPLAY_FORMAT(int i) {
            this.value = i;
        }

        @DexIgnore
        public final int getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public enum TEMP_UNIT {
        CELSIUS("c"),
        FAHRENHEIT("f");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final TEMP_UNIT getTempUnit(String str) {
                wg6.b(str, ServerSetting.VALUE);
                for (TEMP_UNIT temp_unit : TEMP_UNIT.values()) {
                    if (wg6.a((Object) temp_unit.getValue(), (Object) str)) {
                        return temp_unit;
                    }
                }
                return TEMP_UNIT.CELSIUS;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public TEMP_UNIT(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public WeatherSettings() {
        this.location = "";
        double d = (double) 0;
        this.latLng = new LatLng(d, d);
        this.tempUnit = TEMP_UNIT.CELSIUS;
        this.displayFormat = DISPLAY_FORMAT.CURRENT_TEMP;
        this.forecast = "clear-day";
        this.temperature = 30.0f;
        this.rainProbability = 0.0f;
        this.isUseCurrentLocation = true;
        this.updatedAt = 0;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final DISPLAY_FORMAT getDisplayFormat() {
        return this.displayFormat;
    }

    @DexIgnore
    public final String getForecast() {
        return this.forecast;
    }

    @DexIgnore
    public final LatLng getLatLng() {
        return this.latLng;
    }

    @DexIgnore
    public final String getLocation() {
        return this.location;
    }

    @DexIgnore
    public final float getRainProbability() {
        return this.rainProbability;
    }

    @DexIgnore
    public final TEMP_UNIT getTempUnit() {
        return this.tempUnit;
    }

    @DexIgnore
    public final float getTemperature() {
        return this.temperature;
    }

    @DexIgnore
    public final float getTemperatureIn(TEMP_UNIT temp_unit) {
        wg6.b(temp_unit, Constants.PROFILE_KEY_UNIT);
        TEMP_UNIT temp_unit2 = this.tempUnit;
        if (temp_unit2 == temp_unit) {
            return this.temperature;
        }
        if (temp_unit2 == TEMP_UNIT.CELSIUS && temp_unit == TEMP_UNIT.FAHRENHEIT) {
            return zj4.a(this.temperature);
        }
        if (this.tempUnit == TEMP_UNIT.FAHRENHEIT && temp_unit == TEMP_UNIT.CELSIUS) {
            return zj4.c(this.temperature);
        }
        return this.temperature;
    }

    @DexIgnore
    public final long getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final boolean isUseCurrentLocation() {
        return this.isUseCurrentLocation;
    }

    @DexIgnore
    public final void setDisplayFormat(DISPLAY_FORMAT display_format) {
        wg6.b(display_format, "<set-?>");
        this.displayFormat = display_format;
    }

    @DexIgnore
    public final void setForecast(String str) {
        wg6.b(str, "<set-?>");
        this.forecast = str;
    }

    @DexIgnore
    public final void setLatLng(LatLng latLng2) {
        wg6.b(latLng2, "<set-?>");
        this.latLng = latLng2;
    }

    @DexIgnore
    public final void setLocation(String str) {
        wg6.b(str, "<set-?>");
        this.location = str;
    }

    @DexIgnore
    public final void setRainProbability(float f) {
        this.rainProbability = f;
    }

    @DexIgnore
    public final void setTempUnit(TEMP_UNIT temp_unit) {
        wg6.b(temp_unit, "<set-?>");
        this.tempUnit = temp_unit;
    }

    @DexIgnore
    public final void setTemperature(float f) {
        this.temperature = f;
    }

    @DexIgnore
    public final void setUpdatedAt(long j) {
        this.updatedAt = j;
    }

    @DexIgnore
    public final void setUseCurrentLocation(boolean z) {
        this.isUseCurrentLocation = z;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(this.location);
        parcel.writeString(this.tempUnit.getValue());
        parcel.writeInt(this.displayFormat.getValue());
        parcel.writeString(String.valueOf(this.isUseCurrentLocation));
        parcel.writeString(this.forecast);
        parcel.writeFloat(this.temperature);
        parcel.writeParcelable(this.latLng, 0);
        parcel.writeFloat(this.rainProbability);
        parcel.writeLong(this.updatedAt);
    }

    @DexIgnore
    public WeatherSettings(Parcel parcel) {
        wg6.b(parcel, "in");
        String readString = parcel.readString();
        this.location = readString == null ? "" : readString;
        TEMP_UNIT.Companion companion = TEMP_UNIT.Companion;
        String readString2 = parcel.readString();
        this.tempUnit = companion.getTempUnit(readString2 == null ? TEMP_UNIT.CELSIUS.getValue() : readString2);
        this.displayFormat = DISPLAY_FORMAT.Companion.getDisplayFormat(parcel.readInt());
        Boolean valueOf = Boolean.valueOf(parcel.readString());
        wg6.a((Object) valueOf, "java.lang.Boolean.valueOf(`in`.readString())");
        this.isUseCurrentLocation = valueOf.booleanValue();
        String readString3 = parcel.readString();
        this.forecast = readString3 == null ? "" : readString3;
        this.temperature = parcel.readFloat();
        LatLng readParcelable = parcel.readParcelable(LatLng.class.getClassLoader());
        this.latLng = readParcelable == null ? new LatLng(0.0d, 0.0d) : readParcelable;
        this.rainProbability = parcel.readFloat();
        this.updatedAt = parcel.readLong();
    }
}
