package com.portfolio.platform.data.model.microapp;

import com.fossil.wg6;
import com.portfolio.platform.data.legacy.threedotzero.MicroAppSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MicroAppLastSetting {
    @DexIgnore
    public String appId;
    @DexIgnore
    public String setting;
    @DexIgnore
    public String updatedAt;

    @DexIgnore
    public MicroAppLastSetting(String str, String str2, String str3) {
        wg6.b(str, "appId");
        wg6.b(str2, "updatedAt");
        wg6.b(str3, MicroAppSetting.SETTING);
        this.appId = str;
        this.updatedAt = str2;
        this.setting = str3;
    }

    @DexIgnore
    public static /* synthetic */ MicroAppLastSetting copy$default(MicroAppLastSetting microAppLastSetting, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            str = microAppLastSetting.appId;
        }
        if ((i & 2) != 0) {
            str2 = microAppLastSetting.updatedAt;
        }
        if ((i & 4) != 0) {
            str3 = microAppLastSetting.setting;
        }
        return microAppLastSetting.copy(str, str2, str3);
    }

    @DexIgnore
    public final String component1() {
        return this.appId;
    }

    @DexIgnore
    public final String component2() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String component3() {
        return this.setting;
    }

    @DexIgnore
    public final MicroAppLastSetting copy(String str, String str2, String str3) {
        wg6.b(str, "appId");
        wg6.b(str2, "updatedAt");
        wg6.b(str3, MicroAppSetting.SETTING);
        return new MicroAppLastSetting(str, str2, str3);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof MicroAppLastSetting)) {
            return false;
        }
        MicroAppLastSetting microAppLastSetting = (MicroAppLastSetting) obj;
        return wg6.a((Object) this.appId, (Object) microAppLastSetting.appId) && wg6.a((Object) this.updatedAt, (Object) microAppLastSetting.updatedAt) && wg6.a((Object) this.setting, (Object) microAppLastSetting.setting);
    }

    @DexIgnore
    public final String getAppId() {
        return this.appId;
    }

    @DexIgnore
    public final String getSetting() {
        return this.setting;
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.appId;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.updatedAt;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.setting;
        if (str3 != null) {
            i = str3.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public final void setAppId(String str) {
        wg6.b(str, "<set-?>");
        this.appId = str;
    }

    @DexIgnore
    public final void setSetting(String str) {
        wg6.b(str, "<set-?>");
        this.setting = str;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        wg6.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public String toString() {
        return "MicroAppLastSetting(appId=" + this.appId + ", updatedAt=" + this.updatedAt + ", setting=" + this.setting + ")";
    }
}
