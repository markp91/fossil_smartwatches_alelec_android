package com.portfolio.platform.data.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "firmware")
public class Firmware {
    @DexIgnore
    public static /* final */ String COLUMN_CHECKSUM; // = "checksum";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_MODEL; // = "deviceModel";
    @DexIgnore
    public static /* final */ String COLUMN_DOWNLOAD_URL; // = "downloadUrl";
    @DexIgnore
    public static /* final */ String COLUMN_IS_LATEST; // = "isLatest";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_SERVER_HREF; // = "serverHref";
    @DexIgnore
    public static /* final */ String COLUMN_VERSION_NUMBER; // = "versionNumber";
    @DexIgnore
    @DatabaseField(columnName = "checksum")
    public String checksum;
    @DexIgnore
    @DatabaseField(columnName = "deviceModel", id = true)
    public String deviceModel;
    @DexIgnore
    @DatabaseField(columnName = "downloadUrl")
    public String downloadUrl;
    @DexIgnore
    public boolean embedded;
    @DexIgnore
    @DatabaseField(columnName = "isLatest")
    public boolean isLatest;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    public int resourceId;
    @DexIgnore
    @DatabaseField(columnName = "serverHref")
    public String serverHref;
    @DexIgnore
    @DatabaseField(columnName = "versionNumber")
    public String versionNumber;

    @DexIgnore
    public Firmware() {
    }

    @DexIgnore
    public String getChecksum() {
        return this.checksum;
    }

    @DexIgnore
    public String getDeviceModel() {
        return this.deviceModel;
    }

    @DexIgnore
    public String getDownloadUrl() {
        return this.downloadUrl;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public int getResourceId() {
        return this.resourceId;
    }

    @DexIgnore
    public String getServerHref() {
        return this.serverHref;
    }

    @DexIgnore
    public String getVersionNumber() {
        return this.versionNumber;
    }

    @DexIgnore
    public boolean isEmbedded() {
        return this.embedded;
    }

    @DexIgnore
    public boolean isLatest() {
        return this.isLatest;
    }

    @DexIgnore
    public void setChecksum(String str) {
        this.checksum = str;
    }

    @DexIgnore
    public void setDeviceModel(String str) {
        this.deviceModel = str;
    }

    @DexIgnore
    public void setDownloadUrl(String str) {
        this.downloadUrl = str;
    }

    @DexIgnore
    public void setIsLatest(boolean z) {
        this.isLatest = z;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setServerHref(String str) {
        this.serverHref = str;
    }

    @DexIgnore
    public void setVersionNumber(String str) {
        this.versionNumber = str;
    }

    @DexIgnore
    public String toString() {
        return "[Firmware: deviceModel=" + this.deviceModel + ", version=" + this.versionNumber + ", embedded=" + this.embedded + ", resId=" + this.resourceId + ']';
    }

    @DexIgnore
    public Firmware(String str, String str2, String str3, String str4, boolean z, String str5, String str6) {
        this.objectId = str;
        this.deviceModel = str2;
        this.versionNumber = str3;
        this.downloadUrl = str4;
        this.isLatest = z;
        this.serverHref = str5;
        this.checksum = str6;
    }

    @DexIgnore
    public Firmware(String str, String str2, boolean z, int i) {
        this(str, z, i);
        this.deviceModel = str2;
    }

    @DexIgnore
    public Firmware(String str, boolean z, int i) {
        this.versionNumber = str;
        this.embedded = z;
        this.resourceId = i;
    }
}
