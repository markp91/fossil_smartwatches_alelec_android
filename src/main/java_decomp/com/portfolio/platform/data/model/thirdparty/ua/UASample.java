package com.portfolio.platform.data.model.thirdparty.ua;

import com.fossil.b;
import com.fossil.d;
import com.fossil.e;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UASample {
    @DexIgnore
    public /* final */ double calorie;
    @DexIgnore
    public /* final */ double distance;
    @DexIgnore
    public int id;
    @DexIgnore
    public /* final */ int step;
    @DexIgnore
    public /* final */ long time;

    @DexIgnore
    public UASample(int i, double d, double d2, long j) {
        this.step = i;
        this.distance = d;
        this.calorie = d2;
        this.time = j;
    }

    @DexIgnore
    public static /* synthetic */ UASample copy$default(UASample uASample, int i, double d, double d2, long j, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            i = uASample.step;
        }
        if ((i2 & 2) != 0) {
            d = uASample.distance;
        }
        double d3 = d;
        if ((i2 & 4) != 0) {
            d2 = uASample.calorie;
        }
        double d4 = d2;
        if ((i2 & 8) != 0) {
            j = uASample.time;
        }
        return uASample.copy(i, d3, d4, j);
    }

    @DexIgnore
    public final int component1() {
        return this.step;
    }

    @DexIgnore
    public final double component2() {
        return this.distance;
    }

    @DexIgnore
    public final double component3() {
        return this.calorie;
    }

    @DexIgnore
    public final long component4() {
        return this.time;
    }

    @DexIgnore
    public final UASample copy(int i, double d, double d2, long j) {
        return new UASample(i, d, d2, j);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof UASample)) {
            return false;
        }
        UASample uASample = (UASample) obj;
        return this.step == uASample.step && Double.compare(this.distance, uASample.distance) == 0 && Double.compare(this.calorie, uASample.calorie) == 0 && this.time == uASample.time;
    }

    @DexIgnore
    public final double getCalorie() {
        return this.calorie;
    }

    @DexIgnore
    public final double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final int getStep() {
        return this.step;
    }

    @DexIgnore
    public final long getTime() {
        return this.time;
    }

    @DexIgnore
    public int hashCode() {
        return (((((d.a(this.step) * 31) + b.a(this.distance)) * 31) + b.a(this.calorie)) * 31) + e.a(this.time);
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public String toString() {
        return "UASample(step=" + this.step + ", distance=" + this.distance + ", calorie=" + this.calorie + ", time=" + this.time + ")";
    }
}
