package com.portfolio.platform.data;

import android.content.Context;
import com.fossil.jm4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.view.NumberPickerLarge;
import java.io.Serializable;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HeightFormatter implements NumberPickerLarge.f, Serializable {
    @DexIgnore
    public static /* final */ int UNIT_FEET; // = 0;
    @DexIgnore
    public static /* final */ int UNIT_INCHES; // = 1;
    @DexIgnore
    public /* final */ int unit;

    @DexIgnore
    public HeightFormatter(int i) {
        this.unit = i;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public String format(int i) {
        int i2 = this.unit;
        if (i2 == 0) {
            return String.format(Locale.US, jm4.a((Context) PortfolioApp.T, 2131887145), new Object[]{Integer.valueOf(i)});
        } else if (i2 == 1) {
            return String.format(Locale.US, jm4.a((Context) PortfolioApp.T, 2131887174), new Object[]{Integer.valueOf(i)});
        } else {
            return String.format(Locale.US, jm4.a((Context) PortfolioApp.T, 2131887224), new Object[]{Integer.valueOf(i)});
        }
    }
}
