package com.portfolio.platform.cloudimage;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.AssetUtil$checkAssetExist$isFilePath1Exist$1", f = "AssetUtil.kt", l = {}, m = "invokeSuspend")
public final class AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $filePath1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(String str, xe6 xe6) {
        super(2, xe6);
        this.$filePath1 = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        AssetUtil$checkAssetExist$isFilePath1Exist$Anon1 assetUtil$checkAssetExist$isFilePath1Exist$Anon1 = new AssetUtil$checkAssetExist$isFilePath1Exist$Anon1(this.$filePath1, xe6);
        assetUtil$checkAssetExist$isFilePath1Exist$Anon1.p$ = (il6) obj;
        return assetUtil$checkAssetExist$isFilePath1Exist$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((AssetUtil$checkAssetExist$isFilePath1Exist$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return hf6.a(AssetUtil.INSTANCE.checkFileExist(this.$filePath1));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
