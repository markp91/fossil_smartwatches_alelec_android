package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.cd6;
import com.fossil.cn6;
import com.fossil.ff6;
import com.fossil.gk6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.rc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.cloudimage.CloudImageHelper;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1", f = "CloudImageHelper.kt", l = {118, 122}, m = "invokeSuspend")
public final class CloudImageHelper$ItemImage$downloadForCalibration$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ CloudImageHelper.ItemImage this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.cloudimage.CloudImageHelper$ItemImage$downloadForCalibration$1$1", f = "CloudImageHelper.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ CloudImageHelper$ItemImage$downloadForCalibration$Anon1 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(CloudImageHelper$ItemImage$downloadForCalibration$Anon1 cloudImageHelper$ItemImage$downloadForCalibration$Anon1, xe6 xe6) {
            super(2, xe6);
            this.this$0 = cloudImageHelper$ItemImage$downloadForCalibration$Anon1;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.this$0.this$0.mWeakReferenceImageView != null) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String tag = CloudImageHelper.Companion.getTAG();
                    local.d(tag, "download setDefaultImage first, resourceId=" + this.this$0.this$0.mResourceId);
                    WeakReference access$getMWeakReferenceImageView$p = this.this$0.this$0.mWeakReferenceImageView;
                    if (access$getMWeakReferenceImageView$p != null) {
                        ImageView imageView = (ImageView) access$getMWeakReferenceImageView$p.get();
                        if (imageView != null) {
                            Integer access$getMResourceId$p = this.this$0.this$0.mResourceId;
                            if (access$getMResourceId$p != null) {
                                imageView.setImageResource(access$getMResourceId$p.intValue());
                            } else {
                                wg6.a();
                                throw null;
                            }
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type java.lang.ref.WeakReference<android.widget.ImageView>");
                    }
                }
                File access$getMFile$p = this.this$0.this$0.mFile;
                if (access$getMFile$p != null) {
                    String access$getMSerialNumber$p = this.this$0.this$0.mSerialNumber;
                    if (access$getMSerialNumber$p != null) {
                        String access$getMSerialPrefix$p = this.this$0.this$0.mSerialPrefix;
                        if (access$getMSerialPrefix$p != null) {
                            CloudImageHelper.this.getMAppExecutors().b().execute(new CloudImageRunnable(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution(), Constants.DownloadAssetType.CALIBRATION, this.this$0.this$0.mCalibrationType.getType(), this.this$0.this$0.mListener));
                            return cd6.a;
                        }
                        wg6.a();
                        throw null;
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CloudImageHelper$ItemImage$downloadForCalibration$Anon1(CloudImageHelper.ItemImage itemImage, xe6 xe6) {
        super(2, xe6);
        this.this$0 = itemImage;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        CloudImageHelper$ItemImage$downloadForCalibration$Anon1 cloudImageHelper$ItemImage$downloadForCalibration$Anon1 = new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this.this$0, xe6);
        cloudImageHelper$ItemImage$downloadForCalibration$Anon1.p$ = (il6) obj;
        return cloudImageHelper$ItemImage$downloadForCalibration$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((CloudImageHelper$ItemImage$downloadForCalibration$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object obj2;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            if (this.this$0.mFile == null) {
                CloudImageHelper.ItemImage itemImage = this.this$0;
                itemImage.mFile = CloudImageHelper.this.getMApp().getFilesDir();
            }
            AssetUtil assetUtil = AssetUtil.INSTANCE;
            File access$getMFile$p = this.this$0.mFile;
            if (access$getMFile$p != null) {
                String access$getMSerialNumber$p = this.this$0.mSerialNumber;
                if (access$getMSerialNumber$p != null) {
                    String access$getMSerialPrefix$p = this.this$0.mSerialPrefix;
                    if (access$getMSerialPrefix$p != null) {
                        String resolution = ResolutionHelper.INSTANCE.getResolutionFromDevice().getResolution();
                        String feature = Constants.Feature.CALIBRATION.getFeature();
                        String type = this.this$0.mCalibrationType.getType();
                        CloudImageHelper.OnImageCallbackListener access$getMListener$p = this.this$0.mListener;
                        this.L$0 = il6;
                        this.label = 1;
                        obj2 = assetUtil.checkAssetExist(access$getMFile$p, access$getMSerialNumber$p, access$getMSerialPrefix$p, resolution, feature, type, access$getMListener$p, this);
                        if (obj2 == a) {
                            return a;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else if (i == 1) {
            nc6.a(obj);
            il6 = (il6) this.L$0;
            obj2 = obj;
        } else if (i == 2) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (((Boolean) obj2).booleanValue()) {
            return cd6.a;
        }
        cn6 c = zl6.c();
        Anon1_Level2 anon1_Level2 = new Anon1_Level2(this, (xe6) null);
        this.L$0 = il6;
        this.label = 2;
        if (gk6.a(c, anon1_Level2, this) == a) {
            return a;
        }
        return cd6.a;
    }
}
