package com.portfolio.platform.cloudimage;

import android.widget.ImageView;
import com.fossil.af6;
import com.fossil.ch6;
import com.fossil.dh6;
import com.fossil.ic6;
import com.fossil.ik6;
import com.fossil.jc6;
import com.fossil.jl6;
import com.fossil.kh6;
import com.fossil.li6;
import com.fossil.ll6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.u04;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.cloudimage.Constants;
import java.io.File;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CloudImageHelper {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ ic6 instance$delegate; // = jc6.a(CloudImageHelper$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public PortfolioApp mApp;
    @DexIgnore
    public u04 mAppExecutors;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public static /* final */ /* synthetic */ li6[] $$delegatedProperties;

        /*
        static {
            dh6 dh6 = new dh6(kh6.a(Companion.class), "instance", "getInstance()Lcom/portfolio/platform/cloudimage/CloudImageHelper;");
            kh6.a((ch6) dh6);
            $$delegatedProperties = new li6[]{dh6};
        }
        */

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final CloudImageHelper getInstance() {
            ic6 access$getInstance$cp = CloudImageHelper.instance$delegate;
            Companion companion = CloudImageHelper.Companion;
            li6 li6 = $$delegatedProperties[0];
            return (CloudImageHelper) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public final String getTAG() {
            return CloudImageHelper.TAG;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Holder {
        @DexIgnore
        public static /* final */ Holder INSTANCE; // = new Holder();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ CloudImageHelper f1INSTANCE; // = new CloudImageHelper();

        @DexIgnore
        public final CloudImageHelper getINSTANCE() {
            return f1INSTANCE;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ItemImage {
        @DexIgnore
        public Constants.CalibrationType mCalibrationType; // = Constants.CalibrationType.NONE;
        @DexIgnore
        public Constants.DeviceType mDeviceType; // = Constants.DeviceType.NONE;
        @DexIgnore
        public String mFastPairId;
        @DexIgnore
        public File mFile;
        @DexIgnore
        public OnImageCallbackListener mListener;
        @DexIgnore
        public Integer mResourceId; // = -1;
        @DexIgnore
        public String mSerialNumber;
        @DexIgnore
        public String mSerialPrefix;
        @DexIgnore
        public WeakReference<ImageView> mWeakReferenceImageView;

        @DexIgnore
        public ItemImage() {
        }

        @DexIgnore
        public final void download() {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new CloudImageHelper$ItemImage$download$Anon1(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public final void downloadForCalibration() {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new CloudImageHelper$ItemImage$downloadForCalibration$Anon1(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public final void downloadForWearOS() {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new CloudImageHelper$ItemImage$downloadForWearOS$Anon1(this, (xe6) null), 3, (Object) null);
        }

        @DexIgnore
        public final File getFile() {
            return this.mFile;
        }

        @DexIgnore
        public final ItemImage setFastPairId(String str) {
            wg6.b(str, "fastPairId");
            this.mFastPairId = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setFile(File file) {
            this.mFile = file;
            return this;
        }

        @DexIgnore
        public final ItemImage setImageCallback(OnImageCallbackListener onImageCallbackListener) {
            this.mListener = onImageCallbackListener;
            return this;
        }

        @DexIgnore
        public final ItemImage setPlaceHolder(ImageView imageView, int i) {
            wg6.b(imageView, "imageView");
            this.mWeakReferenceImageView = new WeakReference<>(imageView);
            this.mResourceId = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialNumber(String str) {
            wg6.b(str, "serialNumber");
            this.mSerialNumber = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setSerialPrefix(String str) {
            wg6.b(str, "serialPrefix");
            this.mSerialPrefix = str;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.DeviceType deviceType) {
            wg6.b(deviceType, "deviceType");
            this.mDeviceType = deviceType;
            return this;
        }

        @DexIgnore
        public final ItemImage setType(Constants.CalibrationType calibrationType) {
            wg6.b(calibrationType, "calibrationType");
            this.mCalibrationType = calibrationType;
            return this;
        }
    }

    @DexIgnore
    public interface OnForceDownloadCallbackListener {
        @DexIgnore
        void onDownloadCallback(boolean z);
    }

    @DexIgnore
    public interface OnImageCallbackListener {
        @DexIgnore
        void onImageCallback(String str, String str2);
    }

    /*
    static {
        String simpleName = CloudImageHelper$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        wg6.a((Object) simpleName, "CloudImageHelper::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CloudImageHelper() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public static final CloudImageHelper getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    public final PortfolioApp getMApp() {
        PortfolioApp portfolioApp = this.mApp;
        if (portfolioApp != null) {
            return portfolioApp;
        }
        wg6.d("mApp");
        throw null;
    }

    @DexIgnore
    public final u04 getMAppExecutors() {
        u04 u04 = this.mAppExecutors;
        if (u04 != null) {
            return u04;
        }
        wg6.d("mAppExecutors");
        throw null;
    }

    @DexIgnore
    public final void setMApp(PortfolioApp portfolioApp) {
        wg6.b(portfolioApp, "<set-?>");
        this.mApp = portfolioApp;
    }

    @DexIgnore
    public final void setMAppExecutors(u04 u04) {
        wg6.b(u04, "<set-?>");
        this.mAppExecutors = u04;
    }

    @DexIgnore
    public final ItemImage with() {
        ItemImage itemImage = new ItemImage();
        if (itemImage.getFile() == null) {
            rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new CloudImageHelper$with$Anon1(this, itemImage, (xe6) null), 3, (Object) null);
        }
        return itemImage;
    }
}
