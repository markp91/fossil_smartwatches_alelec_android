package com.portfolio.platform.receiver;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Process;
import android.text.TextUtils;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.workers.PushPendingDataWorker;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NetworkChangedReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public UserRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.NetworkChangedReceiver$onReceive$2", f = "NetworkChangedReceiver.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ boolean $isInternetAvailable;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NetworkChangedReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(NetworkChangedReceiver networkChangedReceiver, boolean z, xe6 xe6) {
            super(2, xe6);
            this.this$0 = networkChangedReceiver;
            this.$isInternetAvailable = z;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$isInternetAvailable, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                if (this.this$0.a().getCurrentUser() != null && !this.$isInternetAvailable) {
                    PushPendingDataWorker.z.a();
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = NetworkChangedReceiver.class.getSimpleName();
        wg6.a((Object) simpleName, "NetworkChangedReceiver::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public NetworkChangedReceiver() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final UserRepository a() {
        UserRepository userRepository = this.a;
        if (userRepository != null) {
            return userRepository;
        }
        wg6.d("mUserRepository");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void onReceive(Context context, Intent intent) {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        wg6.b(context, "context");
        wg6.b(intent, "intent");
        FLogger.INSTANCE.getLocal().d(b, "onReceive");
        int myPid = Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Constants.ACTIVITY);
        if (activityManager != null) {
            if (activityManager.getRunningAppProcesses() != null) {
                Iterator<ActivityManager.RunningAppProcessInfo> it = activityManager.getRunningAppProcesses().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    runningAppProcessInfo = it.next();
                    FLogger.INSTANCE.getLocal().d(b, "onReceive processId=" + runningAppProcessInfo.pid);
                    if (runningAppProcessInfo.pid == myPid) {
                        break;
                    }
                }
            } else {
                return;
            }
        }
        runningAppProcessInfo = null;
        if (runningAppProcessInfo != null) {
            if (!TextUtils.isEmpty(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null)) {
                if (wg6.a((Object) runningAppProcessInfo != null ? runningAppProcessInfo.processName : null, (Object) PortfolioApp.get.instance().getPackageName())) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = b;
                    StringBuilder sb = new StringBuilder();
                    sb.append("onReceive processName=");
                    sb.append(runningAppProcessInfo != null ? runningAppProcessInfo.processName : null);
                    local.d(str, sb.toString());
                    rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, a(context), (xe6) null), 3, (Object) null);
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) {
            return false;
        }
        return true;
    }
}
