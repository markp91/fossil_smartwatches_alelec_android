package com.portfolio.platform.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import com.fossil.NotificationAppHelper;
import com.fossil.af6;
import com.fossil.an4;
import com.fossil.cd6;
import com.fossil.cj4;
import com.fossil.d15;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.mz4;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AppPackageInstallReceiver extends BroadcastReceiver {
    @DexIgnore
    public static /* final */ String g;
    @DexIgnore
    public NotificationsRepository a;
    @DexIgnore
    public an4 b;
    @DexIgnore
    public cj4 c;
    @DexIgnore
    public d15 d;
    @DexIgnore
    public mz4 e;
    @DexIgnore
    public NotificationSettingsDatabase f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver$onReceive$1", f = "AppPackageInstallReceiver.kt", l = {68}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $packageName;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageInstallReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(AppPackageInstallReceiver appPackageInstallReceiver, String str, xe6 xe6) {
            super(2, xe6);
            this.this$0 = appPackageInstallReceiver;
            this.$packageName = str;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$packageName, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(AppPackageInstallReceiver.g, "new app installed, start full sync");
                AppFilter appFilter = new AppFilter();
                appFilter.setDeviceFamily(MFDeviceFamily.DEVICE_FAMILY_DIANA.ordinal());
                appFilter.setType(this.$packageName);
                this.this$0.a().saveAppFilter(appFilter);
                AppPackageInstallReceiver appPackageInstallReceiver = this.this$0;
                this.L$0 = il6;
                this.L$1 = appFilter;
                this.label = 1;
                if (appPackageInstallReceiver.a(this) == a) {
                    return a;
                }
            } else if (i == 1) {
                AppFilter appFilter2 = (AppFilter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.receiver.AppPackageInstallReceiver", f = "AppPackageInstallReceiver.kt", l = {74}, m = "setRuleNotificationFilterToDevice")
    public static final class c extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ AppPackageInstallReceiver this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(AppPackageInstallReceiver appPackageInstallReceiver, xe6 xe6) {
            super(xe6);
            this.this$0 = appPackageInstallReceiver;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a(this);
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = AppPackageRemoveReceiver.class.getSimpleName();
        wg6.a((Object) simpleName, "AppPackageRemoveReceiver::class.java.simpleName");
        g = simpleName;
    }
    */

    @DexIgnore
    public AppPackageInstallReceiver() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final NotificationsRepository a() {
        NotificationsRepository notificationsRepository = this.a;
        if (notificationsRepository != null) {
            return notificationsRepository;
        }
        wg6.d("mNotificationsRepository");
        throw null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0015, code lost:
        r2 = r11.getData();
     */
    @DexIgnore
    public void onReceive(Context context, Intent intent) {
        Uri data;
        an4 an4 = this.b;
        if (an4 != null) {
            boolean B = an4.B();
            String e2 = PortfolioApp.get.instance().e();
            String encodedSchemeSpecificPart = (intent == null || data == null) ? null : data.getEncodedSchemeSpecificPart();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = g;
            StringBuilder sb = new StringBuilder();
            sb.append("onReceive action ");
            sb.append(intent != null ? intent.getAction() : null);
            sb.append(" new app install ");
            sb.append(encodedSchemeSpecificPart);
            sb.append(", isAllAppEnable ");
            sb.append(B);
            local.d(str, sb.toString());
            if (B && DeviceHelper.o.f(e2) && !TextUtils.isEmpty(encodedSchemeSpecificPart)) {
                rm6 unused = ik6.b(jl6.a(zl6.b()), (af6) null, (ll6) null, new b(this, encodedSchemeSpecificPart, (xe6) null), 3, (Object) null);
                return;
            }
            return;
        }
        wg6.d("mSharedPreferencesManager");
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0024  */
    public final /* synthetic */ Object a(xe6<? super cd6> xe6) {
        c cVar;
        int i;
        if (xe6 instanceof c) {
            cVar = (c) xe6;
            int i2 = cVar.label;
            if ((i2 & Integer.MIN_VALUE) != 0) {
                cVar.label = i2 - Integer.MIN_VALUE;
                c cVar2 = cVar;
                Object obj = cVar2.result;
                Object a2 = ff6.a();
                i = cVar2.label;
                if (i != 0) {
                    nc6.a(obj);
                    NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                    d15 d15 = this.d;
                    if (d15 != null) {
                        mz4 mz4 = this.e;
                        if (mz4 != null) {
                            NotificationSettingsDatabase notificationSettingsDatabase = this.f;
                            if (notificationSettingsDatabase != null) {
                                an4 an4 = this.b;
                                if (an4 != null) {
                                    cVar2.L$0 = this;
                                    cVar2.label = 1;
                                    obj = notificationAppHelper.a(d15, mz4, notificationSettingsDatabase, an4, cVar2);
                                    if (obj == a2) {
                                        return a2;
                                    }
                                } else {
                                    wg6.d("mSharedPreferencesManager");
                                    throw null;
                                }
                            } else {
                                wg6.d("mNotificationSettingsDatabase");
                                throw null;
                            }
                        } else {
                            wg6.d("mGetAllContactGroup");
                            throw null;
                        }
                    } else {
                        wg6.d("mGetApps");
                        throw null;
                    }
                } else if (i == 1) {
                    AppPackageInstallReceiver appPackageInstallReceiver = (AppPackageInstallReceiver) cVar2.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                PortfolioApp.get.instance().a(new AppNotificationFilterSettings((List) obj, System.currentTimeMillis()), PortfolioApp.get.instance().e());
                return cd6.a;
            }
        }
        cVar = new c(this, xe6);
        c cVar22 = cVar;
        Object obj2 = cVar22.result;
        Object a22 = ff6.a();
        i = cVar22.label;
        if (i != 0) {
        }
        PortfolioApp.get.instance().a(new AppNotificationFilterSettings((List) obj2, System.currentTimeMillis()), PortfolioApp.get.instance().e());
        return cd6.a;
    }
}
