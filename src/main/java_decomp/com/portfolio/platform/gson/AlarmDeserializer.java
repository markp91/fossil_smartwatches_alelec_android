package com.portfolio.platform.gson;

import com.fossil.fu3;
import com.fossil.gu3;
import com.fossil.hu3;
import com.fossil.ku3;
import com.fossil.qd6;
import com.fossil.wg6;
import com.google.gson.JsonElement;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import java.lang.reflect.Type;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmDeserializer implements hu3<Alarm> {
    @DexIgnore
    public Alarm deserialize(JsonElement jsonElement, Type type, gu3 gu3) {
        String str;
        String str2;
        String str3;
        int[] iArr;
        String f;
        String f2;
        String f3;
        String f4;
        ku3 d = jsonElement != null ? jsonElement.d() : null;
        if (d == null) {
            return null;
        }
        JsonElement a = d.a("id");
        String f5 = a != null ? a.f() : null;
        if (f5 == null) {
            return null;
        }
        JsonElement a2 = d.a(Explore.COLUMN_TITLE);
        String str4 = (a2 == null || (f4 = a2.f()) == null) ? "" : f4;
        JsonElement a3 = d.a("message");
        if (a3 == null || (f3 = a3.f()) == null) {
            str = "";
        } else {
            str = f3;
        }
        JsonElement a4 = d.a("minute");
        int i = 0;
        int b = a4 != null ? a4.b() : 0;
        JsonElement a5 = d.a("hour");
        int b2 = a5 != null ? a5.b() : 0;
        JsonElement a6 = d.a("isActive");
        boolean a7 = a6 != null ? a6.a() : false;
        JsonElement a8 = d.a("isRepeated");
        boolean a9 = a8 != null ? a8.a() : false;
        JsonElement a10 = d.a("createdAt");
        if (a10 == null || (f2 = a10.f()) == null) {
            str2 = "";
        } else {
            str2 = f2;
        }
        JsonElement a11 = d.a("updatedAt");
        if (a11 == null || (f = a11.f()) == null) {
            str3 = "";
        } else {
            str3 = f;
        }
        fu3 b3 = d.b(com.misfit.frameworks.buttonservice.model.Alarm.COLUMN_DAYS);
        if (b3 != null) {
            int[] iArr2 = new int[b3.size()];
            for (Object next : b3) {
                int i2 = i + 1;
                if (i >= 0) {
                    JsonElement jsonElement2 = (JsonElement) next;
                    if (jsonElement2 != null) {
                        AlarmHelper.a aVar = AlarmHelper.d;
                        String f6 = jsonElement2.f();
                        wg6.a((Object) f6, "it.asString");
                        iArr2[i] = aVar.a(f6);
                    }
                    i = i2;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            iArr = iArr2;
        } else {
            iArr = null;
        }
        return new Alarm(f5, f5, str4, str, b2, b, iArr, a7, a9, str2, str3, 0);
    }
}
