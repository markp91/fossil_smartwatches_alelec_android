package com.misfit.frameworks.buttonservice.core;

import android.os.AsyncTask;
import java.util.ArrayDeque;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SerialExecutor implements Executor {
    @DexIgnore
    public Runnable mActive;
    @DexIgnore
    public /* final */ ArrayDeque<Runnable> mTasks; // = new ArrayDeque<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable val$r;

        @DexIgnore
        public Anon1(Runnable runnable) {
            this.val$r = runnable;
        }

        @DexIgnore
        public void run() {
            try {
                this.val$r.run();
            } finally {
                SerialExecutor.this.scheduleNext();
            }
        }
    }

    @DexIgnore
    public synchronized void execute(Runnable runnable) {
        this.mTasks.offer(new Anon1(runnable));
        if (this.mActive == null) {
            scheduleNext();
        }
    }

    @DexIgnore
    public synchronized void scheduleNext() {
        Runnable poll = this.mTasks.poll();
        this.mActive = poll;
        if (poll != null) {
            AsyncTask.SERIAL_EXECUTOR.execute(this.mActive);
        }
    }
}
