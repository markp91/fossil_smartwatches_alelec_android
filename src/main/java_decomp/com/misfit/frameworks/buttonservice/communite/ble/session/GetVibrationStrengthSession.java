package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.a70;
import com.fossil.bc0;
import com.fossil.nh6;
import com.fossil.r60;
import com.fossil.rc6;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class GetVibrationStrengthSession extends EnableMaintainingSession {
    @DexIgnore
    public a70.a mVibrationStrengthLevel; // = a70.a.MEDIUM;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class GetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public zb0<HashMap<s60, r60>> task;

        @DexIgnore
        public GetVibrationStrengthState() {
            super(GetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final void logConfiguration(HashMap<s60, r60> hashMap) {
            GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
            nh6 nh6 = nh6.a;
            Object[] objArr = {hashMap.get(s60.TIME), hashMap.get(s60.BATTERY), hashMap.get(s60.BIOMETRIC_PROFILE), hashMap.get(s60.DAILY_STEP), hashMap.get(s60.DAILY_STEP_GOAL), hashMap.get(s60.DAILY_CALORIE), hashMap.get(s60.DAILY_CALORIE_GOAL), hashMap.get(s60.DAILY_TOTAL_ACTIVE_MINUTE), hashMap.get(s60.DAILY_ACTIVE_MINUTE_GOAL), hashMap.get(s60.DAILY_DISTANCE), hashMap.get(s60.INACTIVE_NUDGE), hashMap.get(s60.VIBE_STRENGTH), hashMap.get(s60.DO_NOT_DISTURB_SCHEDULE)};
            String format = String.format("Get configuration  " + GetVibrationStrengthSession.this.getSerial() + "\n" + "\t[timeConfiguration: \n" + "\ttime = %s,\n" + "\tbattery = %s,\n" + "\tbiometric = %s,\n" + "\tdaily steps = %s,\n" + "\tdaily step goal = %s, \n" + "\tdaily calorie: %s, \n" + "\tdaily calorie goal: %s, \n" + "\tdaily total active minute: %s, \n" + "\tdaily active minute goal: %s, \n" + "\tdaily distance: %s, \n" + "\tinactive nudge: %s, \n" + "\tvibration strength: %s, \n" + "\tdo not disturb schedule: %s, \n" + "\t]", Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            getVibrationStrengthSession.log(format);
        }

        @DexIgnore
        public boolean onEnter() {
            GetVibrationStrengthSession.this.log("Get Vibration Strength Level");
            this.task = GetVibrationStrengthSession.this.getBleAdapter().getDeviceConfig(GetVibrationStrengthSession.this.getLogSession(), this);
            if (this.task == null) {
                GetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onGetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            GetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_GET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        public void onGetDeviceConfigSuccess(HashMap<s60, r60> hashMap) {
            wg6.b(hashMap, "deviceConfiguration");
            stopTimeout();
            logConfiguration(hashMap);
            if (hashMap.containsKey(s60.VIBE_STRENGTH)) {
                GetVibrationStrengthSession getVibrationStrengthSession = GetVibrationStrengthSession.this;
                a70 a70 = hashMap.get(s60.VIBE_STRENGTH);
                if (a70 != null) {
                    getVibrationStrengthSession.mVibrationStrengthLevel = a70.getVibeStrengthLevel();
                    GetVibrationStrengthSession getVibrationStrengthSession2 = GetVibrationStrengthSession.this;
                    getVibrationStrengthSession2.log("Get Vibration Strength Level Success, value=" + GetVibrationStrengthSession.this.mVibrationStrengthLevel);
                } else {
                    throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.VibeStrengthConfig");
                }
            } else {
                GetVibrationStrengthSession.this.log("Get Vibration Strength Level Success, but no value.");
            }
            GetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<HashMap<s60, r60>> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public GetVibrationStrengthSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.READ_REAL_TIME_STEP, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(ButtonService.Companion.getVIBRATION_STRENGTH_LEVEL(), VibrationStrengthObj.Companion.consumeSDKVibrationStrengthLevel$default(VibrationStrengthObj.Companion, this.mVibrationStrengthLevel, false, 2, (Object) null));
    }

    @DexIgnore
    public BleSession copyObject() {
        GetVibrationStrengthSession getVibrationStrengthSession = new GetVibrationStrengthSession(getBleAdapter(), getBleSessionCallback());
        getVibrationStrengthSession.setDevice(getDevice());
        return getVibrationStrengthSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.GET_VIBRATION_STRENGTH_STATE;
        String name = GetVibrationStrengthState.class.getName();
        wg6.a((Object) name, "GetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
