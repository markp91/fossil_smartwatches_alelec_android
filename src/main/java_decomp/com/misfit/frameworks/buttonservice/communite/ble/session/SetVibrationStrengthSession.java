package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetVibrationStrengthSession extends QuickResponseSession {
    @DexIgnore
    public /* final */ VibrationStrengthObj mVibrationStrengthLevelObj;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetVibrationStrengthState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetVibrationStrengthState() {
            super(SetVibrationStrengthSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareData() {
            b70 b70 = new b70();
            b70.a(SetVibrationStrengthSession.this.mVibrationStrengthLevelObj.toSDKVibrationStrengthLevel());
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            SetVibrationStrengthSession setVibrationStrengthSession = SetVibrationStrengthSession.this;
            setVibrationStrengthSession.log("Set Vibration Strength, value=" + SetVibrationStrengthSession.this.mVibrationStrengthLevelObj);
            this.task = SetVibrationStrengthSession.this.getBleAdapter().setDeviceConfig(SetVibrationStrengthSession.this.getLogSession(), prepareData(), this);
            if (this.task == null) {
                SetVibrationStrengthSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SetVibrationStrengthSession.this.stop(FailureCode.FAILED_TO_SET_VIBRATION_STRENGTH);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            SetVibrationStrengthSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetVibrationStrengthSession(VibrationStrengthObj vibrationStrengthObj, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_VIBRATION_STRENGTH, bleAdapterImpl, bleSessionCallback);
        wg6.b(vibrationStrengthObj, "mVibrationStrengthLevelObj");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mVibrationStrengthLevelObj = vibrationStrengthObj;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetVibrationStrengthSession setVibrationStrengthSession = new SetVibrationStrengthSession(this.mVibrationStrengthLevelObj, getBleAdapter(), getBleSessionCallback());
        setVibrationStrengthSession.setDevice(getDevice());
        return setVibrationStrengthSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        return createConcreteState(BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_VIBRATION_STRENGTH_STATE;
        String name = SetVibrationStrengthState.class.getName();
        wg6.a((Object) name, "SetVibrationStrengthState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
