package com.misfit.frameworks.buttonservice.communite.ble.device;

import com.fossil.ac0;
import com.fossil.af6;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.hg6;
import com.fossil.ig6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.lf6;
import com.fossil.ll6;
import com.fossil.nc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xg6;
import com.fossil.zl6;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DeviceCommunicator$sendNotificationFromQueue$Anon2 extends xg6 implements hg6<ac0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationBaseObj $notification;
    @DexIgnore
    public /* final */ /* synthetic */ DeviceCommunicator this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator$sendNotificationFromQueue$2$1", f = "DeviceCommunicator.kt", l = {}, m = "invokeSuspend")
    public static final class Anon1_Level2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DeviceCommunicator$sendNotificationFromQueue$Anon2 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public Anon1_Level2(DeviceCommunicator$sendNotificationFromQueue$Anon2 deviceCommunicator$sendNotificationFromQueue$Anon2, xe6 xe6) {
            super(2, xe6);
            this.this$0 = deviceCommunicator$sendNotificationFromQueue$Anon2;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            Anon1_Level2 anon1_Level2 = new Anon1_Level2(this.this$0, xe6);
            anon1_Level2.p$ = (il6) obj;
            return anon1_Level2;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((Anon1_Level2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                DeviceCommunicator$sendNotificationFromQueue$Anon2 deviceCommunicator$sendNotificationFromQueue$Anon2 = this.this$0;
                deviceCommunicator$sendNotificationFromQueue$Anon2.this$0.sendDianaNotification(deviceCommunicator$sendNotificationFromQueue$Anon2.$notification);
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DeviceCommunicator$sendNotificationFromQueue$Anon2(DeviceCommunicator deviceCommunicator, NotificationBaseObj notificationBaseObj) {
        super(1);
        this.this$0 = deviceCommunicator;
        this.$notification = notificationBaseObj;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((ac0) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(ac0 ac0) {
        wg6.b(ac0, Constants.YO_ERROR_POST);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.OTHER;
        String serial = this.this$0.getSerial();
        String access$getTAG$p = this.this$0.getTAG();
        remote.e(component, session, serial, access$getTAG$p, "Send notification: " + this.$notification.toRemoteLogString() + " Failed, error=" + ErrorCodeBuilder.INSTANCE.build(ErrorCodeBuilder.Step.SEND_APP_NOTIFICATION, ErrorCodeBuilder.Component.SDK, ac0));
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String access$getTAG$p2 = this.this$0.getTAG();
        local.d(access$getTAG$p2, " .sendNotificationFromQueue() = " + this.$notification + ", error=" + ac0.getErrorCode());
        rm6 unused = ik6.b(jl6.a(zl6.a()), (af6) null, (ll6) null, new Anon1_Level2(this, (xe6) null), 3, (Object) null);
    }
}
