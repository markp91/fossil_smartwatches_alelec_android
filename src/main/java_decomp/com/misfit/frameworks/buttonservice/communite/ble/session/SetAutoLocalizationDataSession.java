package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.nb0;
import com.fossil.wg6;
import com.fossil.yb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoLocalizationDataSession extends SetAutoSettingsSession {
    @DexIgnore
    public LocalizationData mNewLocalizationData;
    @DexIgnore
    public LocalizationData mOldLocalizationData;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoLocalizationDataSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoLocalizationDataSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetLocalizationState extends BleStateAbs {
        @DexIgnore
        public yb0<String> task;

        @DexIgnore
        public SetLocalizationState() {
            super(SetAutoLocalizationDataSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (SetAutoLocalizationDataSession.this.mNewLocalizationData.isDataValid()) {
                this.task = SetAutoLocalizationDataSession.this.getBleAdapter().setLocalizationData(SetAutoLocalizationDataSession.this.mNewLocalizationData, SetAutoLocalizationDataSession.this.getLogSession(), this);
                if (this.task == null) {
                    SetAutoLocalizationDataSession.this.stop(10000);
                    return true;
                }
                startTimeout();
                return true;
            }
            SetAutoLocalizationDataSession.this.log("Localization data is invalid!");
            return true;
        }

        @DexIgnore
        public void onSetLocalizationDataFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetAutoLocalizationDataSession.this.getContext(), SetAutoLocalizationDataSession.this.getSerial())) {
                SetAutoLocalizationDataSession.this.log("Reach the limit retry. Stop.");
                SetAutoLocalizationDataSession setAutoLocalizationDataSession = SetAutoLocalizationDataSession.this;
                setAutoLocalizationDataSession.storeMappings(setAutoLocalizationDataSession.mNewLocalizationData, true);
                SetAutoLocalizationDataSession.this.stop(FailureCode.FAILED_TO_SET_LOCALIZATION_DATA);
            }
        }

        @DexIgnore
        public void onSetLocalizationDataSuccess() {
            stopTimeout();
            SetAutoLocalizationDataSession setAutoLocalizationDataSession = SetAutoLocalizationDataSession.this;
            setAutoLocalizationDataSession.storeMappings(setAutoLocalizationDataSession.mNewLocalizationData, false);
            SetAutoLocalizationDataSession setAutoLocalizationDataSession2 = SetAutoLocalizationDataSession.this;
            setAutoLocalizationDataSession2.enterStateAsync(setAutoLocalizationDataSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<String> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoLocalizationDataSession(LocalizationData localizationData, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_LOCALIZATION_DATA, bleAdapterImpl, bleSessionCallback);
        wg6.b(localizationData, "mNewLocalizationData");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNewLocalizationData = localizationData;
        setLogSession(FLogger.Session.OTHER);
    }

    @DexIgnore
    private final void storeMappings(LocalizationData localizationData, boolean z) {
        localizationData.setCheckSum(LocalizationData.Companion.getLocalizationDataCheckSum(localizationData));
        DevicePreferenceUtils.setAutoLocalizationData(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a(localizationData));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.LOCALIZATION_DATA);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoLocalizationDataSession setAutoLocalizationDataSession = new SetAutoLocalizationDataSession(this.mNewLocalizationData, getBleAdapter(), getBleSessionCallback());
        setAutoLocalizationDataSession.setDevice(getDevice());
        return setAutoLocalizationDataSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldLocalizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(nb0.class) == null) {
            log("This device does not support set localization data.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (!this.mNewLocalizationData.isDataValid()) {
            log("New localization data is invalid.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (!LocalizationData.Companion.isTheSameFile(this.mOldLocalizationData, this.mNewLocalizationData)) {
            storeMappings(this.mNewLocalizationData, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_LOCALIZATION_STATE);
        } else {
            log("Old localization data is the same with the new one, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_LOCALIZATION_STATE;
        String name = SetLocalizationState.class.getName();
        wg6.a((Object) name, "SetLocalizationState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
