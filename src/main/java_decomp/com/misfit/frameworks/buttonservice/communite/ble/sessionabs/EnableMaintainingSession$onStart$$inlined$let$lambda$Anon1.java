package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class EnableMaintainingSession$onStart$$inlined$let$lambda$Anon1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ EnableMaintainingSession this$0;

    @DexIgnore
    public EnableMaintainingSession$onStart$$inlined$let$lambda$Anon1(EnableMaintainingSession enableMaintainingSession) {
        this.this$0 = enableMaintainingSession;
    }

    @DexIgnore
    public final void run() {
        this.this$0.stop(FailureCode.BLUETOOTH_IS_DISABLED);
    }
}
