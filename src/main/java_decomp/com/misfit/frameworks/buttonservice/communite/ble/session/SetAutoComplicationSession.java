package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.pb0;
import com.fossil.wg6;
import com.fossil.zb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.SetAutoSettingsSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetAutoComplicationSession extends SetAutoSettingsSession {
    @DexIgnore
    public /* final */ ComplicationAppMappingSettings mNewComplicationAppMappingSettings;
    @DexIgnore
    public ComplicationAppMappingSettings mOldComplicationAppMappingSettings;
    @DexIgnore
    public BleState startState; // = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneState extends BleStateAbs {
        @DexIgnore
        public DoneState() {
            super(SetAutoComplicationSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetAutoComplicationSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetComplicationsState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public SetComplicationsState() {
            super(SetAutoComplicationSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SetAutoComplicationSession.this.getBleAdapter().setComplications(SetAutoComplicationSession.this.getLogSession(), SetAutoComplicationSession.this.mNewComplicationAppMappingSettings, this);
            if (this.task == null) {
                SetAutoComplicationSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetComplicationFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetAutoComplicationSession.this.getContext(), SetAutoComplicationSession.this.getSerial())) {
                SetAutoComplicationSession.this.log("Reach the limit retry. Stop.");
                SetAutoComplicationSession setAutoComplicationSession = SetAutoComplicationSession.this;
                setAutoComplicationSession.storeMappings(setAutoComplicationSession.mNewComplicationAppMappingSettings, true);
                SetAutoComplicationSession.this.stop(1920);
            }
        }

        @DexIgnore
        public void onSetComplicationSuccess() {
            stopTimeout();
            SetAutoComplicationSession setAutoComplicationSession = SetAutoComplicationSession.this;
            setAutoComplicationSession.storeMappings(setAutoComplicationSession.mNewComplicationAppMappingSettings, false);
            SetAutoComplicationSession setAutoComplicationSession2 = SetAutoComplicationSession.this;
            setAutoComplicationSession2.enterStateAsync(setAutoComplicationSession2.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetAutoComplicationSession(ComplicationAppMappingSettings complicationAppMappingSettings, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(CommunicateMode.SET_AUTO_COMPLICATION_APPS, bleAdapterImpl, bleSessionCallback);
        wg6.b(complicationAppMappingSettings, "mNewComplicationAppMappingSettings");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.mNewComplicationAppMappingSettings = complicationAppMappingSettings;
        setLogSession(FLogger.Session.SET_COMPLICATION);
    }

    @DexIgnore
    private final void storeMappings(ComplicationAppMappingSettings complicationAppMappingSettings, boolean z) {
        DevicePreferenceUtils.setAutoComplicationAppSettings(getBleAdapter().getContext(), getBleAdapter().getSerial(), new Gson().a(complicationAppMappingSettings));
        if (z) {
            DevicePreferenceUtils.setSettingFlag(getBleAdapter().getContext(), DeviceSettings.COMPLICATION_APPS);
        }
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        SetAutoComplicationSession setAutoComplicationSession = new SetAutoComplicationSession(this.mNewComplicationAppMappingSettings, getBleAdapter(), getBleSessionCallback());
        setAutoComplicationSession.setDevice(getDevice());
        return setAutoComplicationSession;
    }

    @DexIgnore
    public BleState getStartState() {
        return this.startState;
    }

    @DexIgnore
    public void initSettings() {
        BleState bleState;
        super.initSettings();
        this.mOldComplicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        if (getBleAdapter().isSupportedFeature(pb0.class) == null) {
            log("This device does not support set complication apps.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (ComplicationAppMappingSettings.Companion.isSettingsSame(this.mOldComplicationAppMappingSettings, this.mNewComplicationAppMappingSettings)) {
            log("New complication settings and complication settings are the same, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        } else if (ComplicationAppMappingSettings.Companion.compareTimeStamp(this.mNewComplicationAppMappingSettings, this.mOldComplicationAppMappingSettings) > 0) {
            storeMappings(this.mNewComplicationAppMappingSettings, true);
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE);
        } else {
            log("Old complication settings timestamp is greater then the new one, no need to store again.");
            bleState = createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
        setStartState(bleState);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_COMPLICATIONS_STATE;
        String name = SetComplicationsState.class.getName();
        wg6.a((Object) name, "SetComplicationsState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneState.class.getName();
        wg6.a((Object) name2, "DoneState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public void setStartState(BleState bleState) {
        wg6.b(bleState, "<set-?>");
        this.startState = bleState;
    }
}
