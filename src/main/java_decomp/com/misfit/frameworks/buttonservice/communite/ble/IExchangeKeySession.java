package com.misfit.frameworks.buttonservice.communite.ble;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface IExchangeKeySession {
    @DexIgnore
    void onPing();

    @DexIgnore
    void onReceiveCurrentSecretKey(byte[] bArr);

    @DexIgnore
    void onReceiveRandomKey(byte[] bArr, int i);

    @DexIgnore
    void onReceiveServerSecretKey(byte[] bArr, int i);
}
