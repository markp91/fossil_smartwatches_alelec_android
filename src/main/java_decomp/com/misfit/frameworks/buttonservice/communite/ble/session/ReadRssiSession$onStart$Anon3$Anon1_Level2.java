package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.misfit.frameworks.buttonservice.log.FailureCode;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadRssiSession$onStart$Anon3$Anon1_Level2 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ ReadRssiSession $this_run;

    @DexIgnore
    public ReadRssiSession$onStart$Anon3$Anon1_Level2(ReadRssiSession readRssiSession) {
        this.$this_run = readRssiSession;
    }

    @DexIgnore
    public final void run() {
        this.$this_run.stop(FailureCode.FAILED_TO_READ_RSSI);
    }
}
