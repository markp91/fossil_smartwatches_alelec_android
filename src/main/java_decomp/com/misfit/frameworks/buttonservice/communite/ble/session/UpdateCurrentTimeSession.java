package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b70;
import com.fossil.bc0;
import com.fossil.r60;
import com.fossil.s60;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class UpdateCurrentTimeSession extends EnableMaintainingSession {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class SetTimeState extends BleStateAbs {
        @DexIgnore
        public zb0<s60[]> task;

        @DexIgnore
        public SetTimeState() {
            super(UpdateCurrentTimeSession.this.getTAG());
        }

        @DexIgnore
        private final r60[] prepareConfigData() {
            long currentTimeMillis = System.currentTimeMillis();
            long j = (long) 1000;
            long j2 = currentTimeMillis / j;
            b70 b70 = new b70();
            b70.a(j2, (short) ((int) (currentTimeMillis - (j * j2))), (short) ((TimeZone.getDefault().getOffset(currentTimeMillis) / 1000) / 60));
            return b70.a();
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = UpdateCurrentTimeSession.this.getBleAdapter().setDeviceConfig(UpdateCurrentTimeSession.this.getLogSession(), prepareConfigData(), this);
            if (this.task == null) {
                UpdateCurrentTimeSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onSetDeviceConfigFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            UpdateCurrentTimeSession.this.stop(FailureCode.FAILED_TO_SET_CONFIG);
        }

        @DexIgnore
        public void onSetDeviceConfigSuccess() {
            stopTimeout();
            UpdateCurrentTimeSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<s60[]> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public UpdateCurrentTimeSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.BACK_GROUND, CommunicateMode.UPDATE_CURRENT_TIME, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        UpdateCurrentTimeSession updateCurrentTimeSession = new UpdateCurrentTimeSession(getBleAdapter(), getBleSessionCallback());
        updateCurrentTimeSession.setDevice(getDevice());
        return updateCurrentTimeSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.UPDATE_CURRENT_TIME_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.UPDATE_CURRENT_TIME_STATE;
        String name = SetTimeState.class.getName();
        wg6.a((Object) name, "SetTimeState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
