package com.misfit.frameworks.buttonservice.communite;

import android.content.Context;
import com.fossil.hg6;
import com.fossil.hi6;
import com.fossil.kh6;
import com.fossil.qg6;
import com.fossil.tg6;
import com.fossil.wg6;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.device.DeviceCommunicator;
import com.misfit.frameworks.buttonservice.utils.DeviceUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommunicateManager {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public /* final */ ConcurrentHashMap<String, BleCommunicator> bleCommunicators;
    @DexIgnore
    public /* final */ Context context;
    @DexIgnore
    public BleCommunicator currentCommunicator;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion extends SingletonHolder<CommunicateManager, Context> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final /* synthetic */ class Anon1 extends tg6 implements hg6<Context, CommunicateManager> {
            @DexIgnore
            public static /* final */ Anon1 INSTANCE; // = new Anon1();

            @DexIgnore
            public Anon1() {
                super(1);
            }

            @DexIgnore
            public final String getName() {
                return "<init>";
            }

            @DexIgnore
            public final hi6 getOwner() {
                return kh6.a(CommunicateManager.class);
            }

            @DexIgnore
            public final String getSignature() {
                return "<init>(Landroid/content/Context;)V";
            }

            @DexIgnore
            public final CommunicateManager invoke(Context context) {
                wg6.b(context, "p1");
                return new CommunicateManager(context, (qg6) null);
            }
        }

        @DexIgnore
        public Companion() {
            super(Anon1.INSTANCE);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        String simpleName = CommunicateManager.class.getSimpleName();
        wg6.a((Object) simpleName, "CommunicateManager::class.java.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public CommunicateManager(Context context2) {
        this.context = context2;
        this.bleCommunicators = new ConcurrentHashMap<>();
    }

    @DexIgnore
    private final BleCommunicator createDeviceCommunicator(Context context2, String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        return new DeviceCommunicator(context2, str, str2, communicationResultCallback);
    }

    @DexIgnore
    public final void clearCommunicatorSessionQueue(String str) {
        wg6.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator != null) {
            bleCommunicator.clearSessionQueue();
        }
    }

    @DexIgnore
    public final void clearCurrentCommunicatorSessionQueueIfNot(String str) {
        wg6.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        BleCommunicator bleCommunicator2 = this.currentCommunicator;
        if (bleCommunicator != bleCommunicator2 && bleCommunicator2 != null) {
            if (bleCommunicator2 != null) {
                bleCommunicator2.clearSessionQueue();
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public final int getBatteryLevel(String str) {
        wg6.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return -1;
        }
        wg6.a((Object) bleCommunicator, "bleCommunicators[serial] ?: return -1");
        return bleCommunicator.getBleAdapter().getBatteryLevel();
    }

    @DexIgnore
    public final ConcurrentHashMap<String, BleCommunicator> getBleCommunicators() {
        return this.bleCommunicators;
    }

    @DexIgnore
    public final synchronized BleCommunicator getCommunicator(String str, String str2, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        BleCommunicator bleCommunicator;
        wg6.b(str, "serial");
        wg6.b(str2, "macAddress");
        wg6.b(communicationResultCallback, "communicationResultCallback");
        bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            bleCommunicator = createDeviceCommunicator(this.context, str, str2, communicationResultCallback);
            this.bleCommunicators.put(str, bleCommunicator);
        }
        this.currentCommunicator = bleCommunicator;
        return bleCommunicator;
    }

    @DexIgnore
    public final String getLocale(String str) {
        wg6.b(str, "serial");
        BleCommunicator bleCommunicator = this.bleCommunicators.get(str);
        if (bleCommunicator == null) {
            return "";
        }
        wg6.a((Object) bleCommunicator, "bleCommunicators[serial] ?: return \"\"");
        return bleCommunicator.getBleAdapter().getLocale();
    }

    @DexIgnore
    public final List<BleCommunicator> getRunningCommunicator() {
        Collection<BleCommunicator> values = this.bleCommunicators.values();
        wg6.a((Object) values, "bleCommunicators.values");
        ArrayList arrayList = new ArrayList();
        for (T next : values) {
            if (((BleCommunicator) next).isRunning()) {
                arrayList.add(next);
            }
        }
        return yd6.d(arrayList);
    }

    @DexIgnore
    public final void removeCommunicator(String str) {
        wg6.b(str, "serial");
        this.bleCommunicators.remove(str);
    }

    @DexIgnore
    public /* synthetic */ CommunicateManager(Context context2, qg6 qg6) {
        this(context2);
    }

    @DexIgnore
    public final BleCommunicator getCommunicator(String str, BleCommunicator.CommunicationResultCallback communicationResultCallback) {
        wg6.b(str, "serial");
        wg6.b(communicationResultCallback, "communicationResultCallback");
        String macAddress = DeviceUtils.getInstance(this.context).getMacAddress(this.context, str);
        wg6.a((Object) macAddress, "DeviceUtils.getInstance(\u2026cAddress(context, serial)");
        return getCommunicator(str, macAddress, communicationResultCallback);
    }
}
