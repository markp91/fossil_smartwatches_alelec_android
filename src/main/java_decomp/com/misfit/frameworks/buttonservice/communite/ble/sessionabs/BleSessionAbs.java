package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import android.content.Context;
import com.fossil.ac0;
import com.fossil.qg6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.session.NullSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleSessionAbs extends BleSession {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public BleAdapterImpl bleAdapter;
    @DexIgnore
    public BleStateAbs mState;
    @DexIgnore
    public HashMap<SessionState, String> sessionStateMap;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final BleSessionAbs createNullSession(Context context) {
            wg6.b(context, "context");
            return new NullSession(context);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public enum SessionState {
        SCANNING_STATE,
        ENABLE_MAINTAINING_CONNECTION_STATE,
        FETCH_DEVICE_INFO_STATE,
        GET_DEVICE_CONFIG_STATE,
        PLAY_DEVICE_ANIMATION_STATE,
        ERASE_DATA_FILE_STATE,
        DONE_STATE,
        OTA_STATE,
        SET_DEVICE_CONFIG_STATE,
        READ_OR_ERASE_STATE,
        READ_DATA_FILE_STATE,
        PROCESS_AND_STORE_DATA_STATE,
        SET_COMPLICATIONS_STATE,
        SET_WATCH_APPS_STATE,
        SET_BACKGROUND_IMAGE_CONFIG_STATE,
        SET_NOTIFICATION_FILTERS_STATE,
        SET_INACTIVE_NUDGE_STATE,
        SET_DISPLAY_UNIT_STATE,
        SET_LOCALIZATION_STATE,
        CLEAR_CACHE_DEVICE,
        CLOSE_CONNECTION_STATE,
        REQUEST_HAND_CONTROL_STATE,
        RESET_HANDS_STATE,
        MOVE_HAND_STATE,
        APPLY_HAND_STATE,
        RELEASE_HAND_CONTROL_STATE,
        SET_STEP_GOAL_STATE,
        READ_RSSI_STATE,
        READ_REAL_TIME_STEPS_STATE,
        UPDATE_CURRENT_TIME_STATE,
        GET_BATTERY_LEVEL_STATE,
        SET_VIBRATION_STRENGTH_STATE,
        GET_VIBRATION_STRENGTH_STATE,
        SET_LIST_ALARMS_STATE,
        SET_BIOMETRIC_DATA_STATE,
        SET_SETTING_DONE_STATE,
        SET_HEART_RATE_MODE_STATE,
        SET_FRONT_LIGHT_ENABLE_STATE,
        READ_CURRENT_WORKOUT_STATE,
        STOP_CURRENT_WORKOUT_STATE,
        TRANSFER_DATA_SUB_FLOW,
        TRANSFER_SETTINGS_SUB_FLOW,
        PAIRING_CHECK_FIRMWARE,
        LINK_SERVER,
        EXCHANGE_SECRET_KEY_SUB_FLOW,
        AUTHENTICATE_DEVICE,
        EXCHANGE_SECRET_KEY,
        VERIFY_SECRET_KEY,
        GET_SECRET_KEY,
        GET_SECRET_KEY_THROUGH_SDK,
        AUTHORIZE_DEVICE,
        PUSH_SECRET_KEY_TO_CLOUD,
        PLAY_ANIMATION_STATE,
        ERASE_FILE_STATE,
        SET_ALARM_AFTER_PAIRING_STATE,
        SET_MULTIPLE_ALAMRS_AFTER_PAIRING_STATE,
        CLEAR_ALL_MAPPINGS_STATE,
        DISABLE_GOAL_TRACKING_STATE,
        SET_MAPPING_STATE,
        SET_MICRO_APP_MAPPING_STATE,
        DONE_PAIRING_NEW_DEVICE_SESSION_STATE,
        PROCESS_OLD_MAPPING,
        PROCESS_MAPPING,
        PROCESS_HID,
        PROCESS_STREAMING,
        STOP_STREAMING_STATE,
        PLAY_SYNC_ANIMATION_STATE,
        READ_HARDWARE_LOG_STATE,
        GET_DEVICE_CONFIGURATION_STATE,
        SET_DEVICE_CONFIGURATION_STATE,
        GET_DEVICE_CONFIGURATION_AFTER_SYNC_STATE,
        SET_ALARM_IF_FULL_SYNC_STATE,
        SET_MULTIPLE_ALARMS_IF_FULL_SYNC_STATE,
        SET_MICRO_APP_MAPPING_AFTER_SYNCING_STATE,
        SET_MAPPING_AFTER_SYNCING_STATE,
        SET_GOAL_TRACKING_STATE,
        START_STREAMING_AFTER_SYNCING_STATE,
        HID_CONNECT_AFTER_SYNCING_STATE,
        DONE_SYNC_STATE,
        GET_DEVICE_CONFIGURATION_AFTER_OTA_STATE,
        READ_FIRMWARE_VERSION_AFTER_OTA_STATE,
        DONE_OTA_STATE,
        SET_SECOND_TIMEZONE_STATE,
        SET_REPLY_MESSAGE_MAPPING_STATE,
        NOTIFY_NOTIFICATION_EVENT_STATE,
        PLAY_VIBRATION_STATE,
        DONE_CONNECT_DEVICE
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleSessionAbs(CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(communicateMode, bleAdapterImpl.getContext(), bleAdapterImpl.getSerial(), bleAdapterImpl.getFirmwareVersion(), bleSessionCallback);
        wg6.b(communicateMode, "communicateMode");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.bleAdapter = bleAdapterImpl;
        this.mState = new NullBleState(getTAG());
        initMap();
    }

    @DexIgnore
    private final void initMap() {
        this.sessionStateMap = new HashMap<>();
        initStateMap();
    }

    @DexIgnore
    public boolean canRetry(int i, int i2) {
        return false;
    }

    @DexIgnore
    public final BleState createConcreteState(SessionState sessionState) {
        wg6.b(sessionState, Constants.STATE);
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            String str = hashMap.get(sessionState);
            if (str == null) {
                return createNullState();
            }
            BleState createConcreteState = createConcreteState(str);
            return createConcreteState != null ? createConcreteState : createNullState();
        }
        wg6.d("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public BleState createNullState() {
        return new NullBleState(getTAG());
    }

    @DexIgnore
    public final void endLogSession(int i) {
        FLogger.INSTANCE.getRemote().summary(i, FLogger.Component.BLE, getLogSession(), getSerial(), getTAG());
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, ErrorCodeBuilder.AppError appError) {
        wg6.b(str, "message");
        wg6.b(step, "step");
        wg6.b(appError, Constants.YO_ERROR_POST);
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.APP, appError);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        if (getMfLog() != null) {
            MFLog mfLog = getMfLog();
            if (mfLog != null) {
                mfLog.log('[' + getSerial() + "] " + str + " , error=" + build);
            } else {
                wg6.a();
                throw null;
            }
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, getLogSession(), getSerial(), getTAG(), build, step, str);
    }

    @DexIgnore
    public final BleAdapterImpl getBleAdapter() {
        return this.bleAdapter;
    }

    @DexIgnore
    public BleState getCurrentState() {
        return this.mState;
    }

    @DexIgnore
    public final BleStateAbs getCurrentStateV2() {
        return this.mState;
    }

    @DexIgnore
    public final HashMap<SessionState, String> getSessionStateMap() {
        HashMap<SessionState, String> hashMap = this.sessionStateMap;
        if (hashMap != null) {
            return hashMap;
        }
        wg6.d("sessionStateMap");
        throw null;
    }

    @DexIgnore
    public final boolean handleUnexpectedDisconnection() {
        log("Handle device disconnected.");
        stop(FailureCode.UNEXPECTED_DISCONNECT);
        return true;
    }

    @DexIgnore
    public abstract void initStateMap();

    @DexIgnore
    public void onStop(int i) {
        super.onStop(i);
        endLogSession(i);
    }

    @DexIgnore
    public void preStart() {
        super.preStart();
        startLogSession();
    }

    @DexIgnore
    public final void setBleAdapter(BleAdapterImpl bleAdapterImpl) {
        wg6.b(bleAdapterImpl, "<set-?>");
        this.bleAdapter = bleAdapterImpl;
    }

    @DexIgnore
    public void setCurrentState(BleState bleState) {
        BleStateAbs bleStateAbs;
        wg6.b(bleState, ServerSetting.VALUE);
        if (bleState instanceof BleStateAbs) {
            bleStateAbs = (BleStateAbs) bleState;
        } else {
            FLogger.INSTANCE.getLocal().e(getTAG(), ".setCurrentState(), this state is not BleStateV2. Move to null state");
            bleStateAbs = new NullBleState(getTAG());
        }
        this.mState = bleStateAbs;
    }

    @DexIgnore
    public final void setSessionStateMap(HashMap<SessionState, String> hashMap) {
        wg6.b(hashMap, "<set-?>");
        this.sessionStateMap = hashMap;
    }

    @DexIgnore
    public final void startLogSession() {
        FLogger.INSTANCE.getRemote().startSession(getLogSession(), getSerial(), getTAG());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public BleSessionAbs(SessionType sessionType, CommunicateMode communicateMode, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        this(communicateMode, bleAdapterImpl, bleSessionCallback);
        wg6.b(sessionType, "sessionType");
        wg6.b(communicateMode, "communicateMode");
        wg6.b(bleAdapterImpl, "bleAdapter");
        setSessionType(sessionType);
    }

    @DexIgnore
    public final void errorLog(String str, ErrorCodeBuilder.Step step, ac0 ac0) {
        wg6.b(str, "message");
        wg6.b(step, "step");
        wg6.b(ac0, "sdkError");
        String build = ErrorCodeBuilder.INSTANCE.build(step, ErrorCodeBuilder.Component.SDK, ac0);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.e(tag, str + ", error=" + build);
        if (getMfLog() != null) {
            MFLog mfLog = getMfLog();
            if (mfLog != null) {
                mfLog.log('[' + getSerial() + "] " + str + " , error=" + build);
            } else {
                wg6.a();
                throw null;
            }
        }
        FLogger.INSTANCE.getRemote().e(FLogger.Component.BLE, getLogSession(), getSerial(), getTAG(), build, step, str);
    }
}
