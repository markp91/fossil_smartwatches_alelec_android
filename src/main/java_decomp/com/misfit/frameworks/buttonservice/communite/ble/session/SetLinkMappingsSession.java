package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.cd6;
import com.fossil.nd0;
import com.fossil.wg6;
import com.fossil.yb0;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.QuickResponseSession;
import com.misfit.frameworks.buttonservice.enums.DeviceSettings;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.BLEMapping;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SetLinkMappingsSession extends QuickResponseSession {
    @DexIgnore
    public List<? extends MicroAppMapping> mappings;
    @DexIgnore
    public List<? extends MicroAppMapping> oldMappings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class DoneSetLinkMappingState extends BleStateAbs {
        @DexIgnore
        public DoneSetLinkMappingState() {
            super(SetLinkMappingsSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "All done of " + getTAG());
            SetLinkMappingsSession.this.stop(0);
            return true;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class WatchSetMicroAppMappingState extends BleStateAbs {
        @DexIgnore
        public yb0<cd6> task;

        @DexIgnore
        public WatchSetMicroAppMappingState() {
            super(SetLinkMappingsSession.this.getTAG());
        }

        @DexIgnore
        private final void storeSettings(List<? extends MicroAppMapping> list) {
            DevicePreferenceUtils.setAutoSetMapping(SetLinkMappingsSession.this.getBleAdapter().getContext(), SetLinkMappingsSession.this.getBleAdapter().getSerial(), new Gson().a(list));
            DevicePreferenceUtils.removeSettingFlag(SetLinkMappingsSession.this.getBleAdapter().getContext(), DeviceSettings.MAPPINGS);
        }

        @DexIgnore
        public void onConfigureMicroAppFail(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            if (!retry(SetLinkMappingsSession.this.getContext(), SetLinkMappingsSession.this.getSerial())) {
                SetLinkMappingsSession.this.log("Reach the limit retry. Stop.");
                storeSettings(SetLinkMappingsSession.this.mappings);
                SetLinkMappingsSession.this.stop(FailureCode.FAILED_TO_SET_MICRO_APP_MAPPING);
            }
        }

        @DexIgnore
        public void onConfigureMicroAppSuccess() {
            stopTimeout();
            storeSettings(SetLinkMappingsSession.this.mappings);
            SetLinkMappingsSession setLinkMappingsSession = SetLinkMappingsSession.this;
            setLinkMappingsSession.enterState(setLinkMappingsSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            if (!SetLinkMappingsSession.this.mappings.isEmpty()) {
                BleAdapterImpl access$getBleAdapter$p = SetLinkMappingsSession.this.getBleAdapter();
                FLogger.Session access$getLogSession$p = SetLinkMappingsSession.this.getLogSession();
                List<nd0> convertToSDKMapping = MicroAppMapping.convertToSDKMapping(SetLinkMappingsSession.this.mappings);
                wg6.a((Object) convertToSDKMapping, "MicroAppMapping.convertToSDKMapping(mappings)");
                this.task = access$getBleAdapter$p.configureMicroApp(access$getLogSession$p, convertToSDKMapping, this);
                if (this.task == null) {
                    SetLinkMappingsSession.this.stop(10000);
                } else {
                    startTimeout();
                }
            } else {
                SetLinkMappingsSession setLinkMappingsSession = SetLinkMappingsSession.this;
                setLinkMappingsSession.enterStateAsync(setLinkMappingsSession.createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            }
            return true;
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            yb0<cd6> yb0 = this.task;
            if (yb0 != null) {
                yb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SetLinkMappingsSession(List<? extends BLEMapping> list, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.SET_LINK_MAPPING, bleAdapterImpl, bleSessionCallback);
        wg6.b(list, "mappings");
        wg6.b(bleAdapterImpl, "bleAdapter");
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(list);
        wg6.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertToMicroAppMapping(mappings)");
        this.mappings = convertToMicroAppMapping;
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        return (getCommunicateMode() == bleSession.getCommunicateMode() || bleSession.getCommunicateMode() == CommunicateMode.SET_AUTO_MAPPING) ? false : true;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
    }

    @DexIgnore
    public BleSession copyObject() {
        List<BLEMapping> convertToBLEMapping = MicroAppMapping.convertToBLEMapping(this.mappings);
        wg6.a((Object) convertToBLEMapping, "MicroAppMapping.convertToBLEMapping(mappings)");
        SetLinkMappingsSession setLinkMappingsSession = new SetLinkMappingsSession(convertToBLEMapping, getBleAdapter(), getBleSessionCallback());
        setLinkMappingsSession.setDevice(getDevice());
        return setLinkMappingsSession;
    }

    @DexIgnore
    public BleState getFirstState() {
        if (getBleAdapter().isDeviceReady()) {
            List<? extends MicroAppMapping> list = this.oldMappings;
            if (list == null) {
                wg6.d("oldMappings");
                throw null;
            } else if (BLEMapping.isMappingTheSame(list, this.mappings)) {
                log("New mappings and old mappings are the same. No need to set again.");
                return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
            } else {
                List<? extends MicroAppMapping> list2 = this.mappings;
                List<? extends MicroAppMapping> list3 = this.oldMappings;
                if (list3 == null) {
                    wg6.d("oldMappings");
                    throw null;
                } else if (BLEMapping.compareTimeStamp(list2, list3).longValue() > 0) {
                    return createConcreteState(BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE);
                } else {
                    log("Old mappings timestamp is greater then the new one. No need to set again.");
                    return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
                }
            }
        } else {
            log("Device is not ready. Cannot set mapping.");
            return createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE);
        }
    }

    @DexIgnore
    public void initSettings() {
        List<MicroAppMapping> convertToMicroAppMapping = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getBleAdapter().getContext(), getBleAdapter().getSerial()));
        wg6.a((Object) convertToMicroAppMapping, "MicroAppMapping.convertT\u2026text, bleAdapter.serial))");
        this.oldMappings = convertToMicroAppMapping;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.SET_MICRO_APP_MAPPING_STATE;
        String name = WatchSetMicroAppMappingState.class.getName();
        wg6.a((Object) name, "WatchSetMicroAppMappingState::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.SET_SETTING_DONE_STATE;
        String name2 = DoneSetLinkMappingState.class.getName();
        wg6.a((Object) name2, "DoneSetLinkMappingState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wg6.b(objArr, "params");
        initSettings();
        List<? extends MicroAppMapping> list = this.oldMappings;
        if (list == null) {
            wg6.d("oldMappings");
            throw null;
        } else if (!BLEMapping.isMappingTheSame(list, this.mappings)) {
            return super.onStart(Arrays.copyOf(objArr, objArr.length));
        } else {
            log("New mappings and old mappings are the same. No need to set again.");
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.SET_SETTING_DONE_STATE));
            return true;
        }
    }
}
