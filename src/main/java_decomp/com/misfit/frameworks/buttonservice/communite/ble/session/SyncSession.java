package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.b90;
import com.fossil.bc0;
import com.fossil.c90;
import com.fossil.cd6;
import com.fossil.fitness.FitnessData;
import com.fossil.rc6;
import com.fossil.ta0;
import com.fossil.u40;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.IExchangeKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.IReadWorkoutStateSession;
import com.misfit.frameworks.buttonservice.communite.ble.ISyncSession;
import com.misfit.frameworks.buttonservice.communite.ble.IVerifySecretKeySession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferDataSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseTransferSettingsSubFlow;
import com.misfit.frameworks.buttonservice.communite.ble.subflow.BaseVerifySecretKeySubFlow;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.IRemoteFLogger;
import com.misfit.frameworks.buttonservice.log.MFLog;
import com.misfit.frameworks.buttonservice.log.MFLogManager;
import com.misfit.frameworks.buttonservice.log.MFSyncLog;
import com.misfit.frameworks.buttonservice.model.LocalizationData;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.buttonservice.model.alarm.AlarmSetting;
import com.misfit.frameworks.buttonservice.model.background.BackgroundConfig;
import com.misfit.frameworks.buttonservice.model.complicationapp.mapping.ComplicationAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.microapp.mapping.MicroAppMapping;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.watchapp.mapping.WatchAppMappingSettings;
import com.misfit.frameworks.buttonservice.model.watchparams.WatchParamsFileMapping;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.DevicePreferenceUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SyncSession extends EnableMaintainingSession implements ISyncSession, IExchangeKeySession, IReadWorkoutStateSession, IVerifySecretKeySession {
    @DexIgnore
    public BackgroundConfig backgroundConfig;
    @DexIgnore
    public ComplicationAppMappingSettings complicationAppMappingSettings;
    @DexIgnore
    public LocalizationData localizationData;
    @DexIgnore
    public List<? extends MicroAppMapping> microAppMappings;
    @DexIgnore
    public List<AlarmSetting> multiAlarmSettings;
    @DexIgnore
    public AppNotificationFilterSettings notificationFilterSettings;
    @DexIgnore
    public int secondTimezoneOffset;
    @DexIgnore
    public MFSyncLog syncLog;
    @DexIgnore
    public UserProfile userProfile;
    @DexIgnore
    public WatchAppMappingSettings watchAppMappingSettings;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadCurrentWorkoutSession extends BleStateAbs {
        @DexIgnore
        public zb0<c90> task;

        @DexIgnore
        public ReadCurrentWorkoutSession() {
            super(SyncSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SyncSession.this.getBleAdapter().readCurrentWorkoutSession(SyncSession.this.getLogSession(), this);
            if (this.task != null) {
                startTimeout();
                return true;
            } else if (SyncSession.this.getBleAdapter().isSupportedFeature(ta0.class) != null) {
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
                return true;
            } else {
                SyncSession syncSession2 = SyncSession.this;
                syncSession2.enterStateAsync(syncSession2.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return true;
            }
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SyncSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionSuccess(c90 c90) {
            wg6.b(c90, "workoutSession");
            if (c90.getSessionState() == b90.COMPLETED) {
                stopTimeout();
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
            } else if (SyncSession.this.userProfile.getOriginalSyncMode() == 15 || SyncSession.this.userProfile.getOriginalSyncMode() == 12) {
                BleSession.BleSessionCallback access$getBleSessionCallback$p = SyncSession.this.getBleSessionCallback();
                if (access$getBleSessionCallback$p != null) {
                    access$getBleSessionCallback$p.onAskForStopWorkout(SyncSession.this.getSerial());
                }
            } else {
                stopTimeout();
                setMaxRetries(0);
                SyncSession.this.errorLog("Read current workout session; workout is running", ErrorCodeBuilder.Step.READ_WORKOUT, ErrorCodeBuilder.AppError.UNKNOWN);
                SyncSession.this.stop(FailureCode.FAILED_TO_WORKOUT_IS_RUNNING);
            }
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<c90> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
            SyncSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class StopCurrentWorkoutState extends BleStateAbs {
        @DexIgnore
        public zb0<cd6> task;

        @DexIgnore
        public StopCurrentWorkoutState() {
            super(SyncSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = SyncSession.this.getBleAdapter().stopCurrentWorkoutSession(SyncSession.this.getLogSession(), this);
            if (this.task == null) {
                SyncSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            SyncSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        public void onStopCurrentWorkoutSessionSuccess() {
            stopTimeout();
            SyncSession syncSession = SyncSession.this;
            syncSession.enterStateAsync(syncSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<cd6> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
            SyncSession.this.stop(FailureCode.FAILED_TO_STOP_CURRENT_WORKOUT_SESSION);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferDataSubFlow extends BaseTransferDataSubFlow {
        @DexIgnore
        public TransferDataSubFlow() {
            super(SyncSession.this.getTAG(), SyncSession.this, SyncSession.this.syncLog, SyncSession.this.getLogSession(), SyncSession.this.getSerial(), SyncSession.this.getBleAdapter(), SyncSession.this.userProfile, SyncSession.this.getBleSessionCallback(), true);
        }

        @DexIgnore
        public void onStop(int i) {
            if (i != 0) {
                SyncSession.this.stop(i);
                return;
            }
            SyncSession syncSession = SyncSession.this;
            syncSession.enterStateAsync(syncSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class TransferSettingsSubFlow extends BaseTransferSettingsSubFlow {
        @DexIgnore
        public /* final */ /* synthetic */ SyncSession this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public TransferSettingsSubFlow(SyncSession syncSession) {
            super(syncSession.getTAG(), syncSession, syncSession.syncLog, syncSession.getLogSession(), syncSession.isFullSync(), syncSession.getSerial(), syncSession.getBleAdapter(), syncSession.userProfile, syncSession.multiAlarmSettings, syncSession.complicationAppMappingSettings, syncSession.watchAppMappingSettings, syncSession.backgroundConfig, syncSession.notificationFilterSettings, syncSession.localizationData, syncSession.microAppMappings, syncSession.secondTimezoneOffset, syncSession.getBleSessionCallback());
            this.this$0 = syncSession;
        }

        @DexIgnore
        public void onStop(int i) {
            this.this$0.stop(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class VerifySecretKeySessionState extends BaseVerifySecretKeySubFlow {
        @DexIgnore
        public VerifySecretKeySessionState() {
            super(CommunicateMode.LINK, SyncSession.this.getTAG(), SyncSession.this, SyncSession.this.getMfLog(), SyncSession.this.getLogSession(), SyncSession.this.getSerial(), SyncSession.this.getBleAdapter(), SyncSession.this.getBleSessionCallback());
        }

        @DexIgnore
        public void onStop(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String tag = getTAG();
            local.d(tag, "VerifySecretKeySessionState onStop failureCode " + i);
            if (i == u40.REQUEST_UNSUPPORTED.getCode() || i == u40.UNSUPPORTED_FORMAT.getCode() || i == 0) {
                SyncSession syncSession = SyncSession.this;
                syncSession.enterStateAsync(syncSession.createConcreteState(BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW));
                return;
            }
            SyncSession.this.stop(i);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SyncSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, UserProfile userProfile2) {
        super(SessionType.SYNC, CommunicateMode.SYNC, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapter");
        wg6.b(userProfile2, "userProfile");
        this.userProfile = userProfile2;
        setLogSession(FLogger.Session.SYNC);
    }

    @DexIgnore
    public boolean accept(BleSession bleSession) {
        wg6.b(bleSession, "bleSession");
        if (bleSession.getCommunicateMode() != CommunicateMode.SYNC) {
            if (getCommunicateMode() != bleSession.getCommunicateMode()) {
                return true;
            }
        } else if (((SyncSession) bleSession).getSyncMode() == 13) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        MFLog end = MFLogManager.getInstance(getBleAdapter().getContext()).end(CommunicateMode.SYNC, getSerial());
        getExtraInfoReturned().putInt(ButtonService.Companion.getLOG_ID(), end != null ? end.getStartTimeEpoch() : 0);
        getExtraInfoReturned().putLong(Constants.SYNC_ID, this.userProfile.getSyncId());
        getExtraInfoReturned().putInt(ButtonService.Companion.getSYNC_MODE(), this.userProfile.getSyncMode());
        getExtraInfoReturned().putInt(ButtonService.Companion.getORIGINAL_SYNC_MODE(), this.userProfile.getOriginalSyncMode());
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public void confirmStopWorkout(String str, boolean z) {
        wg6.b(str, "serial");
        if (!(getCurrentState() instanceof ReadCurrentWorkoutSession)) {
            return;
        }
        if (z) {
            enterStateAsync(createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
            return;
        }
        errorLog("User canceled stop workout", ErrorCodeBuilder.Step.STOP_WORKOUT, ErrorCodeBuilder.AppError.USER_CANCELLED);
        stop(FailureCode.USER_CANCELLED);
    }

    @DexIgnore
    public BleSession copyObject() {
        SyncSession syncSession = new SyncSession(getBleAdapter(), getBleSessionCallback(), this.userProfile);
        syncSession.setDevice(getDevice());
        return syncSession;
    }

    @DexIgnore
    public void doNextState() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "doNextState, serial=" + getSerial());
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "doNextState, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    public final List<FitnessData> getSyncData() {
        log("getSyncData currentState " + getCurrentState());
        if (!(getCurrentState() instanceof TransferDataSubFlow)) {
            return new ArrayList();
        }
        BleState currentState = getCurrentState();
        if (currentState != null) {
            return ((TransferDataSubFlow) currentState).getSyncData();
        }
        throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferDataSubFlow");
    }

    @DexIgnore
    public int getSyncMode() {
        return this.userProfile.getSyncMode();
    }

    @DexIgnore
    public void initSettings() {
        super.initSettings();
        this.multiAlarmSettings = DevicePreferenceUtils.getAutoListAlarm(getContext());
        this.complicationAppMappingSettings = DevicePreferenceUtils.getAutoComplicationAppSettings(getContext(), getSerial());
        this.watchAppMappingSettings = DevicePreferenceUtils.getAutoWatchAppSettings(getContext(), getSerial());
        this.backgroundConfig = DevicePreferenceUtils.getAutoBackgroundImageConfig(getContext(), getSerial());
        this.notificationFilterSettings = DevicePreferenceUtils.getAutoNotificationFiltersConfig(getContext(), getSerial());
        this.localizationData = DevicePreferenceUtils.getAutoLocalizationDataSettings(getContext(), getSerial());
        this.microAppMappings = MicroAppMapping.convertToMicroAppMapping(DevicePreferenceUtils.getAutoMapping(getContext(), getSerial()));
        this.secondTimezoneOffset = DevicePreferenceUtils.getAutoSecondTimezone(getContext());
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE;
        String name = ReadCurrentWorkoutSession.class.getName();
        wg6.a((Object) name, "ReadCurrentWorkoutSession::class.java.name");
        sessionStateMap.put(sessionState, name);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap2 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState2 = BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE;
        String name2 = StopCurrentWorkoutState.class.getName();
        wg6.a((Object) name2, "StopCurrentWorkoutState::class.java.name");
        sessionStateMap2.put(sessionState2, name2);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap3 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState3 = BleSessionAbs.SessionState.VERIFY_SECRET_KEY;
        String name3 = VerifySecretKeySessionState.class.getName();
        wg6.a((Object) name3, "VerifySecretKeySessionState::class.java.name");
        sessionStateMap3.put(sessionState3, name3);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap4 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState4 = BleSessionAbs.SessionState.TRANSFER_DATA_SUB_FLOW;
        String name4 = TransferDataSubFlow.class.getName();
        wg6.a((Object) name4, "TransferDataSubFlow::class.java.name");
        sessionStateMap4.put(sessionState4, name4);
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap5 = getSessionStateMap();
        BleSessionAbs.SessionState sessionState5 = BleSessionAbs.SessionState.TRANSFER_SETTINGS_SUB_FLOW;
        String name5 = TransferSettingsSubFlow.class.getName();
        wg6.a((Object) name5, "TransferSettingsSubFlow::class.java.name");
        sessionStateMap5.put(sessionState5, name5);
    }

    @DexIgnore
    public final boolean isFullSync() {
        return getSyncMode() == 13;
    }

    @DexIgnore
    public void onGetWatchParamFailed() {
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed");
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).doNextState();
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "onGetWatchParamFailed, can't execute because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void onReceiveRandomKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveRandomKey randomKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveRandomKey(bArr, i);
        }
    }

    @DexIgnore
    public void onReceiveServerResponse(boolean z) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerResponse isSuccess " + z);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceivePushSecretKeyResponse(z);
        }
    }

    @DexIgnore
    public void onReceiveServerSecretKey(byte[] bArr, int i) {
        BleState currentState = getCurrentState();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "onReceiveServerSecretKey secretKey " + bArr + " state " + currentState);
        if (currentState instanceof VerifySecretKeySessionState) {
            ((VerifySecretKeySessionState) currentState).onReceiveServerSecretKey(bArr, i);
        }
    }

    @DexIgnore
    public boolean onStart(Object... objArr) {
        wg6.b(objArr, "params");
        this.syncLog = MFLogManager.getInstance(getBleAdapter().getContext()).startSyncLog(getSerial());
        setMfLog(this.syncLog);
        MFSyncLog mFSyncLog = this.syncLog;
        if (mFSyncLog != null) {
            if (mFSyncLog != null) {
                mFSyncLog.setSerial(getSerial());
                MFSyncLog mFSyncLog2 = this.syncLog;
                if (mFSyncLog2 != null) {
                    mFSyncLog2.setSdkVersion(ButtonService.Companion.getSDKVersion());
                    MFSyncLog mFSyncLog3 = this.syncLog;
                    if (mFSyncLog3 != null) {
                        mFSyncLog3.setFirmwareVersion(getBleAdapter().getFirmwareVersion());
                        MFSyncLog mFSyncLog4 = this.syncLog;
                        if (mFSyncLog4 != null) {
                            mFSyncLog4.setSyncMode(getSyncMode());
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        }
        log("Start new sync session: newDevice=" + this.userProfile.isNewDevice() + ", syncMode=" + this.userProfile.getSyncMode() + ", bluetoothOn=" + BluetoothUtils.isBluetoothEnable() + ", locationServiceEnabled=" + LocationUtils.isLocationEnable(getContext()) + ", locationPermissionAllowed=" + LocationUtils.isLocationPermissionGranted(getContext()));
        return super.onStart(Arrays.copyOf(objArr, objArr.length));
    }

    @DexIgnore
    public void setLatestWatchParam(String str, WatchParamsFileMapping watchParamsFileMapping) {
        wg6.b(str, "serial");
        wg6.b(watchParamsFileMapping, "watchParamsData");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "setLatestWatchParam, serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        IRemoteFLogger remote = FLogger.INSTANCE.getRemote();
        FLogger.Component component = FLogger.Component.BLE;
        FLogger.Session session = FLogger.Session.SYNC;
        String tag2 = getTAG();
        remote.d(component, session, str, tag2, "setLatestWatchParam(), serial=" + str + ", watchParamsData=" + watchParamsFileMapping);
        if (getCurrentState() instanceof TransferSettingsSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferSettingsSubFlow) currentState).setLatestWatchParam(str, watchParamsFileMapping);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferSettingsSubFlow");
        }
        FLogger.INSTANCE.getLocal().d(getTAG(), "setLatestWatchParam, can't set WatchParams because currentState is not an instance of TransferSettingSubFlow");
    }

    @DexIgnore
    public void updateCurrentStepAndStepGoalFromApp(boolean z, UserProfile userProfile2) {
        wg6.b(userProfile2, "userProfile");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String tag = getTAG();
        local.d(tag, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - currentState=" + getCurrentState());
        if (getCurrentState() instanceof TransferDataSubFlow) {
            BleState currentState = getCurrentState();
            if (currentState != null) {
                ((TransferDataSubFlow) currentState).updateCurrentStepAndStepGoalFromApp(z, userProfile2);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.communite.ble.session.SyncSession.TransferDataSubFlow");
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String tag2 = getTAG();
        local2.d(tag2, "Inside " + getTAG() + ".updateCurrentStepAndStepGoalFromApp - cannot update current steps and step goal " + "to device caused by current state null or not an instance of TransferDataSubFlow.");
    }
}
