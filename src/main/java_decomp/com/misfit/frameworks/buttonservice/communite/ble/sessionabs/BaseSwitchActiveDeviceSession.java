package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.bc0;
import com.fossil.q40;
import com.fossil.r40;
import com.fossil.wg6;
import com.fossil.xb0;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleCommunicator;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.buttonservice.model.MisfitDeviceProfile;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BaseSwitchActiveDeviceSession extends EnableMaintainingSession {
    @DexIgnore
    public /* final */ BleCommunicator.CommunicationResultCallback communicationResultCallback;
    @DexIgnore
    public /* final */ UserProfile userProfile;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class FetchDeviceInfoState extends BleStateAbs {
        @DexIgnore
        public zb0<r40> task;

        @DexIgnore
        public FetchDeviceInfoState() {
            super(BaseSwitchActiveDeviceSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = BaseSwitchActiveDeviceSession.this.getBleAdapter().fetchDeviceInfo(BaseSwitchActiveDeviceSession.this.getLogSession(), this);
            if (this.task == null) {
                BaseSwitchActiveDeviceSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onFetchDeviceInfoFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            q40 deviceObj = BaseSwitchActiveDeviceSession.this.getBleAdapter().getDeviceObj();
            if ((deviceObj != null ? deviceObj.getState() : null) != q40.c.CONNECTED) {
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            } else if (!retry(BaseSwitchActiveDeviceSession.this.getBleAdapter().getContext(), BaseSwitchActiveDeviceSession.this.getSerial())) {
                BaseSwitchActiveDeviceSession.this.log("Reach the limit retry. Stop.");
                BaseSwitchActiveDeviceSession.this.stop(FailureCode.FAILED_TO_CONNECT);
            }
        }

        @DexIgnore
        public void onFetchDeviceInfoSuccess(r40 r40) {
            wg6.b(r40, "deviceInformation");
            stopTimeout();
            if (BaseSwitchActiveDeviceSession.this.getBleAdapter().isSupportedFeature(xb0.class) != null) {
                BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession = BaseSwitchActiveDeviceSession.this;
                baseSwitchActiveDeviceSession.enterStateAsync(baseSwitchActiveDeviceSession.createConcreteState(BleSessionAbs.SessionState.VERIFY_SECRET_KEY));
                return;
            }
            BaseSwitchActiveDeviceSession baseSwitchActiveDeviceSession2 = BaseSwitchActiveDeviceSession.this;
            baseSwitchActiveDeviceSession2.enterStateAsync(baseSwitchActiveDeviceSession2.createConcreteState(BleSessionAbs.SessionState.STOP_CURRENT_WORKOUT_STATE));
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<r40> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BaseSwitchActiveDeviceSession(UserProfile userProfile2, BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback, BleCommunicator.CommunicationResultCallback communicationResultCallback2) {
        super(SessionType.UI, CommunicateMode.SWITCH_DEVICE, bleAdapterImpl, bleSessionCallback);
        wg6.b(userProfile2, "userProfile");
        wg6.b(bleAdapterImpl, "bleAdapter");
        this.userProfile = userProfile2;
        this.communicationResultCallback = communicationResultCallback2;
        setSkipEnableMaintainingConnection(true);
        setLogSession(FLogger.Session.OTHER);
        this.userProfile.setNewDevice(true);
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable("device", MisfitDeviceProfile.Companion.cloneFrom(getBleAdapter()));
    }

    @DexIgnore
    public final BleCommunicator.CommunicationResultCallback getCommunicationResultCallback() {
        return this.communicationResultCallback;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE);
    }

    @DexIgnore
    public final UserProfile getUserProfile() {
        return this.userProfile;
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.FETCH_DEVICE_INFO_STATE;
        String name = FetchDeviceInfoState.class.getName();
        wg6.a((Object) name, "FetchDeviceInfoState::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
