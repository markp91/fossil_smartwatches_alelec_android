package com.misfit.frameworks.buttonservice.communite.ble.subflow;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.NullBleState;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.communite.ble.subflow.SubFlow$stopSubFlow$1", f = "SubFlow.kt", l = {}, m = "invokeSuspend")
public final class SubFlow$stopSubFlow$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ int $failureCode;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SubFlow this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public SubFlow$stopSubFlow$Anon1(SubFlow subFlow, int i, xe6 xe6) {
        super(2, xe6);
        this.this$0 = subFlow;
        this.$failureCode = i;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        SubFlow$stopSubFlow$Anon1 subFlow$stopSubFlow$Anon1 = new SubFlow$stopSubFlow$Anon1(this.this$0, this.$failureCode, xe6);
        subFlow$stopSubFlow$Anon1.p$ = (il6) obj;
        return subFlow$stopSubFlow$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((SubFlow$stopSubFlow$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            if (!BleState.Companion.isNull(this.this$0.getMCurrentState())) {
                this.this$0.getMCurrentState().stopTimeout();
                SubFlow subFlow = this.this$0;
                boolean unused = subFlow.enterSubState(new NullBleState(subFlow.getTAG()));
            }
            this.this$0.onStop(this.$failureCode);
            this.this$0.setExist(false);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
