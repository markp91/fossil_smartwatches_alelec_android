package com.misfit.frameworks.buttonservice.communite.ble;

import android.os.Handler;
import android.os.Looper;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class BleState {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ int DELAY_TIME_ENTER_STATE; // = 500;
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public boolean isExist; // = true;
    @DexIgnore
    public int timeout; // = 45000;
    @DexIgnore
    public Handler timeoutHandler; // = new Handler(Looper.getMainLooper());
    @DexIgnore
    public /* final */ Runnable timeoutTask; // = new BleState$timeoutTask$Anon1(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final boolean isNull(BleState bleState) {
            return bleState == null || (bleState instanceof INullState);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface INullState {
    }

    @DexIgnore
    public BleState(String str) {
        wg6.b(str, "tagName");
        this.TAG = str + "." + getClass().getSimpleName();
    }

    @DexIgnore
    public void cancelCurrentBleTask() {
        FLogger.INSTANCE.getLocal().d(this.TAG, ".cancelCurrentBleTask()");
    }

    @DexIgnore
    public final String getTAG() {
        return this.TAG;
    }

    @DexIgnore
    public final int getTimeout() {
        return this.timeout;
    }

    @DexIgnore
    public final boolean isExist() {
        return this.isExist;
    }

    @DexIgnore
    public final boolean isInterruptable() {
        return false;
    }

    @DexIgnore
    public final void logUnexpectedCallback() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.i(str, toString() + " State " + " not expected to " + stackTrace[1]);
    }

    @DexIgnore
    public boolean onEnter() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Enter state: ");
        String str2 = this.TAG;
        Locale locale = Locale.US;
        wg6.a((Object) locale, "Locale.US");
        if (str2 != null) {
            String upperCase = str2.toUpperCase(locale);
            wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            sb.append(upperCase);
            local.d(str, sb.toString());
            return true;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void onExit() {
        stopTimeout();
        this.isExist = false;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        StringBuilder sb = new StringBuilder();
        sb.append("Exit state: ");
        String str2 = this.TAG;
        Locale locale = Locale.US;
        wg6.a((Object) locale, "Locale.US");
        if (str2 != null) {
            String upperCase = str2.toUpperCase(locale);
            wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase(locale)");
            sb.append(upperCase);
            local.d(str, sb.toString());
            return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public void onFatal(int i) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, "Fatal state: " + this.TAG + ", failureCode=" + i);
    }

    @DexIgnore
    public void onTimeout() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".onTimeout() state: " + this.TAG);
    }

    @DexIgnore
    public final void setExist(boolean z) {
        this.isExist = z;
    }

    @DexIgnore
    public final void setTimeout(int i) {
        this.timeout = i;
    }

    @DexIgnore
    public final synchronized void startTimeout() {
        stopTimeout();
        long j = (long) this.timeout;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.TAG;
        local.d(str, ".startTimeout() " + (j / ((long) 1000)) + " second(s)");
        if (this.isExist) {
            this.timeoutHandler.postDelayed(this.timeoutTask, j);
        }
    }

    @DexIgnore
    public final synchronized void stopTimeout() {
        FLogger.INSTANCE.getLocal().d(this.TAG, ".stopTimeout()");
        this.timeoutHandler.removeCallbacks(this.timeoutTask);
    }
}
