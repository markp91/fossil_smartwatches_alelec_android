package com.misfit.frameworks.buttonservice.communite.ble.sessionabs;

import com.fossil.cd6;
import com.fossil.hg6;
import com.fossil.jh6;
import com.fossil.qb0;
import com.fossil.wg6;
import com.fossil.xg6;
import com.fossil.zd0;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.ReplyMessageMappingGroup;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1 extends xg6 implements hg6<cd6, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ISessionSdkCallback $callback$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ FLogger.Session $logSession$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ qb0 $replyMessageFeature$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ ReplyMessageMappingGroup $replyMessageGroup$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ jh6 $replyMessageIcon$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ zd0[] $replyMessageMapping$inlined;
    @DexIgnore
    public /* final */ /* synthetic */ BleAdapterImpl this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public BleAdapterImpl$setReplyMessageMapping$$inlined$use$lambda$Anon1(zd0[] zd0Arr, BleAdapterImpl bleAdapterImpl, jh6 jh6, ReplyMessageMappingGroup replyMessageMappingGroup, qb0 qb0, FLogger.Session session, ISessionSdkCallback iSessionSdkCallback) {
        super(1);
        this.$replyMessageMapping$inlined = zd0Arr;
        this.this$0 = bleAdapterImpl;
        this.$replyMessageIcon$inlined = jh6;
        this.$replyMessageGroup$inlined = replyMessageMappingGroup;
        this.$replyMessageFeature$inlined = qb0;
        this.$logSession$inlined = session;
        this.$callback$inlined = iSessionSdkCallback;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        invoke((cd6) obj);
        return cd6.a;
    }

    @DexIgnore
    public final void invoke(cd6 cd6) {
        wg6.b(cd6, "it");
        this.this$0.log(this.$logSession$inlined, "setReplyMessageMapping Success");
        this.$callback$inlined.onSetReplyMessageMappingSuccess();
    }
}
