package com.misfit.frameworks.buttonservice.communite.ble.session;

import com.fossil.bc0;
import com.fossil.c90;
import com.fossil.wg6;
import com.fossil.zb0;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.communite.SessionType;
import com.misfit.frameworks.buttonservice.communite.ble.BleSession;
import com.misfit.frameworks.buttonservice.communite.ble.BleState;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleAdapterImpl;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleSessionAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.BleStateAbs;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.EnableMaintainingSession;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.misfit.frameworks.common.constants.Constants;
import java.util.HashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ReadCurrentWorkoutSessionSession extends EnableMaintainingSession {
    @DexIgnore
    public c90 mCurrentWorkoutSession;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class ReadCurrentWorkoutSession extends BleStateAbs {
        @DexIgnore
        public zb0<c90> task;

        @DexIgnore
        public ReadCurrentWorkoutSession() {
            super(ReadCurrentWorkoutSessionSession.this.getTAG());
        }

        @DexIgnore
        public boolean onEnter() {
            super.onEnter();
            this.task = ReadCurrentWorkoutSessionSession.this.getBleAdapter().readCurrentWorkoutSession(ReadCurrentWorkoutSessionSession.this.getLogSession(), this);
            if (this.task == null) {
                ReadCurrentWorkoutSessionSession.this.stop(10000);
                return true;
            }
            startTimeout();
            return true;
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionFailed(bc0 bc0) {
            wg6.b(bc0, Constants.YO_ERROR_POST);
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.stop(FailureCode.FAILED_TO_READ_CURRENT_WORKOUT_SESSION);
        }

        @DexIgnore
        public void onReadCurrentWorkoutSessionSuccess(c90 c90) {
            wg6.b(c90, "workoutSession");
            stopTimeout();
            ReadCurrentWorkoutSessionSession.this.mCurrentWorkoutSession = c90;
            ReadCurrentWorkoutSessionSession.this.stop(0);
        }

        @DexIgnore
        public void onTimeout() {
            super.onTimeout();
            zb0<c90> zb0 = this.task;
            if (zb0 != null) {
                zb0.e();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ReadCurrentWorkoutSessionSession(BleAdapterImpl bleAdapterImpl, BleSession.BleSessionCallback bleSessionCallback) {
        super(SessionType.UI, CommunicateMode.READ_CURRENT_WORKOUT_SESSION, bleAdapterImpl, bleSessionCallback);
        wg6.b(bleAdapterImpl, "bleAdapterV2");
    }

    @DexIgnore
    public void buildExtraInfoReturned() {
        getExtraInfoReturned().putParcelable(com.misfit.frameworks.buttonservice.utils.Constants.CURRENT_WORKOUT_SESSION, this.mCurrentWorkoutSession);
    }

    @DexIgnore
    public BleSession copyObject() {
        ReadCurrentWorkoutSessionSession readCurrentWorkoutSessionSession = new ReadCurrentWorkoutSessionSession(getBleAdapter(), getBleSessionCallback());
        readCurrentWorkoutSessionSession.setDevice(getDevice());
        return readCurrentWorkoutSessionSession;
    }

    @DexIgnore
    public BleState getStateAfterEnableMaintainingConnection() {
        return createConcreteState(BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE);
    }

    @DexIgnore
    public void initStateMap() {
        super.initStateMap();
        HashMap<BleSessionAbs.SessionState, String> sessionStateMap = getSessionStateMap();
        BleSessionAbs.SessionState sessionState = BleSessionAbs.SessionState.READ_CURRENT_WORKOUT_STATE;
        String name = ReadCurrentWorkoutSession.class.getName();
        wg6.a((Object) name, "ReadCurrentWorkoutSession::class.java.name");
        sessionStateMap.put(sessionState, name);
    }
}
