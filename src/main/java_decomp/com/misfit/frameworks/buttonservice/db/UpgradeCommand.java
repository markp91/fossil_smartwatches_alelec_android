package com.misfit.frameworks.buttonservice.db;

import android.database.sqlite.SQLiteDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface UpgradeCommand {
    @DexIgnore
    void execute(SQLiteDatabase sQLiteDatabase);
}
