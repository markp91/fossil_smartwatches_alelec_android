package com.misfit.frameworks.buttonservice.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class HwLogProvider extends BaseDbProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "hw_log.db";
    @DexIgnore
    public static HwLogProvider sInstance;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class Anon1_Level2 implements UpgradeCommand {
            @DexIgnore
            public Anon1_Level2() {
            }

            @DexIgnore
            public void execute(SQLiteDatabase sQLiteDatabase) {
                sQLiteDatabase.execSQL("ALTER TABLE hardwarelog ADD COLUMN read INTEGER");
            }
        }

        @DexIgnore
        public Anon1() {
            put(2, new Anon1_Level2());
        }
    }

    @DexIgnore
    public HwLogProvider(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<HardwareLog, String> getHardwareLogDao() throws SQLException {
        return this.databaseHelper.getDao(HardwareLog.class);
    }

    @DexIgnore
    public static synchronized HwLogProvider getInstance(Context context) {
        HwLogProvider hwLogProvider;
        synchronized (HwLogProvider.class) {
            if (sInstance == null) {
                sInstance = new HwLogProvider(context, DB_NAME);
            }
            hwLogProvider = sInstance;
        }
        return hwLogProvider;
    }

    @DexIgnore
    public void clearHwLog() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.i(str, "Inside " + this.TAG + ".clearHwLog");
            getHardwareLogDao().deleteBuilder().delete();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.e(str2, "Error inside " + this.TAG + ".clearHwLog - e=" + e);
        }
    }

    @DexIgnore
    public List<HardwareLog> getAllHardwareLogs() {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<HardwareLog, String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().eq(HardwareLog.COLUMN_READ, false);
            List<HardwareLog> query = getHardwareLogDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Error inside " + this.TAG + ".getAllHardwareLogs - e=" + e);
        }
        return arrayList;
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{HardwareLog.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 2;
    }

    @DexIgnore
    public HardwareLog getHardwareLog(String str) {
        try {
            QueryBuilder<HardwareLog, String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().eq("serial", str);
            return getHardwareLogDao().queryForFirst(queryBuilder.prepare());
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".getHardwareLog - =e" + e);
            return null;
        }
    }

    @DexIgnore
    public void saveHwLog(HardwareLog hardwareLog) {
        try {
            getHardwareLogDao().create(hardwareLog);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.e(str, "Error inside " + this.TAG + ".saveHwLog - e=" + e);
        }
    }

    @DexIgnore
    public void setHardwareLogRead() {
        try {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.TAG;
            local.i(str, "Inside " + this.TAG + ".setHardwareLogRead");
            UpdateBuilder<HardwareLog, String> updateBuilder = getHardwareLogDao().updateBuilder();
            updateBuilder.updateColumnValue(HardwareLog.COLUMN_READ, true);
            updateBuilder.update();
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local2.e(str2, "Error inside " + this.TAG + ".setHardwareLogRead - e=" + e);
        }
    }

    @DexIgnore
    public List<HardwareLog> getAllHardwareLogs(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<HardwareLog, String> queryBuilder = getHardwareLogDao().queryBuilder();
            queryBuilder.where().lt("serial", str);
            List<HardwareLog> query = getHardwareLogDao().query(queryBuilder.prepare());
            if (query != null) {
                arrayList.addAll(query);
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".getAllHardwareLogs - e=" + e);
        }
        return arrayList;
    }
}
