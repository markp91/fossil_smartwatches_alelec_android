package com.misfit.frameworks.buttonservice.model.vibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.rc6;
import com.fossil.wg6;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VibrationStrengthObj$Companion$CREATOR$Anon1 implements Parcelable.Creator<VibrationStrengthObj> {
    @DexIgnore
    public VibrationStrengthObj createFromParcel(Parcel parcel) {
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        if (readString != null) {
            try {
                Class<?> cls = Class.forName(readString);
                wg6.a((Object) cls, "Class.forName(dynamicClassName!!)");
                Constructor<?> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Parcel.class});
                wg6.a((Object) declaredConstructor, "dynamicClass.getDeclared\u2026uctor(Parcel::class.java)");
                declaredConstructor.setAccessible(true);
                Object newInstance = declaredConstructor.newInstance(new Object[]{parcel});
                if (newInstance != null) {
                    return (VibrationStrengthObj) newInstance;
                }
                throw new rc6("null cannot be cast to non-null type com.misfit.frameworks.buttonservice.model.vibration.VibrationStrengthObj");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchMethodException e2) {
                e2.printStackTrace();
                return null;
            } catch (IllegalAccessException e3) {
                e3.printStackTrace();
                return null;
            } catch (InstantiationException e4) {
                e4.printStackTrace();
                return null;
            } catch (InvocationTargetException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public VibrationStrengthObj[] newArray(int i) {
        return new VibrationStrengthObj[i];
    }
}
