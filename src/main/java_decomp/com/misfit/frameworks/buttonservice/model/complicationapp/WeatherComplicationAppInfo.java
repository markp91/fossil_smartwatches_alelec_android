package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import com.fossil.ca0;
import com.fossil.e90;
import com.fossil.h70;
import com.fossil.j70;
import com.fossil.kc6;
import com.fossil.tc0;
import com.fossil.v80;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.fossil.zc0;
import com.portfolio.platform.data.model.MFUser;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WeatherComplicationAppInfo extends DeviceAppResponse {
    @DexIgnore
    public /* final */ long expiredAt;
    @DexIgnore
    public float temperature;
    @DexIgnore
    public TemperatureUnit temperatureUnit;
    @DexIgnore
    public WeatherCondition weatherCondition;

    @DexIgnore
    public enum TemperatureUnit {
        C,
        F;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[TemperatureUnit.values().length];
                $EnumSwitchMapping$0[TemperatureUnit.C.ordinal()] = 1;
                $EnumSwitchMapping$0[TemperatureUnit.F.ordinal()] = 2;
            }
            */
        }

        @DexIgnore
        public final h70 toSdkTemperatureUnit() {
            int i = WhenMappings.$EnumSwitchMapping$0[ordinal()];
            if (i == 1) {
                return h70.C;
            }
            if (i == 2) {
                return h70.F;
            }
            throw new kc6();
        }
    }

    @DexIgnore
    public enum WeatherCondition {
        CLEAR_DAY,
        CLEAR_NIGHT,
        RAIN,
        SNOW,
        SLEET,
        WIND,
        FOG,
        CLOUDY,
        PARTLY_CLOUDY_DAY,
        PARTLY_CLOUDY_NIGHT;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = null;

            /*
            static {
                $EnumSwitchMapping$0 = new int[WeatherCondition.values().length];
                $EnumSwitchMapping$0[WeatherCondition.CLEAR_DAY.ordinal()] = 1;
                $EnumSwitchMapping$0[WeatherCondition.CLEAR_NIGHT.ordinal()] = 2;
                $EnumSwitchMapping$0[WeatherCondition.RAIN.ordinal()] = 3;
                $EnumSwitchMapping$0[WeatherCondition.SNOW.ordinal()] = 4;
                $EnumSwitchMapping$0[WeatherCondition.SLEET.ordinal()] = 5;
                $EnumSwitchMapping$0[WeatherCondition.WIND.ordinal()] = 6;
                $EnumSwitchMapping$0[WeatherCondition.FOG.ordinal()] = 7;
                $EnumSwitchMapping$0[WeatherCondition.CLOUDY.ordinal()] = 8;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_DAY.ordinal()] = 9;
                $EnumSwitchMapping$0[WeatherCondition.PARTLY_CLOUDY_NIGHT.ordinal()] = 10;
            }
            */
        }

        @DexIgnore
        public final j70 toSdkWeatherCondition() {
            switch (WhenMappings.$EnumSwitchMapping$0[ordinal()]) {
                case 1:
                    return j70.CLEAR_DAY;
                case 2:
                    return j70.CLEAR_NIGHT;
                case 3:
                    return j70.RAIN;
                case 4:
                    return j70.SNOW;
                case 5:
                    return j70.SLEET;
                case 6:
                    return j70.WIND;
                case 7:
                    return j70.FOG;
                case 8:
                    return j70.CLOUDY;
                case 9:
                    return j70.PARTLY_CLOUDY_DAY;
                case 10:
                    return j70.PARTLY_CLOUDY_NIGHT;
                default:
                    throw new kc6();
            }
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(float f, TemperatureUnit temperatureUnit2, WeatherCondition weatherCondition2, long j) {
        super(e90.WEATHER_COMPLICATION);
        wg6.b(temperatureUnit2, MFUser.TEMPERATURE_UNIT);
        wg6.b(weatherCondition2, "weatherCondition");
        this.temperature = f;
        this.temperatureUnit = temperatureUnit2;
        this.weatherCondition = weatherCondition2;
        this.expiredAt = j;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return new zc0(new v80(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof ca0)) {
            return null;
        }
        return new zc0((ca0) x90, new v80(this.expiredAt, this.temperatureUnit.toSdkTemperatureUnit(), this.temperature, this.weatherCondition.toSdkWeatherCondition()));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.temperature);
        parcel.writeInt(this.temperatureUnit.ordinal());
        parcel.writeInt(this.weatherCondition.ordinal());
        parcel.writeLong(this.expiredAt);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public WeatherComplicationAppInfo(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.temperature = parcel.readFloat();
        this.temperatureUnit = TemperatureUnit.values()[parcel.readInt()];
        this.weatherCondition = WeatherCondition.values()[parcel.readInt()];
        this.expiredAt = parcel.readLong();
    }
}
