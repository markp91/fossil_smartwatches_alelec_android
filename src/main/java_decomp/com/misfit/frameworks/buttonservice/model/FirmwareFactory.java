package com.misfit.frameworks.buttonservice.model;

import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFactory {
    @DexIgnore
    public static FirmwareFactory INSTANCE;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE; // = new int[FossilDeviceSerialPatternUtil.DEVICE.values().length];

        /*
        static {
            try {
                $SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.DEVICE.SAM.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface BundleFirmware {
        @DexIgnore
        public static /* final */ String FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5; // = "HW0.0.1.0r.v2_autotest_v5";
        @DexIgnore
        public static /* final */ String FIRMWARE_HW_14R_PROD_V3_DISABLE_HIBERNATION; // = "HW0.0.0.14r.prod.v3_disable_hibernation";
        @DexIgnore
        public static /* final */ String FIRMWARE_HW_15R_V2; // = "HW0.0.0.15r.v2";
        @DexIgnore
        public static /* final */ String FIRMWARE_OTA_DN_0_0_0_3T_V2; // = "DN0.0.0.3t.v2";
    }

    @DexIgnore
    public static List<FirmwareData> getAllOfBundleFirmwares() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new FirmwareData(BundleFirmware.FIRMWARE_HW_14R_PROD_V3_DISABLE_HIBERNATION, "HW.0.0", R.raw.sam_ota_hw0_0_0_14r_prod_v3_disable_hibernation));
        arrayList.add(new FirmwareData(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5, "HW.0.0", R.raw.sam_ota_hw0_0_1_0r_v2_autotest_v5));
        arrayList.add(new FirmwareData(BundleFirmware.FIRMWARE_HW_15R_V2, "HW.0.0", R.raw.sam_ota_20170808_hw0_0_0_15r_v2_crc_encr));
        arrayList.add(new FirmwareData(BundleFirmware.FIRMWARE_OTA_DN_0_0_0_3T_V2, "DN.0.0", R.raw.diana_ota_dn0_0_0_3t_v2));
        return arrayList;
    }

    @DexIgnore
    public static synchronized FirmwareFactory getInstance() {
        FirmwareFactory firmwareFactory;
        synchronized (FirmwareFactory.class) {
            if (INSTANCE == null) {
                INSTANCE = new FirmwareFactory();
            }
            firmwareFactory = INSTANCE;
        }
        return firmwareFactory;
    }

    @DexIgnore
    public FirmwareData createFirmwareData(String str, String str2, String str3) {
        return new FirmwareData(str, str2, str3);
    }

    @DexIgnore
    public List<FirmwareData> getAllLatestBundleFirmwares() {
        ArrayList arrayList = new ArrayList();
        arrayList.add(new FirmwareData(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5, "HW.0.0", R.raw.sam_ota_hw0_0_1_0r_v2_autotest_v5));
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public FirmwareData getBundleFirmware(String str) {
        char c;
        switch (str.hashCode()) {
            case -1951128539:
                if (str.equals(BundleFirmware.FIRMWARE_HW_15R_V2)) {
                    c = 0;
                    break;
                }
            case -854215618:
                if (str.equals(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5)) {
                    c = 1;
                    break;
                }
            case -733438869:
                if (str.equals(BundleFirmware.FIRMWARE_HW_14R_PROD_V3_DISABLE_HIBERNATION)) {
                    c = 2;
                    break;
                }
            case 922419941:
                if (str.equals(BundleFirmware.FIRMWARE_OTA_DN_0_0_0_3T_V2)) {
                    c = 3;
                    break;
                }
            default:
                c = 65535;
                break;
        }
        if (c == 0) {
            return new FirmwareData(BundleFirmware.FIRMWARE_HW_15R_V2, "HW.0.0", R.raw.sam_ota_20170808_hw0_0_0_15r_v2_crc_encr);
        }
        if (c == 1) {
            return new FirmwareData(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5, "HW.0.0", R.raw.sam_ota_hw0_0_1_0r_v2_autotest_v5);
        }
        if (c == 2) {
            return new FirmwareData(BundleFirmware.FIRMWARE_HW_14R_PROD_V3_DISABLE_HIBERNATION, "HW.0.0", R.raw.sam_ota_hw0_0_0_14r_prod_v3_disable_hibernation);
        }
        if (c != 3) {
            return new FirmwareData(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5, "HW.0.0", R.raw.sam_ota_hw0_0_1_0r_v2_autotest_v5);
        }
        return new FirmwareData(BundleFirmware.FIRMWARE_OTA_DN_0_0_0_3T_V2, "DN.0.0", R.raw.diana_ota_dn0_0_0_3t_v2);
    }

    @DexIgnore
    public FirmwareData getLatestBundleFirmware(String str) {
        if (Anon1.$SwitchMap$com$misfit$frameworks$buttonservice$utils$FossilDeviceSerialPatternUtil$DEVICE[FossilDeviceSerialPatternUtil.getDeviceBySerial(str).ordinal()] != 1) {
            return null;
        }
        return new FirmwareData(BundleFirmware.FIRMWARE_HW_0_1_Or_V2_AUTOMATION_TEST_V5, "HW.0.0", R.raw.sam_ota_hw0_0_1_0r_v2_autotest_v5);
    }
}
