package com.misfit.frameworks.buttonservice.model.vibration;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.a70;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class VibrationStrengthObj implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<VibrationStrengthObj> CREATOR; // = new VibrationStrengthObj$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public /* final */ boolean isDefaultValue;
    @DexIgnore
    public /* final */ int vibrationStrengthLevel;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final /* synthetic */ class WhenMappings {
            @DexIgnore
            public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[a70.a.values().length];

            /*
            static {
                $EnumSwitchMapping$0[a70.a.LOW.ordinal()] = 1;
                $EnumSwitchMapping$0[a70.a.MEDIUM.ordinal()] = 2;
                $EnumSwitchMapping$0[a70.a.HIGH.ordinal()] = 3;
            }
            */
        }

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ VibrationStrengthObj consumeSDKVibrationStrengthLevel$default(Companion companion, a70.a aVar, boolean z, int i, Object obj) {
            if ((i & 2) != 0) {
                z = false;
            }
            return companion.consumeSDKVibrationStrengthLevel(aVar, z);
        }

        @DexIgnore
        public final VibrationStrengthObj consumeSDKVibrationStrengthLevel(a70.a aVar, boolean z) {
            wg6.b(aVar, "vibrationStrengthLevel");
            int i = WhenMappings.$EnumSwitchMapping$0[aVar.ordinal()];
            int i2 = 2;
            if (i == 1) {
                i2 = 1;
            } else if (i != 2 && i == 3) {
                i2 = 3;
            }
            return new VibrationStrengthObj(i2, z);
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public VibrationStrengthObj(int i, boolean z) {
        this.vibrationStrengthLevel = i;
        this.isDefaultValue = z;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final int getVibrationStrengthLevel() {
        return this.vibrationStrengthLevel;
    }

    @DexIgnore
    public final boolean isDefaultValue() {
        return this.isDefaultValue;
    }

    @DexIgnore
    public final a70.a toSDKVibrationStrengthLevel() {
        int i = this.vibrationStrengthLevel;
        if (i == 1) {
            return a70.a.LOW;
        }
        if (i == 2) {
            return a70.a.MEDIUM;
        }
        if (i != 3) {
            return a70.a.MEDIUM;
        }
        return a70.a.HIGH;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(VibrationStrengthObj.class.getName());
        parcel.writeInt(this.vibrationStrengthLevel);
        parcel.writeInt(this.isDefaultValue ? 1 : 0);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ VibrationStrengthObj(int i, boolean z, int i2, qg6 qg6) {
        this(i, (i2 & 2) != 0 ? false : z);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public VibrationStrengthObj(Parcel parcel) {
        this(parcel.readInt(), parcel.readInt() != 1 ? false : true);
        wg6.b(parcel, "parcel");
    }
}
