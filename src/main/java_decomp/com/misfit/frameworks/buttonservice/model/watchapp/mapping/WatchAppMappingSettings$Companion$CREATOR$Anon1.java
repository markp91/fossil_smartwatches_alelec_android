package com.misfit.frameworks.buttonservice.model.watchapp.mapping;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WatchAppMappingSettings$Companion$CREATOR$Anon1 implements Parcelable.Creator<WatchAppMappingSettings> {
    @DexIgnore
    public WatchAppMappingSettings createFromParcel(Parcel parcel) {
        wg6.b(parcel, "parcel");
        return new WatchAppMappingSettings(parcel, (qg6) null);
    }

    @DexIgnore
    public WatchAppMappingSettings[] newArray(int i) {
        return new WatchAppMappingSettings[i];
    }
}
