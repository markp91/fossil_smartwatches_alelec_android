package com.misfit.frameworks.buttonservice.model.complicationapp;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.e90;
import com.fossil.qg6;
import com.fossil.tc0;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.model.LifeTimeObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class DeviceAppResponse implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<DeviceAppResponse> CREATOR; // = new DeviceAppResponse$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ long LIFE_TIME; // = 8000;
    @DexIgnore
    public e90 deviceEventId;
    @DexIgnore
    public boolean isForceUpdate;
    @DexIgnore
    public /* final */ LifeTimeObject lifeTimeObject;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public DeviceAppResponse(e90 e90) {
        wg6.b(e90, "deviceEventId");
        this.deviceEventId = e90;
        this.lifeTimeObject = new LifeTimeObject(LIFE_TIME);
    }

    @DexIgnore
    public static /* synthetic */ tc0 getSDKDeviceResponse$default(DeviceAppResponse deviceAppResponse, x90 x90, w40 w40, int i, Object obj) {
        if (obj == null) {
            if ((i & 2) != 0) {
                w40 = null;
            }
            return deviceAppResponse.getSDKDeviceResponse(x90, w40);
        }
        throw new UnsupportedOperationException("Super calls with default arguments not supported in this target, function: getSDKDeviceResponse");
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final e90 getDeviceEventId() {
        return this.deviceEventId;
    }

    @DexIgnore
    public final LifeTimeObject getLifeTimeObject() {
        return this.lifeTimeObject;
    }

    @DexIgnore
    public abstract tc0 getSDKDeviceData();

    @DexIgnore
    public abstract tc0 getSDKDeviceResponse(x90 x90, w40 w40);

    @DexIgnore
    public final boolean isForceUpdate() {
        return this.isForceUpdate;
    }

    @DexIgnore
    public final void setDeviceEventId(e90 e90) {
        wg6.b(e90, "<set-?>");
        this.deviceEventId = e90;
    }

    @DexIgnore
    public final void setForceUpdate(boolean z) {
        this.isForceUpdate = z;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(getClass().getName());
        parcel.writeInt(this.deviceEventId.ordinal());
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public DeviceAppResponse(Parcel parcel) {
        this(e90.values()[parcel.readInt()]);
        wg6.b(parcel, "parcel");
    }
}
