package com.misfit.frameworks.buttonservice.model.watchapp.response.commutetime;

import android.os.Parcel;
import com.fossil.dd0;
import com.fossil.e90;
import com.fossil.kc6;
import com.fossil.sc0;
import com.fossil.tc0;
import com.fossil.uc0;
import com.fossil.w40;
import com.fossil.w90;
import com.fossil.wg6;
import com.fossil.x90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeWatchAppMessage extends DeviceAppResponse {
    @DexIgnore
    public dd0 deviceMessageType;
    @DexIgnore
    public String message;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final /* synthetic */ class WhenMappings {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $EnumSwitchMapping$0; // = new int[dd0.values().length];

        /*
        static {
            $EnumSwitchMapping$0[dd0.END.ordinal()] = 1;
            $EnumSwitchMapping$0[dd0.IN_PROGRESS.ordinal()] = 2;
        }
        */
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(String str, dd0 dd0) {
        super(e90.COMMUTE_TIME_WATCH_APP);
        wg6.b(str, "message");
        wg6.b(dd0, "deviceMessageType");
        this.message = "";
        this.deviceMessageType = dd0.END;
        this.message = str;
        this.deviceMessageType = dd0;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return new sc0(new uc0(this.message, this.deviceMessageType));
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof w90)) {
            return null;
        }
        int i = WhenMappings.$EnumSwitchMapping$0[this.deviceMessageType.ordinal()];
        if (i == 1) {
            return new sc0((w90) x90, new uc0(this.message, this.deviceMessageType));
        }
        if (i == 2) {
            return new sc0(new uc0(this.message, this.deviceMessageType));
        }
        throw new kc6();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeString(this.message);
        parcel.writeInt(this.deviceMessageType.ordinal());
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeWatchAppMessage(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        String str = "";
        this.message = str;
        this.deviceMessageType = dd0.END;
        String readString = parcel.readString();
        this.message = readString != null ? readString : str;
        this.deviceMessageType = dd0.values()[parcel.readInt()];
    }
}
