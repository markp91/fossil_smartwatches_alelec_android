package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;
import com.google.gson.Gson;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ShineDevice extends Device implements Parcelable, Comparable<Object> {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ShineDevice> CREATOR; // = new ShineDevice$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public final ShineDevice clone(ScannedDevice scannedDevice) {
            wg6.b(scannedDevice, "device");
            return new ShineDevice(scannedDevice.getDeviceSerial(), scannedDevice.getDeviceName(), scannedDevice.getDeviceMACAddress(), scannedDevice.getRssi(), scannedDevice.getFastPairIdInHex());
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ShineDevice(String str, String str2, String str3, int i, String str4) {
        super(str, str2, str3, i, str4);
        wg6.b(str, "serial");
        wg6.b(str2, "name");
        wg6.b(str3, "macAddress");
        wg6.b(str4, "fastPairId");
    }

    @DexIgnore
    public int compareTo(Object obj) {
        wg6.b(obj, "other");
        if (!(obj instanceof ShineDevice)) {
            return 1;
        }
        ShineDevice shineDevice = (ShineDevice) obj;
        if (shineDevice.getRssi() == getRssi()) {
            return 0;
        }
        if (shineDevice.getRssi() > getRssi()) {
            return 1;
        }
        return -1;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "dest");
        parcel.writeString(this.serial);
        parcel.writeString(this.name);
        parcel.writeString(this.macAddress);
        parcel.writeInt(this.rssi);
        parcel.writeString(this.fastPairId);
    }

    @DexIgnore
    public ShineDevice(Parcel parcel) {
        wg6.b(parcel, "parcel");
        this.serial = parcel.readString();
        this.name = parcel.readString();
        this.macAddress = parcel.readString();
        this.rssi = parcel.readInt();
        this.fastPairId = parcel.readString();
    }
}
