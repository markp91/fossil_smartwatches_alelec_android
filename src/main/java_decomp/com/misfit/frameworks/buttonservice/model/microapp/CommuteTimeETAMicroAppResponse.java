package com.misfit.frameworks.buttonservice.model.microapp;

import android.os.Parcel;
import com.fossil.e90;
import com.fossil.qc0;
import com.fossil.tc0;
import com.fossil.u90;
import com.fossil.w40;
import com.fossil.wg6;
import com.fossil.x90;
import com.misfit.frameworks.buttonservice.model.complicationapp.DeviceAppResponse;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CommuteTimeETAMicroAppResponse extends DeviceAppResponse {
    @DexIgnore
    public int mHour;
    @DexIgnore
    public int mMinute;

    @DexIgnore
    public CommuteTimeETAMicroAppResponse(int i, int i2) {
        super(e90.COMMUTE_TIME_ETA_MICRO_APP);
        this.mHour = i;
        this.mMinute = i2;
    }

    @DexIgnore
    public tc0 getSDKDeviceData() {
        return null;
    }

    @DexIgnore
    public tc0 getSDKDeviceResponse(x90 x90, w40 w40) {
        wg6.b(x90, "deviceRequest");
        if (!(x90 instanceof u90) || w40 == null) {
            return null;
        }
        return new qc0((u90) x90, w40, this.mHour, this.mMinute);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        super.writeToParcel(parcel, i);
        parcel.writeInt(this.mHour);
        parcel.writeInt(this.mMinute);
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CommuteTimeETAMicroAppResponse(Parcel parcel) {
        super(parcel);
        wg6.b(parcel, "parcel");
        this.mHour = parcel.readInt();
        this.mMinute = parcel.readInt();
    }
}
