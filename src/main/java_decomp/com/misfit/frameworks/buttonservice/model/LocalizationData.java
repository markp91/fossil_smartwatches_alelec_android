package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.cd6;
import com.fossil.kd0;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.fossil.xf6;
import com.fossil.yf6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.FileInputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LocalizationData implements Parcelable {
    @DexIgnore
    public static Parcelable.Creator<LocalizationData> CREATOR; // = new LocalizationData$Companion$CREATOR$Anon1();
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public String checkSum;
    @DexIgnore
    public byte[] data;
    @DexIgnore
    public /* final */ String filePath;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        private final String bytesToString(byte[] bArr) {
            StringBuilder sb = new StringBuilder();
            int length = bArr.length;
            int i = 0;
            while (i < length) {
                String num = Integer.toString((bArr[i] & 255) + 256, 16);
                wg6.a((Object) num, "Integer.toString((bInput\u2026t() and 255) + 0x100, 16)");
                if (num != null) {
                    String substring = num.substring(1);
                    wg6.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                    sb.append(substring);
                    i++;
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            String sb2 = sb.toString();
            wg6.a((Object) sb2, "ret.toString()");
            if (sb2 != null) {
                String lowerCase = sb2.toLowerCase();
                wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                return lowerCase;
            }
            throw new rc6("null cannot be cast to non-null type java.lang.String");
        }

        @DexIgnore
        public final String getLocalizationDataCheckSum(LocalizationData localizationData) {
            wg6.b(localizationData, "localizationData");
            if (wg6.a((Object) localizationData.getCheckSum(), (Object) "")) {
                return bytesToString(localizationData.toLocalizationFile().getData());
            }
            return localizationData.getCheckSum();
        }

        @DexIgnore
        public final boolean isTheSameFile(LocalizationData localizationData, LocalizationData localizationData2) {
            wg6.b(localizationData2, "newLocalizationData");
            if (localizationData == null) {
                return false;
            }
            String localizationDataCheckSum = getLocalizationDataCheckSum(localizationData2);
            if (!wg6.a((Object) localizationData.getFilePath(), (Object) localizationData2.getFilePath()) || !wg6.a((Object) localizationData.getCheckSum(), (Object) localizationDataCheckSum)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public LocalizationData(String str, String str2) {
        wg6.b(str, "filePath");
        wg6.b(str2, "checkSum");
        this.filePath = str;
        this.checkSum = str2;
        initialize();
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final String getCheckSum() {
        return this.checkSum;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.data;
    }

    @DexIgnore
    public final String getFilePath() {
        return this.filePath;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        com.fossil.yf6.a(r0, r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001a, code lost:
        throw r2;
     */
    @DexIgnore
    public final void initialize() {
        try {
            FileInputStream fileInputStream = new FileInputStream(this.filePath);
            this.data = xf6.a(fileInputStream);
            cd6 cd6 = cd6.a;
            yf6.a(fileInputStream, (Throwable) null);
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            e.printStackTrace();
            local.e("LocalizationData", String.valueOf(cd6.a));
        }
    }

    @DexIgnore
    public final boolean isDataValid() {
        return this.data != null;
    }

    @DexIgnore
    public final void setCheckSum(String str) {
        wg6.b(str, "<set-?>");
        this.checkSum = str;
    }

    @DexIgnore
    public final void setData(byte[] bArr) {
        this.data = bArr;
    }

    @DexIgnore
    public final kd0 toLocalizationFile() {
        byte[] bArr = this.data;
        if (bArr != null) {
            return new kd0(bArr);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public String toString() {
        String a = new Gson().a(this);
        wg6.a((Object) a, "Gson().toJson(this)");
        return a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeString(LocalizationData.class.getName());
        parcel.writeString(this.filePath);
        parcel.writeString(this.checkSum);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ LocalizationData(String str, String str2, int i, qg6 qg6) {
        this(str, (i & 2) != 0 ? "" : str2);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public LocalizationData(Parcel parcel) {
        this(r0, r3 == null ? "" : r3);
        wg6.b(parcel, "parcel");
        String readString = parcel.readString();
        readString = readString == null ? "" : readString;
        String readString2 = parcel.readString();
        initialize();
    }
}
