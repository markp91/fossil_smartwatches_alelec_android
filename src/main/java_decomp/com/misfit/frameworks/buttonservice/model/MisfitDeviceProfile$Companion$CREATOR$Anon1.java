package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class MisfitDeviceProfile$Companion$CREATOR$Anon1 implements Parcelable.Creator<MisfitDeviceProfile> {
    @DexIgnore
    public MisfitDeviceProfile createFromParcel(Parcel parcel) {
        wg6.b(parcel, "in");
        return new MisfitDeviceProfile(parcel);
    }

    @DexIgnore
    public MisfitDeviceProfile[] newArray(int i) {
        return new MisfitDeviceProfile[i];
    }
}
