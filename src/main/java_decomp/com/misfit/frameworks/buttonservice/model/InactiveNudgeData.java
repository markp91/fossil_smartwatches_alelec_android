package com.misfit.frameworks.buttonservice.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.qg6;
import com.fossil.wg6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class InactiveNudgeData implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((qg6) null);
    @DexIgnore
    public /* final */ String TAG;
    @DexIgnore
    public /* final */ boolean isEnable;
    @DexIgnore
    public /* final */ short repeatInterval;
    @DexIgnore
    public /* final */ byte startHour;
    @DexIgnore
    public /* final */ byte startMinute;
    @DexIgnore
    public /* final */ byte stopHour;
    @DexIgnore
    public /* final */ byte stopMinute;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<InactiveNudgeData> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(qg6 qg6) {
            this();
        }

        @DexIgnore
        public InactiveNudgeData createFromParcel(Parcel parcel) {
            wg6.b(parcel, "parcel");
            return new InactiveNudgeData(parcel);
        }

        @DexIgnore
        public InactiveNudgeData[] newArray(int i) {
            return new InactiveNudgeData[i];
        }
    }

    @DexIgnore
    public InactiveNudgeData(byte b, byte b2, byte b3, byte b4, short s, boolean z) {
        String name = InactiveNudgeData.class.getName();
        wg6.a((Object) name, "InactiveNudgeData::class.java.name");
        this.TAG = name;
        this.startHour = b;
        this.startMinute = b2;
        this.stopHour = b3;
        this.stopMinute = b4;
        this.repeatInterval = s;
        this.isEnable = z;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.repeatInterval;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.startHour;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.startMinute;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.stopHour;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.stopMinute;
    }

    @DexIgnore
    public final boolean isEnable() {
        return this.isEnable;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        wg6.b(parcel, "parcel");
        parcel.writeByte(this.startHour);
        parcel.writeByte(this.startMinute);
        parcel.writeByte(this.stopHour);
        parcel.writeByte(this.stopMinute);
        parcel.writeInt(this.repeatInterval);
        parcel.writeByte(this.isEnable ? (byte) 1 : 0);
    }

    @DexIgnore
    public InactiveNudgeData(Parcel parcel) {
        wg6.b(parcel, "parcel");
        String name = InactiveNudgeData.class.getName();
        wg6.a((Object) name, "InactiveNudgeData::class.java.name");
        this.TAG = name;
        this.startHour = parcel.readByte();
        this.startMinute = parcel.readByte();
        this.stopHour = parcel.readByte();
        this.stopMinute = parcel.readByte();
        this.repeatInterval = (short) parcel.readInt();
        this.isEnable = parcel.readByte() != 0;
    }
}
