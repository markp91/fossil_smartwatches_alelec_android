package com.misfit.frameworks.buttonservice.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ServiceActionId {
    IDLE,
    DEVICE_CONNECT,
    DEVICE_REMOVEBOND,
    DEVICE_SET_CONNECTION_PARAM,
    DEVICE_GET_CONNECTION_PARAM,
    DEVICE_UPDATE_FIRMWARE,
    DEVICE_GET_CONFIG_PARAM,
    DEVICE_SET_CONFIG_PARAM,
    DEVICE_START_STREAMING,
    DEVICE_STOP_STREAMING,
    DEVICE_GET_RSSI,
    DEVICE_GET_BATTERRY,
    DEVICE_SYNCING,
    DISCONNECT_ALL_BUTTON,
    CONNECT_ALL_BUTTON,
    DEVICE_CHANGE_MODE,
    DEVICE_PLAY_ANIMATION,
    DEVICE_ENABLE_CLOCK
}
