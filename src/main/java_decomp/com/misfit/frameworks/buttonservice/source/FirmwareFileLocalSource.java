package com.misfit.frameworks.buttonservice.source;

import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FirmwareFileLocalSource {
    @DexIgnore
    public /* final */ String TAG;

    @DexIgnore
    public FirmwareFileLocalSource() {
        String name = FirmwareFileLocalSource.class.getName();
        wg6.a((Object) name, "FirmwareFileLocalSource::class.java.name");
        this.TAG = name;
    }

    @DexIgnore
    public boolean deleteFile(String str) {
        wg6.b(str, "filePath");
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.d(str2, ".saveFile(), success=" + true + ", filePath=" + str);
            return true;
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".deleteFile(), cannot delete file, ex=" + e);
            return false;
        }
    }

    @DexIgnore
    public File getFile(String str) {
        wg6.b(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".getFile(), filePath=" + str);
        try {
            return new File(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".getFile(), cannot get file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    public FileInputStream readFile(String str) {
        wg6.b(str, "filePath");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.TAG;
        local.d(str2, ".readFile(), filePath=" + str);
        try {
            return new FileInputStream(str);
        } catch (Exception e) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str3 = this.TAG;
            local2.e(str3, ".readFile(), cannot read file, ex=" + e);
            return null;
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:24:0x009a A[SYNTHETIC, Splitter:B:24:0x009a] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x00bc A[SYNTHETIC, Splitter:B:29:0x00bc] */
    /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
    public boolean saveFile(InputStream inputStream, String str) {
        wg6.b(inputStream, "inputStream");
        wg6.b(str, "filePath");
        FileOutputStream fileOutputStream = null;
        try {
            FileOutputStream fileOutputStream2 = new FileOutputStream(str);
            try {
                byte[] bArr = new byte[1024];
                long j = 0;
                int read = inputStream.read(bArr);
                while (read != -1) {
                    j += (long) read;
                    fileOutputStream2.write(bArr, 0, read);
                    read = inputStream.read(bArr);
                }
                fileOutputStream2.flush();
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                local.d(str2, ".saveFile(), success=" + true + ", total size=" + j);
                try {
                    fileOutputStream2.close();
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str3 = this.TAG;
                    local2.e(str3, ".saveFile(), close output stream, ex=" + e);
                }
                return true;
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = fileOutputStream2;
                try {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str4 = this.TAG;
                    local3.e(str4, ".saveFile(), cannot save file, ex=" + e);
                    if (fileOutputStream != null) {
                    }
                } catch (Throwable th) {
                    th = th;
                    fileOutputStream2 = fileOutputStream;
                    if (fileOutputStream2 != null) {
                        try {
                            fileOutputStream2.close();
                        } catch (Exception e3) {
                            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                            String str5 = this.TAG;
                            local4.e(str5, ".saveFile(), close output stream, ex=" + e3);
                        }
                    }
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (fileOutputStream2 != null) {
                }
                throw th;
            }
        } catch (Exception e4) {
            e = e4;
            ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
            String str42 = this.TAG;
            local32.e(str42, ".saveFile(), cannot save file, ex=" + e);
            if (fileOutputStream != null) {
                return false;
            }
            try {
                fileOutputStream.close();
                return false;
            } catch (Exception e5) {
                ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                String str6 = this.TAG;
                local5.e(str6, ".saveFile(), close output stream, ex=" + e5);
                return false;
            }
        }
    }

    @DexIgnore
    public boolean verify(String str, String str2) {
        wg6.b(str, "filePath");
        wg6.b(str2, "checkSum");
        boolean verifyDownloadFile = FileUtils.verifyDownloadFile(str, str2);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = this.TAG;
        local.d(str3, ".verify(), isValid=" + verifyDownloadFile + ", filePath=" + str + ", checkSum=" + str2);
        return verifyDownloadFile;
    }
}
