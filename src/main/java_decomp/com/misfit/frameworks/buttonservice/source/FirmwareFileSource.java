package com.misfit.frameworks.buttonservice.source;

import com.fossil.xe6;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface FirmwareFileSource {
    @DexIgnore
    Object downloadFirmware(String str, String str2, String str3, xe6<? super File> xe6);

    @DexIgnore
    String getFirmwareFilePath(String str);

    @DexIgnore
    boolean isDownloaded(String str, String str2);

    @DexIgnore
    byte[] readFirmware(String str);
}
