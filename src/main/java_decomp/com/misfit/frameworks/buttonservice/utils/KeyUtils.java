package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class KeyUtils {
    @DexIgnore
    public static /* final */ String BACKGROUND_IMAGE_CONFIG_TAG; // = "autoBackgroundImageConfig";
    @DexIgnore
    public static /* final */ String BIOMETRIC_SETTINGS_TAG; // = "autoWatchAppSettings";
    @DexIgnore
    public static /* final */ String BUTTON_PREFERENCE; // = "buttonservice";
    @DexIgnore
    public static /* final */ String COMPLICATION_APP_SETTINGS_TAG; // = "autoComplicationAppSettings";
    @DexIgnore
    public static /* final */ String GOAL_TRACKING_TAG; // = "autoGoalTracking";
    @DexIgnore
    public static /* final */ String KEY_ALARM_SETTING; // = "alarmSetting";
    @DexIgnore
    public static /* final */ String KEY_ALL_ACTIVE_BUTTON; // = "activeButton";
    @DexIgnore
    public static /* final */ String KEY_ALL_PAIRED_BUTTON; // = "pairedButton";
    @DexIgnore
    public static /* final */ String KEY_COUNTDOWN_SETTING; // = "countdownSetting";
    @DexIgnore
    public static /* final */ String KEY_CURRENT_SECOND_TIMEZONE_OFFSET; // = "currentSecondTimezoneOffset";
    @DexIgnore
    public static /* final */ String KEY_DEVICE_PROFILE; // = "deviceProfile";
    @DexIgnore
    public static /* final */ String KEY_DEVICE_SETTING_DATA; // = "keyDeviceSettingData";
    @DexIgnore
    public static /* final */ String KEY_LIST_ALARM; // = "listAlarm";
    @DexIgnore
    public static /* final */ String KEY_LOCALIZATION_DATA; // = "keyLocalizationData";
    @DexIgnore
    public static /* final */ String KEY_SECOND_TIMEZONE; // = "secondTimezone";
    @DexIgnore
    public static /* final */ String MAPPING_TAG; // = "autoSetMapping";
    @DexIgnore
    public static /* final */ String NOTIFICATION_FILTERS_CONFIG_TAG; // = "autoNotificationFilterConfig";
    @DexIgnore
    public static /* final */ String STREAMING_TAG; // = "autoStreaming";
    @DexIgnore
    public static /* final */ String WATCH_APP_SETTINGS_TAG; // = "autoWatchAppSettings";

    @DexIgnore
    public static String getButtonPreferenceKey(Context context) {
        return context.getPackageName() + BUTTON_PREFERENCE;
    }

    @DexIgnore
    public static String getKeyAlarmSetting(Context context) {
        return context.getPackageName() + KEY_ALARM_SETTING;
    }

    @DexIgnore
    public static String getKeyAllActiveButton(Context context) {
        return context.getPackageName() + KEY_ALL_ACTIVE_BUTTON;
    }

    @DexIgnore
    public static String getKeyAllPairedButton(Context context) {
        return context.getPackageName() + KEY_ALL_PAIRED_BUTTON;
    }

    @DexIgnore
    public static String getKeyAutoCountdownSetting(Context context) {
        return context.getPackageName() + KEY_COUNTDOWN_SETTING;
    }

    @DexIgnore
    public static String getKeyAutoLocalizationDataSettings(Context context) {
        return context.getPackageName() + KEY_LOCALIZATION_DATA;
    }

    @DexIgnore
    public static String getKeyAutoSetBackgroundImageConfig(Context context) {
        return context.getPackageName() + BACKGROUND_IMAGE_CONFIG_TAG;
    }

    @DexIgnore
    public static String getKeyAutoSetBiometricSettings(Context context) {
        return context.getPackageName() + "autoWatchAppSettings";
    }

    @DexIgnore
    public static String getKeyAutoSetComplicationAppSettings(Context context) {
        return context.getPackageName() + COMPLICATION_APP_SETTINGS_TAG;
    }

    @DexIgnore
    public static String getKeyAutoSetMapping(Context context) {
        return context.getPackageName() + MAPPING_TAG;
    }

    @DexIgnore
    public static String getKeyAutoSetNotificationFiltersConfig(Context context) {
        return context.getPackageName() + NOTIFICATION_FILTERS_CONFIG_TAG;
    }

    @DexIgnore
    public static String getKeyAutoSetWatchAppSettings(Context context) {
        return context.getPackageName() + "autoWatchAppSettings";
    }

    @DexIgnore
    public static String getKeyCurrentSecondTimezoneOffset(Context context) {
        return context.getPackageName() + KEY_CURRENT_SECOND_TIMEZONE_OFFSET;
    }

    @DexIgnore
    public static String getKeyDeviceProfile(Context context) {
        return context.getPackageName() + KEY_DEVICE_PROFILE;
    }

    @DexIgnore
    public static String getKeyDeviceSettingData(Context context) {
        return context.getPackageName() + KEY_DEVICE_SETTING_DATA;
    }

    @DexIgnore
    public static String getKeyListAlarm(Context context) {
        return context.getPackageName() + KEY_LIST_ALARM;
    }

    @DexIgnore
    public static String getKeySecondTimezone(Context context) {
        return context.getPackageName() + KEY_SECOND_TIMEZONE;
    }
}
