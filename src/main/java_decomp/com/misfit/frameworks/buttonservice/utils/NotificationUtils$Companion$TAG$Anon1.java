package com.misfit.frameworks.buttonservice.utils;

import com.fossil.ch6;
import com.fossil.eg6;
import com.fossil.hi6;
import com.fossil.kh6;
import com.fossil.ni6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class NotificationUtils$Companion$TAG$Anon1 extends ch6 {
    @DexIgnore
    public static /* final */ ni6 INSTANCE; // = new NotificationUtils$Companion$TAG$Anon1();

    @DexIgnore
    public Object get(Object obj) {
        return eg6.a((NotificationUtils) obj);
    }

    @DexIgnore
    public String getName() {
        return "javaClass";
    }

    @DexIgnore
    public hi6 getOwner() {
        return kh6.a(eg6.class, "buttonservice_release");
    }

    @DexIgnore
    public String getSignature() {
        return "getJavaClass(Ljava/lang/Object;)Ljava/lang/Class;";
    }
}
