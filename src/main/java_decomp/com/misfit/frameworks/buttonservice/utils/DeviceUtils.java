package com.misfit.frameworks.buttonservice.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.misfit.frameworks.common.enums.Action;
import com.misfit.frameworks.common.enums.ButtonType;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DeviceUtils {
    @DexIgnore
    public static /* final */ String PREFERENCE_NAME; // = "com.misfit.frameworks.buttonservice.cacheddevices";
    @DexIgnore
    public static DeviceUtils sInstance;
    @DexIgnore
    public Context context;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class Anon1 {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] $SwitchMap$com$misfit$frameworks$common$enums$ButtonType; // = new int[ButtonType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|(3:21|22|24)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|24) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.SELFIE.ordinal()] = 1;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.MUSIC.ordinal()] = 2;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.PRESENTATION.ordinal()] = 3;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.ACTIVITY.ordinal()] = 4;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.BOLT_CONTROL.ordinal()] = 5;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.CUSTOM.ordinal()] = 6;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.PLUTO_TRACKER.ordinal()] = 7;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.SILVRETTA_TRACKER.ordinal()] = 8;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.BMW_TRACKER.ordinal()] = 9;
            $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.SWAROVSKI_TRACKER.ordinal()] = 10;
            try {
                $SwitchMap$com$misfit$frameworks$common$enums$ButtonType[ButtonType.ThirdPartyApp.ordinal()] = 11;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore
    public DeviceUtils(Context context2) {
        this.context = context2.getApplicationContext();
    }

    @DexIgnore
    public static synchronized DeviceUtils getInstance(Context context2) {
        DeviceUtils deviceUtils;
        synchronized (DeviceUtils.class) {
            if (sInstance == null) {
                sInstance = new DeviceUtils(context2);
            }
            deviceUtils = sInstance;
        }
        return deviceUtils;
    }

    @DexIgnore
    public ButtonType bleCommandToButtonType(int i) {
        if (i == 1) {
            return ButtonType.SELFIE;
        }
        if (i == 2) {
            return ButtonType.MUSIC;
        }
        if (i == 3) {
            return ButtonType.PRESENTATION;
        }
        if (i == 5) {
            return ButtonType.ACTIVITY;
        }
        if (i == 6) {
            return ButtonType.BOLT_CONTROL;
        }
        if (i == 7) {
            return ButtonType.CUSTOM;
        }
        if (i != 50) {
            return ButtonType.NONE;
        }
        return ButtonType.PLUTO_TRACKER;
    }

    @DexIgnore
    public int buttonTypeToBleCommand(ButtonType buttonType) {
        switch (Anon1.$SwitchMap$com$misfit$frameworks$common$enums$ButtonType[buttonType.ordinal()]) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 3;
            case 4:
                return 5;
            case 5:
                return 6;
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
                return 7;
            default:
                return 0;
        }
    }

    @DexIgnore
    public void clearMacAddress(Context context2, String str) {
        setString(context2, str, "");
    }

    @DexIgnore
    public ButtonType getButtonTypeByAction(int i) {
        if (Action.Music.isActionBelongToThisType(i)) {
            return ButtonType.MUSIC;
        }
        if (Action.Selfie.isActionBelongToThisType(i)) {
            return ButtonType.SELFIE;
        }
        if (Action.Presenter.isActionBelongToThisType(i)) {
            return ButtonType.PRESENTATION;
        }
        if (Action.ActivityTracker.isActionBelongToThisType(i)) {
            return ButtonType.ACTIVITY;
        }
        if (Action.DisplayMode.isActionBelongToThisType(i)) {
            return ButtonType.DISPLAY_MODE;
        }
        if (i == 505) {
            return ButtonType.RING_MY_PHONE;
        }
        if (i == 1000) {
            return ButtonType.GOAL_TRACKING;
        }
        if (i <= 600 || i >= 699) {
            return ButtonType.NONE;
        }
        return ButtonType.BOLT_CONTROL;
    }

    @DexIgnore
    public String getMacAddress(Context context2, String str) {
        return getString(context2, str);
    }

    @DexIgnore
    public SharedPreferences getPreferences(Context context2) {
        return context2.getSharedPreferences(PREFERENCE_NAME, 0);
    }

    @DexIgnore
    public String getSerial(Context context2, String str) {
        return getString(context2, str);
    }

    @DexIgnore
    public String getString(Context context2, String str) {
        SharedPreferences preferences = getPreferences(context2);
        if (preferences != null) {
            return preferences.getString(str, "");
        }
        return "";
    }

    @DexIgnore
    public void saveMacAddress(Context context2, String str, String str2) {
        setString(context2, str, str2);
        setString(context2, str2, str);
    }

    @DexIgnore
    public void setString(Context context2, String str, String str2) {
        SharedPreferences preferences = getPreferences(context2);
        if (preferences != null) {
            SharedPreferences.Editor edit = preferences.edit();
            edit.putString(str, str2);
            edit.apply();
        }
    }
}
