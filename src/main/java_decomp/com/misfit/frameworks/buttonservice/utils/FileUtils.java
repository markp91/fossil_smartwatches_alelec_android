package com.misfit.frameworks.buttonservice.utils;

import com.fossil.bv6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import java.io.FileInputStream;
import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FileUtils {
    @DexIgnore
    public static /* final */ String TAG; // = "FileUtils";

    @DexIgnore
    public static String bytesToString(byte[] bArr) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            sb.append(Integer.toString((b & 255) + 256, 16).substring(1));
        }
        return sb.toString().toLowerCase();
    }

    @DexIgnore
    public static boolean verifyDownloadFile(String str, String str2) {
        if (bv6.b(str2)) {
            return false;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            FileInputStream fileInputStream = new FileInputStream(str);
            byte[] bArr = new byte[2014];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1) {
                    return str2.toLowerCase().equals(bytesToString(instance.digest()));
                } else if (read > 0) {
                    instance.update(bArr, 0, read);
                }
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = TAG;
            local.e(str3, "Error inside " + TAG + ".verifyDownloadFile - e=" + e);
            return false;
        }
    }
}
