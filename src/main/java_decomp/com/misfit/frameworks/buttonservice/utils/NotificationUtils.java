package com.misfit.frameworks.buttonservice.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.os.Build;
import com.fossil.ch6;
import com.fossil.dh6;
import com.fossil.ic6;
import com.fossil.jc6;
import com.fossil.kh6;
import com.fossil.li6;
import com.fossil.p6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.wg6;
import com.misfit.frameworks.buttonservice.R;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.Explore;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationUtils {
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE; // = "com.fossil.wearables.fossil.channel.defaultService";
    @DexIgnore
    public static /* final */ String DEFAULT_CHANNEL_WATCH_SERVICE_NAME; // = "Sync service";
    @DexIgnore
    public static /* final */ int DEVICE_STATUS_NOTIFICATION_ID; // = 1;
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public static /* final */ ic6 instance$delegate; // = jc6.a(NotificationUtils$Companion$instance$Anon2.INSTANCE);
    @DexIgnore
    public static p6.d mNotificationBuilder;
    @DexIgnore
    public String mLastContent;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public static /* final */ /* synthetic */ li6[] $$delegatedProperties;

        /*
        static {
            dh6 dh6 = new dh6(kh6.a(Companion.class), "instance", "getInstance()Lcom/misfit/frameworks/buttonservice/utils/NotificationUtils;");
            kh6.a((ch6) dh6);
            $$delegatedProperties = new li6[]{dh6};
        }
        */

        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public static /* synthetic */ void instance$annotations() {
        }

        @DexIgnore
        public final NotificationUtils getInstance() {
            ic6 access$getInstance$cp = NotificationUtils.instance$delegate;
            Companion companion = NotificationUtils.Companion;
            li6 li6 = $$delegatedProperties[0];
            return (NotificationUtils) access$getInstance$cp.getValue();
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Singleton {
        @DexIgnore
        public static /* final */ Singleton INSTANCE; // = new Singleton();

        @DexIgnore
        /* renamed from: INSTANCE  reason: collision with other field name */
        public static /* final */ NotificationUtils f0INSTANCE; // = new NotificationUtils((qg6) null);

        @DexIgnore
        public final NotificationUtils getINSTANCE() {
            return f0INSTANCE;
        }
    }

    /*
    static {
        String simpleName = NotificationUtils$Companion$TAG$Anon1.INSTANCE.getClass().getSimpleName();
        wg6.a((Object) simpleName, "NotificationUtils::javaClass.javaClass.simpleName");
        TAG = simpleName;
    }
    */

    @DexIgnore
    public NotificationUtils() {
        this.mLastContent = "";
    }

    @DexIgnore
    private final Notification createDefaultDeviceStatusNotification(Context context, String str, String str2) {
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        p6.d dVar = new p6.d(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        ArrayList arrayList = dVar.b;
        if (arrayList != null) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            dVar.b(context.getResources().getString(R.string.app_name));
            dVar.a(str2);
            dVar.e(R.drawable.ic_launcher_transparent);
            dVar.a(Constants.SERVICE);
            dVar.c(false);
            dVar.d(false);
            dVar.a((p6.e) null);
            dVar.d(-2);
            Notification a = dVar.a();
            wg6.a((Object) a, "builder\n                \u2026                 .build()");
            return a;
        }
        dVar.e(R.drawable.ic_launcher_transparent);
        dVar.a(Constants.SERVICE);
        dVar.b(str2);
        dVar.a((p6.e) null);
        dVar.c(false);
        dVar.d(false);
        dVar.d(-2);
        Notification a2 = dVar.a();
        wg6.a((Object) a2, "builder\n                \u2026                 .build()");
        return a2;
    }

    @DexIgnore
    private final Notification createDeviceStatusNotification(Context context, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        ArrayList arrayList;
        if (!(str.length() == 0)) {
            this.mLastContent = str;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new p6.d(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        p6.d dVar = mNotificationBuilder;
        if (!(dVar == null || (arrayList = dVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            p6.d dVar2 = mNotificationBuilder;
            if (dVar2 != null) {
                dVar2.b(context.getResources().getString(R.string.app_name));
                dVar2.a(Constants.SERVICE);
                dVar2.c(false);
                dVar2.a(str);
                dVar2.a((p6.e) null);
                dVar2.e(R.drawable.ic_launcher_transparent);
                dVar2.d(false);
                dVar2.d(-2);
                Notification a = dVar2.a();
                wg6.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            wg6.a();
            throw null;
        }
        p6.d dVar3 = mNotificationBuilder;
        if (dVar3 != null) {
            dVar3.e(R.drawable.ic_launcher_transparent);
            dVar3.a(Constants.SERVICE);
            dVar3.b(str);
            dVar3.a((p6.e) null);
            dVar3.c(false);
            dVar3.d(false);
            dVar3.d(-2);
            if (pendingIntent2 != null) {
                dVar3.a(R.drawable.ic_refresh, context.getString(R.string.sync), pendingIntent2);
            }
            if (pendingIntent != null) {
                dVar3.a(R.drawable.ic_refresh, context.getString(R.string.home), pendingIntent);
            }
            Notification a2 = dVar3.a();
            wg6.a((Object) a2, "builder.build()");
            return a2;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Notification createDeviceStatusNotification$default(NotificationUtils notificationUtils, Context context, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i, Object obj) {
        if ((i & 4) != 0) {
            pendingIntent = null;
        }
        if ((i & 8) != 0) {
            pendingIntent2 = null;
        }
        return notificationUtils.createDeviceStatusNotification(context, str, pendingIntent, pendingIntent2);
    }

    @DexIgnore
    private final Notification createDeviceStatusWithRichTextNotification(Context context, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2) {
        ArrayList arrayList;
        if (!(str2.length() == 0)) {
            this.mLastContent = str2;
        }
        if (mNotificationBuilder == null) {
            mNotificationBuilder = new p6.d(context, DEFAULT_CHANNEL_WATCH_SERVICE);
        }
        p6.d dVar = mNotificationBuilder;
        if (!(dVar == null || (arrayList = dVar.b) == null)) {
            arrayList.clear();
        }
        if (Build.VERSION.SDK_INT < 24) {
            p6.d dVar2 = mNotificationBuilder;
            if (dVar2 != null) {
                dVar2.b(context.getResources().getString(R.string.app_name));
                dVar2.a(Constants.SERVICE);
                p6.c cVar = new p6.c();
                cVar.a(str2);
                dVar2.a(cVar);
                dVar2.c(false);
                dVar2.a(str);
                dVar2.e(R.drawable.ic_launcher_transparent);
                dVar2.d(false);
                dVar2.d(-2);
                Notification a = dVar2.a();
                wg6.a((Object) a, "mNotificationBuilder!!\n \u2026                 .build()");
                return a;
            }
            wg6.a();
            throw null;
        }
        p6.d dVar3 = mNotificationBuilder;
        if (dVar3 != null) {
            dVar3.e(R.drawable.ic_launcher_transparent);
            dVar3.a(Constants.SERVICE);
            p6.c cVar2 = new p6.c();
            cVar2.a(str2);
            dVar3.a(cVar2);
            dVar3.b(str);
            dVar3.c(false);
            dVar3.d(false);
            dVar3.d(-2);
            if (pendingIntent2 != null) {
                dVar3.a(R.drawable.ic_refresh, context.getString(R.string.sync), pendingIntent2);
            }
            if (pendingIntent != null) {
                dVar3.a(R.drawable.ic_refresh, context.getString(R.string.home), pendingIntent);
            }
            Notification a2 = dVar3.a();
            wg6.a((Object) a2, "builder.build()");
            return a2;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public static /* synthetic */ Notification createDeviceStatusWithRichTextNotification$default(NotificationUtils notificationUtils, Context context, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, int i, Object obj) {
        return notificationUtils.createDeviceStatusWithRichTextNotification(context, str, str2, (i & 8) != 0 ? null : pendingIntent, (i & 16) != 0 ? null : pendingIntent2);
    }

    @DexIgnore
    private final NotificationManager createNotificationChannels(Context context) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = TAG;
        local.d(str, "createNotificationChannels - context=" + context);
        Object systemService = context.getSystemService("notification");
        if (systemService != null) {
            NotificationManager notificationManager = (NotificationManager) systemService;
            if (Build.VERSION.SDK_INT >= 26) {
                NotificationChannel notificationChannel = notificationManager.getNotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE);
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = TAG;
                local2.d(str2, "createNotificationChannels - currentChannel=" + notificationChannel);
                if (notificationChannel == null) {
                    NotificationChannel notificationChannel2 = new NotificationChannel(DEFAULT_CHANNEL_WATCH_SERVICE, DEFAULT_CHANNEL_WATCH_SERVICE_NAME, 1);
                    notificationChannel2.setShowBadge(false);
                    notificationManager.createNotificationChannel(notificationChannel2);
                }
            }
            return notificationManager;
        }
        throw new rc6("null cannot be cast to non-null type android.app.NotificationManager");
    }

    @DexIgnore
    public static final NotificationUtils getInstance() {
        return Companion.getInstance();
    }

    @DexIgnore
    private final void riseNotification(NotificationManager notificationManager, int i, Notification notification) {
        try {
            notificationManager.notify(i, notification);
        } catch (Exception e) {
            FLogger.INSTANCE.getLocal().d(TAG, e.getMessage());
            e.printStackTrace();
        }
    }

    @DexIgnore
    public static /* synthetic */ void updateNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        notificationUtils.updateNotification(context, i, str, (i2 & 8) != 0 ? null : pendingIntent, (i2 & 16) != 0 ? null : pendingIntent2, z);
    }

    @DexIgnore
    public static /* synthetic */ void updateRichTextNotification$default(NotificationUtils notificationUtils, Context context, int i, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z, int i2, Object obj) {
        notificationUtils.updateRichTextNotification(context, i, str, str2, (i2 & 16) != 0 ? null : pendingIntent, (i2 & 32) != 0 ? null : pendingIntent2, z);
    }

    @DexIgnore
    public final void startForegroundNotification(Context context, Service service, String str, String str2, boolean z) {
        wg6.b(context, "context");
        wg6.b(service, Constants.SERVICE);
        wg6.b(str, "content");
        wg6.b(str2, "subText");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "startForegroundNotification - context=" + context + ',' + " service=" + service + ", content=" + str + ", subText=" + str2 + ", isStopForeground = " + z);
        createNotificationChannels(context);
        service.startForeground(1, createDefaultDeviceStatusNotification(context, str, str2));
        if (z) {
            FLogger.INSTANCE.getLocal().d(TAG, "startForegroundNotification() - stop foreground service and remove notification");
            service.stopForeground(true);
        }
    }

    @DexIgnore
    public final void updateNotification(Context context, int i, String str, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        wg6.b(context, "context");
        wg6.b(str, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = TAG;
        local.d(str2, "updateNotification - context=" + context + ", content=" + str + " notificationId=" + i + ", isReset=" + z);
        if (!(str.length() == 0) || z) {
            this.mLastContent = str;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusNotification(context, str, pendingIntent, pendingIntent2));
    }

    @DexIgnore
    public final void updateRichTextNotification(Context context, int i, String str, String str2, PendingIntent pendingIntent, PendingIntent pendingIntent2, boolean z) {
        wg6.b(context, "context");
        wg6.b(str, Explore.COLUMN_TITLE);
        wg6.b(str2, "content");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = TAG;
        local.d(str3, "updateNotification - context=" + context + ", title=" + str + ", content=" + str2 + ", notificationId=" + i + ", isReset=" + z);
        if ((str2.length() > 0) || z) {
            this.mLastContent = str2;
        }
        riseNotification(createNotificationChannels(context), i, createDeviceStatusWithRichTextNotification(context, str, str2, pendingIntent, pendingIntent2));
    }

    @DexIgnore
    public /* synthetic */ NotificationUtils(qg6 qg6) {
        this();
    }
}
