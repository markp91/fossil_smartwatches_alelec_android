package com.misfit.frameworks.buttonservice.log;

import com.fossil.hg6;
import com.fossil.wg6;
import com.fossil.xg6;
import com.misfit.frameworks.buttonservice.log.db.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class DBLogWriter$writeLog$Anon1 extends xg6 implements hg6<LogEvent, Log> {
    @DexIgnore
    public static /* final */ DBLogWriter$writeLog$Anon1 INSTANCE; // = new DBLogWriter$writeLog$Anon1();

    @DexIgnore
    public DBLogWriter$writeLog$Anon1() {
        super(1);
    }

    @DexIgnore
    public final Log invoke(LogEvent logEvent) {
        wg6.b(logEvent, "it");
        return new Log(logEvent.getTimestamp(), logEvent.toString(), Log.Flag.ADD);
    }
}
