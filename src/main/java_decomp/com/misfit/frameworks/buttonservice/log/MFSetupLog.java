package com.misfit.frameworks.buttonservice.log;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFSetupLog extends MFLog {
    @DexIgnore
    public MFSetupLog(Context context) {
        super(context);
    }

    @DexIgnore
    public int getLogType() {
        return 2;
    }
}
