package com.misfit.frameworks.buttonservice.log;

import com.fossil.wg6;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ServerErrorException extends IOException {
    @DexIgnore
    public /* final */ ServerError mServerError;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public ServerErrorException(ServerError serverError) {
        super(r0 == null ? "" : r0);
        wg6.b(serverError, "mServerError");
        String userMessage = serverError.getUserMessage();
        userMessage = userMessage == null ? serverError.getMessage() : userMessage;
        this.mServerError = serverError;
    }

    @DexIgnore
    public final ServerError getServerError() {
        return this.mServerError;
    }
}
