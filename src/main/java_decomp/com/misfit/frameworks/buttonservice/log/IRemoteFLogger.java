package com.misfit.frameworks.buttonservice.log;

import android.content.Context;
import com.misfit.frameworks.buttonservice.communite.ble.sessionabs.ErrorCodeBuilder;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.model.ActiveDeviceInfo;
import com.misfit.frameworks.buttonservice.log.model.AppLogInfo;
import com.misfit.frameworks.buttonservice.log.model.CloudLogConfig;
import com.misfit.frameworks.buttonservice.log.model.SessionDetailInfo;
import java.io.File;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface IRemoteFLogger {
    @DexIgnore
    void d(FLogger.Component component, FLogger.Session session, String str, String str2, String str3);

    @DexIgnore
    void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3);

    @DexIgnore
    void e(FLogger.Component component, FLogger.Session session, String str, String str2, String str3, ErrorCodeBuilder.Step step, String str4);

    @DexIgnore
    List<File> exportAppLogs();

    @DexIgnore
    void flush();

    @DexIgnore
    void i(FLogger.Component component, FLogger.Session session, String str, String str2, String str3);

    @DexIgnore
    void init(String str, AppLogInfo appLogInfo, ActiveDeviceInfo activeDeviceInfo, CloudLogConfig cloudLogConfig, Context context, boolean z, boolean z2);

    @DexIgnore
    void setPrintToConsole(boolean z);

    @DexIgnore
    void startSession(FLogger.Session session, String str, String str2);

    @DexIgnore
    void summary(int i, FLogger.Component component, FLogger.Session session, String str, String str2);

    @DexIgnore
    void updateActiveDeviceInfo(ActiveDeviceInfo activeDeviceInfo);

    @DexIgnore
    void updateAppLogInfo(AppLogInfo appLogInfo);

    @DexIgnore
    void updateCloudLogConfig(CloudLogConfig cloudLogConfig);

    @DexIgnore
    void updateSessionDetailInfo(SessionDetailInfo sessionDetailInfo);
}
