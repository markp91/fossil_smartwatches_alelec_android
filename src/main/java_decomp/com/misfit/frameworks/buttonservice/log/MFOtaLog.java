package com.misfit.frameworks.buttonservice.log;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class MFOtaLog extends MFLog {
    @DexIgnore
    public int batteryLevel;
    @DexIgnore
    public String newFirmware;
    @DexIgnore
    public String oldFirmware;
    @DexIgnore
    public float progress;
    @DexIgnore
    public int retries;

    @DexIgnore
    public MFOtaLog(Context context) {
        super(context);
    }

    @DexIgnore
    public int getBatteryLevel() {
        return this.batteryLevel;
    }

    @DexIgnore
    public int getLogType() {
        return 3;
    }

    @DexIgnore
    public String getNewFirmware() {
        return this.newFirmware;
    }

    @DexIgnore
    public String getOldFirmware() {
        return this.oldFirmware;
    }

    @DexIgnore
    public float getProgress() {
        return this.progress;
    }

    @DexIgnore
    public int getRetries() {
        return this.retries;
    }

    @DexIgnore
    public void setBatteryLevel(int i) {
        this.batteryLevel = i;
    }

    @DexIgnore
    public void setNewFirmware(String str) {
        this.newFirmware = str;
    }

    @DexIgnore
    public void setOldFirmware(String str) {
        this.oldFirmware = str;
    }

    @DexIgnore
    public void setProgress(float f) {
        this.progress = f;
    }

    @DexIgnore
    public void setRetries(int i) {
        this.retries = i;
    }
}
