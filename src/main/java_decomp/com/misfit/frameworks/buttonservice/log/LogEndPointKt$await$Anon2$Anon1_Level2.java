package com.misfit.frameworks.buttonservice.log;

import com.fossil.dx6;
import com.fossil.mc6;
import com.fossil.rx6;
import com.fossil.wg6;
import com.fossil.xe6;
import retrofit2.Call;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class LogEndPointKt$await$Anon2$Anon1_Level2 implements dx6<T> {
    @DexIgnore
    public /* final */ /* synthetic */ xe6 $continuation;

    @DexIgnore
    public LogEndPointKt$await$Anon2$Anon1_Level2(xe6 xe6) {
        this.$continuation = xe6;
    }

    @DexIgnore
    public void onFailure(Call<T> call, Throwable th) {
        wg6.b(th, "t");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onFailure=" + th);
        xe6 xe6 = this.$continuation;
        Failure create = RepoResponse.Companion.create(th);
        mc6.a aVar = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(create));
    }

    @DexIgnore
    public void onResponse(Call<T> call, rx6<T> rx6) {
        wg6.b(rx6, "response");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("await", "onResponse=" + rx6);
        xe6 xe6 = this.$continuation;
        RepoResponse create = RepoResponse.Companion.create(rx6);
        mc6.a aVar = mc6.Companion;
        xe6.resumeWith(mc6.m1constructorimpl(create));
    }
}
