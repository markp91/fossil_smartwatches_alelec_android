package com.misfit.frameworks.buttonservice.log;

import android.util.Log;
import com.fossil.af6;
import com.fossil.aj6;
import com.fossil.cd6;
import com.fossil.hf6;
import com.fossil.ik6;
import com.fossil.il6;
import com.fossil.jl6;
import com.fossil.ll6;
import com.fossil.md6;
import com.fossil.mn6;
import com.fossil.nh6;
import com.fossil.qg6;
import com.fossil.rm6;
import com.fossil.ro3;
import com.fossil.vd6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xp6;
import com.fossil.yd6;
import com.fossil.zl6;
import com.fossil.zp6;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.extensions.SynchronizeQueue;
import com.misfit.frameworks.common.constants.Constants;
import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class BufferLogWriter {
    @DexIgnore
    public static /* final */ String ALL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log\\w*\\.txt";
    @DexIgnore
    public static /* final */ String BUFFER_FILE_NAME; // = "%s_buffer_log.txt";
    @DexIgnore
    public static /* final */ Companion Companion; // = new Companion((qg6) null);
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_PATTERN; // = "%s_buffer_log_%s.txt";
    @DexIgnore
    public static /* final */ String FULL_BUFFER_FILE_NAME_REGEX_PATTERN; // = "\\w*buffer_log_\\d+\\.txt";
    @DexIgnore
    public static /* final */ String LOG_FOLDER; // = "buffer_logs";
    @DexIgnore
    public static /* final */ String TAG;
    @DexIgnore
    public IBufferLogCallback callback;
    @DexIgnore
    public BufferDebugOption debugOption;
    @DexIgnore
    public String directoryPath; // = "";
    @DexIgnore
    public /* final */ String floggerName;
    @DexIgnore
    public boolean isMainFLogger;
    @DexIgnore
    public /* final */ SynchronizeQueue<LogEvent> logEventQueue; // = new SynchronizeQueue<>();
    @DexIgnore
    public String logFilePath; // = "";
    @DexIgnore
    public /* final */ xp6 mBufferLogMutex; // = zp6.a(false, 1, (Object) null);
    @DexIgnore
    public /* final */ il6 mBufferLogScope; // = jl6.a(zl6.b().plus(mn6.a((rm6) null, 1, (Object) null)));
    @DexIgnore
    public /* final */ int threshold;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class Companion {
        @DexIgnore
        public Companion() {
        }

        @DexIgnore
        public /* synthetic */ Companion(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface IBufferLogCallback {
        @DexIgnore
        void onFullBuffer(List<LogEvent> list, boolean z);

        @DexIgnore
        void onWrittenSummaryLog(LogEvent logEvent);
    }

    /*
    static {
        String name = BufferLogWriter.class.getName();
        wg6.a((Object) name, "BufferLogWriter::class.java.name");
        TAG = name;
    }
    */

    @DexIgnore
    public BufferLogWriter(String str, int i) {
        wg6.b(str, "floggerName");
        this.floggerName = str;
        this.threshold = i;
    }

    @DexIgnore
    private final String getFullBufferLogFileName(long j) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.directoryPath);
        sb.append(File.separatorChar);
        nh6 nh6 = nh6.a;
        Object[] objArr = {this.floggerName, Long.valueOf(j), Locale.US};
        String format = String.format(FULL_BUFFER_FILE_NAME_PATTERN, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    @DexIgnore
    private final rm6 pollEventQueue() {
        return ik6.b(this.mBufferLogScope, (af6) null, (ll6) null, new BufferLogWriter$pollEventQueue$Anon1(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    private final List<LogEvent> toLogEvents(List<String> list) {
        return aj6.g(aj6.d(yd6.b(list), new BufferLogWriter$toLogEvents$Anon1(new Gson())));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x000f, code lost:
        r0 = com.fossil.nd6.h(r0);
     */
    @DexIgnore
    public final List<File> exportLogs() {
        List<File> h;
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$exportLogs$Anon1.INSTANCE);
        return (listFiles == null || h == null) ? new ArrayList() : h;
    }

    @DexIgnore
    public final void forceFlushBuffer() {
        writeLog(new FlushLogEvent());
    }

    @DexIgnore
    public final IBufferLogCallback getCallback() {
        return this.callback;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0088, code lost:
        if (r3 != null) goto L_0x0090;
     */
    @DexIgnore
    public final /* synthetic */ Object renameToFullBufferFile(File file, boolean z, xe6<? super cd6> xe6) {
        Boolean bool;
        List list;
        Iterable iterable;
        Calendar instance = Calendar.getInstance();
        wg6.a((Object) instance, "Calendar.getInstance()");
        file.renameTo(new File(getFullBufferLogFileName(instance.getTimeInMillis())));
        File[] listFiles = new File(this.directoryPath).listFiles(BufferLogWriter$renameToFullBufferFile$founds$Anon1.INSTANCE);
        int i = 0;
        if (this.isMainFLogger) {
            if (listFiles != null && listFiles.length > 1) {
                md6.a((T[]) listFiles, new BufferLogWriter$renameToFullBufferFile$$inlined$sortBy$Anon1());
            }
            if (listFiles != null) {
                ArrayList arrayList = new ArrayList();
                for (File file2 : listFiles) {
                    try {
                        iterable = ro3.b(file2, Charset.defaultCharset());
                    } catch (Exception e) {
                        String str = TAG;
                        StringBuilder sb = new StringBuilder();
                        sb.append(".renameToFullBufferFile(), read buffer file, name=");
                        wg6.a((Object) file2, "it");
                        sb.append(file2.getName());
                        sb.append(", ex=");
                        sb.append(e);
                        Log.e(str, sb.toString());
                        iterable = new ArrayList();
                    }
                    vd6.a(arrayList, iterable);
                }
                list = yd6.d(arrayList);
            }
            list = new ArrayList();
            if (listFiles != null) {
                int length = listFiles.length;
                while (i < length) {
                    File file3 = listFiles[i];
                    try {
                        file3.delete();
                    } catch (Exception e2) {
                        String str2 = TAG;
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(".renameToFullBufferFile(), delete buffer file, name=");
                        wg6.a((Object) file3, "it");
                        sb2.append(file3.getName());
                        sb2.append(", ex=");
                        sb2.append(e2);
                        Log.e(str2, sb2.toString());
                    }
                    i++;
                }
            }
            FLogger.INSTANCE.getLocal().d(TAG, "onFullBuffer delegate callback");
            IBufferLogCallback iBufferLogCallback = this.callback;
            if (iBufferLogCallback != null) {
                iBufferLogCallback.onFullBuffer(toLogEvents(list), z);
            }
        } else {
            if (listFiles != null) {
                if (listFiles.length == 0) {
                    i = 1;
                }
                bool = hf6.a(i ^ true);
            } else {
                bool = null;
            }
            if (bool == null) {
                wg6.a();
                throw null;
            } else if (bool.booleanValue()) {
                FLogger.INSTANCE.getLocal().d(TAG, "onFullBuffer delegate callback");
                IBufferLogCallback iBufferLogCallback2 = this.callback;
                if (iBufferLogCallback2 != null) {
                    iBufferLogCallback2.onFullBuffer(new ArrayList(), z);
                }
            }
        }
        return cd6.a;
    }

    @DexIgnore
    public final void setCallback(IBufferLogCallback iBufferLogCallback) {
        this.callback = iBufferLogCallback;
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback) {
        String str2;
        wg6.b(str, "directoryPath");
        wg6.b(iBufferLogCallback, Constants.CALLBACK);
        this.callback = iBufferLogCallback;
        this.directoryPath = str + File.separatorChar + LOG_FOLDER;
        if (!xj6.a(this.directoryPath)) {
            StringBuilder sb = new StringBuilder();
            sb.append(this.directoryPath);
            sb.append(File.separatorChar);
            nh6 nh6 = nh6.a;
            Object[] objArr = {this.floggerName, Locale.US};
            String format = String.format(BUFFER_FILE_NAME, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            sb.append(format);
            str2 = sb.toString();
        } else {
            str2 = "";
        }
        this.logFilePath = str2;
        this.isMainFLogger = z;
        pollEventQueue();
    }

    @DexIgnore
    public final void writeLog(LogEvent logEvent) {
        wg6.b(logEvent, "logEvent");
        this.logEventQueue.add(logEvent);
        pollEventQueue();
    }

    @DexIgnore
    public final void startWriter(String str, boolean z, IBufferLogCallback iBufferLogCallback, BufferDebugOption bufferDebugOption) {
        wg6.b(str, "directoryPath");
        wg6.b(iBufferLogCallback, Constants.CALLBACK);
        this.debugOption = bufferDebugOption;
        startWriter(str, z, iBufferLogCallback);
    }
}
