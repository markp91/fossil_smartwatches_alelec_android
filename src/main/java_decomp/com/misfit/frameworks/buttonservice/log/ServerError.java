package com.misfit.frameworks.buttonservice.log;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.vu3;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ServerError implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ServerError> CREATOR; // = new Anon1();
    @DexIgnore
    @vu3("_error")
    public int code;
    @DexIgnore
    @vu3("_errorDetails")
    public String errorDetails;
    @DexIgnore
    @vu3("_errorMessage")
    public String message;
    @DexIgnore
    @vu3("_userMessage")
    public String userMessage;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<ServerError> {
        @DexIgnore
        public ServerError createFromParcel(Parcel parcel) {
            return new ServerError(parcel);
        }

        @DexIgnore
        public ServerError[] newArray(int i) {
            return new ServerError[i];
        }
    }

    @DexIgnore
    public ServerError() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public Integer getCode() {
        return Integer.valueOf(this.code);
    }

    @DexIgnore
    public String getMessage() {
        return this.message;
    }

    @DexIgnore
    public String getUserMessage() {
        return this.userMessage;
    }

    @DexIgnore
    public void setCode(Integer num) {
        this.code = num.intValue();
    }

    @DexIgnore
    public void setMessage(String str) {
        this.message = str;
    }

    @DexIgnore
    public void setUserMessage(String str) {
        this.userMessage = str;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.code);
        parcel.writeString(this.message);
        parcel.writeString(this.userMessage);
        parcel.writeString(this.errorDetails);
    }

    @DexIgnore
    public ServerError(int i, String str) {
        this.code = i;
        this.message = str;
    }

    @DexIgnore
    public ServerError(Parcel parcel) {
        this.code = parcel.readInt();
        this.message = parcel.readString();
        this.userMessage = parcel.readString();
        this.errorDetails = parcel.readString();
    }
}
