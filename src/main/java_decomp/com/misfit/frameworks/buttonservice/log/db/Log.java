package com.misfit.frameworks.buttonservice.log.db;

import com.fossil.qg6;
import com.fossil.wg6;
import com.portfolio.platform.data.model.ServerSetting;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Log {
    @DexIgnore
    public Flag cloudFlag;
    @DexIgnore
    public String content;
    @DexIgnore
    public int id;
    @DexIgnore
    public long timeStamp;

    @DexIgnore
    public enum Flag {
        ADD("ADD"),
        SYNCING("SYNCING");
        
        @DexIgnore
        public static /* final */ Companion Companion; // = null;
        @DexIgnore
        public /* final */ String value;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class Companion {
            @DexIgnore
            public Companion() {
            }

            @DexIgnore
            public final Flag fromValue(String str) {
                Flag flag;
                wg6.b(str, ServerSetting.VALUE);
                Flag[] values = Flag.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        flag = null;
                        break;
                    }
                    flag = values[i];
                    if (wg6.a((Object) flag.getValue(), (Object) str)) {
                        break;
                    }
                    i++;
                }
                return flag != null ? flag : Flag.ADD;
            }

            @DexIgnore
            public /* synthetic */ Companion(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            Companion = new Companion((qg6) null);
        }
        */

        @DexIgnore
        public Flag(String str) {
            this.value = str;
        }

        @DexIgnore
        public final String getValue() {
            return this.value;
        }
    }

    @DexIgnore
    public Log(long j, String str, Flag flag) {
        wg6.b(str, "content");
        wg6.b(flag, "cloudFlag");
        this.timeStamp = j;
        this.content = str;
        this.cloudFlag = flag;
    }

    @DexIgnore
    public static /* synthetic */ Log copy$default(Log log, long j, String str, Flag flag, int i, Object obj) {
        if ((i & 1) != 0) {
            j = log.timeStamp;
        }
        if ((i & 2) != 0) {
            str = log.content;
        }
        if ((i & 4) != 0) {
            flag = log.cloudFlag;
        }
        return log.copy(j, str, flag);
    }

    @DexIgnore
    public final long component1() {
        return this.timeStamp;
    }

    @DexIgnore
    public final String component2() {
        return this.content;
    }

    @DexIgnore
    public final Flag component3() {
        return this.cloudFlag;
    }

    @DexIgnore
    public final Log copy(long j, String str, Flag flag) {
        wg6.b(str, "content");
        wg6.b(flag, "cloudFlag");
        return new Log(j, str, flag);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Log)) {
            return false;
        }
        Log log = (Log) obj;
        return this.timeStamp == log.timeStamp && wg6.a((Object) this.content, (Object) log.content) && wg6.a((Object) this.cloudFlag, (Object) log.cloudFlag);
    }

    @DexIgnore
    public final Flag getCloudFlag() {
        return this.cloudFlag;
    }

    @DexIgnore
    public final String getContent() {
        return this.content;
    }

    @DexIgnore
    public final int getId() {
        return this.id;
    }

    @DexIgnore
    public final long getTimeStamp() {
        return this.timeStamp;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.timeStamp;
        int i = ((int) (j ^ (j >>> 32))) * 31;
        String str = this.content;
        int i2 = 0;
        int hashCode = (i + (str != null ? str.hashCode() : 0)) * 31;
        Flag flag = this.cloudFlag;
        if (flag != null) {
            i2 = flag.hashCode();
        }
        return hashCode + i2;
    }

    @DexIgnore
    public final void setCloudFlag(Flag flag) {
        wg6.b(flag, "<set-?>");
        this.cloudFlag = flag;
    }

    @DexIgnore
    public final void setContent(String str) {
        wg6.b(str, "<set-?>");
        this.content = str;
    }

    @DexIgnore
    public final void setId(int i) {
        this.id = i;
    }

    @DexIgnore
    public final void setTimeStamp(long j) {
        this.timeStamp = j;
    }

    @DexIgnore
    public String toString() {
        return "Log(timeStamp=" + this.timeStamp + ", content=" + this.content + ", cloudFlag=" + this.cloudFlag + ")";
    }
}
