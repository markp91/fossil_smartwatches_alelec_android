package com.misfit.frameworks.buttonservice.log;

import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.yd6;
import com.misfit.frameworks.buttonservice.log.db.Log;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.DBLogWriter$updateCloudFlag$2", f = "DBLogWriter.kt", l = {}, m = "invokeSuspend")
public final class DBLogWriter$updateCloudFlag$Anon2 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Log.Flag $logFlag;
    @DexIgnore
    public /* final */ /* synthetic */ List $logIds;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DBLogWriter this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public DBLogWriter$updateCloudFlag$Anon2(DBLogWriter dBLogWriter, List list, Log.Flag flag, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dBLogWriter;
        this.$logIds = list;
        this.$logFlag = flag;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        DBLogWriter$updateCloudFlag$Anon2 dBLogWriter$updateCloudFlag$Anon2 = new DBLogWriter$updateCloudFlag$Anon2(this.this$0, this.$logIds, this.$logFlag, xe6);
        dBLogWriter$updateCloudFlag$Anon2.p$ = (il6) obj;
        return dBLogWriter$updateCloudFlag$Anon2;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((DBLogWriter$updateCloudFlag$Anon2) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            for (List d : yd6.b(this.$logIds, 500)) {
                this.this$0.logDao.updateCloudFlagByIds(yd6.d(d), this.$logFlag);
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
