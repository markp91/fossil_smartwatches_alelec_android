package com.misfit.frameworks.buttonservice.log;

import android.os.Bundle;
import com.fossil.cd6;
import com.fossil.ff6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.lf6;
import com.fossil.nc6;
import com.fossil.sf6;
import com.fossil.wg6;
import com.fossil.xe6;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.misfit.frameworks.buttonservice.log.RemoteFLogger$flush$1", f = "RemoteFLogger.kt", l = {}, m = "invokeSuspend")
public final class RemoteFLogger$flush$Anon1 extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ RemoteFLogger this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RemoteFLogger$flush$Anon1(RemoteFLogger remoteFLogger, xe6 xe6) {
        super(2, xe6);
        this.this$0 = remoteFLogger;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        RemoteFLogger$flush$Anon1 remoteFLogger$flush$Anon1 = new RemoteFLogger$flush$Anon1(this.this$0, xe6);
        remoteFLogger$flush$Anon1.p$ = (il6) obj;
        return remoteFLogger$flush$Anon1;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((RemoteFLogger$flush$Anon1) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            if (this.this$0.isMainFLogger && !this.this$0.isFlushing) {
                this.this$0.isFlushing = true;
                RemoteFLogger remoteFLogger = this.this$0;
                remoteFLogger.sendInternalMessage(RemoteFLogger.MESSAGE_ACTION_FLUSH, remoteFLogger.floggerName, RemoteFLogger.MessageTarget.TO_ALL_EXCEPT_MAIN_FLOGGER, new Bundle());
                this.this$0.isFlushing = false;
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
