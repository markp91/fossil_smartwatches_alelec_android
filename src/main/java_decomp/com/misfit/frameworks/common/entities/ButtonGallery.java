package com.misfit.frameworks.common.entities;

import android.graphics.Color;
import com.misfit.frameworks.common.constants.Constants;
import com.misfit.frameworks.common.enums.Gesture;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ButtonGallery implements Serializable {
    @DexIgnore
    public String androidStoreUrl;
    @DexIgnore
    public int appId;
    @DexIgnore
    public String appName;
    @DexIgnore
    public String comName;
    @DexIgnore
    public String desc;
    @DexIgnore
    public String descPicUrl;
    @DexIgnore
    public Map<Gesture, String> gestureDescMap; // = new HashMap();
    @DexIgnore
    public String id;
    @DexIgnore
    public String jsonString;
    @DexIgnore
    public String logoUrl;
    @DexIgnore
    public String packageName;
    @DexIgnore
    public int themeColor;
    @DexIgnore
    public int themeColorSecondary;
    @DexIgnore
    public int versionCode;

    /* JADX WARNING: Can't wrap try/catch for region: R(2:5|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r1 = r10.getInt("appId");
     */
    @DexIgnore
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0019 */
    public static ButtonGallery objectFromJson(JSONObject jSONObject) {
        ButtonGallery buttonGallery = new ButtonGallery();
        try {
            String string = jSONObject.getString("id");
            int i = Integer.parseInt(jSONObject.getString("appId"));
            if (string != null && !string.isEmpty()) {
                if (i != 0) {
                    buttonGallery.id = string;
                    buttonGallery.appId = i;
                    buttonGallery.appName = jSONObject.getString("appName");
                    buttonGallery.desc = jSONObject.getString(Constants.DESC);
                    buttonGallery.comName = jSONObject.getString("comName");
                    buttonGallery.logoUrl = jSONObject.getString("logoUrl");
                    buttonGallery.themeColor = Color.parseColor(jSONObject.getString("themeColor"));
                    buttonGallery.themeColorSecondary = Color.parseColor(jSONObject.getString("themeColorSecondary"));
                    buttonGallery.androidStoreUrl = jSONObject.getString("androidStoreUrl");
                    buttonGallery.packageName = jSONObject.getString("androidPackageName");
                    try {
                        buttonGallery.versionCode = Integer.parseInt(jSONObject.getString("lowestAndroidAppVersion"));
                    } catch (Exception unused) {
                    }
                    buttonGallery.descPicUrl = jSONObject.getString("descPicUrl");
                    JSONArray jSONArray = jSONObject.getJSONObject(Constants.COMMANDS).getJSONArray("data");
                    HashMap hashMap = new HashMap();
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        JSONObject jSONObject2 = jSONArray.getJSONObject(i2);
                        String string2 = jSONObject2.getString("name");
                        String string3 = jSONObject2.getString(Constants.DESC);
                        if (!string2.isEmpty() && !string3.isEmpty()) {
                            hashMap.put(string2, string3);
                        }
                    }
                    JSONArray jSONArray2 = jSONObject.getJSONObject("mappings").getJSONArray(Constants.DEFAULTS);
                    for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                        JSONObject jSONObject3 = jSONArray2.getJSONObject(i3);
                        buttonGallery.gestureDescMap.put(Gesture.values()[jSONObject3.getInt("gesture")], (String) hashMap.get(jSONObject3.getString("action")));
                    }
                    buttonGallery.jsonString = jSONObject.toString();
                    return buttonGallery;
                }
            }
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String getAndroidStoreUrl() {
        return this.androidStoreUrl;
    }

    @DexIgnore
    public int getAppId() {
        return this.appId;
    }

    @DexIgnore
    public String getAppName() {
        return this.appName;
    }

    @DexIgnore
    public String getComName() {
        return this.comName;
    }

    @DexIgnore
    public String getDesc() {
        return this.desc;
    }

    @DexIgnore
    public String getDescPicUrl() {
        return this.descPicUrl;
    }

    @DexIgnore
    public Map<Gesture, String> getGestureDescMap() {
        return this.gestureDescMap;
    }

    @DexIgnore
    public String getId() {
        return this.id;
    }

    @DexIgnore
    public String getJsonString() {
        return this.jsonString;
    }

    @DexIgnore
    public String getLogoUrl() {
        return this.logoUrl;
    }

    @DexIgnore
    public String getMappingsString() {
        try {
            return new JSONObject(this.jsonString).getJSONObject("mappings").getJSONArray(Constants.DEFAULTS).toString();
        } catch (Exception unused) {
            return null;
        }
    }

    @DexIgnore
    public String getPackageName() {
        return this.packageName;
    }

    @DexIgnore
    public int getThemeColor() {
        return this.themeColor;
    }

    @DexIgnore
    public int getThemeColorSecondary() {
        return this.themeColorSecondary;
    }

    @DexIgnore
    public int getVersionCode() {
        return this.versionCode;
    }

    @DexIgnore
    public void setAppId(int i) {
        this.appId = i;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }
}
