package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ButtonStreamingEvent {
    @DexIgnore
    public static /* final */ int DOUBLE_PRESS; // = 20;
    @DexIgnore
    public static /* final */ int LONG_PRESS; // = 22;
    @DexIgnore
    public static /* final */ int SINGLE_PRESS; // = 19;
    @DexIgnore
    public static /* final */ int TRIPLE_PRESS; // = 21;
}
