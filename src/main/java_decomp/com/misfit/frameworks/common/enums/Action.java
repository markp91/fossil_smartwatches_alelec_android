package com.misfit.frameworks.common.enums;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Action {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class ActivityTracker {
        @DexIgnore
        public static /* final */ int TAG_ACTIVITY; // = 402;
        @DexIgnore
        public static /* final */ int TIME_PROGRESS; // = 401;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 401 || i == 402;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Apps {
        @DexIgnore
        public static /* final */ int BOLT; // = 501;
        @DexIgnore
        public static /* final */ int HARMONY; // = 504;
        @DexIgnore
        public static /* final */ int IF; // = 502;
        @DexIgnore
        public static /* final */ int RING_MY_PHONE; // = 505;
        @DexIgnore
        public static /* final */ int YO; // = 503;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 501 || i == 502 || i == 503 || i == 505;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Bolt {
        @DexIgnore
        public static /* final */ int BOLT_END_ACTION; // = 699;
        @DexIgnore
        public static /* final */ int BOLT_START_ACTION; // = 600;
        @DexIgnore
        public static /* final */ int CHANGE_BRIGHTNESS; // = 601;
        @DexIgnore
        public static /* final */ int TURN_OFF; // = 602;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 600 || i == 601 || i == 602;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class DisplayAction {
        @DexIgnore
        public static Integer[] DISPLAY_ACTIONS; // = {Integer.valueOf(DisplayMode.ACTIVITY), Integer.valueOf(DisplayMode.DATE), Integer.valueOf(DisplayMode.NOTIFICATION), Integer.valueOf(DisplayMode.SECOND_TIMEZONE), Integer.valueOf(DisplayMode.TOGGLE_MODE), Integer.valueOf(DisplayMode.ALARM), Integer.valueOf(DisplayMode.COUNT_DOWN), Integer.valueOf(DisplayMode.STOP_WATCH)};

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            for (Integer intValue : DISPLAY_ACTIONS) {
                if (intValue.intValue() == i) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class DisplayMode {
        @DexIgnore
        public static /* final */ int ACTIVITY; // = 2001;
        @DexIgnore
        public static /* final */ int ALARM; // = 2005;
        @DexIgnore
        public static /* final */ int COUNT_DOWN; // = 2008;
        @DexIgnore
        public static /* final */ int DATE; // = 2003;
        @DexIgnore
        public static /* final */ int NOTIFICATION; // = 2002;
        @DexIgnore
        public static /* final */ int SECOND_TIMEZONE; // = 2004;
        @DexIgnore
        public static /* final */ int STOP_WATCH; // = 2007;
        @DexIgnore
        public static /* final */ int TOGGLE_MODE; // = 2006;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 2001 || i == 2002 || i == 2003 || i == 2004 || i == 2005 || i == 2006 || i == 2007 || i == 2008;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class GoalTracking {
        @DexIgnore
        public static /* final */ int GOAL_TRACKING; // = 1000;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 1000;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class HIDAction {
        @DexIgnore
        public static Integer[] HID_ACTIONS; // = {101, 102, 103, 104, 105, 201, Integer.valueOf(Selfie.TAKE_BURST)};

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            for (Integer intValue : HID_ACTIONS) {
                if (intValue.intValue() == i) {
                    return true;
                }
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class MicroAppAction {
        @DexIgnore
        public static /* final */ int SEVEN_MINUTE_WORKOUT; // = 3103;
        @DexIgnore
        public static /* final */ int SHOW_COMMUTE; // = 3002;
        @DexIgnore
        public static /* final */ int SHOW_TEMPERATURE; // = 3001;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 3001 || i == 3002 || i == 3103;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Music {
        @DexIgnore
        public static /* final */ int MUSIC_END_ACTION; // = 199;
        @DexIgnore
        public static /* final */ int NEXT; // = 102;
        @DexIgnore
        public static /* final */ int PLAY; // = 101;
        @DexIgnore
        public static /* final */ int PREVIOUS; // = 103;
        @DexIgnore
        public static /* final */ int VOLUMN_DOWN; // = 105;
        @DexIgnore
        public static /* final */ int VOLUMN_UP; // = 104;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 101 || i == 102 || i == 103 || i == 104 || i == 105;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Presenter {
        @DexIgnore
        public static /* final */ int BLACKOUT; // = 303;
        @DexIgnore
        public static /* final */ int NEXT; // = 301;
        @DexIgnore
        public static /* final */ int PREVIOUS; // = 302;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 301 || i == 302 || i == 303;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Selfie {
        @DexIgnore
        public static /* final */ int SELFIE_END_ACTION; // = 299;
        @DexIgnore
        public static /* final */ int TAKE_BURST; // = 202;
        @DexIgnore
        public static /* final */ int TAKE_ONE; // = 201;

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            return i == 201 || i == 202;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class StreamingAction {
        @DexIgnore
        public static Integer[] STREAMING_ACTIONS; // = {Integer.valueOf(Apps.RING_MY_PHONE), Integer.valueOf(MicroAppAction.SHOW_COMMUTE), Integer.valueOf(MicroAppAction.SHOW_TEMPERATURE), Integer.valueOf(MicroAppAction.SEVEN_MINUTE_WORKOUT)};

        @DexIgnore
        public static boolean isActionBelongToThisType(int i) {
            for (Integer intValue : STREAMING_ACTIONS) {
                if (intValue.intValue() == i) {
                    return true;
                }
            }
            return false;
        }
    }
}
