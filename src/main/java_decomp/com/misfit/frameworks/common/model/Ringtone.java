package com.misfit.frameworks.common.model;

import android.util.Log;
import com.misfit.frameworks.common.constants.Constants;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class Ringtone {
    @DexIgnore
    public static /* final */ String TAG; // = "Ringtone";
    @DexIgnore
    public String mButtonSerial;
    @DexIgnore
    public String mExtension;
    @DexIgnore
    public int mGesture;
    @DexIgnore
    public String mName;

    @DexIgnore
    public Ringtone() {
    }

    @DexIgnore
    public static Ringtone fromJsonString(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            Ringtone ringtone = new Ringtone();
            if (jSONObject.has(Constants.RINGTONE)) {
                ringtone.setName(jSONObject.getString(Constants.RINGTONE));
            }
            if (jSONObject.has("serialNumber")) {
                ringtone.setButtonSerial(jSONObject.getString("serialNumber"));
            }
            if (jSONObject.has("gesture")) {
                ringtone.setGesture(jSONObject.getInt("gesture"));
            }
            if (jSONObject.has(Constants.RINGTONE_EXTENSION)) {
                ringtone.setExtension(jSONObject.getString(Constants.RINGTONE_EXTENSION));
            }
            return ringtone;
        } catch (JSONException unused) {
            Log.d(TAG, "fail to parse json from string");
            return null;
        }
    }

    @DexIgnore
    public String getButtonSerial() {
        return this.mButtonSerial;
    }

    @DexIgnore
    public String getExtension() {
        return this.mExtension;
    }

    @DexIgnore
    public int getGesture() {
        return this.mGesture;
    }

    @DexIgnore
    public String getName() {
        return this.mName;
    }

    @DexIgnore
    public void setButtonSerial(String str) {
        this.mButtonSerial = str;
    }

    @DexIgnore
    public void setExtension(String str) {
        this.mExtension = str;
    }

    @DexIgnore
    public void setGesture(int i) {
        this.mGesture = i;
    }

    @DexIgnore
    public void setName(String str) {
        this.mName = str;
    }

    @DexIgnore
    public JSONObject toJSON() throws Exception {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(Constants.RINGTONE, this.mName);
        jSONObject.put("serialNumber", this.mButtonSerial);
        jSONObject.put("gesture", this.mGesture);
        jSONObject.put(Constants.RINGTONE_EXTENSION, this.mExtension);
        return jSONObject;
    }

    @DexIgnore
    public String toJsonString() {
        try {
            return toJSON().toString();
        } catch (Exception unused) {
            Log.d(TAG, "Exception when get string from json");
            return "";
        }
    }

    @DexIgnore
    public String toString() {
        return this.mName + "." + this.mExtension;
    }

    @DexIgnore
    public Ringtone(String str, String str2, int i, String str3) {
        this.mName = str;
        this.mExtension = str2;
        this.mGesture = i;
        this.mButtonSerial = str3;
    }
}
