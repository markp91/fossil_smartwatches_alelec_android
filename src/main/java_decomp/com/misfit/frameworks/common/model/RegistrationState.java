package com.misfit.frameworks.common.model;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum RegistrationState implements Parcelable {
    STATE_REGISTERED(0),
    STATE_UNREGISTERED(1),
    STATE_UNKNOWN(-1);
    
    @DexIgnore
    public static /* final */ Parcelable.Creator<RegistrationState> CREATOR; // = null;
    @DexIgnore
    public int mId;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<RegistrationState> {
        @DexIgnore
        public RegistrationState createFromParcel(Parcel parcel) {
            return RegistrationState.getValue(parcel.readInt());
        }

        @DexIgnore
        public RegistrationState[] newArray(int i) {
            return new RegistrationState[i];
        }
    }

    /*
    static {
        CREATOR = new Anon1();
    }
    */

    @DexIgnore
    public RegistrationState(int i) {
        this.mId = i;
    }

    @DexIgnore
    public static RegistrationState getValue(int i) {
        for (RegistrationState registrationState : values()) {
            if (registrationState.mId == i) {
                return registrationState;
            }
        }
        return STATE_UNKNOWN;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
    }
}
