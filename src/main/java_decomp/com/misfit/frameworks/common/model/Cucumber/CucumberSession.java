package com.misfit.frameworks.common.model.Cucumber;

import com.misfit.frameworks.common.model.MFBRpm;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CucumberSession {
    @DexIgnore
    public CucumberMapInfo[] arrayMapInfo;
    @DexIgnore
    public MFBRpm[] chart;
    @DexIgnore
    public CucumberSessionInfo info;

    @DexIgnore
    public CucumberMapInfo[] getArrayMapInfo() {
        return this.arrayMapInfo;
    }

    @DexIgnore
    public MFBRpm[] getChart() {
        return this.chart;
    }

    @DexIgnore
    public CucumberSessionInfo getInfo() {
        return this.info;
    }

    @DexIgnore
    public void setArrayMapInfo(CucumberMapInfo[] cucumberMapInfoArr) {
        this.arrayMapInfo = cucumberMapInfoArr;
    }

    @DexIgnore
    public void setChart(MFBRpm[] mFBRpmArr) {
        this.chart = mFBRpmArr;
    }

    @DexIgnore
    public void setInfo(CucumberSessionInfo cucumberSessionInfo) {
        this.info = cucumberSessionInfo;
    }
}
