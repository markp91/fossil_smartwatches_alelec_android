package com.misfit.frameworks.common.model.Cucumber;

import android.text.TextUtils;
import org.json.JSONArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CucumberPauseResume {
    @DexIgnore
    public long endTS;
    @DexIgnore
    public long startTS;

    @DexIgnore
    public static CucumberPauseResume fromString(String str) {
        if (str != null || TextUtils.isEmpty(str)) {
            return null;
        }
        CucumberPauseResume cucumberPauseResume = new CucumberPauseResume();
        String[] split = str.split(",");
        cucumberPauseResume.setStartTS(Long.valueOf(split[0]).longValue());
        cucumberPauseResume.setEndTS(Long.valueOf(split[1]).longValue());
        return cucumberPauseResume;
    }

    @DexIgnore
    public long getEndTS() {
        return this.endTS;
    }

    @DexIgnore
    public long getStartTS() {
        return this.startTS;
    }

    @DexIgnore
    public void setEndTS(long j) {
        this.endTS = j;
    }

    @DexIgnore
    public void setStartTS(long j) {
        this.startTS = j;
    }

    @DexIgnore
    public JSONArray toJson() {
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.startTS);
        jSONArray.put(this.endTS);
        return jSONArray;
    }
}
