package com.fossil;

import com.fossil.fs;
import com.fossil.jv;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mv<Model, Data> implements jv<Model, Data> {
    @DexIgnore
    public /* final */ List<jv<Model, Data>> a;
    @DexIgnore
    public /* final */ v8<List<Throwable>> b;

    @DexIgnore
    public mv(List<jv<Model, Data>> list, v8<List<Throwable>> v8Var) {
        this.a = list;
        this.b = v8Var;
    }

    @DexIgnore
    public jv.a<Data> a(Model model, int i, int i2, xr xrVar) {
        jv.a a2;
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        vr vrVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            jv jvVar = this.a.get(i3);
            if (jvVar.a(model) && (a2 = jvVar.a(model, i, i2, xrVar)) != null) {
                vrVar = a2.a;
                arrayList.add(a2.c);
            }
        }
        if (arrayList.isEmpty() || vrVar == null) {
            return null;
        }
        return new jv.a<>(vrVar, new a(arrayList, this.b));
    }

    @DexIgnore
    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.a.toArray()) + '}';
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<Data> implements fs<Data>, fs.a<Data> {
        @DexIgnore
        public /* final */ List<fs<Data>> a;
        @DexIgnore
        public /* final */ v8<List<Throwable>> b;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public br d;
        @DexIgnore
        public fs.a<? super Data> e;
        @DexIgnore
        public List<Throwable> f;
        @DexIgnore
        public boolean g;

        @DexIgnore
        public a(List<fs<Data>> list, v8<List<Throwable>> v8Var) {
            this.b = v8Var;
            q00.a(list);
            this.a = list;
        }

        @DexIgnore
        public void a(br brVar, fs.a<? super Data> aVar) {
            this.d = brVar;
            this.e = aVar;
            this.f = this.b.a();
            this.a.get(this.c).a(brVar, this);
            if (this.g) {
                cancel();
            }
        }

        @DexIgnore
        public pr b() {
            return this.a.get(0).b();
        }

        @DexIgnore
        public final void c() {
            if (!this.g) {
                if (this.c < this.a.size() - 1) {
                    this.c++;
                    a(this.d, this.e);
                    return;
                }
                q00.a(this.f);
                this.e.a((Exception) new mt("Fetch failed", (List<Throwable>) new ArrayList(this.f)));
            }
        }

        @DexIgnore
        public void cancel() {
            this.g = true;
            for (fs<Data> cancel : this.a) {
                cancel.cancel();
            }
        }

        @DexIgnore
        public Class<Data> getDataClass() {
            return this.a.get(0).getDataClass();
        }

        @DexIgnore
        public void a() {
            List<Throwable> list = this.f;
            if (list != null) {
                this.b.a(list);
            }
            this.f = null;
            for (fs<Data> a2 : this.a) {
                a2.a();
            }
        }

        @DexIgnore
        public void a(Data data) {
            if (data != null) {
                this.e.a(data);
            } else {
                c();
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            List<Throwable> list = this.f;
            q00.a(list);
            list.add(exc);
            c();
        }
    }

    @DexIgnore
    public boolean a(Model model) {
        for (jv<Model, Data> a2 : this.a) {
            if (a2.a(model)) {
                return true;
            }
        }
        return false;
    }
}
