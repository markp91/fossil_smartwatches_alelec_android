package com.fossil;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wl4 {
    @DexIgnore
    public Vector<am4> a;
    @DexIgnore
    public Map<String, am4> b;
    @DexIgnore
    public Map<String, am4> c;
    @DexIgnore
    public Map<String, am4> d;
    @DexIgnore
    public Map<String, am4> e;

    @DexIgnore
    public wl4() {
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.a = new Vector<>();
        this.b = new HashMap();
        this.c = new HashMap();
        this.d = new HashMap();
        this.e = new HashMap();
    }

    @DexIgnore
    public boolean a(am4 am4) {
        if (am4 != null) {
            b(am4);
            return this.a.add(am4);
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public final void b(am4 am4) {
        byte[] a2 = am4.a();
        if (a2 != null) {
            this.b.put(new String(a2), am4);
        }
        byte[] b2 = am4.b();
        if (b2 != null) {
            this.c.put(new String(b2), am4);
        }
        byte[] f = am4.f();
        if (f != null) {
            this.d.put(new String(f), am4);
        }
        byte[] e2 = am4.e();
        if (e2 != null) {
            this.e.put(new String(e2), am4);
        }
    }

    @DexIgnore
    public void a(int i, am4 am4) {
        if (am4 != null) {
            b(am4);
            this.a.add(i, am4);
            return;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public void a() {
        this.a.clear();
    }

    @DexIgnore
    public am4 a(int i) {
        return this.a.get(i);
    }
}
