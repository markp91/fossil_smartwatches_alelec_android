package com.fossil;

import android.app.PendingIntent;
import com.fossil.c12;
import com.fossil.rv1;
import java.util.ArrayList;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class px1 extends yx1 {
    @DexIgnore
    public /* final */ Map<rv1.f, qx1> b;
    @DexIgnore
    public /* final */ /* synthetic */ ox1 c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public px1(ox1 ox1, Map<rv1.f, qx1> map) {
        super(ox1, (nx1) null);
        this.c = ox1;
        this.b = map;
    }

    @DexIgnore
    public final void a() {
        m12 m12 = new m12(this.c.d);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (rv1.f next : this.b.keySet()) {
            if (!next.i() || this.b.get(next).c) {
                arrayList2.add(next);
            } else {
                arrayList.add(next);
            }
        }
        int i = -1;
        int i2 = 0;
        if (!arrayList.isEmpty()) {
            int size = arrayList.size();
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                i = m12.a(this.c.c, (rv1.f) obj);
                if (i != 0) {
                    break;
                }
            }
        } else {
            int size2 = arrayList2.size();
            while (i2 < size2) {
                Object obj2 = arrayList2.get(i2);
                i2++;
                i = m12.a(this.c.c, (rv1.f) obj2);
                if (i == 0) {
                    break;
                }
            }
        }
        if (i != 0) {
            this.c.a.a((iy1) new sx1(this, this.c, new gv1(i, (PendingIntent) null)));
            return;
        }
        if (this.c.m && this.c.k != null) {
            this.c.k.b();
        }
        for (rv1.f next2 : this.b.keySet()) {
            c12.c cVar = this.b.get(next2);
            if (!next2.i() || m12.a(this.c.c, next2) == 0) {
                next2.a(cVar);
            } else {
                this.c.a.a((iy1) new rx1(this, this.c, cVar));
            }
        }
    }
}
