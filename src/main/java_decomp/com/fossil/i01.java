package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i01 extends xg6 implements hg6<qv0, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ u51 a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public i01(u51 u51) {
        super(1);
        this.a = u51;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        sk1 sk1;
        qv0 qv0 = (qv0) obj;
        bn0 bn0 = qv0.v;
        if (bn0.c == il0.RESPONSE_ERROR) {
            sk1 = sk1.AUTHENTICATION_FAILED;
        } else {
            sk1 = sk1.G.a(bn0);
        }
        u51 u51 = this.a;
        u51.a(km1.a(u51.v, (eh1) null, sk1, qv0.v, 1));
        return cd6.a;
    }
}
