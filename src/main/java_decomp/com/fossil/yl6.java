package com.fossil;

import com.fossil.mc6;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yl6<T> extends mp6 {
    @DexIgnore
    public int c;

    @DexIgnore
    public yl6(int i) {
        this.c = i;
    }

    @DexIgnore
    public final Throwable a(Object obj) {
        if (!(obj instanceof vk6)) {
            obj = null;
        }
        vk6 vk6 = (vk6) obj;
        if (vk6 != null) {
            return vk6.a;
        }
        return null;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        wg6.b(th, "cause");
    }

    @DexIgnore
    public abstract xe6<T> b();

    @DexIgnore
    public abstract Object c();

    @DexIgnore
    public <T> T c(Object obj) {
        return obj;
    }

    @DexIgnore
    public final void run() {
        Object obj;
        af6 context;
        Object b;
        Object obj2;
        np6 np6 = this.b;
        try {
            xe6 b2 = b();
            if (b2 != null) {
                vl6 vl6 = (vl6) b2;
                xe6<T> xe6 = vl6.h;
                context = xe6.getContext();
                Object c2 = c();
                b = yo6.b(context, vl6.f);
                Throwable a = a(c2);
                rm6 rm6 = jn6.a(this.c) ? (rm6) context.get(rm6.n) : null;
                if (a == null && rm6 != null && !rm6.isActive()) {
                    CancellationException k = rm6.k();
                    a(c2, (Throwable) k);
                    mc6.a aVar = mc6.Companion;
                    xe6.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(k, (xe6<?>) xe6))));
                } else if (a != null) {
                    mc6.a aVar2 = mc6.Companion;
                    xe6.resumeWith(mc6.m1constructorimpl(nc6.a(to6.a(a, (xe6<?>) xe6))));
                } else {
                    Object c3 = c(c2);
                    mc6.a aVar3 = mc6.Companion;
                    xe6.resumeWith(mc6.m1constructorimpl(c3));
                }
                cd6 cd6 = cd6.a;
                yo6.a(context, b);
                try {
                    mc6.a aVar4 = mc6.Companion;
                    np6.m();
                    obj2 = mc6.m1constructorimpl(cd6.a);
                } catch (Throwable th) {
                    mc6.a aVar5 = mc6.Companion;
                    obj2 = mc6.m1constructorimpl(nc6.a(th));
                }
                a((Throwable) null, mc6.m4exceptionOrNullimpl(obj2));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.DispatchedContinuation<T>");
        } catch (Throwable th2) {
            try {
                mc6.a aVar6 = mc6.Companion;
                np6.m();
                obj = mc6.m1constructorimpl(cd6.a);
            } catch (Throwable th3) {
                mc6.a aVar7 = mc6.Companion;
                obj = mc6.m1constructorimpl(nc6.a(th3));
            }
            a(th2, mc6.m4exceptionOrNullimpl(obj));
        }
    }

    @DexIgnore
    public final void a(Throwable th, Throwable th2) {
        if (th != null || th2 != null) {
            if (!(th == null || th2 == null)) {
                ec6.a(th, th2);
            }
            if (th == null) {
                th = th2;
            }
            String str = "Fatal exception in coroutines machinery for " + this + ". " + "Please read KDoc to 'handleFatalException' method and report this incident to maintainers";
            if (th != null) {
                fl6.a(b().getContext(), (Throwable) new ml6(str, th));
                return;
            }
            wg6.a();
            throw null;
        }
    }
}
