package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l14 implements Factory<hn4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public l14(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static l14 a(b14 b14) {
        return new l14(b14);
    }

    @DexIgnore
    public static hn4 b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static hn4 c(b14 b14) {
        hn4 f = b14.f();
        z76.a(f, "Cannot return null from a non-@Nullable @Provides method");
        return f;
    }

    @DexIgnore
    public hn4 get() {
        return b(this.a);
    }
}
