package com.fossil;

import android.app.Activity;
import android.os.Bundle;
import com.fossil.w52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e62 implements w52.a {
    @DexIgnore
    public /* final */ /* synthetic */ Activity a;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle b;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle c;
    @DexIgnore
    public /* final */ /* synthetic */ w52 d;

    @DexIgnore
    public e62(w52 w52, Activity activity, Bundle bundle, Bundle bundle2) {
        this.d = w52;
        this.a = activity;
        this.b = bundle;
        this.c = bundle2;
    }

    @DexIgnore
    public final void a(y52 y52) {
        this.d.a.a(this.a, this.b, this.c);
    }

    @DexIgnore
    public final int getState() {
        return 0;
    }
}
