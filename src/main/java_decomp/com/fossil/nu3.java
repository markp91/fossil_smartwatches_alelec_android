package com.fossil;

import com.google.gson.JsonElement;
import java.math.BigInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu3 extends JsonElement {
    @DexIgnore
    public static /* final */ Class<?>[] b; // = {Integer.TYPE, Long.TYPE, Short.TYPE, Float.TYPE, Double.TYPE, Byte.TYPE, Boolean.TYPE, Character.TYPE, Integer.class, Long.class, Short.class, Float.class, Double.class, Byte.class, Boolean.class, Character.class};
    @DexIgnore
    public Object a;

    @DexIgnore
    public nu3(Boolean bool) {
        a((Object) bool);
    }

    @DexIgnore
    public void a(Object obj) {
        if (obj instanceof Character) {
            this.a = String.valueOf(((Character) obj).charValue());
            return;
        }
        yu3.a((obj instanceof Number) || b(obj));
        this.a = obj;
    }

    @DexIgnore
    public int b() {
        return t() ? r().intValue() : Integer.parseInt(f());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || nu3.class != obj.getClass()) {
            return false;
        }
        nu3 nu3 = (nu3) obj;
        if (this.a == null) {
            if (nu3.a == null) {
                return true;
            }
            return false;
        } else if (!a(this) || !a(nu3)) {
            if (!(this.a instanceof Number) || !(nu3.a instanceof Number)) {
                return this.a.equals(nu3.a);
            }
            double doubleValue = r().doubleValue();
            double doubleValue2 = nu3.r().doubleValue();
            if (doubleValue == doubleValue2) {
                return true;
            }
            if (!Double.isNaN(doubleValue) || !Double.isNaN(doubleValue2)) {
                return false;
            }
            return true;
        } else if (r().longValue() == nu3.r().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    @DexIgnore
    public String f() {
        if (t()) {
            return r().toString();
        }
        if (s()) {
            return o().toString();
        }
        return (String) this.a;
    }

    @DexIgnore
    public int hashCode() {
        long doubleToLongBits;
        if (this.a == null) {
            return 31;
        }
        if (a(this)) {
            doubleToLongBits = r().longValue();
        } else {
            Object obj = this.a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(r().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    @DexIgnore
    public Boolean o() {
        return (Boolean) this.a;
    }

    @DexIgnore
    public double p() {
        return t() ? r().doubleValue() : Double.parseDouble(f());
    }

    @DexIgnore
    public long q() {
        return t() ? r().longValue() : Long.parseLong(f());
    }

    @DexIgnore
    public Number r() {
        Object obj = this.a;
        return obj instanceof String ? new dv3((String) obj) : (Number) obj;
    }

    @DexIgnore
    public boolean s() {
        return this.a instanceof Boolean;
    }

    @DexIgnore
    public boolean t() {
        return this.a instanceof Number;
    }

    @DexIgnore
    public boolean u() {
        return this.a instanceof String;
    }

    @DexIgnore
    public static boolean b(Object obj) {
        if (obj instanceof String) {
            return true;
        }
        Class<?> cls = obj.getClass();
        for (Class<?> isAssignableFrom : b) {
            if (isAssignableFrom.isAssignableFrom(cls)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public nu3(Number number) {
        a((Object) number);
    }

    @DexIgnore
    public nu3(String str) {
        a((Object) str);
    }

    @DexIgnore
    public nu3(Object obj) {
        a(obj);
    }

    @DexIgnore
    public boolean a() {
        if (s()) {
            return o().booleanValue();
        }
        return Boolean.parseBoolean(f());
    }

    @DexIgnore
    public static boolean a(nu3 nu3) {
        Object obj = nu3.a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }
}
