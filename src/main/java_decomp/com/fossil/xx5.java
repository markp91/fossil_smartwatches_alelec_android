package com.fossil;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.InAppPermission;
import com.portfolio.platform.service.FossilNotificationListenerService;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx5 {
    @DexIgnore
    public static /* final */ a a; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final boolean a(Context context) {
            wg6.b(context, "context");
            return a(context, "android.permission.ACCESS_BACKGROUND_LOCATION");
        }

        @DexIgnore
        public final boolean b(Context context) {
            wg6.b(context, "context");
            return a(context, "android.permission.ACCESS_FINE_LOCATION");
        }

        @DexIgnore
        public final boolean c() {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return defaultAdapter != null && defaultAdapter.isEnabled();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: type inference failed for: r0v8, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final boolean d() {
            LocationManager locationManager = (LocationManager) PortfolioApp.get.instance().getSystemService("location");
            if (locationManager == null) {
                return false;
            }
            String bestProvider = locationManager.getBestProvider(new Criteria(), true);
            if (!(bv6.a(bestProvider) || wg6.a((Object) "passive", (Object) bestProvider) || (xj6.b(LocationUtils.HUAWEI_MODEL, Build.MANUFACTURER, true) && xj6.b(LocationUtils.HUAWEI_LOCAL_PROVIDER, bestProvider, true)))) {
                return true;
            }
            try {
                if (Settings.Secure.getInt(PortfolioApp.get.instance().getContentResolver(), "location_mode") != 0) {
                    return true;
                }
                return false;
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final boolean e() {
            Object instance = PortfolioApp.get.instance();
            String string = Settings.Secure.getString(instance.getContentResolver(), "enabled_notification_listeners");
            String str = instance.getPackageName() + ZendeskConfig.SLASH + FossilNotificationListenerService.class.getCanonicalName();
            FLogger.INSTANCE.getLocal().d("PermissionUtils", "isNotificationListenerEnabled - notificationServicePath=" + str + ", enabledNotificationListeners=" + string);
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            wg6.a((Object) string, "enabledNotificationListeners");
            return yj6.a((CharSequence) string, (CharSequence) str, false, 2, (Object) null);
        }

        @DexIgnore
        public final boolean f(Context context) {
            return a(context, "android.permission.READ_PHONE_STATE");
        }

        @DexIgnore
        public final boolean g(Context context) {
            return a(context, "android.permission.READ_SMS");
        }

        @DexIgnore
        public final boolean h(Context context) {
            return a(context, "android.permission.WRITE_EXTERNAL_STORAGE");
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }

        @DexIgnore
        public final boolean a(Context context, String... strArr) {
            wg6.b(context, "context");
            wg6.b(strArr, "perms");
            return pw6.a(context, (String[]) Arrays.copyOf(strArr, strArr.length));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final HashMap<String, Boolean> b() {
            Object instance = PortfolioApp.get.instance();
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.put(InAppPermission.ACCESS_BACKGROUND_LOCATION, Boolean.valueOf(a((Context) instance)));
            linkedHashMap.put(InAppPermission.ACCESS_FINE_LOCATION, Boolean.valueOf(b(instance)));
            linkedHashMap.put(InAppPermission.LOCATION_SERVICE, Boolean.valueOf(d()));
            linkedHashMap.put(InAppPermission.BLUETOOTH, Boolean.valueOf(c()));
            linkedHashMap.put(InAppPermission.READ_CONTACTS, Boolean.valueOf(e(instance)));
            linkedHashMap.put(InAppPermission.READ_PHONE_STATE, Boolean.valueOf(f(instance)));
            linkedHashMap.put(InAppPermission.READ_SMS, Boolean.valueOf(g(instance)));
            linkedHashMap.put(InAppPermission.NOTIFICATION_ACCESS, Boolean.valueOf(e()));
            linkedHashMap.put(InAppPermission.CAMERA, Boolean.valueOf(c(instance)));
            linkedHashMap.put(InAppPermission.WRITE_EXTERNAL_STORAGE, Boolean.valueOf(h(instance)));
            return linkedHashMap;
        }

        @DexIgnore
        public final String[] a() {
            HashMap b = b();
            Set keySet = b.keySet();
            wg6.a((Object) keySet, "permissionList.keys");
            ArrayList arrayList = new ArrayList();
            for (Object next : keySet) {
                String str = (String) next;
                wg6.a((Object) str, "it");
                if (((Boolean) he6.b(b, str)).booleanValue()) {
                    arrayList.add(next);
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }

        @DexIgnore
        public final boolean c(Context context) {
            return a(context, "android.permission.CAMERA");
        }

        @DexIgnore
        public final boolean c(Activity activity, int i) {
            wg6.b(activity, Constants.ACTIVITY);
            if (h(activity)) {
                return true;
            }
            h6.a(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
            return false;
        }

        @DexIgnore
        public final boolean d(Context context) {
            wg6.b(context, "context");
            return a(context, "android.permission.READ_CALL_LOG");
        }

        @DexIgnore
        public final boolean e(Context context) {
            wg6.b(context, "context");
            return a(context, "android.permission.READ_CONTACTS");
        }

        @DexIgnore
        public final boolean a(Activity activity, int i) {
            wg6.b(activity, Constants.ACTIVITY);
            if (a((Context) activity)) {
                return true;
            }
            h6.a(activity, new String[]{"android.permission.ACCESS_BACKGROUND_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final void a(FragmentActivity fragmentActivity) {
            wg6.b(fragmentActivity, Constants.ACTIVITY);
            fragmentActivity.startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
        }

        @DexIgnore
        public final boolean a(Context context, String str) {
            if (Build.VERSION.SDK_INT >= 23 && w6.a(context, str) != 0) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final boolean b(Activity activity, int i) {
            wg6.b(activity, Constants.ACTIVITY);
            if (b(activity)) {
                return true;
            }
            h6.a(activity, new String[]{"android.permission.ACCESS_FINE_LOCATION"}, i);
            return false;
        }

        @DexIgnore
        public final void a(Fragment fragment, int i, String... strArr) {
            wg6.b(fragment, "fragment");
            wg6.b(strArr, "perms");
            Context context = fragment.getContext();
            if (context == null) {
                wg6.a();
                throw null;
            } else if (pw6.a(context, (String[]) Arrays.copyOf(strArr, strArr.length))) {
                a((Object) fragment, i, (String[]) Arrays.copyOf(strArr, strArr.length));
            } else {
                fragment.requestPermissions(strArr, i);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final String a(String str) {
            wg6.b(str, "permission");
            switch (str.hashCode()) {
                case 385352715:
                    if (str.equals(InAppPermission.LOCATION_SERVICE)) {
                        String a = jm4.a((Context) PortfolioApp.get.instance(), 2131886980);
                        wg6.a((Object) a, "LanguageHelper.getString\u2026__TurnOnLocationServices)");
                        return a;
                    }
                    break;
                case 564039755:
                    if (str.equals(InAppPermission.ACCESS_BACKGROUND_LOCATION)) {
                        String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886630);
                        wg6.a((Object) a2, "LanguageHelper.getString\u2026BackgroundLocationAccess)");
                        return a2;
                    }
                    break;
                case 766697727:
                    if (str.equals(InAppPermission.ACCESS_FINE_LOCATION)) {
                        String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131887014);
                        wg6.a((Object) a3, "LanguageHelper.getString\u2026ng.allow_location_access)");
                        return a3;
                    }
                    break;
                case 2009556792:
                    if (str.equals(InAppPermission.NOTIFICATION_ACCESS)) {
                        String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886573);
                        wg6.a((Object) a4, "LanguageHelper.getString\u2026AllowNotificationService)");
                        return a4;
                    }
                    break;
            }
            return "";
        }

        @DexIgnore
        public final void a(Object obj, int i, String... strArr) {
            int[] iArr = new int[strArr.length];
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                iArr[i2] = 0;
            }
            pw6.a(i, strArr, iArr, new Object[]{obj});
        }
    }
}
