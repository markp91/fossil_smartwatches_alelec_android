package com.fossil;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.contact.ContactProviderImpl;
import com.fossil.wearables.fsl.contact.EmailAddress;
import com.fossil.wearables.fsl.contact.PhoneNumber;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.misc.TransactionManager;
import com.j256.ormlite.table.TableUtils;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.helper.DeviceHelper;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vn4 extends ContactProviderImpl {
    @DexIgnore
    public static /* final */ String a; // = "vn4";

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements UpgradeCommand {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void execute(SQLiteDatabase sQLiteDatabase) {
            FLogger.INSTANCE.getLocal().d(vn4.a, " ---- UPGRADE DB ENTOURAGE, table CONTACTGROUP");
            sQLiteDatabase.execSQL("ALTER TABLE contactgroup ADD COLUMN deviceFamily int");
            FLogger.INSTANCE.getLocal().d(vn4.a, " ---- UPGRADE DB ENTOURAGE, table CONTACTGROUP SUCCESS");
            StringBuilder sb = new StringBuilder();
            String e = PortfolioApp.T.e();
            if (!TextUtils.isEmpty(e)) {
                sb.append("UPDATE ");
                sb.append("contactgroup");
                sb.append(" SET deviceFamily = ");
                sb.append(DeviceIdentityUtils.getDeviceFamily(e).ordinal());
                sQLiteDatabase.execSQL(sb.toString());
                return;
            }
            vn4.this.removeAllContactGroups();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements UpgradeCommand {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0189 A[Catch:{ Exception -> 0x022b }] */
        public void execute(SQLiteDatabase sQLiteDatabase) {
            String str;
            String str2;
            String str3;
            String str4;
            int i;
            int a2;
            SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
            try {
                FLogger.INSTANCE.getLocal().d(vn4.a, "Inside .doInBackground upgrade contact");
                List<u8<Integer, Integer>> a3 = vn4.this.a(sQLiteDatabase2);
                Cursor query = sQLiteDatabase.query(true, "contactgroup", new String[]{"id", "color", "name", "haptic", "timestamp", "enabled", "deviceFamily"}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
                ArrayList<ContactGroup> arrayList = new ArrayList<>();
                int i2 = -1;
                String e = PortfolioApp.T.e();
                if (!TextUtils.isEmpty(e)) {
                    i2 = DeviceHelper.o.a(e).getValue();
                }
                String str5 = "deviceFamily";
                String str6 = "enabled";
                String str7 = "timestamp";
                String str8 = "name";
                if (query != null && i2 > 0) {
                    if (DeviceHelper.o.g(e)) {
                        query.moveToFirst();
                        while (!query.isAfterLast()) {
                            String string = query.getString(query.getColumnIndex("color"));
                            String string2 = query.getString(query.getColumnIndex(str8));
                            String string3 = query.getString(query.getColumnIndex("haptic"));
                            String str9 = str8;
                            int i3 = query.getInt(query.getColumnIndex(str7));
                            String str10 = str7;
                            int i4 = query.getInt(query.getColumnIndex(str6));
                            String str11 = str6;
                            int i5 = query.getInt(query.getColumnIndex(str5));
                            String str12 = str5;
                            int i6 = query.getInt(query.getColumnIndex("id"));
                            if (i5 == i2) {
                                i = i2;
                                ContactGroup contactGroup = new ContactGroup();
                                contactGroup.setColor(string);
                                contactGroup.setName(string2);
                                contactGroup.setHaptic(string3);
                                contactGroup.setTimestamp((long) i3);
                                boolean z = true;
                                if (i4 != 1) {
                                    z = false;
                                }
                                contactGroup.setEnabled(z);
                                contactGroup.setDeviceFamily(i5);
                                contactGroup.setDbRowId(i6);
                                if (!a3.isEmpty() && (a2 = vn4.this.a(a3, contactGroup.getDbRowId())) > 0) {
                                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                                    String str13 = vn4.a;
                                    local.d(str13, "Find contact, contactId=" + a2);
                                    wn4 a4 = zm4.p.a().f().a(String.valueOf(a2), MFDeviceFamily.fromInt(i5).toString());
                                    if (a4 != null) {
                                        contactGroup.setHour(a4.d());
                                        contactGroup.setVibrationOnly(a4.f());
                                    }
                                }
                                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                                String str14 = vn4.a;
                                local2.d(str14, "Add contact=" + contactGroup);
                                if (!contactGroup.isVibrationOnly()) {
                                    arrayList.add(contactGroup);
                                }
                            } else {
                                i = i2;
                            }
                            query.moveToNext();
                            str8 = str9;
                            str7 = str10;
                            str6 = str11;
                            str5 = str12;
                            i2 = i;
                        }
                        str = str5;
                        str2 = str6;
                        str3 = str7;
                        str4 = str8;
                        query.close();
                        sQLiteDatabase2.execSQL("CREATE TABLE contactgroup_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, deviceFamily INTEGER, hour INTEGER, isVibrationOnly INTEGER);");
                        if (!arrayList.isEmpty()) {
                            for (ContactGroup contactGroup2 : arrayList) {
                                ContentValues contentValues = new ContentValues();
                                contentValues.put("color", contactGroup2.getColor());
                                contentValues.put("haptic", contactGroup2.getHaptic());
                                String str15 = str3;
                                contentValues.put(str15, Long.valueOf(contactGroup2.getTimestamp()));
                                String str16 = str4;
                                contentValues.put(str16, contactGroup2.getName());
                                String str17 = str2;
                                contentValues.put(str17, Boolean.valueOf(contactGroup2.isEnabled()));
                                String str18 = str;
                                contentValues.put(str18, Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                                contentValues.put("hour", Integer.valueOf(contactGroup2.getHour()));
                                contentValues.put("isVibrationOnly", Boolean.valueOf(contactGroup2.isVibrationOnly()));
                                contentValues.put("id", Integer.valueOf(contactGroup2.getDbRowId()));
                                sQLiteDatabase2.insert("contactgroup_copy", (String) null, contentValues);
                                str3 = str15;
                                str4 = str16;
                                str2 = str17;
                                str = str18;
                            }
                        }
                        sQLiteDatabase2.execSQL("DROP TABLE contactgroup;");
                        sQLiteDatabase2.execSQL("ALTER TABLE contactgroup_copy RENAME TO contactgroup;");
                        FLogger.INSTANCE.getLocal().d(vn4.a, "Migration complete");
                    }
                }
                str = str5;
                str2 = str6;
                str3 = str7;
                str4 = str8;
                sQLiteDatabase2.execSQL("CREATE TABLE contactgroup_copy (id INTEGER PRIMARY KEY AUTOINCREMENT, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, deviceFamily INTEGER, hour INTEGER, isVibrationOnly INTEGER);");
                if (!arrayList.isEmpty()) {
                }
                sQLiteDatabase2.execSQL("DROP TABLE contactgroup;");
                sQLiteDatabase2.execSQL("ALTER TABLE contactgroup_copy RENAME TO contactgroup;");
                FLogger.INSTANCE.getLocal().d(vn4.a, "Migration complete");
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str19 = vn4.a;
                local3.e(str19, "Error inside " + vn4.a + ".upgrade - e=" + e2);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements UpgradeCommand {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Callable<Boolean> {
            @DexIgnore
            public /* final */ /* synthetic */ SQLiteDatabase a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public a(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            public Boolean call() {
                try {
                    this.a.execSQL("CREATE TABLE unique_contact (id INTEGER PRIMARY KEY AUTOINCREMENT, firstName VARCHAR, lastName VARCHAR, contactId BIGINT, companyName VARCHAR, photoThumbUri VARCHAR, useCall INTEGER, useEmail INTEGER, useSms INTEGER, contact_group_id INTEGER);");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = vn4.a;
                    local.d(str, "Create new contact unique table complete, uniqueContactListSize=" + this.b.size());
                    for (Contact contact : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", Integer.valueOf(contact.getDbRowId()));
                        contentValues.put("firstname", contact.getFirstName());
                        contentValues.put("lastName", contact.getLastName());
                        contentValues.put("contactId", Integer.valueOf(contact.getContactId()));
                        contentValues.put("companyName", contact.getCompanyName());
                        contentValues.put("photoThumbUri", contact.getPhotoThumbUri());
                        contentValues.put("useCall", Boolean.valueOf(contact.isUseCall()));
                        contentValues.put("useEmail", Boolean.valueOf(contact.isUseEmail()));
                        contentValues.put("useSms", Boolean.valueOf(contact.isUseSms()));
                        contentValues.put("contact_group_id", Integer.valueOf(contact.getContactGroup().getDbRowId()));
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = vn4.a;
                        local2.d(str2, "Put contact with contact id=" + contact.getDbRowId() + ", contactGroupId=" + contact.getContactGroup().getDbRowId());
                        this.a.insert("unique_contact", (String) null, contentValues);
                    }
                    this.a.execSQL("DROP TABLE contact;");
                    this.a.execSQL("ALTER TABLE unique_contact RENAME TO contact;");
                    FLogger.INSTANCE.getLocal().d(vn4.a, "Done remove duplicate for contact table");
                    return true;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = vn4.a;
                    local3.e(str3, "Exception when execute transaction put contact e=" + e);
                    return false;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Callable<Boolean> {
            @DexIgnore
            public /* final */ /* synthetic */ SQLiteDatabase a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public b(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            public Boolean call() {
                try {
                    this.a.execSQL("CREATE TABLE unique_contactgroup (id INTEGER PRIMARY KEY AUTOINCREMENT, color VARCHAR, haptic VARCHAR, timestamp BIGINT, enabled INTEGER, name VARCHAR, deviceFamily INTEGER, hour INTEGER, isVibrationOnly INTEGER);");
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = vn4.a;
                    local.d(str, "Create new contact group unique table complete, uniqueContactGroupListSize=" + this.b.size());
                    for (ContactGroup contactGroup : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("color", contactGroup.getColor());
                        contentValues.put("haptic", contactGroup.getHaptic());
                        contentValues.put("timestamp", Long.valueOf(contactGroup.getTimestamp()));
                        contentValues.put("name", contactGroup.getName());
                        contentValues.put("enabled", Boolean.valueOf(contactGroup.isEnabled()));
                        contentValues.put("deviceFamily", Integer.valueOf(MFDeviceFamily.DEVICE_FAMILY_SAM.getValue()));
                        contentValues.put("hour", Integer.valueOf(contactGroup.getHour()));
                        contentValues.put("isVibrationOnly", Boolean.valueOf(contactGroup.isVibrationOnly()));
                        contentValues.put("id", Integer.valueOf(contactGroup.getDbRowId()));
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str2 = vn4.a;
                        local2.d(str2, "Put contact group with contact group id=" + contactGroup.getDbRowId());
                        this.a.insert("unique_contactgroup", (String) null, contentValues);
                    }
                    this.a.execSQL("DROP TABLE contactgroup;");
                    this.a.execSQL("ALTER TABLE unique_contactgroup RENAME TO contactgroup;");
                    return true;
                } catch (Exception e) {
                    ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                    String str3 = vn4.a;
                    local3.e(str3, "Exception when execute put unique contact group, e=" + e);
                    return false;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.vn4$c$c")
        /* renamed from: com.fossil.vn4$c$c  reason: collision with other inner class name */
        public class C0045c implements Callable<Boolean> {
            @DexIgnore
            public /* final */ /* synthetic */ SQLiteDatabase a;
            @DexIgnore
            public /* final */ /* synthetic */ List b;

            @DexIgnore
            public C0045c(c cVar, SQLiteDatabase sQLiteDatabase, List list) {
                this.a = sQLiteDatabase;
                this.b = list;
            }

            @DexIgnore
            public Boolean call() {
                try {
                    this.a.execSQL("CREATE TABLE unique_phone_number (id INTEGER PRIMARY KEY AUTOINCREMENT, phone_number_id INTEGER, number VARCHAR);");
                    for (PhoneNumber phoneNumber : this.b) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("id", Integer.valueOf(phoneNumber.getDbRowId()));
                        contentValues.put("phone_number_id", Integer.valueOf(phoneNumber.getContact().getDbRowId()));
                        contentValues.put("number", phoneNumber.getNumber());
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = vn4.a;
                        local.d(str, "Put phone number with contact id=" + phoneNumber.getContact().getDbRowId() + ", id=" + phoneNumber.getDbRowId());
                        this.a.insert("unique_phone_number", (String) null, contentValues);
                    }
                    this.a.execSQL("DROP TABLE phonenumber;");
                    this.a.execSQL("ALTER TABLE unique_phone_number RENAME TO phonenumber;");
                    return true;
                } catch (Exception e) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    String str2 = vn4.a;
                    local2.e(str2, "Exception when execute insert phone number, e=" + e);
                    return false;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class d implements Callable<Boolean> {
            @DexIgnore
            public d() {
            }

            @DexIgnore
            public Boolean call() {
                try {
                    TableUtils.clearTable(vn4.this.databaseHelper.getConnectionSource(), EmailAddress.class);
                    return true;
                } catch (Exception e) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str = vn4.a;
                    local.e(str, "Exception when drop email address table, e=" + e);
                    return false;
                }
            }
        }

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void execute(SQLiteDatabase sQLiteDatabase) {
            Cursor query;
            Cursor rawQuery;
            Cursor rawQuery2;
            SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
            FLogger.INSTANCE.getLocal().d(vn4.a, "------- Enter upgrade database version 5 --------");
            try {
                FLogger.INSTANCE.getLocal().d(vn4.a, "------- Start upgrade database version 5 --------");
                query = sQLiteDatabase.query(true, "contact", new String[]{"id", "firstName", "lastName", "contactId", "companyName", "photoThumbUri", "useCall", "useEmail", "useSms", "contact_group_id"}, (String) null, (String[]) null, "contactId", (String) null, (String) null, (String) null);
                FLogger.INSTANCE.getLocal().d(vn4.a, "Query cursor for contact success");
                ArrayList arrayList = new ArrayList();
                if (query != null) {
                    query.moveToFirst();
                    while (!query.isAfterLast()) {
                        try {
                            int i = query.getInt(query.getColumnIndex("id"));
                            String string = query.getString(query.getColumnIndex("firstName"));
                            String string2 = query.getString(query.getColumnIndex("lastName"));
                            int i2 = query.getInt(query.getColumnIndex("contactId"));
                            String string3 = query.getString(query.getColumnIndex("companyName"));
                            String string4 = query.getString(query.getColumnIndex("photoThumbUri"));
                            int i3 = query.getInt(query.getColumnIndex("useCall"));
                            int i4 = query.getInt(query.getColumnIndex("useEmail"));
                            int i5 = query.getInt(query.getColumnIndex("useSms"));
                            int i6 = query.getInt(query.getColumnIndex("contact_group_id"));
                            Contact contact = new Contact();
                            contact.setContactId(i2);
                            contact.setLastName(string2);
                            contact.setFirstName(string);
                            contact.setCompanyName(string3);
                            contact.setPhotoThumbUri(string4);
                            contact.setUseCall(i3 == 1);
                            contact.setUseEmail(i4 == 1);
                            contact.setUseSms(i5 == 1);
                            ContactGroup contactGroup = new ContactGroup();
                            contactGroup.setDbRowId(i6);
                            contact.setContactGroup(contactGroup);
                            contact.setDbRowId(i);
                            arrayList.add(contact);
                            query.moveToNext();
                        } catch (Exception e) {
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String str = vn4.a;
                            local.e(str, "Exception when query cursor contact, e=" + e);
                            vn4.this.g();
                            query.close();
                            return;
                        }
                    }
                    query.close();
                }
                if (!arrayList.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(vn4.this.databaseHelper.getConnectionSource(), new a(this, sQLiteDatabase2, arrayList));
                    } catch (Exception e2) {
                        e2.printStackTrace();
                        vn4.this.g();
                        return;
                    }
                }
                rawQuery = sQLiteDatabase2.rawQuery("select contactgroup.id, contactgroup.color, contactgroup.deviceFamily, contactgroup.enabled, contactgroup.haptic, contactgroup.hour, contactgroup.isVibrationOnly, contactgroup.name, contactgroup.timestamp from contactgroup,contact where contact.contact_group_id = contactgroup.id", new String[0]);
                FLogger.INSTANCE.getLocal().d(vn4.a, "Query cursor for contact group success");
                ArrayList arrayList2 = new ArrayList();
                if (rawQuery != null) {
                    rawQuery.moveToFirst();
                    while (!rawQuery.isAfterLast()) {
                        try {
                            String string5 = rawQuery.getString(rawQuery.getColumnIndex("color"));
                            String string6 = rawQuery.getString(rawQuery.getColumnIndex("name"));
                            String string7 = rawQuery.getString(rawQuery.getColumnIndex("haptic"));
                            int i7 = rawQuery.getInt(rawQuery.getColumnIndex("timestamp"));
                            int i8 = rawQuery.getInt(rawQuery.getColumnIndex("enabled"));
                            int i9 = rawQuery.getInt(rawQuery.getColumnIndex("deviceFamily"));
                            int i10 = rawQuery.getInt(rawQuery.getColumnIndex("id"));
                            int i11 = rawQuery.getInt(rawQuery.getColumnIndex("hour"));
                            int i12 = rawQuery.getInt(rawQuery.getColumnIndex("isVibrationOnly"));
                            ContactGroup contactGroup2 = new ContactGroup();
                            contactGroup2.setColor(string5);
                            contactGroup2.setName(string6);
                            contactGroup2.setHaptic(string7);
                            contactGroup2.setTimestamp((long) i7);
                            contactGroup2.setEnabled(i8 == 1);
                            contactGroup2.setDeviceFamily(i9);
                            contactGroup2.setDbRowId(i10);
                            contactGroup2.setHour(i11);
                            contactGroup2.setVibrationOnly(i12 == 1);
                            arrayList2.add(contactGroup2);
                            rawQuery.moveToNext();
                        } catch (Exception e3) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String str2 = vn4.a;
                            local2.e(str2, "Exception when query cursor contact group, e=" + e3);
                            vn4.this.g();
                            rawQuery.close();
                            return;
                        }
                    }
                    rawQuery.close();
                }
                if (!arrayList2.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(vn4.this.databaseHelper.getConnectionSource(), new b(this, sQLiteDatabase2, arrayList2));
                    } catch (Exception e4) {
                        e4.printStackTrace();
                        vn4.this.g();
                        return;
                    }
                }
                rawQuery2 = sQLiteDatabase2.rawQuery("select phonenumber.id, phonenumber.number, phonenumber.phone_number_id from phonenumber,contact where contact.id = phonenumber.phone_number_id ", new String[0]);
                ArrayList arrayList3 = new ArrayList();
                FLogger.INSTANCE.getLocal().d(vn4.a, "Query cursor for phone number success");
                if (rawQuery2 != null) {
                    rawQuery2.moveToFirst();
                    while (!rawQuery2.isAfterLast()) {
                        try {
                            int i13 = rawQuery2.getInt(rawQuery2.getColumnIndex("phone_number_id"));
                            String string8 = rawQuery2.getString(rawQuery2.getColumnIndex("number"));
                            int i14 = rawQuery2.getInt(rawQuery2.getColumnIndex("id"));
                            Contact contact2 = new Contact();
                            contact2.setDbRowId(i13);
                            PhoneNumber phoneNumber = new PhoneNumber();
                            phoneNumber.setDbRowId(i14);
                            phoneNumber.setNumber(string8);
                            phoneNumber.setContact(contact2);
                            arrayList3.add(phoneNumber);
                            rawQuery2.moveToNext();
                        } catch (Exception e5) {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str3 = vn4.a;
                            local3.e(str3, "Exception when query cursor phone number, e=" + e5);
                            vn4.this.g();
                            rawQuery2.close();
                            return;
                        }
                    }
                    rawQuery2.close();
                }
                if (!arrayList3.isEmpty()) {
                    try {
                        TransactionManager.callInTransaction(vn4.this.databaseHelper.getConnectionSource(), new C0045c(this, sQLiteDatabase2, arrayList3));
                    } catch (Exception e6) {
                        e6.printStackTrace();
                        vn4.this.g();
                        return;
                    }
                }
                try {
                    TransactionManager.callInTransaction(vn4.this.databaseHelper.getConnectionSource(), new d());
                } catch (Exception e7) {
                    e7.printStackTrace();
                }
                FLogger.INSTANCE.getLocal().d(vn4.a, "------- Done upgrade database version 5 --------");
            } catch (Exception e8) {
                e8.printStackTrace();
                vn4.this.g();
            } catch (Throwable th) {
                query.close();
                throw th;
            }
            FLogger.INSTANCE.getLocal().d(vn4.a, "------- Exit upgrade database version 5 --------");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Callable<Boolean> {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public Boolean call() {
            try {
                TableUtils.clearTable(vn4.this.databaseHelper.getConnectionSource(), ContactGroup.class);
                TableUtils.clearTable(vn4.this.databaseHelper.getConnectionSource(), Contact.class);
                TableUtils.clearTable(vn4.this.databaseHelper.getConnectionSource(), PhoneNumber.class);
                TableUtils.clearTable(vn4.this.databaseHelper.getConnectionSource(), EmailAddress.class);
                return true;
            } catch (Exception e) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vn4.a;
                local.e(str, "Inside .forceClearAllTables exception when drop all table, e=" + e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder a;

        @DexIgnore
        public e(StringBuilder sb) {
            this.a = sb;
        }

        @DexIgnore
        public Boolean call() {
            try {
                Dao m = vn4.this.getContactDao();
                m.queryRaw("DELETE FROM contact WHERE contactId NOT IN " + this.a, new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vn4.a;
                local.d(str, ".Inside step 4 - remove redundant contact, executed sql, IN condition=DELETE FROM contact WHERE contactId NOT IN " + this.a);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = vn4.a;
                local2.d(str2, ".Inside step 4 - remove redundant contact, exception=" + e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder a;

        @DexIgnore
        public f(StringBuilder sb) {
            this.a = sb;
        }

        @DexIgnore
        public Boolean call() {
            try {
                Dao c = vn4.this.getContactGroupDao();
                c.queryRaw("DELETE FROM contactgroup WHERE id NOT IN " + this.a, new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vn4.a;
                local.d(str, ".Inside step 5 - remove redundant contact group, executed sql=DELETE FROM contactgroup WHERE id NOT IN " + this.a);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = vn4.a;
                local2.d(str2, ".Inside step 5 - remove redundant contact group, Exception outside, exception=" + e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ StringBuilder a;

        @DexIgnore
        public g(StringBuilder sb) {
            this.a = sb;
        }

        @DexIgnore
        public Boolean call() {
            try {
                Dao d = vn4.this.getPhoneNumberDao();
                d.queryRaw("DELETE FROM phonenumber WHERE phone_number_id NOT IN " + this.a, new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vn4.a;
                local.d(str, ".Inside step 6 - remove phone number, executed sql=DELETE FROM phonenumber WHERE phone_number_id NOT IN " + this.a);
                return true;
            } catch (Exception e) {
                e.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = vn4.a;
                local2.d(str2, ".Inside step 6 - remove phone number, exception=" + e);
                return false;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h implements Callable<Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public h(int i) {
            this.a = i;
        }

        @DexIgnore
        public Boolean call() {
            try {
                Dao e = vn4.this.getPhoneNumberDao();
                e.queryRaw("DELETE FROM phonenumber WHERE phone_number_id = '" + this.a + '\'', new String[0]);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = vn4.a;
                local.d(str, ".Inside removePhoneNumberByContactGroupId, executed sql=DELETE FROM phonenumber WHERE phone_number_id = '" + this.a + '\'');
                return true;
            } catch (Exception e2) {
                e2.printStackTrace();
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = vn4.a;
                local2.d(str2, ".Inside removePhoneNumberByContactGroupId, exception=" + e2);
                return false;
            }
        }
    }

    @DexIgnore
    public vn4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    @SuppressLint({"UseSparseArrays"})
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        Map<Integer, UpgradeCommand> dbUpgrades = vn4.super.getDbUpgrades();
        if (dbUpgrades == null) {
            dbUpgrades = new HashMap<>();
        }
        dbUpgrades.put(2, new a());
        dbUpgrades.put(4, new b());
        dbUpgrades.put(5, new c());
        return dbUpgrades;
    }

    @DexIgnore
    public int a(List<u8<Integer, Integer>> list, int i) {
        Iterator<u8<Integer, Integer>> it = list.iterator();
        while (it.hasNext()) {
            u8 u8Var = (u8) it.next();
            if (((Integer) u8Var.a).intValue() == i) {
                return ((Integer) u8Var.b).intValue();
            }
        }
        return -1;
    }

    @DexIgnore
    public List<Integer> b(List<Integer> list) {
        ArrayList arrayList = new ArrayList();
        try {
            StringBuilder f2 = f(list);
            Dao contactDao = getContactDao();
            for (String[] strArr : contactDao.queryRaw("SELECT contact_group_id FROM contact WHERE contactId IN " + f2, new String[0]).getResults()) {
                int parseInt = Integer.parseInt(strArr[0]);
                arrayList.add(Integer.valueOf(parseInt));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, ".Inside step 3 - get contact group Id, contactGroupId=" + parseInt);
            }
        } catch (SQLiteException | SQLException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local2.d(str2, ".Inside step 3 , SQLiteException, ex=" + e2);
            e2.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    public void c(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new e(f(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, ".Inside step 4 - remove redundant contact, Exception outside, exception=" + e2);
        }
    }

    @DexIgnore
    public void d(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new g(f(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, ".Inside step 6 - remove phone number, exception=" + e2);
        }
    }

    @DexIgnore
    public void e(List<Integer> list) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new f(f(list)));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, ".Inside step 5 - remove redundant contact group, Exception, exception=" + e2);
        }
    }

    @DexIgnore
    public final StringBuilder f(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        for (Integer intValue : list) {
            int intValue2 = intValue.intValue();
            if (sb.length() > 1) {
                sb.append(',');
            }
            sb.append(intValue2);
        }
        sb.append(')');
        return sb;
    }

    @DexIgnore
    public void g() {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new d());
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public List<Integer> h() {
        ArrayList arrayList = new ArrayList();
        try {
            for (String[] strArr : getContactDao().queryRaw("SELECT contactId FROM contact", new String[0]).getResults()) {
                int parseInt = Integer.parseInt(strArr[0]);
                arrayList.add(Integer.valueOf(parseInt));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, ".Inside step 1 - get Id on local DB, contactId=" + parseInt);
            }
        } catch (SQLiteException | SQLException e2) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = a;
            local2.d(str2, ".Inside step 1 , SQLiteException, ex=" + e2);
            e2.printStackTrace();
        }
        return arrayList;
    }

    @DexIgnore
    public List<u8<Integer, Integer>> a(SQLiteDatabase sQLiteDatabase) {
        Cursor query = sQLiteDatabase.query(true, "contact", new String[]{"contactId", "contact_group_id"}, (String) null, (String[]) null, (String) null, (String) null, (String) null, (String) null);
        ArrayList arrayList = new ArrayList();
        if (query != null) {
            query.moveToFirst();
            while (!query.isAfterLast()) {
                int i = query.getInt(query.getColumnIndex("contactId"));
                int i2 = query.getInt(query.getColumnIndex("contact_group_id"));
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = a;
                local.d(str, "Add pair contactGroupid=" + i2 + ", contacId=" + i);
                arrayList.add(new u8(Integer.valueOf(i2), Integer.valueOf(i)));
                query.moveToNext();
            }
            query.close();
        }
        return arrayList;
    }

    @DexIgnore
    public void b(int i) {
        try {
            TransactionManager.callInTransaction(this.databaseHelper.getConnectionSource(), new h(i));
        } catch (Exception e2) {
            e2.printStackTrace();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, ".Inside removePhoneNumberByContactGroupId, exception=" + e2);
        }
    }

    @DexIgnore
    public Contact a(int i) {
        try {
            return (Contact) getContactDao().queryForEq("contactId", Integer.valueOf(i)).get(0);
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = a;
            local.d(str, "getContactById, ex=" + e2);
            e2.printStackTrace();
            return null;
        }
    }
}
