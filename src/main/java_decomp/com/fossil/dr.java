package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import com.fossil.gs;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dr {
    @DexIgnore
    public /* final */ lv a; // = new lv(this.j);
    @DexIgnore
    public /* final */ az b; // = new az();
    @DexIgnore
    public /* final */ ez c; // = new ez();
    @DexIgnore
    public /* final */ fz d; // = new fz();
    @DexIgnore
    public /* final */ hs e; // = new hs();
    @DexIgnore
    public /* final */ dy f; // = new dy();
    @DexIgnore
    public /* final */ bz g; // = new bz();
    @DexIgnore
    public /* final */ dz h; // = new dz();
    @DexIgnore
    public /* final */ cz i; // = new cz();
    @DexIgnore
    public /* final */ v8<List<Throwable>> j; // = s00.b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends RuntimeException {
        @DexIgnore
        public a(String str) {
            super(str);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends a {
        @DexIgnore
        public b() {
            super("Failed to find image header parser.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends a {
        @DexIgnore
        public c(Object obj) {
            super("Failed to find any ModelLoaders registered for model class: " + obj.getClass());
        }

        @DexIgnore
        public <M> c(M m, List<jv<M, ?>> list) {
            super("Found ModelLoaders for model class: " + list + ", but none that handle this specific model instance: " + m);
        }

        @DexIgnore
        public c(Class<?> cls, Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends a {
        @DexIgnore
        public d(Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends a {
        @DexIgnore
        public e(Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    @DexIgnore
    public dr() {
        a((List<String>) Arrays.asList(new String[]{"Gif", "Bitmap", "BitmapDrawable"}));
    }

    @DexIgnore
    public <Data> dr a(Class<Data> cls, sr<Data> srVar) {
        this.b.a(cls, srVar);
        return this;
    }

    @DexIgnore
    public <Data, TResource, Transcode> pt<Data, TResource, Transcode> b(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        pt<Data, TResource, Transcode> a2 = this.i.a(cls, cls2, cls3);
        if (this.i.a(a2)) {
            return null;
        }
        if (a2 == null) {
            List<et<Data, TResource, Transcode>> a3 = a(cls, cls2, cls3);
            if (a3.isEmpty()) {
                a2 = null;
            } else {
                a2 = new pt<>(cls, cls2, cls3, a3, this.j);
            }
            this.i.a(cls, cls2, cls3, a2);
        }
        return a2;
    }

    @DexIgnore
    public <Model, TResource, Transcode> List<Class<?>> c(Class<Model> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        List<Class<?>> a2 = this.h.a(cls, cls2, cls3);
        if (a2 == null) {
            a2 = new ArrayList<>();
            for (Class<?> b2 : this.a.a((Class<?>) cls)) {
                for (Class next : this.c.b(b2, cls2)) {
                    if (!this.f.b(next, cls3).isEmpty() && !a2.contains(next)) {
                        a2.add(next);
                    }
                }
            }
            this.h.a(cls, cls2, cls3, Collections.unmodifiableList(a2));
        }
        return a2;
    }

    @DexIgnore
    public <Data, TResource> dr a(Class<Data> cls, Class<TResource> cls2, zr<Data, TResource> zrVar) {
        a("legacy_append", cls, cls2, zrVar);
        return this;
    }

    @DexIgnore
    public <Data, TResource> dr a(String str, Class<Data> cls, Class<TResource> cls2, zr<Data, TResource> zrVar) {
        this.c.a(str, zrVar, cls, cls2);
        return this;
    }

    @DexIgnore
    public final dr a(List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.c.a((List<String>) arrayList);
        return this;
    }

    @DexIgnore
    public boolean b(rt<?> rtVar) {
        return this.d.a(rtVar.c()) != null;
    }

    @DexIgnore
    public <TResource> dr a(Class<TResource> cls, as<TResource> asVar) {
        this.d.a(cls, asVar);
        return this;
    }

    @DexIgnore
    public <X> gs<X> b(X x) {
        return this.e.a(x);
    }

    @DexIgnore
    public dr a(gs.a<?> aVar) {
        this.e.a(aVar);
        return this;
    }

    @DexIgnore
    public <TResource, Transcode> dr a(Class<TResource> cls, Class<Transcode> cls2, cy<TResource, Transcode> cyVar) {
        this.f.a(cls, cls2, cyVar);
        return this;
    }

    @DexIgnore
    public dr a(ImageHeaderParser imageHeaderParser) {
        this.g.a(imageHeaderParser);
        return this;
    }

    @DexIgnore
    public <Model, Data> dr a(Class<Model> cls, Class<Data> cls2, kv<Model, Data> kvVar) {
        this.a.a(cls, cls2, kvVar);
        return this;
    }

    @DexIgnore
    public final <Data, TResource, Transcode> List<et<Data, TResource, Transcode>> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class next : this.c.b(cls, cls2)) {
            for (Class next2 : this.f.b(next, cls3)) {
                arrayList.add(new et(cls, next, next2, this.c.a(cls, next), this.f.a(next, next2), this.j));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public <X> sr<X> c(X x) throws e {
        sr<X> a2 = this.b.a(x.getClass());
        if (a2 != null) {
            return a2;
        }
        throw new e(x.getClass());
    }

    @DexIgnore
    public <X> as<X> a(rt<X> rtVar) throws d {
        as<X> a2 = this.d.a(rtVar.c());
        if (a2 != null) {
            return a2;
        }
        throw new d(rtVar.c());
    }

    @DexIgnore
    public <Model> List<jv<Model, ?>> a(Model model) {
        return this.a.a(model);
    }

    @DexIgnore
    public List<ImageHeaderParser> a() {
        List<ImageHeaderParser> a2 = this.g.a();
        if (!a2.isEmpty()) {
            return a2;
        }
        throw new b();
    }
}
