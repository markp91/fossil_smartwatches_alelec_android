package com.fossil;

import com.fossil.bf;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ve<Key, Value> extends xe<Key, Value> {
    @DexIgnore
    public abstract void dispatchLoadAfter(int i, Value value, int i2, Executor executor, bf.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadBefore(int i, Value value, int i2, Executor executor, bf.a<Value> aVar);

    @DexIgnore
    public abstract void dispatchLoadInitial(Key key, int i, int i2, boolean z, Executor executor, bf.a<Value> aVar);

    @DexIgnore
    public abstract Key getKey(int i, Value value);

    @DexIgnore
    public boolean isContiguous() {
        return true;
    }

    @DexIgnore
    public boolean supportsPageDropping() {
        return true;
    }
}
