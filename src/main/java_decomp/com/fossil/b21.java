package com.fossil;

import com.fossil.q40;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b21 extends xg6 implements hg6<cd6, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ii1 a;
    @DexIgnore
    public /* final */ /* synthetic */ em0 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public b21(ii1 ii1, em0 em0) {
        super(1);
        this.a = ii1;
        this.b = em0;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        cd6 cd6 = (cd6) obj;
        ii1 ii1 = this.a;
        i90 i90 = new i90(this.b.b);
        q40.b v = ii1.v();
        if (v != null) {
            v.onEventReceived(ii1, i90);
        }
        return cd6.a;
    }
}
