package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wl3<K, V> extends uk3<K, V> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ K key;
    @DexIgnore
    public /* final */ V value;

    @DexIgnore
    public wl3(K k, V v) {
        this.key = k;
        this.value = v;
    }

    @DexIgnore
    public final K getKey() {
        return this.key;
    }

    @DexIgnore
    public final V getValue() {
        return this.value;
    }

    @DexIgnore
    public final V setValue(V v) {
        throw new UnsupportedOperationException();
    }
}
