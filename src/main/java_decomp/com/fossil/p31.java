package com.fossil;

import android.os.Parcel;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p31 extends em0 {
    @DexIgnore
    public static /* final */ t11 CREATOR; // = new t11((qg6) null);
    @DexIgnore
    public /* final */ fd0 c;
    @DexIgnore
    public /* final */ ed0 d;
    @DexIgnore
    public /* final */ gd0 e;
    @DexIgnore
    public /* final */ byte[] f;

    @DexIgnore
    public p31(byte b, fd0 fd0, ed0 ed0, gd0 gd0, byte[] bArr) {
        super(xh0.ENCRYPTED_DATA, b);
        this.c = fd0;
        this.d = ed0;
        this.e = gd0;
        this.f = bArr;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(super.a(), bm0.DATA_TYPE, (Object) cw0.a((Enum<?>) this.c)), bm0.ENCRYPT_METHOD, (Object) cw0.a((Enum<?>) this.d)), bm0.KEY_TYPE, (Object) cw0.a((Enum<?>) this.e)), bm0.RAW_DATA_CRC, (Object) Long.valueOf(h51.a.a(this.f, q11.CRC32)));
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.c.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.d.a());
        }
        if (parcel != null) {
            parcel.writeByte(this.e.a());
        }
        if (parcel != null) {
            parcel.writeByteArray(this.f);
        }
    }

    @DexIgnore
    public p31(Parcel parcel) {
        super(parcel);
        fd0 a = fd0.c.a(parcel.readByte());
        if (a != null) {
            this.c = a;
            ed0 a2 = ed0.c.a(parcel.readByte());
            if (a2 != null) {
                this.d = a2;
                gd0 a3 = gd0.c.a(parcel.readByte());
                if (a3 != null) {
                    this.e = a3;
                    byte[] createByteArray = parcel.createByteArray();
                    this.f = createByteArray == null ? new byte[0] : createByteArray;
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }
}
