package com.fossil;

import com.fossil.zf;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uu4 extends zf.d<ActivitySummary> {
    @DexIgnore
    /* renamed from: a */
    public boolean areContentsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        wg6.b(activitySummary, "oldItem");
        wg6.b(activitySummary2, "newItem");
        return wg6.a((Object) activitySummary, (Object) activitySummary2);
    }

    @DexIgnore
    /* renamed from: b */
    public boolean areItemsTheSame(ActivitySummary activitySummary, ActivitySummary activitySummary2) {
        wg6.b(activitySummary, "oldItem");
        wg6.b(activitySummary2, "newItem");
        return activitySummary.getDay() == activitySummary2.getDay() && activitySummary.getMonth() == activitySummary2.getMonth() && activitySummary.getYear() == activitySummary2.getYear();
    }
}
