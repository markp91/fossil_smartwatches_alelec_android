package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu0 extends ok0 {
    @DexIgnore
    public mw0 j; // = mw0.BOND_NONE;

    @DexIgnore
    public vu0(at0 at0) {
        super(hm0.CREATE_BOND, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.b();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r2 = ((com.fossil.dt0) r2).b;
     */
    @DexIgnore
    public boolean b(p51 p51) {
        mw0 mw0;
        return (p51 instanceof dt0) && (mw0 == mw0.BONDED || mw0 == mw0.BOND_NONE);
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.i;
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        c(p51);
        t31 t31 = p51.a;
        if (t31.a != x11.SUCCESS) {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        } else if (this.j == mw0.BONDED) {
            ch0 = ch0.a(this.d, (hm0) null, lf0.SUCCESS, (t31) null, 5);
        } else {
            ch0 = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        }
        this.d = ch0;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.j = ((dt0) p51).b;
    }
}
