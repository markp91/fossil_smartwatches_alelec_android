package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ix4 implements Factory<k45> {
    @DexIgnore
    public static k45 a(ex4 ex4) {
        k45 d = ex4.d();
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }
}
