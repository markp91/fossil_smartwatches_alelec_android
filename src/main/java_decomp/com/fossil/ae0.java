package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ae0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ be0[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ae0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable[] readParcelableArray = parcel.readParcelableArray(be0.class.getClassLoader());
            if (readParcelableArray != null) {
                return new ae0((be0[]) readParcelableArray);
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<com.fossil.blesdk.model.preset.DevicePresetItem>");
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ae0[i];
        }
    }

    @DexIgnore
    public ae0(be0[] be0Arr) {
        this.a = be0Arr;
        b();
    }

    @DexIgnore
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (be0 a2 : this.a) {
            jSONArray.put(a2.a());
        }
        return cw0.a(new JSONObject(), bm0.PRESET, (Object) jSONArray);
    }

    @DexIgnore
    public final void b() {
        ArrayList arrayList = new ArrayList();
        for (y50 d : md6.a(this.a, y50.class)) {
            arrayList.addAll(d.d());
        }
        for (be0 be0 : this.a) {
            if (be0 instanceof m50) {
                vd6.a(((m50) be0).d(), new qc1(arrayList));
            }
        }
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final be0[] getPresetItems() {
        return this.a;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelableArray(this.a, i);
        }
    }
}
