package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bx3 extends cx3 {
    @DexIgnore
    public /* final */ dx3 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K> implements Map.Entry<K, Object> {
        @DexIgnore
        public Map.Entry<K, bx3> a;

        @DexIgnore
        public K getKey() {
            return this.a.getKey();
        }

        @DexIgnore
        public Object getValue() {
            bx3 value = this.a.getValue();
            if (value == null) {
                return null;
            }
            return value.a();
        }

        @DexIgnore
        public Object setValue(Object obj) {
            if (obj instanceof dx3) {
                return this.a.getValue().c((dx3) obj);
            }
            throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
        }

        @DexIgnore
        public b(Map.Entry<K, bx3> entry) {
            this.a = entry;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<K> implements Iterator<Map.Entry<K, Object>> {
        @DexIgnore
        public Iterator<Map.Entry<K, Object>> a;

        @DexIgnore
        public c(Iterator<Map.Entry<K, Object>> it) {
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public void remove() {
            this.a.remove();
        }

        @DexIgnore
        public Map.Entry<K, Object> next() {
            Map.Entry<K, Object> next = this.a.next();
            return next.getValue() instanceof bx3 ? new b(next) : next;
        }
    }

    @DexIgnore
    public dx3 a() {
        return b(this.d);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a().equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        return a().hashCode();
    }

    @DexIgnore
    public String toString() {
        return a().toString();
    }
}
