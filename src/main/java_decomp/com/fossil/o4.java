package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RadialGradient;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o4 extends Drawable {
    @DexIgnore
    public static /* final */ double q; // = Math.cos(Math.toRadians(45.0d));
    @DexIgnore
    public static a r;
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public Paint b;
    @DexIgnore
    public Paint c;
    @DexIgnore
    public Paint d;
    @DexIgnore
    public /* final */ RectF e;
    @DexIgnore
    public float f;
    @DexIgnore
    public Path g;
    @DexIgnore
    public float h;
    @DexIgnore
    public float i;
    @DexIgnore
    public float j;
    @DexIgnore
    public ColorStateList k;
    @DexIgnore
    public boolean l; // = true;
    @DexIgnore
    public /* final */ int m;
    @DexIgnore
    public /* final */ int n;
    @DexIgnore
    public boolean o; // = true;
    @DexIgnore
    public boolean p; // = false;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Canvas canvas, RectF rectF, float f, Paint paint);
    }

    @DexIgnore
    public o4(Resources resources, ColorStateList colorStateList, float f2, float f3, float f4) {
        this.m = resources.getColor(e4.cardview_shadow_start_color);
        this.n = resources.getColor(e4.cardview_shadow_end_color);
        this.a = resources.getDimensionPixelSize(f4.cardview_compat_inset_shadow);
        this.b = new Paint(5);
        a(colorStateList);
        this.c = new Paint(5);
        this.c.setStyle(Paint.Style.FILL);
        this.f = (float) ((int) (f2 + 0.5f));
        this.e = new RectF();
        this.d = new Paint(this.c);
        this.d.setAntiAlias(false);
        a(f3, f4);
    }

    @DexIgnore
    public static float b(float f2, float f3, boolean z) {
        return z ? (float) (((double) (f2 * 1.5f)) + ((1.0d - q) * ((double) f3))) : f2 * 1.5f;
    }

    @DexIgnore
    public final void a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            colorStateList = ColorStateList.valueOf(0);
        }
        this.k = colorStateList;
        this.b.setColor(this.k.getColorForState(getState(), this.k.getDefaultColor()));
    }

    @DexIgnore
    public float c() {
        return this.f;
    }

    @DexIgnore
    public final int d(float f2) {
        int i2 = (int) (f2 + 0.5f);
        return i2 % 2 == 1 ? i2 - 1 : i2;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        if (this.l) {
            a(getBounds());
            this.l = false;
        }
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.j / 2.0f);
        a(canvas);
        canvas.translate(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (-this.j) / 2.0f);
        r.a(canvas, this.e, this.f, this.b);
    }

    @DexIgnore
    public float e() {
        float f2 = this.h;
        return (Math.max(f2, this.f + ((float) this.a) + ((f2 * 1.5f) / 2.0f)) * 2.0f) + (((this.h * 1.5f) + ((float) this.a)) * 2.0f);
    }

    @DexIgnore
    public float f() {
        float f2 = this.h;
        return (Math.max(f2, this.f + ((float) this.a) + (f2 / 2.0f)) * 2.0f) + ((this.h + ((float) this.a)) * 2.0f);
    }

    @DexIgnore
    public float g() {
        return this.j;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        int ceil = (int) Math.ceil((double) b(this.h, this.f, this.o));
        int ceil2 = (int) Math.ceil((double) a(this.h, this.f, this.o));
        rect.set(ceil2, ceil, ceil2, ceil);
        return true;
    }

    @DexIgnore
    public boolean isStateful() {
        ColorStateList colorStateList = this.k;
        return (colorStateList != null && colorStateList.isStateful()) || super.isStateful();
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.l = true;
    }

    @DexIgnore
    public boolean onStateChange(int[] iArr) {
        ColorStateList colorStateList = this.k;
        int colorForState = colorStateList.getColorForState(iArr, colorStateList.getDefaultColor());
        if (this.b.getColor() == colorForState) {
            return false;
        }
        this.b.setColor(colorForState);
        this.l = true;
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.b.setAlpha(i2);
        this.c.setAlpha(i2);
        this.d.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.b.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void b(Rect rect) {
        getPadding(rect);
    }

    @DexIgnore
    public void c(float f2) {
        a(f2, this.h);
    }

    @DexIgnore
    public float d() {
        return this.h;
    }

    @DexIgnore
    public void a(boolean z) {
        this.o = z;
        invalidateSelf();
    }

    @DexIgnore
    public void b(float f2) {
        a(this.j, f2);
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        a(colorStateList);
        invalidateSelf();
    }

    @DexIgnore
    public final void a(float f2, float f3) {
        if (f2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            throw new IllegalArgumentException("Invalid shadow size " + f2 + ". Must be >= 0");
        } else if (f3 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float d2 = (float) d(f2);
            float d3 = (float) d(f3);
            if (d2 > d3) {
                if (!this.p) {
                    this.p = true;
                }
                d2 = d3;
            }
            if (this.j != d2 || this.h != d3) {
                this.j = d2;
                this.h = d3;
                this.i = (float) ((int) ((d2 * 1.5f) + ((float) this.a) + 0.5f));
                this.l = true;
                invalidateSelf();
            }
        } else {
            throw new IllegalArgumentException("Invalid max shadow size " + f3 + ". Must be >= 0");
        }
    }

    @DexIgnore
    public ColorStateList b() {
        return this.k;
    }

    @DexIgnore
    public static float a(float f2, float f3, boolean z) {
        return z ? (float) (((double) f2) + ((1.0d - q) * ((double) f3))) : f2;
    }

    @DexIgnore
    public void a(float f2) {
        if (f2 >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            float f3 = (float) ((int) (f2 + 0.5f));
            if (this.f != f3) {
                this.f = f3;
                this.l = true;
                invalidateSelf();
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Invalid radius " + f2 + ". Must be >= 0");
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        float f2 = this.f;
        float f3 = (-f2) - this.i;
        float f4 = f2 + ((float) this.a) + (this.j / 2.0f);
        float f5 = f4 * 2.0f;
        boolean z = this.e.width() - f5 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z2 = this.e.height() - f5 > LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        int save = canvas.save();
        RectF rectF = this.e;
        canvas.translate(rectF.left + f4, rectF.top + f4);
        canvas.drawPath(this.g, this.c);
        if (z) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.width() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save);
        int save2 = canvas.save();
        RectF rectF2 = this.e;
        canvas.translate(rectF2.right - f4, rectF2.bottom - f4);
        canvas.rotate(180.0f);
        canvas.drawPath(this.g, this.c);
        if (z) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.width() - f5, (-this.f) + this.i, this.d);
        }
        canvas.restoreToCount(save2);
        int save3 = canvas.save();
        RectF rectF3 = this.e;
        canvas.translate(rectF3.left + f4, rectF3.bottom - f4);
        canvas.rotate(270.0f);
        canvas.drawPath(this.g, this.c);
        if (z2) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.height() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save3);
        int save4 = canvas.save();
        RectF rectF4 = this.e;
        canvas.translate(rectF4.right - f4, rectF4.top + f4);
        canvas.rotate(90.0f);
        canvas.drawPath(this.g, this.c);
        if (z2) {
            canvas.drawRect(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f3, this.e.height() - f5, -this.f, this.d);
        }
        canvas.restoreToCount(save4);
    }

    @DexIgnore
    public final void a() {
        float f2 = this.f;
        RectF rectF = new RectF(-f2, -f2, f2, f2);
        RectF rectF2 = new RectF(rectF);
        float f3 = this.i;
        rectF2.inset(-f3, -f3);
        Path path = this.g;
        if (path == null) {
            this.g = new Path();
        } else {
            path.reset();
        }
        this.g.setFillType(Path.FillType.EVEN_ODD);
        this.g.moveTo(-this.f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.rLineTo(-this.i, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.arcTo(rectF2, 180.0f, 90.0f, false);
        this.g.arcTo(rectF, 270.0f, -90.0f, false);
        this.g.close();
        float f4 = this.f;
        float f5 = this.i;
        Paint paint = this.c;
        float f6 = f4 + f5;
        int i2 = this.m;
        paint.setShader(new RadialGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f6, new int[]{i2, i2, this.n}, new float[]{0.0f, f4 / (f4 + f5), 1.0f}, Shader.TileMode.CLAMP));
        Paint paint2 = this.d;
        float f7 = this.f;
        float f8 = this.i;
        float f9 = (-f7) + f8;
        float f10 = (-f7) - f8;
        int i3 = this.m;
        int[] iArr = {i3, i3, this.n};
        paint2.setShader(new LinearGradient(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f9, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, f10, iArr, new float[]{LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 0.5f, 1.0f}, Shader.TileMode.CLAMP));
        this.d.setAntiAlias(false);
    }

    @DexIgnore
    public final void a(Rect rect) {
        float f2 = this.h;
        float f3 = 1.5f * f2;
        this.e.set(((float) rect.left) + f2, ((float) rect.top) + f3, ((float) rect.right) - f2, ((float) rect.bottom) - f3);
        a();
    }
}
