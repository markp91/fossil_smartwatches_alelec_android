package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hu4 extends RecyclerView.g<a> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public List<lc6<Complication, String>> a;
    @DexIgnore
    public List<lc6<Complication, String>> b; // = new ArrayList();
    @DexIgnore
    public String c; // = "";
    @DexIgnore
    public e d;
    @DexIgnore
    public d e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends RecyclerView.ViewHolder {
        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(hu4 hu4, View view) {
            super(view);
            wg6.b(view, "itemView");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends a {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(hu4 hu4, View view) {
            super(hu4, view);
            wg6.b(view, "itemView");
            View findViewById = view.findViewById(2131363199);
            if (findViewById != null) {
                this.a = (FlexibleTextView) findViewById;
                String b = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                if (!TextUtils.isEmpty(b)) {
                    this.a.setBackgroundColor(Color.parseColor(b));
                    return;
                }
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final FlexibleTextView a() {
            return this.a;
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a(String str);
    }

    @DexIgnore
    public interface e {
        @DexIgnore
        void a(Complication complication);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class f extends a {
        @DexIgnore
        public /* final */ CustomizeWidget a;
        @DexIgnore
        public /* final */ FlexibleTextView b;
        @DexIgnore
        public /* final */ FlexibleTextView c;
        @DexIgnore
        public /* final */ View d;
        @DexIgnore
        public Complication e;
        @DexIgnore
        public /* final */ /* synthetic */ hu4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ f a;

            @DexIgnore
            public a(f fVar) {
                this.a = fVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Complication a2 = this.a.a();
                if (a2 != null) {
                    e a3 = this.a.f.d;
                    if (a3 != null) {
                        a3.a(a2);
                    } else {
                        FLogger.INSTANCE.getLocal().d(hu4.f, "itemClick(), no listener.");
                    }
                } else {
                    FLogger.INSTANCE.getLocal().d(hu4.f, "itemClick(), complication tag null.");
                }
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(hu4 hu4, View view) {
            super(hu4, view);
            wg6.b(view, "itemView");
            this.f = hu4;
            View findViewById = view.findViewById(2131363336);
            wg6.a((Object) findViewById, "itemView.findViewById(R.id.wc_icon)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363186);
            wg6.a((Object) findViewById2, "itemView.findViewById(R.id.tv_name)");
            this.b = (FlexibleTextView) findViewById2;
            View findViewById3 = view.findViewById(2131363078);
            if (findViewById3 != null) {
                this.c = (FlexibleTextView) findViewById3;
                View findViewById4 = view.findViewById(2131362851);
                if (findViewById4 != null) {
                    this.d = findViewById4;
                    String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
                    if (!TextUtils.isEmpty(b2)) {
                        this.d.setBackgroundColor(Color.parseColor(b2));
                    }
                    view.setOnClickListener(new a(this));
                    return;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public final Complication a() {
            return this.e;
        }

        @DexIgnore
        public final FlexibleTextView b() {
            return this.c;
        }

        @DexIgnore
        public final FlexibleTextView c() {
            return this.b;
        }

        @DexIgnore
        public final CustomizeWidget d() {
            return this.a;
        }

        @DexIgnore
        public final void a(Complication complication) {
            this.e = complication;
        }
    }

    /*
    static {
        new b((qg6) null);
        String name = hu4.class.getName();
        wg6.a((Object) name, "ComplicationSearchAdapter::class.java.name");
        f = name;
    }
    */

    @DexIgnore
    public final void b(List<lc6<Complication, String>> list) {
        d dVar;
        this.a = list;
        List<lc6<Complication, String>> list2 = this.a;
        if (!(list2 == null || !list2.isEmpty() || (dVar = this.e) == null)) {
            dVar.a(this.c);
        }
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getItemCount() {
        List<lc6<Complication, String>> list = this.a;
        return list != null ? list.size() : this.b.size() + 1;
    }

    @DexIgnore
    public int getItemViewType(int i) {
        return (this.a == null && i == 0) ? 1 : 2;
    }

    @DexIgnore
    public final void a(List<lc6<Complication, String>> list) {
        wg6.b(list, ServerSetting.VALUE);
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (i == 1) {
            View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558649, viewGroup, false);
            wg6.a((Object) inflate, "view");
            return new c(this, inflate);
        }
        View inflate2 = LayoutInflater.from(viewGroup.getContext()).inflate(2131558648, viewGroup, false);
        wg6.a((Object) inflate2, "view");
        return new f(this, inflate2);
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "<set-?>");
        this.c = str;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v14, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r1v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r3v16, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r8v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* JADX WARNING: type inference failed for: r8v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (!(aVar instanceof c)) {
            f fVar = (f) aVar;
            List<lc6<Complication, String>> list = this.a;
            if (list == null) {
                int i2 = i - 1;
                if (i2 < this.b.size()) {
                    if (!TextUtils.isEmpty((CharSequence) this.b.get(i2).getSecond())) {
                        fVar.b().setVisibility(0);
                        fVar.b().setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886347));
                    } else {
                        fVar.b().setVisibility(8);
                    }
                    fVar.d().b(((Complication) this.b.get(i2).getFirst()).getComplicationId());
                    fVar.c().setText(jm4.a(PortfolioApp.get.instance(), ((Complication) this.b.get(i2).getFirst()).getNameKey(), ((Complication) this.b.get(i2).getFirst()).getName()));
                    fVar.a((Complication) this.b.get(i2).getFirst());
                    return;
                }
                fVar.a((Complication) null);
            } else if (i - 1 < list.size()) {
                if (!TextUtils.isEmpty((CharSequence) list.get(i).getSecond())) {
                    fVar.b().setVisibility(0);
                    fVar.b().setText(jm4.a((Context) PortfolioApp.get.instance(), 2131886347));
                } else {
                    fVar.b().setVisibility(8);
                }
                fVar.d().b(((Complication) list.get(i).getFirst()).getComplicationId());
                String a2 = jm4.a(PortfolioApp.get.instance(), ((Complication) list.get(i).getFirst()).getNameKey(), ((Complication) list.get(i).getFirst()).getName());
                Object c2 = fVar.c();
                wg6.a((Object) a2, "name");
                c2.setText(a(a2, this.c));
                fVar.a((Complication) list.get(i).getFirst());
            } else {
                fVar.a((Complication) null);
            }
        } else if (this.b.isEmpty()) {
            ((c) aVar).a().setVisibility(4);
        } else {
            ((c) aVar).a().setVisibility(0);
        }
    }

    @DexIgnore
    public final SpannableString a(String str, String str2) {
        if (!(str.length() == 0)) {
            if (!(str2.length() == 0)) {
                mj6 mj6 = new mj6(str2);
                if (str != null) {
                    String lowerCase = str.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    ti6<kj6> findAll$default = mj6.findAll$default(mj6, lowerCase, 0, 2, (Object) null);
                    SpannableString spannableString = new SpannableString(str);
                    for (kj6 kj6 : findAll$default) {
                        spannableString.setSpan(new StyleSpan(1), kj6.a().e().intValue(), kj6.a().e().intValue() + str2.length(), 0);
                    }
                    return spannableString;
                }
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        return new SpannableString(str);
    }

    @DexIgnore
    public final void a(e eVar) {
        wg6.b(eVar, "listener");
        this.d = eVar;
    }

    @DexIgnore
    public final void a(d dVar) {
        wg6.b(dVar, "listener");
        this.e = dVar;
    }
}
