package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum q50 {
    FULL((byte) 1),
    HALF((byte) 2),
    QUARTER((byte) 3),
    EIGHTH((byte) 4),
    SIXTEENTH((byte) 5);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public q50(byte b2) {
        this.a = b2;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
