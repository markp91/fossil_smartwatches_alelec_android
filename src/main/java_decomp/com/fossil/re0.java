package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum re0 implements sj0 {
    SUCCESS((byte) 0),
    SIZE_OVER_LIMIT((byte) 7),
    FIRMWARE_INTERNAL_ERROR((byte) 128);
    
    @DexIgnore
    public static /* final */ vm1 g; // = null;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        g = new vm1((qg6) null);
    }
    */

    @DexIgnore
    public re0(byte b2) {
        this.b = b2;
        this.a = cw0.a((Enum<?>) this);
    }

    @DexIgnore
    public boolean a() {
        return this == SUCCESS;
    }

    @DexIgnore
    public String getLogName() {
        return this.a;
    }
}
