package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import androidx.core.content.FileProvider;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class n66 {
    @DexIgnore
    public h66 a;
    @DexIgnore
    public k66 b;

    @DexIgnore
    public n66(h66 h66) {
        this.a = h66;
        this.b = h66.b();
    }

    @DexIgnore
    public void a(Context context, Intent intent, Uri uri, int i) {
        for (ResolveInfo resolveInfo : context.getPackageManager().queryIntentActivities(intent, 65536)) {
            context.grantUriPermission(resolveInfo.activityInfo.packageName, uri, i);
        }
    }

    @DexIgnore
    public String b(Context context) {
        String string = context.getString(s66.belvedere_sdk_fpa_suffix);
        return String.format(Locale.US, "%s%s", new Object[]{context.getPackageName(), string});
    }

    @DexIgnore
    public File c(Context context, Uri uri) {
        File a2 = a(context, "gallery");
        String str = null;
        if (a2 == null) {
            this.b.w("BelvedereStorage", "Error creating cache directory");
            return null;
        }
        String b2 = b(context, uri);
        if (TextUtils.isEmpty(b2)) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US);
            b2 = String.format(Locale.US, "attachment_%s", new Object[]{simpleDateFormat.format(new Date(System.currentTimeMillis()))});
            str = a(context, uri);
        }
        return a(b2, str, a2);
    }

    @DexIgnore
    public File b(Context context, String str) {
        File a2 = a(context, "request");
        if (a2 != null) {
            return a(str, (String) null, a2);
        }
        this.b.w("BelvedereStorage", "Error creating cache directory");
        return null;
    }

    @DexIgnore
    public void a(Context context, Uri uri, int i) {
        context.revokeUriPermission(uri, i);
    }

    @DexIgnore
    public Uri a(Context context, File file) {
        String b2 = b(context);
        try {
            return FileProvider.getUriForFile(context, b2, file);
        } catch (IllegalArgumentException unused) {
            this.b.e("BelvedereStorage", String.format(Locale.US, "The selected file can't be shared %s", new Object[]{file.toString()}));
            return null;
        } catch (NullPointerException e) {
            String format = String.format(Locale.US, "=====================\nFileProvider failed to retrieve file uri. There might be an issue with the FileProvider \nPlease make sure that manifest-merger is working, and that you have defined the applicationId (package name) in the build.gradle\nManifest merger: http://tools.android.com/tech-docs/new-build-system/user-guide/manifest-merger\nIf your are not able to use gradle or the manifest merger, please add the following to your AndroidManifest.xml:\n        <provider\n            android:name=\"com.zendesk.belvedere.BelvedereFileProvider\"\n            android:authorities=\"${applicationId}${belvedereFileProviderAuthoritySuffix}\"\n            android:exported=\"false\"\n            android:grantUriPermissions=\"true\">\n            <meta-data\n                android:name=\"android.support.FILE_PROVIDER_PATHS\"\n                android:resource=\"@xml/belvedere_attachment_storage\" />\n        </provider>\n=====================", new Object[]{b2});
            Log.e("BelvedereStorage", format, e);
            this.b.e("BelvedereStorage", format, e);
            return null;
        }
    }

    @DexIgnore
    public final String b(Context context, Uri uri) {
        Cursor query = context.getContentResolver().query(uri, new String[]{"_display_name"}, (String) null, (String[]) null, (String) null);
        String str = "";
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    str = query.getString(0);
                }
            } finally {
                query.close();
            }
        }
        return str;
    }

    @DexIgnore
    public final String c(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }

    @DexIgnore
    public File a(Context context) {
        File a2 = a(context, "camera");
        if (a2 == null) {
            this.b.w("BelvedereStorage", "Error creating cache directory");
            return null;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.US);
        return a(String.format(Locale.US, "camera_image_%s", new Object[]{simpleDateFormat.format(new Date(System.currentTimeMillis()))}), ".jpg", a2);
    }

    @DexIgnore
    public final File a(String str, String str2, File file) {
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        if (TextUtils.isEmpty(str2)) {
            str2 = "";
        }
        sb.append(str2);
        return new File(file, sb.toString());
    }

    @DexIgnore
    public final File a(Context context, String str) {
        String str2;
        if (!TextUtils.isEmpty(str)) {
            str2 = str + File.separator;
        } else {
            str2 = "";
        }
        File file = new File(c(context) + File.separator + this.a.g() + File.separator + str2);
        if (!file.isDirectory()) {
            file.mkdirs();
        }
        if (file.isDirectory()) {
            return file;
        }
        return null;
    }

    @DexIgnore
    public final String a(Context context, Uri uri) {
        String extensionFromMimeType = MimeTypeMap.getSingleton().getExtensionFromMimeType(context.getContentResolver().getType(uri));
        Locale locale = Locale.US;
        Object[] objArr = new Object[1];
        if (TextUtils.isEmpty(extensionFromMimeType)) {
            extensionFromMimeType = "tmp";
        }
        objArr[0] = extensionFromMimeType;
        return String.format(locale, ".%s", objArr);
    }
}
