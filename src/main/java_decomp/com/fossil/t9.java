package com.fossil;

import android.view.View;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t9 implements ViewTreeObserver.OnPreDrawListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public ViewTreeObserver b;
    @DexIgnore
    public /* final */ Runnable c;

    @DexIgnore
    public t9(View view, Runnable runnable) {
        this.a = view;
        this.b = view.getViewTreeObserver();
        this.c = runnable;
    }

    @DexIgnore
    public static t9 a(View view, Runnable runnable) {
        if (view == null) {
            throw new NullPointerException("view == null");
        } else if (runnable != null) {
            t9 t9Var = new t9(view, runnable);
            view.getViewTreeObserver().addOnPreDrawListener(t9Var);
            view.addOnAttachStateChangeListener(t9Var);
            return t9Var;
        } else {
            throw new NullPointerException("runnable == null");
        }
    }

    @DexIgnore
    public boolean onPreDraw() {
        a();
        this.c.run();
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
        this.b = view.getViewTreeObserver();
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        a();
    }

    @DexIgnore
    public void a() {
        if (this.b.isAlive()) {
            this.b.removeOnPreDrawListener(this);
        } else {
            this.a.getViewTreeObserver().removeOnPreDrawListener(this);
        }
        this.a.removeOnAttachStateChangeListener(this);
    }
}
