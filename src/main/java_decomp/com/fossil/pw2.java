package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw2 implements Parcelable.Creator<ew2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    z = f22.i(parcel, a);
                    break;
                case 2:
                    z2 = f22.i(parcel, a);
                    break;
                case 3:
                    z3 = f22.i(parcel, a);
                    break;
                case 4:
                    z4 = f22.i(parcel, a);
                    break;
                case 5:
                    z5 = f22.i(parcel, a);
                    break;
                case 6:
                    z6 = f22.i(parcel, a);
                    break;
                default:
                    f22.v(parcel, a);
                    break;
            }
        }
        f22.h(parcel, b);
        return new ew2(z, z2, z3, z4, z5, z6);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ew2[i];
    }
}
