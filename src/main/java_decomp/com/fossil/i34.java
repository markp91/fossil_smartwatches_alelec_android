package com.fossil;

import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i34 extends RecyclerView.g<c> {
    @DexIgnore
    public /* final */ ArrayList<Ringtone> a; // = new ArrayList<>();
    @DexIgnore
    public Ringtone b;
    @DexIgnore
    public /* final */ b c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(Ringtone ringtone);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public View b;
        @DexIgnore
        public /* final */ TextView c;
        @DexIgnore
        public /* final */ ConstraintLayout d;
        @DexIgnore
        public /* final */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ i34 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                i34 i34 = this.a.f;
                Object obj = i34.a.get(this.a.getAdapterPosition());
                wg6.a(obj, "mRingPhones[adapterPosition]");
                i34.b = (Ringtone) obj;
                this.a.f.c.a(i34.c(this.a.f));
                this.a.f.notifyDataSetChanged();
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(i34 i34, View view) {
            super(view);
            wg6.b(view, "view");
            this.f = i34;
            View findViewById = view.findViewById(2131362620);
            wg6.a((Object) findViewById, "view.findViewById(R.id.iv_ringphone)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131362621);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.iv_ringphone_selected)");
            this.b = findViewById2;
            View findViewById3 = view.findViewById(2131363202);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.tv_ring_phone)");
            this.c = (TextView) findViewById3;
            ConstraintLayout findViewById4 = view.findViewById(2131362851);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.root_background)");
            this.d = findViewById4;
            View findViewById5 = view.findViewById(2131362511);
            wg6.a((Object) findViewById5, "view.findViewById(R.id.indicator)");
            this.e = findViewById5;
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                this.d.setBackgroundColor(Color.parseColor(b2));
            }
            String b3 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            if (!TextUtils.isEmpty(b3)) {
                this.e.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.setOnClickListener(new a(this));
        }

        @DexIgnore
        public final void a(Ringtone ringtone) {
            wg6.b(ringtone, "ringTone");
            this.c.setText(ringtone.getRingtoneName());
            this.b.setVisibility(0);
            if (wg6.a((Object) ringtone.getRingtoneName(), (Object) i34.c(this.f).getRingtoneName())) {
                this.b.setVisibility(0);
            } else {
                this.b.setVisibility(8);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public i34(b bVar) {
        wg6.b(bVar, "mListener");
        this.c = bVar;
    }

    @DexIgnore
    public static final /* synthetic */ Ringtone c(i34 i34) {
        Ringtone ringtone = i34.b;
        if (ringtone != null) {
            return ringtone;
        }
        wg6.d("mSelectedRingPhone");
        throw null;
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public c onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558680, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026ing_phone, parent, false)");
        return new c(this, inflate);
    }

    @DexIgnore
    public final void a(List<Ringtone> list, Ringtone ringtone) {
        wg6.b(list, "data");
        wg6.b(ringtone, "selectedRingtone");
        this.a.clear();
        this.a.addAll(list);
        this.b = ringtone;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(c cVar, int i) {
        wg6.b(cVar, "holder");
        int adapterPosition = cVar.getAdapterPosition();
        if (getItemCount() > adapterPosition && adapterPosition != -1) {
            Ringtone ringtone = this.a.get(adapterPosition);
            wg6.a((Object) ringtone, "mRingPhones[adapterPos]");
            cVar.a(ringtone);
        }
    }
}
