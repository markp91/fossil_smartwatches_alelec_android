package com.fossil;

import com.facebook.AccessToken;
import com.facebook.internal.BoltsMeasurementEventListener;
import com.facebook.internal.FetchedAppGateKeepersManager;
import com.fossil.r40;
import java.util.LinkedHashMap;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn0 extends p40 {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ og0 e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public String g;
    @DexIgnore
    public String h;
    @DexIgnore
    public boolean i;
    @DexIgnore
    public String j;
    @DexIgnore
    public r40 k;
    @DexIgnore
    public bw0 l;
    @DexIgnore
    public JSONObject m;

    @DexIgnore
    public /* synthetic */ nn0(String str, og0 og0, String str2, String str3, String str4, boolean z, String str5, r40 r40, bw0 bw0, JSONObject jSONObject, int i2) {
        String str6;
        r40 r402;
        String str7;
        String str8 = str;
        String str9 = str2;
        String str10 = str3;
        String str11 = str4;
        int i3 = i2;
        String str12 = (i3 & 64) != 0 ? "" : str5;
        if ((i3 & 128) != 0) {
            str6 = str12;
            str7 = str9;
            r402 = new r40("", str2, "", (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, (String) null, 262136);
        } else {
            str7 = str9;
            str6 = str12;
            r402 = r40;
        }
        int i4 = i2;
        bw0 bw02 = (i4 & 256) != 0 ? new bw0("", "", "", 0, "", (String) null, 32) : bw0;
        JSONObject jSONObject2 = (i4 & 512) != 0 ? new JSONObject() : jSONObject;
        StringBuilder sb = new StringBuilder();
        sb.append(str7);
        String str13 = str4;
        sb.append(str13);
        String str14 = str3;
        sb.append(str14);
        sb.append(str8);
        sb.append(cw0.a((Enum<?>) og0));
        sb.toString();
        String str15 = str7;
        this.a = System.currentTimeMillis();
        this.d = str8;
        this.e = og0;
        this.f = str15;
        this.g = str14;
        this.h = str13;
        this.i = z;
        this.j = str6;
        this.k = r402;
        this.l = bw02;
        this.m = jSONObject2;
        this.c = ze0.a("UUID.randomUUID().toString()");
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("timestamp", cw0.a(this.a)).put("line_number", this.b);
        wg6.a(put, "JSONObject().put(LogEntr\u2026.LINE_NUMBER, lineNumber)");
        put.put("phase_uuid", this.h).put("phase_name", this.g).put("entry_uuid", this.c).put(BoltsMeasurementEventListener.MEASUREMENT_EVENT_NAME_KEY, this.d).put("type", cw0.a((Enum<?>) this.e)).put("is_success", this.i).put("value", this.m).put(AccessToken.USER_ID_KEY, this.l.c).put("serial_number", this.k.getSerialNumber()).put("model_number", this.k.getModelNumber()).put("firmware_version", this.k.getFirmwareVersion()).put("phone_model", this.l.b).put("os", this.l.f).put("os_version", this.l.a).put(FetchedAppGateKeepersManager.APPLICATION_SDK_VERSION, this.l.e).put("session_uuid", this.j).put("timezone_offset", this.l.d);
        return put;
    }
}
