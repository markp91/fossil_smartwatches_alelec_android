package com.fossil;

import com.portfolio.platform.data.source.remote.GoogleApiService;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.detail.weather.WeatherSettingPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j85 implements Factory<i85> {
    @DexIgnore
    public static WeatherSettingPresenter a(f85 f85, GoogleApiService googleApiService) {
        return new WeatherSettingPresenter(f85, googleApiService);
    }
}
