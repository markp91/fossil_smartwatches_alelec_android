package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kp0 implements Parcelable.Creator<cr0> {
    @DexIgnore
    public /* synthetic */ kp0(qg6 qg6) {
    }

    @DexIgnore
    public cr0 createFromParcel(Parcel parcel) {
        return new cr0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new cr0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m35createFromParcel(Parcel parcel) {
        return new cr0(parcel, (qg6) null);
    }
}
