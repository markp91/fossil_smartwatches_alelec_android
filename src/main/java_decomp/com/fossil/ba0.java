package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba0 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ hd0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ba0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new ba0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ba0[i];
        }
    }

    @DexIgnore
    public ba0(byte b, int i, hd0 hd0) {
        super(e90.RING_PHONE, b, i);
        this.d = hd0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(ba0.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return this.d == ((ba0) obj).d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.RingPhoneRequest");
    }

    @DexIgnore
    public final hd0 getAction() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public /* synthetic */ ba0(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.d = hd0.values()[parcel.readInt()];
    }
}
