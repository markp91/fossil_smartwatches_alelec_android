package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz2 implements Parcelable.Creator<az2> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        LatLng latLng = null;
        String str = null;
        String str2 = null;
        IBinder iBinder = null;
        float f = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f4 = 0.5f;
        float f5 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        float f6 = 1.0f;
        float f7 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 2:
                    latLng = f22.a(parcel2, a, LatLng.CREATOR);
                    break;
                case 3:
                    str = f22.e(parcel2, a);
                    break;
                case 4:
                    str2 = f22.e(parcel2, a);
                    break;
                case 5:
                    iBinder = f22.p(parcel2, a);
                    break;
                case 6:
                    f = f22.n(parcel2, a);
                    break;
                case 7:
                    f2 = f22.n(parcel2, a);
                    break;
                case 8:
                    z = f22.i(parcel2, a);
                    break;
                case 9:
                    z2 = f22.i(parcel2, a);
                    break;
                case 10:
                    z3 = f22.i(parcel2, a);
                    break;
                case 11:
                    f3 = f22.n(parcel2, a);
                    break;
                case 12:
                    f4 = f22.n(parcel2, a);
                    break;
                case 13:
                    f5 = f22.n(parcel2, a);
                    break;
                case 14:
                    f6 = f22.n(parcel2, a);
                    break;
                case 15:
                    f7 = f22.n(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new az2(latLng, str, str2, iBinder, f, f2, z, z2, z3, f3, f4, f5, f6, f7);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new az2[i];
    }
}
