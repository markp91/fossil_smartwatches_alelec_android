package com.fossil;

import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.uirenew.home.customize.domain.usecase.SetDianaPresetToWatchUseCase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e45 implements Factory<d45> {
    @DexIgnore
    public static DianaCustomizeEditPresenter a(z35 z35, UserRepository userRepository, int i, SetDianaPresetToWatchUseCase setDianaPresetToWatchUseCase, om4 om4, an4 an4) {
        return new DianaCustomizeEditPresenter(z35, userRepository, i, setDianaPresetToWatchUseCase, om4, an4);
    }
}
