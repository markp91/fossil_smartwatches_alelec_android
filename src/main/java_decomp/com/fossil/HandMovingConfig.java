package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class HandMovingConfig extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ n50 a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ p50 c;
    @DexIgnore
    public /* final */ q50 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<o50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                n50 valueOf = n50.valueOf(readString);
                int readInt = parcel.readInt();
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    p50 valueOf2 = p50.valueOf(readString2);
                    String readString3 = parcel.readString();
                    if (readString3 != null) {
                        wg6.a(readString3, "parcel.readString()!!");
                        return new HandMovingConfig(valueOf, readInt, valueOf2, q50.valueOf(readString3));
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new HandMovingConfig[i];
        }
    }

    @DexIgnore
    public HandMovingConfig(n50 n50, int i, p50 p50, q50 q50) {
        this.a = n50;
        this.b = i;
        this.c = p50;
        this.d = q50;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.HAND_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.DEGREE, (Object) Integer.valueOf(this.b)), bm0.DIRECTION, (Object) cw0.a((Enum<?>) this.c)), bm0.SPEED, (Object) cw0.a((Enum<?>) this.d));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] array = ByteBuffer.allocate(5).order(ByteOrder.LITTLE_ENDIAN).put(this.a.a()).putShort((short) this.b).put(this.c.a()).put(this.d.a()).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(HandMovingConfig.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            HandMovingConfig handMovingConfig = (HandMovingConfig) obj;
            return this.a == handMovingConfig.a && this.b == handMovingConfig.b && this.c == handMovingConfig.c && this.d == handMovingConfig.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.calibration.HandMovingConfig");
    }

    @DexIgnore
    public final int getDegree() {
        return this.b;
    }

    @DexIgnore
    public final n50 getHandId() {
        return this.a;
    }

    @DexIgnore
    public final p50 getMovingDirection() {
        return this.c;
    }

    @DexIgnore
    public final q50 getMovingSpeed() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (((this.a.hashCode() * 31) + this.b) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
    }
}
