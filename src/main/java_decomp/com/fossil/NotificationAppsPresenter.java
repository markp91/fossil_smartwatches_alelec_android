package com.fossil;

import android.content.Context;
import android.content.Intent;
import com.fossil.gy4;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.enums.ServiceActionResult;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppsPresenter extends zx4 {
    @DexIgnore
    public static /* final */ String r;
    @DexIgnore
    public static /* final */ a s; // = new a((qg6) null);
    @DexIgnore
    public boolean e; // = this.p.B();
    @DexIgnore
    public List<InstalledApp> f; // = new ArrayList();
    @DexIgnore
    public List<vx4> g; // = new ArrayList();
    @DexIgnore
    public List<AppNotificationFilter> h; // = new ArrayList();
    @DexIgnore
    public List<ContactGroup> i; // = new ArrayList();
    @DexIgnore
    public /* final */ b j; // = new b();
    @DexIgnore
    public /* final */ ay4 k;
    @DexIgnore
    public /* final */ z24 l;
    @DexIgnore
    public /* final */ d15 m;
    @DexIgnore
    public /* final */ mz4 n;
    @DexIgnore
    public /* final */ gy4 o;
    @DexIgnore
    public /* final */ an4 p;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return NotificationAppsPresenter.r;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b implements BleCommandResultManager.b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(CommunicateMode communicateMode, Intent intent) {
            wg6.b(communicateMode, "communicateMode");
            wg6.b(intent, "intent");
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "SetNotificationFilterReceiver");
            int intExtra = intent.getIntExtra(ButtonService.Companion.getSERVICE_BLE_PHASE(), CommunicateMode.IDLE.ordinal());
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationAppsPresenter.s.a();
            local.d(a2, "onReceive - phase=" + intExtra + ", communicateMode=" + communicateMode);
            if (communicateMode != CommunicateMode.SET_NOTIFICATION_FILTERS) {
                return;
            }
            if (intent.getIntExtra(ButtonService.Companion.getSERVICE_ACTION_RESULT(), -1) == ServiceActionResult.SUCCEEDED.ordinal()) {
                FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - success");
                NotificationAppsPresenter.this.p.a(NotificationAppsPresenter.this.j());
                NotificationAppsPresenter.this.l.a(NotificationAppsPresenter.this.o, new gy4.b(NotificationAppsPresenter.this.k()), new dy4$b$a(this));
                return;
            }
            FLogger.INSTANCE.getLocal().d(NotificationAppsPresenter.s.a(), "onReceive - failed");
            int intExtra2 = intent.getIntExtra(ButtonService.Companion.getLAST_DEVICE_ERROR_STATE(), -1);
            ArrayList<Integer> integerArrayListExtra = intent.getIntegerArrayListExtra(ButtonService.Companion.getLIST_PERMISSION_CODES());
            if (integerArrayListExtra == null) {
                integerArrayListExtra = new ArrayList<>(intExtra2);
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = NotificationAppsPresenter.s.a();
            local2.d(a3, "permissionErrorCodes=" + integerArrayListExtra + " , size=" + integerArrayListExtra.size());
            int size = integerArrayListExtra.size();
            for (int i = 0; i < size; i++) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String a4 = NotificationAppsPresenter.s.a();
                local3.d(a4, "error code " + i + " =" + integerArrayListExtra.get(i));
            }
            if (intExtra2 != 1101) {
                if (intExtra2 == 1924) {
                    NotificationAppsPresenter.this.k.j();
                    return;
                } else if (intExtra2 == 8888) {
                    NotificationAppsPresenter.this.k.c();
                    return;
                } else if (!(intExtra2 == 1112 || intExtra2 == 1113)) {
                    return;
                }
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(integerArrayListExtra);
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026ode(permissionErrorCodes)");
            ay4 l = NotificationAppsPresenter.this.k;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                l.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1", f = "NotificationAppsPresenter.kt", l = {132, 137, 186, 194, 203, 212}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ NotificationAppsPresenter this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(NotificationAppsPresenter notificationAppsPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = notificationAppsPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:100:0x0271, code lost:
            com.fossil.wg6.a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x0274, code lost:
            throw null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x0275, code lost:
            if (r15 == false) goto L_0x0298;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:103:0x0277, code lost:
            r15 = com.fossil.NotificationAppsPresenter.l(r14.this$0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x027d, code lost:
            if (r15 == null) goto L_0x02a8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x027f, code lost:
            r6 = ((com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment) r15).getContext();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:106:0x0285, code lost:
            if (r6 == null) goto L_0x0298;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:107:0x0287, code lost:
            com.fossil.hf6.a(com.fossil.xm4.a(com.fossil.xm4.d, r6, "NOTIFICATION_APPS", false, false, false, 28, (java.lang.Object) null));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:108:0x0298, code lost:
            com.fossil.NotificationAppsPresenter.l(r14.this$0).f(r14.this$0.k());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x02af, code lost:
            throw new com.fossil.rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:112:0x02b2, code lost:
            if ((r15 instanceof com.portfolio.platform.CoroutineUseCase.a) == false) goto L_0x02ce;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:113:0x02b4, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.NotificationAppsPresenter.s.a(), "GetApps onError");
            com.fossil.NotificationAppsPresenter.l(r14.this$0).a();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:114:0x02ce, code lost:
            r15 = com.fossil.NotificationAppsPresenter.a(r14.this$0);
            r5 = new com.fossil.dy4$c$d(r14, (com.fossil.xe6) null);
            r14.L$0 = r1;
            r14.label = 3;
            r15 = com.fossil.gk6.a(r15, r5, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:115:0x02e2, code lost:
            if (r15 != r0) goto L_0x0045;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:116:0x02e4, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:117:0x02e5, code lost:
            r1 = (com.portfolio.platform.CoroutineUseCase.c) r15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:118:0x02ea, code lost:
            if ((r1 instanceof com.fossil.mz4.d) == false) goto L_0x03d0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:119:0x02ec, code lost:
            r15 = com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal();
            r6 = com.fossil.NotificationAppsPresenter.s.a();
            r7 = new java.lang.StringBuilder();
            r7.append("GetAllContactGroup onSuccess, size = ");
            r8 = (com.fossil.mz4.d) r1;
            r7.append(r8.a().size());
            r15.d(r6, r7.toString());
            com.fossil.NotificationAppsPresenter.f(r14.this$0).clear();
            com.fossil.NotificationAppsPresenter.a(r14.this$0, (java.util.List) com.fossil.yd6.d(r8.a()));
            r15 = com.fossil.NotificationAppsPresenter.b(r14.this$0);
            r6 = new com.fossil.dy4$c$f(r14, (com.fossil.xe6) null);
            r14.L$0 = r5;
            r14.L$1 = r1;
            r14.label = 4;
            r15 = com.fossil.gk6.a(r15, r6, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:120:0x0343, code lost:
            if (r15 != r0) goto L_0x0346;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:121:0x0345, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:122:0x0346, code lost:
            r15 = (java.util.List) r15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:123:0x034c, code lost:
            if (r15.isEmpty() == false) goto L_0x0388;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:124:0x034e, code lost:
            r6 = new java.util.ArrayList();
            r7 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowCallsFrom", 0, true);
            r2 = new com.portfolio.platform.data.model.NotificationSettingsModel("AllowMessagesFrom", 0, false);
            r6.add(r7);
            r6.add(r2);
            r3 = com.fossil.NotificationAppsPresenter.b(r14.this$0);
            r8 = new com.fossil.dy4$c$b(r14, r6, (com.fossil.xe6) null);
            r14.L$0 = r5;
            r14.L$1 = r1;
            r14.L$2 = r15;
            r14.L$3 = r6;
            r14.L$4 = r7;
            r14.L$5 = r2;
            r14.label = 5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:125:0x0385, code lost:
            if (com.fossil.gk6.a(r3, r8, r14) != r0) goto L_0x03e5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:126:0x0387, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:127:0x0388, code lost:
            com.fossil.NotificationAppsPresenter.e(r14.this$0).clear();
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.NotificationAppsPresenter.s.a(), "listNotificationSettings.size = " + r15.size());
            r2 = com.fossil.NotificationAppsPresenter.a(r14.this$0);
            r3 = new com.fossil.dy4$c$c(r14, r15, (com.fossil.xe6) null);
            r14.L$0 = r5;
            r14.L$1 = r1;
            r14.L$2 = r15;
            r14.label = 6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:128:0x03cd, code lost:
            if (com.fossil.gk6.a(r2, r3, r14) != r0) goto L_0x03e5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:129:0x03cf, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:131:0x03d2, code lost:
            if ((r1 instanceof com.fossil.mz4.b) == false) goto L_0x03e5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:132:0x03d4, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.NotificationAppsPresenter.s.a(), "GetAllContactGroup onError");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:133:0x03e5, code lost:
            com.fossil.NotificationAppsPresenter.l(r14.this$0).g(r14.this$0.j());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:134:0x03f6, code lost:
            return com.fossil.cd6.a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0083, code lost:
            r15 = com.fossil.NotificationAppsPresenter.a(r14.this$0);
            r5 = new com.fossil.dy4$c$e(r14, (com.fossil.xe6) null);
            r14.L$0 = r1;
            r14.label = 2;
            r15 = com.fossil.gk6.a(r15, r5, r14);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:17:0x0097, code lost:
            if (r15 != r0) goto L_0x009a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:0x0099, code lost:
            return r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x009a, code lost:
            r15 = (com.portfolio.platform.CoroutineUseCase.c) r15;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:20:0x009e, code lost:
            if ((r15 instanceof com.fossil.d15.a) == false) goto L_0x02b0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x00a0, code lost:
            com.fossil.NotificationAppsPresenter.l(r14.this$0).a();
            r5 = r14.this$0.k().size();
            r15 = (com.fossil.d15.a) r15;
            r6 = r15.a().size();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:22:0x00bd, code lost:
            if (r5 <= r6) goto L_0x0136;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:23:0x00bf, code lost:
            r5 = r14.this$0;
            r6 = r5.k();
            r7 = new java.util.ArrayList();
            r6 = r6.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:25:0x00d2, code lost:
            if (r6.hasNext() == false) goto L_0x012d;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d4, code lost:
            r8 = r6.next();
            r9 = (com.fossil.AppWrapper) r8;
            r10 = r15.a().iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:28:0x00e7, code lost:
            if (r10.hasNext() == false) goto L_0x0117;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:29:0x00e9, code lost:
            r11 = r10.next();
            r12 = ((com.fossil.AppWrapper) r11).getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:30:0x00f4, code lost:
            if (r12 == null) goto L_0x00fb;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f6, code lost:
            r12 = r12.getIdentifier();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:32:0x00fb, code lost:
            r12 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:33:0x00fc, code lost:
            r13 = r9.getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:34:0x0100, code lost:
            if (r13 == null) goto L_0x0107;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x0102, code lost:
            r13 = r13.getIdentifier();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0107, code lost:
            r13 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:38:0x0114, code lost:
            if (com.fossil.hf6.a(com.fossil.wg6.a((java.lang.Object) r12, (java.lang.Object) r13)).booleanValue() == false) goto L_0x00e3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:39:0x0117, code lost:
            r11 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:41:0x011a, code lost:
            if (((com.fossil.AppWrapper) r11) == null) goto L_0x011e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:42:0x011c, code lost:
            r9 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:43:0x011e, code lost:
            r9 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:45:0x0127, code lost:
            if (com.fossil.hf6.a(r9).booleanValue() == false) goto L_0x00ce;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:46:0x0129, code lost:
            r7.add(r8);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:47:0x012d, code lost:
            r5.a((java.util.List<com.fossil.vx4>) com.fossil.yd6.d(r7));
         */
        /* JADX WARNING: Code restructure failed: missing block: B:48:0x0136, code lost:
            if (r5 >= r6) goto L_0x01af;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:49:0x0138, code lost:
            r5 = r15.a();
            r6 = new java.util.ArrayList();
            r5 = r5.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:51:0x0149, code lost:
            if (r5.hasNext() == false) goto L_0x01a6;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:52:0x014b, code lost:
            r7 = r5.next();
            r8 = (com.fossil.AppWrapper) r7;
            r9 = r14.this$0.k().iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:54:0x0160, code lost:
            if (r9.hasNext() == false) goto L_0x0190;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:55:0x0162, code lost:
            r10 = r9.next();
            r11 = (com.fossil.AppWrapper) r10;
            r12 = r8.getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:56:0x016d, code lost:
            if (r12 == null) goto L_0x0174;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:57:0x016f, code lost:
            r12 = r12.getIdentifier();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:58:0x0174, code lost:
            r12 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x0175, code lost:
            r11 = r11.getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x0020, code lost:
            r0 = (java.util.List) r14.L$2;
            r0 = (com.portfolio.platform.CoroutineUseCase.c) r14.L$1;
            r0 = (com.fossil.il6) r14.L$0;
            com.fossil.nc6.a(r15);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:60:0x0179, code lost:
            if (r11 == null) goto L_0x0180;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:61:0x017b, code lost:
            r11 = r11.getIdentifier();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:62:0x0180, code lost:
            r11 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:64:0x018d, code lost:
            if (com.fossil.hf6.a(com.fossil.wg6.a((java.lang.Object) r12, (java.lang.Object) r11)).booleanValue() == false) goto L_0x015c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:65:0x0190, code lost:
            r10 = null;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:67:0x0193, code lost:
            if (((com.fossil.AppWrapper) r10) != null) goto L_0x0197;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:68:0x0195, code lost:
            r8 = true;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:69:0x0197, code lost:
            r8 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:71:0x01a0, code lost:
            if (com.fossil.hf6.a(r8).booleanValue() == false) goto L_0x0145;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:72:0x01a2, code lost:
            r6.add(r7);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:73:0x01a6, code lost:
            r14.this$0.k().addAll(r6);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:74:0x01af, code lost:
            com.misfit.frameworks.buttonservice.log.FLogger.INSTANCE.getLocal().d(com.fossil.NotificationAppsPresenter.s.a(), "GetApps onSuccess size " + r15.a().size() + " isAllAppEnabled " + r14.this$0.j());
            com.fossil.NotificationAppsPresenter.g(r14.this$0).clear();
            r15 = r14.this$0.k().iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:76:0x01fc, code lost:
            if (r15.hasNext() == false) goto L_0x0223;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:77:0x01fe, code lost:
            r5 = r15.next().getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:78:0x0208, code lost:
            if (r5 == null) goto L_0x01f8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x020a, code lost:
            r6 = r5.isSelected();
            com.fossil.wg6.a((java.lang.Object) r6, "it.isSelected");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:80:0x0217, code lost:
            if (r6.booleanValue() == false) goto L_0x01f8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:81:0x0219, code lost:
            com.fossil.NotificationAppsPresenter.g(r14.this$0).add(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:83:0x0229, code lost:
            if (r14.this$0.j() == false) goto L_0x0230;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:84:0x022b, code lost:
            com.fossil.NotificationAppsPresenter.a(r14.this$0, true);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:86:0x0236, code lost:
            if (r14.this$0.j() != false) goto L_0x0277;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:87:0x0238, code lost:
            r15 = r14.this$0.k();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:88:0x0240, code lost:
            if ((r15 instanceof java.util.Collection) == false) goto L_0x024a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x0045, code lost:
            r5 = r1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:90:0x0246, code lost:
            if (r15.isEmpty() == false) goto L_0x024a;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:91:0x0248, code lost:
            r15 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:92:0x024a, code lost:
            r15 = r15.iterator();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x0252, code lost:
            if (r15.hasNext() == false) goto L_0x0248;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x0254, code lost:
            r5 = ((com.fossil.AppWrapper) r15.next()).getInstalledApp();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x025e, code lost:
            if (r5 == null) goto L_0x0271;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:97:0x0260, code lost:
            r5 = r5.isSelected();
            com.fossil.wg6.a((java.lang.Object) r5, "it.installedApp!!.isSelected");
         */
        /* JADX WARNING: Code restructure failed: missing block: B:98:0x026d, code lost:
            if (r5.booleanValue() == false) goto L_0x024e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x026f, code lost:
            r15 = true;
         */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            il6 il6;
            Object a = ff6.a();
            switch (this.label) {
                case 0:
                    nc6.a(obj);
                    il6 = this.p$;
                    if (!PortfolioApp.get.instance().w().P()) {
                        dl6 a2 = this.this$0.b();
                        dy4$c$a dy4_c_a = new dy4$c$a((xe6) null);
                        this.L$0 = il6;
                        this.label = 1;
                        if (gk6.a(a2, dy4_c_a, this) == a) {
                            return a;
                        }
                    }
                    break;
                case 1:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 2:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 3:
                    il6 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 4:
                    CoroutineUseCase.c cVar = (CoroutineUseCase.c) this.L$1;
                    il6 il62 = (il6) this.L$0;
                    nc6.a(obj);
                    break;
                case 5:
                    NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) this.L$5;
                    NotificationSettingsModel notificationSettingsModel2 = (NotificationSettingsModel) this.L$4;
                    List list = (List) this.L$3;
                    break;
                case 6:
                    break;
                default:
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }
    }

    /*
    static {
        String simpleName = NotificationAppsPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAppsPresenter::class.java.simpleName");
        r = simpleName;
    }
    */

    @DexIgnore
    public NotificationAppsPresenter(ay4 ay4, z24 z24, d15 d15, mz4 mz4, gy4 gy4, an4 an4, NotificationSettingsDatabase notificationSettingsDatabase) {
        wg6.b(ay4, "mView");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(d15, "mGetApps");
        wg6.b(mz4, "mGetAllContactGroup");
        wg6.b(gy4, "mSaveAppsNotification");
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.k = ay4;
        this.l = z24;
        this.m = d15;
        this.n = mz4;
        this.o = gy4;
        this.p = an4;
        this.q = notificationSettingsDatabase;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "init mIsAllAppToggleEnable " + this.e);
        this.p.r(true);
    }

    @DexIgnore
    public final void m() {
        FLogger.INSTANCE.getLocal().d(r, "registerBroadcastReceiver");
        BleCommandResultManager.d.a((BleCommandResultManager.b) this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public void n() {
        this.k.a(this);
    }

    @DexIgnore
    public final void o() {
        FLogger.INSTANCE.getLocal().d(r, "unregisterBroadcastReceiver");
        BleCommandResultManager.d.b((BleCommandResultManager.b) this.j, CommunicateMode.SET_NOTIFICATION_FILTERS);
    }

    @DexIgnore
    public final boolean b(boolean z) {
        int i2;
        int i3;
        List<vx4> list = this.g;
        if (!(list instanceof Collection) || !list.isEmpty()) {
            Iterator<T> it = list.iterator();
            i2 = 0;
            while (it.hasNext()) {
                AppWrapper appWrapper = (AppWrapper) it.next();
                InstalledApp installedApp = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                if (isSelected != null) {
                    if ((isSelected.booleanValue() == z && appWrapper.getUri() != null) && (i2 = i2 + 1) < 0) {
                        qd6.b();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
        } else {
            i2 = 0;
        }
        List<vx4> list2 = this.g;
        if (!(list2 instanceof Collection) || !list2.isEmpty()) {
            Iterator<T> it2 = list2.iterator();
            i3 = 0;
            while (it2.hasNext()) {
                if ((((AppWrapper) it2.next()).getUri() != null) && (i3 = i3 + 1) < 0) {
                    qd6.b();
                    throw null;
                }
            }
        } else {
            i3 = 0;
        }
        if (i2 == i3) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void c(boolean z) {
        InstalledApp installedApp;
        for (AppWrapper next : this.g) {
            if (!(next.getUri() == null || (installedApp = next.getInstalledApp()) == null)) {
                installedApp.setSelected(z);
            }
        }
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(r, "start");
        m();
        this.k.b();
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new c(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(r, "stop");
        o();
        this.k.a();
    }

    @DexIgnore
    public void h() {
        if (!l()) {
            this.p.a(this.e);
            FLogger.INSTANCE.getLocal().d(r, "closeAndSave, nothing changed");
            this.k.close();
            return;
        }
        xm4 xm4 = xm4.d;
        ay4 ay4 = this.k;
        if (ay4 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        } else if (xm4.a(xm4, ((NotificationAppsFragment) ay4).getContext(), "SET_NOTIFICATION", false, false, false, 28, (Object) null)) {
            FLogger.INSTANCE.getLocal().d(r, "setRuleToDevice, showProgressDialog");
            this.k.b();
            long currentTimeMillis = System.currentTimeMillis();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = r;
            local.d(str, "filter notification, time start=" + currentTimeMillis);
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = r;
            local2.d(str2, "mListAppWrapper.size = " + this.g.size());
            this.h.addAll(ux5.a(this.g, false, 1, (Object) null));
            long b2 = PortfolioApp.get.instance().b(new AppNotificationFilterSettings(this.h, System.currentTimeMillis()), PortfolioApp.get.instance().e());
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = r;
            local3.d(str3, "filter notification, time end= " + b2);
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = r;
            local4.d(str4, "delayTime =" + (b2 - currentTimeMillis) + " mili seconds");
        }
    }

    @DexIgnore
    public void i() {
        this.k.a();
        this.k.close();
    }

    @DexIgnore
    public final boolean j() {
        return this.e;
    }

    @DexIgnore
    public final List<vx4> k() {
        return this.g;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0048  */
    public final boolean l() {
        T t;
        ArrayList<InstalledApp> arrayList = new ArrayList<>();
        Iterator<vx4> it = this.g.iterator();
        while (it.hasNext()) {
            InstalledApp installedApp = it.next().getInstalledApp();
            if (installedApp != null) {
                Boolean isSelected = installedApp.isSelected();
                wg6.a((Object) isSelected, "it.isSelected");
                if (isSelected.booleanValue()) {
                    arrayList.add(installedApp);
                }
            }
        }
        if (arrayList.size() != this.f.size()) {
            return true;
        }
        for (InstalledApp installedApp2 : arrayList) {
            Iterator<T> it2 = this.f.iterator();
            while (true) {
                if (!it2.hasNext()) {
                    t = null;
                    break;
                }
                t = it2.next();
                if (wg6.a((Object) ((InstalledApp) t).getIdentifier(), (Object) installedApp2.getIdentifier())) {
                    break;
                }
            }
            InstalledApp installedApp3 = (InstalledApp) t;
            if (installedApp3 == null || (!wg6.a((Object) installedApp3.isSelected(), (Object) installedApp2.isSelected()))) {
                return true;
            }
            while (r0.hasNext()) {
            }
        }
        return false;
    }

    @DexIgnore
    public final void a(List<vx4> list) {
        wg6.b(list, "<set-?>");
        this.g = list;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i2) {
        if (i2 == 0) {
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886089);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026alllsFrom_Text__Everyone)");
            return a2;
        } else if (i2 != 1) {
            String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886091);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026owCalllsFrom_Text__NoOne)");
            return a3;
        } else {
            String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886090);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026m_Text__FavoriteContacts)");
            return a4;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        local.d(str, "setEnableAllAppsToggle: enable = " + z);
        this.e = z;
        boolean z2 = true;
        if (!z && b(true)) {
            c(false);
        } else if (z) {
            c(true);
        } else {
            z2 = false;
        }
        if (z2) {
            this.k.f(this.g);
        }
        if (this.e) {
            xm4 xm4 = xm4.d;
            ay4 ay4 = this.k;
            if (ay4 != null) {
                xm4.a(xm4, ((NotificationAppsFragment) ay4).getContext(), "NOTIFICATION_APPS", false, false, false, 28, (Object) null);
                return;
            }
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
        }
    }

    @DexIgnore
    public void a(AppWrapper appWrapper, boolean z) {
        T t;
        InstalledApp installedApp;
        wg6.b(appWrapper, "appWrapper");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = r;
        StringBuilder sb = new StringBuilder();
        sb.append("setAppState: appName = ");
        InstalledApp installedApp2 = appWrapper.getInstalledApp();
        sb.append(installedApp2 != null ? installedApp2.getTitle() : null);
        sb.append(", selected = ");
        sb.append(z);
        local.d(str, sb.toString());
        Iterator<T> it = this.g.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((AppWrapper) t).getUri(), (Object) appWrapper.getUri())) {
                break;
            }
        }
        AppWrapper appWrapper2 = (AppWrapper) t;
        if (!(appWrapper2 == null || (installedApp = appWrapper2.getInstalledApp()) == null)) {
            installedApp.setSelected(z);
        }
        boolean z2 = false;
        if (b(z) && this.e != z) {
            this.e = z;
            this.k.g(z);
        } else if (!z && this.e) {
            this.e = false;
            this.k.g(false);
        }
        if (!this.e) {
            List<vx4> list = this.g;
            if (!(list instanceof Collection) || !list.isEmpty()) {
                Iterator<T> it2 = list.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    InstalledApp installedApp3 = ((AppWrapper) it2.next()).getInstalledApp();
                    if (installedApp3 != null) {
                        Boolean isSelected = installedApp3.isSelected();
                        wg6.a((Object) isSelected, "it.installedApp!!.isSelected");
                        if (isSelected.booleanValue()) {
                            z2 = true;
                            break;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            if (!z2) {
                return;
            }
        }
        ay4 ay4 = this.k;
        if (ay4 != null) {
            Context context = ((NotificationAppsFragment) ay4).getContext();
            if (context != null) {
                xm4.a(xm4.d, context, "NOTIFICATION_APPS", false, false, false, 28, (Object) null);
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsFragment");
    }
}
