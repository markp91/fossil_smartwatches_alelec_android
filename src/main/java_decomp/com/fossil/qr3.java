package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.PowerManager;
import android.util.Log;
import com.facebook.AccessToken;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdReceiver;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr3 implements Runnable {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ PowerManager.WakeLock b; // = ((PowerManager) a().getSystemService("power")).newWakeLock(1, "fiid-sync");
    @DexIgnore
    public /* final */ FirebaseInstanceId c;
    @DexIgnore
    public /* final */ sr3 d;

    @DexIgnore
    public qr3(FirebaseInstanceId firebaseInstanceId, dr3 dr3, sr3 sr3, long j) {
        this.c = firebaseInstanceId;
        this.d = sr3;
        this.a = j;
        this.b.setReferenceCounted(false);
    }

    @DexIgnore
    public final Context a() {
        return this.c.c().b();
    }

    @DexIgnore
    public final boolean b() {
        ConnectivityManager connectivityManager = (ConnectivityManager) a().getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public final boolean c() throws IOException {
        nr3 d2 = this.c.d();
        if (!this.c.a(d2)) {
            return true;
        }
        try {
            String e = this.c.e();
            if (e == null) {
                Log.e("FirebaseInstanceId", "Token retrieval failed: null");
                return false;
            }
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                Log.d("FirebaseInstanceId", "Token successfully retrieved");
            }
            if ((d2 == null || (d2 != null && !e.equals(d2.a))) && "[DEFAULT]".equals(this.c.c().c())) {
                if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf = String.valueOf(this.c.c().c());
                    Log.d("FirebaseInstanceId", valueOf.length() != 0 ? "Invoking onNewToken for app: ".concat(valueOf) : new String("Invoking onNewToken for app: "));
                }
                Intent intent = new Intent("com.google.firebase.messaging.NEW_TOKEN");
                intent.putExtra(AccessToken.TOKEN_KEY, e);
                Context a2 = a();
                Intent intent2 = new Intent(a2, FirebaseInstanceIdReceiver.class);
                intent2.setAction("com.google.firebase.MESSAGING_EVENT");
                intent2.putExtra("wrapped_intent", intent);
                a2.sendBroadcast(intent2);
            }
            return true;
        } catch (IOException e2) {
            if ("SERVICE_NOT_AVAILABLE".equals(e2.getMessage()) || "INTERNAL_SERVER_ERROR".equals(e2.getMessage())) {
                Log.e("FirebaseInstanceId", "Token retrieval failed without exception message. Will retry token retrieval");
                return false;
            } else if (e2.getMessage() == null) {
                String message = e2.getMessage();
                StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 52);
                sb.append("Token retrieval failed: ");
                sb.append(message);
                sb.append(". Will retry token retrieval");
                Log.e("FirebaseInstanceId", sb.toString());
                return false;
            } else {
                throw e2;
            }
        } catch (SecurityException unused) {
            Log.e("FirebaseInstanceId", "Token retrieval failed with SecurityException. Will retry token retrieval");
            return false;
        }
    }

    @DexIgnore
    @SuppressLint({"Wakelock"})
    public final void run() {
        if (lr3.b().a(a())) {
            this.b.acquire();
        }
        try {
            this.c.a(true);
            if (!this.c.g()) {
                this.c.a(false);
                if (lr3.b().a(a())) {
                    this.b.release();
                }
            } else if (!lr3.b().b(a()) || b()) {
                if (!c() || !this.d.a(this.c)) {
                    this.c.a(this.a);
                } else {
                    this.c.a(false);
                }
                if (lr3.b().a(a())) {
                    this.b.release();
                }
            } else {
                new pr3(this).a();
                if (lr3.b().a(a())) {
                    this.b.release();
                }
            }
        } catch (IOException e) {
            String message = e.getMessage();
            StringBuilder sb = new StringBuilder(String.valueOf(message).length() + 93);
            sb.append("Topic sync or token retrieval failed on hard failure exceptions: ");
            sb.append(message);
            sb.append(". Won't retry the operation.");
            Log.e("FirebaseInstanceId", sb.toString());
            this.c.a(false);
            if (lr3.b().a(a())) {
                this.b.release();
            }
        } catch (Throwable th) {
            if (lr3.b().a(a())) {
                this.b.release();
            }
            throw th;
        }
    }
}
