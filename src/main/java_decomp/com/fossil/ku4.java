package com.fossil;

import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ku4 extends RecyclerView.g<a> {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Typeface c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public ArrayList<WatchFaceWrapper> e;
    @DexIgnore
    public c f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public ImageView a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public /* final */ /* synthetic */ ku4 d;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.ku4$a$a")
        /* renamed from: com.fossil.ku4$a$a  reason: collision with other inner class name */
        public static final class C0021a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0021a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (!(this.a.d.getItemCount() <= this.a.getAdapterPosition() || this.a.getAdapterPosition() == -1 || (d = this.a.d.d()) == null)) {
                    Object obj = this.a.d.e.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.a((WatchFaceWrapper) obj);
                }
                a aVar = this.a;
                aVar.d.a(aVar.getAdapterPosition());
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ku4 ku4, View view) {
            super(view);
            wg6.b(view, "view");
            this.d = ku4;
            View findViewById = view.findViewById(2131362547);
            wg6.a((Object) findViewById, "view.findViewById(R.id.iv_background_preview)");
            this.a = (ImageView) findViewById;
            View findViewById2 = view.findViewById(2131363186);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_name)");
            this.b = (TextView) findViewById2;
            View findViewById3 = view.findViewById(2131363259);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.v_background_selected)");
            this.c = findViewById3;
            this.a.setOnClickListener(new C0021a(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(WatchFaceWrapper watchFaceWrapper, int i) {
            wg6.b(watchFaceWrapper, "watchFaceWrapper");
            Drawable combination = watchFaceWrapper.getCombination();
            if (combination != null) {
                this.a.setBackground(combination);
            } else {
                this.a.setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231278));
            }
            this.b.setText(watchFaceWrapper.getName());
            if (i == this.d.a) {
                this.b.setTextColor(this.d.e());
                this.b.setTypeface(this.d.c());
                this.c.setVisibility(0);
                return;
            }
            this.b.setTextColor(this.d.f());
            this.b.setTypeface(this.d.c());
            this.c.setVisibility(8);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(WatchFaceWrapper watchFaceWrapper);
    }

    /*
    static {
        new b((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ku4(ArrayList arrayList, c cVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final Typeface c() {
        return this.c;
    }

    @DexIgnore
    public final c d() {
        return this.f;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public final int f() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.e.size();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558659, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026ackground, parent, false)");
        return new a(this, inflate);
    }

    @DexIgnore
    public ku4(ArrayList<WatchFaceWrapper> arrayList, c cVar) {
        wg6.b(arrayList, "mData");
        this.e = arrayList;
        this.f = cVar;
        String b2 = ThemeManager.l.a().b("primaryText");
        this.b = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        this.c = ThemeManager.l.a().c("descriptionText1");
        String b3 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.d = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
    }

    @DexIgnore
    public final void a(List<WatchFaceWrapper> list) {
        wg6.b(list, "data");
        this.e.clear();
        this.e.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        wg6.b(str, "id");
        Iterator<WatchFaceWrapper> it = this.e.iterator();
        int i = 0;
        while (true) {
            if (!it.hasNext()) {
                i = -1;
                break;
            } else if (wg6.a((Object) it.next().getId(), (Object) str)) {
                break;
            } else {
                i++;
            }
        }
        a(i);
        return i;
    }

    @DexIgnore
    public final void a(int i) {
        try {
            if (this.a != i) {
                int i2 = this.a;
                this.a = i;
                notifyItemChanged(i);
                notifyItemChanged(i2);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "holder");
        if (getItemCount() > i && i != -1) {
            WatchFaceWrapper watchFaceWrapper = this.e.get(i);
            wg6.a((Object) watchFaceWrapper, "mData[position]");
            aVar.a(watchFaceWrapper, i);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wg6.b(cVar, "listener");
        this.f = cVar;
    }
}
