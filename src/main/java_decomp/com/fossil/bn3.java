package com.fossil;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bn3<K0, V0> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends e<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public <K, V> Map<K, Collection<V>> b() {
            return ym3.b(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<V> implements nk3<List<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public b(int i) {
            bl3.a(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        public List<V> get() {
            return new ArrayList(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c<V> implements nk3<Set<V>>, Serializable {
        @DexIgnore
        public /* final */ int expectedValuesPerKey;

        @DexIgnore
        public c(int i) {
            bl3.a(i, "expectedValuesPerKey");
            this.expectedValuesPerKey = i;
        }

        @DexIgnore
        public Set<V> get() {
            return yn3.b(this.expectedValuesPerKey);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class d<K0, V0> extends bn3<K0, V0> {
        @DexIgnore
        public d() {
            super((an3) null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> tm3<K, V> b();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class e<K0> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends d<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public a(int i) {
                this.a = i;
            }

            @DexIgnore
            public <K extends K0, V> tm3<K, V> b() {
                return cn3.a(e.this.b(), new b(this.a));
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends f<K0, Object> {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public b(int i) {
                this.a = i;
            }

            @DexIgnore
            public <K extends K0, V> xn3<K, V> b() {
                return cn3.b(e.this.b(), new c(this.a));
            }
        }

        @DexIgnore
        public d<K0, Object> a() {
            return a(2);
        }

        @DexIgnore
        public f<K0, Object> b(int i) {
            bl3.a(i, "expectedValuesPerKey");
            return new b(i);
        }

        @DexIgnore
        public abstract <K extends K0, V> Map<K, Collection<V>> b();

        @DexIgnore
        public f<K0, Object> c() {
            return b(2);
        }

        @DexIgnore
        public d<K0, Object> a(int i) {
            bl3.a(i, "expectedValuesPerKey");
            return new a(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f<K0, V0> extends bn3<K0, V0> {
        @DexIgnore
        public f() {
            super((an3) null);
        }

        @DexIgnore
        public abstract <K extends K0, V extends V0> xn3<K, V> b();
    }

    @DexIgnore
    public /* synthetic */ bn3(an3 an3) {
        this();
    }

    @DexIgnore
    public static e<Object> a() {
        return a(8);
    }

    @DexIgnore
    public bn3() {
    }

    @DexIgnore
    public static e<Object> a(int i) {
        bl3.a(i, "expectedKeys");
        return new a(i);
    }
}
