package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gx5 implements MembersInjector<ex5> {
    @DexIgnore
    public static void a(ex5 ex5, DeviceRepository deviceRepository) {
        ex5.h = deviceRepository;
    }

    @DexIgnore
    public static void a(ex5 ex5, NotificationSettingsDatabase notificationSettingsDatabase) {
        ex5.i = notificationSettingsDatabase;
    }
}
