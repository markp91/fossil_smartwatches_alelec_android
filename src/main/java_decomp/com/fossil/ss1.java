package com.fossil;

import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ss1 implements Factory<rs1> {
    @DexIgnore
    public /* final */ Provider<zs1> a;
    @DexIgnore
    public /* final */ Provider<zs1> b;
    @DexIgnore
    public /* final */ Provider<vr1> c;
    @DexIgnore
    public /* final */ Provider<vs1> d;

    @DexIgnore
    public ss1(Provider<zs1> provider, Provider<zs1> provider2, Provider<vr1> provider3, Provider<vs1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static ss1 a(Provider<zs1> provider, Provider<zs1> provider2, Provider<vr1> provider3, Provider<vs1> provider4) {
        return new ss1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public rs1 get() {
        return new rs1((zs1) this.a.get(), (zs1) this.b.get(), (vr1) this.c.get(), (vs1) this.d.get());
    }
}
