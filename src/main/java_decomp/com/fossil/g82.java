package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.RawDataPoint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g82 implements Parcelable.Creator<RawDataPoint> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [java.lang.Object[]] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        l72[] l72Arr = null;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    j = f22.s(parcel2, a);
                    break;
                case 2:
                    j2 = f22.s(parcel2, a);
                    break;
                case 3:
                    l72Arr = f22.b(parcel2, a, l72.CREATOR);
                    break;
                case 4:
                    i = f22.q(parcel2, a);
                    break;
                case 5:
                    i2 = f22.q(parcel2, a);
                    break;
                case 6:
                    j3 = f22.s(parcel2, a);
                    break;
                case 7:
                    j4 = f22.s(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new RawDataPoint(j, j2, l72Arr, i, i2, j3, j4);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new RawDataPoint[i];
    }
}
