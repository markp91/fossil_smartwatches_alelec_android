package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dp6 {
    @DexIgnore
    public static final <T, R> Object a(ck6<? super T> ck6, R r, ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6) {
        Object obj;
        wg6.b(ck6, "$this$startUndispatchedOrReturn");
        wg6.b(ig6, "block");
        ck6.n();
        try {
            oh6.a((Object) ig6, 2);
            obj = ig6.invoke(r, ck6);
        } catch (Throwable th) {
            obj = new vk6(th, false, 2, (qg6) null);
        }
        if (obj == ff6.a()) {
            return ff6.a();
        }
        if (!ck6.b(obj, 4)) {
            return ff6.a();
        }
        Object d = ck6.d();
        if (!(d instanceof vk6)) {
            return zm6.b(d);
        }
        throw so6.a(ck6, ((vk6) d).a);
    }

    @DexIgnore
    public static final <T, R> Object b(ck6<? super T> ck6, R r, ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6) {
        Object obj;
        wg6.b(ck6, "$this$startUndispatchedOrReturnIgnoreTimeout");
        wg6.b(ig6, "block");
        ck6.n();
        boolean z = false;
        try {
            oh6.a((Object) ig6, 2);
            obj = ig6.invoke(r, ck6);
        } catch (Throwable th) {
            obj = new vk6(th, false, 2, (qg6) null);
        }
        if (obj == ff6.a()) {
            return ff6.a();
        }
        if (!ck6.b(obj, 4)) {
            return ff6.a();
        }
        Object d = ck6.d();
        if (!(d instanceof vk6)) {
            return zm6.b(d);
        }
        vk6 vk6 = (vk6) d;
        Throwable th2 = vk6.a;
        if (!(th2 instanceof rn6) || ((rn6) th2).coroutine != ck6) {
            z = true;
        }
        if (z) {
            throw so6.a(ck6, vk6.a);
        } else if (!(obj instanceof vk6)) {
            return obj;
        } else {
            throw so6.a(ck6, ((vk6) obj).a);
        }
    }

    @DexIgnore
    public static final <T> void a(hg6<? super xe6<? super T>, ? extends Object> hg6, xe6<? super T> xe6) {
        af6 context;
        Object b;
        wg6.b(hg6, "$this$startCoroutineUndispatched");
        wg6.b(xe6, "completion");
        nf6.a(xe6);
        try {
            context = xe6.getContext();
            b = yo6.b(context, (Object) null);
            oh6.a((Object) hg6, 1);
            Object invoke = hg6.invoke(xe6);
            yo6.a(context, b);
            if (invoke != ff6.a()) {
                mc6.a aVar = mc6.Companion;
                xe6.resumeWith(mc6.m1constructorimpl(invoke));
            }
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        }
    }

    @DexIgnore
    public static final <R, T> void a(ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6, R r, xe6<? super T> xe6) {
        af6 context;
        Object b;
        wg6.b(ig6, "$this$startCoroutineUndispatched");
        wg6.b(xe6, "completion");
        nf6.a(xe6);
        try {
            context = xe6.getContext();
            b = yo6.b(context, (Object) null);
            oh6.a((Object) ig6, 2);
            Object invoke = ig6.invoke(r, xe6);
            yo6.a(context, b);
            if (invoke != ff6.a()) {
                mc6.a aVar = mc6.Companion;
                xe6.resumeWith(mc6.m1constructorimpl(invoke));
            }
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            xe6.resumeWith(mc6.m1constructorimpl(nc6.a(th)));
        }
    }
}
