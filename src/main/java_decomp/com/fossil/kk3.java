package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface kk3<T> {
    @DexIgnore
    boolean apply(T t);

    @DexIgnore
    boolean equals(Object obj);
}
