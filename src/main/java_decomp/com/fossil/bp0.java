package com.fossil;

import android.bluetooth.BluetoothGattService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bp0 extends xg6 implements hg6<BluetoothGattService, String> {
    @DexIgnore
    public static /* final */ bp0 a; // = new bp0();

    @DexIgnore
    public bp0() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        BluetoothGattService bluetoothGattService = (BluetoothGattService) obj;
        wg6.a(bluetoothGattService, "service");
        String uuid = bluetoothGattService.getUuid().toString();
        wg6.a(uuid, "service.uuid.toString()");
        return uuid;
    }
}
