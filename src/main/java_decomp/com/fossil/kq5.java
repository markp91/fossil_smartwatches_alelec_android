package com.fossil;

import android.content.Context;
import android.os.Bundle;
import com.fossil.m24;
import com.fossil.qt4;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq5 extends gq5 {
    @DexIgnore
    public String e; // = "";
    @DexIgnore
    public /* final */ hq5 f;
    @DexIgnore
    public /* final */ ResetPasswordUseCase g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<qt4.d, qt4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ kq5 a;

        @DexIgnore
        public b(kq5 kq5) {
            this.a = kq5;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(ResetPasswordUseCase.d dVar) {
            wg6.b(dVar, "responseValue");
            this.a.h().i();
            this.a.h().C0();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public void a(ResetPasswordUseCase.c cVar) {
            wg6.b(cVar, "errorValue");
            this.a.h().i();
            int a2 = cVar.a();
            if (a2 == 400005) {
                hq5 h = this.a.h();
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886742);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
                h.J(a3);
                this.a.h().J(false);
            } else if (a2 != 404001) {
                hq5 h2 = this.a.h();
                int a4 = cVar.a();
                String b = cVar.b();
                if (b == null) {
                    b = "";
                }
                h2.a(a4, b);
                this.a.h().J(false);
            } else {
                hq5 h3 = this.a.h();
                String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886737);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026xt__EmailIsNotRegistered)");
                h3.J(a5);
                this.a.h().J(true);
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public kq5(hq5 hq5, ResetPasswordUseCase resetPasswordUseCase) {
        wg6.b(hq5, "mView");
        wg6.b(resetPasswordUseCase, "mResetPasswordUseCase");
        this.f = hq5;
        this.g = resetPasswordUseCase;
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "email");
        this.e = str;
        if (str.length() == 0) {
            this.f.s0();
        } else {
            this.f.g0();
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.ui.user.usecase.ResetPasswordUseCase, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: type inference failed for: r3v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.kq5$b] */
    public void b(String str) {
        wg6.b(str, "email");
        if (i()) {
            this.f.k();
            this.g.a(new ResetPasswordUseCase.b(str), new b(this));
        }
    }

    @DexIgnore
    public final void c(String str) {
        wg6.b(str, "email");
        this.e = str;
    }

    @DexIgnore
    public void f() {
        this.f.Q(this.e);
    }

    @DexIgnore
    public final hq5 h() {
        return this.f;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final boolean i() {
        if (this.e.length() == 0) {
            this.f.J("");
            return false;
        } else if (!sx5.a(this.e)) {
            hq5 hq5 = this.f;
            String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886742);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026ext__InvalidEmailAddress)");
            hq5.J(a2);
            return false;
        } else {
            this.f.J("");
            return true;
        }
    }

    @DexIgnore
    public void j() {
        this.f.a(this);
    }

    @DexIgnore
    public final void a(String str, Bundle bundle) {
        wg6.b(str, "key");
        wg6.b(bundle, "outState");
        bundle.putString(str, this.e);
    }
}
