package com.fossil;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jm extends zl {
    @DexIgnore
    public static /* final */ String j; // = tl.a("WorkContinuationImpl");
    @DexIgnore
    public /* final */ lm a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ ql c;
    @DexIgnore
    public /* final */ List<? extends cm> d;
    @DexIgnore
    public /* final */ List<String> e;
    @DexIgnore
    public /* final */ List<String> f;
    @DexIgnore
    public /* final */ List<jm> g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public wl i;

    @DexIgnore
    public jm(lm lmVar, String str, ql qlVar, List<? extends cm> list) {
        this(lmVar, str, qlVar, list, (List<jm>) null);
    }

    @DexIgnore
    public wl a() {
        if (!this.h) {
            ho hoVar = new ho(this);
            this.a.h().a(hoVar);
            this.i = hoVar.b();
        } else {
            tl.a().e(j, String.format("Already enqueued work ids (%s)", new Object[]{TextUtils.join(", ", this.e)}), new Throwable[0]);
        }
        return this.i;
    }

    @DexIgnore
    public ql b() {
        return this.c;
    }

    @DexIgnore
    public List<String> c() {
        return this.e;
    }

    @DexIgnore
    public String d() {
        return this.b;
    }

    @DexIgnore
    public List<jm> e() {
        return this.g;
    }

    @DexIgnore
    public List<? extends cm> f() {
        return this.d;
    }

    @DexIgnore
    public lm g() {
        return this.a;
    }

    @DexIgnore
    public boolean h() {
        return a(this, new HashSet());
    }

    @DexIgnore
    public boolean i() {
        return this.h;
    }

    @DexIgnore
    public void j() {
        this.h = true;
    }

    @DexIgnore
    public jm(lm lmVar, String str, ql qlVar, List<? extends cm> list, List<jm> list2) {
        this.a = lmVar;
        this.b = str;
        this.c = qlVar;
        this.d = list;
        this.g = list2;
        this.e = new ArrayList(this.d.size());
        this.f = new ArrayList();
        if (list2 != null) {
            for (jm jmVar : list2) {
                this.f.addAll(jmVar.f);
            }
        }
        for (int i2 = 0; i2 < list.size(); i2++) {
            String b2 = ((cm) list.get(i2)).b();
            this.e.add(b2);
            this.f.add(b2);
        }
    }

    @DexIgnore
    public static boolean a(jm jmVar, Set<String> set) {
        set.addAll(jmVar.c());
        Set<String> a2 = a(jmVar);
        for (String contains : set) {
            if (a2.contains(contains)) {
                return true;
            }
        }
        List<jm> e2 = jmVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (jm a3 : e2) {
                if (a(a3, set)) {
                    return true;
                }
            }
        }
        set.removeAll(jmVar.c());
        return false;
    }

    @DexIgnore
    public static Set<String> a(jm jmVar) {
        HashSet hashSet = new HashSet();
        List<jm> e2 = jmVar.e();
        if (e2 != null && !e2.isEmpty()) {
            for (jm c2 : e2) {
                hashSet.addAll(c2.c());
            }
        }
        return hashSet;
    }
}
