package com.fossil;

import com.fossil.wearables.fsl.BaseProvider;
import com.portfolio.platform.data.model.ServerSetting;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ho4 extends BaseProvider {
    @DexIgnore
    void a(List<ServerSetting> list);

    @DexIgnore
    boolean a();

    @DexIgnore
    boolean addOrUpdateServerSetting(ServerSetting serverSetting);

    @DexIgnore
    ServerSetting getServerSettingByKey(String str);
}
