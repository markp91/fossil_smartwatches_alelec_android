package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ck0 {
    CLOSE((byte) 1),
    SWITCH_ACTIVITY((byte) 2),
    START_CRITICAL((byte) 3),
    END_CRITICAL((byte) 4),
    REMAP((byte) 5),
    START_REPEAT((byte) 6),
    END_REPEAT((byte) 7),
    DELAY((byte) 8),
    ANIMATION((byte) 9),
    VIBE((byte) 19),
    STREAM((byte) 11),
    HID((byte) 12);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public ck0(byte b) {
        this.a = b;
    }
}
