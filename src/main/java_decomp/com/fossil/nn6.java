package com.fossil;

import com.fossil.af6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface nn6<S> extends af6.b {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public static <S, E extends af6.b> E a(nn6<S> nn6, af6.c<E> cVar) {
            wg6.b(cVar, "key");
            return af6.b.a.a((af6.b) nn6, cVar);
        }

        @DexIgnore
        public static <S> af6 a(nn6<S> nn6, af6 af6) {
            wg6.b(af6, "context");
            return af6.b.a.a((af6.b) nn6, af6);
        }

        @DexIgnore
        public static <S, R> R a(nn6<S> nn6, R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
            wg6.b(ig6, "operation");
            return af6.b.a.a(nn6, r, ig6);
        }

        @DexIgnore
        public static <S> af6 b(nn6<S> nn6, af6.c<?> cVar) {
            wg6.b(cVar, "key");
            return af6.b.a.b(nn6, cVar);
        }
    }

    @DexIgnore
    S a(af6 af6);

    @DexIgnore
    void a(af6 af6, S s);
}
