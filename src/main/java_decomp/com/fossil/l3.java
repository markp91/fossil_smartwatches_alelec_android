package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class l3 implements View.OnLongClickListener, View.OnHoverListener, View.OnAttachStateChangeListener {
    @DexIgnore
    public static l3 j;
    @DexIgnore
    public static l3 o;
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public /* final */ CharSequence b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ Runnable d; // = new a();
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public m3 h;
    @DexIgnore
    public boolean i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void run() {
            l3.this.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void run() {
            l3.this.c();
        }
    }

    @DexIgnore
    public l3(View view, CharSequence charSequence) {
        this.a = view;
        this.b = charSequence;
        this.c = y9.a(ViewConfiguration.get(this.a.getContext()));
        b();
        this.a.setOnLongClickListener(this);
        this.a.setOnHoverListener(this);
    }

    @DexIgnore
    public static void a(View view, CharSequence charSequence) {
        l3 l3Var = j;
        if (l3Var != null && l3Var.a == view) {
            a((l3) null);
        }
        if (TextUtils.isEmpty(charSequence)) {
            l3 l3Var2 = o;
            if (l3Var2 != null && l3Var2.a == view) {
                l3Var2.c();
            }
            view.setOnLongClickListener((View.OnLongClickListener) null);
            view.setLongClickable(false);
            view.setOnHoverListener((View.OnHoverListener) null);
            return;
        }
        new l3(view, charSequence);
    }

    @DexIgnore
    public final void b() {
        this.f = Integer.MAX_VALUE;
        this.g = Integer.MAX_VALUE;
    }

    @DexIgnore
    public void c() {
        if (o == this) {
            o = null;
            m3 m3Var = this.h;
            if (m3Var != null) {
                m3Var.a();
                this.h = null;
                b();
                this.a.removeOnAttachStateChangeListener(this);
            } else {
                Log.e("TooltipCompatHandler", "sActiveHandler.mPopup == null");
            }
        }
        if (j == this) {
            a((l3) null);
        }
        this.a.removeCallbacks(this.e);
    }

    @DexIgnore
    public final void d() {
        this.a.postDelayed(this.d, (long) ViewConfiguration.getLongPressTimeout());
    }

    @DexIgnore
    public boolean onHover(View view, MotionEvent motionEvent) {
        if (this.h != null && this.i) {
            return false;
        }
        AccessibilityManager accessibilityManager = (AccessibilityManager) this.a.getContext().getSystemService("accessibility");
        if (accessibilityManager.isEnabled() && accessibilityManager.isTouchExplorationEnabled()) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action != 7) {
            if (action == 10) {
                b();
                c();
            }
        } else if (this.a.isEnabled() && this.h == null && a(motionEvent)) {
            a(this);
        }
        return false;
    }

    @DexIgnore
    public boolean onLongClick(View view) {
        this.f = view.getWidth() / 2;
        this.g = view.getHeight() / 2;
        a(true);
        return true;
    }

    @DexIgnore
    public void onViewAttachedToWindow(View view) {
    }

    @DexIgnore
    public void onViewDetachedFromWindow(View view) {
        c();
    }

    @DexIgnore
    public void a(boolean z) {
        long j2;
        int i2;
        long j3;
        if (x9.D(this.a)) {
            a((l3) null);
            l3 l3Var = o;
            if (l3Var != null) {
                l3Var.c();
            }
            o = this;
            this.i = z;
            this.h = new m3(this.a.getContext());
            this.h.a(this.a, this.f, this.g, this.i, this.b);
            this.a.addOnAttachStateChangeListener(this);
            if (this.i) {
                j2 = 2500;
            } else {
                if ((x9.x(this.a) & 1) == 1) {
                    j3 = 3000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                } else {
                    j3 = 15000;
                    i2 = ViewConfiguration.getLongPressTimeout();
                }
                j2 = j3 - ((long) i2);
            }
            this.a.removeCallbacks(this.e);
            this.a.postDelayed(this.e, j2);
        }
    }

    @DexIgnore
    public static void a(l3 l3Var) {
        l3 l3Var2 = j;
        if (l3Var2 != null) {
            l3Var2.a();
        }
        j = l3Var;
        l3 l3Var3 = j;
        if (l3Var3 != null) {
            l3Var3.d();
        }
    }

    @DexIgnore
    public final void a() {
        this.a.removeCallbacks(this.d);
    }

    @DexIgnore
    public final boolean a(MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        if (Math.abs(x - this.f) <= this.c && Math.abs(y - this.g) <= this.c) {
            return false;
        }
        this.f = x;
        this.g = y;
        return true;
    }
}
