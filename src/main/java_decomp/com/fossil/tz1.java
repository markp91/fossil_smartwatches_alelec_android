package com.fossil;

import android.util.Log;
import android.util.SparseArray;
import com.fossil.wv1;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class tz1 extends vz1 {
    @DexIgnore
    public /* final */ SparseArray<a> f; // = new SparseArray<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wv1.c {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ wv1 b;
        @DexIgnore
        public /* final */ wv1.c c;

        @DexIgnore
        public a(int i, wv1 wv1, wv1.c cVar) {
            this.a = i;
            this.b = wv1;
            this.c = cVar;
            wv1.a((wv1.c) this);
        }

        @DexIgnore
        public final void a(gv1 gv1) {
            String valueOf = String.valueOf(gv1);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            tz1.this.b(gv1, this.a);
        }
    }

    @DexIgnore
    public tz1(tw1 tw1) {
        super(tw1);
        this.a.a("AutoManageHelper", (LifecycleCallback) this);
    }

    @DexIgnore
    public static tz1 b(sw1 sw1) {
        tw1 a2 = LifecycleCallback.a(sw1);
        tz1 tz1 = (tz1) a2.a("AutoManageHelper", tz1.class);
        if (tz1 != null) {
            return tz1;
        }
        return new tz1(a2);
    }

    @DexIgnore
    public final void a(int i, wv1 wv1, wv1.c cVar) {
        w12.a(wv1, (Object) "GoogleApiClient instance cannot be null");
        boolean z = this.f.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        w12.b(z, sb.toString());
        xz1 xz1 = this.c.get();
        boolean z2 = this.b;
        String valueOf = String.valueOf(xz1);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.f.put(i, new a(i, wv1, cVar));
        if (this.b && xz1 == null) {
            String valueOf2 = String.valueOf(wv1);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            wv1.c();
        }
    }

    @DexIgnore
    public void d() {
        super.d();
        boolean z = this.b;
        String valueOf = String.valueOf(this.f);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (this.c.get() == null) {
            for (int i = 0; i < this.f.size(); i++) {
                a b = b(i);
                if (b != null) {
                    b.b.c();
                }
            }
        }
    }

    @DexIgnore
    public void e() {
        super.e();
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.b.d();
            }
        }
    }

    @DexIgnore
    public final void f() {
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                b.b.c();
            }
        }
    }

    @DexIgnore
    public final a b(int i) {
        if (this.f.size() <= i) {
            return null;
        }
        SparseArray<a> sparseArray = this.f;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    @DexIgnore
    public final void a(int i) {
        a aVar = this.f.get(i);
        this.f.remove(i);
        if (aVar != null) {
            aVar.b.b((wv1.c) aVar);
            aVar.b.d();
        }
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f.size(); i++) {
            a b = b(i);
            if (b != null) {
                printWriter.append(str).append("GoogleApiClient #").print(b.a);
                printWriter.println(":");
                b.b.a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    @DexIgnore
    public final void a(gv1 gv1, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        a aVar = this.f.get(i);
        if (aVar != null) {
            a(i);
            wv1.c cVar = aVar.c;
            if (cVar != null) {
                cVar.a(gv1);
            }
        }
    }
}
