package com.fossil;

import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oh6 {
    @DexIgnore
    public static <T extends Throwable> T a(T t) {
        wg6.a(t, oh6.class.getName());
        return t;
    }

    @DexIgnore
    public static Iterable b(Object obj) {
        if (!(obj instanceof ph6)) {
            return e(obj);
        }
        a(obj, "kotlin.collections.MutableIterable");
        throw null;
    }

    @DexIgnore
    public static List c(Object obj) {
        if (!(obj instanceof ph6)) {
            return f(obj);
        }
        a(obj, "kotlin.collections.MutableList");
        throw null;
    }

    @DexIgnore
    public static Collection d(Object obj) {
        try {
            return (Collection) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static Iterable e(Object obj) {
        try {
            return (Iterable) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static List f(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    @DexIgnore
    public static int g(Object obj) {
        if (obj instanceof sg6) {
            return ((sg6) obj).getArity();
        }
        if (obj instanceof gg6) {
            return 0;
        }
        if (obj instanceof hg6) {
            return 1;
        }
        return obj instanceof ig6 ? 2 : -1;
    }

    @DexIgnore
    public static void a(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        a(name + " cannot be cast to " + str);
        throw null;
    }

    @DexIgnore
    public static void a(String str) {
        a(new ClassCastException(str));
        throw null;
    }

    @DexIgnore
    public static boolean b(Object obj, int i) {
        return (obj instanceof fc6) && g(obj) == i;
    }

    @DexIgnore
    public static ClassCastException a(ClassCastException classCastException) {
        a(classCastException);
        throw classCastException;
    }

    @DexIgnore
    public static Collection a(Object obj) {
        if (!(obj instanceof ph6)) {
            return d(obj);
        }
        a(obj, "kotlin.collections.MutableCollection");
        throw null;
    }

    @DexIgnore
    public static Object a(Object obj, int i) {
        if (obj == null || b(obj, i)) {
            return obj;
        }
        a(obj, "kotlin.jvm.functions.Function" + i);
        throw null;
    }
}
