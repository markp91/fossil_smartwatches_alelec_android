package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y74 extends x74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362013, 1);
        x.put(2131363286, 2);
        x.put(2131362572, 3);
        x.put(2131363287, 4);
        x.put(2131362206, 5);
    }
    */

    @DexIgnore
    public y74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public y74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[5], objArr[3], objArr[2], objArr[4]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
