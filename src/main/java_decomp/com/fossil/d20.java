package com.fossil;

import android.content.Context;
import com.fossil.j96;
import java.util.Map;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class d20 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ j96 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public d20(Context context, j96 j96, String str, String str2) {
        this.a = context;
        this.b = j96;
        this.c = str;
        this.d = str2;
    }

    @DexIgnore
    public b20 a() {
        Map f = this.b.f();
        String n = z86.n(this.a);
        String k = this.b.k();
        String h = this.b.h();
        return new b20(this.b.d(), UUID.randomUUID().toString(), this.b.e(), this.b.l(), (String) f.get(j96.a.FONT_TOKEN), n, k, h, this.c, this.d);
    }
}
