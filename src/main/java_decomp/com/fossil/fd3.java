package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fd3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ ed3 b;

    @DexIgnore
    public fd3(ed3 ed3, qc3 qc3) {
        this.b = ed3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b.b) {
            if (this.b.c != null) {
                this.b.c.onFailure(this.a.a());
            }
        }
    }
}
