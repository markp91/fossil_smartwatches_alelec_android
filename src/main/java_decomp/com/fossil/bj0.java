package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bj0 extends xg6 implements hg6<bn0, Boolean> {
    @DexIgnore
    public static /* final */ bj0 a; // = new bj0();

    @DexIgnore
    public bj0() {
        super(1);
    }

    @DexIgnore
    public Object invoke(Object obj) {
        bn0 bn0 = (bn0) obj;
        il0 il0 = bn0.c;
        return Boolean.valueOf(il0 == il0.CONNECTION_DROPPED || il0 == il0.BLUETOOTH_OFF || bn0.d.c.a == x11.GATT_NULL);
    }
}
