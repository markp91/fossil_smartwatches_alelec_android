package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o86<T> extends n86<T> {
    @DexIgnore
    public T b;

    @DexIgnore
    public o86() {
        this((p86) null);
    }

    @DexIgnore
    public T a(Context context) {
        return this.b;
    }

    @DexIgnore
    public void b(Context context, T t) {
        this.b = t;
    }

    @DexIgnore
    public o86(p86<T> p86) {
        super(p86);
    }
}
