package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gc6<T> implements ic6<T>, Serializable {
    @DexIgnore
    public /* final */ T value;

    @DexIgnore
    public gc6(T t) {
        this.value = t;
    }

    @DexIgnore
    public T getValue() {
        return this.value;
    }

    @DexIgnore
    public boolean isInitialized() {
        return true;
    }

    @DexIgnore
    public String toString() {
        return String.valueOf(getValue());
    }
}
