package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc6 implements Comparable<zc6> {
    @DexIgnore
    public /* final */ short a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public /* synthetic */ zc6(short s) {
        this.a = s;
    }

    @DexIgnore
    public static boolean a(short s, Object obj) {
        if (obj instanceof zc6) {
            if (s == ((zc6) obj).a()) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public static final /* synthetic */ zc6 b(short s) {
        return new zc6(s);
    }

    @DexIgnore
    public static short c(short s) {
        return s;
    }

    @DexIgnore
    public static int d(short s) {
        return s;
    }

    @DexIgnore
    public static String e(short s) {
        return String.valueOf(s & 65535);
    }

    @DexIgnore
    public final int a(short s) {
        return a(this.a, s);
    }

    @DexIgnore
    public final /* synthetic */ short a() {
        return this.a;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ int compareTo(Object obj) {
        return a(((zc6) obj).a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        short s = this.a;
        d(s);
        return s;
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public static int a(short s, short s2) {
        return wg6.a((int) s & 65535, (int) s2 & 65535);
    }
}
