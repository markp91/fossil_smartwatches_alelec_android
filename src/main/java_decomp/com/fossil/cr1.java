package com.fossil;

import com.fossil.fr1;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cr1 extends fr1.b {
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ Set<fr1.c> c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends fr1.b.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Long b;
        @DexIgnore
        public Set<fr1.c> c;

        @DexIgnore
        public fr1.b.a a(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public fr1.b.a b(long j) {
            this.b = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public fr1.b.a a(Set<fr1.c> set) {
            if (set != null) {
                this.c = set;
                return this;
            }
            throw new NullPointerException("Null flags");
        }

        @DexIgnore
        public fr1.b a() {
            String str = "";
            if (this.a == null) {
                str = str + " delta";
            }
            if (this.b == null) {
                str = str + " maxAllowedDelay";
            }
            if (this.c == null) {
                str = str + " flags";
            }
            if (str.isEmpty()) {
                return new cr1(this.a.longValue(), this.b.longValue(), this.c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public long a() {
        return this.a;
    }

    @DexIgnore
    public Set<fr1.c> b() {
        return this.c;
    }

    @DexIgnore
    public long c() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fr1.b)) {
            return false;
        }
        fr1.b bVar = (fr1.b) obj;
        if (this.a == bVar.a() && this.b == bVar.c() && this.c.equals(bVar.b())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        long j2 = this.b;
        return this.c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "ConfigValue{delta=" + this.a + ", maxAllowedDelay=" + this.b + ", flags=" + this.c + "}";
    }

    @DexIgnore
    public cr1(long j, long j2, Set<fr1.c> set) {
        this.a = j;
        this.b = j2;
        this.c = set;
    }
}
