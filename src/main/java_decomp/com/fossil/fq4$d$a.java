package com.fossil;

import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.PlaybackState;
import android.os.Bundle;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$d$a extends MediaController.Callback {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.d a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;

    @DexIgnore
    public fq4$d$a(MusicControlComponent.d dVar, String str) {
        this.a = dVar;
        this.b = str;
    }

    @DexIgnore
    public void onMetadataChanged(MediaMetadata mediaMetadata) {
        super.onMetadataChanged(mediaMetadata);
        if (mediaMetadata != null) {
            String a2 = xi4.a(mediaMetadata.getString("android.media.metadata.TITLE"), "Unknown");
            String a3 = xi4.a(mediaMetadata.getString("android.media.metadata.ARTIST"), "Unknown");
            String a4 = xi4.a(mediaMetadata.getString("android.media.metadata.ALBUM"), "Unknown");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a5 = MusicControlComponent.o.a();
            local.d(a5, ".onMetadataChanged callback, title=" + a2 + ", artist=" + a3);
            MusicControlComponent.c cVar = new MusicControlComponent.c(this.a.c(), this.b, a2, a3, a4);
            if (!wg6.a((Object) cVar, (Object) this.a.e())) {
                MusicControlComponent.c e = this.a.e();
                this.a.a(cVar);
                this.a.a(e, cVar);
            }
        }
    }

    @DexIgnore
    public void onPlaybackStateChanged(PlaybackState playbackState) {
        super.onPlaybackStateChanged(playbackState);
        int state = playbackState != null ? playbackState.getState() : 0;
        if (state != this.a.f()) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = MusicControlComponent.o.a();
            StringBuilder sb = new StringBuilder();
            sb.append(".onPlaybackStateChanged, PlaybackState = ");
            sb.append(playbackState != null ? Integer.valueOf(playbackState.getState()) : null);
            local.d(a2, sb.toString());
            int f = this.a.f();
            this.a.a(state);
            MusicControlComponent.d dVar = this.a;
            dVar.a(f, state, dVar);
        }
    }

    @DexIgnore
    public void onSessionDestroyed() {
        super.onSessionDestroyed();
        MusicControlComponent.d dVar = this.a;
        dVar.a((MusicControlComponent.b) dVar);
    }

    @DexIgnore
    public void onSessionEvent(String str, Bundle bundle) {
        wg6.b(str, Constants.EVENT);
        super.onSessionEvent(str, bundle);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String a2 = MusicControlComponent.o.a();
        local.d(a2, ".onSessionEvent callback, event=" + str + ", extras=" + bundle);
    }
}
