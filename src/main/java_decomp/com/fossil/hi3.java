package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.util.StateSet;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hi3 {
    @DexIgnore
    public /* final */ ArrayList<b> a; // = new ArrayList<>();
    @DexIgnore
    public b b; // = null;
    @DexIgnore
    public ValueAnimator c; // = null;
    @DexIgnore
    public /* final */ Animator.AnimatorListener d; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            hi3 hi3 = hi3.this;
            if (hi3.c == animator) {
                hi3.c = null;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ int[] a;
        @DexIgnore
        public /* final */ ValueAnimator b;

        @DexIgnore
        public b(int[] iArr, ValueAnimator valueAnimator) {
            this.a = iArr;
            this.b = valueAnimator;
        }
    }

    @DexIgnore
    public void a(int[] iArr, ValueAnimator valueAnimator) {
        b bVar = new b(iArr, valueAnimator);
        valueAnimator.addListener(this.d);
        this.a.add(bVar);
    }

    @DexIgnore
    public void b() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.end();
            this.c = null;
        }
    }

    @DexIgnore
    public void a(int[] iArr) {
        b bVar;
        int size = this.a.size();
        int i = 0;
        while (true) {
            if (i >= size) {
                bVar = null;
                break;
            }
            bVar = this.a.get(i);
            if (StateSet.stateSetMatches(bVar.a, iArr)) {
                break;
            }
            i++;
        }
        b bVar2 = this.b;
        if (bVar != bVar2) {
            if (bVar2 != null) {
                a();
            }
            this.b = bVar;
            if (bVar != null) {
                a(bVar);
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        this.c = bVar.b;
        this.c.start();
    }

    @DexIgnore
    public final void a() {
        ValueAnimator valueAnimator = this.c;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            this.c = null;
        }
    }
}
