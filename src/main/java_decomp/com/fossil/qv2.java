package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qv2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ Bundle g;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 h;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qv2(ov2 ov2, String str, String str2, Bundle bundle) {
        super(ov2);
        this.h = ov2;
        this.e = str;
        this.f = str2;
        this.g = bundle;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.h.g.clearConditionalUserProperty(this.e, this.f, this.g);
    }
}
