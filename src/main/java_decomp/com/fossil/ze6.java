package com.fossil;

import com.fossil.mc6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ze6 {
    @DexIgnore
    public static final <T> void a(hg6<? super xe6<? super T>, ? extends Object> hg6, xe6<? super T> xe6) {
        wg6.b(hg6, "$this$startCoroutine");
        wg6.b(xe6, "completion");
        xe6<cd6> a = ef6.a(ef6.a(hg6, xe6));
        cd6 cd6 = cd6.a;
        mc6.a aVar = mc6.Companion;
        a.resumeWith(mc6.m1constructorimpl(cd6));
    }

    @DexIgnore
    public static final <R, T> void a(ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6, R r, xe6<? super T> xe6) {
        wg6.b(ig6, "$this$startCoroutine");
        wg6.b(xe6, "completion");
        xe6<cd6> a = ef6.a(ef6.a(ig6, r, xe6));
        cd6 cd6 = cd6.a;
        mc6.a aVar = mc6.Companion;
        a.resumeWith(mc6.m1constructorimpl(cd6));
    }
}
