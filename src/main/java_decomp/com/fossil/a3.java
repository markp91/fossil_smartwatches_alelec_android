package com.fossil;

import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a3 {
    @DexIgnore
    public int a; // = 0;
    @DexIgnore
    public int b; // = 0;
    @DexIgnore
    public int c; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int d; // = RecyclerView.UNDEFINED_DURATION;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public int f; // = 0;
    @DexIgnore
    public boolean g; // = false;
    @DexIgnore
    public boolean h; // = false;

    @DexIgnore
    public int a() {
        return this.g ? this.a : this.b;
    }

    @DexIgnore
    public int b() {
        return this.a;
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public int d() {
        return this.g ? this.b : this.a;
    }

    @DexIgnore
    public void a(int i, int i2) {
        this.h = false;
        if (i != Integer.MIN_VALUE) {
            this.e = i;
            this.a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.f = i2;
            this.b = i2;
        }
    }

    @DexIgnore
    public void b(int i, int i2) {
        this.c = i;
        this.d = i2;
        this.h = true;
        if (this.g) {
            if (i2 != Integer.MIN_VALUE) {
                this.a = i2;
            }
            if (i != Integer.MIN_VALUE) {
                this.b = i;
                return;
            }
            return;
        }
        if (i != Integer.MIN_VALUE) {
            this.a = i;
        }
        if (i2 != Integer.MIN_VALUE) {
            this.b = i2;
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (z != this.g) {
            this.g = z;
            if (!this.h) {
                this.a = this.e;
                this.b = this.f;
            } else if (z) {
                int i = this.d;
                if (i == Integer.MIN_VALUE) {
                    i = this.e;
                }
                this.a = i;
                int i2 = this.c;
                if (i2 == Integer.MIN_VALUE) {
                    i2 = this.f;
                }
                this.b = i2;
            } else {
                int i3 = this.c;
                if (i3 == Integer.MIN_VALUE) {
                    i3 = this.e;
                }
                this.a = i3;
                int i4 = this.d;
                if (i4 == Integer.MIN_VALUE) {
                    i4 = this.f;
                }
                this.b = i4;
            }
        }
    }
}
