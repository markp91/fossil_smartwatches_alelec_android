package com.fossil;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class nn extends mn<ym> {
    @DexIgnore
    public static /* final */ String j; // = tl.a("NetworkStateTracker");
    @DexIgnore
    public /* final */ ConnectivityManager g; // = ((ConnectivityManager) this.b.getSystemService("connectivity"));
    @DexIgnore
    public b h;
    @DexIgnore
    public a i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends BroadcastReceiver {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null && intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
                tl.a().a(nn.j, "Network broadcast received", new Throwable[0]);
                nn nnVar = nn.this;
                nnVar.a(nnVar.d());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ConnectivityManager.NetworkCallback {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
            tl.a().a(nn.j, String.format("Network capabilities changed: %s", new Object[]{networkCapabilities}), new Throwable[0]);
            nn nnVar = nn.this;
            nnVar.a(nnVar.d());
        }

        @DexIgnore
        public void onLost(Network network) {
            tl.a().a(nn.j, "Network connection lost", new Throwable[0]);
            nn nnVar = nn.this;
            nnVar.a(nnVar.d());
        }
    }

    @DexIgnore
    public nn(Context context, to toVar) {
        super(context, toVar);
        if (f()) {
            this.h = new b();
        } else {
            this.i = new a();
        }
    }

    @DexIgnore
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public void b() {
        if (f()) {
            try {
                tl.a().a(j, "Registering network callback", new Throwable[0]);
                this.g.registerDefaultNetworkCallback(this.h);
            } catch (IllegalArgumentException e) {
                tl.a().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            tl.a().a(j, "Registering broadcast receiver", new Throwable[0]);
            this.b.registerReceiver(this.i, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        }
    }

    @DexIgnore
    public void c() {
        if (f()) {
            try {
                tl.a().a(j, "Unregistering network callback", new Throwable[0]);
                this.g.unregisterNetworkCallback(this.h);
            } catch (IllegalArgumentException e) {
                tl.a().b(j, "Received exception while unregistering network callback", e);
            }
        } else {
            tl.a().a(j, "Unregistering broadcast receiver", new Throwable[0]);
            this.b.unregisterReceiver(this.i);
        }
    }

    @DexIgnore
    public ym d() {
        NetworkInfo activeNetworkInfo = this.g.getActiveNetworkInfo();
        boolean z = true;
        boolean z2 = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        boolean e = e();
        boolean a2 = y7.a(this.g);
        if (activeNetworkInfo == null || activeNetworkInfo.isRoaming()) {
            z = false;
        }
        return new ym(z2, e, a2, z);
    }

    @DexIgnore
    public final boolean e() {
        NetworkCapabilities networkCapabilities;
        if (Build.VERSION.SDK_INT >= 23 && (networkCapabilities = this.g.getNetworkCapabilities(this.g.getActiveNetwork())) != null && networkCapabilities.hasCapability(16)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public ym a() {
        return d();
    }
}
