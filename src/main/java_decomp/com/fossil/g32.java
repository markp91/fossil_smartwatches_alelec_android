package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g32 implements Parcelable.Creator<x12> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        Account account = null;
        int i = 0;
        GoogleSignInAccount googleSignInAccount = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                i = f22.q(parcel, a);
            } else if (a2 == 2) {
                account = (Account) f22.a(parcel, a, Account.CREATOR);
            } else if (a2 == 3) {
                i2 = f22.q(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                googleSignInAccount = (GoogleSignInAccount) f22.a(parcel, a, GoogleSignInAccount.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new x12(i, account, i2, googleSignInAccount);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new x12[i];
    }
}
