package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleAutoCompleteTextView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h74 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleAutoCompleteTextView q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ FlexibleEditText t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ RTLImageView v;
    @DexIgnore
    public /* final */ ImageView w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h74(Object obj, View view, int i, FlexibleAutoCompleteTextView flexibleAutoCompleteTextView, View view2, FlexibleButton flexibleButton, ImageView imageView, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, ImageView imageView2, ConstraintLayout constraintLayout, View view3, ImageView imageView3, LinearLayout linearLayout, ConstraintLayout constraintLayout2, FlexibleSwitchCompat flexibleSwitchCompat, View view4) {
        super(obj, view, i);
        this.q = flexibleAutoCompleteTextView;
        this.r = flexibleButton;
        this.s = imageView;
        this.t = flexibleEditText;
        this.u = flexibleTextView;
        this.v = rTLImageView;
        this.w = imageView2;
        this.x = view3;
        this.y = constraintLayout2;
        this.z = flexibleSwitchCompat;
    }
}
