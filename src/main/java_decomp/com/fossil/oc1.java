package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc1 implements Parcelable.Creator<je1> {
    @DexIgnore
    public /* synthetic */ oc1(qg6 qg6) {
    }

    @DexIgnore
    public je1 createFromParcel(Parcel parcel) {
        return new je1(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new je1[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m48createFromParcel(Parcel parcel) {
        return new je1(parcel, (qg6) null);
    }
}
