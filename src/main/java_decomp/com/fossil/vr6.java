package com.fossil;

import java.io.IOException;
import java.net.ProtocolException;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vr6 implements Interceptor {
    @DexIgnore
    public /* final */ boolean a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends nt6 {
        @DexIgnore
        public long b;

        @DexIgnore
        public a(yt6 yt6) {
            super(yt6);
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            super.a(jt6, j);
            this.b += j;
        }
    }

    @DexIgnore
    public vr6(boolean z) {
        this.a = z;
    }

    @DexIgnore
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Response response;
        as6 as6 = (as6) chain;
        wr6 g = as6.g();
        tr6 h = as6.h();
        pr6 pr6 = (pr6) as6.c();
        yq6 t = as6.t();
        long currentTimeMillis = System.currentTimeMillis();
        as6.f().d(as6.e());
        g.a(t);
        as6.f().a(as6.e(), t);
        Response.a aVar = null;
        if (zr6.b(t.e()) && t.a() != null) {
            if ("100-continue".equalsIgnoreCase(t.a("Expect"))) {
                g.b();
                as6.f().f(as6.e());
                aVar = g.a(true);
            }
            if (aVar == null) {
                as6.f().c(as6.e());
                a aVar2 = new a(g.a(t, t.a().a()));
                kt6 a2 = st6.a((yt6) aVar2);
                t.a().a(a2);
                a2.close();
                as6.f().a(as6.e(), aVar2.b);
            } else if (!pr6.e()) {
                h.e();
            }
        }
        g.a();
        if (aVar == null) {
            as6.f().f(as6.e());
            aVar = g.a(false);
        }
        aVar.a(t);
        aVar.a(h.c().d());
        aVar.b(currentTimeMillis);
        aVar.a(System.currentTimeMillis());
        Response a3 = aVar.a();
        int n = a3.n();
        if (n == 100) {
            Response.a a4 = g.a(false);
            a4.a(t);
            a4.a(h.c().d());
            a4.b(currentTimeMillis);
            a4.a(System.currentTimeMillis());
            a3 = a4.a();
            n = a3.n();
        }
        as6.f().a(as6.e(), a3);
        if (!this.a || n != 101) {
            Response.a E = a3.E();
            E.a(g.a(a3));
            response = E.a();
        } else {
            Response.a E2 = a3.E();
            E2.a(fr6.c);
            response = E2.a();
        }
        if ("close".equalsIgnoreCase(response.I().a("Connection")) || "close".equalsIgnoreCase(response.e("Connection"))) {
            h.e();
        }
        if ((n != 204 && n != 205) || response.k().contentLength() <= 0) {
            return response;
        }
        throw new ProtocolException("HTTP " + n + " had non-zero Content-Length: " + response.k().contentLength());
    }
}
