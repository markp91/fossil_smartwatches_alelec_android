package com.fossil;

import com.portfolio.platform.data.model.diana.Complication;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter$mComplicationsOfSelectedCategoryTransformations$1$1", f = "ComplicationsPresenter.kt", l = {}, m = "invokeSuspend")
public final class t45$f$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $complicationByCategories;
    @DexIgnore
    public /* final */ /* synthetic */ DianaPreset $currentPreset;
    @DexIgnore
    public /* final */ /* synthetic */ ArrayList $filteredComplicationByCategories;
    @DexIgnore
    public /* final */ /* synthetic */ String $selectedComplicationId;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ComplicationsPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public t45$f$a(ComplicationsPresenter.f fVar, DianaPreset dianaPreset, List list, String str, ArrayList arrayList, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$currentPreset = dianaPreset;
        this.$complicationByCategories = list;
        this.$selectedComplicationId = str;
        this.$filteredComplicationByCategories = arrayList;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        t45$f$a t45_f_a = new t45$f$a(this.this$0, this.$currentPreset, this.$complicationByCategories, this.$selectedComplicationId, this.$filteredComplicationByCategories, xe6);
        t45_f_a.p$ = (il6) obj;
        return t45_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((t45$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        T t;
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            if (this.$currentPreset != null) {
                for (Complication complication : this.$complicationByCategories) {
                    Iterator<T> it = this.$currentPreset.getComplications().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            t = null;
                            break;
                        }
                        t = it.next();
                        DianaPresetComplicationSetting dianaPresetComplicationSetting = (DianaPresetComplicationSetting) t;
                        boolean z = true;
                        if (!wg6.a((Object) dianaPresetComplicationSetting.getId(), (Object) complication.getComplicationId()) || !(!wg6.a((Object) dianaPresetComplicationSetting.getId(), (Object) this.$selectedComplicationId))) {
                            z = false;
                        }
                        if (hf6.a(z).booleanValue()) {
                            break;
                        }
                    }
                    if (((DianaPresetComplicationSetting) t) == null || wg6.a((Object) complication.getComplicationId(), (Object) "empty")) {
                        this.$filteredComplicationByCategories.add(complication);
                    }
                }
            }
            this.this$0.a.h.a(this.$filteredComplicationByCategories);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
