package com.fossil;

import android.util.AndroidRuntimeException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc extends AndroidRuntimeException {
    @DexIgnore
    public lc(String str) {
        super(str);
    }
}
