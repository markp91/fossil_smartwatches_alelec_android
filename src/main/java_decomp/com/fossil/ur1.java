package com.fossil;

import java.io.Closeable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ur1 extends Closeable {
    @DexIgnore
    zr1 a(rp1 rp1, np1 np1);

    @DexIgnore
    Iterable<zr1> a(rp1 rp1);

    @DexIgnore
    void a(rp1 rp1, long j);

    @DexIgnore
    void a(Iterable<zr1> iterable);

    @DexIgnore
    long b(rp1 rp1);

    @DexIgnore
    void b(Iterable<zr1> iterable);

    @DexIgnore
    boolean c(rp1 rp1);

    @DexIgnore
    int cleanUp();

    @DexIgnore
    Iterable<rp1> w();
}
