package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;
import com.j256.ormlite.logger.Logger;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vu4 extends df<ActivitySummary, RecyclerView.ViewHolder> {
    @DexIgnore
    public /* final */ Calendar d; // = Calendar.getInstance();
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ PortfolioApp l;
    @DexIgnore
    public /* final */ bv4 m;
    @DexIgnore
    public /* final */ FragmentManager n;
    @DexIgnore
    public /* final */ BaseFragment o;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public Date a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public boolean c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;

        @DexIgnore
        public b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5) {
            wg6.b(str, "mDayOfWeek");
            wg6.b(str2, "mDayOfMonth");
            wg6.b(str3, "mDailyValue");
            wg6.b(str4, "mDailyUnit");
            wg6.b(str5, "mDailyEst");
            this.a = date;
            this.b = z;
            this.c = z2;
            this.d = str;
            this.e = str2;
            this.f = str3;
            this.g = str4;
            this.h = str5;
        }

        @DexIgnore
        public final void a(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.b = z;
        }

        @DexIgnore
        public final String c() {
            return this.f;
        }

        @DexIgnore
        public final Date d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public final String f() {
            return this.d;
        }

        @DexIgnore
        public final boolean g() {
            return this.c;
        }

        @DexIgnore
        public final boolean h() {
            return this.b;
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ b(Date date, boolean z, boolean z2, String str, String str2, String str3, String str4, String str5, int i, qg6 qg6) {
            this(r1, (r0 & 2) != 0 ? false : z, (r0 & 4) == 0 ? z2 : false, (r0 & 8) != 0 ? r5 : str, (r0 & 16) != 0 ? r5 : str2, (r0 & 32) != 0 ? r5 : str3, (r0 & 64) != 0 ? r5 : str4, (r0 & Logger.DEFAULT_FULL_MESSAGE_LENGTH) == 0 ? str5 : r5);
            int i2 = i;
            Date date2 = (i2 & 1) != 0 ? null : date;
            String str6 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.c = z;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final void c(String str) {
            wg6.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final void d(String str) {
            wg6.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String a() {
            return this.h;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.g = str;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.h = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends RecyclerView.ViewHolder {
        @DexIgnore
        public Date a;
        @DexIgnore
        public /* final */ bf4 b;
        @DexIgnore
        public /* final */ /* synthetic */ vu4 c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ c a;

            @DexIgnore
            public a(c cVar) {
                this.a = cVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                Date a2 = this.a.a;
                if (a2 != null) {
                    this.a.c.m.b(a2);
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r3v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r3v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(vu4 vu4, bf4 bf4, View view) {
            super(view);
            wg6.b(bf4, "binding");
            wg6.b(view, "root");
            this.c = vu4;
            this.b = bf4;
            this.b.d().setOnClickListener(new a(this));
            this.b.r.setTextColor(vu4.g);
            this.b.v.setTextColor(vu4.g);
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r8v6, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v8, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v16, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v18, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v20, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v22, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v26, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v28, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r8v32, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r0v51, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v53, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v55, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r5v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v57, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(ActivitySummary activitySummary) {
            b a2 = this.c.a(activitySummary);
            this.a = a2.d();
            Object r0 = this.b.u;
            wg6.a((Object) r0, "binding.ftvDayOfWeek");
            r0.setText(a2.f());
            Object r02 = this.b.t;
            wg6.a((Object) r02, "binding.ftvDayOfMonth");
            r02.setText(a2.e());
            Object r03 = this.b.s;
            wg6.a((Object) r03, "binding.ftvDailyValue");
            r03.setText(a2.c());
            Object r04 = this.b.r;
            wg6.a((Object) r04, "binding.ftvDailyUnit");
            r04.setText(a2.b());
            Object r05 = this.b.v;
            wg6.a((Object) r05, "binding.ftvEst");
            r05.setText(a2.a());
            if (a2.g()) {
                this.b.r.setTextColor(w6.a(PortfolioApp.get.instance(), 2131099966));
                Object r06 = this.b.r;
                wg6.a((Object) r06, "binding.ftvDailyUnit");
                r06.setAllCaps(true);
            } else {
                this.b.r.setTextColor(w6.a(PortfolioApp.get.instance(), 2131099968));
                Object r07 = this.b.r;
                wg6.a((Object) r07, "binding.ftvDailyUnit");
                r07.setAllCaps(false);
            }
            ConstraintLayout constraintLayout = this.b.q;
            wg6.a((Object) constraintLayout, "binding.container");
            constraintLayout.setSelected(!a2.g());
            Object r08 = this.b.u;
            wg6.a((Object) r08, "binding.ftvDayOfWeek");
            r08.setSelected(a2.h());
            Object r09 = this.b.t;
            wg6.a((Object) r09, "binding.ftvDayOfMonth");
            r09.setSelected(a2.h());
            if (a2.h()) {
                this.b.q.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.h);
                this.b.t.setBackgroundColor(this.c.h);
                this.b.u.setTextColor(this.c.k);
                this.b.t.setTextColor(this.c.k);
            } else if (a2.g()) {
                this.b.q.setBackgroundColor(this.c.f);
                this.b.u.setBackgroundColor(this.c.f);
                this.b.t.setBackgroundColor(this.c.f);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            } else {
                this.b.q.setBackgroundColor(this.c.j);
                this.b.u.setBackgroundColor(this.c.j);
                this.b.t.setBackgroundColor(this.c.j);
                this.b.u.setTextColor(this.c.i);
                this.b.t.setTextColor(this.c.e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d {
        @DexIgnore
        public Date a;
        @DexIgnore
        public Date b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;

        @DexIgnore
        public d(Date date, Date date2, String str, String str2) {
            wg6.b(str, "mWeekly");
            wg6.b(str2, "mWeeklyValue");
            this.a = date;
            this.b = date2;
            this.c = str;
            this.d = str2;
        }

        @DexIgnore
        public final Date a() {
            return this.b;
        }

        @DexIgnore
        public final Date b() {
            return this.a;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final String d() {
            return this.d;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ d(Date date, Date date2, String str, String str2, int i, qg6 qg6) {
            this((i & 1) != 0 ? null : date, (i & 2) != 0 ? null : date2, (i & 4) != 0 ? "" : str, (i & 8) != 0 ? "" : str2);
        }

        @DexIgnore
        public final void a(Date date) {
            this.b = date;
        }

        @DexIgnore
        public final void b(Date date) {
            this.a = date;
        }

        @DexIgnore
        public final void a(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends c {
        @DexIgnore
        public Date d;
        @DexIgnore
        public Date e;
        @DexIgnore
        public /* final */ df4 f;
        @DexIgnore
        public /* final */ /* synthetic */ vu4 g;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                if (this.a.d != null && this.a.e != null) {
                    bv4 e = this.a.g.m;
                    Date b = this.a.d;
                    if (b != null) {
                        Date a2 = this.a.e;
                        if (a2 != null) {
                            e.b(b, a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public e(vu4 vu4, df4 df4) {
            super(vu4, r0, r1);
            wg6.b(df4, "binding");
            this.g = vu4;
            bf4 bf4 = df4.r;
            if (bf4 != null) {
                wg6.a((Object) bf4, "binding.dailyItem!!");
                View d2 = df4.d();
                wg6.a((Object) d2, "binding.root");
                this.f = df4;
                this.f.q.setOnClickListener(new a(this));
                return;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public void a(ActivitySummary activitySummary) {
            d b = this.g.b(activitySummary);
            this.e = b.a();
            this.d = b.b();
            Object r1 = this.f.s;
            wg6.a((Object) r1, "binding.ftvWeekly");
            r1.setText(b.c());
            Object r12 = this.f.t;
            wg6.a((Object) r12, "binding.ftvWeeklyValue");
            r12.setText(b.d());
            super.a(activitySummary);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements View.OnAttachStateChangeListener {
        @DexIgnore
        public /* final */ /* synthetic */ vu4 a;
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;

        @DexIgnore
        public f(vu4 vu4, RecyclerView.ViewHolder viewHolder, boolean z) {
            this.a = vu4;
            this.b = viewHolder;
            this.c = z;
        }

        @DexIgnore
        public void onViewAttachedToWindow(View view) {
            wg6.b(view, "v");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id=" + this.a.o.getId() + ", isAdded=" + this.a.o.isAdded());
            this.b.itemView.removeOnAttachStateChangeListener(this);
            Fragment b2 = this.a.n.b(this.a.o.h1());
            if (b2 == null) {
                FLogger.INSTANCE.getLocal().d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment==NULL");
                hc b3 = this.a.n.b();
                b3.a(view.getId(), this.a.o, this.a.o.h1());
                b3.d();
            } else if (this.c) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                local2.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
                hc b4 = this.a.n.b();
                b4.d(b2);
                b4.d();
                hc b5 = this.a.n.b();
                b5.a(view.getId(), this.a.o, this.a.o.h1());
                b5.d();
            } else {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                local3.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - oldFragment.id=" + b2.getId() + ", isAdded=" + b2.isAdded());
            }
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.d("DashboardActiveTimeAdapter", "onViewAttachedToWindow - mFragment.id2=" + this.a.o.getId() + ", isAdded2=" + this.a.o.isAdded());
        }

        @DexIgnore
        public void onViewDetachedFromWindow(View view) {
            wg6.b(view, "v");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends RecyclerView.ViewHolder {
        @DexIgnore
        public g(FrameLayout frameLayout, View view) {
            super(view);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vu4(uu4 uu4, PortfolioApp portfolioApp, bv4 bv4, FragmentManager fragmentManager, BaseFragment baseFragment) {
        super(uu4);
        wg6.b(uu4, "activityDifference");
        wg6.b(portfolioApp, "mApp");
        wg6.b(bv4, "mOnItemClick");
        wg6.b(fragmentManager, "mFragmentManager");
        wg6.b(baseFragment, "mFragment");
        this.l = portfolioApp;
        this.m = bv4;
        this.n = fragmentManager;
        this.o = baseFragment;
        String b2 = ThemeManager.l.a().b("primaryText");
        this.e = Color.parseColor(b2 == null ? "#FFFFFF" : b2);
        String b3 = ThemeManager.l.a().b("nonBrandSurface");
        this.f = Color.parseColor(b3 == null ? "#FFFFFF" : b3);
        String b4 = ThemeManager.l.a().b("nonBrandNonReachGoal");
        this.g = Color.parseColor(b4 == null ? "#FFFFFF" : b4);
        String b5 = ThemeManager.l.a().b("dianaActiveMinutesTab");
        this.h = Color.parseColor(b5 == null ? "#FFFFFF" : b5);
        String b6 = ThemeManager.l.a().b("secondaryText");
        this.i = Color.parseColor(b6 == null ? "#FFFFFF" : b6);
        String b7 = ThemeManager.l.a().b("nonBrandActivityDetailBackground");
        this.j = Color.parseColor(b7 == null ? "#FFFFFF" : b7);
        String b8 = ThemeManager.l.a().b("onDianaActiveMinutesTab");
        this.k = Color.parseColor(b8 == null ? "#FFFFFF" : b8);
    }

    @DexIgnore
    public long getItemId(int i2) {
        if (getItemViewType(i2) != 0) {
            return vu4.super.getItemId(i2);
        }
        if (this.o.getId() == 0) {
            return 1010101;
        }
        return (long) this.o.getId();
    }

    @DexIgnore
    public int getItemViewType(int i2) {
        if (i2 == 0) {
            return 0;
        }
        ActivitySummary activitySummary = (ActivitySummary) getItem(i2);
        if (activitySummary == null) {
            return 1;
        }
        this.d.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
        Calendar calendar = this.d;
        wg6.a((Object) calendar, "mCalendar");
        Boolean t = bk4.t(calendar.getTime());
        wg6.a((Object) t, "DateHelper.isToday(mCalendar.time)");
        if (t.booleanValue() || this.d.get(7) == 7) {
            return 2;
        }
        return 1;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i2) {
        wg6.b(viewHolder, "holder");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("DashboardActiveTimeAdapter", "onBindViewHolder - position=" + i2);
        int itemViewType = getItemViewType(i2);
        boolean z = true;
        if (itemViewType == 0) {
            View view = viewHolder.itemView;
            wg6.a((Object) view, "holder.itemView");
            if (view.getId() == ((int) 1010101)) {
                z = false;
            }
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onBindViewHolder - itemView.id=");
            View view2 = viewHolder.itemView;
            wg6.a((Object) view2, "holder.itemView");
            sb.append(view2.getId());
            sb.append(", reattach=");
            sb.append(z);
            local2.d("DashboardActiveTimeAdapter", sb.toString());
            View view3 = viewHolder.itemView;
            wg6.a((Object) view3, "holder.itemView");
            view3.setId((int) getItemId(i2));
            viewHolder.itemView.addOnAttachStateChangeListener(new f(this, viewHolder, z));
        } else if (itemViewType == 1) {
            ((c) viewHolder).a((ActivitySummary) getItem(i2));
        } else if (itemViewType != 2) {
            ((c) viewHolder).a((ActivitySummary) getItem(i2));
        } else {
            ((e) viewHolder).a((ActivitySummary) getItem(i2));
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i2) {
        wg6.b(viewGroup, "parent");
        LayoutInflater from = LayoutInflater.from(viewGroup.getContext());
        if (i2 == 0) {
            FrameLayout frameLayout = new FrameLayout(viewGroup.getContext());
            frameLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, -2));
            return new g(frameLayout, frameLayout);
        } else if (i2 == 1) {
            bf4 a2 = bf4.a(from, viewGroup, false);
            wg6.a((Object) a2, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View d2 = a2.d();
            wg6.a((Object) d2, "itemActiveTimeDayBinding.root");
            return new c(this, a2, d2);
        } else if (i2 != 2) {
            bf4 a3 = bf4.a(from, viewGroup, false);
            wg6.a((Object) a3, "ItemActiveTimeDayBinding\u2026tInflater, parent, false)");
            View d3 = a3.d();
            wg6.a((Object) d3, "itemActiveTimeDayBinding.root");
            return new c(this, a3, d3);
        } else {
            df4 a4 = df4.a(from, viewGroup, false);
            wg6.a((Object) a4, "ItemActiveTimeWeekBindin\u2026tInflater, parent, false)");
            return new e(this, a4);
        }
    }

    @DexIgnore
    public final void c(cf<ActivitySummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("updateList - size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardActiveTimeAdapter", sb.toString());
        vu4.super.b(cfVar);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r13v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final b a(ActivitySummary activitySummary) {
        b bVar = new b((Date) null, false, false, (String) null, (String) null, (String) null, (String) null, (String) null, 255, (qg6) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            int i2 = instance.get(7);
            wg6.a((Object) instance, "calendar");
            Boolean t = bk4.t(instance.getTime());
            wg6.a((Object) t, "DateHelper.isToday(calendar.time)");
            if (t.booleanValue()) {
                String a2 = jm4.a((Context) this.l, 2131886424);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026MinutesToday_Text__Today)");
                bVar.e(a2);
            } else {
                bVar.e(yk4.b.b(i2));
            }
            bVar.a(instance.getTime());
            bVar.d(String.valueOf(instance.get(5)));
            if (activitySummary.getActiveTime() > 0) {
                int activeTime = activitySummary.getActiveTime();
                bVar.c(bl4.a.a(Integer.valueOf(activeTime)));
                String a3 = jm4.a((Context) this.l, 2131886422);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026MinutesToday_Label__Mins)");
                if (a3 != null) {
                    String lowerCase = a3.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    bVar.b(lowerCase);
                    bVar.a("");
                    boolean z = false;
                    if (activitySummary.getActiveTimeGoal() > 0) {
                        if (activeTime >= activitySummary.getActiveTimeGoal()) {
                            z = true;
                        }
                        bVar.b(z);
                    } else {
                        bVar.b(false);
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            } else {
                String a4 = jm4.a((Context) this.l, 2131886399);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026eNoRecord_Text__NoRecord)");
                bVar.b(a4);
                bVar.a(true);
            }
        }
        return bVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r0v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final d b(ActivitySummary activitySummary) {
        String str;
        d dVar = new d((Date) null, (Date) null, (String) null, (String) null, 15, (qg6) null);
        if (activitySummary != null) {
            Calendar instance = Calendar.getInstance();
            instance.set(activitySummary.getYear(), activitySummary.getMonth() - 1, activitySummary.getDay());
            wg6.a((Object) instance, "calendar");
            Boolean t = bk4.t(instance.getTime());
            int i2 = instance.get(5);
            int i3 = instance.get(2);
            String b2 = bk4.b(i3);
            int i4 = instance.get(1);
            dVar.a(instance.getTime());
            instance.add(5, -6);
            int i5 = instance.get(5);
            int i6 = instance.get(2);
            String b3 = bk4.b(i6);
            int i7 = instance.get(1);
            dVar.b(instance.getTime());
            wg6.a((Object) t, "isToday");
            if (t.booleanValue()) {
                str = jm4.a((Context) this.l, 2131886426);
                wg6.a((Object) str, "LanguageHelper.getString\u2026tesToday_Title__ThisWeek)");
            } else if (i3 == i6) {
                str = b3 + ' ' + i5 + " - " + b3 + ' ' + i2;
            } else if (i7 == i4) {
                str = b3 + ' ' + i5 + " - " + b2 + ' ' + i2;
            } else {
                str = b3 + ' ' + i5 + ", " + i7 + " - " + b2 + ' ' + i2 + ", " + i4;
            }
            dVar.a(str);
            nh6 nh6 = nh6.a;
            String a2 = jm4.a((Context) this.l, 2131886423);
            wg6.a((Object) a2, "LanguageHelper.getString\u2026esToday_Text__NumberMins)");
            Object[] objArr = new Object[1];
            bl4 bl4 = bl4.a;
            ActivitySummary.TotalValuesOfWeek totalValuesOfWeek = activitySummary.getTotalValuesOfWeek();
            objArr[0] = bl4.a(totalValuesOfWeek != null ? Integer.valueOf(totalValuesOfWeek.getTotalActiveTimeOfWeek()) : null);
            String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
            dVar.b(format);
        }
        return dVar;
    }
}
