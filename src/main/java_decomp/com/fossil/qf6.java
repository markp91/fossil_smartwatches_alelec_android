package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qf6 extends pf6 implements sg6<Object>, rf6 {
    @DexIgnore
    public /* final */ int arity;

    @DexIgnore
    public qf6(int i, xe6<Object> xe6) {
        super(xe6);
        this.arity = i;
    }

    @DexIgnore
    public int getArity() {
        return this.arity;
    }

    @DexIgnore
    public String toString() {
        if (getCompletion() != null) {
            return super.toString();
        }
        String a = kh6.a((sg6) this);
        wg6.a((Object) a, "Reflection.renderLambdaToString(this)");
        return a;
    }

    @DexIgnore
    public qf6(int i) {
        this(i, (xe6<Object>) null);
    }
}
