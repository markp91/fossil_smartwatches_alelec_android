package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fk6 extends fm6 {
    @DexIgnore
    public /* final */ Thread f;

    @DexIgnore
    public fk6(Thread thread) {
        wg6.b(thread, "thread");
        this.f = thread;
    }

    @DexIgnore
    public Thread F() {
        return this.f;
    }
}
