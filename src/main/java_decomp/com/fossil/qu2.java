package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu2 implements ru2 {
    @DexIgnore
    public static /* final */ pk2<Boolean> a;
    @DexIgnore
    public static /* final */ pk2<Boolean> b;
    @DexIgnore
    public static /* final */ pk2<Boolean> c;
    @DexIgnore
    public static /* final */ pk2<Boolean> d;

    /*
    static {
        vk2 vk2 = new vk2(qk2.a("com.google.android.gms.measurement"));
        a = vk2.a("measurement.client.sessions.background_sessions_enabled", true);
        b = vk2.a("measurement.client.sessions.immediate_start_enabled_foreground", true);
        c = vk2.a("measurement.client.sessions.remove_expired_session_properties_enabled", true);
        d = vk2.a("measurement.client.sessions.session_id_enabled", true);
    }
    */

    @DexIgnore
    public final boolean zza() {
        return a.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzb() {
        return b.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzc() {
        return c.b().booleanValue();
    }

    @DexIgnore
    public final boolean zzd() {
        return d.b().booleanValue();
    }
}
