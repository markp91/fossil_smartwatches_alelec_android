package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iz5 {
    @DexIgnore
    public int a;
    @DexIgnore
    public int b;
    @DexIgnore
    public int c;

    @DexIgnore
    public iz5() {
        this(0, 0, 0, 7, (qg6) null);
    }

    @DexIgnore
    public iz5(int i, int i2, int i3) {
        this.a = i;
        this.b = i2;
        this.c = i3;
    }

    @DexIgnore
    public final int a() {
        return this.a;
    }

    @DexIgnore
    public final void a(int i) {
        this.c = i;
    }

    @DexIgnore
    public final int b() {
        return this.b;
    }

    @DexIgnore
    public final void b(int i) {
        this.b = i;
    }

    @DexIgnore
    public final int c() {
        return this.c;
    }

    @DexIgnore
    public final void c(int i) {
        this.a = i;
    }

    @DexIgnore
    public final int d() {
        return this.c;
    }

    @DexIgnore
    public final int e() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof iz5)) {
            return false;
        }
        iz5 iz5 = (iz5) obj;
        return this.a == iz5.a && this.b == iz5.b && this.c == iz5.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((d.a(this.a) * 31) + d.a(this.b)) * 31) + d.a(this.c);
    }

    @DexIgnore
    public String toString() {
        return "HeartRateSleepSessionModel(value=" + this.a + ", time=" + this.b + ", sleepState=" + this.c + ")";
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ iz5(int i, int i2, int i3, int i4, qg6 qg6) {
        this((i4 & 1) != 0 ? -1 : i, (i4 & 2) != 0 ? -1 : i2, (i4 & 4) != 0 ? -1 : i3);
    }
}
