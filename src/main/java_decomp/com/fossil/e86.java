package com.fossil;

import android.os.SystemClock;
import android.text.TextUtils;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e86 implements Callable<Map<String, k86>> {
    @DexIgnore
    public /* final */ String a;

    @DexIgnore
    public e86(String str) {
        this.a = str;
    }

    @DexIgnore
    public final Map<String, k86> a() {
        HashMap hashMap = new HashMap();
        try {
            Class.forName("com.google.android.gms.ads.AdView");
            k86 k86 = new k86("com.google.firebase.firebase-ads", "0.0.0", "binary");
            hashMap.put(k86.b(), k86);
            c86.g().v("Fabric", "Found kit: com.google.firebase.firebase-ads");
        } catch (Exception unused) {
        }
        return hashMap;
    }

    @DexIgnore
    public final Map<String, k86> b() throws Exception {
        k86 a2;
        HashMap hashMap = new HashMap();
        ZipFile c = c();
        Enumeration<? extends ZipEntry> entries = c.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            if (zipEntry.getName().startsWith("fabric/") && zipEntry.getName().length() > 7 && (a2 = a(zipEntry, c)) != null) {
                hashMap.put(a2.b(), a2);
                c86.g().v("Fabric", String.format("Found kit:[%s] version:[%s]", new Object[]{a2.b(), a2.c()}));
            }
        }
        if (c != null) {
            try {
                c.close();
            } catch (IOException unused) {
            }
        }
        return hashMap;
    }

    @DexIgnore
    public ZipFile c() throws IOException {
        return new ZipFile(this.a);
    }

    @DexIgnore
    public Map<String, k86> call() throws Exception {
        HashMap hashMap = new HashMap();
        long elapsedRealtime = SystemClock.elapsedRealtime();
        hashMap.putAll(a());
        hashMap.putAll(b());
        l86 g = c86.g();
        g.v("Fabric", "finish scanning in " + (SystemClock.elapsedRealtime() - elapsedRealtime));
        return hashMap;
    }

    @DexIgnore
    public final k86 a(ZipEntry zipEntry, ZipFile zipFile) {
        InputStream inputStream;
        try {
            inputStream = zipFile.getInputStream(zipEntry);
            try {
                Properties properties = new Properties();
                properties.load(inputStream);
                String property = properties.getProperty("fabric-identifier");
                String property2 = properties.getProperty("fabric-version");
                String property3 = properties.getProperty("fabric-build-type");
                if (TextUtils.isEmpty(property) || TextUtils.isEmpty(property2)) {
                    throw new IllegalStateException("Invalid format of fabric file," + zipEntry.getName());
                }
                k86 k86 = new k86(property, property2, property3);
                z86.a((Closeable) inputStream);
                return k86;
            } catch (IOException e) {
                e = e;
                try {
                    c86.g().e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
                    z86.a((Closeable) inputStream);
                    return null;
                } catch (Throwable th) {
                    th = th;
                    z86.a((Closeable) inputStream);
                    throw th;
                }
            }
        } catch (IOException e2) {
            e = e2;
            inputStream = null;
            c86.g().e("Fabric", "Error when parsing fabric properties " + zipEntry.getName(), e);
            z86.a((Closeable) inputStream);
            return null;
        } catch (Throwable th2) {
            th = th2;
            inputStream = null;
            z86.a((Closeable) inputStream);
            throw th;
        }
    }
}
