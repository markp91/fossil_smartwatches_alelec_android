package com.fossil;

import com.misfit.frameworks.common.constants.Constants;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.ServiceLoader;
import java.util.Set;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class go6 {
    @DexIgnore
    public static /* final */ go6 a; // = new go6();

    @DexIgnore
    public final <S> List<S> a(Class<S> cls, ClassLoader classLoader) {
        wg6.b(cls, Constants.SERVICE);
        wg6.b(classLoader, "loader");
        try {
            return b(cls, classLoader);
        } catch (Throwable unused) {
            ServiceLoader<S> load = ServiceLoader.load(cls, classLoader);
            wg6.a((Object) load, "ServiceLoader.load(service, loader)");
            return yd6.m(load);
        }
    }

    @DexIgnore
    public final <S> List<S> b(Class<S> cls, ClassLoader classLoader) {
        wg6.b(cls, Constants.SERVICE);
        wg6.b(classLoader, "loader");
        Enumeration<URL> resources = classLoader.getResources("META-INF/services/" + cls.getName());
        wg6.a((Object) resources, "urls");
        ArrayList<T> list = Collections.list(resources);
        wg6.a((Object) list, "java.util.Collections.list(this)");
        ArrayList arrayList = new ArrayList();
        for (T t : list) {
            go6 go6 = a;
            wg6.a((Object) t, "it");
            vd6.a(arrayList, go6.a((URL) t));
        }
        Set<String> p = yd6.p(arrayList);
        if (!p.isEmpty()) {
            ArrayList arrayList2 = new ArrayList(rd6.a(p, 10));
            for (String a2 : p) {
                arrayList2.add(a.a(a2, classLoader, cls));
            }
            return arrayList2;
        }
        throw new IllegalArgumentException("No providers were loaded with FastServiceLoader".toString());
    }

    @DexIgnore
    public final <S> S a(String str, ClassLoader classLoader, Class<S> cls) {
        Class<?> cls2 = Class.forName(str, false, classLoader);
        if (cls.isAssignableFrom(cls2)) {
            return cls.cast(cls2.getDeclaredConstructor(new Class[0]).newInstance(new Object[0]));
        }
        throw new IllegalArgumentException(("Expected service of class " + cls + ", but found " + cls2).toString());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0051, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:?, code lost:
        com.fossil.yf6.a(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0055, code lost:
        throw r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0058, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x005c, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005e, code lost:
        com.fossil.ec6.a(r6, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0061, code lost:
        throw r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x007c, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x007d, code lost:
        com.fossil.yf6.a(r0, r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0080, code lost:
        throw r1;
     */
    @DexIgnore
    public final List<String> a(URL url) {
        String url2 = url.toString();
        wg6.a((Object) url2, "url.toString()");
        if (xj6.c(url2, "jar", false, 2, (Object) null)) {
            String a2 = yj6.a(yj6.a(url2, "jar:file:", (String) null, 2, (Object) null), '!', (String) null, 2, (Object) null);
            String a3 = yj6.a(url2, "!/", (String) null, 2, (Object) null);
            JarFile jarFile = new JarFile(a2, false);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(jarFile.getInputStream(new ZipEntry(a3)), "UTF-8"));
            List<String> a4 = a.a(bufferedReader);
            yf6.a(bufferedReader, (Throwable) null);
            jarFile.close();
            return a4;
        }
        BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(url.openStream()));
        List<String> a5 = a.a(bufferedReader2);
        yf6.a(bufferedReader2, (Throwable) null);
        return a5;
    }

    @DexIgnore
    public final List<String> a(BufferedReader bufferedReader) {
        boolean z;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine == null) {
                return yd6.m(linkedHashSet);
            }
            String b = yj6.b(readLine, "#", (String) null, 2, (Object) null);
            if (b != null) {
                String obj = yj6.d(b).toString();
                boolean z2 = false;
                int i = 0;
                while (true) {
                    if (i >= obj.length()) {
                        z = true;
                        break;
                    }
                    char charAt = obj.charAt(i);
                    if (!(charAt == '.' || Character.isJavaIdentifierPart(charAt))) {
                        z = false;
                        break;
                    }
                    i++;
                }
                if (z) {
                    if (obj.length() > 0) {
                        z2 = true;
                    }
                    if (z2) {
                        linkedHashSet.add(obj);
                    }
                } else {
                    throw new IllegalArgumentException(("Illegal service provider class name: " + obj).toString());
                }
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
            }
        }
    }
}
