package com.fossil;

import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum oh4 {
    WALKING("walking"),
    RUNNING("running"),
    BIKING("biking");
    
    @DexIgnore
    public /* final */ String value;

    @DexIgnore
    public oh4(String str) {
        this.value = str;
    }

    @DexIgnore
    public static oh4 fromString(String str) {
        if (!TextUtils.isEmpty(str)) {
            for (oh4 oh4 : values()) {
                if (str.equalsIgnoreCase(oh4.value)) {
                    return oh4;
                }
            }
        }
        return WALKING;
    }

    @DexIgnore
    public String getValue() {
        return this.value;
    }
}
