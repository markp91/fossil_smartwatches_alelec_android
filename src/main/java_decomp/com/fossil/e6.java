package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e6 {
    @DexIgnore
    public static /* final */ int compat_button_inset_horizontal_material; // = 2131165301;
    @DexIgnore
    public static /* final */ int compat_button_inset_vertical_material; // = 2131165302;
    @DexIgnore
    public static /* final */ int compat_button_padding_horizontal_material; // = 2131165303;
    @DexIgnore
    public static /* final */ int compat_button_padding_vertical_material; // = 2131165304;
    @DexIgnore
    public static /* final */ int compat_control_corner_material; // = 2131165305;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_height; // = 2131165306;
    @DexIgnore
    public static /* final */ int compat_notification_large_icon_max_width; // = 2131165307;
    @DexIgnore
    public static /* final */ int notification_action_icon_size; // = 2131165657;
    @DexIgnore
    public static /* final */ int notification_action_text_size; // = 2131165658;
    @DexIgnore
    public static /* final */ int notification_big_circle_margin; // = 2131165659;
    @DexIgnore
    public static /* final */ int notification_content_margin_start; // = 2131165660;
    @DexIgnore
    public static /* final */ int notification_large_icon_height; // = 2131165661;
    @DexIgnore
    public static /* final */ int notification_large_icon_width; // = 2131165662;
    @DexIgnore
    public static /* final */ int notification_main_column_padding_top; // = 2131165663;
    @DexIgnore
    public static /* final */ int notification_media_narrow_margin; // = 2131165664;
    @DexIgnore
    public static /* final */ int notification_right_icon_size; // = 2131165665;
    @DexIgnore
    public static /* final */ int notification_right_side_padding_top; // = 2131165666;
    @DexIgnore
    public static /* final */ int notification_small_icon_background_padding; // = 2131165667;
    @DexIgnore
    public static /* final */ int notification_small_icon_size_as_large; // = 2131165668;
    @DexIgnore
    public static /* final */ int notification_subtext_size; // = 2131165669;
    @DexIgnore
    public static /* final */ int notification_top_pad; // = 2131165670;
    @DexIgnore
    public static /* final */ int notification_top_pad_large_text; // = 2131165671;
}
