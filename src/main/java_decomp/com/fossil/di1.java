package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class di1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ xj1 a;
    @DexIgnore
    public /* final */ /* synthetic */ if1 b;
    @DexIgnore
    public /* final */ /* synthetic */ float c;

    @DexIgnore
    public di1(xj1 xj1, if1 if1, float f) {
        this.a = xj1;
        this.b = if1;
        this.c = f;
    }

    @DexIgnore
    public final void run() {
        ii1.a(this.a.b, cc0.DEBUG, cw0.a((Enum<?>) this.b.y), "Progress: %.4f.", Float.valueOf(this.c));
        this.a.a.a(this.c);
    }
}
