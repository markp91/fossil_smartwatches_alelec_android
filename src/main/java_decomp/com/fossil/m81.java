package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m81 extends ml0 {
    @DexIgnore
    public /* final */ long J;

    @DexIgnore
    public m81(long j, ue1 ue1) {
        super(tf1.SET_HEARTBEAT_INTERVAL, lx0.SET_HEARTBEAT_INTERVAL, ue1, 0, 8);
        this.J = j;
        this.C = true;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.HEARTBEAT_INTERVAL_IN_MS, (Object) Long.valueOf(this.J));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.J).array();
        wg6.a(array, "ByteBuffer.allocate(4).o\u2026\n                .array()");
        return array;
    }
}
