package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l83 extends z33 {
    @DexIgnore
    public /* final */ d93 c;
    @DexIgnore
    public l43 d;
    @DexIgnore
    public volatile Boolean e;
    @DexIgnore
    public /* final */ b03 f;
    @DexIgnore
    public /* final */ w93 g;
    @DexIgnore
    public /* final */ List<Runnable> h; // = new ArrayList();
    @DexIgnore
    public /* final */ b03 i;

    @DexIgnore
    public l83(x53 x53) {
        super(x53);
        this.g = new w93(x53.zzm());
        this.c = new d93(this);
        this.f = new k83(this, x53);
        this.i = new u83(this, x53);
    }

    @DexIgnore
    public final boolean A() {
        g();
        w();
        return this.d != null;
    }

    @DexIgnore
    public final void B() {
        g();
        w();
        a((Runnable) new t83(this, a(true)));
    }

    @DexIgnore
    public final void C() {
        g();
        e();
        w();
        ra3 a = a(false);
        if (H()) {
            s().A();
        }
        a((Runnable) new o83(this, a));
    }

    @DexIgnore
    public final void D() {
        g();
        w();
        ra3 a = a(true);
        boolean a2 = l().a(l03.D0);
        if (a2) {
            s().B();
        }
        a((Runnable) new p83(this, a, a2));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010d  */
    public final void E() {
        boolean z;
        boolean z2;
        boolean z3;
        g();
        w();
        if (!A()) {
            boolean z4 = false;
            if (this.e == null) {
                g();
                w();
                Boolean u = k().u();
                if (u == null || !u.booleanValue()) {
                    d();
                    if (p().E() != 1) {
                        b().B().a("Checking service availability");
                        int a = j().a((int) nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE);
                        if (a != 0) {
                            if (a != 1) {
                                if (a == 2) {
                                    b().A().a("Service container out of date");
                                    if (j().u() >= 17443) {
                                        Boolean u2 = k().u();
                                        if (u2 == null || u2.booleanValue()) {
                                            z = true;
                                            z2 = false;
                                            if (!z && l().u()) {
                                                b().t().a("No way to upload. Consider using the full version of Analytics");
                                                z2 = false;
                                            }
                                            if (z2) {
                                                k().a(z);
                                            }
                                        }
                                    }
                                } else if (a == 3) {
                                    b().w().a("Service disabled");
                                } else if (a == 9) {
                                    b().w().a("Service invalid");
                                } else if (a != 18) {
                                    b().w().a("Unexpected service status", Integer.valueOf(a));
                                } else {
                                    b().w().a("Service updating");
                                }
                                z = false;
                                z2 = false;
                                b().t().a("No way to upload. Consider using the full version of Analytics");
                                z2 = false;
                                if (z2) {
                                }
                            } else {
                                b().B().a("Service missing");
                            }
                            z3 = false;
                            z2 = true;
                            b().t().a("No way to upload. Consider using the full version of Analytics");
                            z2 = false;
                            if (z2) {
                            }
                        } else {
                            b().B().a("Service available");
                        }
                    }
                    z3 = true;
                    z2 = true;
                    b().t().a("No way to upload. Consider using the full version of Analytics");
                    z2 = false;
                    if (z2) {
                    }
                } else {
                    z = true;
                }
                this.e = Boolean.valueOf(z);
            }
            if (this.e.booleanValue()) {
                this.c.b();
            } else if (!l().u()) {
                d();
                List<ResolveInfo> queryIntentServices = c().getPackageManager().queryIntentServices(new Intent().setClassName(c(), "com.google.android.gms.measurement.AppMeasurementService"), 65536);
                if (queryIntentServices != null && queryIntentServices.size() > 0) {
                    z4 = true;
                }
                if (z4) {
                    Intent intent = new Intent("com.google.android.gms.measurement.START");
                    Context c2 = c();
                    d();
                    intent.setComponent(new ComponentName(c2, "com.google.android.gms.measurement.AppMeasurementService"));
                    this.c.a(intent);
                    return;
                }
                b().t().a("Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest");
            }
        }
    }

    @DexIgnore
    public final Boolean F() {
        return this.e;
    }

    @DexIgnore
    public final void G() {
        g();
        w();
        this.c.a();
        try {
            b42.a().a(c(), this.c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.d = null;
    }

    @DexIgnore
    public final boolean H() {
        d();
        return true;
    }

    @DexIgnore
    public final void I() {
        g();
        this.g.a();
        this.f.a(l03.L.a(null).longValue());
    }

    @DexIgnore
    public final void J() {
        g();
        if (A()) {
            b().B().a("Inactivity, disconnecting from the service");
            G();
        }
    }

    @DexIgnore
    public final void K() {
        g();
        b().B().a("Processing queued up service tasks", Integer.valueOf(this.h.size()));
        for (Runnable run : this.h) {
            try {
                run.run();
            } catch (Exception e2) {
                b().t().a("Task exception while flushing queue", e2);
            }
        }
        this.h.clear();
        this.i.c();
    }

    @DexIgnore
    public final void a(l43 l43, e22 e22, ra3 ra3) {
        int i2;
        List<e22> a;
        g();
        e();
        w();
        boolean H = H();
        int i3 = 0;
        int i4 = 100;
        while (i3 < 1001 && i4 == 100) {
            ArrayList arrayList = new ArrayList();
            if (!H || (a = s().a(100)) == null) {
                i2 = 0;
            } else {
                arrayList.addAll(a);
                i2 = a.size();
            }
            if (e22 != null && i2 < 100) {
                arrayList.add(e22);
            }
            int size = arrayList.size();
            int i5 = 0;
            while (i5 < size) {
                Object obj = arrayList.get(i5);
                i5++;
                e22 e222 = (e22) obj;
                if (e222 instanceof j03) {
                    try {
                        l43.a((j03) e222, ra3);
                    } catch (RemoteException e2) {
                        b().t().a("Failed to send event to the service", e2);
                    }
                } else if (e222 instanceof la3) {
                    try {
                        l43.a((la3) e222, ra3);
                    } catch (RemoteException e3) {
                        b().t().a("Failed to send attribute to the service", e3);
                    }
                } else if (e222 instanceof ab3) {
                    try {
                        l43.a((ab3) e222, ra3);
                    } catch (RemoteException e4) {
                        b().t().a("Failed to send conditional property to the service", e4);
                    }
                } else {
                    b().t().a("Discarding data. Unrecognized parcel type.");
                }
            }
            i3++;
            i4 = i2;
        }
    }

    @DexIgnore
    public final boolean z() {
        return false;
    }

    @DexIgnore
    public final void a(j03 j03, String str) {
        w12.a(j03);
        g();
        w();
        boolean H = H();
        a((Runnable) new w83(this, H, H && s().a(j03), j03, a(true), str));
    }

    @DexIgnore
    public final void a(ab3 ab3) {
        w12.a(ab3);
        g();
        w();
        d();
        a((Runnable) new v83(this, true, s().a(ab3), new ab3(ab3), a(true), ab3));
    }

    @DexIgnore
    public final void a(AtomicReference<List<ab3>> atomicReference, String str, String str2, String str3) {
        g();
        w();
        a((Runnable) new y83(this, atomicReference, str, str2, str3, a(false)));
    }

    @DexIgnore
    public final void a(ev2 ev2, String str, String str2) {
        g();
        w();
        a((Runnable) new x83(this, str, str2, a(false), ev2));
    }

    @DexIgnore
    public final void a(AtomicReference<List<la3>> atomicReference, String str, String str2, String str3, boolean z) {
        g();
        w();
        a((Runnable) new b93(this, atomicReference, str, str2, str3, z, a(false)));
    }

    @DexIgnore
    public final void a(ev2 ev2, String str, String str2, boolean z) {
        g();
        w();
        a((Runnable) new z83(this, str, str2, z, a(false), ev2));
    }

    @DexIgnore
    public final void a(la3 la3) {
        g();
        w();
        a((Runnable) new m83(this, H() && s().a(la3), la3, a(true)));
    }

    @DexIgnore
    public final void a(AtomicReference<String> atomicReference) {
        g();
        w();
        a((Runnable) new n83(this, atomicReference, a(false)));
    }

    @DexIgnore
    public final void a(ev2 ev2) {
        g();
        w();
        a((Runnable) new q83(this, a(false), ev2));
    }

    @DexIgnore
    public final void a(h83 h83) {
        g();
        w();
        a((Runnable) new s83(this, h83));
    }

    @DexIgnore
    public final void a(l43 l43) {
        g();
        w12.a(l43);
        this.d = l43;
        I();
        K();
    }

    @DexIgnore
    public final void a(ComponentName componentName) {
        g();
        if (this.d != null) {
            this.d = null;
            b().B().a("Disconnected from device MeasurementService", componentName);
            g();
            E();
        }
    }

    @DexIgnore
    public final void a(Runnable runnable) throws IllegalStateException {
        g();
        if (A()) {
            runnable.run();
        } else if (((long) this.h.size()) >= 1000) {
            b().t().a("Discarding data. Max runnable queue size reached");
        } else {
            this.h.add(runnable);
            this.i.a(60000);
            E();
        }
    }

    @DexIgnore
    public final ra3 a(boolean z) {
        d();
        return p().a(z ? b().C() : null);
    }

    @DexIgnore
    public final void a(ev2 ev2, j03 j03, String str) {
        g();
        w();
        if (j().a((int) nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE) != 0) {
            b().w().a("Not bundling data. Service unavailable or out of date");
            j().a(ev2, new byte[0]);
            return;
        }
        a((Runnable) new r83(this, j03, str, ev2));
    }
}
