package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ml4 extends Exception {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = -7323249827281485390L;

    @DexIgnore
    public ml4() {
    }

    @DexIgnore
    public ml4(String str) {
        super(str);
    }

    @DexIgnore
    public ml4(Throwable th) {
        super(th);
    }

    @DexIgnore
    public ml4(String str, Throwable th) {
        super(str, th);
    }
}
