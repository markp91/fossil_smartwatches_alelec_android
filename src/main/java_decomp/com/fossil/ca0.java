package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ca0 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ca0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new ca0(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new ca0[i];
        }
    }

    @DexIgnore
    public ca0(byte b, int i) {
        super(e90.WEATHER_COMPLICATION, b, i);
    }

    @DexIgnore
    public /* synthetic */ ca0(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
