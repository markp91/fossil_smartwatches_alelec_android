package com.fossil;

import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupActivity;
import com.portfolio.platform.uirenew.onboarding.profilesetup.ProfileSetupPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr5 implements MembersInjector<ProfileSetupActivity> {
    @DexIgnore
    public static void a(ProfileSetupActivity profileSetupActivity, ProfileSetupPresenter profileSetupPresenter) {
        profileSetupActivity.B = profileSetupPresenter;
    }
}
