package com.fossil;

import android.app.Activity;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a20 {
    @DexIgnore
    public /* final */ b20 a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ c c;
    @DexIgnore
    public /* final */ Map<String, String> d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ Map<String, Object> f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Map<String, Object> h;
    @DexIgnore
    public String i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ c a;
        @DexIgnore
        public /* final */ long b; // = System.currentTimeMillis();
        @DexIgnore
        public Map<String, String> c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public Map<String, Object> e; // = null;
        @DexIgnore
        public String f; // = null;
        @DexIgnore
        public Map<String, Object> g; // = null;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public b a(String str) {
            this.d = str;
            return this;
        }

        @DexIgnore
        public b b(Map<String, String> map) {
            this.c = map;
            return this;
        }

        @DexIgnore
        public b a(Map<String, Object> map) {
            this.e = map;
            return this;
        }

        @DexIgnore
        public a20 a(b20 b20) {
            return new a20(b20, this.b, this.a, this.c, this.d, this.e, this.f, this.g);
        }
    }

    @DexIgnore
    public enum c {
        START,
        RESUME,
        PAUSE,
        STOP,
        CRASH,
        INSTALL,
        CUSTOM,
        PREDEFINED
    }

    @DexIgnore
    public static b a(c cVar, Activity activity) {
        Map singletonMap = Collections.singletonMap("activity", activity.getClass().getName());
        b bVar = new b(cVar);
        bVar.b(singletonMap);
        return bVar;
    }

    @DexIgnore
    public String toString() {
        if (this.i == null) {
            this.i = "[" + a20.class.getSimpleName() + ": " + "timestamp=" + this.b + ", type=" + this.c + ", details=" + this.d + ", customType=" + this.e + ", customAttributes=" + this.f + ", predefinedType=" + this.g + ", predefinedAttributes=" + this.h + ", metadata=[" + this.a + "]]";
        }
        return this.i;
    }

    @DexIgnore
    public a20(b20 b20, long j, c cVar, Map<String, String> map, String str, Map<String, Object> map2, String str2, Map<String, Object> map3) {
        this.a = b20;
        this.b = j;
        this.c = cVar;
        this.d = map;
        this.e = str;
        this.f = map2;
        this.g = str2;
        this.h = map3;
    }

    @DexIgnore
    public static b a(long j) {
        b bVar = new b(c.INSTALL);
        bVar.b(Collections.singletonMap("installedAt", String.valueOf(j)));
        return bVar;
    }

    @DexIgnore
    public static b a(String str) {
        Map singletonMap = Collections.singletonMap("sessionId", str);
        b bVar = new b(c.CRASH);
        bVar.b(singletonMap);
        return bVar;
    }

    @DexIgnore
    public static b a(String str, String str2) {
        b a2 = a(str);
        a2.a((Map<String, Object>) Collections.singletonMap("exceptionName", str2));
        return a2;
    }

    @DexIgnore
    public static b a(j10 j10) {
        b bVar = new b(c.CUSTOM);
        bVar.a(j10.b());
        bVar.a(j10.a());
        return bVar;
    }
}
