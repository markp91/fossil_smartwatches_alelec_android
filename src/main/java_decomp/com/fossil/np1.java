package com.fossil;

import com.fossil.ip1;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class np1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract a a(Integer num);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public final a a(String str, String str2) {
            b().put(str, str2);
            return this;
        }

        @DexIgnore
        public abstract a a(Map<String, String> map);

        @DexIgnore
        public abstract a a(byte[] bArr);

        @DexIgnore
        public abstract np1 a();

        @DexIgnore
        public abstract a b(long j);

        @DexIgnore
        public abstract Map<String, String> b();

        @DexIgnore
        public final a a(String str, long j) {
            b().put(str, String.valueOf(j));
            return this;
        }

        @DexIgnore
        public final a a(String str, int i) {
            b().put(str, String.valueOf(i));
            return this;
        }
    }

    @DexIgnore
    public static a i() {
        ip1.b bVar = new ip1.b();
        bVar.a((Map<String, String>) new HashMap());
        return bVar;
    }

    @DexIgnore
    public final String a(String str) {
        String str2 = a().get(str);
        return str2 == null ? "" : str2;
    }

    @DexIgnore
    public abstract Map<String, String> a();

    @DexIgnore
    public final int b(String str) {
        String str2 = a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Integer.valueOf(str2).intValue();
    }

    @DexIgnore
    public abstract Integer b();

    @DexIgnore
    public abstract long c();

    @DexIgnore
    public final long c(String str) {
        String str2 = a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Long.valueOf(str2).longValue();
    }

    @DexIgnore
    public final Map<String, String> d() {
        return Collections.unmodifiableMap(a());
    }

    @DexIgnore
    public abstract byte[] e();

    @DexIgnore
    public abstract String f();

    @DexIgnore
    public abstract long g();

    @DexIgnore
    public a h() {
        ip1.b bVar = new ip1.b();
        bVar.a(f());
        bVar.a(b());
        bVar.a(e());
        bVar.a(c());
        bVar.b(g());
        bVar.a((Map<String, String>) new HashMap(a()));
        return bVar;
    }
}
