package com.fossil;

import com.fossil.sq6;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yr6 {
    /*
    static {
        mt6.encodeUtf8("\"\\");
        mt6.encodeUtf8("\t ,=");
    }
    */

    @DexIgnore
    public static long a(Response response) {
        return a(response.p());
    }

    @DexIgnore
    public static boolean b(sq6 sq6) {
        return c(sq6).contains("*");
    }

    @DexIgnore
    public static boolean c(Response response) {
        return b(response.p());
    }

    @DexIgnore
    public static Set<String> d(Response response) {
        return c(response.p());
    }

    @DexIgnore
    public static sq6 e(Response response) {
        return a(response.D().I().c(), response.p());
    }

    @DexIgnore
    public static long a(sq6 sq6) {
        return a(sq6.a("Content-Length"));
    }

    @DexIgnore
    public static boolean b(Response response) {
        if (response.I().e().equals("HEAD")) {
            return false;
        }
        int n = response.n();
        if (((n >= 100 && n < 200) || n == 204 || n == 304) && a(response) == -1 && !"chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static Set<String> c(sq6 sq6) {
        Set<String> emptySet = Collections.emptySet();
        int b = sq6.b();
        Set<String> set = emptySet;
        for (int i = 0; i < b; i++) {
            if ("Vary".equalsIgnoreCase(sq6.a(i))) {
                String b2 = sq6.b(i);
                if (set.isEmpty()) {
                    set = new TreeSet<>(String.CASE_INSENSITIVE_ORDER);
                }
                for (String trim : b2.split(",")) {
                    set.add(trim.trim());
                }
            }
        }
        return set;
    }

    @DexIgnore
    public static long a(String str) {
        if (str == null) {
            return -1;
        }
        try {
            return Long.parseLong(str);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    @DexIgnore
    public static boolean a(Response response, sq6 sq6, yq6 yq6) {
        for (String next : d(response)) {
            if (!fr6.a((Object) sq6.b(next), (Object) yq6.b(next))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static sq6 a(sq6 sq6, sq6 sq62) {
        Set<String> c = c(sq62);
        if (c.isEmpty()) {
            return new sq6.a().a();
        }
        sq6.a aVar = new sq6.a();
        int b = sq6.b();
        for (int i = 0; i < b; i++) {
            String a = sq6.a(i);
            if (c.contains(a)) {
                aVar.a(a, sq6.b(i));
            }
        }
        return aVar.a();
    }

    @DexIgnore
    public static int b(String str, int i) {
        while (i < str.length() && ((r0 = str.charAt(i)) == ' ' || r0 == 9)) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static void a(lq6 lq6, tq6 tq6, sq6 sq6) {
        if (lq6 != lq6.a) {
            List<kq6> a = kq6.a(tq6, sq6);
            if (!a.isEmpty()) {
                lq6.a(tq6, a);
            }
        }
    }

    @DexIgnore
    public static int a(String str, int i, String str2) {
        while (i < str.length() && str2.indexOf(str.charAt(i)) == -1) {
            i++;
        }
        return i;
    }

    @DexIgnore
    public static int a(String str, int i) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong > 2147483647L) {
                return Integer.MAX_VALUE;
            }
            if (parseLong < 0) {
                return 0;
            }
            return (int) parseLong;
        } catch (NumberFormatException unused) {
            return i;
        }
    }
}
