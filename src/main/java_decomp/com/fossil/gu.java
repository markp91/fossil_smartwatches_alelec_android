package com.fossil;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gu implements au {
    @DexIgnore
    public static /* final */ Bitmap.Config j; // = Bitmap.Config.ARGB_8888;
    @DexIgnore
    public /* final */ hu a;
    @DexIgnore
    public /* final */ Set<Bitmap.Config> b;
    @DexIgnore
    public /* final */ a c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h;
    @DexIgnore
    public int i;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(Bitmap bitmap);

        @DexIgnore
        void b(Bitmap bitmap);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements a {
        @DexIgnore
        public void a(Bitmap bitmap) {
        }

        @DexIgnore
        public void b(Bitmap bitmap) {
        }
    }

    @DexIgnore
    public gu(long j2, hu huVar, Set<Bitmap.Config> set) {
        this.d = j2;
        this.a = huVar;
        this.b = set;
        this.c = new b();
    }

    @DexIgnore
    @TargetApi(26)
    public static Set<Bitmap.Config> f() {
        HashSet hashSet = new HashSet(Arrays.asList(Bitmap.Config.values()));
        if (Build.VERSION.SDK_INT >= 19) {
            hashSet.add((Object) null);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            hashSet.remove(Bitmap.Config.HARDWARE);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    @DexIgnore
    public static hu g() {
        if (Build.VERSION.SDK_INT >= 19) {
            return new ju();
        }
        return new yt();
    }

    @DexIgnore
    public synchronized void a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                if (!bitmap.isRecycled()) {
                    if (bitmap.isMutable() && ((long) this.a.b(bitmap)) <= this.d) {
                        if (this.b.contains(bitmap.getConfig())) {
                            int b2 = this.a.b(bitmap);
                            this.a.a(bitmap);
                            this.c.b(bitmap);
                            this.h++;
                            this.e += (long) b2;
                            if (Log.isLoggable("LruBitmapPool", 2)) {
                                Log.v("LruBitmapPool", "Put bitmap in pool=" + this.a.c(bitmap));
                            }
                            b();
                            d();
                            return;
                        }
                    }
                    if (Log.isLoggable("LruBitmapPool", 2)) {
                        Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.a.c(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.b.contains(bitmap.getConfig()));
                    }
                    bitmap.recycle();
                    return;
                }
                throw new IllegalStateException("Cannot pool recycled bitmap");
            } catch (Throwable th) {
                throw th;
            }
        } else {
            throw new NullPointerException("Bitmap must not be null");
        }
    }

    @DexIgnore
    public Bitmap b(int i2, int i3, Bitmap.Config config) {
        Bitmap c2 = c(i2, i3, config);
        return c2 == null ? d(i2, i3, config) : c2;
    }

    @DexIgnore
    public final synchronized Bitmap c(int i2, int i3, Bitmap.Config config) {
        Bitmap a2;
        a(config);
        a2 = this.a.a(i2, i3, config != null ? config : j);
        if (a2 == null) {
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Missing bitmap=" + this.a.b(i2, i3, config));
            }
            this.g++;
        } else {
            this.f++;
            this.e -= (long) this.a.b(a2);
            this.c.a(a2);
            c(a2);
        }
        if (Log.isLoggable("LruBitmapPool", 2)) {
            Log.v("LruBitmapPool", "Get bitmap=" + this.a.b(i2, i3, config));
        }
        b();
        return a2;
    }

    @DexIgnore
    public final void d() {
        a(this.d);
    }

    @DexIgnore
    public long e() {
        return this.d;
    }

    @DexIgnore
    public static Bitmap d(int i2, int i3, Bitmap.Config config) {
        if (config == null) {
            config = j;
        }
        return Bitmap.createBitmap(i2, i3, config);
    }

    @DexIgnore
    @TargetApi(19)
    public static void b(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    @DexIgnore
    public final void b() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            c();
        }
    }

    @DexIgnore
    public gu(long j2) {
        this(j2, g(), f());
    }

    @DexIgnore
    public static void c(Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        b(bitmap);
    }

    @DexIgnore
    public final void c() {
        Log.v("LruBitmapPool", "Hits=" + this.f + ", misses=" + this.g + ", puts=" + this.h + ", evictions=" + this.i + ", currentSize=" + this.e + ", maxSize=" + this.d + "\nStrategy=" + this.a);
    }

    @DexIgnore
    public Bitmap a(int i2, int i3, Bitmap.Config config) {
        Bitmap c2 = c(i2, i3, config);
        if (c2 == null) {
            return d(i2, i3, config);
        }
        c2.eraseColor(0);
        return c2;
    }

    @DexIgnore
    @TargetApi(26)
    public static void a(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE) {
            throw new IllegalArgumentException("Cannot create a mutable Bitmap with config: " + config + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        }
    }

    @DexIgnore
    public void a() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        a(0);
    }

    @DexIgnore
    @SuppressLint({"InlinedApi"})
    public void a(int i2) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "trimMemory, level=" + i2);
        }
        if (i2 >= 40 || (Build.VERSION.SDK_INT >= 23 && i2 >= 20)) {
            a();
        } else if (i2 >= 20 || i2 == 15) {
            a(e() / 2);
        }
    }

    @DexIgnore
    public final synchronized void a(long j2) {
        while (this.e > j2) {
            Bitmap a2 = this.a.a();
            if (a2 == null) {
                if (Log.isLoggable("LruBitmapPool", 5)) {
                    Log.w("LruBitmapPool", "Size mismatch, resetting");
                    c();
                }
                this.e = 0;
                return;
            }
            this.c.a(a2);
            this.e -= (long) this.a.b(a2);
            this.i++;
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Evicting bitmap=" + this.a.c(a2));
            }
            b();
            a2.recycle();
        }
    }
}
