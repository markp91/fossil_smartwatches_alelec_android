package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.model.ServerSetting;
import com.portfolio.platform.data.model.diana.workout.WorkoutSession;
import com.portfolio.platform.data.source.local.diana.workout.WorkoutSessionDifference;
import com.portfolio.platform.manager.ThemeManager;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sh5 extends df<WorkoutSession, b> {
    @DexIgnore
    public /* final */ String d; // = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
    @DexIgnore
    public c e;
    @DexIgnore
    public zh4 f;
    @DexIgnore
    public String g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public WorkoutSession a;
        @DexIgnore
        public /* final */ fh4 b;
        @DexIgnore
        public /* final */ /* synthetic */ sh5 c;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sh5 sh5, fh4 fh4) {
            super(fh4.d());
            wg6.b(fh4, "mBinding");
            this.c = sh5;
            this.b = fh4;
            String a2 = sh5.d;
            if (a2 != null) {
                this.b.q.setBackgroundColor(Color.parseColor(a2));
                ImageView imageView = this.b.E;
                wg6.a((Object) imageView, "mBinding.ivWorkout");
                Drawable background = imageView.getBackground();
                if (background instanceof ShapeDrawable) {
                    Paint paint = ((ShapeDrawable) background).getPaint();
                    wg6.a((Object) paint, "drawable.paint");
                    paint.setColor(Color.parseColor(a2));
                } else if (background instanceof GradientDrawable) {
                    ((GradientDrawable) background).setColor(Color.parseColor(a2));
                } else if (background instanceof ColorDrawable) {
                    ((ColorDrawable) background).setColor(Color.parseColor(a2));
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v13, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v15, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v21, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v23, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v28, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r1v30, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r1v32, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r1v34, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r1v36, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r1v38, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r1v40, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r1v42, types: [android.widget.ImageView, com.portfolio.platform.view.RTLImageView] */
        /* JADX WARNING: type inference failed for: r1v44, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r1v46, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        /* JADX WARNING: type inference failed for: r4v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(WorkoutSession workoutSession) {
            int i;
            wg6.b(workoutSession, ServerSetting.VALUE);
            this.a = workoutSession;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("WorkoutAdapter", "build - value=" + workoutSession);
            sh5 sh5 = this.c;
            View d = this.b.d();
            wg6.a((Object) d, "mBinding.root");
            Context context = d.getContext();
            wg6.a((Object) context, "mBinding.root.context");
            WorkoutSession workoutSession2 = this.a;
            if (workoutSession2 != null) {
                d a2 = sh5.a(context, workoutSession2);
                this.b.E.setImageResource(a2.f());
                Object r0 = this.b.y;
                wg6.a((Object) r0, "mBinding.ftvWorkoutTitle");
                r0.setText(a2.i());
                Object r02 = this.b.x;
                wg6.a((Object) r02, "mBinding.ftvWorkoutTime");
                r02.setText(a2.h());
                Object r03 = this.b.r;
                wg6.a((Object) r03, "mBinding.ftvActiveTime");
                r03.setText(a2.a());
                Object r04 = this.b.t;
                wg6.a((Object) r04, "mBinding.ftvDistance");
                r04.setText(a2.c());
                Object r05 = this.b.w;
                wg6.a((Object) r05, "mBinding.ftvSteps");
                r05.setText(a2.g());
                Object r06 = this.b.s;
                wg6.a((Object) r06, "mBinding.ftvCalories");
                r06.setText(a2.b());
                Object r07 = this.b.u;
                wg6.a((Object) r07, "mBinding.ftvHeartRateAvg");
                r07.setText(a2.d());
                Object r08 = this.b.v;
                wg6.a((Object) r08, "mBinding.ftvHeartRateMax");
                r08.setText(a2.e());
                if (TextUtils.isEmpty(this.c.g)) {
                    i = w6.a(PortfolioApp.get.instance(), 2131099815);
                } else {
                    i = Color.parseColor(this.c.g);
                }
                ColorStateList valueOf = ColorStateList.valueOf(i);
                wg6.a((Object) valueOf, "ColorStateList.valueOf(color)");
                int i2 = th5.a[this.c.e.ordinal()];
                if (i2 == 1) {
                    this.b.z.setColorFilter(i);
                    this.b.r.setTextColor(i);
                } else if (i2 == 2) {
                    this.b.D.setColorFilter(i);
                    this.b.w.setTextColor(i);
                } else if (i2 == 3) {
                    this.b.A.setColorFilter(i);
                    this.b.s.setTextColor(i);
                } else if (i2 == 4) {
                    this.b.B.setColorFilter(i);
                    this.b.C.setColorFilter(i);
                    this.b.u.setTextColor(i);
                    this.b.v.setTextColor(i);
                }
                ImageView imageView = this.b.E;
                wg6.a((Object) imageView, "mBinding.ivWorkout");
                imageView.setImageTintList(valueOf);
                return;
            }
            wg6.d("mValue");
            throw null;
        }
    }

    @DexIgnore
    public enum c {
        ACTIVE_TIME,
        STEPS,
        CALORIES,
        HEART_RATE
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public String g;
        @DexIgnore
        public String h;
        @DexIgnore
        public String i;

        @DexIgnore
        public d(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
            wg6.b(str, "mTitle");
            wg6.b(str2, "mTime");
            wg6.b(str3, "mActiveMinute");
            wg6.b(str4, "mDistance");
            wg6.b(str5, "mSteps");
            wg6.b(str6, "mCalories");
            wg6.b(str7, "mHeartRateAvg");
            wg6.b(str8, "mHeartRateMax");
            this.a = i2;
            this.b = str;
            this.c = str2;
            this.d = str3;
            this.e = str4;
            this.f = str5;
            this.g = str6;
            this.h = str7;
            this.i = str8;
        }

        @DexIgnore
        public final String a() {
            return this.d;
        }

        @DexIgnore
        public final String b() {
            return this.g;
        }

        @DexIgnore
        public final String c() {
            return this.e;
        }

        @DexIgnore
        public final String d() {
            return this.h;
        }

        @DexIgnore
        public final String e() {
            return this.i;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof d)) {
                return false;
            }
            d dVar = (d) obj;
            return this.a == dVar.a && wg6.a((Object) this.b, (Object) dVar.b) && wg6.a((Object) this.c, (Object) dVar.c) && wg6.a((Object) this.d, (Object) dVar.d) && wg6.a((Object) this.e, (Object) dVar.e) && wg6.a((Object) this.f, (Object) dVar.f) && wg6.a((Object) this.g, (Object) dVar.g) && wg6.a((Object) this.h, (Object) dVar.h) && wg6.a((Object) this.i, (Object) dVar.i);
        }

        @DexIgnore
        public final int f() {
            return this.a;
        }

        @DexIgnore
        public final String g() {
            return this.f;
        }

        @DexIgnore
        public final String h() {
            return this.c;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = d.a(this.a) * 31;
            String str = this.b;
            int i2 = 0;
            int hashCode = (a2 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            String str3 = this.d;
            int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
            String str4 = this.e;
            int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
            String str5 = this.f;
            int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
            String str6 = this.g;
            int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
            String str7 = this.h;
            int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
            String str8 = this.i;
            if (str8 != null) {
                i2 = str8.hashCode();
            }
            return hashCode7 + i2;
        }

        @DexIgnore
        public final String i() {
            return this.b;
        }

        @DexIgnore
        public String toString() {
            return "WorkoutModel(mResIconId=" + this.a + ", mTitle=" + this.b + ", mTime=" + this.c + ", mActiveMinute=" + this.d + ", mDistance=" + this.e + ", mSteps=" + this.f + ", mCalories=" + this.g + ", mHeartRateAvg=" + this.h + ", mHeartRateMax=" + this.i + ")";
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sh5(c cVar, zh4 zh4, WorkoutSessionDifference workoutSessionDifference, String str) {
        super(workoutSessionDifference);
        wg6.b(cVar, "mWorkoutItem");
        wg6.b(zh4, "mDistanceUnit");
        wg6.b(workoutSessionDifference, "workoutSessionDiff");
        this.e = cVar;
        this.f = zh4;
        this.g = str;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        fh4 a2 = fh4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemWorkoutBinding.infla\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(zh4 zh4, cf<WorkoutSession> cfVar) {
        wg6.b(zh4, MFUser.DISTANCE_UNIT);
        wg6.b(cfVar, "data");
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("WorkoutAdapter", "setData - distanceUnit=" + zh4 + ", data=" + cfVar.size());
        this.f = zh4;
        b(cfVar);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        WorkoutSession workoutSession = (WorkoutSession) getItem(i);
        if (workoutSession != null) {
            bVar.a(workoutSession);
        }
    }

    @DexIgnore
    public final d a(Context context, WorkoutSession workoutSession) {
        String format;
        String str;
        String str2;
        String a2 = bk4.a(workoutSession.getStartTime().getMillis(), workoutSession.getTimezoneOffsetInSecond());
        pc6<Integer, Integer, Integer> c2 = bk4.c(workoutSession.getDuration());
        wg6.a((Object) c2, "DateHelper.getTimeValues(workoutSession.duration)");
        if (wg6.a(c2.getFirst().intValue(), 0) > 0) {
            nh6 nh6 = nh6.a;
            String a3 = jm4.a(context, 2131886485);
            wg6.a((Object) a3, "LanguageHelper.getString\u2026rHrsNumberMinsNumberSecs)");
            Object[] objArr = {c2.getFirst(), c2.getSecond(), c2.getThird()};
            format = String.format(a3, Arrays.copyOf(objArr, objArr.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
        } else if (wg6.a(c2.getSecond().intValue(), 0) > 0) {
            nh6 nh62 = nh6.a;
            String a4 = jm4.a(context, 2131886486);
            wg6.a((Object) a4, "LanguageHelper.getString\u2026xt__NumberMinsNumberSecs)");
            Object[] objArr2 = {c2.getSecond(), c2.getThird()};
            format = String.format(a4, Arrays.copyOf(objArr2, objArr2.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
        } else {
            nh6 nh63 = nh6.a;
            String a5 = jm4.a(context, 2131886487);
            wg6.a((Object) a5, "LanguageHelper.getString\u2026ailPage_Text__NumberSecs)");
            Object[] objArr3 = {c2.getThird()};
            format = String.format(a5, Arrays.copyOf(objArr3, objArr3.length));
            wg6.a((Object) format, "java.lang.String.format(format, *args)");
        }
        String str3 = format;
        if (workoutSession.getWorkoutType() != fi4.RUNNING) {
            str = jm4.a(context, 2131887066);
        } else {
            bl4 bl4 = bl4.a;
            Double totalDistance = workoutSession.getTotalDistance();
            str = bl4.a(totalDistance != null ? Float.valueOf((float) totalDistance.doubleValue()) : null, this.f);
        }
        if (this.f == zh4.IMPERIAL) {
            str2 = str + " " + jm4.a(context, 2131886396);
        } else {
            str2 = str + " " + jm4.a(context, 2131886395);
        }
        nh6 nh64 = nh6.a;
        String a6 = jm4.a(context, 2131886469);
        wg6.a((Object) a6, "LanguageHelper.getString\u2026sToday_Text__NumberSteps)");
        Object[] objArr4 = {bl4.a.b(workoutSession.getTotalSteps())};
        String format2 = String.format(a6, Arrays.copyOf(objArr4, objArr4.length));
        wg6.a((Object) format2, "java.lang.String.format(format, *args)");
        String str4 = bl4.a.a(workoutSession.getTotalCalorie()) + " " + jm4.a(context, 2131886392);
        StringBuilder sb = new StringBuilder();
        Float averageHeartRate = workoutSession.getAverageHeartRate();
        float f2 = 0.0f;
        sb.append(tk4.b(averageHeartRate != null ? averageHeartRate.floatValue() : 0.0f, 0));
        sb.append(" ");
        sb.append(jm4.a(context, 2131886393));
        String sb2 = sb.toString();
        StringBuilder sb3 = new StringBuilder();
        Integer maxHeartRate = workoutSession.getMaxHeartRate();
        if (maxHeartRate != null) {
            f2 = (float) maxHeartRate.intValue();
        }
        sb3.append(tk4.b(f2, 0));
        sb3.append(" ");
        sb3.append(jm4.a(context, 2131886394));
        String sb4 = sb3.toString();
        lc6<Integer, Integer> a7 = fi4.Companion.a(workoutSession.getWorkoutType());
        String a8 = jm4.a(context, a7.getSecond().intValue());
        int intValue = a7.getFirst().intValue();
        wg6.a((Object) a8, Explore.COLUMN_TITLE);
        wg6.a((Object) a2, LogBuilder.KEY_TIME);
        return new d(intValue, a8, a2, str3, str2, format2, str4, sb2, sb4);
    }
}
