package com.fossil;

import android.os.Parcelable;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$checkSettingOfSelectedWatchApp$1$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class p75$b$a extends sf6 implements ig6<il6, xe6<? super Parcelable>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p75$b$a(WatchAppsPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        p75$b$a p75_b_a = new p75$b$a(this.this$0, xe6);
        p75_b_a.p$ = (il6) obj;
        return p75_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((p75$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return WatchAppsPresenter.f(this.this$0.this$0).g(this.this$0.$id);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
