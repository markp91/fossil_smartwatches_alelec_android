package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u5 extends j5 {
    @DexIgnore
    public ArrayList<j5> k0; // = new ArrayList<>();

    @DexIgnore
    public void E() {
        this.k0.clear();
        super.E();
    }

    @DexIgnore
    public void I() {
        super.I();
        ArrayList<j5> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                j5 j5Var = this.k0.get(i);
                j5Var.b(h(), i());
                if (!(j5Var instanceof k5)) {
                    j5Var.I();
                }
            }
        }
    }

    @DexIgnore
    public k5 K() {
        j5 l = l();
        k5 k5Var = this instanceof k5 ? (k5) this : null;
        while (l != null) {
            j5 l2 = l.l();
            if (l instanceof k5) {
                k5Var = (k5) l;
            }
            l = l2;
        }
        return k5Var;
    }

    @DexIgnore
    public void L() {
        I();
        ArrayList<j5> arrayList = this.k0;
        if (arrayList != null) {
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                j5 j5Var = this.k0.get(i);
                if (j5Var instanceof u5) {
                    ((u5) j5Var).L();
                }
            }
        }
    }

    @DexIgnore
    public void M() {
        this.k0.clear();
    }

    @DexIgnore
    public void a(x4 x4Var) {
        super.a(x4Var);
        int size = this.k0.size();
        for (int i = 0; i < size; i++) {
            this.k0.get(i).a(x4Var);
        }
    }

    @DexIgnore
    public void b(j5 j5Var) {
        this.k0.add(j5Var);
        if (j5Var.l() != null) {
            ((u5) j5Var.l()).c(j5Var);
        }
        j5Var.a((j5) this);
    }

    @DexIgnore
    public void c(j5 j5Var) {
        this.k0.remove(j5Var);
        j5Var.a((j5) null);
    }

    @DexIgnore
    public void b(int i, int i2) {
        super.b(i, i2);
        int size = this.k0.size();
        for (int i3 = 0; i3 < size; i3++) {
            this.k0.get(i3).b(p(), q());
        }
    }
}
