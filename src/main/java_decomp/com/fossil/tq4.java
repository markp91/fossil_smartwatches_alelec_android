package com.fossil;

import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.ChangeTransform;
import android.transition.Fade;
import android.transition.Slide;
import android.transition.Transition;
import android.transition.TransitionSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tq4 {
    @DexIgnore
    public static /* final */ tq4 a; // = new tq4();

    @DexIgnore
    public final Transition a() {
        Fade fade = new Fade();
        fade.excludeTarget(16908336, true);
        fade.excludeTarget(16908335, true);
        return fade;
    }

    @DexIgnore
    public final void a(View view) {
        wg6.b(view, "view");
        view.setVisibility(0);
        TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) view.getHeight(), 0.0f);
        translateAnimation.setDuration(500);
        translateAnimation.setFillAfter(true);
        view.startAnimation(translateAnimation);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final TransitionSet a(long j) {
        TransitionSet transitionSet = new TransitionSet();
        Slide slide = new Slide(80);
        slide.setInterpolator(AnimationUtils.loadInterpolator(PortfolioApp.get.instance(), 17563662));
        slide.setDuration(j);
        slide.excludeTarget(16908336, true);
        slide.excludeTarget(16908335, true);
        transitionSet.addTransition(slide);
        return transitionSet;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v0, types: [android.app.Application, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final Transition a(PortfolioApp portfolioApp) {
        Object r0 = portfolioApp;
        wg6.b(r0, "mApp");
        TransitionSet transitionSet = new TransitionSet();
        transitionSet.setOrdering(0);
        transitionSet.setDuration(500);
        ChangeImageTransform changeImageTransform = new ChangeImageTransform();
        changeImageTransform.addTarget(r0.getString(2131887342));
        changeImageTransform.addTarget(r0.getString(2131887343));
        transitionSet.addTransition(changeImageTransform);
        ChangeBounds changeBounds = new ChangeBounds();
        changeBounds.addTarget(r0.getString(2131887342));
        changeBounds.addTarget(r0.getString(2131887343));
        changeBounds.addTarget(r0.getString(2131887348));
        changeBounds.addTarget(r0.getString(2131887341));
        changeBounds.addTarget(r0.getString(2131887338));
        changeBounds.addTarget(r0.getString(2131887340));
        changeBounds.addTarget(r0.getString(2131887339));
        changeBounds.addTarget(r0.getString(2131887352));
        changeBounds.addTarget(r0.getString(2131887351));
        changeBounds.addTarget(r0.getString(2131887350));
        changeBounds.addTarget(r0.getString(2131887346));
        changeBounds.addTarget(r0.getString(2131887345));
        changeBounds.addTarget(r0.getString(2131887347));
        changeBounds.addTarget(r0.getString(2131887349));
        transitionSet.addTransition(changeBounds);
        ChangeTransform changeTransform = new ChangeTransform();
        changeTransform.addTarget(r0.getString(2131887342));
        changeTransform.addTarget(r0.getString(2131887343));
        changeTransform.addTarget(r0.getString(2131887348));
        changeTransform.addTarget(r0.getString(2131887341));
        changeTransform.addTarget(r0.getString(2131887338));
        changeTransform.addTarget(r0.getString(2131887340));
        changeTransform.addTarget(r0.getString(2131887339));
        changeTransform.addTarget(r0.getString(2131887352));
        changeTransform.addTarget(r0.getString(2131887351));
        changeTransform.addTarget(r0.getString(2131887350));
        changeTransform.addTarget(r0.getString(2131887346));
        changeTransform.addTarget(r0.getString(2131887347));
        changeTransform.addTarget(r0.getString(2131887345));
        changeTransform.addTarget(r0.getString(2131887349));
        transitionSet.addTransition(changeTransform);
        Fade fade = new Fade();
        fade.addTarget(r0.getString(2131887349));
        fade.addTarget(r0.getString(2131887344));
        transitionSet.addTransition(fade);
        vq4 vq4 = new vq4();
        vq4.addTarget(r0.getString(2131887341));
        vq4.addTarget(r0.getString(2131887341));
        vq4.addTarget(r0.getString(2131887338));
        vq4.addTarget(r0.getString(2131887340));
        vq4.addTarget(r0.getString(2131887339));
        vq4.addTarget(r0.getString(2131887352));
        vq4.addTarget(r0.getString(2131887351));
        vq4.addTarget(r0.getString(2131887350));
        transitionSet.addTransition(vq4);
        uq4 uq4 = new uq4();
        uq4.addTarget(r0.getString(2131887348));
        transitionSet.addTransition(uq4);
        return transitionSet;
    }
}
