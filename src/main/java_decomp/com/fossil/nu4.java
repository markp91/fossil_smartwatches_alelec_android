package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.BaseWebViewActivity;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.manager.ThemeManager;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu4 extends RecyclerView.g<RecyclerView.ViewHolder> {
    @DexIgnore
    public List<c> a;
    @DexIgnore
    public b b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();

        @DexIgnore
        void a(c cVar);

        @DexIgnore
        void b(c cVar);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public int a;
        @DexIgnore
        public String b;
        @DexIgnore
        public String c;
        @DexIgnore
        public List<String> d;
        @DexIgnore
        public String e;
        @DexIgnore
        public boolean f;

        @DexIgnore
        public c() {
            this(0, (String) null, (String) null, (List) null, (String) null, false, 63, (qg6) null);
        }

        @DexIgnore
        public c(int i, String str, String str2, List<String> list, String str3, boolean z) {
            wg6.b(str, "shortDescription");
            wg6.b(str2, "longDescription");
            wg6.b(list, "permsSet");
            this.a = i;
            this.b = str;
            this.c = str2;
            this.d = list;
            this.e = str3;
            this.f = z;
        }

        @DexIgnore
        public final String a() {
            return this.e;
        }

        @DexIgnore
        public final boolean b() {
            return this.f;
        }

        @DexIgnore
        public final String c() {
            return this.c;
        }

        @DexIgnore
        public final int d() {
            return this.a;
        }

        @DexIgnore
        public final List<String> e() {
            return this.d;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof c)) {
                return false;
            }
            c cVar = (c) obj;
            return this.a == cVar.a && wg6.a((Object) this.b, (Object) cVar.b) && wg6.a((Object) this.c, (Object) cVar.c) && wg6.a((Object) this.d, (Object) cVar.d) && wg6.a((Object) this.e, (Object) cVar.e) && this.f == cVar.f;
        }

        @DexIgnore
        public final String f() {
            return this.b;
        }

        @DexIgnore
        public int hashCode() {
            int a2 = d.a(this.a) * 31;
            String str = this.b;
            int i = 0;
            int hashCode = (a2 + (str != null ? str.hashCode() : 0)) * 31;
            String str2 = this.c;
            int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
            List<String> list = this.d;
            int hashCode3 = (hashCode2 + (list != null ? list.hashCode() : 0)) * 31;
            String str3 = this.e;
            if (str3 != null) {
                i = str3.hashCode();
            }
            int i2 = (hashCode3 + i) * 31;
            boolean z = this.f;
            if (z) {
                z = true;
            }
            return i2 + (z ? 1 : 0);
        }

        @DexIgnore
        public String toString() {
            return "PermissionModel(permsId=" + this.a + ", shortDescription=" + this.b + ", longDescription=" + this.c + ", permsSet=" + this.d + ", extraLink=" + this.e + ", granted=" + this.f + ")";
        }

        @DexIgnore
        /* JADX WARNING: Illegal instructions before constructor call */
        public /* synthetic */ c(int i, String str, String str2, List list, String str3, boolean z, int i2, qg6 qg6) {
            this(r13, (i2 & 2) != 0 ? r1 : str, (i2 & 4) == 0 ? str2 : r1, (i2 & 8) != 0 ? new ArrayList() : list, (i2 & 16) != 0 ? null : str3, (i2 & 32) != 0 ? false : z);
            int i3 = (i2 & 1) != 0 ? 0 : i;
            String str4 = "";
        }

        @DexIgnore
        public final void a(boolean z) {
            this.f = z;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ zg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ nu4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public a(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                b a2 = this.a.b.b;
                if (a2 != null) {
                    a2.a();
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public b(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.E;
                    ImageButton imageButton = this.a.a.t;
                    wg6.a((Object) imageButton, "binding.ibInfo");
                    Context context = imageButton.getContext();
                    wg6.a((Object) context, "binding.ibInfo.context");
                    List b = this.a.b.a;
                    if (b != null) {
                        String a2 = ((c) b.get(adapterPosition)).a();
                        if (a2 != null) {
                            aVar.a(context, "", a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ d a;

            @DexIgnore
            public c(d dVar) {
                this.a = dVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.a.b.a;
                    if (b != null) {
                        c cVar = (c) b.get(adapterPosition);
                        if (xm4.d.c(cVar.d())) {
                            b a2 = this.a.b.b;
                            if (a2 != null) {
                                a2.a(cVar);
                                return;
                            }
                            return;
                        }
                        b a3 = this.a.b.b;
                        if (a3 != null) {
                            a3.b(cVar);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r2v10, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(nu4 nu4, zg4 zg4) {
            super(zg4.d());
            wg6.b(zg4, "binding");
            this.b = nu4;
            this.a = zg4;
            this.a.r.setOnClickListener(new a(this));
            this.a.t.setOnClickListener(new b(this));
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (b2 != null) {
                this.a.w.setBackgroundColor(Color.parseColor(b2));
            }
            this.a.q.setOnClickListener(new c(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v5, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v7, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v16, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        public final void a(c cVar) {
            wg6.b(cVar, "permissionModel");
            int i = 0;
            if (this.b.d) {
                Object r0 = this.a.r;
                wg6.a((Object) r0, "binding.fbSkip");
                r0.setVisibility(0);
            }
            if (cVar.d() == 2 || cVar.d() == 3) {
                this.a.v.setImageResource(2131231118);
            }
            Object r02 = this.a.s;
            wg6.a((Object) r02, "binding.ftvDescription");
            r02.setText(cVar.f());
            Object r03 = this.a.q;
            wg6.a((Object) r03, "binding.fbGrantPermission");
            r03.setEnabled(!cVar.b());
            ImageView imageView = this.a.u;
            wg6.a((Object) imageView, "binding.ivCheck");
            imageView.setVisibility(cVar.b() ? 0 : 4);
            ImageButton imageButton = this.a.t;
            wg6.a((Object) imageButton, "binding.ibInfo");
            if (cVar.a() == null) {
                i = 4;
            }
            imageButton.setVisibility(i);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ vg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ nu4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public a(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            /* JADX WARNING: type inference failed for: r1v2, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton, java.lang.Object] */
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    BaseWebViewActivity.a aVar = BaseWebViewActivity.E;
                    Object r1 = this.a.a.s;
                    wg6.a((Object) r1, "binding.ibInfo");
                    Context context = r1.getContext();
                    wg6.a((Object) context, "binding.ibInfo.context");
                    List b = this.a.b.a;
                    if (b != null) {
                        String a2 = ((c) b.get(adapterPosition)).a();
                        if (a2 != null) {
                            aVar.a(context, "", a2);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ e a;

            @DexIgnore
            public b(e eVar) {
                this.a = eVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List b = this.a.b.a;
                    if (b != null) {
                        c cVar = (c) b.get(adapterPosition);
                        if (xm4.d.c(cVar.d())) {
                            b a2 = this.a.b.b;
                            if (a2 != null) {
                                a2.a(cVar);
                                return;
                            }
                            return;
                        }
                        b a3 = this.a.b.b;
                        if (a3 != null) {
                            a3.b(cVar);
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton] */
        /* JADX WARNING: type inference failed for: r2v8, types: [android.widget.Button, com.portfolio.platform.view.FlexibleButton] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(nu4 nu4, vg4 vg4) {
            super(vg4.d());
            wg6.b(vg4, "binding");
            this.b = nu4;
            this.a = vg4;
            this.a.s.setOnClickListener(new a(this));
            String b2 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (b2 != null) {
                this.a.t.setBackgroundColor(Color.parseColor(b2));
                this.a.u.setColorFilter(Color.parseColor(b2), PorterDuff.Mode.SRC_ATOP);
            }
            this.a.q.setOnClickListener(new b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v4, types: [android.widget.Button, java.lang.Object, com.portfolio.platform.view.FlexibleButton] */
        /* JADX WARNING: type inference failed for: r0v8, types: [android.widget.ImageButton, com.portfolio.platform.view.FlexibleImageButton, java.lang.Object] */
        public final void a(c cVar) {
            wg6.b(cVar, "permissionModel");
            Object r0 = this.a.r;
            wg6.a((Object) r0, "binding.ftvDescription");
            r0.setText(cVar.f());
            Object r02 = this.a.q;
            wg6.a((Object) r02, "binding.fbGrantPermission");
            r02.setEnabled(!cVar.b());
            ImageView imageView = this.a.u;
            wg6.a((Object) imageView, "binding.ivCheck");
            int i = 0;
            imageView.setVisibility(cVar.b() ? 0 : 4);
            Object r03 = this.a.s;
            wg6.a((Object) r03, "binding.ibInfo");
            if (cVar.a() == null) {
                i = 4;
            }
            r03.setVisibility(i);
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public nu4(int i, boolean z) {
        this.c = i;
        this.d = z;
    }

    @DexIgnore
    public int getItemCount() {
        List<c> list = this.a;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        wg6.b(viewHolder, "holder");
        if (viewHolder instanceof e) {
            e eVar = (e) viewHolder;
            List<c> list = this.a;
            if (list != null) {
                eVar.a(list.get(i));
            } else {
                wg6.a();
                throw null;
            }
        } else {
            d dVar = (d) viewHolder;
            List<c> list2 = this.a;
            if (list2 != null) {
                dVar.a(list2.get(i));
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        if (this.c != 1) {
            zg4 a2 = zg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
            wg6.a((Object) a2, "ItemSinglePermissionBind\u2026.context), parent, false)");
            return new d(this, a2);
        }
        vg4 a3 = vg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a3, "ItemPermissionBinding.in\u2026.context), parent, false)");
        return new e(this, a3);
    }

    @DexIgnore
    public final void a(List<c> list) {
        wg6.b(list, "permissionList");
        this.a = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(b bVar) {
        wg6.b(bVar, "listener");
        this.b = bVar;
    }
}
