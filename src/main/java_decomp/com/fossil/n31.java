package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n31 extends xg6 implements hg6<if1, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ ii1 a;
    @DexIgnore
    public /* final */ /* synthetic */ if1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public n31(ii1 ii1, if1 if1) {
        super(1);
        this.a = ii1;
        this.b = if1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        if1 if1 = (if1) obj;
        if (!fm0.f.a(this.a.r, this.b)) {
            if1.a(sk1.REQUEST_UNSUPPORTED);
        }
        return cd6.a;
    }
}
