package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ short b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<y60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final y60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new y60(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", ", "require: 2"));
        }

        @DexIgnore
        public y60 createFromParcel(Parcel parcel) {
            return new y60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new y60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m72createFromParcel(Parcel parcel) {
            return new y60(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public y60(short s) {
        super(s60.SECOND_TIMEZONE_OFFSET);
        this.b = s;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(this.b).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        short s = this.b;
        if (!(s == 1024 || (-720 <= s && 840 >= s))) {
            StringBuilder b2 = ze0.b("secondTimezoneOffsetInMinute (");
            b2.append(this.b);
            b2.append(") ");
            b2.append(" must be equal to 1024 ");
            b2.append(" or in range ");
            b2.append("[-720, ");
            b2.append("840].");
            throw new IllegalArgumentException(b2.toString());
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(y60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((y60) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.SecondTimezoneOffsetConfig");
    }

    @DexIgnore
    public final short getSecondTimezoneOffsetInMinute() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (super.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.b));
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(jSONObject, bm0.TIMEZONE_OFFSET_IN_MINUTE, (Object) Short.valueOf(this.b));
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ y60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = (short) parcel.readInt();
        e();
    }
}
