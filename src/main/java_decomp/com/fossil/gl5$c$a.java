package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.uirenew.home.profile.help.HelpPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.help.HelpPresenter$logSendOpenFeedbackEvent$1$skuModel$1", f = "HelpPresenter.kt", l = {}, m = "invokeSuspend")
public final class gl5$c$a extends sf6 implements ig6<il6, xe6<? super SKUModel>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HelpPresenter.c this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gl5$c$a(HelpPresenter.c cVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = cVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        gl5$c$a gl5_c_a = new gl5$c$a(this.this$0, xe6);
        gl5_c_a.p$ = (il6) obj;
        return gl5_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((gl5$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.f.getSkuModelBySerialPrefix(PortfolioApp.get.instance().e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
