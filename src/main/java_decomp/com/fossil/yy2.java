package com.fossil;

import android.graphics.Bitmap;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yy2 {
    @DexIgnore
    public static mh2 a;

    @DexIgnore
    public static mh2 a() {
        mh2 mh2 = a;
        w12.a(mh2, (Object) "IBitmapDescriptorFactory is not initialized");
        return mh2;
    }

    @DexIgnore
    public static void a(mh2 mh2) {
        if (a == null) {
            w12.a(mh2);
            a = mh2;
        }
    }

    @DexIgnore
    public static xy2 a(Bitmap bitmap) {
        try {
            return new xy2(a().zza(bitmap));
        } catch (RemoteException e) {
            throw new bz2(e);
        }
    }
}
