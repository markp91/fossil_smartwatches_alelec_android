package com.fossil;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt3 extends TypeAdapter<Date> {
    @DexIgnore
    public /* final */ Class<? extends Date> a;
    @DexIgnore
    public /* final */ List<DateFormat> b; // = new ArrayList();

    @DexIgnore
    public yt3(Class<? extends Date> cls, String str) {
        a(cls);
        this.a = cls;
        this.b.add(new SimpleDateFormat(str, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.b.add(new SimpleDateFormat(str));
        }
    }

    @DexIgnore
    public static Class<? extends Date> a(Class<? extends Date> cls) {
        if (cls == Date.class || cls == java.sql.Date.class || cls == Timestamp.class) {
            return cls;
        }
        throw new IllegalArgumentException("Date type must be one of " + Date.class + ", " + Timestamp.class + ", or " + java.sql.Date.class + " but was " + cls);
    }

    @DexIgnore
    public String toString() {
        DateFormat dateFormat = this.b.get(0);
        if (dateFormat instanceof SimpleDateFormat) {
            return "DefaultDateTypeAdapter(" + ((SimpleDateFormat) dateFormat).toPattern() + ')';
        }
        return "DefaultDateTypeAdapter(" + dateFormat.getClass().getSimpleName() + ')';
    }

    @DexIgnore
    public Date read(JsonReader jsonReader) throws IOException {
        if (jsonReader.N() == rv3.NULL) {
            jsonReader.K();
            return null;
        }
        Date a2 = a(jsonReader.L());
        Class<? extends Date> cls = this.a;
        if (cls == Date.class) {
            return a2;
        }
        if (cls == Timestamp.class) {
            return new Timestamp(a2.getTime());
        }
        if (cls == java.sql.Date.class) {
            return new java.sql.Date(a2.getTime());
        }
        throw new AssertionError();
    }

    @DexIgnore
    public void write(JsonWriter jsonWriter, Date date) throws IOException {
        if (date == null) {
            jsonWriter.F();
            return;
        }
        synchronized (this.b) {
            jsonWriter.h(this.b.get(0).format(date));
        }
    }

    @DexIgnore
    public final Date a(String str) {
        synchronized (this.b) {
            for (DateFormat parse : this.b) {
                try {
                    Date parse2 = parse.parse(str);
                    return parse2;
                } catch (ParseException unused) {
                }
            }
            try {
                Date a2 = nv3.a(str, new ParsePosition(0));
                return a2;
            } catch (ParseException e) {
                throw new qu3(str, e);
            }
        }
    }

    @DexIgnore
    public yt3(Class<? extends Date> cls, int i, int i2) {
        a(cls);
        this.a = cls;
        this.b.add(DateFormat.getDateTimeInstance(i, i2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.b.add(DateFormat.getDateTimeInstance(i, i2));
        }
        if (bv3.c()) {
            this.b.add(gv3.a(i, i2));
        }
    }
}
