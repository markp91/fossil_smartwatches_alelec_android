package com.fossil;

import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.DashBar;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextInputEditText;
import com.portfolio.platform.view.FlexibleTextInputLayout;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ld4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton A;
    @DexIgnore
    public /* final */ FlexibleButton B;
    @DexIgnore
    public /* final */ FlexibleButton C;
    @DexIgnore
    public /* final */ FlexibleTextView D;
    @DexIgnore
    public /* final */ FlexibleTextView E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout H;
    @DexIgnore
    public /* final */ FlexibleTextInputLayout I;
    @DexIgnore
    public /* final */ RTLImageView J;
    @DexIgnore
    public /* final */ RTLImageView K;
    @DexIgnore
    public /* final */ RTLImageView L;
    @DexIgnore
    public /* final */ RTLImageView M;
    @DexIgnore
    public /* final */ ImageView N;
    @DexIgnore
    public /* final */ DashBar O;
    @DexIgnore
    public /* final */ ConstraintLayout P;
    @DexIgnore
    public /* final */ AppCompatCheckBox q;
    @DexIgnore
    public /* final */ AppCompatCheckBox r;
    @DexIgnore
    public /* final */ AppCompatCheckBox s;
    @DexIgnore
    public /* final */ AppCompatCheckBox t;
    @DexIgnore
    public /* final */ AppCompatCheckBox u;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText v;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText w;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText x;
    @DexIgnore
    public /* final */ FlexibleTextInputEditText y;
    @DexIgnore
    public /* final */ FlexibleButton z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ld4(Object obj, View view, int i, AppCompatCheckBox appCompatCheckBox, AppCompatCheckBox appCompatCheckBox2, AppCompatCheckBox appCompatCheckBox3, AppCompatCheckBox appCompatCheckBox4, AppCompatCheckBox appCompatCheckBox5, FlexibleTextInputEditText flexibleTextInputEditText, FlexibleTextInputEditText flexibleTextInputEditText2, FlexibleTextInputEditText flexibleTextInputEditText3, FlexibleTextInputEditText flexibleTextInputEditText4, FlexibleButton flexibleButton, FlexibleButton flexibleButton2, FlexibleButton flexibleButton3, FlexibleButton flexibleButton4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextInputLayout flexibleTextInputLayout, FlexibleTextInputLayout flexibleTextInputLayout2, FlexibleTextInputLayout flexibleTextInputLayout3, FlexibleTextInputLayout flexibleTextInputLayout4, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, ImageView imageView, DashBar dashBar, ConstraintLayout constraintLayout, ScrollView scrollView, FlexibleTextView flexibleTextView7) {
        super(obj, view, i);
        this.q = appCompatCheckBox;
        this.r = appCompatCheckBox2;
        this.s = appCompatCheckBox3;
        this.t = appCompatCheckBox4;
        this.u = appCompatCheckBox5;
        this.v = flexibleTextInputEditText;
        this.w = flexibleTextInputEditText2;
        this.x = flexibleTextInputEditText3;
        this.y = flexibleTextInputEditText4;
        this.z = flexibleButton;
        this.A = flexibleButton2;
        this.B = flexibleButton3;
        this.C = flexibleButton4;
        this.D = flexibleTextView;
        this.E = flexibleTextView2;
        this.F = flexibleTextView4;
        this.G = flexibleTextView5;
        this.H = flexibleTextInputLayout;
        this.I = flexibleTextInputLayout2;
        this.J = rTLImageView;
        this.K = rTLImageView2;
        this.L = rTLImageView3;
        this.M = rTLImageView4;
        this.N = imageView;
        this.O = dashBar;
        this.P = constraintLayout;
    }
}
