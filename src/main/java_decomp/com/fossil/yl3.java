package com.fossil;

import java.io.Serializable;
import java.lang.Enum;
import java.util.Collection;
import java.util.EnumSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yl3<E extends Enum<E>> extends im3<E> {
    @DexIgnore
    public /* final */ transient EnumSet<E> b;
    @DexIgnore
    public transient int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<E extends Enum<E>> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ EnumSet<E> delegate;

        @DexIgnore
        public b(EnumSet<E> enumSet) {
            this.delegate = enumSet;
        }

        @DexIgnore
        public Object readResolve() {
            return new yl3(this.delegate.clone());
        }
    }

    @DexIgnore
    public static im3 asImmutable(EnumSet enumSet) {
        int size = enumSet.size();
        if (size == 0) {
            return im3.of();
        }
        if (size != 1) {
            return new yl3(enumSet);
        }
        return im3.of(pm3.b(enumSet));
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return this.b.contains(obj);
    }

    @DexIgnore
    public boolean containsAll(Collection<?> collection) {
        if (collection instanceof yl3) {
            collection = ((yl3) collection).b;
        }
        return this.b.containsAll(collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof yl3) {
            obj = ((yl3) obj).b;
        }
        return this.b.equals(obj);
    }

    @DexIgnore
    public int hashCode() {
        int i = this.c;
        if (i != 0) {
            return i;
        }
        int hashCode = this.b.hashCode();
        this.c = hashCode;
        return hashCode;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.b.isEmpty();
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return true;
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.b.size();
    }

    @DexIgnore
    public String toString() {
        return this.b.toString();
    }

    @DexIgnore
    public Object writeReplace() {
        return new b(this.b);
    }

    @DexIgnore
    public yl3(EnumSet<E> enumSet) {
        this.b = enumSet;
    }

    @DexIgnore
    public jo3<E> iterator() {
        return qm3.e(this.b.iterator());
    }
}
