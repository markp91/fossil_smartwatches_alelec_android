package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hq2 {
    @DexIgnore
    public static void b(byte b, char[] cArr, int i) {
        cArr[i] = (char) b;
    }

    @DexIgnore
    public static boolean d(byte b) {
        return b >= 0;
    }

    @DexIgnore
    public static boolean e(byte b) {
        return b < -32;
    }

    @DexIgnore
    public static boolean f(byte b) {
        return b < -16;
    }

    @DexIgnore
    public static boolean g(byte b) {
        return b > -65;
    }

    @DexIgnore
    public static void b(byte b, byte b2, char[] cArr, int i) throws qn2 {
        if (b < -62 || g(b2)) {
            throw qn2.zzh();
        }
        cArr[i] = (char) (((b & 31) << 6) | (b2 & 63));
    }

    @DexIgnore
    public static void b(byte b, byte b2, byte b3, char[] cArr, int i) throws qn2 {
        if (g(b2) || ((b == -32 && b2 < -96) || ((b == -19 && b2 >= -96) || g(b3)))) {
            throw qn2.zzh();
        }
        cArr[i] = (char) (((b & 15) << 12) | ((b2 & 63) << 6) | (b3 & 63));
    }

    @DexIgnore
    public static void b(byte b, byte b2, byte b3, byte b4, char[] cArr, int i) throws qn2 {
        if (g(b2) || (((b << 28) + (b2 + 112)) >> 30) != 0 || g(b3) || g(b4)) {
            throw qn2.zzh();
        }
        byte b5 = ((b & 7) << 18) | ((b2 & 63) << 12) | ((b3 & 63) << 6) | (b4 & 63);
        cArr[i] = (char) ((b5 >>> 10) + 55232);
        cArr[i + 1] = (char) ((b5 & 1023) + 56320);
    }
}
