package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb {
    @DexIgnore
    public static /* final */ int fragment_close_enter; // = 2130771997;
    @DexIgnore
    public static /* final */ int fragment_close_exit; // = 2130771998;
    @DexIgnore
    public static /* final */ int fragment_fade_enter; // = 2130772000;
    @DexIgnore
    public static /* final */ int fragment_fade_exit; // = 2130772001;
    @DexIgnore
    public static /* final */ int fragment_fast_out_extra_slow_in; // = 2130772002;
    @DexIgnore
    public static /* final */ int fragment_open_enter; // = 2130772003;
    @DexIgnore
    public static /* final */ int fragment_open_exit; // = 2130772004;
}
