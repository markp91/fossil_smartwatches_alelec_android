package com.fossil;

import com.fossil.fn2;
import java.io.IOException;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zp2 {
    @DexIgnore
    public static /* final */ zp2 f; // = new zp2(0, new int[0], new Object[0], false);
    @DexIgnore
    public int a;
    @DexIgnore
    public int[] b;
    @DexIgnore
    public Object[] c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;

    @DexIgnore
    public zp2() {
        this(0, new int[8], new Object[8], true);
    }

    @DexIgnore
    public static zp2 a(zp2 zp2, zp2 zp22) {
        int i = zp2.a + zp22.a;
        int[] copyOf = Arrays.copyOf(zp2.b, i);
        System.arraycopy(zp22.b, 0, copyOf, zp2.a, zp22.a);
        Object[] copyOf2 = Arrays.copyOf(zp2.c, i);
        System.arraycopy(zp22.c, 0, copyOf2, zp2.a, zp22.a);
        return new zp2(i, copyOf, copyOf2, true);
    }

    @DexIgnore
    public static zp2 d() {
        return f;
    }

    @DexIgnore
    public static zp2 e() {
        return new zp2();
    }

    @DexIgnore
    public final void b(tq2 tq2) throws IOException {
        if (this.a != 0) {
            if (tq2.zza() == fn2.e.k) {
                for (int i = 0; i < this.a; i++) {
                    a(this.b[i], this.c[i], tq2);
                }
                return;
            }
            for (int i2 = this.a - 1; i2 >= 0; i2--) {
                a(this.b[i2], this.c[i2], tq2);
            }
        }
    }

    @DexIgnore
    public final int c() {
        int i;
        int i2 = this.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        for (int i4 = 0; i4 < this.a; i4++) {
            int i5 = this.b[i4];
            int i6 = i5 >>> 3;
            int i7 = i5 & 7;
            if (i7 == 0) {
                i = pm2.e(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 1) {
                i = pm2.g(i6, ((Long) this.c[i4]).longValue());
            } else if (i7 == 2) {
                i = pm2.c(i6, (yl2) this.c[i4]);
            } else if (i7 == 3) {
                i = (pm2.e(i6) << 1) + ((zp2) this.c[i4]).c();
            } else if (i7 == 5) {
                i = pm2.i(i6, ((Integer) this.c[i4]).intValue());
            } else {
                throw new IllegalStateException(qn2.zzf());
            }
            i3 += i;
        }
        this.d = i3;
        return i3;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof zp2)) {
            return false;
        }
        zp2 zp2 = (zp2) obj;
        int i = this.a;
        if (i == zp2.a) {
            int[] iArr = this.b;
            int[] iArr2 = zp2.b;
            int i2 = 0;
            while (true) {
                if (i2 >= i) {
                    z = true;
                    break;
                } else if (iArr[i2] != iArr2[i2]) {
                    z = false;
                    break;
                } else {
                    i2++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = zp2.c;
                int i3 = this.a;
                int i4 = 0;
                while (true) {
                    if (i4 >= i3) {
                        z2 = true;
                        break;
                    } else if (!objArr[i4].equals(objArr2[i4])) {
                        z2 = false;
                        break;
                    } else {
                        i4++;
                    }
                }
                return z2;
            }
        }
    }

    @DexIgnore
    public final int hashCode() {
        int i = this.a;
        int i2 = (i + 527) * 31;
        int[] iArr = this.b;
        int i3 = 17;
        int i4 = 17;
        for (int i5 = 0; i5 < i; i5++) {
            i4 = (i4 * 31) + iArr[i5];
        }
        int i6 = (i2 + i4) * 31;
        Object[] objArr = this.c;
        int i7 = this.a;
        for (int i8 = 0; i8 < i7; i8++) {
            i3 = (i3 * 31) + objArr[i8].hashCode();
        }
        return i6 + i3;
    }

    @DexIgnore
    public zp2(int i, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i;
        this.b = iArr;
        this.c = objArr;
        this.e = z;
    }

    @DexIgnore
    public final void a() {
        this.e = false;
    }

    @DexIgnore
    public final int b() {
        int i = this.d;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.a; i3++) {
            i2 += pm2.d(this.b[i3] >>> 3, (yl2) this.c[i3]);
        }
        this.d = i2;
        return i2;
    }

    @DexIgnore
    public final void a(tq2 tq2) throws IOException {
        if (tq2.zza() == fn2.e.l) {
            for (int i = this.a - 1; i >= 0; i--) {
                tq2.zza(this.b[i] >>> 3, this.c[i]);
            }
            return;
        }
        for (int i2 = 0; i2 < this.a; i2++) {
            tq2.zza(this.b[i2] >>> 3, this.c[i2]);
        }
    }

    @DexIgnore
    public static void a(int i, Object obj, tq2 tq2) throws IOException {
        int i2 = i >>> 3;
        int i3 = i & 7;
        if (i3 == 0) {
            tq2.zza(i2, ((Long) obj).longValue());
        } else if (i3 == 1) {
            tq2.zzd(i2, ((Long) obj).longValue());
        } else if (i3 == 2) {
            tq2.a(i2, (yl2) obj);
        } else if (i3 != 3) {
            if (i3 == 5) {
                tq2.zzd(i2, ((Integer) obj).intValue());
                return;
            }
            throw new RuntimeException(qn2.zzf());
        } else if (tq2.zza() == fn2.e.k) {
            tq2.zza(i2);
            ((zp2) obj).b(tq2);
            tq2.zzb(i2);
        } else {
            tq2.zzb(i2);
            ((zp2) obj).b(tq2);
            tq2.zza(i2);
        }
    }

    @DexIgnore
    public final void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < this.a; i2++) {
            so2.a(sb, i, String.valueOf(this.b[i2] >>> 3), this.c[i2]);
        }
    }

    @DexIgnore
    public final void a(int i, Object obj) {
        if (this.e) {
            int i2 = this.a;
            if (i2 == this.b.length) {
                int i3 = this.a + (i2 < 4 ? 8 : i2 >> 1);
                this.b = Arrays.copyOf(this.b, i3);
                this.c = Arrays.copyOf(this.c, i3);
            }
            int[] iArr = this.b;
            int i4 = this.a;
            iArr[i4] = i;
            this.c[i4] = obj;
            this.a = i4 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
