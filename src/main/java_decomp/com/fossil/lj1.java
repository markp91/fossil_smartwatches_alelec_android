package com.fossil;

import com.facebook.login.LoginStatusClient;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class lj1 extends vf1 {
    @DexIgnore
    public long K;
    @DexIgnore
    public byte[] L; // = new byte[0];
    @DexIgnore
    public long M;
    @DexIgnore
    public long N;
    @DexIgnore
    public boolean O;
    @DexIgnore
    public int P; // = -1;
    @DexIgnore
    public float Q;
    @DexIgnore
    public rg1 R;
    @DexIgnore
    public long S;
    @DexIgnore
    public /* final */ ne0 T;
    @DexIgnore
    public /* final */ gg6<cd6> U;

    @DexIgnore
    public lj1(dl1 dl1, short s, lx0 lx0, ue1 ue1, int i) {
        super(dl1, s, lx0, ue1, i);
        rg1 rg1 = rg1.UNKNOWN;
        this.R = rg1;
        this.T = new ne0(0, rg1, new byte[0], (JSONObject) null, 9);
        this.U = new rh1(this, ue1);
    }

    @DexIgnore
    public final JSONObject a(byte[] bArr) {
        rg1 rg1;
        byte[] bArr2 = bArr;
        boolean z = true;
        if (!this.O) {
            JSONObject jSONObject = new JSONObject();
            if (bArr2.length >= 4) {
                this.K = cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
                cw0.a(jSONObject, bm0.DATA_SIZE, (Object) Long.valueOf(this.K));
                if (this.K == 0) {
                    a(bn0.a(this.v, (lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27));
                } else {
                    if (bArr2.length >= 5) {
                        rg1 = rg1.p.a(bArr2[4]);
                    } else {
                        rg1 = rg1.FTD;
                    }
                    this.R = rg1;
                    cw0.a(jSONObject, bm0.SOCKET_ID, (Object) this.R.a);
                    this.T.b = this.R;
                    if (this.s) {
                        ue0.b.a(this.y.t).b(this.R);
                    }
                }
            } else {
                this.v = bn0.a(this.v, (lx0) null, (String) null, il0.INVALID_RESPONSE_LENGTH, (ch0) null, (sj0) null, 27);
            }
            this.O = true;
            if (this.v.c == il0.SUCCESS) {
                z = false;
            }
            this.C = z;
            byte[] array = ByteBuffer.allocate(3).order(ByteOrder.LITTLE_ENDIAN).put(dl1.EOF_REACH.a()).putShort(this.J).array();
            wg6.a(array, "ByteBuffer.allocate(1 + \u2026                 .array()");
            this.F = array;
            return jSONObject;
        }
        j();
        JSONObject jSONObject2 = new JSONObject();
        ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
        c(cw0.b(order.getInt(0)));
        b(cw0.b(order.getInt(4)));
        cw0.a(cw0.a(jSONObject2, bm0.FILE_SIZE, (Object) Long.valueOf(t())), bm0.FILE_CRC, (Object) Long.valueOf(s()));
        v();
        this.C = true;
        return jSONObject2;
    }

    @DexIgnore
    public void b(byte[] bArr) {
        this.L = bArr;
    }

    @DexIgnore
    public final boolean c(sg1 sg1) {
        return sg1.a == this.R;
    }

    @DexIgnore
    public final void f(sg1 sg1) {
        byte[] bArr;
        if (this.s) {
            bArr = lg0.b.a(this.y.t, this.R, sg1.b);
        } else {
            bArr = sg1.b;
        }
        boolean z = false;
        if (cw0.b(cw0.b((byte) (bArr[0] & 63))) == (this.P + 1) % 64) {
            if (((byte) (bArr[0] & ((byte) 128))) != ((byte) 0)) {
                z = true;
            }
            if (z) {
                a((long) LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
                a(this.U);
            } else {
                a(this.p);
            }
            this.P++;
            b(cw0.a(u(), md6.a(bArr, 1, bArr.length)));
            float length = (((float) u().length) * 1.0f) / ((float) this.K);
            if (length - this.Q > 0.001f || length == 1.0f) {
                this.Q = length;
                a(length);
            }
            if (this.S == 0) {
                a(this.T);
            }
            this.S += (long) bArr.length;
            cw0.a(cw0.a(cw0.a(this.T.d, bm0.PACKAGE_COUNT, (Object) Integer.valueOf(this.P + 1)), bm0.TRANSFERRED_DATA_SIZE, (Object) Long.valueOf(this.S)), bm0.PROCESSED_DATA_LENGTH, (Object) Integer.valueOf(u().length));
            return;
        }
        this.v = bn0.a(this.v, (lx0) null, (String) null, il0.MISS_PACKAGE, (ch0) null, (sj0) null, 27);
        this.C = true;
    }

    @DexIgnore
    public long s() {
        return this.N;
    }

    @DexIgnore
    public long t() {
        return this.M;
    }

    @DexIgnore
    public byte[] u() {
        return this.L;
    }

    @DexIgnore
    public void v() {
    }

    @DexIgnore
    public void b(long j) {
        this.N = j;
    }

    @DexIgnore
    public void c(long j) {
        this.M = j;
    }
}
