package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.ServerProtocol;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x60 extends r60 {
    @DexIgnore
    public static /* final */ b CREATOR; // = new b((qg6) null);
    @DexIgnore
    public static /* final */ short h; // = 255;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ byte d;
    @DexIgnore
    public /* final */ byte e;
    @DexIgnore
    public /* final */ short f;
    @DexIgnore
    public /* final */ a g;

    @DexIgnore
    public enum a {
        DISABLE((byte) 0),
        ENABLE((byte) 1);
        
        @DexIgnore
        public static /* final */ C0056a c; // = null;
        @DexIgnore
        public /* final */ byte a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.x60$a$a")
        /* renamed from: com.fossil.x60$a$a  reason: collision with other inner class name */
        public static final class C0056a {
            @DexIgnore
            public /* synthetic */ C0056a(qg6 qg6) {
            }

            @DexIgnore
            public final a a(byte b) throws IllegalArgumentException {
                a aVar;
                a[] values = a.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        aVar = null;
                        break;
                    }
                    aVar = values[i];
                    if (aVar.a() == b) {
                        break;
                    }
                    i++;
                }
                if (aVar != null) {
                    return aVar;
                }
                throw new IllegalArgumentException("Invalid id: " + b);
            }
        }

        /*
        static {
            c = new C0056a((qg6) null);
        }
        */

        @DexIgnore
        public a(byte b2) {
            this.a = b2;
        }

        @DexIgnore
        public final byte a() {
            return this.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Parcelable.Creator<x60> {
        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
        }

        @DexIgnore
        public final x60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 6) {
                return new x60(bArr[0], bArr[1], bArr[2], bArr[3], cw0.b(bArr[4]), a.c.a(bArr[5]));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 6"));
        }

        @DexIgnore
        public x60 createFromParcel(Parcel parcel) {
            return new x60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new x60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m68createFromParcel(Parcel parcel) {
            return new x60(parcel, (qg6) null);
        }
    }

    /*
    static {
        lg6 lg6 = lg6.a;
    }
    */

    @DexIgnore
    public x60(byte b2, byte b3, byte b4, byte b5, short s, a aVar) throws IllegalArgumentException {
        super(s60.INACTIVE_NUDGE);
        this.b = b2;
        this.c = b3;
        this.d = b4;
        this.e = b5;
        this.f = s;
        this.g = aVar;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(6).order(ByteOrder.LITTLE_ENDIAN).put(this.b).put(this.c).put(this.d).put(this.e).put((byte) this.f).put(this.g.a()).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        byte b2 = this.b;
        boolean z = true;
        if (b2 >= 0 && 23 >= b2) {
            byte b3 = this.c;
            if (b3 >= 0 && 59 >= b3) {
                byte b4 = this.d;
                if (b4 >= 0 && 23 >= b4) {
                    byte b5 = this.e;
                    if (b5 >= 0 && 59 >= b5) {
                        short s = h;
                        short s2 = this.f;
                        if (s2 < 0 || s < s2) {
                            z = false;
                        }
                        if (!z) {
                            StringBuilder b6 = ze0.b("repeatInterval(");
                            b6.append(this.f);
                            b6.append(") is out of range ");
                            b6.append("[0, ");
                            throw new IllegalArgumentException(ze0.a(b6, h, "] (in minute."));
                        }
                        return;
                    }
                    throw new IllegalArgumentException(ze0.a(ze0.b("stopMinute("), this.e, ") is out of range ", "[0, 59]."));
                }
                throw new IllegalArgumentException(ze0.a(ze0.b("stopHour("), this.d, ") is out of range ", "[0, 23]."));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("startMinute("), this.c, ") is out of range ", "[0, 59]."));
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("startHour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(x60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x60 x60 = (x60) obj;
            return this.b == x60.b && this.c == x60.c && this.d == x60.d && this.e == x60.e && this.f == x60.f && this.g == x60.g;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.InactiveNudgeConfig");
    }

    @DexIgnore
    public final short getRepeatInterval() {
        return this.f;
    }

    @DexIgnore
    public final byte getStartHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getStartMinute() {
        return this.c;
    }

    @DexIgnore
    public final a getState() {
        return this.g;
    }

    @DexIgnore
    public final byte getStopHour() {
        return this.d;
    }

    @DexIgnore
    public final byte getStopMinute() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        return this.g.hashCode() + (((((((((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + this.e) * 31) + this.f) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            parcel.writeByte(this.d);
        }
        if (parcel != null) {
            parcel.writeByte(this.e);
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.f));
        }
        if (parcel != null) {
            parcel.writeString(this.g.name());
        }
    }

    @DexIgnore
    public JSONObject d() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("start_hour", Byte.valueOf(this.b));
            jSONObject.put("start_minute", Byte.valueOf(this.c));
            jSONObject.put("stop_hour", Byte.valueOf(this.d));
            jSONObject.put("stop_minute", Byte.valueOf(this.e));
            jSONObject.put("repeat_interval", Short.valueOf(this.f));
            jSONObject.put(ServerProtocol.DIALOG_PARAM_STATE, cw0.a((Enum<?>) this.g));
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        return jSONObject;
    }

    @DexIgnore
    public /* synthetic */ x60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readByte();
        this.c = parcel.readByte();
        this.d = parcel.readByte();
        this.e = parcel.readByte();
        this.f = (short) parcel.readInt();
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.g = a.valueOf(readString);
            e();
            return;
        }
        wg6.a();
        throw null;
    }
}
