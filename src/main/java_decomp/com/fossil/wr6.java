package com.fossil;

import java.io.IOException;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface wr6 {
    @DexIgnore
    yt6 a(yq6 yq6, long j);

    @DexIgnore
    zq6 a(Response response) throws IOException;

    @DexIgnore
    Response.a a(boolean z) throws IOException;

    @DexIgnore
    void a() throws IOException;

    @DexIgnore
    void a(yq6 yq6) throws IOException;

    @DexIgnore
    void b() throws IOException;

    @DexIgnore
    void cancel();
}
