package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.c12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u43 extends c12<l43> {
    @DexIgnore
    public u43(Context context, Looper looper, c12.a aVar, c12.b bVar) {
        super(context, looper, 93, aVar, bVar, (String) null);
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.measurement.START";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof l43) {
            return (l43) queryLocalInterface;
        }
        return new n43(iBinder);
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }
}
