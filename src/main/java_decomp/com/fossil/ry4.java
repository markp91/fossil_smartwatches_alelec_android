package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.contacts.NotificationContactsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ry4 implements MembersInjector<NotificationContactsActivity> {
    @DexIgnore
    public static void a(NotificationContactsActivity notificationContactsActivity, NotificationContactsPresenter notificationContactsPresenter) {
        notificationContactsActivity.B = notificationContactsPresenter;
    }
}
