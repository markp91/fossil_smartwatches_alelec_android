package com.fossil;

import com.google.android.gms.common.data.DataHolder;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class s02<T> implements t02<T> {
    @DexIgnore
    public /* final */ DataHolder a;

    @DexIgnore
    public s02(DataHolder dataHolder) {
        this.a = dataHolder;
    }

    @DexIgnore
    public void a() {
        DataHolder dataHolder = this.a;
        if (dataHolder != null) {
            dataHolder.close();
        }
    }

    @DexIgnore
    public final void close() {
        a();
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new u02(this);
    }
}
