package com.fossil;

import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.musiccontrol.MusicControlComponent$NewNotificationMusicController$playbackState$1", f = "MusicControlComponent.kt", l = {}, m = "invokeSuspend")
public final class fq4$d$c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ Integer $currentState;
    @DexIgnore
    public /* final */ /* synthetic */ int $oldState;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fq4$d$c(MusicControlComponent.d dVar, int i, Integer num, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
        this.$oldState = i;
        this.$currentState = num;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        fq4$d$c fq4_d_c = new fq4$d$c(this.this$0, this.$oldState, this.$currentState, xe6);
        fq4_d_c.p$ = (il6) obj;
        return fq4_d_c;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((fq4$d$c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.a(this.$oldState, this.$currentState.intValue(), this.this$0);
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
