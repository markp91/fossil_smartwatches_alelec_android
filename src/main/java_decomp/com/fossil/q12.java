package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q12 implements r12 {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public q12(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public final void a(p12 p12, h12 h12) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        Parcel obtain2 = Parcel.obtain();
        try {
            obtain.writeInterfaceToken("com.google.android.gms.common.internal.IGmsServiceBroker");
            obtain.writeStrongBinder(p12 != null ? p12.asBinder() : null);
            if (h12 != null) {
                obtain.writeInt(1);
                h12.writeToParcel(obtain, 0);
            } else {
                obtain.writeInt(0);
            }
            this.a.transact(46, obtain, obtain2, 0);
            obtain2.readException();
        } finally {
            obtain2.recycle();
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.a;
    }
}
