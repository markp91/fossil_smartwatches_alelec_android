package com.fossil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum k70 {
    SUNDAY((byte) 1, "Sun"),
    MONDAY((byte) 2, "Mon"),
    TUESDAY((byte) 4, "Tue"),
    WEDNESDAY((byte) 8, "Wed"),
    THURSDAY((byte) 16, "Thu"),
    FRIDAY((byte) 32, "Fri"),
    SATURDAY((byte) 64, "Sat");
    
    @DexIgnore
    public static /* final */ a d; // = null;
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ String b;

    /*
    static {
        d = new a((qg6) null);
    }
    */

    @DexIgnore
    public k70(byte b2, String str) {
        this.a = b2;
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final byte b() {
        return this.a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final Set<k70> a(byte b) {
            LinkedHashSet linkedHashSet = new LinkedHashSet();
            for (k70 k70 : k70.values()) {
                if (((byte) (k70.b() & b)) == k70.b()) {
                    linkedHashSet.add(k70);
                }
            }
            return linkedHashSet;
        }

        @DexIgnore
        public final k70[] a(String[] strArr) {
            k70 k70;
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                k70[] values = k70.values();
                int length = values.length;
                int i = 0;
                while (true) {
                    if (i >= length) {
                        k70 = null;
                        break;
                    }
                    k70 = values[i];
                    if (wg6.a(cw0.a((Enum<?>) k70), str) || wg6.a(k70.name(), str)) {
                        break;
                    }
                    i++;
                }
                if (k70 != null) {
                    arrayList.add(k70);
                }
            }
            Object[] array = arrayList.toArray(new k70[0]);
            if (array != null) {
                return (k70[]) array;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }
}
