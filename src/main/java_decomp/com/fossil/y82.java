package com.fossil;

import android.content.Context;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y82 {
    @DexIgnore
    public static y82 b;
    @DexIgnore
    public /* final */ Map<String, Map<String, Boolean>> a; // = new p4();

    @DexIgnore
    public y82(Context context) {
    }

    @DexIgnore
    public static y82 a(Context context) {
        y82 y82;
        synchronized (y82.class) {
            if (b == null) {
                b = new y82(context.getApplicationContext());
            }
            y82 = b;
        }
        return y82;
    }

    @DexIgnore
    public final synchronized void b(String str, String str2) {
        Map map = this.a.get(str2);
        if (map != null) {
            if ((map.remove(str) != null) && map.isEmpty()) {
                this.a.remove(str2);
            }
        }
    }

    @DexIgnore
    public final synchronized boolean c(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            return false;
        }
        Boolean bool = (Boolean) map.get(str);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    @DexIgnore
    public final synchronized boolean a(String str, String str2) {
        Map map = this.a.get(str2);
        if (map == null) {
            map = new p4();
            this.a.put(str2, map);
        }
        if (map.put(str, false) == null) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final synchronized boolean a(String str) {
        return this.a.containsKey(str);
    }
}
