package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm6 {
    @DexIgnore
    public static /* final */ uo6 a; // = new uo6("SEALED");
    @DexIgnore
    public static /* final */ dm6 b; // = new dm6(false);
    @DexIgnore
    public static /* final */ dm6 c; // = new dm6(true);

    @DexIgnore
    public static final Object a(Object obj) {
        return obj instanceof mm6 ? new nm6((mm6) obj) : obj;
    }

    @DexIgnore
    public static final Object b(Object obj) {
        mm6 mm6;
        nm6 nm6 = (nm6) (!(obj instanceof nm6) ? null : obj);
        return (nm6 == null || (mm6 = nm6.a) == null) ? obj : mm6;
    }
}
