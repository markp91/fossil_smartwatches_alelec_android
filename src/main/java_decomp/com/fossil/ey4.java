package com.fossil;

import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey4 implements Factory<dy4> {
    @DexIgnore
    public static NotificationAppsPresenter a(ay4 ay4, z24 z24, d15 d15, mz4 mz4, gy4 gy4, an4 an4, NotificationSettingsDatabase notificationSettingsDatabase) {
        return new NotificationAppsPresenter(ay4, z24, d15, mz4, gy4, an4, notificationSettingsDatabase);
    }
}
