package com.fossil;

import com.portfolio.platform.data.source.remote.ShortcutApiService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b24 implements Factory<ShortcutApiService> {
    @DexIgnore
    public /* final */ b14 a;
    @DexIgnore
    public /* final */ Provider<ip4> b;
    @DexIgnore
    public /* final */ Provider<mp4> c;

    @DexIgnore
    public b24(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        this.a = b14;
        this.b = provider;
        this.c = provider2;
    }

    @DexIgnore
    public static b24 a(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return new b24(b14, provider, provider2);
    }

    @DexIgnore
    public static ShortcutApiService b(b14 b14, Provider<ip4> provider, Provider<mp4> provider2) {
        return a(b14, provider.get(), provider2.get());
    }

    @DexIgnore
    public static ShortcutApiService a(b14 b14, ip4 ip4, mp4 mp4) {
        ShortcutApiService d = b14.d(ip4, mp4);
        z76.a(d, "Cannot return null from a non-@Nullable @Provides method");
        return d;
    }

    @DexIgnore
    public ShortcutApiService get() {
        return b(this.a, this.b, this.c);
    }
}
