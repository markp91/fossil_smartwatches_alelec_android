package com.fossil;

import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class v40<V, E> {
    @DexIgnore
    public V a;
    @DexIgnore
    public E b;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public CopyOnWriteArrayList<hg6<V, cd6>> d; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<hg6<E, cd6>> e; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<hg6<cd6, cd6>> f; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<hg6<Float, cd6>> g; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public CopyOnWriteArrayList<hg6<E, cd6>> h; // = new CopyOnWriteArrayList<>();

    @DexIgnore
    public final boolean a() {
        return c() || b();
    }

    @DexIgnore
    public final boolean b() {
        return this.b != null;
    }

    @DexIgnore
    public final boolean c() {
        return this.a != null;
    }

    @DexIgnore
    public final synchronized void d() {
        Iterator<T> it = this.f.iterator();
        while (it.hasNext()) {
            try {
                ((hg6) it.next()).invoke(cd6.a);
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public v40<V, E> a(hg6<? super E, cd6> hg6) {
        if (!a()) {
            this.h.add(hg6);
        }
        return this;
    }

    @DexIgnore
    public v40<V, E> b(hg6<? super E, cd6> hg6) {
        if (!a()) {
            this.e.add(hg6);
        } else if (b()) {
            try {
                E e2 = this.b;
                if (e2 != null) {
                    hg6.invoke(e2);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e3) {
                qs0.h.a(e3);
            }
        }
        return this;
    }

    @DexIgnore
    public v40<V, E> c(hg6<? super V, cd6> hg6) {
        if (!a()) {
            this.d.add(hg6);
        } else if (c()) {
            try {
                V v = this.a;
                if (v != null) {
                    hg6.invoke(v);
                } else {
                    wg6.a();
                    throw null;
                }
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
        return this;
    }

    @DexIgnore
    public final synchronized void a(float f2) {
        Iterator<T> it = this.g.iterator();
        while (it.hasNext()) {
            try {
                ((hg6) it.next()).invoke(Float.valueOf(f2));
            } catch (Exception e2) {
                qs0.h.a(e2);
            }
        }
    }

    @DexIgnore
    public final synchronized void b(E e2) {
        if (!a()) {
            this.b = e2;
            Iterator<T> it = this.e.iterator();
            while (it.hasNext()) {
                hg6 hg6 = (hg6) it.next();
                try {
                    E e3 = this.b;
                    if (e3 != null) {
                        hg6.invoke(e3);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } catch (Exception e4) {
                    qs0.h.a(e4);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void c(V v) {
        if (!a()) {
            this.a = v;
            Iterator<T> it = this.d.iterator();
            while (it.hasNext()) {
                hg6 hg6 = (hg6) it.next();
                try {
                    V v2 = this.a;
                    if (v2 != null) {
                        hg6.invoke(v2);
                    } else {
                        wg6.a();
                        throw null;
                    }
                } catch (Exception e2) {
                    qs0.h.a(e2);
                }
            }
            d();
        }
    }

    @DexIgnore
    public final synchronized void a(E e2) {
        if (!a() && !this.c) {
            this.c = true;
            if (true ^ this.h.isEmpty()) {
                Iterator<T> it = this.h.iterator();
                while (it.hasNext()) {
                    try {
                        ((hg6) it.next()).invoke(e2);
                    } catch (Exception e3) {
                        qs0.h.a(e3);
                    }
                }
                this.c = false;
            } else {
                b(e2);
            }
        }
    }
}
