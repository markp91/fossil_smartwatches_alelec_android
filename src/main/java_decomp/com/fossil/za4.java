package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleFitnessTab;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.RingProgressBar;
import com.portfolio.platform.view.swiperefreshlayout.CustomSwipeRefreshLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class za4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleFitnessTab A;
    @DexIgnore
    public /* final */ FlexibleFitnessTab B;
    @DexIgnore
    public /* final */ FlexibleFitnessTab C;
    @DexIgnore
    public /* final */ FlexibleFitnessTab D;
    @DexIgnore
    public /* final */ FlexibleFitnessTab E;
    @DexIgnore
    public /* final */ FlexibleTextView F;
    @DexIgnore
    public /* final */ FlexibleTextView G;
    @DexIgnore
    public /* final */ ImageView H;
    @DexIgnore
    public /* final */ RTLImageView I;
    @DexIgnore
    public /* final */ NestedScrollView J;
    @DexIgnore
    public /* final */ FlexibleProgressBar K;
    @DexIgnore
    public /* final */ ConstraintLayout L;
    @DexIgnore
    public /* final */ RingProgressBar M;
    @DexIgnore
    public /* final */ RingProgressBar N;
    @DexIgnore
    public /* final */ RingProgressBar O;
    @DexIgnore
    public /* final */ RingProgressBar P;
    @DexIgnore
    public /* final */ ViewPager2 Q;
    @DexIgnore
    public /* final */ CustomSwipeRefreshLayout R;
    @DexIgnore
    public /* final */ FlexibleProgressBar S;
    @DexIgnore
    public /* final */ FlexibleTextView T;
    @DexIgnore
    public /* final */ View U;
    @DexIgnore
    public /* final */ View V;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ CoordinatorLayout s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ ConstraintLayout v;
    @DexIgnore
    public /* final */ ConstraintLayout w;
    @DexIgnore
    public /* final */ ConstraintLayout x;
    @DexIgnore
    public /* final */ FlexibleButton y;
    @DexIgnore
    public /* final */ FlexibleFitnessTab z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public za4(Object obj, View view, int i, AppBarLayout appBarLayout, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, ConstraintLayout constraintLayout, CoordinatorLayout coordinatorLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, ConstraintLayout constraintLayout6, ConstraintLayout constraintLayout7, ConstraintLayout constraintLayout8, CollapsingToolbarLayout collapsingToolbarLayout, FlexibleButton flexibleButton, FlexibleFitnessTab flexibleFitnessTab, FlexibleFitnessTab flexibleFitnessTab2, FlexibleFitnessTab flexibleFitnessTab3, FlexibleFitnessTab flexibleFitnessTab4, FlexibleFitnessTab flexibleFitnessTab5, FlexibleFitnessTab flexibleFitnessTab6, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, ImageView imageView, RTLImageView rTLImageView, NestedScrollView nestedScrollView, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout9, RingProgressBar ringProgressBar, RingProgressBar ringProgressBar2, RingProgressBar ringProgressBar3, RingProgressBar ringProgressBar4, ViewPager2 viewPager2, CustomSwipeRefreshLayout customSwipeRefreshLayout, FlexibleProgressBar flexibleProgressBar2, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, View view2, View view3, View view4) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = coordinatorLayout;
        this.t = constraintLayout3;
        this.u = constraintLayout5;
        this.v = constraintLayout6;
        this.w = constraintLayout7;
        this.x = constraintLayout8;
        this.y = flexibleButton;
        this.z = flexibleFitnessTab;
        this.A = flexibleFitnessTab2;
        this.B = flexibleFitnessTab3;
        this.C = flexibleFitnessTab4;
        this.D = flexibleFitnessTab5;
        this.E = flexibleFitnessTab6;
        this.F = flexibleTextView3;
        this.G = flexibleTextView5;
        this.H = imageView;
        this.I = rTLImageView;
        this.J = nestedScrollView;
        this.K = flexibleProgressBar;
        this.L = constraintLayout9;
        this.M = ringProgressBar;
        this.N = ringProgressBar2;
        this.O = ringProgressBar3;
        this.P = ringProgressBar4;
        this.Q = viewPager2;
        this.R = customSwipeRefreshLayout;
        this.S = flexibleProgressBar2;
        this.T = flexibleTextView7;
        this.U = view2;
        this.V = view4;
    }
}
