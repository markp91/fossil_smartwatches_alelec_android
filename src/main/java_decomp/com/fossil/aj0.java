package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aj0 extends xg6 implements hg6<if1, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ eh1[] a;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public aj0(eh1[] eh1Arr) {
        super(1);
        this.a = eh1Arr;
    }

    @DexIgnore
    public final boolean a(if1 if1) {
        return !nd6.a(this.a, if1.y);
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(a((if1) obj));
    }
}
