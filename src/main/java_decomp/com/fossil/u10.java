package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u10 {
    @DexIgnore
    public long a;
    @DexIgnore
    public ha6 b;

    @DexIgnore
    public u10(ha6 ha6) {
        if (ha6 != null) {
            this.b = ha6;
            return;
        }
        throw new NullPointerException("retryState must not be null");
    }

    @DexIgnore
    public boolean a(long j) {
        return j - this.a >= this.b.a() * 1000000;
    }

    @DexIgnore
    public void b(long j) {
        this.a = j;
        this.b = this.b.c();
    }

    @DexIgnore
    public void a() {
        this.a = 0;
        this.b = this.b.b();
    }
}
