package com.fossil;

import com.fossil.gs;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hs {
    @DexIgnore
    public static /* final */ gs.a<?> b; // = new a();
    @DexIgnore
    public /* final */ Map<Class<?>, gs.a<?>> a; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements gs.a<Object> {
        @DexIgnore
        public gs<Object> a(Object obj) {
            return new b(obj);
        }

        @DexIgnore
        public Class<Object> getDataClass() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements gs<Object> {
        @DexIgnore
        public /* final */ Object a;

        @DexIgnore
        public b(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        public Object b() {
            return this.a;
        }
    }

    @DexIgnore
    public synchronized void a(gs.a<?> aVar) {
        this.a.put(aVar.getDataClass(), aVar);
    }

    @DexIgnore
    public synchronized <T> gs<T> a(T t) {
        gs.a<?> aVar;
        q00.a(t);
        aVar = this.a.get(t.getClass());
        if (aVar == null) {
            Iterator<gs.a<?>> it = this.a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                gs.a<?> next = it.next();
                if (next.getDataClass().isAssignableFrom(t.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = b;
        }
        return aVar.a(t);
    }
}
