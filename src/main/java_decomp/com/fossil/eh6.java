package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class eh6 extends mg6 implements li6 {
    @DexIgnore
    public eh6() {
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof eh6) {
            eh6 eh6 = (eh6) obj;
            if (!getOwner().equals(eh6.getOwner()) || !getName().equals(eh6.getName()) || !getSignature().equals(eh6.getSignature()) || !wg6.a(getBoundReceiver(), eh6.getBoundReceiver())) {
                return false;
            }
            return true;
        } else if (obj instanceof li6) {
            return obj.equals(compute());
        } else {
            return false;
        }
    }

    @DexIgnore
    public int hashCode() {
        return (((getOwner().hashCode() * 31) + getName().hashCode()) * 31) + getSignature().hashCode();
    }

    @DexIgnore
    public boolean isConst() {
        return getReflected().isConst();
    }

    @DexIgnore
    public boolean isLateinit() {
        return getReflected().isLateinit();
    }

    @DexIgnore
    public String toString() {
        ei6 compute = compute();
        if (compute != this) {
            return compute.toString();
        }
        return "property " + getName() + " (Kotlin reflection is not available)";
    }

    @DexIgnore
    public eh6(Object obj) {
        super(obj);
    }

    @DexIgnore
    public li6 getReflected() {
        return (li6) super.getReflected();
    }
}
