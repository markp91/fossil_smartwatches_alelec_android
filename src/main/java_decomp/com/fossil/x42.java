package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x42 implements Executor {
    @DexIgnore
    public /* final */ Handler a;

    @DexIgnore
    public x42(Looper looper) {
        this.a = new fb2(looper);
    }

    @DexIgnore
    public void execute(Runnable runnable) {
        this.a.post(runnable);
    }
}
