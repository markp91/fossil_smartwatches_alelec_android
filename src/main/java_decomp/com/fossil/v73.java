package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@TargetApi(14)
public final class v73 implements Application.ActivityLifecycleCallbacks {
    @DexIgnore
    public /* final */ /* synthetic */ e73 a;

    @DexIgnore
    public v73(e73 e73) {
        this.a = e73;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009c A[SYNTHETIC, Splitter:B:33:0x009c] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e4 A[Catch:{ Exception -> 0x01a7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f5 A[SYNTHETIC, Splitter:B:49:0x00f5] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0124 A[Catch:{ Exception -> 0x01a7 }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0125 A[Catch:{ Exception -> 0x01a7 }] */
    public final void a(boolean z, Uri uri, String str, String str2) {
        Bundle bundle;
        Bundle bundle2;
        String str3 = str;
        String str4 = str2;
        try {
            if (!this.a.l().a(l03.E0)) {
                if (!this.a.l().a(l03.G0)) {
                    if (this.a.l().a(l03.F0)) {
                    }
                    bundle = null;
                    boolean z2 = false;
                    if (z) {
                        bundle2 = this.a.j().a(uri);
                        if (bundle2 != null) {
                            bundle2.putString("_cis", "intent");
                            if (this.a.l().a(l03.E0) && !bundle2.containsKey("gclid") && bundle != null && bundle.containsKey("gclid")) {
                                bundle2.putString("_cer", String.format("gclid=%s", new Object[]{bundle.getString("gclid")}));
                            }
                            this.a.a(str3, "_cmp", bundle2);
                        }
                    } else {
                        bundle2 = null;
                    }
                    if (this.a.l().a(l03.G0)) {
                        if (!this.a.l().a(l03.F0) && bundle != null && bundle.containsKey("gclid") && (bundle2 == null || !bundle2.containsKey("gclid"))) {
                            this.a.a("auto", "_lgclid", (Object) bundle.getString("gclid"), true);
                        }
                    }
                    if (!TextUtils.isEmpty(str2)) {
                        this.a.b().A().a("Activity created with referrer", str4);
                        if (this.a.l().a(l03.F0)) {
                            if (bundle != null) {
                                this.a.a(str3, "_cmp", bundle);
                            } else {
                                this.a.b().A().a("Referrer does not contain valid parameters", str4);
                            }
                            this.a.a("auto", "_ldl", (Object) null, true);
                            return;
                        }
                        if (str4.contains("gclid") && (str4.contains("utm_campaign") || str4.contains("utm_source") || str4.contains("utm_medium") || str4.contains("utm_term") || str4.contains("utm_content"))) {
                            z2 = true;
                        }
                        if (!z2) {
                            this.a.b().A().a("Activity created with data 'referrer' without required params");
                            return;
                        } else if (!TextUtils.isEmpty(str2)) {
                            this.a.a("auto", "_ldl", (Object) str4, true);
                            return;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
            ma3 j = this.a.j();
            if (!TextUtils.isEmpty(str2)) {
                if (str4.contains("gclid") || str4.contains("utm_campaign") || str4.contains("utm_source") || str4.contains("utm_medium")) {
                    String valueOf = String.valueOf(str2);
                    bundle = j.a(Uri.parse(valueOf.length() != 0 ? "https://google.com/search?".concat(valueOf) : new String("https://google.com/search?")));
                    if (bundle != null) {
                        bundle.putString("_cis", "referrer");
                    }
                    boolean z22 = false;
                    if (z) {
                    }
                    if (this.a.l().a(l03.G0)) {
                    }
                    if (!TextUtils.isEmpty(str2)) {
                    }
                } else {
                    j.b().A().a("Activity created with data 'referrer' without required params");
                }
            }
            bundle = null;
            boolean z222 = false;
            if (z) {
            }
            if (this.a.l().a(l03.G0)) {
            }
            if (!TextUtils.isEmpty(str2)) {
            }
        } catch (Exception e) {
            this.a.b().t().a("Throwable caught in handleReferrerForOnActivityCreated", e);
        }
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.a.b().B().a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data != null) {
                    if (data.isHierarchical()) {
                        this.a.j();
                        String str = ma3.a(intent) ? "gs" : "auto";
                        String queryParameter = data.getQueryParameter("referrer");
                        boolean z = bundle == null;
                        if (!it2.a() || !l03.H0.a(null).booleanValue()) {
                            a(z, data, str, queryParameter);
                        } else {
                            this.a.a().a((Runnable) new y73(this, z, data, str, queryParameter));
                        }
                        this.a.r().a(activity, bundle);
                        return;
                    }
                }
                this.a.r().a(activity, bundle);
            }
        } catch (Exception e) {
            this.a.b().t().a("Throwable caught in onActivityCreated", e);
        } finally {
            this.a.r().a(activity, bundle);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
        this.a.r().c(activity);
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
        this.a.r().b(activity);
        m93 t = this.a.t();
        t.a().a((Runnable) new o93(t, t.zzm().c()));
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        if (!qs2.a() || !l03.d0.a(null).booleanValue()) {
            this.a.r().a(activity);
            this.a.t().A();
            return;
        }
        this.a.t().A();
        this.a.r().a(activity);
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.a.r().b(activity, bundle);
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public /* synthetic */ v73(e73 e73, g73 g73) {
        this(e73);
    }
}
