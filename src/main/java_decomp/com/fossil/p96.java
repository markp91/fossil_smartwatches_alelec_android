package com.fossil;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.util.Log;
import com.j256.ormlite.logger.Logger;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class p96<Params, Progress, Result> {
    @DexIgnore
    public static /* final */ int f; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int g;
    @DexIgnore
    public static /* final */ int h;
    @DexIgnore
    public static /* final */ ThreadFactory i; // = new a();
    @DexIgnore
    public static /* final */ BlockingQueue<Runnable> j; // = new LinkedBlockingQueue(Logger.DEFAULT_FULL_MESSAGE_LENGTH);
    @DexIgnore
    public static /* final */ Executor o; // = new ThreadPoolExecutor(g, h, 1, TimeUnit.SECONDS, j, i);
    @DexIgnore
    public static /* final */ Executor p; // = new g((a) null);
    @DexIgnore
    public static /* final */ f q; // = new f();
    @DexIgnore
    public /* final */ i<Params, Result> a; // = new b();
    @DexIgnore
    public /* final */ FutureTask<Result> b; // = new c(this.a);
    @DexIgnore
    public volatile h c; // = h.PENDING;
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ThreadFactory {
        @DexIgnore
        public /* final */ AtomicInteger a; // = new AtomicInteger(1);

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "AsyncTask #" + this.a.getAndIncrement());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends i<Params, Result> {
        @DexIgnore
        public b() {
            super((a) null);
        }

        @DexIgnore
        public Result call() throws Exception {
            p96.this.e.set(true);
            Process.setThreadPriority(10);
            p96 p96 = p96.this;
            Result a = p96.a(this.a);
            Object unused = p96.e(a);
            return a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends FutureTask<Result> {
        @DexIgnore
        public c(Callable callable) {
            super(callable);
        }

        @DexIgnore
        public void done() {
            try {
                p96.this.f(get());
            } catch (InterruptedException e) {
                Log.w("AsyncTask", e);
            } catch (ExecutionException e2) {
                throw new RuntimeException("An error occured while executing doInBackground()", e2.getCause());
            } catch (CancellationException unused) {
                p96.this.f(null);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class d {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[h.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /*
        static {
            a[h.RUNNING.ordinal()] = 1;
            a[h.FINISHED.ordinal()] = 2;
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e<Data> {
        @DexIgnore
        public /* final */ p96 a;
        @DexIgnore
        public /* final */ Data[] b;

        @DexIgnore
        public e(p96 p96, Data... dataArr) {
            this.a = p96;
            this.b = dataArr;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends Handler {
        @DexIgnore
        public f() {
            super(Looper.getMainLooper());
        }

        @DexIgnore
        public void handleMessage(Message message) {
            e eVar = (e) message.obj;
            int i = message.what;
            if (i == 1) {
                eVar.a.b(eVar.b[0]);
            } else if (i == 2) {
                eVar.a.b((Progress[]) eVar.b);
            }
        }
    }

    @DexIgnore
    public enum h {
        PENDING,
        RUNNING,
        FINISHED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class i<Params, Result> implements Callable<Result> {
        @DexIgnore
        public Params[] a;

        @DexIgnore
        public i() {
        }

        @DexIgnore
        public /* synthetic */ i(a aVar) {
            this();
        }
    }

    /*
    static {
        int i2 = f;
        g = i2 + 1;
        h = (i2 * 2) + 1;
    }
    */

    @DexIgnore
    public abstract Result a(Params... paramsArr);

    @DexIgnore
    public void b(Progress... progressArr) {
    }

    @DexIgnore
    public abstract void c(Result result);

    @DexIgnore
    public final h d() {
        return this.c;
    }

    @DexIgnore
    public abstract void d(Result result);

    @DexIgnore
    public final Result e(Result result) {
        q.obtainMessage(1, new e(this, result)).sendToTarget();
        return result;
    }

    @DexIgnore
    public void f() {
    }

    @DexIgnore
    public final void f(Result result) {
        if (!this.e.get()) {
            e(result);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g implements Executor {
        @DexIgnore
        public /* final */ LinkedList<Runnable> a;
        @DexIgnore
        public Runnable b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Runnable a;

            @DexIgnore
            public a(Runnable runnable) {
                this.a = runnable;
            }

            @DexIgnore
            public void run() {
                try {
                    this.a.run();
                } finally {
                    g.this.a();
                }
            }
        }

        @DexIgnore
        public g() {
            this.a = new LinkedList<>();
        }

        @DexIgnore
        public synchronized void a() {
            Runnable poll = this.a.poll();
            this.b = poll;
            if (poll != null) {
                p96.o.execute(this.b);
            }
        }

        @DexIgnore
        public synchronized void execute(Runnable runnable) {
            this.a.offer(new a(runnable));
            if (this.b == null) {
                a();
            }
        }

        @DexIgnore
        public /* synthetic */ g(a aVar) {
            this();
        }
    }

    @DexIgnore
    public final boolean b(boolean z) {
        this.d.set(true);
        return this.b.cancel(z);
    }

    @DexIgnore
    public final p96<Params, Progress, Result> a(Executor executor, Params... paramsArr) {
        if (this.c != h.PENDING) {
            int i2 = d.a[this.c.ordinal()];
            if (i2 == 1) {
                throw new IllegalStateException("Cannot execute task: the task is already running.");
            } else if (i2 == 2) {
                throw new IllegalStateException("Cannot execute task: the task has already been executed (a task can be executed only once)");
            }
        }
        this.c = h.RUNNING;
        f();
        this.a.a = paramsArr;
        executor.execute(this.b);
        return this;
    }

    @DexIgnore
    public final boolean e() {
        return this.d.get();
    }

    @DexIgnore
    public final void b(Result result) {
        if (e()) {
            c(result);
        } else {
            d(result);
        }
        this.c = h.FINISHED;
    }
}
