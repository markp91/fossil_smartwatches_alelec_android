package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gq1 {
    @DexIgnore
    public static gq1 a(Context context, zs1 zs1, zs1 zs12, String str) {
        return new bq1(context, zs1, zs12, str);
    }

    @DexIgnore
    public abstract Context a();

    @DexIgnore
    public abstract String b();

    @DexIgnore
    public abstract zs1 c();

    @DexIgnore
    public abstract zs1 d();
}
