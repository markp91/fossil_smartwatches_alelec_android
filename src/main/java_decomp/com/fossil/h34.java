package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.LocationBias;
import com.google.android.libraries.places.api.model.LocationRestriction;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h34 extends ArrayAdapter<AutocompletePrediction> implements Filterable {
    @DexIgnore
    public static /* final */ String f; // = f;
    @DexIgnore
    public static /* final */ StyleSpan g; // = new StyleSpan(1);
    @DexIgnore
    public List<? extends AutocompletePrediction> a; // = new ArrayList();
    @DexIgnore
    public b b;
    @DexIgnore
    public AutocompleteSessionToken c;
    @DexIgnore
    public Date d;
    @DexIgnore
    public /* final */ PlacesClient e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void P(boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ h34 a;

        @DexIgnore
        public c(h34 h34) {
            this.a = h34;
        }

        @DexIgnore
        public CharSequence convertResultToString(Object obj) {
            SpannableString fullText;
            wg6.b(obj, "resultValue");
            AutocompletePrediction autocompletePrediction = (AutocompletePrediction) (!(obj instanceof AutocompletePrediction) ? null : obj);
            if (autocompletePrediction != null && (fullText = autocompletePrediction.getFullText((CharacterStyle) null)) != null) {
                return fullText;
            }
            CharSequence convertResultToString = super.convertResultToString(obj);
            wg6.a((Object) convertResultToString, "super.convertResultToString(resultValue)");
            return convertResultToString;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            Filter.FilterResults filterResults = new Filter.FilterResults();
            List arrayList = new ArrayList();
            if (charSequence != null) {
                arrayList = this.a.a(charSequence);
            }
            filterResults.values = arrayList;
            if (arrayList != null) {
                filterResults.count = arrayList.size();
            } else {
                filterResults.count = 0;
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            if (filterResults == null || filterResults.count <= 0) {
                this.a.notifyDataSetInvalidated();
                if (this.a.b != null) {
                    b a2 = this.a.b;
                    if (a2 != null) {
                        a2.P(true);
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            } else {
                h34 h34 = this.a;
                Object obj = filterResults.values;
                if (obj != null) {
                    h34.a = (List) obj;
                    this.a.notifyDataSetChanged();
                    if (this.a.b != null) {
                        b a3 = this.a.b;
                        if (a3 != null) {
                            a3.P(false);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.collections.List<com.google.android.libraries.places.api.model.AutocompletePrediction>");
                }
            }
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h34(Context context, PlacesClient placesClient) {
        super(context, 2131558492, 2131362818);
        wg6.b(context, "context");
        this.e = placesClient;
    }

    @DexIgnore
    public final AutocompleteSessionToken b() {
        return this.c;
    }

    @DexIgnore
    public int getCount() {
        return this.a.size();
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r0v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    /* JADX WARNING: type inference failed for: r1v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
    public View getView(int i, View view, ViewGroup viewGroup) {
        wg6.b(viewGroup, "parent");
        View view2 = super.getView(i, view, viewGroup);
        wg6.a((Object) view2, "super.getView(position, convertView, parent)");
        AutocompletePrediction item = getItem(i);
        if (item != null) {
            Object r6 = (FlexibleTextView) view2.findViewById(2131362458);
            Object r0 = (FlexibleTextView) view2.findViewById(2131362818);
            Object r1 = (FlexibleTextView) view2.findViewById(2131362928);
            wg6.a((Object) r6, "fullTv");
            r6.setText(item.getFullText(g));
            wg6.a((Object) r0, "textView1");
            r0.setText(item.getPrimaryText(g));
            wg6.a((Object) r1, "textView2");
            r1.setText(item.getSecondaryText(g));
        }
        return view2;
    }

    @DexIgnore
    public AutocompletePrediction getItem(int i) {
        return this.a.get(i);
    }

    @DexIgnore
    public final void a(b bVar) {
        this.b = bVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        if ((r2 - r0.getTime()) >= ((long) 180000)) goto L_0x002c;
     */
    @DexIgnore
    public final List<AutocompletePrediction> a(CharSequence charSequence) {
        if (this.e != null) {
            if (this.c != null) {
                if (this.d != null) {
                    long time = new Date().getTime();
                    Date date = this.d;
                    if (date == null) {
                        wg6.a();
                        throw null;
                    }
                }
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f;
                local.d(str, "Starting autocomplete query for: " + charSequence);
                FindAutocompletePredictionsRequest.Builder typeFilter = FindAutocompletePredictionsRequest.builder().setQuery(charSequence.toString()).setCountry((String) null).setLocationBias((LocationBias) null).setLocationRestriction((LocationRestriction) null).setTypeFilter((TypeFilter) null);
                wg6.a((Object) typeFilter, "FindAutocompletePredicti\u2026     .setTypeFilter(null)");
                typeFilter.setSessionToken(this.c);
                qc3 findAutocompletePredictions = this.e.findAutocompletePredictions(typeFilter.build());
                wg6.a((Object) findAutocompletePredictions, "mPlacesClient.findAutoco\u2026s(requestBuilder.build())");
                FindAutocompletePredictionsResponse findAutocompletePredictionsResponse = (FindAutocompletePredictionsResponse) tc3.a(findAutocompletePredictions);
                wg6.a((Object) findAutocompletePredictionsResponse, "response");
                return findAutocompletePredictionsResponse.getAutocompletePredictions();
            }
            this.c = AutocompleteSessionToken.newInstance();
            this.d = new Date();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "Starting autocomplete query for: " + charSequence);
            FindAutocompletePredictionsRequest.Builder typeFilter2 = FindAutocompletePredictionsRequest.builder().setQuery(charSequence.toString()).setCountry((String) null).setLocationBias((LocationBias) null).setLocationRestriction((LocationRestriction) null).setTypeFilter((TypeFilter) null);
            wg6.a((Object) typeFilter2, "FindAutocompletePredicti\u2026     .setTypeFilter(null)");
            typeFilter2.setSessionToken(this.c);
            qc3 findAutocompletePredictions2 = this.e.findAutocompletePredictions(typeFilter2.build());
            wg6.a((Object) findAutocompletePredictions2, "mPlacesClient.findAutoco\u2026s(requestBuilder.build())");
            try {
                FindAutocompletePredictionsResponse findAutocompletePredictionsResponse2 = (FindAutocompletePredictionsResponse) tc3.a(findAutocompletePredictions2);
                wg6.a((Object) findAutocompletePredictionsResponse2, "response");
                return findAutocompletePredictionsResponse2.getAutocompletePredictions();
            } catch (Exception e2) {
                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                String str3 = f;
                local3.d(str3, "Query failed. Received Exception=" + e2);
                e2.printStackTrace();
                return null;
            }
        } else {
            FLogger.INSTANCE.getLocal().e(f, "client is not connected for autocomplete query.");
            return null;
        }
    }

    @DexIgnore
    public final void a() {
        this.c = null;
    }
}
