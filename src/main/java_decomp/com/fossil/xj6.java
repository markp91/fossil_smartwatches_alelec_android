package com.fossil;

import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xj6 extends wj6 {
    @DexIgnore
    public static /* synthetic */ String a(String str, char c, char c2, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, c, c2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean b(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(str, str2, z);
    }

    @DexIgnore
    public static /* synthetic */ boolean c(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(str, str2, z);
    }

    @DexIgnore
    public static final String d(String str) {
        wg6.b(str, "$this$capitalize");
        if (!(str.length() > 0) || !Character.isLowerCase(str.charAt(0))) {
            return str;
        }
        StringBuilder sb = new StringBuilder();
        String substring = str.substring(0, 1);
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        if (substring != null) {
            String upperCase = substring.toUpperCase();
            wg6.a((Object) upperCase, "(this as java.lang.String).toUpperCase()");
            sb.append(upperCase);
            String substring2 = str.substring(1);
            wg6.a((Object) substring2, "(this as java.lang.String).substring(startIndex)");
            sb.append(substring2);
            return sb.toString();
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public static final String a(String str, char c, char c2, boolean z) {
        wg6.b(str, "$this$replace");
        if (!z) {
            String replace = str.replace(c, c2);
            wg6.a((Object) replace, "(this as java.lang.Strin\u2026replace(oldChar, newChar)");
            return replace;
        }
        return aj6.a(yj6.a((CharSequence) str, new char[]{c}, z, 0, 4, (Object) null), String.valueOf(c2), (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (hg6) null, 62, (Object) null);
    }

    @DexIgnore
    public static final boolean b(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        }
        if (!z) {
            return str.equals(str2);
        }
        return str.equalsIgnoreCase(str2);
    }

    @DexIgnore
    public static final boolean c(String str, String str2, boolean z) {
        wg6.b(str, "$this$startsWith");
        wg6.b(str2, "prefix");
        if (!z) {
            return str.startsWith(str2);
        }
        return a(str, 0, str2, 0, str2.length(), z);
    }

    @DexIgnore
    public static /* synthetic */ String a(String str, String str2, String str3, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = false;
        }
        return a(str, str2, str3, z);
    }

    @DexIgnore
    public static final String a(String str, String str2, String str3, boolean z) {
        wg6.b(str, "$this$replace");
        wg6.b(str2, "oldValue");
        wg6.b(str3, "newValue");
        return aj6.a(yj6.b((CharSequence) str, new String[]{str2}, z, 0, 4, (Object) null), str3, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (hg6) null, 62, (Object) null);
    }

    @DexIgnore
    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(str, str2, z);
    }

    @DexIgnore
    public static final boolean a(String str, String str2, boolean z) {
        wg6.b(str, "$this$endsWith");
        wg6.b(str2, "suffix");
        if (!z) {
            return str.endsWith(str2);
        }
        return a(str, str.length() - str2.length(), str2, 0, str2.length(), true);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:17:? A[RETURN, SYNTHETIC] */
    public static final boolean a(CharSequence charSequence) {
        boolean z;
        wg6.b(charSequence, "$this$isBlank");
        if (charSequence.length() != 0) {
            wh6 b = yj6.b(charSequence);
            if (!(b instanceof Collection) || !((Collection) b).isEmpty()) {
                Iterator it = b.iterator();
                while (true) {
                    if (it.hasNext()) {
                        if (!cj6.a(charSequence.charAt(((ce6) it).a()))) {
                            z = false;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                if (!z) {
                    return true;
                }
                return false;
            }
            z = true;
            if (!z) {
            }
        }
        return true;
    }

    @DexIgnore
    public static final boolean a(String str, int i, String str2, int i2, int i3, boolean z) {
        wg6.b(str, "$this$regionMatches");
        wg6.b(str2, "other");
        if (!z) {
            return str.regionMatches(i, str2, i2, i3);
        }
        return str.regionMatches(z, i, str2, i2, i3);
    }
}
