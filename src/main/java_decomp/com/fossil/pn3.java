package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pn3<E> extends zl3<E> {
    @DexIgnore
    public static /* final */ zl3<Object> EMPTY; // = new pn3(in3.a);
    @DexIgnore
    public /* final */ transient Object[] a;

    @DexIgnore
    public pn3(Object[] objArr) {
        this.a = objArr;
    }

    @DexIgnore
    public int copyIntoArray(Object[] objArr, int i) {
        Object[] objArr2 = this.a;
        System.arraycopy(objArr2, 0, objArr, i, objArr2.length);
        return i + this.a.length;
    }

    @DexIgnore
    public E get(int i) {
        return this.a[i];
    }

    @DexIgnore
    public boolean isPartialView() {
        return false;
    }

    @DexIgnore
    public int size() {
        return this.a.length;
    }

    @DexIgnore
    public ko3<E> listIterator(int i) {
        Object[] objArr = this.a;
        return qm3.a(objArr, 0, objArr.length, i);
    }
}
