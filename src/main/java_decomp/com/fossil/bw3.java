package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bw3 implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 1;
    @DexIgnore
    public a countryCodeSource_; // = a.FROM_NUMBER_WITH_PLUS_SIGN;
    @DexIgnore
    public int countryCode_; // = 0;
    @DexIgnore
    public String extension_; // = "";
    @DexIgnore
    public boolean hasCountryCode;
    @DexIgnore
    public boolean hasCountryCodeSource;
    @DexIgnore
    public boolean hasExtension;
    @DexIgnore
    public boolean hasItalianLeadingZero;
    @DexIgnore
    public boolean hasNationalNumber;
    @DexIgnore
    public boolean hasNumberOfLeadingZeros;
    @DexIgnore
    public boolean hasPreferredDomesticCarrierCode;
    @DexIgnore
    public boolean hasRawInput;
    @DexIgnore
    public boolean italianLeadingZero_; // = false;
    @DexIgnore
    public long nationalNumber_; // = 0;
    @DexIgnore
    public int numberOfLeadingZeros_; // = 1;
    @DexIgnore
    public String preferredDomesticCarrierCode_; // = "";
    @DexIgnore
    public String rawInput_; // = "";

    @DexIgnore
    public enum a {
        FROM_NUMBER_WITH_PLUS_SIGN,
        FROM_NUMBER_WITH_IDD,
        FROM_NUMBER_WITHOUT_PLUS_SIGN,
        FROM_DEFAULT_COUNTRY
    }

    @DexIgnore
    public final bw3 clear() {
        clearCountryCode();
        clearNationalNumber();
        clearExtension();
        clearItalianLeadingZero();
        clearNumberOfLeadingZeros();
        clearRawInput();
        clearCountryCodeSource();
        clearPreferredDomesticCarrierCode();
        return this;
    }

    @DexIgnore
    public bw3 clearCountryCode() {
        this.hasCountryCode = false;
        this.countryCode_ = 0;
        return this;
    }

    @DexIgnore
    public bw3 clearCountryCodeSource() {
        this.hasCountryCodeSource = false;
        this.countryCodeSource_ = a.FROM_NUMBER_WITH_PLUS_SIGN;
        return this;
    }

    @DexIgnore
    public bw3 clearExtension() {
        this.hasExtension = false;
        this.extension_ = "";
        return this;
    }

    @DexIgnore
    public bw3 clearItalianLeadingZero() {
        this.hasItalianLeadingZero = false;
        this.italianLeadingZero_ = false;
        return this;
    }

    @DexIgnore
    public bw3 clearNationalNumber() {
        this.hasNationalNumber = false;
        this.nationalNumber_ = 0;
        return this;
    }

    @DexIgnore
    public bw3 clearNumberOfLeadingZeros() {
        this.hasNumberOfLeadingZeros = false;
        this.numberOfLeadingZeros_ = 1;
        return this;
    }

    @DexIgnore
    public bw3 clearPreferredDomesticCarrierCode() {
        this.hasPreferredDomesticCarrierCode = false;
        this.preferredDomesticCarrierCode_ = "";
        return this;
    }

    @DexIgnore
    public bw3 clearRawInput() {
        this.hasRawInput = false;
        this.rawInput_ = "";
        return this;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof bw3) && exactlySameAs((bw3) obj);
    }

    @DexIgnore
    public boolean exactlySameAs(bw3 bw3) {
        if (bw3 == null) {
            return false;
        }
        if (this == bw3) {
            return true;
        }
        return this.countryCode_ == bw3.countryCode_ && this.nationalNumber_ == bw3.nationalNumber_ && this.extension_.equals(bw3.extension_) && this.italianLeadingZero_ == bw3.italianLeadingZero_ && this.numberOfLeadingZeros_ == bw3.numberOfLeadingZeros_ && this.rawInput_.equals(bw3.rawInput_) && this.countryCodeSource_ == bw3.countryCodeSource_ && this.preferredDomesticCarrierCode_.equals(bw3.preferredDomesticCarrierCode_) && hasPreferredDomesticCarrierCode() == bw3.hasPreferredDomesticCarrierCode();
    }

    @DexIgnore
    public int getCountryCode() {
        return this.countryCode_;
    }

    @DexIgnore
    public a getCountryCodeSource() {
        return this.countryCodeSource_;
    }

    @DexIgnore
    public String getExtension() {
        return this.extension_;
    }

    @DexIgnore
    public long getNationalNumber() {
        return this.nationalNumber_;
    }

    @DexIgnore
    public int getNumberOfLeadingZeros() {
        return this.numberOfLeadingZeros_;
    }

    @DexIgnore
    public String getPreferredDomesticCarrierCode() {
        return this.preferredDomesticCarrierCode_;
    }

    @DexIgnore
    public String getRawInput() {
        return this.rawInput_;
    }

    @DexIgnore
    public boolean hasCountryCode() {
        return this.hasCountryCode;
    }

    @DexIgnore
    public boolean hasCountryCodeSource() {
        return this.hasCountryCodeSource;
    }

    @DexIgnore
    public boolean hasExtension() {
        return this.hasExtension;
    }

    @DexIgnore
    public boolean hasItalianLeadingZero() {
        return this.hasItalianLeadingZero;
    }

    @DexIgnore
    public boolean hasNationalNumber() {
        return this.hasNationalNumber;
    }

    @DexIgnore
    public boolean hasNumberOfLeadingZeros() {
        return this.hasNumberOfLeadingZeros;
    }

    @DexIgnore
    public boolean hasPreferredDomesticCarrierCode() {
        return this.hasPreferredDomesticCarrierCode;
    }

    @DexIgnore
    public boolean hasRawInput() {
        return this.hasRawInput;
    }

    @DexIgnore
    public int hashCode() {
        int i = 1231;
        int countryCode = (((((((((((((((2173 + getCountryCode()) * 53) + Long.valueOf(getNationalNumber()).hashCode()) * 53) + getExtension().hashCode()) * 53) + (isItalianLeadingZero() ? 1231 : 1237)) * 53) + getNumberOfLeadingZeros()) * 53) + getRawInput().hashCode()) * 53) + getCountryCodeSource().hashCode()) * 53) + getPreferredDomesticCarrierCode().hashCode()) * 53;
        if (!hasPreferredDomesticCarrierCode()) {
            i = 1237;
        }
        return countryCode + i;
    }

    @DexIgnore
    public boolean isItalianLeadingZero() {
        return this.italianLeadingZero_;
    }

    @DexIgnore
    public bw3 mergeFrom(bw3 bw3) {
        if (bw3.hasCountryCode()) {
            setCountryCode(bw3.getCountryCode());
        }
        if (bw3.hasNationalNumber()) {
            setNationalNumber(bw3.getNationalNumber());
        }
        if (bw3.hasExtension()) {
            setExtension(bw3.getExtension());
        }
        if (bw3.hasItalianLeadingZero()) {
            setItalianLeadingZero(bw3.isItalianLeadingZero());
        }
        if (bw3.hasNumberOfLeadingZeros()) {
            setNumberOfLeadingZeros(bw3.getNumberOfLeadingZeros());
        }
        if (bw3.hasRawInput()) {
            setRawInput(bw3.getRawInput());
        }
        if (bw3.hasCountryCodeSource()) {
            setCountryCodeSource(bw3.getCountryCodeSource());
        }
        if (bw3.hasPreferredDomesticCarrierCode()) {
            setPreferredDomesticCarrierCode(bw3.getPreferredDomesticCarrierCode());
        }
        return this;
    }

    @DexIgnore
    public bw3 setCountryCode(int i) {
        this.hasCountryCode = true;
        this.countryCode_ = i;
        return this;
    }

    @DexIgnore
    public bw3 setCountryCodeSource(a aVar) {
        if (aVar != null) {
            this.hasCountryCodeSource = true;
            this.countryCodeSource_ = aVar;
            return this;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public bw3 setExtension(String str) {
        if (str != null) {
            this.hasExtension = true;
            this.extension_ = str;
            return this;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public bw3 setItalianLeadingZero(boolean z) {
        this.hasItalianLeadingZero = true;
        this.italianLeadingZero_ = z;
        return this;
    }

    @DexIgnore
    public bw3 setNationalNumber(long j) {
        this.hasNationalNumber = true;
        this.nationalNumber_ = j;
        return this;
    }

    @DexIgnore
    public bw3 setNumberOfLeadingZeros(int i) {
        this.hasNumberOfLeadingZeros = true;
        this.numberOfLeadingZeros_ = i;
        return this;
    }

    @DexIgnore
    public bw3 setPreferredDomesticCarrierCode(String str) {
        if (str != null) {
            this.hasPreferredDomesticCarrierCode = true;
            this.preferredDomesticCarrierCode_ = str;
            return this;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public bw3 setRawInput(String str) {
        if (str != null) {
            this.hasRawInput = true;
            this.rawInput_ = str;
            return this;
        }
        throw new NullPointerException();
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Country Code: ");
        sb.append(this.countryCode_);
        sb.append(" National Number: ");
        sb.append(this.nationalNumber_);
        if (hasItalianLeadingZero() && isItalianLeadingZero()) {
            sb.append(" Leading Zero(s): true");
        }
        if (hasNumberOfLeadingZeros()) {
            sb.append(" Number of leading zeros: ");
            sb.append(this.numberOfLeadingZeros_);
        }
        if (hasExtension()) {
            sb.append(" Extension: ");
            sb.append(this.extension_);
        }
        if (hasCountryCodeSource()) {
            sb.append(" Country Code Source: ");
            sb.append(this.countryCodeSource_);
        }
        if (hasPreferredDomesticCarrierCode()) {
            sb.append(" Preferred Domestic Carrier Code: ");
            sb.append(this.preferredDomesticCarrierCode_);
        }
        return sb.toString();
    }
}
