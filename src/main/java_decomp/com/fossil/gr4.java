package com.fossil;

import com.portfolio.platform.data.model.Explore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gr4 extends er4 {
    @DexIgnore
    public boolean d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gr4(String str, String str2, boolean z) {
        super(str, str2);
        wg6.b(str, "tagName");
        wg6.b(str2, Explore.COLUMN_TITLE);
        this.d = z;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.d = z;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
