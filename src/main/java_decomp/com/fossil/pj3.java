package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pj3 extends tj3 {
    @DexIgnore
    public /* final */ TextWatcher d; // = new a();
    @DexIgnore
    public /* final */ TextInputLayout.f e; // = new b();
    @DexIgnore
    public AnimatorSet f;
    @DexIgnore
    public ValueAnimator g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements TextWatcher {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void afterTextChanged(Editable editable) {
            if (!pj3.b(editable)) {
                pj3.this.f.cancel();
                pj3.this.g.start();
            } else if (!pj3.this.a.o()) {
                pj3.this.g.cancel();
                pj3.this.f.start();
            }
        }

        @DexIgnore
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @DexIgnore
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements TextInputLayout.f {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(pj3.b(editText.getText()));
            textInputLayout.setEndIconCheckable(false);
            editText.removeTextChangedListener(pj3.this.d);
            editText.addTextChangedListener(pj3.this.d);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements View.OnClickListener {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void onClick(View view) {
            pj3.this.a.getEditText().setText((CharSequence) null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AnimatorListenerAdapter {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            pj3.this.a.setEndIconVisible(true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            pj3.this.a.setEndIconVisible(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            pj3.this.c.setAlpha(((Float) valueAnimator.getAnimatedValue()).floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g implements ValueAnimator.AnimatorUpdateListener {
        @DexIgnore
        public g() {
        }

        @DexIgnore
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float floatValue = ((Float) valueAnimator.getAnimatedValue()).floatValue();
            pj3.this.c.setScaleX(floatValue);
            pj3.this.c.setScaleY(floatValue);
        }
    }

    @DexIgnore
    public pj3(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    @DexIgnore
    public final void d() {
        ValueAnimator c2 = c();
        ValueAnimator a2 = a(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, 1.0f);
        this.f = new AnimatorSet();
        this.f.playTogether(new Animator[]{c2, a2});
        this.f.addListener(new d());
        this.g = a(1.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.g.addListener(new e());
    }

    @DexIgnore
    public static boolean b(Editable editable) {
        return editable.length() > 0;
    }

    @DexIgnore
    public final ValueAnimator c() {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{0.8f, 1.0f});
        ofFloat.setInterpolator(yf3.d);
        ofFloat.setDuration(150);
        ofFloat.addUpdateListener(new g());
        return ofFloat;
    }

    @DexIgnore
    public void a() {
        this.a.setEndIconDrawable(u0.c(this.b, qf3.mtrl_ic_cancel));
        TextInputLayout textInputLayout = this.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(vf3.clear_text_end_icon_content_description));
        this.a.setEndIconOnClickListener(new c());
        this.a.a(this.e);
        d();
    }

    @DexIgnore
    public final ValueAnimator a(float... fArr) {
        ValueAnimator ofFloat = ValueAnimator.ofFloat(fArr);
        ofFloat.setInterpolator(yf3.a);
        ofFloat.setDuration(100);
        ofFloat.addUpdateListener(new f());
        return ofFloat;
    }
}
