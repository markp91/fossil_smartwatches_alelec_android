package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FailureCode;
import com.zendesk.sdk.deeplinking.ZendeskDeepLinkingParser;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import okhttp3.internal.publicsuffix.PublicSuffixDatabase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kq6 {
    @DexIgnore
    public static /* final */ Pattern j; // = Pattern.compile("(\\d{2,4})[^\\d]*");
    @DexIgnore
    public static /* final */ Pattern k; // = Pattern.compile("(?i)(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec).*");
    @DexIgnore
    public static /* final */ Pattern l; // = Pattern.compile("(\\d{1,2})[^\\d]*");
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("(\\d{1,2}):(\\d{1,2}):(\\d{1,2})[^\\d]*");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ boolean h;
    @DexIgnore
    public /* final */ boolean i;

    @DexIgnore
    public kq6(String str, String str2, long j2, String str3, String str4, boolean z, boolean z2, boolean z3, boolean z4) {
        this.a = str;
        this.b = str2;
        this.c = j2;
        this.d = str3;
        this.e = str4;
        this.f = z;
        this.g = z2;
        this.i = z3;
        this.h = z4;
    }

    @DexIgnore
    public String a() {
        return this.a;
    }

    @DexIgnore
    public String b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof kq6)) {
            return false;
        }
        kq6 kq6 = (kq6) obj;
        if (kq6.a.equals(this.a) && kq6.b.equals(this.b) && kq6.d.equals(this.d) && kq6.e.equals(this.e) && kq6.c == this.c && kq6.f == this.f && kq6.g == this.g && kq6.h == this.h && kq6.i == this.i) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j2 = this.c;
        return ((((((((((((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + ((int) (j2 ^ (j2 >>> 32)))) * 31) + (this.f ^ true ? 1 : 0)) * 31) + (this.g ^ true ? 1 : 0)) * 31) + (this.h ^ true ? 1 : 0)) * 31) + (this.i ^ true ? 1 : 0);
    }

    @DexIgnore
    public String toString() {
        return a(false);
    }

    @DexIgnore
    public static boolean a(String str, String str2) {
        if (str.equals(str2)) {
            return true;
        }
        if (!str.endsWith(str2) || str.charAt((str.length() - str2.length()) - 1) != '.' || fr6.d(str)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public static long b(String str) {
        try {
            long parseLong = Long.parseLong(str);
            if (parseLong <= 0) {
                return Long.MIN_VALUE;
            }
            return parseLong;
        } catch (NumberFormatException e2) {
            if (!str.matches("-?\\d+")) {
                throw e2;
            } else if (str.startsWith(ZendeskDeepLinkingParser.HelpCenterParser.HC_PATH_ELEMENT_NAME_SEPARATOR)) {
                return Long.MIN_VALUE;
            } else {
                return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            }
        }
    }

    @DexIgnore
    public static kq6 a(tq6 tq6, String str) {
        return a(System.currentTimeMillis(), tq6, str);
    }

    @DexIgnore
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00f7  */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0115 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0116  */
    public static kq6 a(long j2, tq6 tq6, String str) {
        long j3;
        String g2;
        String str2;
        kq6 kq6;
        String str3;
        String str4 = str;
        int length = str.length();
        char c2 = ';';
        int a2 = fr6.a(str4, 0, length, ';');
        char c3 = '=';
        int a3 = fr6.a(str4, 0, a2, '=');
        if (a3 == a2) {
            return null;
        }
        String d2 = fr6.d(str4, 0, a3);
        if (d2.isEmpty() || fr6.c(d2) != -1) {
            return null;
        }
        String d3 = fr6.d(str4, a3 + 1, a2);
        if (fr6.c(d3) != -1) {
            return null;
        }
        int i2 = a2 + 1;
        String str5 = null;
        String str6 = null;
        long j4 = -1;
        long j5 = 253402300799999L;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = true;
        boolean z4 = false;
        while (i2 < length) {
            int a4 = fr6.a(str4, i2, length, c2);
            int a5 = fr6.a(str4, i2, a4, c3);
            String d4 = fr6.d(str4, i2, a5);
            String d5 = a5 < a4 ? fr6.d(str4, a5 + 1, a4) : "";
            if (d4.equalsIgnoreCase("expires")) {
                try {
                    j5 = a(d5, 0, d5.length());
                } catch (IllegalArgumentException unused) {
                }
            } else if (d4.equalsIgnoreCase("max-age")) {
                j4 = b(d5);
            } else {
                if (d4.equalsIgnoreCase("domain")) {
                    str5 = a(d5);
                    z3 = false;
                } else if (d4.equalsIgnoreCase("path")) {
                    str6 = d5;
                } else if (d4.equalsIgnoreCase("secure")) {
                    z = true;
                } else if (d4.equalsIgnoreCase("httponly")) {
                    z2 = true;
                }
                i2 = a4 + 1;
                c2 = ';';
                c3 = '=';
            }
            z4 = true;
            i2 = a4 + 1;
            c2 = ';';
            c3 = '=';
        }
        long j6 = Long.MIN_VALUE;
        if (j4 != Long.MIN_VALUE) {
            if (j4 != -1) {
                j6 = j2 + (j4 <= 9223372036854775L ? j4 * 1000 : ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD);
                if (j6 < j2 || j6 > 253402300799999L) {
                    j3 = 253402300799999L;
                }
            } else {
                j3 = j5;
            }
            g2 = tq6.g();
            if (str5 != null) {
                str2 = g2;
                kq6 = null;
            } else if (!a(g2, str5)) {
                return null;
            } else {
                kq6 = null;
                str2 = str5;
            }
            if (g2.length() != str2.length() && PublicSuffixDatabase.c().a(str2) == null) {
                return kq6;
            }
            String str7 = ZendeskConfig.SLASH;
            String str8 = str6;
            if (str8 == null || !str8.startsWith(str7)) {
                String c4 = tq6.c();
                int lastIndexOf = c4.lastIndexOf(47);
                if (lastIndexOf != 0) {
                    str7 = c4.substring(0, lastIndexOf);
                }
                str3 = str7;
            } else {
                str3 = str8;
            }
            return new kq6(d2, d3, j3, str2, str3, z, z2, z3, z4);
        }
        j3 = j6;
        g2 = tq6.g();
        if (str5 != null) {
        }
        if (g2.length() != str2.length() || PublicSuffixDatabase.c().a(str2) == null) {
        }
    }

    @DexIgnore
    public static long a(String str, int i2, int i3) {
        int a2 = a(str, i2, i3, false);
        Matcher matcher = m.matcher(str);
        int i4 = -1;
        int i5 = -1;
        int i6 = -1;
        int i7 = -1;
        int i8 = -1;
        int i9 = -1;
        while (a2 < i3) {
            int a3 = a(str, a2 + 1, i3, true);
            matcher.region(a2, a3);
            if (i5 == -1 && matcher.usePattern(m).matches()) {
                int parseInt = Integer.parseInt(matcher.group(1));
                int parseInt2 = Integer.parseInt(matcher.group(2));
                i9 = Integer.parseInt(matcher.group(3));
                i8 = parseInt2;
                i5 = parseInt;
            } else if (i6 == -1 && matcher.usePattern(l).matches()) {
                i6 = Integer.parseInt(matcher.group(1));
            } else if (i7 == -1 && matcher.usePattern(k).matches()) {
                i7 = k.pattern().indexOf(matcher.group(1).toLowerCase(Locale.US)) / 4;
            } else if (i4 == -1 && matcher.usePattern(j).matches()) {
                i4 = Integer.parseInt(matcher.group(1));
            }
            a2 = a(str, a3 + 1, i3, false);
        }
        if (i4 >= 70 && i4 <= 99) {
            i4 += 1900;
        }
        if (i4 >= 0 && i4 <= 69) {
            i4 += FailureCode.FAILED_TO_ENABLE_MAINTAINING_CONNECTION;
        }
        if (i4 < 1601) {
            throw new IllegalArgumentException();
        } else if (i7 == -1) {
            throw new IllegalArgumentException();
        } else if (i6 < 1 || i6 > 31) {
            throw new IllegalArgumentException();
        } else if (i5 < 0 || i5 > 23) {
            throw new IllegalArgumentException();
        } else if (i8 < 0 || i8 > 59) {
            throw new IllegalArgumentException();
        } else if (i9 < 0 || i9 > 59) {
            throw new IllegalArgumentException();
        } else {
            GregorianCalendar gregorianCalendar = new GregorianCalendar(fr6.o);
            gregorianCalendar.setLenient(false);
            gregorianCalendar.set(1, i4);
            gregorianCalendar.set(2, i7 - 1);
            gregorianCalendar.set(5, i6);
            gregorianCalendar.set(11, i5);
            gregorianCalendar.set(12, i8);
            gregorianCalendar.set(13, i9);
            gregorianCalendar.set(14, 0);
            return gregorianCalendar.getTimeInMillis();
        }
    }

    @DexIgnore
    public static int a(String str, int i2, int i3, boolean z) {
        while (i2 < i3) {
            char charAt = str.charAt(i2);
            if (((charAt < ' ' && charAt != 9) || charAt >= 127 || (charAt >= '0' && charAt <= '9') || ((charAt >= 'a' && charAt <= 'z') || ((charAt >= 'A' && charAt <= 'Z') || charAt == ':'))) == (!z)) {
                return i2;
            }
            i2++;
        }
        return i3;
    }

    @DexIgnore
    public static String a(String str) {
        if (!str.endsWith(".")) {
            if (str.startsWith(".")) {
                str = str.substring(1);
            }
            String a2 = fr6.a(str);
            if (a2 != null) {
                return a2;
            }
            throw new IllegalArgumentException();
        }
        throw new IllegalArgumentException();
    }

    @DexIgnore
    public static List<kq6> a(tq6 tq6, sq6 sq6) {
        List<String> b2 = sq6.b("Set-Cookie");
        int size = b2.size();
        ArrayList arrayList = null;
        for (int i2 = 0; i2 < size; i2++) {
            kq6 a2 = a(tq6, b2.get(i2));
            if (a2 != null) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(a2);
            }
        }
        if (arrayList != null) {
            return Collections.unmodifiableList(arrayList);
        }
        return Collections.emptyList();
    }

    @DexIgnore
    public String a(boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.a);
        sb.append('=');
        sb.append(this.b);
        if (this.h) {
            if (this.c == Long.MIN_VALUE) {
                sb.append("; max-age=0");
            } else {
                sb.append("; expires=");
                sb.append(xr6.a(new Date(this.c)));
            }
        }
        if (!this.i) {
            sb.append("; domain=");
            if (z) {
                sb.append(".");
            }
            sb.append(this.d);
        }
        sb.append("; path=");
        sb.append(this.e);
        if (this.f) {
            sb.append("; secure");
        }
        if (this.g) {
            sb.append("; httponly");
        }
        return sb.toString();
    }
}
