package com.fossil;

import java.io.Serializable;
import java.io.Writer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class xu6 extends Writer implements Serializable {
    @DexIgnore
    public /* final */ StringBuilder builder;

    @DexIgnore
    public xu6() {
        this.builder = new StringBuilder();
    }

    @DexIgnore
    public void close() {
    }

    @DexIgnore
    public void flush() {
    }

    @DexIgnore
    public StringBuilder getBuilder() {
        return this.builder;
    }

    @DexIgnore
    public String toString() {
        return this.builder.toString();
    }

    @DexIgnore
    public void write(String str) {
        if (str != null) {
            this.builder.append(str);
        }
    }

    @DexIgnore
    public void write(char[] cArr, int i, int i2) {
        if (cArr != null) {
            this.builder.append(cArr, i, i2);
        }
    }

    @DexIgnore
    public xu6(int i) {
        this.builder = new StringBuilder(i);
    }

    @DexIgnore
    public Writer append(char c) {
        this.builder.append(c);
        return this;
    }

    @DexIgnore
    public xu6(StringBuilder sb) {
        this.builder = sb == null ? new StringBuilder() : sb;
    }

    @DexIgnore
    public Writer append(CharSequence charSequence) {
        this.builder.append(charSequence);
        return this;
    }

    @DexIgnore
    public Writer append(CharSequence charSequence, int i, int i2) {
        this.builder.append(charSequence, i, i2);
        return this;
    }
}
