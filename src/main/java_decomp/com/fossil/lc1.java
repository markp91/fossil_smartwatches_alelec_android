package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc1 implements Parcelable.Creator<ge1> {
    @DexIgnore
    public /* synthetic */ lc1(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            qn0 valueOf = qn0.valueOf(readString);
            String readString2 = parcel.readString();
            if (readString2 != null) {
                wg6.a(readString2, "parcel.readString()!!");
                dn1 valueOf2 = dn1.valueOf(readString2);
                String readString3 = parcel.readString();
                if (readString3 != null) {
                    wg6.a(readString3, "parcel.readString()!!");
                    ip0 valueOf3 = ip0.valueOf(readString3);
                    String readString4 = parcel.readString();
                    if (readString4 != null) {
                        wg6.a(readString4, "parcel.readString()!!");
                        return new ge1(valueOf, valueOf2, valueOf3, ar0.valueOf(readString4), (short) parcel.readInt());
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ge1[i];
    }
}
