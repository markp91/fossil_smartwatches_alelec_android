package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c93 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ l43 a;
    @DexIgnore
    public /* final */ /* synthetic */ d93 b;

    @DexIgnore
    public c93(d93 d93, l43 l43) {
        this.b = d93;
        this.a = l43;
    }

    @DexIgnore
    public final void run() {
        synchronized (this.b) {
            boolean unused = this.b.a = false;
            if (!this.b.c.A()) {
                this.b.c.b().B().a("Connected to service");
                this.b.c.a(this.a);
            }
        }
    }
}
