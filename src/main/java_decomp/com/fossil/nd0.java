package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ rd0 a;
    @DexIgnore
    public /* final */ qd0[] b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<nd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                rd0 valueOf = rd0.valueOf(readString);
                Object[] createTypedArray = parcel.createTypedArray(qd0.CREATOR);
                if (createTypedArray != null) {
                    wg6.a(createTypedArray, "parcel.createTypedArray(\u2026AppDeclaration.CREATOR)!!");
                    return new nd0(valueOf, (qd0[]) createTypedArray);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new nd0[i];
        }
    }

    @DexIgnore
    public nd0(rd0 rd0, qd0[] qd0Arr) {
        this.a = rd0;
        this.b = qd0Arr;
    }

    @DexIgnore
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (qd0 a2 : this.b) {
            jSONArray.put(a2.a());
        }
        return cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.BUTTON, (Object) cw0.a((Enum<?>) this.a)), bm0.DECLARATIONS, (Object) jSONArray), bm0.TOTAL_DECLARATIONS, (Object) Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(nd0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            nd0 nd0 = (nd0) obj;
            rd0 rd0 = this.a;
            rd0 rd02 = nd0.a;
            return rd0 == rd02 && rd0 == rd02 && Arrays.equals(this.b, nd0.b);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.MicroAppMapping");
    }

    @DexIgnore
    public final rd0 getMicroAppButton() {
        return this.a;
    }

    @DexIgnore
    public final qd0[] getMicroAppDeclarations() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.b, i);
        }
    }
}
