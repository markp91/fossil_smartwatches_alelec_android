package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xk6 {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ Object b;
    @DexIgnore
    public /* final */ fn6 c;

    @DexIgnore
    public xk6(Object obj, Object obj2, fn6 fn6) {
        wg6.b(fn6, "token");
        this.a = obj;
        this.b = obj2;
        this.c = fn6;
    }

    @DexIgnore
    public String toString() {
        return "CompletedIdempotentResult[" + this.b + ']';
    }
}
