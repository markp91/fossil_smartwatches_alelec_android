package com.fossil;

import com.fossil.kz;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hz implements kz, jz {
    @DexIgnore
    public /* final */ Object a;
    @DexIgnore
    public /* final */ kz b;
    @DexIgnore
    public volatile jz c;
    @DexIgnore
    public volatile jz d;
    @DexIgnore
    public kz.a e;
    @DexIgnore
    public kz.a f;

    @DexIgnore
    public hz(Object obj, kz kzVar) {
        kz.a aVar = kz.a.CLEARED;
        this.e = aVar;
        this.f = aVar;
        this.a = obj;
        this.b = kzVar;
    }

    @DexIgnore
    public void a(jz jzVar, jz jzVar2) {
        this.c = jzVar;
        this.d = jzVar2;
    }

    @DexIgnore
    public final boolean b() {
        kz kzVar = this.b;
        return kzVar == null || kzVar.f(this);
    }

    @DexIgnore
    public void c() {
        synchronized (this.a) {
            if (this.e != kz.a.RUNNING) {
                this.e = kz.a.RUNNING;
                this.c.c();
            }
        }
    }

    @DexIgnore
    public void clear() {
        synchronized (this.a) {
            this.e = kz.a.CLEARED;
            this.c.clear();
            if (this.f != kz.a.CLEARED) {
                this.f = kz.a.CLEARED;
                this.d.clear();
            }
        }
    }

    @DexIgnore
    public boolean d(jz jzVar) {
        boolean z;
        synchronized (this.a) {
            z = i() && g(jzVar);
        }
        return z;
    }

    @DexIgnore
    public void e() {
        synchronized (this.a) {
            if (this.e == kz.a.RUNNING) {
                this.e = kz.a.PAUSED;
                this.c.e();
            }
            if (this.f == kz.a.RUNNING) {
                this.f = kz.a.PAUSED;
                this.d.e();
            }
        }
    }

    @DexIgnore
    public boolean f() {
        boolean z;
        synchronized (this.a) {
            z = this.e == kz.a.CLEARED && this.f == kz.a.CLEARED;
        }
        return z;
    }

    @DexIgnore
    public boolean g() {
        boolean z;
        synchronized (this.a) {
            if (this.e != kz.a.SUCCESS) {
                if (this.f != kz.a.SUCCESS) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public final boolean h() {
        kz kzVar = this.b;
        return kzVar == null || kzVar.c(this);
    }

    @DexIgnore
    public final boolean i() {
        kz kzVar = this.b;
        return kzVar == null || kzVar.d(this);
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z;
        synchronized (this.a) {
            if (this.e != kz.a.RUNNING) {
                if (this.f != kz.a.RUNNING) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002e, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001f, code lost:
        return;
     */
    @DexIgnore
    public void b(jz jzVar) {
        synchronized (this.a) {
            if (!jzVar.equals(this.d)) {
                this.e = kz.a.FAILED;
                if (this.f != kz.a.RUNNING) {
                    this.f = kz.a.RUNNING;
                    this.d.c();
                }
            } else {
                this.f = kz.a.FAILED;
                if (this.b != null) {
                    this.b.b(this);
                }
            }
        }
    }

    @DexIgnore
    public boolean a(jz jzVar) {
        if (!(jzVar instanceof hz)) {
            return false;
        }
        hz hzVar = (hz) jzVar;
        if (!this.c.a(hzVar.c) || !this.d.a(hzVar.d)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public boolean d() {
        boolean z;
        synchronized (this.a) {
            if (!this.c.d()) {
                if (!this.d.d()) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    @DexIgnore
    public boolean f(jz jzVar) {
        boolean z;
        synchronized (this.a) {
            z = b() && g(jzVar);
        }
        return z;
    }

    @DexIgnore
    public final boolean g(jz jzVar) {
        return jzVar.equals(this.c) || (this.e == kz.a.FAILED && jzVar.equals(this.d));
    }

    @DexIgnore
    public kz a() {
        kz a2;
        synchronized (this.a) {
            a2 = this.b != null ? this.b.a() : this;
        }
        return a2;
    }

    @DexIgnore
    public boolean c(jz jzVar) {
        boolean z;
        synchronized (this.a) {
            z = h() && g(jzVar);
        }
        return z;
    }

    @DexIgnore
    public void e(jz jzVar) {
        synchronized (this.a) {
            if (jzVar.equals(this.c)) {
                this.e = kz.a.SUCCESS;
            } else if (jzVar.equals(this.d)) {
                this.f = kz.a.SUCCESS;
            }
            if (this.b != null) {
                this.b.e(this);
            }
        }
    }
}
