package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nj6 extends xg6 implements hg6<T, Boolean> {
    @DexIgnore
    public /* final */ /* synthetic */ int $value$inlined;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nj6(int i) {
        super(1);
        this.$value$inlined = i;
    }

    @DexIgnore
    public /* bridge */ /* synthetic */ Object invoke(Object obj) {
        return Boolean.valueOf(invoke((Enum) obj));
    }

    @DexIgnore
    public final boolean invoke(T t) {
        gj6 gj6 = (gj6) t;
        return (this.$value$inlined & gj6.getMask()) == gj6.getValue();
    }
}
