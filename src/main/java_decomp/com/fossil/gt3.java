package com.fossil;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import java.util.concurrent.ExecutorService;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"UnwrappedWakefulBroadcastReceiver"})
public abstract class gt3 extends Service {
    @DexIgnore
    public /* final */ ExecutorService a;
    @DexIgnore
    public Binder b;
    @DexIgnore
    public /* final */ Object c;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore
    public gt3() {
        hb2 a2 = gb2.a();
        String valueOf = String.valueOf(getClass().getSimpleName());
        this.a = a2.a(new y42(valueOf.length() != 0 ? "Firebase-".concat(valueOf) : new String("Firebase-")), lb2.a);
        this.c = new Object();
        this.e = 0;
    }

    @DexIgnore
    public Intent a(Intent intent) {
        return intent;
    }

    @DexIgnore
    public final /* synthetic */ void a(Intent intent, qc3 qc3) {
        f(intent);
    }

    @DexIgnore
    public boolean b(Intent intent) {
        return false;
    }

    @DexIgnore
    public abstract void c(Intent intent);

    @DexIgnore
    /* renamed from: e */
    public final qc3<Void> d(Intent intent) {
        if (b(intent)) {
            return tc3.a(null);
        }
        rc3 rc3 = new rc3();
        this.a.execute(new it3(this, intent, rc3));
        return rc3.a();
    }

    @DexIgnore
    public final void f(Intent intent) {
        if (intent != null) {
            rr3.a(intent);
        }
        synchronized (this.c) {
            this.e--;
            if (this.e == 0) {
                stopSelfResult(this.d);
            }
        }
    }

    @DexIgnore
    public final synchronized IBinder onBind(Intent intent) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "Service received bind request");
        }
        if (this.b == null) {
            this.b = new ur3(new jt3(this));
        }
        return this.b;
    }

    @DexIgnore
    public void onDestroy() {
        this.a.shutdown();
        super.onDestroy();
    }

    @DexIgnore
    public final int onStartCommand(Intent intent, int i, int i2) {
        synchronized (this.c) {
            this.d = i2;
            this.e++;
        }
        Intent a2 = a(intent);
        if (a2 == null) {
            f(intent);
            return 2;
        }
        qc3<Void> e2 = d(a2);
        if (e2.d()) {
            f(intent);
            return 2;
        }
        e2.a(lt3.a, (kc3<Void>) new kt3(this, intent));
        return 3;
    }
}
