package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum wp0 {
    FOSSIL((byte) 1);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public wp0(byte b) {
        this.a = b;
    }
}
