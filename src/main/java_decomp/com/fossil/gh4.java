package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gh4 extends fh4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j G; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray H; // = new SparseIntArray();
    @DexIgnore
    public long F;

    /*
    static {
        H.put(2131363273, 1);
        H.put(2131362643, 2);
        H.put(2131362456, 3);
        H.put(2131363265, 4);
        H.put(2131362455, 5);
        H.put(2131362533, 6);
        H.put(2131362290, 7);
        H.put(2131362568, 8);
        H.put(2131362332, 9);
        H.put(2131362627, 10);
        H.put(2131362432, 11);
        H.put(2131363262, 12);
        H.put(2131362550, 13);
        H.put(2131362306, 14);
        H.put(2131362597, 15);
        H.put(2131362359, 16);
        H.put(2131362598, 17);
        H.put(2131362360, 18);
    }
    */

    @DexIgnore
    public gh4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 19, G, H));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.F = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.F != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.F = 1;
        }
        g();
    }

    @DexIgnore
    public gh4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[7], objArr[14], objArr[9], objArr[16], objArr[18], objArr[11], objArr[5], objArr[3], objArr[6], objArr[13], objArr[8], objArr[15], objArr[17], objArr[10], objArr[2], objArr[12], objArr[4], objArr[1]);
        this.F = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
