package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.utils.DeviceIdentityUtils;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dv6 {
    @DexIgnore
    public static /* final */ Object a; // = "y";
    @DexIgnore
    public static /* final */ Object b; // = "M";
    @DexIgnore
    public static /* final */ Object c; // = "d";
    @DexIgnore
    public static /* final */ Object d; // = "H";
    @DexIgnore
    public static /* final */ Object e; // = "m";
    @DexIgnore
    public static /* final */ Object f; // = "s";
    @DexIgnore
    public static /* final */ Object g; // = DeviceIdentityUtils.SHINE_SERIAL_NUMBER_PREFIX;

    @DexIgnore
    public static String a(long j) {
        return a(j, "HH:mm:ss.SSS");
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ Object a;
        @DexIgnore
        public int b; // = 1;

        @DexIgnore
        public a(Object obj) {
            this.a = obj;
        }

        @DexIgnore
        public static boolean a(a[] aVarArr, Object obj) {
            for (a b2 : aVarArr) {
                if (b2.b() == obj) {
                    return true;
                }
            }
            return false;
        }

        @DexIgnore
        public Object b() {
            return this.a;
        }

        @DexIgnore
        public void c() {
            this.b++;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            if (this.a.getClass() != aVar.a.getClass() || this.b != aVar.b) {
                return false;
            }
            Object obj2 = this.a;
            if (obj2 instanceof StringBuilder) {
                return obj2.toString().equals(aVar.a.toString());
            }
            if (obj2 instanceof Number) {
                return obj2.equals(aVar.a);
            }
            if (obj2 == aVar.a) {
                return true;
            }
            return false;
        }

        @DexIgnore
        public int hashCode() {
            return this.a.hashCode();
        }

        @DexIgnore
        public String toString() {
            return bv6.a(this.a.toString(), this.b);
        }

        @DexIgnore
        public int a() {
            return this.b;
        }
    }

    @DexIgnore
    public static String a(long j, String str) {
        return a(j, str, true);
    }

    @DexIgnore
    public static String a(long j, String str, boolean z) {
        long j2;
        long j3;
        long j4;
        long j5;
        long j6;
        long j7;
        cv6.a(0, ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD, j, "durationMillis must not be negative");
        a[] a2 = a(str);
        if (a.a(a2, c)) {
            long j8 = j / LogBuilder.MAX_INTERVAL;
            j3 = j - (LogBuilder.MAX_INTERVAL * j8);
            j2 = j8;
        } else {
            j3 = j;
            j2 = 0;
        }
        if (a.a(a2, d)) {
            long j9 = j3 / 3600000;
            j3 -= 3600000 * j9;
            j4 = j9;
        } else {
            j4 = 0;
        }
        if (a.a(a2, e)) {
            long j10 = j3 / 60000;
            j3 -= 60000 * j10;
            j5 = j10;
        } else {
            j5 = 0;
        }
        if (a.a(a2, f)) {
            long j11 = j3 / 1000;
            j6 = j3 - (1000 * j11);
            j7 = j11;
        } else {
            j7 = 0;
            j6 = j3;
        }
        return a(a2, 0, 0, j2, j4, j5, j7, j6, z);
    }

    @DexIgnore
    public static String a(a[] aVarArr, long j, long j2, long j3, long j4, long j5, long j6, long j7, boolean z) {
        int i;
        int i2;
        a[] aVarArr2 = aVarArr;
        long j8 = j7;
        boolean z2 = z;
        StringBuilder sb = new StringBuilder();
        int length = aVarArr2.length;
        int i3 = 0;
        boolean z3 = false;
        while (i3 < length) {
            a aVar = aVarArr2[i3];
            Object b2 = aVar.b();
            int a2 = aVar.a();
            if (b2 instanceof StringBuilder) {
                sb.append(b2.toString());
                long j9 = j4;
                long j10 = j5;
                i2 = length;
                i = i3;
            } else {
                if (b2.equals(a)) {
                    sb.append(a(j, z2, a2));
                    long j11 = j4;
                    long j12 = j5;
                    i2 = length;
                    i = i3;
                } else {
                    long j13 = j;
                    if (b2.equals(b)) {
                        i = i3;
                        sb.append(a(j2, z2, a2));
                    } else {
                        i = i3;
                        long j14 = j2;
                        if (b2.equals(c)) {
                            sb.append(a(j3, z2, a2));
                        } else {
                            long j15 = j3;
                            if (b2.equals(d)) {
                                sb.append(a(j4, z2, a2));
                                long j16 = j5;
                                i2 = length;
                            } else {
                                long j17 = j4;
                                if (b2.equals(e)) {
                                    sb.append(a(j5, z2, a2));
                                    i2 = length;
                                } else {
                                    long j18 = j5;
                                    if (b2.equals(f)) {
                                        i2 = length;
                                        sb.append(a(j6, z2, a2));
                                        z3 = true;
                                    } else {
                                        i2 = length;
                                        long j19 = j6;
                                        if (b2.equals(g)) {
                                            if (z3) {
                                                int i4 = 3;
                                                if (z2) {
                                                    i4 = Math.max(3, a2);
                                                }
                                                sb.append(a(j8, true, i4));
                                            } else {
                                                sb.append(a(j8, z2, a2));
                                            }
                                            z3 = false;
                                        }
                                    }
                                    i3 = i + 1;
                                    length = i2;
                                    aVarArr2 = aVarArr;
                                }
                            }
                        }
                    }
                    long j20 = j4;
                    long j162 = j5;
                    i2 = length;
                }
                z3 = false;
            }
            long j21 = j6;
            i3 = i + 1;
            length = i2;
            aVarArr2 = aVarArr;
        }
        return sb.toString();
    }

    @DexIgnore
    public static String a(long j, boolean z, int i) {
        String l = Long.toString(j);
        return z ? bv6.a(l, i, '0') : l;
    }

    @DexIgnore
    public static a[] a(String str) {
        Object obj;
        ArrayList arrayList = new ArrayList(str.length());
        StringBuilder sb = null;
        a aVar = null;
        boolean z = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (!z || charAt == '\'') {
                if (charAt != '\'') {
                    if (charAt == 'H') {
                        obj = d;
                    } else if (charAt == 'M') {
                        obj = b;
                    } else if (charAt == 'S') {
                        obj = g;
                    } else if (charAt == 'd') {
                        obj = c;
                    } else if (charAt == 'm') {
                        obj = e;
                    } else if (charAt == 's') {
                        obj = f;
                    } else if (charAt != 'y') {
                        if (sb == null) {
                            sb = new StringBuilder();
                            arrayList.add(new a(sb));
                        }
                        sb.append(charAt);
                        obj = null;
                    } else {
                        obj = a;
                    }
                } else if (z) {
                    sb = null;
                    obj = null;
                    z = false;
                } else {
                    StringBuilder sb2 = new StringBuilder();
                    arrayList.add(new a(sb2));
                    obj = null;
                    sb = sb2;
                    z = true;
                }
                if (obj != null) {
                    if (aVar == null || !aVar.b().equals(obj)) {
                        aVar = new a(obj);
                        arrayList.add(aVar);
                    } else {
                        aVar.c();
                    }
                    sb = null;
                }
            } else {
                sb.append(charAt);
            }
        }
        if (!z) {
            return (a[]) arrayList.toArray(new a[arrayList.size()]);
        }
        throw new IllegalArgumentException("Unmatched quote in format: " + str);
    }
}
