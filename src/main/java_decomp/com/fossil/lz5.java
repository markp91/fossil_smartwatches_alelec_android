package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.SectionIndexer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lz5 extends RecyclerView.AdapterDataObserver {
    @DexIgnore
    @SuppressLint({"HandlerLeak"})
    public Handler A; // = new a();
    @DexIgnore
    public float a;
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public float d;
    @DexIgnore
    public float e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g;
    @DexIgnore
    public int h; // = -1;
    @DexIgnore
    public boolean i; // = false;
    @DexIgnore
    public RecyclerView j; // = null;
    @DexIgnore
    public SectionIndexer k; // = null;
    @DexIgnore
    public String[] l; // = null;
    @DexIgnore
    public RectF m;
    @DexIgnore
    public int n;
    @DexIgnore
    public float o;
    @DexIgnore
    public float p;
    @DexIgnore
    public int q;
    @DexIgnore
    public boolean r; // = true;
    @DexIgnore
    public int s;
    @DexIgnore
    public Typeface t; // = null;
    @DexIgnore
    public Boolean u; // = true;
    @DexIgnore
    public Boolean v; // = false;
    @DexIgnore
    public int w;
    @DexIgnore
    public int x;
    @DexIgnore
    public int y;
    @DexIgnore
    public int z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends Handler {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void handleMessage(Message message) {
            super.handleMessage(message);
            if (message.what == 1) {
                lz5.this.j.invalidate();
            }
        }
    }

    @DexIgnore
    public lz5(Context context, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        this.n = alphabetFastScrollRecyclerView.d;
        this.o = alphabetFastScrollRecyclerView.e;
        this.p = alphabetFastScrollRecyclerView.f;
        this.q = alphabetFastScrollRecyclerView.g;
        this.s = alphabetFastScrollRecyclerView.h;
        this.w = alphabetFastScrollRecyclerView.j;
        this.x = alphabetFastScrollRecyclerView.o;
        this.y = alphabetFastScrollRecyclerView.p;
        this.z = a(alphabetFastScrollRecyclerView.i);
        this.d = context.getResources().getDisplayMetrics().density;
        this.e = context.getResources().getDisplayMetrics().scaledDensity;
        this.j = alphabetFastScrollRecyclerView;
        a(this.j.getAdapter());
        float f2 = this.o;
        float f3 = this.d;
        this.a = f2 * f3;
        this.b = this.p * f3;
        this.c = ((float) this.q) * f3;
    }

    @DexIgnore
    public final int a(float f2) {
        return (int) (f2 * 255.0f);
    }

    @DexIgnore
    public final void b() {
        try {
            int positionForSection = this.k.getPositionForSection(this.h);
            LinearLayoutManager layoutManager = this.j.getLayoutManager();
            if (layoutManager instanceof LinearLayoutManager) {
                layoutManager.f(positionForSection, 0);
            } else {
                layoutManager.i(positionForSection);
            }
        } catch (Exception unused) {
            Log.d("INDEX_BAR", "Data size returns null");
        }
    }

    @DexIgnore
    public void c(float f2) {
        this.z = a(f2);
    }

    @DexIgnore
    public void d(float f2) {
        this.b = f2;
    }

    @DexIgnore
    public void e(int i2) {
        this.n = i2;
    }

    @DexIgnore
    public void f(int i2) {
        this.q = i2;
    }

    @DexIgnore
    public void a(Canvas canvas) {
        int i2;
        Canvas canvas2 = canvas;
        if (this.u.booleanValue()) {
            Paint paint = new Paint();
            paint.setColor(this.w);
            paint.setAlpha(this.z);
            paint.setAntiAlias(true);
            RectF rectF = this.m;
            int i3 = this.s;
            float f2 = this.d;
            canvas2.drawRoundRect(rectF, ((float) i3) * f2, ((float) i3) * f2, paint);
            String[] strArr = this.l;
            if (strArr != null && strArr.length > 0) {
                if (this.r && (i2 = this.h) >= 0 && strArr[i2] != "") {
                    Paint paint2 = new Paint();
                    paint2.setColor(-16777216);
                    paint2.setAlpha(0);
                    paint2.setAntiAlias(true);
                    paint2.setShadowLayer(3.0f, 0.0f, 0.0f, Color.argb(64, 0, 0, 0));
                    Paint paint3 = new Paint();
                    paint3.setColor(-1);
                    paint3.setAntiAlias(true);
                    paint3.setTextSize(this.e * 50.0f);
                    paint3.setTypeface(this.t);
                    float descent = ((this.c * 2.0f) + paint3.descent()) - paint3.ascent();
                    int i4 = this.f;
                    int i5 = this.g;
                    RectF rectF2 = new RectF((((float) i4) - descent) / 1.15f, (((float) i5) - descent) / 5.0f, ((((float) i4) - descent) / 1.0f) + (descent / 2.0f), ((((float) i5) - descent) / 5.0f) + descent);
                    float f3 = this.d;
                    canvas2.drawRoundRect(rectF2, f3 * 5.0f, f3 * 5.0f, paint2);
                    float f4 = rectF2.right;
                    float f5 = rectF2.left;
                    canvas2.drawText(this.l[this.h], (f5 + ((f4 - f5) / 2.0f)) - (paint3.measureText(this.l[this.h]) / 2.0f), ((rectF2.top + this.c) - paint3.ascent()) + 1.0f, paint3);
                    a(300);
                }
                Paint paint4 = new Paint();
                paint4.setColor(this.x);
                paint4.setAntiAlias(true);
                paint4.setTextSize(((float) this.n) * this.e);
                paint4.setTypeface(this.t);
                float height = (this.m.height() - (this.b * 2.0f)) / ((float) this.l.length);
                float descent2 = (height - (paint4.descent() - paint4.ascent())) / 2.0f;
                for (int i6 = 0; i6 < this.l.length; i6++) {
                    if (!this.v.booleanValue()) {
                        String str = this.l[i6];
                        RectF rectF3 = this.m;
                        canvas2.drawText(str, rectF3.left + ((this.a - paint4.measureText(this.l[i6])) / 2.0f), (((rectF3.top + this.b) + (((float) i6) * height)) + descent2) - paint4.ascent(), paint4);
                    } else {
                        int i7 = this.h;
                        if (i7 <= -1 || i6 != i7) {
                            paint4.setTypeface(this.t);
                            paint4.setTextSize(((float) this.n) * this.e);
                            paint4.setColor(this.x);
                        } else {
                            paint4.setTypeface(Typeface.create(this.t, 1));
                            paint4.setTextSize(((float) (this.n + 3)) * this.e);
                            paint4.setColor(this.y);
                        }
                        String str2 = this.l[i6];
                        RectF rectF4 = this.m;
                        canvas2.drawText(str2, rectF4.left + ((this.a - paint4.measureText(this.l[i6])) / 2.0f), (((rectF4.top + this.b) + (((float) i6) * height)) + descent2) - paint4.ascent(), paint4);
                    }
                }
            }
        }
    }

    @DexIgnore
    public void c(boolean z2) {
        this.r = z2;
    }

    @DexIgnore
    public void d(int i2) {
        this.x = i2;
    }

    @DexIgnore
    public void e(float f2) {
        this.a = f2;
    }

    @DexIgnore
    public void c(int i2) {
        this.y = i2;
    }

    @DexIgnore
    public final int b(float f2) {
        String[] strArr = this.l;
        if (strArr == null || strArr.length == 0) {
            return 0;
        }
        RectF rectF = this.m;
        float f3 = rectF.top;
        if (f2 < this.b + f3) {
            return 0;
        }
        float height = f3 + rectF.height();
        float f4 = this.b;
        if (f2 >= height - f4) {
            return this.l.length - 1;
        }
        RectF rectF2 = this.m;
        return (int) (((f2 - rectF2.top) - f4) / ((rectF2.height() - (this.b * 2.0f)) / ((float) this.l.length)));
    }

    @DexIgnore
    public void b(int i2) {
        this.s = i2;
    }

    @DexIgnore
    public void b(boolean z2) {
        this.u = Boolean.valueOf(z2);
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2 && this.i) {
                    if (a(motionEvent.getX(), motionEvent.getY())) {
                        this.h = b(motionEvent.getY());
                        b();
                    }
                    return true;
                }
            } else if (this.i) {
                this.i = false;
                this.h = -1;
            }
        } else if (a(motionEvent.getX(), motionEvent.getY())) {
            this.i = true;
            this.h = b(motionEvent.getY());
            b();
            return true;
        }
        return false;
    }

    @DexIgnore
    public void a(int i2, int i3, int i4, int i5) {
        this.f = i2;
        this.g = i3;
        float f2 = (float) i2;
        float f3 = this.b;
        this.m = new RectF((f2 - f3) - this.a, f3, f2 - f3, ((float) i3) - f3);
    }

    @DexIgnore
    public void a(RecyclerView.g gVar) {
        if (gVar instanceof SectionIndexer) {
            gVar.registerAdapterDataObserver(this);
            this.k = (SectionIndexer) gVar;
            this.l = (String[]) this.k.getSections();
        }
    }

    @DexIgnore
    public void a() {
        lz5.super.a();
        this.l = (String[]) this.k.getSections();
    }

    @DexIgnore
    public boolean a(float f2, float f3) {
        RectF rectF = this.m;
        if (f2 >= rectF.left) {
            float f4 = rectF.top;
            return f3 >= f4 && f3 <= f4 + rectF.height();
        }
    }

    @DexIgnore
    public final void a(long j2) {
        this.A.removeMessages(0);
        this.A.sendEmptyMessageAtTime(1, SystemClock.uptimeMillis() + j2);
    }

    @DexIgnore
    public void a(Typeface typeface) {
        this.t = typeface;
    }

    @DexIgnore
    public void a(int i2) {
        this.w = i2;
    }

    @DexIgnore
    public void a(boolean z2) {
        this.v = Boolean.valueOf(z2);
    }
}
