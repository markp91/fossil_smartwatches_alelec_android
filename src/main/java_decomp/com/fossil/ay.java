package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ay implements cy<Drawable, byte[]> {
    @DexIgnore
    public /* final */ au a;
    @DexIgnore
    public /* final */ cy<Bitmap, byte[]> b;
    @DexIgnore
    public /* final */ cy<qx, byte[]> c;

    @DexIgnore
    public ay(au auVar, cy<Bitmap, byte[]> cyVar, cy<qx, byte[]> cyVar2) {
        this.a = auVar;
        this.b = cyVar;
        this.c = cyVar2;
    }

    @DexIgnore
    public static rt<qx> a(rt<Drawable> rtVar) {
        return rtVar;
    }

    @DexIgnore
    public rt<byte[]> a(rt<Drawable> rtVar, xr xrVar) {
        Drawable drawable = rtVar.get();
        if (drawable instanceof BitmapDrawable) {
            return this.b.a(hw.a(((BitmapDrawable) drawable).getBitmap(), this.a), xrVar);
        }
        if (!(drawable instanceof qx)) {
            return null;
        }
        cy<qx, byte[]> cyVar = this.c;
        a(rtVar);
        return cyVar.a(rtVar, xrVar);
    }
}
