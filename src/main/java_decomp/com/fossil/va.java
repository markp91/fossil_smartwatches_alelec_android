package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface va {
    @DexIgnore
    ColorStateList getSupportButtonTintList();

    @DexIgnore
    void setSupportButtonTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportButtonTintMode(PorterDuff.Mode mode);
}
