package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cn extends dn<Boolean> {
    @DexIgnore
    public cn(Context context, to toVar) {
        super(pn.a(context, toVar).b());
    }

    @DexIgnore
    public boolean a(zn znVar) {
        return znVar.j.f();
    }

    @DexIgnore
    /* renamed from: a */
    public boolean b(Boolean bool) {
        return !bool.booleanValue();
    }
}
