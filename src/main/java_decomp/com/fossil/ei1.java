package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ei1 {
    @DexIgnore
    public int a;
    @DexIgnore
    public long b;

    @DexIgnore
    public ei1(int i, long j) {
        this.a = i;
        this.b = j;
    }

    @DexIgnore
    public final ei1 a(int i, long j) {
        return new ei1(i, j);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ei1)) {
            return false;
        }
        ei1 ei1 = (ei1) obj;
        return this.a == ei1.a && this.b == ei1.b;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        return (this.a * 31) + ((int) (j ^ (j >>> 32)));
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("ExponentBackOffInfo(currentExponent=");
        b2.append(this.a);
        b2.append(", maxRate=");
        b2.append(this.b);
        b2.append(")");
        return b2.toString();
    }
}
