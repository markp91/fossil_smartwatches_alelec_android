package com.fossil;

import com.fossil.fn2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj2 extends fn2<lj2, a> implements to2 {
    @DexIgnore
    public static /* final */ lj2 zzf;
    @DexIgnore
    public static volatile yo2<lj2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public long zze;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<lj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(lj2.zzf);
        }

        @DexIgnore
        public final a a(int i) {
            f();
            ((lj2) this.b).b(i);
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((lj2) this.b).a(j);
            return this;
        }
    }

    /*
    static {
        lj2 lj2 = new lj2();
        zzf = lj2;
        fn2.a(lj2.class, lj2);
    }
    */

    @DexIgnore
    public static a r() {
        return (a) zzf.h();
    }

    @DexIgnore
    public final void a(long j) {
        this.zzc |= 2;
        this.zze = j;
    }

    @DexIgnore
    public final void b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final boolean p() {
        return (this.zzc & 2) != 0;
    }

    @DexIgnore
    public final long q() {
        return this.zze;
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new lj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0004\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                yo2<lj2> yo2 = zzg;
                if (yo2 == null) {
                    synchronized (lj2.class) {
                        yo2 = zzg;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzf);
                            zzg = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
