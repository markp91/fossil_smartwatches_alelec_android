package com.fossil;

import android.graphics.Bitmap;
import java.io.IOException;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jw implements zr<ByteBuffer, Bitmap> {
    @DexIgnore
    public /* final */ pw a;

    @DexIgnore
    public jw(pw pwVar) {
        this.a = pwVar;
    }

    @DexIgnore
    public boolean a(ByteBuffer byteBuffer, xr xrVar) {
        return this.a.a(byteBuffer);
    }

    @DexIgnore
    public rt<Bitmap> a(ByteBuffer byteBuffer, int i, int i2, xr xrVar) throws IOException {
        return this.a.a(h00.c(byteBuffer), i, i2, xrVar);
    }
}
