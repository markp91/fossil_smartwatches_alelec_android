package com.fossil;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class sl2<E> extends AbstractList<E> implements nn2<E> {
    @DexIgnore
    public boolean a; // = true;

    @DexIgnore
    public final void a() {
        if (!this.a) {
            throw new UnsupportedOperationException();
        }
    }

    @DexIgnore
    public boolean add(E e) {
        a();
        return super.add(e);
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        a();
        return super.addAll(collection);
    }

    @DexIgnore
    public void clear() {
        a();
        super.clear();
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (!(obj instanceof RandomAccess)) {
            return super.equals(obj);
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    @DexIgnore
    public final void k() {
        this.a = false;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        a();
        return super.remove(obj);
    }

    @DexIgnore
    public boolean removeAll(Collection<?> collection) {
        a();
        return super.removeAll(collection);
    }

    @DexIgnore
    public boolean retainAll(Collection<?> collection) {
        a();
        return super.retainAll(collection);
    }

    @DexIgnore
    public boolean zza() {
        return this.a;
    }

    @DexIgnore
    public boolean addAll(int i, Collection<? extends E> collection) {
        a();
        return super.addAll(i, collection);
    }
}
