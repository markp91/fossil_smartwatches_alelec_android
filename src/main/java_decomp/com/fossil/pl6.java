package com.fossil;

import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pl6 extends fm6 implements Runnable {
    @DexIgnore
    public static volatile Thread _thread;
    @DexIgnore
    public static volatile int debugStatus;
    @DexIgnore
    public static /* final */ long f;
    @DexIgnore
    public static /* final */ pl6 g;

    /*
    static {
        Long l;
        pl6 pl6 = new pl6();
        g = pl6;
        em6.b(pl6, false, 1, (Object) null);
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        try {
            l = Long.getLong("kotlinx.coroutines.DefaultExecutor.keepAlive", 1000);
        } catch (SecurityException unused) {
            l = 1000L;
        }
        wg6.a((Object) l, "try {\n            java.l\u2026AULT_KEEP_ALIVE\n        }");
        f = timeUnit.toNanos(l.longValue());
    }
    */

    @DexIgnore
    public Thread F() {
        Thread thread = _thread;
        return thread != null ? thread : N();
    }

    @DexIgnore
    public final synchronized void M() {
        if (O()) {
            debugStatus = 3;
            L();
            notifyAll();
        }
    }

    @DexIgnore
    public final synchronized Thread N() {
        Thread thread;
        thread = _thread;
        if (thread == null) {
            thread = new Thread(this, "kotlinx.coroutines.DefaultExecutor");
            _thread = thread;
            thread.setDaemon(true);
            thread.start();
        }
        return thread;
    }

    @DexIgnore
    public final boolean O() {
        int i = debugStatus;
        return i == 2 || i == 3;
    }

    @DexIgnore
    public final synchronized boolean P() {
        if (O()) {
            return false;
        }
        debugStatus = 1;
        notifyAll();
        return true;
    }

    @DexIgnore
    public am6 a(long j, Runnable runnable) {
        wg6.b(runnable, "block");
        return b(j, runnable);
    }

    @DexIgnore
    public void run() {
        on6.b.a(this);
        pn6 a = qn6.a();
        if (a != null) {
            a.b();
        }
        try {
            if (P()) {
                long j = Long.MAX_VALUE;
                while (true) {
                    Thread.interrupted();
                    long C = C();
                    if (C == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD) {
                        int i = (j > ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? 1 : (j == ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD ? 0 : -1));
                        if (i == 0) {
                            pn6 a2 = qn6.a();
                            long a3 = a2 != null ? a2.a() : System.nanoTime();
                            if (i == 0) {
                                j = f + a3;
                            }
                            long j2 = j - a3;
                            if (j2 <= 0) {
                                _thread = null;
                                M();
                                pn6 a4 = qn6.a();
                                if (a4 != null) {
                                    a4.d();
                                }
                                if (!J()) {
                                    F();
                                    return;
                                }
                                return;
                            }
                            C = ci6.b(C, j2);
                        } else {
                            C = ci6.b(C, f);
                        }
                    }
                    if (C > 0) {
                        if (O()) {
                            _thread = null;
                            M();
                            pn6 a5 = qn6.a();
                            if (a5 != null) {
                                a5.d();
                            }
                            if (!J()) {
                                F();
                                return;
                            }
                            return;
                        }
                        pn6 a6 = qn6.a();
                        if (a6 != null) {
                            a6.a(this, C);
                        } else {
                            LockSupport.parkNanos(this, C);
                        }
                    }
                }
            }
        } finally {
            _thread = null;
            M();
            pn6 a7 = qn6.a();
            if (a7 != null) {
                a7.d();
            }
            if (!J()) {
                F();
            }
        }
    }
}
