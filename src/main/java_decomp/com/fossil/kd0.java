package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kd0 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte[] a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ w40 c;
    @DexIgnore
    public /* final */ short d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<kd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                wg6.a(createByteArray, "parcel.createByteArray()!!");
                return new kd0(createByteArray);
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new kd0[i];
        }
    }

    @DexIgnore
    public kd0(byte[] bArr) throws IllegalArgumentException {
        this.a = bArr;
        if (this.a.length >= 22) {
            this.d = ByteBuffer.wrap(md6.a(bArr, 0, 2)).order(ByteOrder.LITTLE_ENDIAN).getShort(0);
            this.c = new w40(bArr[2], bArr[3]);
            this.b = new String(md6.a(bArr, 12, 17), mi0.A.f());
            return;
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("data.size("), this.a.length, ") is not equal or larger ", "than 22"));
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.FILE_HANDLE, (Object) cw0.a(this.d)), bm0.FILE_VERSION, (Object) this.c.toString()), bm0.LANGUAGE_CODE, (Object) this.b), bm0.FILE_CRC_C, (Object) Long.valueOf(h51.a.a(this.a, q11.CRC32C))), bm0.FILE_SIZE, (Object) Integer.valueOf(this.a.length));
    }

    @DexIgnore
    public final short b() {
        return this.d;
    }

    @DexIgnore
    public final w40 c() {
        return this.c;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public final byte[] getData() {
        return this.a;
    }

    @DexIgnore
    public final String getLocaleString() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeByteArray(this.a);
        }
    }
}
