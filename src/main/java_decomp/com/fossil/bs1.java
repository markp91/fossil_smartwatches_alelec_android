package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class bs1 implements rs1.b {
    @DexIgnore
    public /* final */ rs1 a;
    @DexIgnore
    public /* final */ rp1 b;

    @DexIgnore
    public bs1(rs1 rs1, rp1 rp1) {
        this.a = rs1;
        this.b = rp1;
    }

    @DexIgnore
    public static rs1.b a(rs1 rs1, rp1 rp1) {
        return new bs1(rs1, rp1);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return rs1.b(this.a, this.b, (SQLiteDatabase) obj);
    }
}
