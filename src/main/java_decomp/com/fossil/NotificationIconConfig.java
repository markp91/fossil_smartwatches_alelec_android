package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Map;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationIconConfig extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ NotificationIcon a;
    @DexIgnore
    public /* final */ Hashtable<c80, ld0> b; // = new Hashtable<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<wd0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(NotificationIcon.class.getClassLoader());
            if (readParcelable != null) {
                wg6.a(readParcelable, "parcel.readParcelable<No\u2026class.java.classLoader)!!");
                NotificationIconConfig notificationIconConfig = new NotificationIconConfig((NotificationIcon) readParcelable);
                Hashtable<c80, ld0> hashtable = notificationIconConfig.b;
                Serializable readSerializable = parcel.readSerializable();
                if (readSerializable != null) {
                    hashtable.putAll((Hashtable) readSerializable);
                    return notificationIconConfig;
                }
                throw new rc6("null cannot be cast to non-null type java.util.Hashtable<com.fossil.blesdk.device.data.notification.NotificationType, com.fossil.blesdk.model.file.NotificationIcon>");
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new NotificationIconConfig[i];
        }
    }

    @DexIgnore
    public NotificationIconConfig(NotificationIcon notificationIcon) {
        this.a = notificationIcon;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(new JSONObject(), bm0.DEFAULT_ICON, (Object) this.a.a());
        for (Map.Entry next : this.b.entrySet()) {
            Object key = next.getKey();
            wg6.a(key, "icon.key");
            a2.put(cw0.a((Enum<?>) (Enum) key), ((NotificationIcon) next.getValue()).a());
        }
        return a2;
    }

    @DexIgnore
    public final NotificationIcon[] b() {
        Collection<ld0> values = this.b.values();
        wg6.a(values, "typeIcons.values");
        Object[] array = yd6.c(yd6.a(values, this.a)).toArray(new NotificationIcon[0]);
        if (array != null) {
            return (NotificationIcon[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final byte[] c() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        int i = 0;
        for (NotificationType notificationType : NotificationType.values()) {
            NotificationIcon notificationIcon = this.b.get(notificationType);
            int ordinal = 1 << notificationType.ordinal();
            if (notificationIcon == null || notificationIcon.e()) {
                i |= ordinal;
            } else {
                String fileName = notificationIcon.getFileName();
                Charset f = mi0.A.f();
                if (fileName != null) {
                    byte[] bytes = fileName.getBytes(f);
                    wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
                    byte[] a2 = md6.a(bytes, (byte) 0);
                    ByteBuffer order = ByteBuffer.allocate(a2.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                    wg6.a(order, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                    order.putShort((short) ordinal).put((byte) a2.length).put(a2);
                    byteArrayOutputStream.write(order.array());
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
        }
        if (i != 0) {
            String fileName2 = this.a.getFileName();
            Charset f2 = mi0.A.f();
            if (fileName2 != null) {
                byte[] bytes2 = fileName2.getBytes(f2);
                wg6.a(bytes2, "(this as java.lang.String).getBytes(charset)");
                byte[] a3 = md6.a(bytes2, (byte) 0);
                ByteBuffer order2 = ByteBuffer.allocate(a3.length + 3).order(ByteOrder.LITTLE_ENDIAN);
                wg6.a(order2, "ByteBuffer.allocate(TYPE\u2026(ByteOrder.LITTLE_ENDIAN)");
                order2.putShort((short) i).put((byte) a3.length).put(a3);
                byteArrayOutputStream.write(order2.array());
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "byteArrayOutputStream.toByteArray()");
        return byteArray;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(NotificationIconConfig.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIconConfig notificationIconConfig = (NotificationIconConfig) obj;
            if (!wg6.a(this.a, notificationIconConfig.a) || !wg6.a(this.b, notificationIconConfig.b)) {
                return false;
            }
            return true;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.notification.filter.NotificationIconConfig");
    }

    @DexIgnore
    public final NotificationIcon getDefaultIcon() {
        return this.a;
    }

    @DexIgnore
    public final NotificationIcon getIconForType(NotificationType notificationType) {
        return this.b.get(notificationType);
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (this.a.hashCode() * 31);
    }

    @DexIgnore
    public final NotificationIconConfig setIconForType(NotificationType notificationType, NotificationIcon notificationIcon) {
        if (notificationIcon != null) {
            this.b.put(notificationType, notificationIcon);
        } else {
            this.b.remove(notificationType);
        }
        return this;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeParcelable(this.a, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.b);
        }
    }
}
