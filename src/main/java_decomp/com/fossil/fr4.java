package com.fossil;

import com.portfolio.platform.data.model.Explore;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fr4 extends er4 {
    @DexIgnore
    public /* final */ List<String> d;
    @DexIgnore
    public String e;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public fr4(String str, String str2, List<String> list, String str3) {
        super(str, str2);
        wg6.b(str, "tagName");
        wg6.b(str2, Explore.COLUMN_TITLE);
        wg6.b(list, "values");
        wg6.b(str3, "btnText");
        this.d = list;
        this.e = str3;
    }

    @DexIgnore
    public final String d() {
        return this.e;
    }

    @DexIgnore
    public final List<String> e() {
        return this.d;
    }
}
