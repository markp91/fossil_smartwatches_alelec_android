package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import androidx.fragment.app.Fragment;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ot1 {
    @DexIgnore
    public static qt1 a(Context context, GoogleSignInOptions googleSignInOptions) {
        w12.a(googleSignInOptions);
        return new qt1(context, googleSignInOptions);
    }

    @DexIgnore
    public static GoogleSignInAccount a(Context context) {
        return iu1.a(context).b();
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, rt1 rt1) {
        w12.a(rt1, (Object) "Please provide a non-null GoogleSignInOptionsExtension");
        return a(googleSignInAccount, a(rt1.a()));
    }

    @DexIgnore
    public static boolean a(GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        if (googleSignInAccount == null) {
            return false;
        }
        HashSet hashSet = new HashSet();
        Collections.addAll(hashSet, scopeArr);
        return googleSignInAccount.F().containsAll(hashSet);
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, rt1 rt1) {
        w12.a(fragment, (Object) "Please provide a non-null Fragment");
        w12.a(rt1, (Object) "Please provide a non-null GoogleSignInOptionsExtension");
        a(fragment, i, googleSignInAccount, a(rt1.a()));
    }

    @DexIgnore
    public static void a(Fragment fragment, int i, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        w12.a(fragment, (Object) "Please provide a non-null Fragment");
        w12.a(scopeArr, (Object) "Please provide at least one scope");
        fragment.startActivityForResult(a(fragment.getActivity(), googleSignInAccount, scopeArr), i);
    }

    @DexIgnore
    public static Intent a(Activity activity, GoogleSignInAccount googleSignInAccount, Scope... scopeArr) {
        GoogleSignInOptions.a aVar = new GoogleSignInOptions.a();
        if (scopeArr.length > 0) {
            aVar.a(scopeArr[0], scopeArr);
        }
        if (googleSignInAccount != null && !TextUtils.isEmpty(googleSignInAccount.C())) {
            aVar.c(googleSignInAccount.C());
        }
        return new qt1(activity, aVar.a()).i();
    }

    @DexIgnore
    public static Scope[] a(List<Scope> list) {
        return list == null ? new Scope[0] : (Scope[]) list.toArray(new Scope[list.size()]);
    }
}
