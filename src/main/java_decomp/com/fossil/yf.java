package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class yf extends pg {
    @DexIgnore
    public static /* final */ boolean DEBUG; // = false;
    @DexIgnore
    public static TimeInterpolator sDefaultInterpolator;
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mAddAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<RecyclerView.ViewHolder>> mAdditionsList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mChangeAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<i>> mChangesList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mMoveAnimations; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<ArrayList<j>> mMovesList; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mPendingAdditions; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<i> mPendingChanges; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<j> mPendingMoves; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mPendingRemovals; // = new ArrayList<>();
    @DexIgnore
    public ArrayList<RecyclerView.ViewHolder> mRemoveAnimations; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList a;

        @DexIgnore
        public a(ArrayList arrayList) {
            this.a = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                j jVar = (j) it.next();
                yf.this.animateMoveImpl(jVar.a, jVar.b, jVar.c, jVar.d, jVar.e);
            }
            this.a.clear();
            yf.this.mMovesList.remove(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList a;

        @DexIgnore
        public b(ArrayList arrayList) {
            this.a = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                yf.this.animateChangeImpl((i) it.next());
            }
            this.a.clear();
            yf.this.mChangesList.remove(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList a;

        @DexIgnore
        public c(ArrayList arrayList) {
            this.a = arrayList;
        }

        @DexIgnore
        public void run() {
            Iterator it = this.a.iterator();
            while (it.hasNext()) {
                yf.this.animateAddImpl((RecyclerView.ViewHolder) it.next());
            }
            this.a.clear();
            yf.this.mAdditionsList.remove(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public d(RecyclerView.ViewHolder viewHolder, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = viewHolder;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener((Animator.AnimatorListener) null);
            this.c.setAlpha(1.0f);
            yf.this.dispatchRemoveFinished(this.a);
            yf.this.mRemoveAnimations.remove(this.a);
            yf.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yf.this.dispatchRemoveStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ View b;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator c;

        @DexIgnore
        public e(RecyclerView.ViewHolder viewHolder, View view, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = viewHolder;
            this.b = view;
            this.c = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            this.b.setAlpha(1.0f);
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.c.setListener((Animator.AnimatorListener) null);
            yf.this.dispatchAddFinished(this.a);
            yf.this.mAddAnimations.remove(this.a);
            yf.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yf.this.dispatchAddStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ RecyclerView.ViewHolder a;
        @DexIgnore
        public /* final */ /* synthetic */ int b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator e;

        @DexIgnore
        public f(RecyclerView.ViewHolder viewHolder, int i, View view, int i2, ViewPropertyAnimator viewPropertyAnimator) {
            this.a = viewHolder;
            this.b = i;
            this.c = view;
            this.d = i2;
            this.e = viewPropertyAnimator;
        }

        @DexIgnore
        public void onAnimationCancel(Animator animator) {
            if (this.b != 0) {
                this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            if (this.d != 0) {
                this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.e.setListener((Animator.AnimatorListener) null);
            yf.this.dispatchMoveFinished(this.a);
            yf.this.mMoveAnimations.remove(this.a);
            yf.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yf.this.dispatchMoveStarting(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ i a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public g(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = iVar;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener((Animator.AnimatorListener) null);
            this.c.setAlpha(1.0f);
            this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            yf.this.dispatchChangeFinished(this.a.a, true);
            yf.this.mChangeAnimations.remove(this.a.a);
            yf.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yf.this.dispatchChangeStarting(this.a.a, true);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ i a;
        @DexIgnore
        public /* final */ /* synthetic */ ViewPropertyAnimator b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;

        @DexIgnore
        public h(i iVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
            this.a = iVar;
            this.b = viewPropertyAnimator;
            this.c = view;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            this.b.setListener((Animator.AnimatorListener) null);
            this.c.setAlpha(1.0f);
            this.c.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            this.c.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            yf.this.dispatchChangeFinished(this.a.b, false);
            yf.this.mChangeAnimations.remove(this.a.b);
            yf.this.dispatchFinishedWhenDone();
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            yf.this.dispatchChangeStarting(this.a.b, false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class j {
        @DexIgnore
        public RecyclerView.ViewHolder a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;

        @DexIgnore
        public j(RecyclerView.ViewHolder viewHolder, int i, int i2, int i3, int i4) {
            this.a = viewHolder;
            this.b = i;
            this.c = i2;
            this.d = i3;
            this.e = i4;
        }
    }

    @DexIgnore
    private void animateRemoveImpl(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mRemoveAnimations.add(viewHolder);
        animate.setDuration(getRemoveDuration()).alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new d(viewHolder, animate, view)).start();
    }

    @DexIgnore
    private void endChangeAnimation(List<i> list, RecyclerView.ViewHolder viewHolder) {
        for (int size = list.size() - 1; size >= 0; size--) {
            i iVar = list.get(size);
            if (endChangeAnimationIfNecessary(iVar, viewHolder) && iVar.a == null && iVar.b == null) {
                list.remove(iVar);
            }
        }
    }

    @DexIgnore
    private void endChangeAnimationIfNecessary(i iVar) {
        RecyclerView.ViewHolder viewHolder = iVar.a;
        if (viewHolder != null) {
            endChangeAnimationIfNecessary(iVar, viewHolder);
        }
        RecyclerView.ViewHolder viewHolder2 = iVar.b;
        if (viewHolder2 != null) {
            endChangeAnimationIfNecessary(iVar, viewHolder2);
        }
    }

    @DexIgnore
    private void resetAnimation(RecyclerView.ViewHolder viewHolder) {
        if (sDefaultInterpolator == null) {
            sDefaultInterpolator = new ValueAnimator().getInterpolator();
        }
        viewHolder.itemView.animate().setInterpolator(sDefaultInterpolator);
        endAnimation(viewHolder);
    }

    @DexIgnore
    public boolean animateAdd(RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        viewHolder.itemView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        this.mPendingAdditions.add(viewHolder);
        return true;
    }

    @DexIgnore
    public void animateAddImpl(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        ViewPropertyAnimator animate = view.animate();
        this.mAddAnimations.add(viewHolder);
        animate.alpha(1.0f).setDuration(getAddDuration()).setListener(new e(viewHolder, view, animate)).start();
    }

    @DexIgnore
    public boolean animateChange(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i2, int i3, int i4, int i5) {
        if (viewHolder == viewHolder2) {
            return animateMove(viewHolder, i2, i3, i4, i5);
        }
        float translationX = viewHolder.itemView.getTranslationX();
        float translationY = viewHolder.itemView.getTranslationY();
        float alpha = viewHolder.itemView.getAlpha();
        resetAnimation(viewHolder);
        int i6 = (int) (((float) (i4 - i2)) - translationX);
        int i7 = (int) (((float) (i5 - i3)) - translationY);
        viewHolder.itemView.setTranslationX(translationX);
        viewHolder.itemView.setTranslationY(translationY);
        viewHolder.itemView.setAlpha(alpha);
        if (viewHolder2 != null) {
            resetAnimation(viewHolder2);
            viewHolder2.itemView.setTranslationX((float) (-i6));
            viewHolder2.itemView.setTranslationY((float) (-i7));
            viewHolder2.itemView.setAlpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        this.mPendingChanges.add(new i(viewHolder, viewHolder2, i2, i3, i4, i5));
        return true;
    }

    @DexIgnore
    public void animateChangeImpl(i iVar) {
        View view;
        RecyclerView.ViewHolder viewHolder = iVar.a;
        View view2 = null;
        if (viewHolder == null) {
            view = null;
        } else {
            view = viewHolder.itemView;
        }
        RecyclerView.ViewHolder viewHolder2 = iVar.b;
        if (viewHolder2 != null) {
            view2 = viewHolder2.itemView;
        }
        if (view != null) {
            ViewPropertyAnimator duration = view.animate().setDuration(getChangeDuration());
            this.mChangeAnimations.add(iVar.a);
            duration.translationX((float) (iVar.e - iVar.c));
            duration.translationY((float) (iVar.f - iVar.d));
            duration.alpha(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setListener(new g(iVar, duration, view)).start();
        }
        if (view2 != null) {
            ViewPropertyAnimator animate = view2.animate();
            this.mChangeAnimations.add(iVar.b);
            animate.translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES).setDuration(getChangeDuration()).alpha(1.0f).setListener(new h(iVar, animate, view2)).start();
        }
    }

    @DexIgnore
    public boolean animateMove(RecyclerView.ViewHolder viewHolder, int i2, int i3, int i4, int i5) {
        View view = viewHolder.itemView;
        int translationX = i2 + ((int) view.getTranslationX());
        int translationY = i3 + ((int) viewHolder.itemView.getTranslationY());
        resetAnimation(viewHolder);
        int i6 = i4 - translationX;
        int i7 = i5 - translationY;
        if (i6 == 0 && i7 == 0) {
            dispatchMoveFinished(viewHolder);
            return false;
        }
        if (i6 != 0) {
            view.setTranslationX((float) (-i6));
        }
        if (i7 != 0) {
            view.setTranslationY((float) (-i7));
        }
        this.mPendingMoves.add(new j(viewHolder, translationX, translationY, i4, i5));
        return true;
    }

    @DexIgnore
    public void animateMoveImpl(RecyclerView.ViewHolder viewHolder, int i2, int i3, int i4, int i5) {
        View view = viewHolder.itemView;
        int i6 = i4 - i2;
        int i7 = i5 - i3;
        if (i6 != 0) {
            view.animate().translationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        if (i7 != 0) {
            view.animate().translationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        }
        ViewPropertyAnimator animate = view.animate();
        this.mMoveAnimations.add(viewHolder);
        animate.setDuration(getMoveDuration()).setListener(new f(viewHolder, i6, view, i7, animate)).start();
    }

    @DexIgnore
    public boolean animateRemove(RecyclerView.ViewHolder viewHolder) {
        resetAnimation(viewHolder);
        this.mPendingRemovals.add(viewHolder);
        return true;
    }

    @DexIgnore
    public boolean canReuseUpdatedViewHolder(RecyclerView.ViewHolder viewHolder, List<Object> list) {
        return !list.isEmpty() || super.canReuseUpdatedViewHolder(viewHolder, list);
    }

    @DexIgnore
    public void cancelAll(List<RecyclerView.ViewHolder> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            list.get(size).itemView.animate().cancel();
        }
    }

    @DexIgnore
    public void dispatchFinishedWhenDone() {
        if (!isRunning()) {
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    public void endAnimation(RecyclerView.ViewHolder viewHolder) {
        View view = viewHolder.itemView;
        view.animate().cancel();
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            } else if (this.mPendingMoves.get(size).a == viewHolder) {
                view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                dispatchMoveFinished(viewHolder);
                this.mPendingMoves.remove(size);
            }
        }
        endChangeAnimation(this.mPendingChanges, viewHolder);
        if (this.mPendingRemovals.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchRemoveFinished(viewHolder);
        }
        if (this.mPendingAdditions.remove(viewHolder)) {
            view.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
        }
        for (int size2 = this.mChangesList.size() - 1; size2 >= 0; size2--) {
            ArrayList arrayList = this.mChangesList.get(size2);
            endChangeAnimation(arrayList, viewHolder);
            if (arrayList.isEmpty()) {
                this.mChangesList.remove(size2);
            }
        }
        for (int size3 = this.mMovesList.size() - 1; size3 >= 0; size3--) {
            ArrayList arrayList2 = this.mMovesList.get(size3);
            int size4 = arrayList2.size() - 1;
            while (true) {
                if (size4 < 0) {
                    break;
                } else if (((j) arrayList2.get(size4)).a == viewHolder) {
                    view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(viewHolder);
                    arrayList2.remove(size4);
                    if (arrayList2.isEmpty()) {
                        this.mMovesList.remove(size3);
                    }
                } else {
                    size4--;
                }
            }
        }
        for (int size5 = this.mAdditionsList.size() - 1; size5 >= 0; size5--) {
            ArrayList arrayList3 = this.mAdditionsList.get(size5);
            if (arrayList3.remove(viewHolder)) {
                view.setAlpha(1.0f);
                dispatchAddFinished(viewHolder);
                if (arrayList3.isEmpty()) {
                    this.mAdditionsList.remove(size5);
                }
            }
        }
        this.mRemoveAnimations.remove(viewHolder);
        this.mAddAnimations.remove(viewHolder);
        this.mChangeAnimations.remove(viewHolder);
        this.mMoveAnimations.remove(viewHolder);
        dispatchFinishedWhenDone();
    }

    @DexIgnore
    public void endAnimations() {
        int size = this.mPendingMoves.size();
        while (true) {
            size--;
            if (size < 0) {
                break;
            }
            j jVar = this.mPendingMoves.get(size);
            View view = jVar.a.itemView;
            view.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            view.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            dispatchMoveFinished(jVar.a);
            this.mPendingMoves.remove(size);
        }
        for (int size2 = this.mPendingRemovals.size() - 1; size2 >= 0; size2--) {
            dispatchRemoveFinished(this.mPendingRemovals.get(size2));
            this.mPendingRemovals.remove(size2);
        }
        int size3 = this.mPendingAdditions.size();
        while (true) {
            size3--;
            if (size3 < 0) {
                break;
            }
            RecyclerView.ViewHolder viewHolder = this.mPendingAdditions.get(size3);
            viewHolder.itemView.setAlpha(1.0f);
            dispatchAddFinished(viewHolder);
            this.mPendingAdditions.remove(size3);
        }
        for (int size4 = this.mPendingChanges.size() - 1; size4 >= 0; size4--) {
            endChangeAnimationIfNecessary(this.mPendingChanges.get(size4));
        }
        this.mPendingChanges.clear();
        if (isRunning()) {
            for (int size5 = this.mMovesList.size() - 1; size5 >= 0; size5--) {
                ArrayList arrayList = this.mMovesList.get(size5);
                for (int size6 = arrayList.size() - 1; size6 >= 0; size6--) {
                    j jVar2 = (j) arrayList.get(size6);
                    View view2 = jVar2.a.itemView;
                    view2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    view2.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    dispatchMoveFinished(jVar2.a);
                    arrayList.remove(size6);
                    if (arrayList.isEmpty()) {
                        this.mMovesList.remove(arrayList);
                    }
                }
            }
            for (int size7 = this.mAdditionsList.size() - 1; size7 >= 0; size7--) {
                ArrayList arrayList2 = this.mAdditionsList.get(size7);
                for (int size8 = arrayList2.size() - 1; size8 >= 0; size8--) {
                    RecyclerView.ViewHolder viewHolder2 = (RecyclerView.ViewHolder) arrayList2.get(size8);
                    viewHolder2.itemView.setAlpha(1.0f);
                    dispatchAddFinished(viewHolder2);
                    arrayList2.remove(size8);
                    if (arrayList2.isEmpty()) {
                        this.mAdditionsList.remove(arrayList2);
                    }
                }
            }
            for (int size9 = this.mChangesList.size() - 1; size9 >= 0; size9--) {
                ArrayList arrayList3 = this.mChangesList.get(size9);
                for (int size10 = arrayList3.size() - 1; size10 >= 0; size10--) {
                    endChangeAnimationIfNecessary((i) arrayList3.get(size10));
                    if (arrayList3.isEmpty()) {
                        this.mChangesList.remove(arrayList3);
                    }
                }
            }
            cancelAll(this.mRemoveAnimations);
            cancelAll(this.mMoveAnimations);
            cancelAll(this.mAddAnimations);
            cancelAll(this.mChangeAnimations);
            dispatchAnimationsFinished();
        }
    }

    @DexIgnore
    public boolean isRunning() {
        return !this.mPendingAdditions.isEmpty() || !this.mPendingChanges.isEmpty() || !this.mPendingMoves.isEmpty() || !this.mPendingRemovals.isEmpty() || !this.mMoveAnimations.isEmpty() || !this.mRemoveAnimations.isEmpty() || !this.mAddAnimations.isEmpty() || !this.mChangeAnimations.isEmpty() || !this.mMovesList.isEmpty() || !this.mAdditionsList.isEmpty() || !this.mChangesList.isEmpty();
    }

    @DexIgnore
    public void runPendingAnimations() {
        boolean z = !this.mPendingRemovals.isEmpty();
        boolean z2 = !this.mPendingMoves.isEmpty();
        boolean z3 = !this.mPendingChanges.isEmpty();
        boolean z4 = !this.mPendingAdditions.isEmpty();
        if (z || z2 || z4 || z3) {
            Iterator<RecyclerView.ViewHolder> it = this.mPendingRemovals.iterator();
            while (it.hasNext()) {
                animateRemoveImpl(it.next());
            }
            this.mPendingRemovals.clear();
            if (z2) {
                ArrayList arrayList = new ArrayList();
                arrayList.addAll(this.mPendingMoves);
                this.mMovesList.add(arrayList);
                this.mPendingMoves.clear();
                a aVar = new a(arrayList);
                if (z) {
                    x9.a(((j) arrayList.get(0)).a.itemView, (Runnable) aVar, getRemoveDuration());
                } else {
                    aVar.run();
                }
            }
            if (z3) {
                ArrayList arrayList2 = new ArrayList();
                arrayList2.addAll(this.mPendingChanges);
                this.mChangesList.add(arrayList2);
                this.mPendingChanges.clear();
                b bVar = new b(arrayList2);
                if (z) {
                    x9.a(((i) arrayList2.get(0)).a.itemView, (Runnable) bVar, getRemoveDuration());
                } else {
                    bVar.run();
                }
            }
            if (z4) {
                ArrayList arrayList3 = new ArrayList();
                arrayList3.addAll(this.mPendingAdditions);
                this.mAdditionsList.add(arrayList3);
                this.mPendingAdditions.clear();
                c cVar = new c(arrayList3);
                if (z || z2 || z3) {
                    long j2 = 0;
                    long removeDuration = z ? getRemoveDuration() : 0;
                    long moveDuration = z2 ? getMoveDuration() : 0;
                    if (z3) {
                        j2 = getChangeDuration();
                    }
                    x9.a(((RecyclerView.ViewHolder) arrayList3.get(0)).itemView, (Runnable) cVar, removeDuration + Math.max(moveDuration, j2));
                    return;
                }
                cVar.run();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class i {
        @DexIgnore
        public RecyclerView.ViewHolder a;
        @DexIgnore
        public RecyclerView.ViewHolder b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public int e;
        @DexIgnore
        public int f;

        @DexIgnore
        public i(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2) {
            this.a = viewHolder;
            this.b = viewHolder2;
        }

        @DexIgnore
        public String toString() {
            return "ChangeInfo{oldHolder=" + this.a + ", newHolder=" + this.b + ", fromX=" + this.c + ", fromY=" + this.d + ", toX=" + this.e + ", toY=" + this.f + '}';
        }

        @DexIgnore
        public i(RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder viewHolder2, int i, int i2, int i3, int i4) {
            this(viewHolder, viewHolder2);
            this.c = i;
            this.d = i2;
            this.e = i3;
            this.f = i4;
        }
    }

    @DexIgnore
    private boolean endChangeAnimationIfNecessary(i iVar, RecyclerView.ViewHolder viewHolder) {
        boolean z = false;
        if (iVar.b == viewHolder) {
            iVar.b = null;
        } else if (iVar.a != viewHolder) {
            return false;
        } else {
            iVar.a = null;
            z = true;
        }
        viewHolder.itemView.setAlpha(1.0f);
        viewHolder.itemView.setTranslationX(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        viewHolder.itemView.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        dispatchChangeFinished(viewHolder, z);
        return true;
    }
}
