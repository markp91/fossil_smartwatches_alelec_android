package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lh1 extends j61 {
    @DexIgnore
    public String A; // = "";

    @DexIgnore
    public lh1(ue1 ue1) {
        super(lx0.READ_FIRMWARE_VERSION, ue1);
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.A = new String(((w11) ok0).l, ej6.a);
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.SERIAL_NUMBER, (Object) this.A), 7));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.SERIAL_NUMBER, (Object) this.A);
    }

    @DexIgnore
    public ok0 l() {
        return new w11(rg1.SERIAL_NUMBER, this.y.v);
    }
}
