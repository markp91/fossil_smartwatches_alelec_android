package com.fossil;

import com.facebook.UserSettingsManager;
import com.fossil.sr1;
import com.fossil.wearables.fsl.fitness.FitnessProviderImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vr1 {
    @DexIgnore
    public static /* final */ vr1 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(int i);

        @DexIgnore
        public abstract a a(long j);

        @DexIgnore
        public abstract vr1 a();

        @DexIgnore
        public abstract a b(int i);

        @DexIgnore
        public abstract a b(long j);
    }

    /*
    static {
        a e = e();
        e.b(10485760);
        e.b(200);
        e.a((int) FitnessProviderImpl.DEFAULT_DAILY_STEP_GOAL);
        e.a((long) UserSettingsManager.TIMEOUT_7D);
        a = e.a();
    }
    */

    @DexIgnore
    public static a e() {
        return new sr1.b();
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public abstract long b();

    @DexIgnore
    public abstract int c();

    @DexIgnore
    public abstract long d();
}
