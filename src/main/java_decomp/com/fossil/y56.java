package com.fossil;

import android.content.Context;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.data.model.PinObject;
import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class y56 {
    @DexIgnore
    public static b56 e; // = m56.b();
    @DexIgnore
    public static y56 f; // = null;
    @DexIgnore
    public static Context g; // = null;
    @DexIgnore
    public DefaultHttpClient a; // = null;
    @DexIgnore
    public g56 b; // = null;
    @DexIgnore
    public StringBuilder c; // = new StringBuilder(4096);
    @DexIgnore
    public long d; // = 0;

    @DexIgnore
    /* JADX WARNING: Can't wrap try/catch for region: R(7:0|1|2|(3:4|5|6)|7|8|9) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0066 */
    public y56(Context context) {
        try {
            g = context.getApplicationContext();
            this.d = System.currentTimeMillis() / 1000;
            this.b = new g56();
            if (n36.q()) {
                Logger.getLogger("org.apache.http.wire").setLevel(Level.FINER);
                Logger.getLogger("org.apache.http.headers").setLevel(Level.FINER);
                System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
                System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
                System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", Constants.DEBUG);
                System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", Constants.DEBUG);
                System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", Constants.DEBUG);
            }
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            this.a = new DefaultHttpClient(basicHttpParams);
            this.a.setKeepAliveStrategy(new z56(this));
        } catch (Throwable th) {
            e.a(th);
        }
    }

    @DexIgnore
    public static Context a() {
        return g;
    }

    @DexIgnore
    public static void a(Context context) {
        g = context.getApplicationContext();
    }

    @DexIgnore
    public static y56 b(Context context) {
        if (f == null) {
            synchronized (y56.class) {
                if (f == null) {
                    f = new y56(context);
                }
            }
        }
        return f;
    }

    @DexIgnore
    public void a(v36 v36, x56 x56) {
        b(Arrays.asList(new String[]{v36.f()}), x56);
    }

    @DexIgnore
    public void a(List<?> list, x56 x56) {
        Throwable th;
        List<?> list2 = list;
        if (list2 != null && !list.isEmpty()) {
            int size = list.size();
            list2.get(0);
            try {
                this.c.delete(0, this.c.length());
                this.c.append("[");
                for (int i = 0; i < size; i++) {
                    this.c.append(list2.get(i).toString());
                    if (i != size - 1) {
                        this.c.append(",");
                    }
                }
                this.c.append("]");
                String sb = this.c.toString();
                int length = sb.length();
                String str = n36.n() + "/?index=" + this.d;
                this.d++;
                if (n36.q()) {
                    e.e("[" + str + "]Send request(" + length + "bytes), content:" + sb);
                }
                HttpPost httpPost = new HttpPost(str);
                httpPost.addHeader("Accept-Encoding", "gzip");
                httpPost.setHeader("Connection", "Keep-Alive");
                httpPost.removeHeaders(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER);
                HttpHost a2 = b46.a(g).a();
                httpPost.addHeader("Content-Encoding", "rc4");
                if (a2 == null) {
                    this.a.getParams().removeParameter("http.route.default-proxy");
                } else {
                    if (n36.q()) {
                        e.a((Object) "proxy:" + a2.toHostString());
                    }
                    httpPost.addHeader("X-Content-Encoding", "rc4");
                    this.a.getParams().setParameter("http.route.default-proxy", a2);
                    httpPost.addHeader("X-Online-Host", n36.B);
                    httpPost.addHeader(com.zendesk.sdk.network.Constants.ACCEPT_HEADER, "*/*");
                    httpPost.addHeader("Content-Type", PinObject.COLUMN_JSON_DATA);
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
                byte[] bytes = sb.getBytes("UTF-8");
                int length2 = bytes.length;
                if (length > n36.L) {
                    httpPost.removeHeaders("Content-Encoding");
                    String str2 = "rc4" + ",gzip";
                    httpPost.addHeader("Content-Encoding", str2);
                    if (a2 != null) {
                        httpPost.removeHeaders("X-Content-Encoding");
                        httpPost.addHeader("X-Content-Encoding", str2);
                    }
                    byteArrayOutputStream.write(new byte[4]);
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                    gZIPOutputStream.write(bytes);
                    gZIPOutputStream.close();
                    bytes = byteArrayOutputStream.toByteArray();
                    ByteBuffer.wrap(bytes, 0, 4).putInt(length2);
                    if (n36.q()) {
                        e.a((Object) "before Gzip:" + length2 + " bytes, after Gzip:" + bytes.length + " bytes");
                    }
                }
                httpPost.setEntity(new ByteArrayEntity(h56.a(bytes)));
                HttpResponse execute = this.a.execute(httpPost);
                HttpEntity entity = execute.getEntity();
                int statusCode = execute.getStatusLine().getStatusCode();
                long contentLength = entity.getContentLength();
                if (n36.q()) {
                    e.e("http recv response status code:" + statusCode + ", content length:" + contentLength);
                }
                int i2 = (contentLength > 0 ? 1 : (contentLength == 0 ? 0 : -1));
                if (i2 <= 0) {
                    e.c("Server response no data.");
                    if (x56 != null) {
                        x56.b();
                    }
                    EntityUtils.toString(entity);
                    return;
                }
                if (i2 > 0) {
                    InputStream content = entity.getContent();
                    DataInputStream dataInputStream = new DataInputStream(content);
                    byte[] bArr = new byte[((int) entity.getContentLength())];
                    dataInputStream.readFully(bArr);
                    content.close();
                    dataInputStream.close();
                    Header firstHeader = execute.getFirstHeader("Content-Encoding");
                    if (firstHeader != null) {
                        if (firstHeader.getValue().equalsIgnoreCase("gzip,rc4")) {
                            bArr = h56.b(m56.a(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4,gzip")) {
                            bArr = m56.a(h56.b(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase("gzip")) {
                            bArr = m56.a(bArr);
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4")) {
                            bArr = h56.b(bArr);
                        }
                    }
                    String str3 = new String(bArr, "UTF-8");
                    if (n36.q()) {
                        e.e("http get response data:" + str3);
                    }
                    JSONObject jSONObject = new JSONObject(str3);
                    if (statusCode == 200) {
                        a(jSONObject);
                        if (x56 != null) {
                            if (jSONObject.optInt("ret") == 0) {
                                x56.a();
                            } else {
                                e.d("response error data.");
                            }
                        }
                        content.close();
                    } else {
                        e.d("Server response error code:" + statusCode + ", error:" + new String(bArr, "UTF-8"));
                        if (x56 != null) {
                        }
                        content.close();
                    }
                    x56.b();
                    content.close();
                } else {
                    EntityUtils.toString(entity);
                }
                byteArrayOutputStream.close();
                th = null;
                if (th != null) {
                    e.b(th);
                    if (x56 != null) {
                        try {
                            x56.b();
                        } catch (Throwable th2) {
                            e.a(th2);
                        }
                    }
                    if (th instanceof OutOfMemoryError) {
                        System.gc();
                        this.c = null;
                        this.c = new StringBuilder(2048);
                    }
                    b46.a(g).d();
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
    }

    @DexIgnore
    public final void a(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("mid");
            if (b26.b(optString)) {
                if (n36.q()) {
                    b56 b56 = e;
                    b56.e("update mid:" + optString);
                }
                a26.a(g).a(optString);
            }
            if (!jSONObject.isNull("cfg")) {
                n36.a(g, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (n36.q()) {
                    b56 b562 = e;
                    b562.e("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                m56.C(g);
                m56.a(g, currentTimeMillis);
            }
        } catch (Throwable th) {
            e.g(th);
        }
    }

    @DexIgnore
    public void b(List<?> list, x56 x56) {
        g56 g56 = this.b;
        if (g56 != null) {
            g56.a(new a66(this, list, x56));
        }
    }
}
