package com.fossil;

import java.util.Random;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t10 implements da6 {
    @DexIgnore
    public /* final */ da6 a;
    @DexIgnore
    public /* final */ Random b;
    @DexIgnore
    public /* final */ double c;

    @DexIgnore
    public t10(da6 da6, double d) {
        this(da6, d, new Random());
    }

    @DexIgnore
    public long a(int i) {
        return (long) (a() * ((double) this.a.a(i)));
    }

    @DexIgnore
    public t10(da6 da6, double d, Random random) {
        if (d < 0.0d || d > 1.0d) {
            throw new IllegalArgumentException("jitterPercent must be between 0.0 and 1.0");
        } else if (da6 == null) {
            throw new NullPointerException("backoff must not be null");
        } else if (random != null) {
            this.a = da6;
            this.c = d;
            this.b = random;
        } else {
            throw new NullPointerException("random must not be null");
        }
    }

    @DexIgnore
    public double a() {
        double d = this.c;
        double d2 = 1.0d - d;
        return d2 + (((d + 1.0d) - d2) * this.b.nextDouble());
    }
}
