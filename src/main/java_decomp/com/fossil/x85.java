package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface x85 extends xt4<w85> {
    @DexIgnore
    void a(boolean z);

    @DexIgnore
    void d(int i);

    @DexIgnore
    void e(int i);

    @DexIgnore
    void f(int i);

    @DexIgnore
    int getItemCount();

    @DexIgnore
    void i(List<n35> list);

    @DexIgnore
    void j();

    @DexIgnore
    void j(boolean z);

    @DexIgnore
    void m();

    @DexIgnore
    void n();

    @DexIgnore
    void t(String str);

    @DexIgnore
    void v();
}
