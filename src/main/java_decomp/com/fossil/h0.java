package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h0 {
    @DexIgnore
    public static /* final */ int abc_action_bar_home_description; // = 2131886981;
    @DexIgnore
    public static /* final */ int abc_action_bar_up_description; // = 2131886982;
    @DexIgnore
    public static /* final */ int abc_action_menu_overflow_description; // = 2131886983;
    @DexIgnore
    public static /* final */ int abc_action_mode_done; // = 2131886984;
    @DexIgnore
    public static /* final */ int abc_activity_chooser_view_see_all; // = 2131886985;
    @DexIgnore
    public static /* final */ int abc_activitychooserview_choose_application; // = 2131886986;
    @DexIgnore
    public static /* final */ int abc_capital_off; // = 2131886987;
    @DexIgnore
    public static /* final */ int abc_capital_on; // = 2131886988;
    @DexIgnore
    public static /* final */ int abc_menu_alt_shortcut_label; // = 2131886989;
    @DexIgnore
    public static /* final */ int abc_menu_ctrl_shortcut_label; // = 2131886990;
    @DexIgnore
    public static /* final */ int abc_menu_delete_shortcut_label; // = 2131886991;
    @DexIgnore
    public static /* final */ int abc_menu_enter_shortcut_label; // = 2131886992;
    @DexIgnore
    public static /* final */ int abc_menu_function_shortcut_label; // = 2131886993;
    @DexIgnore
    public static /* final */ int abc_menu_meta_shortcut_label; // = 2131886994;
    @DexIgnore
    public static /* final */ int abc_menu_shift_shortcut_label; // = 2131886995;
    @DexIgnore
    public static /* final */ int abc_menu_space_shortcut_label; // = 2131886996;
    @DexIgnore
    public static /* final */ int abc_menu_sym_shortcut_label; // = 2131886997;
    @DexIgnore
    public static /* final */ int abc_prepend_shortcut_label; // = 2131886998;
    @DexIgnore
    public static /* final */ int abc_search_hint; // = 2131886999;
    @DexIgnore
    public static /* final */ int abc_searchview_description_clear; // = 2131887000;
    @DexIgnore
    public static /* final */ int abc_searchview_description_query; // = 2131887001;
    @DexIgnore
    public static /* final */ int abc_searchview_description_search; // = 2131887002;
    @DexIgnore
    public static /* final */ int abc_searchview_description_submit; // = 2131887003;
    @DexIgnore
    public static /* final */ int abc_searchview_description_voice; // = 2131887004;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with; // = 2131887005;
    @DexIgnore
    public static /* final */ int abc_shareactionprovider_share_with_application; // = 2131887006;
    @DexIgnore
    public static /* final */ int abc_toolbar_collapse_description; // = 2131887007;
    @DexIgnore
    public static /* final */ int search_menu_title; // = 2131887303;
    @DexIgnore
    public static /* final */ int status_bar_notification_info_overflow; // = 2131887323;
}
