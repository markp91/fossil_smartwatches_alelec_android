package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.internal.ServerProtocol;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c90 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ a90 b;
    @DexIgnore
    public /* final */ b90 c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ long f;
    @DexIgnore
    public /* final */ long g;
    @DexIgnore
    public /* final */ long h;
    @DexIgnore
    public /* final */ short i;
    @DexIgnore
    public /* final */ short j;
    @DexIgnore
    public /* final */ short k;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<c90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            long readLong = parcel.readLong();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                a90 valueOf = a90.valueOf(readString);
                String readString2 = parcel.readString();
                if (readString2 != null) {
                    wg6.a(readString2, "parcel.readString()!!");
                    return new c90(readLong, valueOf, b90.valueOf(readString2), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readLong(), (short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt());
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new c90[i];
        }
    }

    @DexIgnore
    public c90(long j2, a90 a90, b90 b90, long j3, long j4, long j5, long j6, long j7, short s, short s2, short s3) {
        this.a = j2;
        this.b = a90;
        this.c = b90;
        this.d = j3;
        this.e = j4;
        this.f = j5;
        this.g = j6;
        this.h = j7;
        this.i = s;
        this.j = s2;
        this.k = s3;
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject put = new JSONObject().put("session_id", this.a).put("type", cw0.a((Enum<?>) this.b)).put(ServerProtocol.DIALOG_PARAM_STATE, cw0.a((Enum<?>) this.c)).put("duration_in_second", this.d).put("number_of_step", this.e).put("distance_in_meter", this.f).put("active_calorie", this.g).put("total_calorie", this.h).put("current_heart_rate", Short.valueOf(this.i)).put("average_heart_rate", Short.valueOf(this.j)).put("maximum_heart_rate", Short.valueOf(this.k));
        wg6.a(put, "JSONObject()\n           \u2026_rate\", maximumHeartRate)");
        return put;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(c90.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            c90 c90 = (c90) obj;
            return this.a == c90.a && this.b == c90.b && this.c == c90.c && this.d == c90.d && this.e == c90.e && this.f == c90.f && this.g == c90.g && this.h == c90.h && this.i == c90.i && this.j == c90.j && this.k == c90.k;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.workoutsession.WorkoutSession");
    }

    @DexIgnore
    public final long getActiveCaloriesInCalorie() {
        return this.g;
    }

    @DexIgnore
    public final a90 getActivityType() {
        return this.b;
    }

    @DexIgnore
    public final short getAverageHeartRate() {
        return this.j;
    }

    @DexIgnore
    public final short getCurrentHeartRate() {
        return this.i;
    }

    @DexIgnore
    public final long getDistanceInMeter() {
        return this.f;
    }

    @DexIgnore
    public final long getDurationInSecond() {
        return this.d;
    }

    @DexIgnore
    public final short getMaximumHeartRate() {
        return this.k;
    }

    @DexIgnore
    public final long getNumberOfStep() {
        return this.e;
    }

    @DexIgnore
    public final long getSessionId() {
        return this.a;
    }

    @DexIgnore
    public final b90 getSessionState() {
        return this.c;
    }

    @DexIgnore
    public final long getTotalCaloriesInCalorie() {
        return this.h;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        int hashCode3 = Long.valueOf(this.d).hashCode();
        int hashCode4 = Long.valueOf(this.e).hashCode();
        int hashCode5 = Long.valueOf(this.f).hashCode();
        int hashCode6 = Long.valueOf(this.g).hashCode();
        return ((((((Long.valueOf(this.h).hashCode() + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + (Long.valueOf(this.a).hashCode() * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31) + this.i) * 31) + this.j) * 31) + this.k;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeLong(this.d);
        }
        if (parcel != null) {
            parcel.writeLong(this.e);
        }
        if (parcel != null) {
            parcel.writeLong(this.f);
        }
        if (parcel != null) {
            parcel.writeLong(this.g);
        }
        if (parcel != null) {
            parcel.writeLong(this.h);
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.i));
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.j));
        }
        if (parcel != null) {
            parcel.writeInt(cw0.b(this.k));
        }
    }
}
