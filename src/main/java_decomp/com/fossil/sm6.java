package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sm6 extends CancellationException implements bl6<sm6> {
    @DexIgnore
    public /* final */ rm6 job;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sm6(String str, Throwable th, rm6 rm6) {
        super(str);
        wg6.b(str, "message");
        wg6.b(rm6, "job");
        this.job = rm6;
        if (th != null) {
            initCause(th);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj != this) {
            if (obj instanceof sm6) {
                sm6 sm6 = (sm6) obj;
                if (!wg6.a((Object) sm6.getMessage(), (Object) getMessage()) || !wg6.a((Object) sm6.job, (Object) this.job) || !wg6.a((Object) sm6.getCause(), (Object) getCause())) {
                    return false;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public Throwable fillInStackTrace() {
        if (!nl6.c()) {
            return this;
        }
        Throwable fillInStackTrace = super.fillInStackTrace();
        wg6.a((Object) fillInStackTrace, "super.fillInStackTrace()");
        return fillInStackTrace;
    }

    @DexIgnore
    public int hashCode() {
        String message = getMessage();
        if (message != null) {
            int hashCode = ((message.hashCode() * 31) + this.job.hashCode()) * 31;
            Throwable cause = getCause();
            return hashCode + (cause != null ? cause.hashCode() : 0);
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    public String toString() {
        return super.toString() + "; job=" + this.job;
    }

    @DexIgnore
    public sm6 createCopy() {
        if (!nl6.c()) {
            return null;
        }
        String message = getMessage();
        if (message != null) {
            return new sm6(message, this, this.job);
        }
        wg6.a();
        throw null;
    }
}
