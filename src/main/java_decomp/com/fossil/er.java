package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class er<TranscodeType> extends gz<er<TranscodeType>> implements Cloneable, ar<er<TranscodeType>> {
    @DexIgnore
    public /* final */ Context E;
    @DexIgnore
    public /* final */ fr F;
    @DexIgnore
    public /* final */ Class<TranscodeType> G;
    @DexIgnore
    public /* final */ yq H;
    @DexIgnore
    public gr<?, ? super TranscodeType> I;
    @DexIgnore
    public Object J;
    @DexIgnore
    public List<mz<TranscodeType>> K;
    @DexIgnore
    public er<TranscodeType> L;
    @DexIgnore
    public er<TranscodeType> M;
    @DexIgnore
    public Float N;
    @DexIgnore
    public boolean O; // = true;
    @DexIgnore
    public boolean P;
    @DexIgnore
    public boolean Q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[ImageView.ScaleType.values().length];
        @DexIgnore
        public static /* final */ /* synthetic */ int[] b; // = new int[br.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /*
        static {
            try {
                b[br.LOW.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
            try {
                b[br.NORMAL.ordinal()] = 2;
            } catch (NoSuchFieldError unused2) {
            }
            try {
                b[br.HIGH.ordinal()] = 3;
            } catch (NoSuchFieldError unused3) {
            }
            try {
                b[br.IMMEDIATE.ordinal()] = 4;
            } catch (NoSuchFieldError unused4) {
            }
            a[ImageView.ScaleType.CENTER_CROP.ordinal()] = 1;
            a[ImageView.ScaleType.CENTER_INSIDE.ordinal()] = 2;
            a[ImageView.ScaleType.FIT_CENTER.ordinal()] = 3;
            a[ImageView.ScaleType.FIT_START.ordinal()] = 4;
            a[ImageView.ScaleType.FIT_END.ordinal()] = 5;
            a[ImageView.ScaleType.FIT_XY.ordinal()] = 6;
            a[ImageView.ScaleType.CENTER.ordinal()] = 7;
            try {
                a[ImageView.ScaleType.MATRIX.ordinal()] = 8;
            } catch (NoSuchFieldError unused5) {
            }
        }
        */
    }

    /*
    static {
        nz nzVar = (nz) ((nz) ((nz) new nz().a(ft.b)).a(br.LOW)).a(true);
    }
    */

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public er(wq wqVar, fr frVar, Class<TranscodeType> cls, Context context) {
        this.F = frVar;
        this.G = cls;
        this.E = context;
        this.I = frVar.b(cls);
        this.H = wqVar.f();
        a(frVar.g());
        a((gz<?>) frVar.h());
    }

    @DexIgnore
    public iz<TranscodeType> P() {
        return c(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    public er<TranscodeType> b(mz<TranscodeType> mzVar) {
        this.K = null;
        return a(mzVar);
    }

    @DexIgnore
    public iz<TranscodeType> c(int i, int i2) {
        lz lzVar = new lz(i, i2);
        a(lzVar, lzVar, l00.a());
        return lzVar;
    }

    @DexIgnore
    @SuppressLint({"CheckResult"})
    public final void a(List<mz<Object>> list) {
        for (mz<Object> a2 : list) {
            a(a2);
        }
    }

    @DexIgnore
    public final er<TranscodeType> b(Object obj) {
        this.J = obj;
        this.P = true;
        return this;
    }

    @DexIgnore
    public er<TranscodeType> clone() {
        er<TranscodeType> erVar = (er) super.clone();
        erVar.I = erVar.I.clone();
        return erVar;
    }

    @DexIgnore
    public er<TranscodeType> a(gz<?> gzVar) {
        q00.a(gzVar);
        return (er) super.a(gzVar);
    }

    @DexIgnore
    public er<TranscodeType> b(byte[] bArr) {
        b((Object) bArr);
        er a2 = !A() ? a((gz<?>) nz.b(ft.a)) : this;
        return !a2.E() ? a2.a((gz<?>) nz.c(true)) : a2;
    }

    @DexIgnore
    public er<TranscodeType> a(mz<TranscodeType> mzVar) {
        if (mzVar != null) {
            if (this.K == null) {
                this.K = new ArrayList();
            }
            this.K.add(mzVar);
        }
        return this;
    }

    @DexIgnore
    public er<TranscodeType> a(er<TranscodeType> erVar) {
        this.M = erVar;
        return this;
    }

    @DexIgnore
    public er<TranscodeType> a(Object obj) {
        b(obj);
        return this;
    }

    @DexIgnore
    public final <Y extends yz<TranscodeType>> Y b(Y y, mz<TranscodeType> mzVar, gz<?> gzVar, Executor executor) {
        q00.a(y);
        if (this.P) {
            jz a2 = a(y, mzVar, gzVar, executor);
            jz d = y.d();
            if (!a2.a(d) || a(gzVar, d)) {
                this.F.a((yz<?>) y);
                y.a(a2);
                this.F.a(y, a2);
                return y;
            }
            q00.a(d);
            if (!d.isRunning()) {
                d.c();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    @DexIgnore
    public er<TranscodeType> a(String str) {
        b((Object) str);
        return this;
    }

    @DexIgnore
    public er<TranscodeType> a(Uri uri) {
        b((Object) uri);
        return this;
    }

    @DexIgnore
    public er<TranscodeType> a(Integer num) {
        b((Object) num);
        return a((gz<?>) nz.b(d00.a(this.E)));
    }

    @DexIgnore
    public <Y extends yz<TranscodeType>> Y a(Y y) {
        a(y, (mz) null, l00.b());
        return y;
    }

    @DexIgnore
    public <Y extends yz<TranscodeType>> Y a(Y y, mz<TranscodeType> mzVar, Executor executor) {
        b(y, mzVar, this, executor);
        return y;
    }

    @DexIgnore
    public final boolean a(gz<?> gzVar, jz jzVar) {
        return !gzVar.B() && jzVar.g();
    }

    @DexIgnore
    public zz<ImageView, TranscodeType> a(ImageView imageView) {
        gz gzVar;
        r00.b();
        q00.a(imageView);
        if (!H() && F() && imageView.getScaleType() != null) {
            switch (a.a[imageView.getScaleType().ordinal()]) {
                case 1:
                    gzVar = clone().K();
                    break;
                case 2:
                    gzVar = clone().L();
                    break;
                case 3:
                case 4:
                case 5:
                    gzVar = clone().M();
                    break;
                case 6:
                    gzVar = clone().L();
                    break;
            }
        }
        gzVar = this;
        zz<ImageView, TranscodeType> a2 = this.H.a(imageView, this.G);
        b(a2, (mz) null, gzVar, l00.b());
        return a2;
    }

    @DexIgnore
    public final br b(br brVar) {
        int i = a.b[brVar.ordinal()];
        if (i == 1) {
            return br.NORMAL;
        }
        if (i == 2) {
            return br.HIGH;
        }
        if (i == 3 || i == 4) {
            return br.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + o());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r27v0, types: [com.fossil.gz<?>, com.fossil.gz] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final jz b(Object obj, yz<TranscodeType> yzVar, mz<TranscodeType> mzVar, kz kzVar, gr<?, ? super TranscodeType> grVar, br brVar, int i, int i2, gz<?> r27, Executor executor) {
        br brVar2;
        Object obj2 = obj;
        kz kzVar2 = kzVar;
        br brVar3 = brVar;
        er<TranscodeType> erVar = this.L;
        if (erVar != null) {
            if (!this.Q) {
                gr<?, ? super TranscodeType> grVar2 = erVar.O ? grVar : erVar.I;
                if (this.L.C()) {
                    brVar2 = this.L.o();
                } else {
                    brVar2 = b(brVar3);
                }
                br brVar4 = brVar2;
                int l = this.L.l();
                int k = this.L.k();
                if (r00.b(i, i2) && !this.L.I()) {
                    l = r27.l();
                    k = r27.k();
                }
                int i3 = k;
                qz qzVar = new qz(obj2, kzVar2);
                Object obj3 = obj;
                yz<TranscodeType> yzVar2 = yzVar;
                mz<TranscodeType> mzVar2 = mzVar;
                qz qzVar2 = qzVar;
                jz a2 = a(obj3, yzVar2, mzVar2, (gz<?>) r27, (kz) qzVar, grVar, brVar, i, i2, executor);
                this.Q = true;
                er<TranscodeType> erVar2 = this.L;
                jz a3 = erVar2.a(obj3, yzVar2, mzVar2, (kz) qzVar2, grVar2, brVar4, l, i3, (gz<?>) erVar2, executor);
                this.Q = false;
                qzVar2.a(a2, a3);
                return qzVar2;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.N == null) {
            return a(obj, yzVar, mzVar, (gz<?>) r27, kzVar, grVar, brVar, i, i2, executor);
        } else {
            qz qzVar3 = new qz(obj2, kzVar2);
            yz<TranscodeType> yzVar3 = yzVar;
            mz<TranscodeType> mzVar3 = mzVar;
            qz qzVar4 = qzVar3;
            gr<?, ? super TranscodeType> grVar3 = grVar;
            int i4 = i;
            int i5 = i2;
            Executor executor2 = executor;
            qzVar3.a(a(obj, yzVar3, mzVar3, (gz<?>) r27, (kz) qzVar4, grVar3, brVar, i4, i5, executor2), a(obj, yzVar3, mzVar3, (gz<?>) r27.clone().a(this.N.floatValue()), (kz) qzVar4, grVar3, b(brVar3), i4, i5, executor2));
            return qzVar3;
        }
    }

    @DexIgnore
    public final jz a(yz<TranscodeType> yzVar, mz<TranscodeType> mzVar, gz<?> gzVar, Executor executor) {
        return a(new Object(), yzVar, mzVar, (kz) null, this.I, gzVar.o(), gzVar.l(), gzVar.k(), gzVar, executor);
    }

    @DexIgnore
    public final jz a(Object obj, yz<TranscodeType> yzVar, mz<TranscodeType> mzVar, kz kzVar, gr<?, ? super TranscodeType> grVar, br brVar, int i, int i2, gz<?> gzVar, Executor executor) {
        hz hzVar;
        hz hzVar2;
        if (this.M != null) {
            hzVar2 = new hz(obj, kzVar);
            hzVar = hzVar2;
        } else {
            Object obj2 = obj;
            hzVar = null;
            hzVar2 = kzVar;
        }
        jz b = b(obj, yzVar, mzVar, hzVar2, grVar, brVar, i, i2, gzVar, executor);
        if (hzVar == null) {
            return b;
        }
        int l = this.M.l();
        int k = this.M.k();
        if (r00.b(i, i2) && !this.M.I()) {
            l = gzVar.l();
            k = gzVar.k();
        }
        er<TranscodeType> erVar = this.M;
        hz hzVar3 = hzVar;
        hzVar3.a(b, erVar.a(obj, yzVar, mzVar, (kz) hzVar3, erVar.I, erVar.o(), l, k, (gz<?>) this.M, executor));
        return hzVar3;
    }

    @DexIgnore
    public final jz a(Object obj, yz<TranscodeType> yzVar, mz<TranscodeType> mzVar, gz<?> gzVar, kz kzVar, gr<?, ? super TranscodeType> grVar, br brVar, int i, int i2, Executor executor) {
        Context context = this.E;
        yq yqVar = this.H;
        return pz.a(context, yqVar, obj, this.J, this.G, gzVar, i, i2, brVar, yzVar, mzVar, this.K, kzVar, yqVar.d(), grVar.a(), executor);
    }
}
