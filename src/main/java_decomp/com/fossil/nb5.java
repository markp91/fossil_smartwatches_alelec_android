package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nb5 implements Factory<qb5> {
    @DexIgnore
    public static qb5 a(lb5 lb5) {
        qb5 b = lb5.b();
        z76.a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }
}
