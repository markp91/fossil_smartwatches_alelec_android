package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pk4 {
    @DexIgnore
    public static /* final */ pk4 a; // = new pk4();

    @DexIgnore
    public final MicroAppInstruction.MicroAppID a(Integer num) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("LegacyHelper", "convertActionToMicroAppId " + num);
        if ((num != null && num.intValue() == 101) || ((num != null && num.intValue() == 102) || (num != null && num.intValue() == 103))) {
            return MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC;
        }
        if (num != null && num.intValue() == 104) {
            return MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_UP_ID;
        }
        if (num != null && num.intValue() == 105) {
            return MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_VOL_DOWN_ID;
        }
        if (num != null && num.intValue() == 2001) {
            return MicroAppInstruction.MicroAppID.UAPP_ACTIVITY_TAGGING_ID;
        }
        if (num != null && num.intValue() == 1000) {
            return MicroAppInstruction.MicroAppID.UAPP_GOAL_TRACKING_ID;
        }
        if (num != null && num.intValue() == 2003) {
            return MicroAppInstruction.MicroAppID.UAPP_DATE_ID;
        }
        if (num != null && num.intValue() == 2004) {
            return MicroAppInstruction.MicroAppID.UAPP_TIME2_ID;
        }
        if (num != null && num.intValue() == 2002) {
            return MicroAppInstruction.MicroAppID.UAPP_ALERT_ID;
        }
        if (num != null && num.intValue() == 2005) {
            return MicroAppInstruction.MicroAppID.UAPP_ALARM_ID;
        }
        if (num != null && num.intValue() == 3001) {
            return MicroAppInstruction.MicroAppID.UAPP_WEATHER_STANDARD;
        }
        if (num != null && num.intValue() == 3002) {
            return MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME;
        }
        if (num != null && num.intValue() == 505) {
            return MicroAppInstruction.MicroAppID.UAPP_RING_PHONE;
        }
        if ((num != null && num.intValue() == 201) || (num != null && num.intValue() == 202)) {
            return MicroAppInstruction.MicroAppID.UAPP_SELFIE;
        }
        if (num != null && num.intValue() == 2006) {
            return MicroAppInstruction.MicroAppID.UAPP_TOGGLE_MODE;
        }
        if (num != null && num.intValue() == 2007) {
            return MicroAppInstruction.MicroAppID.UAPP_STOPWATCH;
        }
        return null;
    }

    @DexIgnore
    public final String a(Gesture gesture) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("LegacyHelper", "convertGestureToPosition " + gesture);
        if (gesture != null) {
            switch (ok4.a[gesture.ordinal()]) {
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                    return "top";
                case 8:
                case 9:
                case 10:
                case 11:
                case 12:
                case 13:
                case 14:
                    return "middle";
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                    return "bottom";
            }
        }
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        local2.e("LegacyHelper", "unknown gesture " + gesture + ' ');
        return "top";
    }
}
