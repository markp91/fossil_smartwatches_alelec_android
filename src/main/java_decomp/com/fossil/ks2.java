package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks2 implements dl2<ns2> {
    @DexIgnore
    public static ks2 b; // = new ks2();
    @DexIgnore
    public /* final */ dl2<ns2> a;

    @DexIgnore
    public ks2(dl2<ns2> dl2) {
        this.a = hl2.a(dl2);
    }

    @DexIgnore
    public static boolean a() {
        return ((ns2) b.zza()).zza();
    }

    @DexIgnore
    public static boolean b() {
        return ((ns2) b.zza()).zzb();
    }

    @DexIgnore
    public final /* synthetic */ Object zza() {
        return this.a.zza();
    }

    @DexIgnore
    public ks2() {
        this(hl2.a(new ms2()));
    }
}
