package com.fossil;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y62 implements rt1 {
    @DexIgnore
    public /* final */ Set<Scope> a;

    @DexIgnore
    public y62(a aVar) {
        this.a = w82.a(aVar.a);
    }

    @DexIgnore
    public static a b() {
        return new a();
    }

    @DexIgnore
    public final List<Scope> a() {
        return new ArrayList(this.a);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof y62)) {
            return false;
        }
        return this.a.equals(((y62) obj).a);
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(this.a);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ Set<Scope> a;

        @DexIgnore
        public a() {
            this.a = new HashSet();
        }

        @DexIgnore
        public final a a(DataType dataType, int i) {
            w12.a(i == 0 || i == 1, (Object) "valid access types are FitnessOptions.ACCESS_READ or FitnessOptions.ACCESS_WRITE");
            if (i == 0 && dataType.C() != null) {
                this.a.add(new Scope(dataType.C()));
            } else if (i == 1 && dataType.D() != null) {
                this.a.add(new Scope(dataType.D()));
            }
            return this;
        }

        @DexIgnore
        public final y62 a() {
            return new y62(this);
        }
    }
}
