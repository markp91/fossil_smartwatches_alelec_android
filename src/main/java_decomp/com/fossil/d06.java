package com.fossil;

import android.content.Context;
import androidx.work.ListenableWorker;
import androidx.work.WorkerParameters;
import java.util.Iterator;
import java.util.Map;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d06 extends dm {
    @DexIgnore
    public /* final */ Map<Class<? extends ListenableWorker>, Provider<e06<? extends ListenableWorker>>> b;

    @DexIgnore
    public d06(Map<Class<? extends ListenableWorker>, Provider<e06<? extends ListenableWorker>>> map) {
        wg6.b(map, "workerFactoryMap");
        this.b = map;
    }

    @DexIgnore
    public ListenableWorker a(Context context, String str, WorkerParameters workerParameters) {
        T t;
        Provider provider;
        wg6.b(context, "appContext");
        wg6.b(str, "workerClassName");
        wg6.b(workerParameters, "workerParameters");
        Iterator<T> it = this.b.entrySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (Class.forName(str).isAssignableFrom((Class) ((Map.Entry) t).getKey())) {
                break;
            }
        }
        Map.Entry entry = (Map.Entry) t;
        if (entry != null && (provider = (Provider) entry.getValue()) != null) {
            return ((e06) provider.get()).a(context, workerParameters);
        }
        throw new IllegalArgumentException("could not find worker: " + str);
    }
}
