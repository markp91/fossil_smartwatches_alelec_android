package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ei2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ String e;
    @DexIgnore
    public /* final */ /* synthetic */ String f;
    @DexIgnore
    public /* final */ /* synthetic */ boolean g;
    @DexIgnore
    public /* final */ /* synthetic */ cs2 h;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 i;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ei2(ov2 ov2, String str, String str2, boolean z, cs2 cs2) {
        super(ov2);
        this.i = ov2;
        this.e = str;
        this.f = str2;
        this.g = z;
        this.h = cs2;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.i.g.getUserProperties(this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    public final void b() {
        this.h.d((Bundle) null);
    }
}
