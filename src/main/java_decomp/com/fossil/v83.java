package com.fossil;

import android.os.RemoteException;
import android.text.TextUtils;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ boolean a;
    @DexIgnore
    public /* final */ /* synthetic */ boolean b;
    @DexIgnore
    public /* final */ /* synthetic */ ab3 c;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 d;
    @DexIgnore
    public /* final */ /* synthetic */ ab3 e;
    @DexIgnore
    public /* final */ /* synthetic */ l83 f;

    @DexIgnore
    public v83(l83 l83, boolean z, boolean z2, ab3 ab3, ra3 ra3, ab3 ab32) {
        this.f = l83;
        this.a = z;
        this.b = z2;
        this.c = ab3;
        this.d = ra3;
        this.e = ab32;
    }

    @DexIgnore
    public final void run() {
        l43 d2 = this.f.d;
        if (d2 == null) {
            this.f.b().t().a("Discarding data. Failed to send conditional user property to service");
            return;
        }
        if (this.a) {
            this.f.a(d2, (e22) this.b ? null : this.c, this.d);
        } else {
            try {
                if (TextUtils.isEmpty(this.e.a)) {
                    d2.a(this.c, this.d);
                } else {
                    d2.a(this.c);
                }
            } catch (RemoteException e2) {
                this.f.b().t().a("Failed to send conditional user property to the service", e2);
            }
        }
        this.f.I();
    }
}
