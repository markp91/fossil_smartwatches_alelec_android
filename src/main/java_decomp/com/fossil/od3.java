package com.fossil;

import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od3<TResult> extends qc3<TResult> {
    @DexIgnore
    public /* final */ Object a; // = new Object();
    @DexIgnore
    public /* final */ ld3<TResult> b; // = new ld3<>();
    @DexIgnore
    public boolean c;
    @DexIgnore
    public volatile boolean d;
    @DexIgnore
    public TResult e;
    @DexIgnore
    public Exception f;

    @DexIgnore
    public final <X extends Throwable> TResult a(Class<X> cls) throws Throwable {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (cls.isInstance(this.f)) {
                throw ((Throwable) cls.cast(this.f));
            } else if (this.f == null) {
                tresult = this.e;
            } else {
                throw new oc3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final TResult b() {
        TResult tresult;
        synchronized (this.a) {
            g();
            i();
            if (this.f == null) {
                tresult = this.e;
            } else {
                throw new oc3(this.f);
            }
        }
        return tresult;
    }

    @DexIgnore
    public final boolean c() {
        return this.d;
    }

    @DexIgnore
    public final boolean d() {
        boolean z;
        synchronized (this.a) {
            z = this.c;
        }
        return z;
    }

    @DexIgnore
    public final boolean e() {
        boolean z;
        synchronized (this.a) {
            z = this.c && !this.d && this.f == null;
        }
        return z;
    }

    @DexIgnore
    public final boolean f() {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.d = true;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final void g() {
        w12.b(this.c, "Task is not yet complete");
    }

    @DexIgnore
    public final void h() {
        w12.b(!this.c, "Task is already complete");
    }

    @DexIgnore
    public final void i() {
        if (this.d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    @DexIgnore
    public final void j() {
        synchronized (this.a) {
            if (this.c) {
                this.b.a(this);
            }
        }
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> b(ic3<TResult, qc3<TContinuationResult>> ic3) {
        return b(sc3.a, ic3);
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> b(Executor executor, ic3<TResult, qc3<TContinuationResult>> ic3) {
        od3 od3 = new od3();
        this.b.a(new yc3(executor, ic3, od3));
        j();
        return od3;
    }

    @DexIgnore
    public final Exception a() {
        Exception exc;
        synchronized (this.a) {
            exc = this.f;
        }
        return exc;
    }

    @DexIgnore
    public final boolean b(TResult tresult) {
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.e = tresult;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final qc3<TResult> a(mc3<? super TResult> mc3) {
        a(sc3.a, mc3);
        return this;
    }

    @DexIgnore
    public final qc3<TResult> a(Executor executor, mc3<? super TResult> mc3) {
        this.b.a(new gd3(executor, mc3));
        j();
        return this;
    }

    @DexIgnore
    public final qc3<TResult> a(lc3 lc3) {
        a(sc3.a, lc3);
        return this;
    }

    @DexIgnore
    public final qc3<TResult> a(Executor executor, lc3 lc3) {
        this.b.a(new ed3(executor, lc3));
        j();
        return this;
    }

    @DexIgnore
    public final qc3<TResult> a(kc3<TResult> kc3) {
        a(sc3.a, kc3);
        return this;
    }

    @DexIgnore
    public final qc3<TResult> a(Executor executor, kc3<TResult> kc3) {
        this.b.a(new cd3(executor, kc3));
        j();
        return this;
    }

    @DexIgnore
    public final boolean b(Exception exc) {
        w12.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            if (this.c) {
                return false;
            }
            this.c = true;
            this.f = exc;
            this.b.a(this);
            return true;
        }
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> a(ic3<TResult, TContinuationResult> ic3) {
        return a(sc3.a, ic3);
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> a(Executor executor, ic3<TResult, TContinuationResult> ic3) {
        od3 od3 = new od3();
        this.b.a(new wc3(executor, ic3, od3));
        j();
        return od3;
    }

    @DexIgnore
    public final qc3<TResult> a(Executor executor, jc3 jc3) {
        this.b.a(new ad3(executor, jc3));
        j();
        return this;
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> a(Executor executor, pc3<TResult, TContinuationResult> pc3) {
        od3 od3 = new od3();
        this.b.a(new id3(executor, pc3, od3));
        j();
        return od3;
    }

    @DexIgnore
    public final <TContinuationResult> qc3<TContinuationResult> a(pc3<TResult, TContinuationResult> pc3) {
        return a(sc3.a, pc3);
    }

    @DexIgnore
    public final void a(TResult tresult) {
        synchronized (this.a) {
            h();
            this.c = true;
            this.e = tresult;
        }
        this.b.a(this);
    }

    @DexIgnore
    public final void a(Exception exc) {
        w12.a(exc, (Object) "Exception must not be null");
        synchronized (this.a) {
            h();
            this.c = true;
            this.f = exc;
        }
        this.b.a(this);
    }
}
