package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;
import com.google.android.gms.common.data.DataHolder;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface pe3 extends IInterface {
    @DexIgnore
    void a(ee3 ee3) throws RemoteException;

    @DexIgnore
    void a(ie3 ie3) throws RemoteException;

    @DexIgnore
    void a(re3 re3) throws RemoteException;

    @DexIgnore
    void a(te3 te3) throws RemoteException;

    @DexIgnore
    void a(we3 we3) throws RemoteException;

    @DexIgnore
    void a(ye3 ye3) throws RemoteException;

    @DexIgnore
    void a(DataHolder dataHolder) throws RemoteException;

    @DexIgnore
    void b(te3 te3) throws RemoteException;

    @DexIgnore
    void b(List<te3> list) throws RemoteException;
}
