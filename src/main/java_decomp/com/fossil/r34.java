package com.fossil;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import com.fossil.dy5;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.BaseFragment;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r34 implements p34 {
    @DexIgnore
    public /* final */ List<String> a; // = qd6.d("Agree term of use", "Agree privacy", "Allow location data processing");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public boolean a(List<String> list) {
        wg6.b(list, "agreeRequirements");
        return list.containsAll(this.a);
    }

    @DexIgnore
    public void a(BaseFragment baseFragment) {
        wg6.b(baseFragment, "fragment");
        String a2 = dy5.a(dy5.c.REPAIR_CENTER, (dy5.b) null);
        wg6.a((Object) a2, "url");
        a(a2, baseFragment);
    }

    @DexIgnore
    public final void a(String str, BaseFragment baseFragment) {
        try {
            baseFragment.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
        } catch (ActivityNotFoundException unused) {
            FLogger.INSTANCE.getLocal().e("CitizenBrandLogic", "Exception when start url with no browser app");
        }
    }
}
