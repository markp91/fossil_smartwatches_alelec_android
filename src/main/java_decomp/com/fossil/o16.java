package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.fossil.n16;
import com.squareup.picasso.Picasso;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o16 extends n16 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public o16(Context context) {
        this.a = context;
    }

    @DexIgnore
    public boolean a(l16 l16) {
        if (l16.e != 0) {
            return true;
        }
        return "android.resource".equals(l16.d.getScheme());
    }

    @DexIgnore
    public n16.a a(l16 l16, int i) throws IOException {
        Resources a2 = t16.a(this.a, l16);
        return new n16.a(a(a2, t16.a(a2, l16), l16), Picasso.LoadedFrom.DISK);
    }

    @DexIgnore
    public static Bitmap a(Resources resources, int i, l16 l16) {
        BitmapFactory.Options b = n16.b(l16);
        if (n16.a(b)) {
            BitmapFactory.decodeResource(resources, i, b);
            n16.a(l16.h, l16.i, b, l16);
        }
        return BitmapFactory.decodeResource(resources, i, b);
    }
}
