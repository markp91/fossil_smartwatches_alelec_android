package com.fossil;

import com.fossil.sq6;
import java.io.EOFException;
import java.io.IOException;
import java.net.ProtocolException;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fs6 implements wr6 {
    @DexIgnore
    public /* final */ OkHttpClient a;
    @DexIgnore
    public /* final */ tr6 b;
    @DexIgnore
    public /* final */ lt6 c;
    @DexIgnore
    public /* final */ kt6 d;
    @DexIgnore
    public int e; // = 0;
    @DexIgnore
    public long f; // = 262144;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public abstract class b implements zt6 {
        @DexIgnore
        public /* final */ pt6 a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public long c;

        @DexIgnore
        public b() {
            this.a = new pt6(fs6.this.c.b());
            this.c = 0;
        }

        @DexIgnore
        public final void a(boolean z, IOException iOException) throws IOException {
            fs6 fs6 = fs6.this;
            int i = fs6.e;
            if (i != 6) {
                if (i == 5) {
                    fs6.a(this.a);
                    fs6 fs62 = fs6.this;
                    fs62.e = 6;
                    tr6 tr6 = fs62.b;
                    if (tr6 != null) {
                        tr6.a(!z, fs62, this.c, iOException);
                        return;
                    }
                    return;
                }
                throw new IllegalStateException("state: " + fs6.this.e);
            }
        }

        @DexIgnore
        public au6 b() {
            return this.a;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            try {
                long b2 = fs6.this.c.b(jt6, j);
                if (b2 > 0) {
                    this.c += b2;
                }
                return b2;
            } catch (IOException e) {
                a(false, e);
                throw e;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class c implements yt6 {
        @DexIgnore
        public /* final */ pt6 a; // = new pt6(fs6.this.d.b());
        @DexIgnore
        public boolean b;

        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            if (this.b) {
                throw new IllegalStateException("closed");
            } else if (j != 0) {
                fs6.this.d.c(j);
                fs6.this.d.a("\r\n");
                fs6.this.d.a(jt6, j);
                fs6.this.d.a("\r\n");
            }
        }

        @DexIgnore
        public au6 b() {
            return this.a;
        }

        @DexIgnore
        public synchronized void close() throws IOException {
            if (!this.b) {
                this.b = true;
                fs6.this.d.a("0\r\n\r\n");
                fs6.this.a(this.a);
                fs6.this.e = 3;
            }
        }

        @DexIgnore
        public synchronized void flush() throws IOException {
            if (!this.b) {
                fs6.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends b {
        @DexIgnore
        public /* final */ tq6 e;
        @DexIgnore
        public long f; // = -1;
        @DexIgnore
        public boolean g; // = true;

        @DexIgnore
        public d(tq6 tq6) {
            super();
            this.e = tq6;
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.b) {
                throw new IllegalStateException("closed");
            } else if (!this.g) {
                return -1;
            } else {
                long j2 = this.f;
                if (j2 == 0 || j2 == -1) {
                    c();
                    if (!this.g) {
                        return -1;
                    }
                }
                long b = super.b(jt6, Math.min(j, this.f));
                if (b != -1) {
                    this.f -= b;
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            }
        }

        @DexIgnore
        public final void c() throws IOException {
            if (this.f != -1) {
                fs6.this.c.h();
            }
            try {
                this.f = fs6.this.c.q();
                String trim = fs6.this.c.h().trim();
                if (this.f < 0 || (!trim.isEmpty() && !trim.startsWith(";"))) {
                    throw new ProtocolException("expected chunk size and optional extensions but was \"" + this.f + trim + "\"");
                } else if (this.f == 0) {
                    this.g = false;
                    yr6.a(fs6.this.a.g(), this.e, fs6.this.f());
                    a(true, (IOException) null);
                }
            } catch (NumberFormatException e2) {
                throw new ProtocolException(e2.getMessage());
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.b) {
                if (this.g && !fr6.a((zt6) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.b = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e implements yt6 {
        @DexIgnore
        public /* final */ pt6 a; // = new pt6(fs6.this.d.b());
        @DexIgnore
        public boolean b;
        @DexIgnore
        public long c;

        @DexIgnore
        public e(long j) {
            this.c = j;
        }

        @DexIgnore
        public void a(jt6 jt6, long j) throws IOException {
            if (!this.b) {
                fr6.a(jt6.p(), 0, j);
                if (j <= this.c) {
                    fs6.this.d.a(jt6, j);
                    this.c -= j;
                    return;
                }
                throw new ProtocolException("expected " + this.c + " bytes but received " + j);
            }
            throw new IllegalStateException("closed");
        }

        @DexIgnore
        public au6 b() {
            return this.a;
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.b) {
                this.b = true;
                if (this.c <= 0) {
                    fs6.this.a(this.a);
                    fs6.this.e = 3;
                    return;
                }
                throw new ProtocolException("unexpected end of stream");
            }
        }

        @DexIgnore
        public void flush() throws IOException {
            if (!this.b) {
                fs6.this.d.flush();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends b {
        @DexIgnore
        public long e;

        @DexIgnore
        public f(fs6 fs6, long j) throws IOException {
            super();
            this.e = j;
            if (this.e == 0) {
                a(true, (IOException) null);
            }
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (!this.b) {
                long j2 = this.e;
                if (j2 == 0) {
                    return -1;
                }
                long b = super.b(jt6, Math.min(j2, j));
                if (b != -1) {
                    this.e -= b;
                    if (this.e == 0) {
                        a(true, (IOException) null);
                    }
                    return b;
                }
                ProtocolException protocolException = new ProtocolException("unexpected end of stream");
                a(false, protocolException);
                throw protocolException;
            } else {
                throw new IllegalStateException("closed");
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.b) {
                if (this.e != 0 && !fr6.a((zt6) this, 100, TimeUnit.MILLISECONDS)) {
                    a(false, (IOException) null);
                }
                this.b = true;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends b {
        @DexIgnore
        public boolean e;

        @DexIgnore
        public g(fs6 fs6) {
            super();
        }

        @DexIgnore
        public long b(jt6 jt6, long j) throws IOException {
            if (j < 0) {
                throw new IllegalArgumentException("byteCount < 0: " + j);
            } else if (this.b) {
                throw new IllegalStateException("closed");
            } else if (this.e) {
                return -1;
            } else {
                long b = super.b(jt6, j);
                if (b != -1) {
                    return b;
                }
                this.e = true;
                a(true, (IOException) null);
                return -1;
            }
        }

        @DexIgnore
        public void close() throws IOException {
            if (!this.b) {
                if (!this.e) {
                    a(false, (IOException) null);
                }
                this.b = true;
            }
        }
    }

    @DexIgnore
    public fs6(OkHttpClient okHttpClient, tr6 tr6, lt6 lt6, kt6 kt6) {
        this.a = okHttpClient;
        this.b = tr6;
        this.c = lt6;
        this.d = kt6;
    }

    @DexIgnore
    public yt6 a(yq6 yq6, long j) {
        if ("chunked".equalsIgnoreCase(yq6.a("Transfer-Encoding"))) {
            return c();
        }
        if (j != -1) {
            return a(j);
        }
        throw new IllegalStateException("Cannot stream a request body without chunked encoding or a known content length!");
    }

    @DexIgnore
    public void b() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public yt6 c() {
        if (this.e == 1) {
            this.e = 2;
            return new c();
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void cancel() {
        pr6 c2 = this.b.c();
        if (c2 != null) {
            c2.b();
        }
    }

    @DexIgnore
    public zt6 d() throws IOException {
        if (this.e == 4) {
            tr6 tr6 = this.b;
            if (tr6 != null) {
                this.e = 5;
                tr6.e();
                return new g(this);
            }
            throw new IllegalStateException("streamAllocation == null");
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public final String e() throws IOException {
        String f2 = this.c.f(this.f);
        this.f -= (long) f2.length();
        return f2;
    }

    @DexIgnore
    public sq6 f() throws IOException {
        sq6.a aVar = new sq6.a();
        while (true) {
            String e2 = e();
            if (e2.length() == 0) {
                return aVar.a();
            }
            dr6.a.a(aVar, e2);
        }
    }

    @DexIgnore
    public zt6 b(long j) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new f(this, j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(yq6 yq6) throws IOException {
        a(yq6.c(), cs6.a(yq6, this.b.c().f().b().type()));
    }

    @DexIgnore
    public zq6 a(Response response) throws IOException {
        tr6 tr6 = this.b;
        tr6.f.e(tr6.e);
        String e2 = response.e("Content-Type");
        if (!yr6.b(response)) {
            return new bs6(e2, 0, st6.a(b(0)));
        }
        if ("chunked".equalsIgnoreCase(response.e("Transfer-Encoding"))) {
            return new bs6(e2, -1, st6.a(a(response.I().g())));
        }
        long a2 = yr6.a(response);
        if (a2 != -1) {
            return new bs6(e2, a2, st6.a(b(a2)));
        }
        return new bs6(e2, -1, st6.a(d()));
    }

    @DexIgnore
    public void a() throws IOException {
        this.d.flush();
    }

    @DexIgnore
    public void a(sq6 sq6, String str) throws IOException {
        if (this.e == 0) {
            this.d.a(str).a("\r\n");
            int b2 = sq6.b();
            for (int i = 0; i < b2; i++) {
                this.d.a(sq6.a(i)).a(": ").a(sq6.b(i)).a("\r\n");
            }
            this.d.a("\r\n");
            this.e = 1;
            return;
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public Response.a a(boolean z) throws IOException {
        int i = this.e;
        if (i == 1 || i == 3) {
            try {
                es6 a2 = es6.a(e());
                Response.a aVar = new Response.a();
                aVar.a(a2.a);
                aVar.a(a2.b);
                aVar.a(a2.c);
                aVar.a(f());
                if (z && a2.b == 100) {
                    return null;
                }
                if (a2.b == 100) {
                    this.e = 3;
                    return aVar;
                }
                this.e = 4;
                return aVar;
            } catch (EOFException e2) {
                IOException iOException = new IOException("unexpected end of stream on " + this.b);
                iOException.initCause(e2);
                throw iOException;
            }
        } else {
            throw new IllegalStateException("state: " + this.e);
        }
    }

    @DexIgnore
    public yt6 a(long j) {
        if (this.e == 1) {
            this.e = 2;
            return new e(j);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public zt6 a(tq6 tq6) throws IOException {
        if (this.e == 4) {
            this.e = 5;
            return new d(tq6);
        }
        throw new IllegalStateException("state: " + this.e);
    }

    @DexIgnore
    public void a(pt6 pt6) {
        au6 g2 = pt6.g();
        pt6.a(au6.d);
        g2.a();
        g2.b();
    }
}
