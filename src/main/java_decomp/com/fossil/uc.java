package com.fossil;

import androidx.lifecycle.LiveData;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class uc<T> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ LiveData<T> b;
    @DexIgnore
    public /* final */ AtomicBoolean c; // = new AtomicBoolean(true);
    @DexIgnore
    public /* final */ AtomicBoolean d; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ Runnable e; // = new b();
    @DexIgnore
    public /* final */ Runnable f; // = new c();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LiveData<T> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void d() {
            uc ucVar = uc.this;
            ucVar.a.execute(ucVar.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements Runnable {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
        public void run() {
            boolean z;
            do {
                if (uc.this.d.compareAndSet(false, true)) {
                    Object obj = null;
                    z = false;
                    while (uc.this.c.compareAndSet(true, false)) {
                        try {
                            obj = uc.this.a();
                            z = true;
                        } finally {
                            uc.this.d.set(false);
                        }
                    }
                    if (z) {
                        uc.this.b.a(obj);
                    }
                } else {
                    z = false;
                }
                if (!z || !uc.this.c.get()) {
                }
            } while (!uc.this.c.get());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            boolean c = uc.this.b.c();
            if (uc.this.c.compareAndSet(false, true) && c) {
                uc ucVar = uc.this;
                ucVar.a.execute(ucVar.e);
            }
        }
    }

    @DexIgnore
    public uc(Executor executor) {
        this.a = executor;
        this.b = new a();
    }

    @DexIgnore
    public abstract T a();

    @DexIgnore
    public LiveData<T> b() {
        return this.b;
    }

    @DexIgnore
    public void c() {
        q3.c().b(this.f);
    }
}
