package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w64 extends v64 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j A; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray B; // = new SparseIntArray();
    @DexIgnore
    public long z;

    /*
    static {
        B.put(2131363256, 1);
        B.put(2131363257, 2);
        B.put(2131362286, 3);
        B.put(2131362018, 4);
        B.put(2131362287, 5);
        B.put(2131362274, 6);
        B.put(2131362280, 7);
        B.put(2131362862, 8);
    }
    */

    @DexIgnore
    public w64(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, A, B));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.z = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.z != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.z = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w64(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[6], objArr[7], objArr[3], objArr[5], objArr[0], objArr[8], objArr[1], objArr[2]);
        this.z = -1;
        this.v.setTag((Object) null);
        a(view);
        f();
    }
}
