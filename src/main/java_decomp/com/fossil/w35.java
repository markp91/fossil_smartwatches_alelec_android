package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w35 implements MembersInjector<DianaCustomizeEditActivity> {
    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, DianaCustomizeEditPresenter dianaCustomizeEditPresenter) {
        dianaCustomizeEditActivity.B = dianaCustomizeEditPresenter;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditActivity dianaCustomizeEditActivity, w04 w04) {
        dianaCustomizeEditActivity.C = w04;
    }
}
