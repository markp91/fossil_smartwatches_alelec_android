package com.fossil;

import android.bluetooth.BluetoothDevice;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wl0 extends xg6 implements hg6<r40, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ BluetoothDevice a;
    @DexIgnore
    public /* final */ /* synthetic */ ii1 b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wl0(BluetoothDevice bluetoothDevice, ii1 ii1) {
        super(1);
        this.a = bluetoothDevice;
        this.b = ii1;
    }

    @DexIgnore
    public Object invoke(Object obj) {
        ut0 ut0 = ut0.c;
        String serialNumber = ((r40) obj).getSerialNumber();
        String address = this.a.getAddress();
        wg6.a(address, "bluetoothDevice.address");
        ut0.a(serialNumber, address);
        k40 k40 = k40.l;
        k40.d.remove(this.a.getAddress());
        k40.a(k40.l, this.b);
        return cd6.a;
    }
}
