package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataType;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fe2 implements Parcelable.Creator<de2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        ArrayList<DataType> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 1) {
                f22.v(parcel, a);
            } else {
                arrayList = f22.c(parcel, a, DataType.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new de2(arrayList);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new de2[i];
    }
}
