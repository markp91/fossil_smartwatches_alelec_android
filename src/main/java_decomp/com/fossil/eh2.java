package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eh2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<eh2> CREATOR; // = new fh2();
    @DexIgnore
    public int a;
    @DexIgnore
    public ch2 b;
    @DexIgnore
    public yw2 c;
    @DexIgnore
    public ag2 d;

    @DexIgnore
    public eh2(int i, ch2 ch2, IBinder iBinder, IBinder iBinder2) {
        this.a = i;
        this.b = ch2;
        ag2 ag2 = null;
        this.c = iBinder == null ? null : zw2.a(iBinder);
        if (!(iBinder2 == null || iBinder2 == null)) {
            IInterface queryLocalInterface = iBinder2.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            ag2 = queryLocalInterface instanceof ag2 ? (ag2) queryLocalInterface : new cg2(iBinder2);
        }
        this.d = ag2;
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, (Parcelable) this.b, i, false);
        yw2 yw2 = this.c;
        IBinder iBinder = null;
        g22.a(parcel, 3, yw2 == null ? null : yw2.asBinder(), false);
        ag2 ag2 = this.d;
        if (ag2 != null) {
            iBinder = ag2.asBinder();
        }
        g22.a(parcel, 4, iBinder, false);
        g22.a(parcel, a2);
    }
}
