package com.fossil;

import com.fossil.vr1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr1 extends vr1 {
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ long e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends vr1.a {
        @DexIgnore
        public Long a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public Integer c;
        @DexIgnore
        public Long d;

        @DexIgnore
        public vr1.a a(int i) {
            this.c = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public vr1.a b(long j) {
            this.a = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public vr1.a a(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public vr1.a b(int i) {
            this.b = Integer.valueOf(i);
            return this;
        }

        @DexIgnore
        public vr1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " maxStorageSizeInBytes";
            }
            if (this.b == null) {
                str = str + " loadBatchSize";
            }
            if (this.c == null) {
                str = str + " criticalSectionEnterTimeoutMs";
            }
            if (this.d == null) {
                str = str + " eventCleanUpAge";
            }
            if (str.isEmpty()) {
                return new sr1(this.a.longValue(), this.b.intValue(), this.c.intValue(), this.d.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public int a() {
        return this.d;
    }

    @DexIgnore
    public long b() {
        return this.e;
    }

    @DexIgnore
    public int c() {
        return this.c;
    }

    @DexIgnore
    public long d() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof vr1)) {
            return false;
        }
        vr1 vr1 = (vr1) obj;
        if (this.b == vr1.d() && this.c == vr1.c() && this.d == vr1.a() && this.e == vr1.b()) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        long j = this.b;
        long j2 = this.e;
        return ((int) ((j2 >>> 32) ^ j2)) ^ ((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.c) * 1000003) ^ this.d) * 1000003);
    }

    @DexIgnore
    public String toString() {
        return "EventStoreConfig{maxStorageSizeInBytes=" + this.b + ", loadBatchSize=" + this.c + ", criticalSectionEnterTimeoutMs=" + this.d + ", eventCleanUpAge=" + this.e + "}";
    }

    @DexIgnore
    public sr1(long j, int i, int i2, long j2) {
        this.b = j;
        this.c = i;
        this.d = i2;
        this.e = j2;
    }
}
