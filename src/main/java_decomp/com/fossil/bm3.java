package com.fossil;

import com.fossil.vl3;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bm3<K, V> implements Map<K, V>, Serializable {
    @DexIgnore
    public static /* final */ Map.Entry<?, ?>[] EMPTY_ENTRY_ARRAY; // = new Map.Entry[0];
    @DexIgnore
    public transient im3<Map.Entry<K, V>> a;
    @DexIgnore
    public transient im3<K> b;
    @DexIgnore
    public transient vl3<V> c;
    @DexIgnore
    public transient jm3<K, V> d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jo3<K> {
        @DexIgnore
        public /* final */ /* synthetic */ jo3 a;

        @DexIgnore
        public a(bm3 bm3, jo3 jo3) {
            this.a = jo3;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public K next() {
            return ((Map.Entry) this.a.next()).getKey();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> {
        @DexIgnore
        public Comparator<? super V> a;
        @DexIgnore
        public cm3<K, V>[] b;
        @DexIgnore
        public int c;
        @DexIgnore
        public boolean d;

        @DexIgnore
        public b() {
            this(4);
        }

        @DexIgnore
        public final void a(int i) {
            cm3<K, V>[] cm3Arr = this.b;
            if (i > cm3Arr.length) {
                this.b = (cm3[]) in3.a((T[]) cm3Arr, vl3.b.a(cm3Arr.length, i));
                this.d = false;
            }
        }

        @DexIgnore
        public b(int i) {
            this.b = new cm3[i];
            this.c = 0;
            this.d = false;
        }

        @DexIgnore
        public b<K, V> a(K k, V v) {
            a(this.c + 1);
            cm3<K, V> entryOf = bm3.entryOf(k, v);
            cm3<K, V>[] cm3Arr = this.b;
            int i = this.c;
            this.c = i + 1;
            cm3Arr[i] = entryOf;
            return this;
        }

        @DexIgnore
        public b<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            return a(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public b<K, V> a(Map<? extends K, ? extends V> map) {
            return a(map.entrySet());
        }

        @DexIgnore
        public b<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            if (iterable instanceof Collection) {
                a(this.c + ((Collection) iterable).size());
            }
            for (Map.Entry a2 : iterable) {
                a(a2);
            }
            return this;
        }

        @DexIgnore
        public bm3<K, V> a() {
            int i = this.c;
            if (i == 0) {
                return bm3.of();
            }
            boolean z = true;
            if (i == 1) {
                return bm3.of(this.b[0].getKey(), this.b[0].getValue());
            }
            if (this.a != null) {
                if (this.d) {
                    this.b = (cm3[]) in3.a((T[]) this.b, i);
                }
                Arrays.sort(this.b, 0, this.c, jn3.from(this.a).onResultOf(ym3.c()));
            }
            if (this.c != this.b.length) {
                z = false;
            }
            this.d = z;
            return qn3.fromEntryArray(this.c, this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends bm3<K, V> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends dm3<K, V> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public bm3<K, V> map() {
                return c.this;
            }

            @DexIgnore
            public jo3<Map.Entry<K, V>> iterator() {
                return c.this.entryIterator();
            }
        }

        @DexIgnore
        public im3<Map.Entry<K, V>> createEntrySet() {
            return new a();
        }

        @DexIgnore
        public abstract jo3<Map.Entry<K, V>> entryIterator();

        @DexIgnore
        public /* bridge */ /* synthetic */ Set entrySet() {
            return bm3.super.entrySet();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Set keySet() {
            return bm3.super.keySet();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Collection values() {
            return bm3.super.values();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class d extends c<K, im3<V>> {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends jo3<Map.Entry<K, im3<V>>> {
            @DexIgnore
            public /* final */ /* synthetic */ Iterator a;

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.bm3$d$a$a")
            /* renamed from: com.fossil.bm3$d$a$a  reason: collision with other inner class name */
            public class C0009a extends uk3<K, im3<V>> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry a;

                @DexIgnore
                public C0009a(a aVar, Map.Entry entry) {
                    this.a = entry;
                }

                @DexIgnore
                public K getKey() {
                    return this.a.getKey();
                }

                @DexIgnore
                public im3<V> getValue() {
                    return im3.of(this.a.getValue());
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.a = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.a.hasNext();
            }

            @DexIgnore
            public Map.Entry<K, im3<V>> next() {
                return new C0009a(this, (Map.Entry) this.a.next());
            }
        }

        @DexIgnore
        public d() {
        }

        @DexIgnore
        public boolean containsKey(Object obj) {
            return bm3.this.containsKey(obj);
        }

        @DexIgnore
        public jo3<Map.Entry<K, im3<V>>> entryIterator() {
            return new a(this, bm3.this.entrySet().iterator());
        }

        @DexIgnore
        public int hashCode() {
            return bm3.this.hashCode();
        }

        @DexIgnore
        public boolean isHashCodeFast() {
            return bm3.this.isHashCodeFast();
        }

        @DexIgnore
        public boolean isPartialView() {
            return bm3.this.isPartialView();
        }

        @DexIgnore
        public int size() {
            return bm3.this.size();
        }

        @DexIgnore
        public /* synthetic */ d(bm3 bm3, a aVar) {
            this();
        }

        @DexIgnore
        public im3<V> get(Object obj) {
            Object obj2 = bm3.this.get(obj);
            if (obj2 == null) {
                return null;
            }
            return im3.of(obj2);
        }

        @DexIgnore
        public im3<K> keySet() {
            return bm3.this.keySet();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ Object[] keys;
        @DexIgnore
        public /* final */ Object[] values;

        @DexIgnore
        public e(bm3<?, ?> bm3) {
            this.keys = new Object[bm3.size()];
            this.values = new Object[bm3.size()];
            jo3<Map.Entry<?, ?>> it = bm3.entrySet().iterator();
            int i = 0;
            while (it.hasNext()) {
                Map.Entry next = it.next();
                this.keys[i] = next.getKey();
                this.values[i] = next.getValue();
                i++;
            }
        }

        @DexIgnore
        public Object createMap(b<Object, Object> bVar) {
            int i = 0;
            while (true) {
                Object[] objArr = this.keys;
                if (i >= objArr.length) {
                    return bVar.a();
                }
                bVar.a(objArr[i], this.values[i]);
                i++;
            }
        }

        @DexIgnore
        public Object readResolve() {
            return createMap(new b(this.keys.length));
        }
    }

    @DexIgnore
    public static <K extends Enum<K>, V> bm3<K, V> a(EnumMap<K, ? extends V> enumMap) {
        EnumMap enumMap2 = new EnumMap(enumMap);
        for (Map.Entry entry : enumMap2.entrySet()) {
            bl3.a(entry.getKey(), entry.getValue());
        }
        return xl3.asImmutable(enumMap2);
    }

    @DexIgnore
    public static <K, V> b<K, V> builder() {
        return new b<>();
    }

    @DexIgnore
    public static void checkNoConflict(boolean z, String str, Map.Entry<?, ?> entry, Map.Entry<?, ?> entry2) {
        if (!z) {
            throw new IllegalArgumentException("Multiple entries with same " + str + ": " + entry + " and " + entry2);
        }
    }

    @DexIgnore
    public static <K, V> bm3<K, V> copyOf(Map<? extends K, ? extends V> map) {
        if ((map instanceof bm3) && !(map instanceof lm3)) {
            bm3<K, V> bm3 = (bm3) map;
            if (!bm3.isPartialView()) {
                return bm3;
            }
        } else if (map instanceof EnumMap) {
            return a((EnumMap) map);
        }
        return copyOf(map.entrySet());
    }

    @DexIgnore
    public static <K, V> cm3<K, V> entryOf(K k, V v) {
        return new cm3<>(k, v);
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of() {
        return ul3.of();
    }

    @DexIgnore
    public jm3<K, V> asMultimap() {
        if (isEmpty()) {
            return jm3.of();
        }
        jm3<K, V> jm3 = this.d;
        if (jm3 != null) {
            return jm3;
        }
        jm3<K, V> jm32 = new jm3<>(new d(this, (a) null), size(), (Comparator) null);
        this.d = jm32;
        return jm32;
    }

    @DexIgnore
    @Deprecated
    public final void clear() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean containsKey(Object obj) {
        return get(obj) != null;
    }

    @DexIgnore
    public boolean containsValue(Object obj) {
        return values().contains(obj);
    }

    @DexIgnore
    public abstract im3<Map.Entry<K, V>> createEntrySet();

    @DexIgnore
    public im3<K> createKeySet() {
        return isEmpty() ? im3.of() : new em3(this);
    }

    @DexIgnore
    public vl3<V> createValues() {
        return new fm3(this);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return ym3.c(this, obj);
    }

    @DexIgnore
    public abstract V get(Object obj);

    @DexIgnore
    public int hashCode() {
        return yn3.a((Set<?>) entrySet());
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public boolean isHashCodeFast() {
        return false;
    }

    @DexIgnore
    public abstract boolean isPartialView();

    @DexIgnore
    public jo3<K> keyIterator() {
        return new a(this, entrySet().iterator());
    }

    @DexIgnore
    @Deprecated
    public final V put(K k, V v) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final void putAll(Map<? extends K, ? extends V> map) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public final V remove(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public String toString() {
        return ym3.a((Map<?, ?>) this);
    }

    @DexIgnore
    public Object writeReplace() {
        return new e(this);
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of(K k, V v) {
        return ul3.of(k, v);
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> entrySet() {
        im3<Map.Entry<K, V>> im3 = this.a;
        if (im3 != null) {
            return im3;
        }
        im3<Map.Entry<K, V>> createEntrySet = createEntrySet();
        this.a = createEntrySet;
        return createEntrySet;
    }

    @DexIgnore
    public im3<K> keySet() {
        im3<K> im3 = this.b;
        if (im3 != null) {
            return im3;
        }
        im3<K> createKeySet = createKeySet();
        this.b = createKeySet;
        return createKeySet;
    }

    @DexIgnore
    public vl3<V> values() {
        vl3<V> vl3 = this.c;
        if (vl3 != null) {
            return vl3;
        }
        vl3<V> createValues = createValues();
        this.c = createValues;
        return createValues;
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of(K k, V v, K k2, V v2) {
        return qn3.fromEntries(entryOf(k, v), entryOf(k2, v2));
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        return qn3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3));
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        return qn3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4));
    }

    @DexIgnore
    public static <K, V> bm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        return qn3.fromEntries(entryOf(k, v), entryOf(k2, v2), entryOf(k3, v3), entryOf(k4, v4), entryOf(k5, v5));
    }

    @DexIgnore
    public static <K, V> bm3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        Map.Entry[] entryArr = (Map.Entry[]) pm3.a(iterable, (T[]) EMPTY_ENTRY_ARRAY);
        int length = entryArr.length;
        if (length == 0) {
            return of();
        }
        if (length != 1) {
            return qn3.fromEntries(entryArr);
        }
        Map.Entry entry = entryArr[0];
        return of(entry.getKey(), entry.getValue());
    }
}
