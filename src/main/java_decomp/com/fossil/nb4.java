package com.fossil;

import android.view.View;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class nb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ScrollView q;
    @DexIgnore
    public /* final */ RecyclerView r;
    @DexIgnore
    public /* final */ RecyclerView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;

    @DexIgnore
    public nb4(Object obj, View view, int i, ConstraintLayout constraintLayout, ScrollView scrollView, RecyclerView recyclerView, RecyclerView recyclerView2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5) {
        super(obj, view, i);
        this.q = scrollView;
        this.r = recyclerView;
        this.s = recyclerView2;
        this.t = flexibleTextView;
        this.u = flexibleTextView2;
        this.v = flexibleTextView3;
        this.w = flexibleTextView4;
        this.x = flexibleTextView5;
    }
}
