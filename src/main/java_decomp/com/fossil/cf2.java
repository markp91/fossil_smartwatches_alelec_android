package com.fossil;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf2 extends qe2<ed2> {
    @DexIgnore
    public static /* final */ je2 E; // = je2.FIT_GOALS;
    @DexIgnore
    public static /* final */ rv1.g<cf2> F; // = new rv1.g<>();
    @DexIgnore
    public static /* final */ rv1<rv1.d.C0044d> G; // = new rv1<>("Fitness.GOALS_API", new zb2(), F);

    /*
    static {
        new rv1("Fitness.GOALS_CLIENT", new bc2(), F);
    }
    */

    @DexIgnore
    public cf2(Context context, Looper looper, e12 e12, wv1.b bVar, wv1.c cVar) {
        super(context, looper, E, bVar, cVar, e12);
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.fitness.GoalsApi";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.fitness.internal.IGoogleFitGoalsApi");
        if (queryLocalInterface instanceof ed2) {
            return (ed2) queryLocalInterface;
        }
        return new dd2(iBinder);
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.fitness.internal.IGoogleFitGoalsApi";
    }
}
