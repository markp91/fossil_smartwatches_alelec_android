package com.fossil;

import android.os.Build;
import android.os.Bundle;
import android.text.style.ClickableSpan;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import com.fossil.ia;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a9 {
    @DexIgnore
    public static /* final */ View.AccessibilityDelegate c; // = new View.AccessibilityDelegate();
    @DexIgnore
    public /* final */ View.AccessibilityDelegate a;
    @DexIgnore
    public /* final */ View.AccessibilityDelegate b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends View.AccessibilityDelegate {
        @DexIgnore
        public /* final */ a9 a;

        @DexIgnore
        public a(a9 a9Var) {
            this.a = a9Var;
        }

        @DexIgnore
        public boolean dispatchPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            return this.a.a(view, accessibilityEvent);
        }

        @DexIgnore
        public AccessibilityNodeProvider getAccessibilityNodeProvider(View view) {
            ja a2 = this.a.a(view);
            if (a2 != null) {
                return (AccessibilityNodeProvider) a2.a();
            }
            return null;
        }

        @DexIgnore
        public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.b(view, accessibilityEvent);
        }

        @DexIgnore
        public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfo accessibilityNodeInfo) {
            ia a2 = ia.a(accessibilityNodeInfo);
            a2.m(x9.H(view));
            a2.k(x9.C(view));
            a2.f(x9.e(view));
            this.a.a(view, a2);
            a2.a(accessibilityNodeInfo.getText(), view);
            List<ia.a> b = a9.b(view);
            for (int i = 0; i < b.size(); i++) {
                a2.a(b.get(i));
            }
        }

        @DexIgnore
        public void onPopulateAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
            this.a.c(view, accessibilityEvent);
        }

        @DexIgnore
        public boolean onRequestSendAccessibilityEvent(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            return this.a.a(viewGroup, view, accessibilityEvent);
        }

        @DexIgnore
        public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
            return this.a.a(view, i, bundle);
        }

        @DexIgnore
        public void sendAccessibilityEvent(View view, int i) {
            this.a.a(view, i);
        }

        @DexIgnore
        public void sendAccessibilityEventUnchecked(View view, AccessibilityEvent accessibilityEvent) {
            this.a.d(view, accessibilityEvent);
        }
    }

    @DexIgnore
    public a9() {
        this(c);
    }

    @DexIgnore
    public View.AccessibilityDelegate a() {
        return this.b;
    }

    @DexIgnore
    public void b(View view, AccessibilityEvent accessibilityEvent) {
        this.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void c(View view, AccessibilityEvent accessibilityEvent) {
        this.a.onPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void d(View view, AccessibilityEvent accessibilityEvent) {
        this.a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
    }

    @DexIgnore
    public a9(View.AccessibilityDelegate accessibilityDelegate) {
        this.a = accessibilityDelegate;
        this.b = new a(this);
    }

    @DexIgnore
    public static List<ia.a> b(View view) {
        List<ia.a> list = (List) view.getTag(f6.tag_accessibility_actions);
        return list == null ? Collections.emptyList() : list;
    }

    @DexIgnore
    public void a(View view, int i) {
        this.a.sendAccessibilityEvent(view, i);
    }

    @DexIgnore
    public boolean a(View view, AccessibilityEvent accessibilityEvent) {
        return this.a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
    }

    @DexIgnore
    public void a(View view, ia iaVar) {
        this.a.onInitializeAccessibilityNodeInfo(view, iaVar.A());
    }

    @DexIgnore
    public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
        return this.a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
    }

    @DexIgnore
    public ja a(View view) {
        AccessibilityNodeProvider accessibilityNodeProvider;
        if (Build.VERSION.SDK_INT < 16 || (accessibilityNodeProvider = this.a.getAccessibilityNodeProvider(view)) == null) {
            return null;
        }
        return new ja(accessibilityNodeProvider);
    }

    @DexIgnore
    public boolean a(View view, int i, Bundle bundle) {
        List<ia.a> b2 = b(view);
        boolean z = false;
        int i2 = 0;
        while (true) {
            if (i2 >= b2.size()) {
                break;
            }
            ia.a aVar = b2.get(i2);
            if (aVar.a() == i) {
                z = aVar.a(view, bundle);
                break;
            }
            i2++;
        }
        if (!z && Build.VERSION.SDK_INT >= 16) {
            z = this.a.performAccessibilityAction(view, i, bundle);
        }
        return (z || i != f6.accessibility_action_clickable_span) ? z : a(bundle.getInt("ACCESSIBILITY_CLICKABLE_SPAN_ID", -1), view);
    }

    @DexIgnore
    public final boolean a(int i, View view) {
        WeakReference weakReference;
        SparseArray sparseArray = (SparseArray) view.getTag(f6.tag_accessibility_clickable_spans);
        if (sparseArray == null || (weakReference = (WeakReference) sparseArray.get(i)) == null) {
            return false;
        }
        ClickableSpan clickableSpan = (ClickableSpan) weakReference.get();
        if (!a(clickableSpan, view)) {
            return false;
        }
        clickableSpan.onClick(view);
        return true;
    }

    @DexIgnore
    public final boolean a(ClickableSpan clickableSpan, View view) {
        if (clickableSpan != null) {
            ClickableSpan[] h = ia.h(view.createAccessibilityNodeInfo().getText());
            int i = 0;
            while (h != null && i < h.length) {
                if (clickableSpan.equals(h[i])) {
                    return true;
                }
                i++;
            }
        }
        return false;
    }
}
