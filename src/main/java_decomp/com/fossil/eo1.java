package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo1<T> extends fo1<T> {
    @DexIgnore
    public /* final */ Integer a;
    @DexIgnore
    public /* final */ T b;
    @DexIgnore
    public /* final */ go1 c;

    @DexIgnore
    public eo1(Integer num, T t, go1 go1) {
        this.a = num;
        if (t != null) {
            this.b = t;
            if (go1 != null) {
                this.c = go1;
                return;
            }
            throw new NullPointerException("Null priority");
        }
        throw new NullPointerException("Null payload");
    }

    @DexIgnore
    public Integer a() {
        return this.a;
    }

    @DexIgnore
    public T b() {
        return this.b;
    }

    @DexIgnore
    public go1 c() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fo1)) {
            return false;
        }
        fo1 fo1 = (fo1) obj;
        Integer num = this.a;
        if (num != null ? num.equals(fo1.a()) : fo1.a() == null) {
            if (!this.b.equals(fo1.b()) || !this.c.equals(fo1.c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        Integer num = this.a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.b.hashCode()) * 1000003) ^ this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Event{code=" + this.a + ", payload=" + this.b + ", priority=" + this.c + "}";
    }
}
