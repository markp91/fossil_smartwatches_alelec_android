package com.fossil;

import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface mv6 extends nv6 {
    @DexIgnore
    String d();

    @DexIgnore
    void writeTo(OutputStream outputStream) throws IOException;
}
