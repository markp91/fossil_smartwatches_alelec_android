package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeEditFragment;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import com.portfolio.platform.uirenew.home.customize.diana.theme.CustomizeThemePresenter;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class a45 implements MembersInjector<DianaCustomizeEditFragment> {
    @DexIgnore
    public static void a(DianaCustomizeEditFragment dianaCustomizeEditFragment, ComplicationsPresenter complicationsPresenter) {
        dianaCustomizeEditFragment.t = complicationsPresenter;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditFragment dianaCustomizeEditFragment, WatchAppsPresenter watchAppsPresenter) {
        dianaCustomizeEditFragment.u = watchAppsPresenter;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditFragment dianaCustomizeEditFragment, CustomizeThemePresenter customizeThemePresenter) {
        dianaCustomizeEditFragment.v = customizeThemePresenter;
    }

    @DexIgnore
    public static void a(DianaCustomizeEditFragment dianaCustomizeEditFragment, w04 w04) {
        dianaCustomizeEditFragment.w = w04;
    }
}
