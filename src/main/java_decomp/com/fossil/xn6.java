package com.fossil;

import android.os.Handler;
import android.os.Looper;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xn6 extends yn6 implements tl6 {
    @DexIgnore
    public volatile xn6 _immediate;
    @DexIgnore
    public /* final */ xn6 a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ boolean d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements am6 {
        @DexIgnore
        public /* final */ /* synthetic */ xn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ Runnable b;

        @DexIgnore
        public a(xn6 xn6, Runnable runnable) {
            this.a = xn6;
            this.b = runnable;
        }

        @DexIgnore
        public void dispose() {
            this.a.b.removeCallbacks(this.b);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ xn6 a;
        @DexIgnore
        public /* final */ /* synthetic */ lk6 b;

        @DexIgnore
        public b(xn6 xn6, lk6 lk6) {
            this.a = xn6;
            this.b = lk6;
        }

        @DexIgnore
        public final void run() {
            this.b.a((dl6) this.a, cd6.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<Throwable, cd6> {
        @DexIgnore
        public /* final */ /* synthetic */ Runnable $block;
        @DexIgnore
        public /* final */ /* synthetic */ xn6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(xn6 xn6, Runnable runnable) {
            super(1);
            this.this$0 = xn6;
            this.$block = runnable;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            invoke((Throwable) obj);
            return cd6.a;
        }

        @DexIgnore
        public final void invoke(Throwable th) {
            this.this$0.b.removeCallbacks(this.$block);
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public xn6(Handler handler, String str, boolean z) {
        super((qg6) null);
        xn6 xn6 = null;
        this.b = handler;
        this.c = str;
        this.d = z;
        this._immediate = this.d ? this : xn6;
        xn6 xn62 = this._immediate;
        if (xn62 == null) {
            xn62 = new xn6(this.b, this.c, true);
            this._immediate = xn62;
        }
        this.a = xn62;
    }

    @DexIgnore
    public boolean b(af6 af6) {
        wg6.b(af6, "context");
        return !this.d || (wg6.a((Object) Looper.myLooper(), (Object) this.b.getLooper()) ^ true);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return (obj instanceof xn6) && ((xn6) obj).b == this.b;
    }

    @DexIgnore
    public int hashCode() {
        return System.identityHashCode(this.b);
    }

    @DexIgnore
    public String toString() {
        String str = this.c;
        if (str == null) {
            String handler = this.b.toString();
            wg6.a((Object) handler, "handler.toString()");
            return handler;
        } else if (!this.d) {
            return str;
        } else {
            return this.c + " [immediate]";
        }
    }

    @DexIgnore
    public void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        this.b.post(runnable);
    }

    @DexIgnore
    public xn6 o() {
        return this.a;
    }

    @DexIgnore
    public am6 a(long j, Runnable runnable) {
        wg6.b(runnable, "block");
        this.b.postDelayed(runnable, ci6.b(j, 4611686018427387903L));
        return new a(this, runnable);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public xn6(Handler handler, String str) {
        this(handler, str, false);
        wg6.b(handler, "handler");
    }

    @DexIgnore
    public void a(long j, lk6<? super cd6> lk6) {
        wg6.b(lk6, "continuation");
        b bVar = new b(this, lk6);
        this.b.postDelayed(bVar, ci6.b(j, 4611686018427387903L));
        lk6.b((hg6<? super Throwable, cd6>) new c(this, bVar));
    }
}
