package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le {
    @DexIgnore
    public me a;

    @DexIgnore
    public le(String str, int i, int i2) {
        if (Build.VERSION.SDK_INT >= 28) {
            this.a = new ne(str, i, i2);
        } else {
            this.a = new oe(str, i, i2);
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof le)) {
            return false;
        }
        return this.a.equals(((le) obj).a);
    }

    @DexIgnore
    public int hashCode() {
        return this.a.hashCode();
    }
}
