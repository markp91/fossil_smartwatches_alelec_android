package com.fossil;

import com.fossil.NotificationAppHelper;
import com.fossil.d15;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.portfolio.platform.CoroutineUseCase;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.util.NotificationAppHelper$buildNotificationAppFilters$2$otherAppsDefer$1", f = "NotificationAppHelper.kt", l = {143}, m = "invokeSuspend")
public final class vx5$a$b extends sf6 implements ig6<il6, xe6<? super List<AppNotificationFilter>>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ boolean $isAllAppToggleEnabled;
    @DexIgnore
    public /* final */ /* synthetic */ List $notificationAllFilterList;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationAppHelper.a this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public vx5$a$b(NotificationAppHelper.a aVar, List list, boolean z, xe6 xe6) {
        super(2, xe6);
        this.this$0 = aVar;
        this.$notificationAllFilterList = list;
        this.$isAllAppToggleEnabled = z;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        vx5$a$b vx5_a_b = new vx5$a$b(this.this$0, this.$notificationAllFilterList, this.$isAllAppToggleEnabled, xe6);
        vx5_a_b.p$ = (il6) obj;
        return vx5_a_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((vx5$a$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v1, types: [com.fossil.d15, com.portfolio.platform.CoroutineUseCase] */
    public final Object invokeSuspend(Object obj) {
        List list;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            List arrayList = new ArrayList();
            Object r3 = this.this$0.$getApps;
            this.L$0 = il6;
            this.L$1 = arrayList;
            this.label = 1;
            obj = n24.a(r3, null, this);
            if (obj == a) {
                return a;
            }
            list = arrayList;
        } else if (i == 1) {
            list = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
        if (cVar instanceof d15.a) {
            this.$notificationAllFilterList.addAll(ux5.a(((d15.a) cVar).a(), this.$isAllAppToggleEnabled));
        }
        return list;
    }
}
