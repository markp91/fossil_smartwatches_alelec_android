package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import java.io.FileDescriptor;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ae<D> {
    @DexIgnore
    public boolean mAbandoned; // = false;
    @DexIgnore
    public boolean mContentChanged; // = false;
    @DexIgnore
    public Context mContext;
    @DexIgnore
    public int mId;
    @DexIgnore
    public c<D> mListener;
    @DexIgnore
    public b<D> mOnLoadCanceledListener;
    @DexIgnore
    public boolean mProcessingChange; // = false;
    @DexIgnore
    public boolean mReset; // = true;
    @DexIgnore
    public boolean mStarted; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends ContentObserver {
        @DexIgnore
        public a() {
            super(new Handler());
        }

        @DexIgnore
        public boolean deliverSelfNotifications() {
            return true;
        }

        @DexIgnore
        public void onChange(boolean z) {
            ae.this.onContentChanged();
        }
    }

    @DexIgnore
    public interface b<D> {
        @DexIgnore
        void a(ae<D> aeVar);
    }

    @DexIgnore
    public interface c<D> {
        @DexIgnore
        void a(ae<D> aeVar, D d);
    }

    @DexIgnore
    public ae(Context context) {
        this.mContext = context.getApplicationContext();
    }

    @DexIgnore
    public void abandon() {
        this.mAbandoned = true;
        onAbandon();
    }

    @DexIgnore
    public boolean cancelLoad() {
        return onCancelLoad();
    }

    @DexIgnore
    public void commitContentChanged() {
        this.mProcessingChange = false;
    }

    @DexIgnore
    public String dataToString(D d) {
        StringBuilder sb = new StringBuilder(64);
        r8.a(d, sb);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void deliverCancellation() {
        b<D> bVar = this.mOnLoadCanceledListener;
        if (bVar != null) {
            bVar.a(this);
        }
    }

    @DexIgnore
    public void deliverResult(D d) {
        c<D> cVar = this.mListener;
        if (cVar != null) {
            cVar.a(this, d);
        }
    }

    @DexIgnore
    @Deprecated
    public void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.print(str);
        printWriter.print("mId=");
        printWriter.print(this.mId);
        printWriter.print(" mListener=");
        printWriter.println(this.mListener);
        if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
            printWriter.print(str);
            printWriter.print("mStarted=");
            printWriter.print(this.mStarted);
            printWriter.print(" mContentChanged=");
            printWriter.print(this.mContentChanged);
            printWriter.print(" mProcessingChange=");
            printWriter.println(this.mProcessingChange);
        }
        if (this.mAbandoned || this.mReset) {
            printWriter.print(str);
            printWriter.print("mAbandoned=");
            printWriter.print(this.mAbandoned);
            printWriter.print(" mReset=");
            printWriter.println(this.mReset);
        }
    }

    @DexIgnore
    public void forceLoad() {
        onForceLoad();
    }

    @DexIgnore
    public Context getContext() {
        return this.mContext;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public boolean isAbandoned() {
        return this.mAbandoned;
    }

    @DexIgnore
    public boolean isReset() {
        return this.mReset;
    }

    @DexIgnore
    public boolean isStarted() {
        return this.mStarted;
    }

    @DexIgnore
    public void onAbandon() {
    }

    @DexIgnore
    public boolean onCancelLoad() {
        throw null;
    }

    @DexIgnore
    public void onContentChanged() {
        if (this.mStarted) {
            forceLoad();
        } else {
            this.mContentChanged = true;
        }
    }

    @DexIgnore
    public void onForceLoad() {
    }

    @DexIgnore
    public void onReset() {
    }

    @DexIgnore
    public void onStartLoading() {
    }

    @DexIgnore
    public void onStopLoading() {
    }

    @DexIgnore
    public void registerListener(int i, c<D> cVar) {
        if (this.mListener == null) {
            this.mListener = cVar;
            this.mId = i;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void registerOnLoadCanceledListener(b<D> bVar) {
        if (this.mOnLoadCanceledListener == null) {
            this.mOnLoadCanceledListener = bVar;
            return;
        }
        throw new IllegalStateException("There is already a listener registered");
    }

    @DexIgnore
    public void reset() {
        onReset();
        this.mReset = true;
        this.mStarted = false;
        this.mAbandoned = false;
        this.mContentChanged = false;
        this.mProcessingChange = false;
    }

    @DexIgnore
    public void rollbackContentChanged() {
        if (this.mProcessingChange) {
            onContentChanged();
        }
    }

    @DexIgnore
    public final void startLoading() {
        this.mStarted = true;
        this.mReset = false;
        this.mAbandoned = false;
        onStartLoading();
    }

    @DexIgnore
    public void stopLoading() {
        this.mStarted = false;
        onStopLoading();
    }

    @DexIgnore
    public boolean takeContentChanged() {
        boolean z = this.mContentChanged;
        this.mContentChanged = false;
        this.mProcessingChange |= z;
        return z;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder(64);
        r8.a(this, sb);
        sb.append(" id=");
        sb.append(this.mId);
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public void unregisterListener(c<D> cVar) {
        c<D> cVar2 = this.mListener;
        if (cVar2 == null) {
            throw new IllegalStateException("No listener register");
        } else if (cVar2 == cVar) {
            this.mListener = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }

    @DexIgnore
    public void unregisterOnLoadCanceledListener(b<D> bVar) {
        b<D> bVar2 = this.mOnLoadCanceledListener;
        if (bVar2 == null) {
            throw new IllegalStateException("No listener register");
        } else if (bVar2 == bVar) {
            this.mOnLoadCanceledListener = null;
        } else {
            throw new IllegalArgumentException("Attempting to unregister the wrong listener");
        }
    }
}
