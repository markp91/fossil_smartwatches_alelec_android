package com.fossil;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa3 extends SSLSocket {
    @DexIgnore
    public /* final */ SSLSocket a;

    @DexIgnore
    public oa3(pa3 pa3, SSLSocket sSLSocket) {
        this.a = sSLSocket;
    }

    @DexIgnore
    public final void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.a.addHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    public final void bind(SocketAddress socketAddress) throws IOException {
        this.a.bind(socketAddress);
    }

    @DexIgnore
    public final synchronized void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public final void connect(SocketAddress socketAddress) throws IOException {
        this.a.connect(socketAddress);
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        return this.a.equals(obj);
    }

    @DexIgnore
    public final SocketChannel getChannel() {
        return this.a.getChannel();
    }

    @DexIgnore
    public final boolean getEnableSessionCreation() {
        return this.a.getEnableSessionCreation();
    }

    @DexIgnore
    public final String[] getEnabledCipherSuites() {
        return this.a.getEnabledCipherSuites();
    }

    @DexIgnore
    public final String[] getEnabledProtocols() {
        return this.a.getEnabledProtocols();
    }

    @DexIgnore
    public final InetAddress getInetAddress() {
        return this.a.getInetAddress();
    }

    @DexIgnore
    public final InputStream getInputStream() throws IOException {
        return this.a.getInputStream();
    }

    @DexIgnore
    public final boolean getKeepAlive() throws SocketException {
        return this.a.getKeepAlive();
    }

    @DexIgnore
    public final InetAddress getLocalAddress() {
        return this.a.getLocalAddress();
    }

    @DexIgnore
    public final int getLocalPort() {
        return this.a.getLocalPort();
    }

    @DexIgnore
    public final SocketAddress getLocalSocketAddress() {
        return this.a.getLocalSocketAddress();
    }

    @DexIgnore
    public final boolean getNeedClientAuth() {
        return this.a.getNeedClientAuth();
    }

    @DexIgnore
    public final boolean getOOBInline() throws SocketException {
        return this.a.getOOBInline();
    }

    @DexIgnore
    public final OutputStream getOutputStream() throws IOException {
        return this.a.getOutputStream();
    }

    @DexIgnore
    public final int getPort() {
        return this.a.getPort();
    }

    @DexIgnore
    public final synchronized int getReceiveBufferSize() throws SocketException {
        return this.a.getReceiveBufferSize();
    }

    @DexIgnore
    public final SocketAddress getRemoteSocketAddress() {
        return this.a.getRemoteSocketAddress();
    }

    @DexIgnore
    public final boolean getReuseAddress() throws SocketException {
        return this.a.getReuseAddress();
    }

    @DexIgnore
    public final synchronized int getSendBufferSize() throws SocketException {
        return this.a.getSendBufferSize();
    }

    @DexIgnore
    public final SSLSession getSession() {
        return this.a.getSession();
    }

    @DexIgnore
    public final int getSoLinger() throws SocketException {
        return this.a.getSoLinger();
    }

    @DexIgnore
    public final synchronized int getSoTimeout() throws SocketException {
        return this.a.getSoTimeout();
    }

    @DexIgnore
    public final String[] getSupportedCipherSuites() {
        return this.a.getSupportedCipherSuites();
    }

    @DexIgnore
    public final String[] getSupportedProtocols() {
        return this.a.getSupportedProtocols();
    }

    @DexIgnore
    public final boolean getTcpNoDelay() throws SocketException {
        return this.a.getTcpNoDelay();
    }

    @DexIgnore
    public final int getTrafficClass() throws SocketException {
        return this.a.getTrafficClass();
    }

    @DexIgnore
    public final boolean getUseClientMode() {
        return this.a.getUseClientMode();
    }

    @DexIgnore
    public final boolean getWantClientAuth() {
        return this.a.getWantClientAuth();
    }

    @DexIgnore
    public final boolean isBound() {
        return this.a.isBound();
    }

    @DexIgnore
    public final boolean isClosed() {
        return this.a.isClosed();
    }

    @DexIgnore
    public final boolean isConnected() {
        return this.a.isConnected();
    }

    @DexIgnore
    public final boolean isInputShutdown() {
        return this.a.isInputShutdown();
    }

    @DexIgnore
    public final boolean isOutputShutdown() {
        return this.a.isOutputShutdown();
    }

    @DexIgnore
    public final void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
        this.a.removeHandshakeCompletedListener(handshakeCompletedListener);
    }

    @DexIgnore
    public final void sendUrgentData(int i) throws IOException {
        this.a.sendUrgentData(i);
    }

    @DexIgnore
    public final void setEnableSessionCreation(boolean z) {
        this.a.setEnableSessionCreation(z);
    }

    @DexIgnore
    public final void setEnabledCipherSuites(String[] strArr) {
        this.a.setEnabledCipherSuites(strArr);
    }

    @DexIgnore
    public final void setEnabledProtocols(String[] strArr) {
        if (strArr != null && Arrays.asList(strArr).contains("SSLv3")) {
            ArrayList arrayList = new ArrayList(Arrays.asList(this.a.getEnabledProtocols()));
            if (arrayList.size() > 1) {
                arrayList.remove("SSLv3");
            }
            strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
        }
        this.a.setEnabledProtocols(strArr);
    }

    @DexIgnore
    public final void setKeepAlive(boolean z) throws SocketException {
        this.a.setKeepAlive(z);
    }

    @DexIgnore
    public final void setNeedClientAuth(boolean z) {
        this.a.setNeedClientAuth(z);
    }

    @DexIgnore
    public final void setOOBInline(boolean z) throws SocketException {
        this.a.setOOBInline(z);
    }

    @DexIgnore
    public final void setPerformancePreferences(int i, int i2, int i3) {
        this.a.setPerformancePreferences(i, i2, i3);
    }

    @DexIgnore
    public final synchronized void setReceiveBufferSize(int i) throws SocketException {
        this.a.setReceiveBufferSize(i);
    }

    @DexIgnore
    public final void setReuseAddress(boolean z) throws SocketException {
        this.a.setReuseAddress(z);
    }

    @DexIgnore
    public final synchronized void setSendBufferSize(int i) throws SocketException {
        this.a.setSendBufferSize(i);
    }

    @DexIgnore
    public final void setSoLinger(boolean z, int i) throws SocketException {
        this.a.setSoLinger(z, i);
    }

    @DexIgnore
    public final synchronized void setSoTimeout(int i) throws SocketException {
        this.a.setSoTimeout(i);
    }

    @DexIgnore
    public final void setTcpNoDelay(boolean z) throws SocketException {
        this.a.setTcpNoDelay(z);
    }

    @DexIgnore
    public final void setTrafficClass(int i) throws SocketException {
        this.a.setTrafficClass(i);
    }

    @DexIgnore
    public final void setUseClientMode(boolean z) {
        this.a.setUseClientMode(z);
    }

    @DexIgnore
    public final void setWantClientAuth(boolean z) {
        this.a.setWantClientAuth(z);
    }

    @DexIgnore
    public final void shutdownInput() throws IOException {
        this.a.shutdownInput();
    }

    @DexIgnore
    public final void shutdownOutput() throws IOException {
        this.a.shutdownOutput();
    }

    @DexIgnore
    public final void startHandshake() throws IOException {
        this.a.startHandshake();
    }

    @DexIgnore
    public final String toString() {
        return this.a.toString();
    }

    @DexIgnore
    public final void connect(SocketAddress socketAddress, int i) throws IOException {
        this.a.connect(socketAddress, i);
    }
}
