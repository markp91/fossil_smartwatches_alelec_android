package com.fossil;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zv1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<R extends ew1> extends BasePendingResult<R> {
        @DexIgnore
        public /* final */ R q;

        @DexIgnore
        public a(wv1 wv1, R r) {
            super(wv1);
            this.q = r;
        }

        @DexIgnore
        public final R a(Status status) {
            return this.q;
        }
    }

    @DexIgnore
    public static yv1<Status> a(Status status, wv1 wv1) {
        w12.a(status, (Object) "Result must not be null");
        ax1 ax1 = new ax1(wv1);
        ax1.a(status);
        return ax1;
    }

    @DexIgnore
    public static <R extends ew1> yv1<R> a(R r, wv1 wv1) {
        w12.a(r, (Object) "Result must not be null");
        w12.a(!r.o().F(), (Object) "Status code must not be SUCCESS");
        a aVar = new a(wv1, r);
        aVar.a(r);
        return aVar;
    }
}
