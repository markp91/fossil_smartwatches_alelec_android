package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j11 implements Parcelable.Creator<e31> {
    @DexIgnore
    public /* synthetic */ j11(qg6 qg6) {
    }

    @DexIgnore
    public e31 createFromParcel(Parcel parcel) {
        return new e31(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new e31[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m27createFromParcel(Parcel parcel) {
        return new e31(parcel, (qg6) null);
    }
}
