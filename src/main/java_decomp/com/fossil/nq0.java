package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum nq0 {
    PRE_SHARED_KEY((byte) 0),
    SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY((byte) 1),
    SIXTEEN_BYTES_LSB_ECDH_SHARED_SECRET_KEY((byte) 2);
    
    @DexIgnore
    public static /* final */ vo0 f; // = null;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        f = new vo0((qg6) null);
    }
    */

    @DexIgnore
    public nq0(byte b) {
        this.a = b;
    }
}
