package com.fossil;

import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rl {
    @DexIgnore
    public static /* final */ String a; // = tl.a("InputMerger");

    @DexIgnore
    public static rl a(String str) {
        try {
            return (rl) Class.forName(str).newInstance();
        } catch (Exception e) {
            tl a2 = tl.a();
            String str2 = a;
            a2.b(str2, "Trouble instantiating + " + str, e);
            return null;
        }
    }

    @DexIgnore
    public abstract pl a(List<pl> list);
}
