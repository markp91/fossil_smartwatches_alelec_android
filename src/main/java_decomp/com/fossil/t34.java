package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.portfolio.platform.data.ActivityIntensities;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t34 {
    @DexIgnore
    public final String a(ActivityIntensities activityIntensities) {
        wg6.b(activityIntensities, "activityIntensities");
        try {
            return new Gson().a(activityIntensities);
        } catch (Exception unused) {
            return "";
        }
    }

    @DexIgnore
    public final ActivityIntensities a(String str) {
        wg6.b(str, "data");
        if (TextUtils.isEmpty(str)) {
            return new ActivityIntensities();
        }
        try {
            Object a = new Gson().a(str, ActivityIntensities.class);
            wg6.a(a, "Gson().fromJson(data, Ac\u2026yIntensities::class.java)");
            return (ActivityIntensities) a;
        } catch (Exception unused) {
            return new ActivityIntensities();
        }
    }
}
