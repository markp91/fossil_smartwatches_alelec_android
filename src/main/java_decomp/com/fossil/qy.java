package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.manager.SupportRequestManagerFragment;
import com.facebook.internal.FileLruCache;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qy implements Handler.Callback {
    @DexIgnore
    public static /* final */ b i; // = new a();
    @DexIgnore
    public volatile fr a;
    @DexIgnore
    public /* final */ Map<FragmentManager, py> b; // = new HashMap();
    @DexIgnore
    public /* final */ Map<androidx.fragment.app.FragmentManager, SupportRequestManagerFragment> c; // = new HashMap();
    @DexIgnore
    public /* final */ Handler d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ p4<View, Fragment> f; // = new p4<>();
    @DexIgnore
    public /* final */ p4<View, android.app.Fragment> g; // = new p4<>();
    @DexIgnore
    public /* final */ Bundle h; // = new Bundle();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b {
        @DexIgnore
        public fr a(wq wqVar, my myVar, ry ryVar, Context context) {
            return new fr(wqVar, myVar, ryVar, context);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        fr a(wq wqVar, my myVar, ry ryVar, Context context);
    }

    @DexIgnore
    public qy(b bVar) {
        this.e = bVar == null ? i : bVar;
        this.d = new Handler(Looper.getMainLooper(), this);
    }

    @DexIgnore
    public static Activity c(Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return c(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @DexIgnore
    public static boolean d(Context context) {
        Activity c2 = c(context);
        return c2 == null || !c2.isFinishing();
    }

    @DexIgnore
    public fr a(Context context) {
        if (context != null) {
            if (r00.d() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return a((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return a((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    ContextWrapper contextWrapper = (ContextWrapper) context;
                    if (contextWrapper.getBaseContext().getApplicationContext() != null) {
                        return a(contextWrapper.getBaseContext());
                    }
                }
            }
            return b(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    @DexIgnore
    public final fr b(Context context) {
        if (this.a == null) {
            synchronized (this) {
                if (this.a == null) {
                    this.a = this.e.a(wq.a(context.getApplicationContext()), new gy(), new ly(), context.getApplicationContext());
                }
            }
        }
        return this.a;
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        Object obj;
        int i2 = message.what;
        Object obj2 = null;
        boolean z = true;
        if (i2 == 1) {
            obj2 = (FragmentManager) message.obj;
            obj = this.b.remove(obj2);
        } else if (i2 != 2) {
            z = false;
            obj = null;
        } else {
            obj2 = (androidx.fragment.app.FragmentManager) message.obj;
            obj = this.c.remove(obj2);
        }
        if (z && obj == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj2);
        }
        return z;
    }

    @DexIgnore
    @TargetApi(17)
    public static void c(Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @DexIgnore
    @Deprecated
    public final void b(FragmentManager fragmentManager, p4<View, android.app.Fragment> p4Var) {
        int i2 = 0;
        while (true) {
            int i3 = i2 + 1;
            this.h.putInt(FileLruCache.HEADER_CACHEKEY_KEY, i2);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.h, FileLruCache.HEADER_CACHEKEY_KEY);
            } catch (Exception unused) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    p4Var.put(fragment.getView(), fragment);
                    if (Build.VERSION.SDK_INT >= 17) {
                        a(fragment.getChildFragmentManager(), p4Var);
                    }
                }
                i2 = i3;
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public fr a(FragmentActivity fragmentActivity) {
        if (r00.c()) {
            return a(fragmentActivity.getApplicationContext());
        }
        c((Activity) fragmentActivity);
        return a((Context) fragmentActivity, fragmentActivity.getSupportFragmentManager(), (Fragment) null, d(fragmentActivity));
    }

    @DexIgnore
    public fr a(Fragment fragment) {
        q00.a(fragment.getContext(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (r00.c()) {
            return a(fragment.getContext().getApplicationContext());
        }
        return a(fragment.getContext(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @DexIgnore
    @Deprecated
    public py b(Activity activity) {
        return a(activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public fr a(Activity activity) {
        if (r00.c()) {
            return a(activity.getApplicationContext());
        }
        c(activity);
        return a((Context) activity, activity.getFragmentManager(), (android.app.Fragment) null, d(activity));
    }

    @DexIgnore
    public fr a(View view) {
        if (r00.c()) {
            return a(view.getContext().getApplicationContext());
        }
        q00.a(view);
        q00.a(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity c2 = c(view.getContext());
        if (c2 == null) {
            return a(view.getContext().getApplicationContext());
        }
        if (c2 instanceof FragmentActivity) {
            FragmentActivity fragmentActivity = (FragmentActivity) c2;
            Fragment a2 = a(view, fragmentActivity);
            return a2 != null ? a(a2) : a(fragmentActivity);
        }
        android.app.Fragment a3 = a(view, c2);
        if (a3 == null) {
            return a(c2);
        }
        return a(a3);
    }

    @DexIgnore
    public static void a(Collection<Fragment> collection, Map<View, Fragment> map) {
        if (collection != null) {
            for (Fragment next : collection) {
                if (!(next == null || next.getView() == null)) {
                    map.put(next.getView(), next);
                    a((Collection<Fragment>) next.getChildFragmentManager().v(), map);
                }
            }
        }
    }

    @DexIgnore
    public final Fragment a(View view, FragmentActivity fragmentActivity) {
        this.f.clear();
        a((Collection<Fragment>) fragmentActivity.getSupportFragmentManager().v(), (Map<View, Fragment>) this.f);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.f.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.f.clear();
        return fragment;
    }

    @DexIgnore
    @Deprecated
    public final android.app.Fragment a(View view, Activity activity) {
        this.g.clear();
        a(activity.getFragmentManager(), this.g);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.g.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.g.clear();
        return fragment;
    }

    @DexIgnore
    @TargetApi(26)
    @Deprecated
    public final void a(FragmentManager fragmentManager, p4<View, android.app.Fragment> p4Var) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment next : fragmentManager.getFragments()) {
                if (next.getView() != null) {
                    p4Var.put(next.getView(), next);
                    a(next.getChildFragmentManager(), p4Var);
                }
            }
            return;
        }
        b(fragmentManager, p4Var);
    }

    @DexIgnore
    @TargetApi(17)
    @Deprecated
    public fr a(android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (r00.c() || Build.VERSION.SDK_INT < 17) {
            return a(fragment.getActivity().getApplicationContext());
        } else {
            return a((Context) fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @DexIgnore
    public final py a(FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        py pyVar = (py) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (pyVar == null && (pyVar = this.b.get(fragmentManager)) == null) {
            pyVar = new py();
            pyVar.b(fragment);
            if (z) {
                pyVar.b().b();
            }
            this.b.put(fragmentManager, pyVar);
            fragmentManager.beginTransaction().add(pyVar, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.d.obtainMessage(1, fragmentManager).sendToTarget();
        }
        return pyVar;
    }

    @DexIgnore
    @Deprecated
    public final fr a(Context context, FragmentManager fragmentManager, android.app.Fragment fragment, boolean z) {
        py a2 = a(fragmentManager, fragment, z);
        fr d2 = a2.d();
        if (d2 != null) {
            return d2;
        }
        fr a3 = this.e.a(wq.a(context), a2.b(), a2.e(), context);
        a2.a(a3);
        return a3;
    }

    @DexIgnore
    public SupportRequestManagerFragment a(Context context, androidx.fragment.app.FragmentManager fragmentManager) {
        return a(fragmentManager, (Fragment) null, d(context));
    }

    @DexIgnore
    public final SupportRequestManagerFragment a(androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) fragmentManager.b("com.bumptech.glide.manager");
        if (supportRequestManagerFragment == null && (supportRequestManagerFragment = this.c.get(fragmentManager)) == null) {
            supportRequestManagerFragment = new SupportRequestManagerFragment();
            supportRequestManagerFragment.b(fragment);
            if (z) {
                supportRequestManagerFragment.e1().b();
            }
            this.c.put(fragmentManager, supportRequestManagerFragment);
            hc b2 = fragmentManager.b();
            b2.a((Fragment) supportRequestManagerFragment, "com.bumptech.glide.manager");
            b2.b();
            this.d.obtainMessage(2, fragmentManager).sendToTarget();
        }
        return supportRequestManagerFragment;
    }

    @DexIgnore
    public final fr a(Context context, androidx.fragment.app.FragmentManager fragmentManager, Fragment fragment, boolean z) {
        SupportRequestManagerFragment a2 = a(fragmentManager, fragment, z);
        fr g1 = a2.g1();
        if (g1 != null) {
            return g1;
        }
        fr a3 = this.e.a(wq.a(context), a2.e1(), a2.h1(), context);
        a2.a(a3);
        return a3;
    }
}
