package com.fossil;

import com.misfit.frameworks.common.enums.Gesture;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ok4 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[Gesture.values().length];

    /*
    static {
        a[Gesture.SAM_BT1_SINGLE_PRESS.ordinal()] = 1;
        a[Gesture.SAM_BT1_SINGLE_PRESS_AND_HOLD.ordinal()] = 2;
        a[Gesture.SAM_BT1_DOUBLE_PRESS.ordinal()] = 3;
        a[Gesture.SAM_BT1_DOUBLE_PRESS_AND_HOLD.ordinal()] = 4;
        a[Gesture.SAM_BT1_TRIPLE_PRESS.ordinal()] = 5;
        a[Gesture.SAM_BT1_TRIPLE_PRESS_AND_HOLD.ordinal()] = 6;
        a[Gesture.SAM_BT1_PRESSED.ordinal()] = 7;
        a[Gesture.SAM_BT2_SINGLE_PRESS.ordinal()] = 8;
        a[Gesture.SAM_BT2_SINGLE_PRESS_AND_HOLD.ordinal()] = 9;
        a[Gesture.SAM_BT2_DOUBLE_PRESS.ordinal()] = 10;
        a[Gesture.SAM_BT2_DOUBLE_PRESS_AND_HOLD.ordinal()] = 11;
        a[Gesture.SAM_BT2_TRIPLE_PRESS.ordinal()] = 12;
        a[Gesture.SAM_BT2_TRIPLE_PRESS_AND_HOLD.ordinal()] = 13;
        a[Gesture.SAM_BT2_PRESSED.ordinal()] = 14;
        a[Gesture.SAM_BT3_SINGLE_PRESS.ordinal()] = 15;
        a[Gesture.SAM_BT3_SINGLE_PRESS_AND_HOLD.ordinal()] = 16;
        a[Gesture.SAM_BT3_DOUBLE_PRESS.ordinal()] = 17;
        a[Gesture.SAM_BT3_DOUBLE_PRESS_AND_HOLD.ordinal()] = 18;
        a[Gesture.SAM_BT3_TRIPLE_PRESS.ordinal()] = 19;
        a[Gesture.SAM_BT3_TRIPLE_PRESS_AND_HOLD.ordinal()] = 20;
        a[Gesture.SAM_BT3_PRESSED.ordinal()] = 21;
    }
    */
}
