package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface af6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.af6$a$a")
        /* renamed from: com.fossil.af6$a$a  reason: collision with other inner class name */
        public static final class C0002a extends xg6 implements ig6<af6, b, af6> {
            @DexIgnore
            public static /* final */ C0002a INSTANCE; // = new C0002a();

            @DexIgnore
            public C0002a() {
                super(2);
            }

            @DexIgnore
            public final af6 invoke(af6 af6, b bVar) {
                wg6.b(af6, "acc");
                wg6.b(bVar, "element");
                af6 minusKey = af6.minusKey(bVar.getKey());
                if (minusKey == bf6.INSTANCE) {
                    return bVar;
                }
                ye6 ye6 = (ye6) minusKey.get(ye6.l);
                if (ye6 == null) {
                    return new we6(minusKey, bVar);
                }
                af6 minusKey2 = minusKey.minusKey(ye6.l);
                if (minusKey2 == bf6.INSTANCE) {
                    return new we6(bVar, ye6);
                }
                return new we6(new we6(minusKey2, bVar), ye6);
            }
        }

        @DexIgnore
        public static af6 a(af6 af6, af6 af62) {
            wg6.b(af62, "context");
            return af62 == bf6.INSTANCE ? af6 : (af6) af62.fold(af6, C0002a.INSTANCE);
        }
    }

    @DexIgnore
    public interface b extends af6 {

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public static <E extends b> E a(b bVar, c<E> cVar) {
                wg6.b(cVar, "key");
                if (!wg6.a((Object) bVar.getKey(), (Object) cVar)) {
                    return null;
                }
                if (bVar != null) {
                    return bVar;
                }
                throw new rc6("null cannot be cast to non-null type E");
            }

            @DexIgnore
            public static af6 a(b bVar, af6 af6) {
                wg6.b(af6, "context");
                return a.a(bVar, af6);
            }

            @DexIgnore
            public static af6 b(b bVar, c<?> cVar) {
                wg6.b(cVar, "key");
                return wg6.a((Object) bVar.getKey(), (Object) cVar) ? bf6.INSTANCE : bVar;
            }

            @DexIgnore
            public static <R> R a(b bVar, R r, ig6<? super R, ? super b, ? extends R> ig6) {
                wg6.b(ig6, "operation");
                return ig6.invoke(r, bVar);
            }
        }

        @DexIgnore
        <E extends b> E get(c<E> cVar);

        @DexIgnore
        c<?> getKey();
    }

    @DexIgnore
    public interface c<E extends b> {
    }

    @DexIgnore
    <R> R fold(R r, ig6<? super R, ? super b, ? extends R> ig6);

    @DexIgnore
    <E extends b> E get(c<E> cVar);

    @DexIgnore
    af6 minusKey(c<?> cVar);

    @DexIgnore
    af6 plus(af6 af6);
}
