package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import com.facebook.GraphRequest;
import com.facebook.devicerequests.internal.DeviceRequestsHelper;
import com.facebook.stetho.inspector.network.DecompressionHelper;
import com.fossil.cp1;
import com.fossil.mo1;
import com.fossil.no1;
import com.fossil.np1;
import com.fossil.so1;
import com.fossil.uo1;
import com.fossil.wo1;
import com.fossil.yo1;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gp1 implements lq1 {
    @DexIgnore
    public /* final */ ConnectivityManager a;
    @DexIgnore
    public /* final */ URL b; // = a(lo1.c);
    @DexIgnore
    public /* final */ zs1 c;
    @DexIgnore
    public /* final */ zs1 d;
    @DexIgnore
    public /* final */ int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ URL a;
        @DexIgnore
        public /* final */ so1 b;
        @DexIgnore
        public /* final */ String c;

        @DexIgnore
        public a(URL url, so1 so1, String str) {
            this.a = url;
            this.b = so1;
            this.c = str;
        }

        @DexIgnore
        public a a(URL url) {
            return new a(url, this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ int a;
        @DexIgnore
        public /* final */ URL b;
        @DexIgnore
        public /* final */ long c;

        @DexIgnore
        public b(int i, URL url, long j) {
            this.a = i;
            this.b = url;
            this.c = j;
        }
    }

    @DexIgnore
    public gp1(Context context, zs1 zs1, zs1 zs12) {
        this.a = (ConnectivityManager) context.getSystemService("connectivity");
        this.c = zs12;
        this.d = zs1;
        this.e = 40000;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x007b, code lost:
        if (com.fossil.cp1.c.zza(r0) != null) goto L_0x007f;
     */
    @DexIgnore
    public np1 a(np1 np1) {
        int i;
        int i2;
        NetworkInfo activeNetworkInfo = this.a.getActiveNetworkInfo();
        np1.a h = np1.h();
        h.a("sdk-version", Build.VERSION.SDK_INT);
        h.a(DeviceRequestsHelper.DEVICE_INFO_MODEL, Build.MODEL);
        h.a("hardware", Build.HARDWARE);
        h.a(DeviceRequestsHelper.DEVICE_INFO_DEVICE, Build.DEVICE);
        h.a("product", Build.PRODUCT);
        h.a("os-uild", Build.ID);
        h.a("manufacturer", Build.MANUFACTURER);
        h.a("fingerprint", Build.FINGERPRINT);
        Calendar.getInstance();
        h.a("tz-offset", (long) (TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000));
        if (activeNetworkInfo == null) {
            i = -1;
        } else {
            i = activeNetworkInfo.getType();
        }
        h.a("net-type", i);
        if (activeNetworkInfo != null) {
            i2 = activeNetworkInfo.getSubtype();
            if (i2 == -1) {
                i2 = 100;
            }
            h.a("mobile-subtype", i2);
            return h.a();
        }
        i2 = 0;
        h.a("mobile-subtype", i2);
        return h.a();
    }

    @DexIgnore
    public fq1 a(eq1 eq1) {
        HashMap hashMap = new HashMap();
        for (np1 next : eq1.a()) {
            String f = next.f();
            if (!hashMap.containsKey(f)) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(next);
                hashMap.put(f, arrayList);
            } else {
                ((List) hashMap.get(f)).add(next);
            }
        }
        so1.b k = so1.k();
        for (Map.Entry entry : hashMap.entrySet()) {
            np1 np1 = (np1) ((List) entry.getValue()).get(0);
            yo1.b k2 = yo1.k();
            k2.a(no1.a.zza);
            k2.a(this.d.a());
            k2.b(this.c.a());
            uo1.b l = uo1.l();
            l.a(uo1.c.ANDROID);
            mo1.b l2 = mo1.l();
            l2.a(np1.b("sdk-version"));
            l2.e(np1.a(DeviceRequestsHelper.DEVICE_INFO_MODEL));
            l2.c(np1.a("hardware"));
            l2.a(np1.a(DeviceRequestsHelper.DEVICE_INFO_DEVICE));
            l2.g(np1.a("product"));
            l2.f(np1.a("os-uild"));
            l2.d(np1.a("manufacturer"));
            l2.b(np1.a("fingerprint"));
            l.a((mo1) l2.build());
            k2.a((uo1) l.build());
            try {
                k2.a(Integer.valueOf((String) entry.getKey()).intValue());
            } catch (NumberFormatException unused) {
                k2.a((String) entry.getKey());
            }
            for (np1 np12 : (List) entry.getValue()) {
                wo1.b k3 = wo1.k();
                k3.a(np12.c());
                k3.b(np12.g());
                k3.c(np12.c("tz-offset"));
                k3.a(sw3.copyFrom(np12.e()));
                cp1.b l3 = cp1.l();
                l3.b(np12.b("net-type"));
                l3.a(np12.b("mobile-subtype"));
                k3.a(l3);
                if (np12.b() != null) {
                    k3.a(np12.b().intValue());
                }
                k2.a(k3);
            }
            k.a((yo1) k2.build());
        }
        so1 so1 = (so1) k.build();
        String str = null;
        URL url = this.b;
        if (eq1.b() != null) {
            try {
                lo1 a2 = lo1.a(eq1.b());
                if (a2.a() != null) {
                    str = a2.a();
                }
                if (a2.b() != null) {
                    url = a(a2.b());
                }
            } catch (IllegalArgumentException unused2) {
                return fq1.c();
            }
        }
        try {
            b bVar = (b) oq1.a(5, new a(url, so1, str), ep1.a(this), fp1.a());
            if (bVar.a == 200) {
                return fq1.a(bVar.c);
            }
            int i = bVar.a;
            if (i < 500) {
                if (i != 404) {
                    return fq1.c();
                }
            }
            return fq1.d();
        } catch (IOException e2) {
            mq1.a("CctTransportBackend", "Could not make request to the backend", (Throwable) e2);
            return fq1.d();
        }
    }

    @DexIgnore
    public static URL a(String str) {
        try {
            return new URL(str);
        } catch (MalformedURLException e2) {
            throw new IllegalArgumentException("Invalid url: " + str, e2);
        }
    }

    @DexIgnore
    public final b a(a aVar) throws IOException {
        GZIPOutputStream gZIPOutputStream;
        InputStream inputStream;
        mq1.a("CctTransportBackend", "Making request to: %s", (Object) aVar.a);
        HttpURLConnection httpURLConnection = (HttpURLConnection) aVar.a.openConnection();
        httpURLConnection.setConnectTimeout(30000);
        httpURLConnection.setReadTimeout(this.e);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setInstanceFollowRedirects(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty(GraphRequest.USER_AGENT_HEADER, String.format("datatransport/%s android/", new Object[]{"2.1.0"}));
        httpURLConnection.setRequestProperty(GraphRequest.CONTENT_ENCODING_HEADER, DecompressionHelper.GZIP_ENCODING);
        httpURLConnection.setRequestProperty("Content-Type", "application/x-protobuf");
        httpURLConnection.setRequestProperty("Accept-Encoding", DecompressionHelper.GZIP_ENCODING);
        String str = aVar.c;
        if (str != null) {
            httpURLConnection.setRequestProperty("X-Goog-Api-Key", str);
        }
        WritableByteChannel newChannel = Channels.newChannel(httpURLConnection.getOutputStream());
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
            aVar.b.a(gZIPOutputStream);
            gZIPOutputStream.close();
            newChannel.write(ByteBuffer.wrap(byteArrayOutputStream.toByteArray()));
            int responseCode = httpURLConnection.getResponseCode();
            StringBuilder sb = new StringBuilder();
            sb.append("Status Code: ");
            sb.append(responseCode);
            mq1.a("CctTransportBackend", sb.toString());
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Content-Type: ");
            sb2.append(httpURLConnection.getHeaderField("Content-Type"));
            mq1.a("CctTransportBackend", sb2.toString());
            StringBuilder sb3 = new StringBuilder();
            sb3.append("Content-Encoding: ");
            sb3.append(httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER));
            mq1.a("CctTransportBackend", sb3.toString());
            if (responseCode != 302) {
                if (responseCode != 301) {
                    if (responseCode != 200) {
                        b bVar = new b(responseCode, (URL) null, 0);
                        newChannel.close();
                        return bVar;
                    }
                    String headerField = httpURLConnection.getHeaderField(GraphRequest.CONTENT_ENCODING_HEADER);
                    if (headerField == null || !headerField.equals(DecompressionHelper.GZIP_ENCODING)) {
                        inputStream = httpURLConnection.getInputStream();
                    } else {
                        inputStream = new GZIPInputStream(httpURLConnection.getInputStream());
                    }
                    b bVar2 = new b(responseCode, (URL) null, ap1.a(inputStream).j());
                    inputStream.close();
                    newChannel.close();
                    return bVar2;
                }
            }
            b bVar3 = new b(responseCode, new URL(httpURLConnection.getHeaderField("Location")), 0);
            newChannel.close();
            return bVar3;
        } catch (Throwable th) {
            newChannel.close();
            throw th;
        }
    }

    @DexIgnore
    public static /* synthetic */ a a(a aVar, b bVar) {
        URL url = bVar.b;
        if (url == null) {
            return null;
        }
        mq1.a("CctTransportBackend", "Following redirect to: %s", (Object) url);
        return aVar.a(bVar.b);
    }
}
