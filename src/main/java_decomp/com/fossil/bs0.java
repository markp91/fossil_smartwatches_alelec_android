package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bs0 {
    ENABLE_MAINTAINING_CONNECTION,
    APP_DISCONNECT,
    SET_SECRET_KEY,
    DEVICE_STATE_CHANGED,
    CLEAR_CACHE,
    ENABLE_BACKGROUND_SYNC
}
