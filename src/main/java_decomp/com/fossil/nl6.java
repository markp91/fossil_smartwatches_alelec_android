package com.fossil;

import java.util.concurrent.atomic.AtomicLong;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nl6 {
    @DexIgnore
    public static /* final */ boolean a; // = gl6.class.desiredAssertionStatus();
    @DexIgnore
    public static /* final */ boolean b;
    @DexIgnore
    public static /* final */ boolean c;
    @DexIgnore
    public static /* final */ AtomicLong d; // = new AtomicLong(0);

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002d, code lost:
        if (r0.equals("auto") != false) goto L_0x002f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0042, code lost:
        if (r0.equals("on") != false) goto L_0x004d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004b, code lost:
        if (r0.equals("") != false) goto L_0x004d;
     */
    /*
    static {
        boolean z;
        String a2 = vo6.a("kotlinx.coroutines.debug");
        boolean z2 = false;
        if (a2 != null) {
            int hashCode = a2.hashCode();
            if (hashCode != 0) {
                if (hashCode != 3551) {
                    if (hashCode != 109935) {
                        if (hashCode == 3005871) {
                        }
                    } else if (a2.equals("off")) {
                        z = false;
                        b = z;
                        if (b && vo6.a("kotlinx.coroutines.stacktrace.recovery", true)) {
                            z2 = true;
                        }
                        c = z2;
                    }
                }
                throw new IllegalStateException(("System property 'kotlinx.coroutines.debug' has unrecognized value '" + a2 + '\'').toString());
            }
            z = true;
            b = z;
            z2 = true;
            c = z2;
        }
        z = a;
        b = z;
        z2 = true;
        c = z2;
    }
    */

    @DexIgnore
    public static final boolean a() {
        return a;
    }

    @DexIgnore
    public static final AtomicLong b() {
        return d;
    }

    @DexIgnore
    public static final boolean c() {
        return b;
    }

    @DexIgnore
    public static final boolean d() {
        return c;
    }
}
