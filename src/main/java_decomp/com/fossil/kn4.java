package com.fossil;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.SignUpSocialAuth;
import com.sina.weibo.sdk.WbSdk;
import com.sina.weibo.sdk.auth.AuthInfo;
import com.sina.weibo.sdk.auth.Oauth2AccessToken;
import com.sina.weibo.sdk.auth.WbAuthListener;
import com.sina.weibo.sdk.auth.WbConnectErrorMessage;
import com.sina.weibo.sdk.auth.sso.SsoHandler;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kn4 {
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ a d; // = new a((qg6) null);
    @DexIgnore
    public SsoHandler a;
    @DexIgnore
    public boolean b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return kn4.c;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ kn4 a;
        @DexIgnore
        public /* final */ /* synthetic */ ln4 b;

        @DexIgnore
        public b(kn4 kn4, ln4 ln4) {
            this.a = kn4;
            this.b = ln4;
        }

        @DexIgnore
        public final void run() {
            if (!this.a.a()) {
                this.b.a(600, (gv1) null, "");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements WbAuthListener {
        @DexIgnore
        public /* final */ /* synthetic */ kn4 a;
        @DexIgnore
        public /* final */ /* synthetic */ Handler b;
        @DexIgnore
        public /* final */ /* synthetic */ ln4 c;

        @DexIgnore
        public c(kn4 kn4, Handler handler, ln4 ln4) {
            this.a = kn4;
            this.b = handler;
            this.c = ln4;
        }

        @DexIgnore
        public void cancel() {
            FLogger.INSTANCE.getLocal().d(kn4.d.a(), "Weibo loginWithEmail cancel");
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            this.c.a(2, (gv1) null, (String) null);
        }

        @DexIgnore
        public void onFailure(WbConnectErrorMessage wbConnectErrorMessage) {
            wg6.b(wbConnectErrorMessage, "wbConnectErrorMessage");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kn4.d.a();
            local.d(a2, "Weibo loginWithEmail failed - message: " + wbConnectErrorMessage.getErrorMessage() + "- code: " + wbConnectErrorMessage.getErrorCode());
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            this.c.a(600, (gv1) null, (String) null);
        }

        @DexIgnore
        public void onSuccess(Oauth2AccessToken oauth2AccessToken) {
            wg6.b(oauth2AccessToken, "oauth2AccessToken");
            this.a.a(true);
            this.b.removeCallbacksAndMessages((Object) null);
            String token = oauth2AccessToken.getToken();
            String format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.US).format(new Date(oauth2AccessToken.getExpiresTime()));
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = kn4.d.a();
            local.d(a2, "Get access token from weibo success: " + token + " ,expire date: " + format);
            SignUpSocialAuth signUpSocialAuth = new SignUpSocialAuth();
            signUpSocialAuth.setClientId(tj4.f.a(""));
            wg6.a((Object) token, "authToken");
            signUpSocialAuth.setToken(token);
            signUpSocialAuth.setService("weibo");
            this.c.a(signUpSocialAuth);
        }
    }

    /*
    static {
        String simpleName = kn4.class.getSimpleName();
        wg6.a((Object) simpleName, "MFLoginWeiboManager::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public final boolean a() {
        return this.b;
    }

    @DexIgnore
    public final void a(boolean z) {
        this.b = z;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a(String str, String str2, String str3) {
        wg6.b(str, "apiKey");
        wg6.b(str2, "redirectUrl");
        wg6.b(str3, "scope");
        try {
            WbSdk.install(PortfolioApp.get.instance(), new AuthInfo(PortfolioApp.get.instance(), str, str2, str3));
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d(c, "exception when install WBSdk");
        }
    }

    @DexIgnore
    public final void a(int i, int i2, Intent intent) {
        wg6.b(intent, "data");
        SsoHandler ssoHandler = this.a;
        if (ssoHandler == null) {
            return;
        }
        if (ssoHandler != null) {
            ssoHandler.authorizeCallBack(i, i2, intent);
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(Activity activity, ln4 ln4) {
        wg6.b(activity, Constants.ACTIVITY);
        wg6.b(ln4, Constants.CALLBACK);
        try {
            this.b = false;
            Handler handler = new Handler(Looper.getMainLooper());
            handler.postDelayed(new b(this, ln4), 60000);
            this.a = new SsoHandler(activity);
            SsoHandler ssoHandler = this.a;
            if (ssoHandler != null) {
                ssoHandler.authorize(new c(this, handler, ln4));
            } else {
                wg6.a();
                throw null;
            }
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "Weibo loginWithEmail failed - ex=" + e);
            ln4.a(600, (gv1) null, "");
        }
    }
}
