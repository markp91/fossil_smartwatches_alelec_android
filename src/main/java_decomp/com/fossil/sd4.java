package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sd4 extends rd4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ScrollView t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(2131362047, 1);
        w.put(2131361851, 2);
        w.put(2131362442, 3);
        w.put(2131362325, 4);
        w.put(2131362326, 5);
        w.put(2131361946, 6);
        w.put(2131362327, 7);
        w.put(2131361941, 8);
    }
    */

    @DexIgnore
    public sd4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 9, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sd4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[8], objArr[6], objArr[1], objArr[4], objArr[5], objArr[7], objArr[3]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
