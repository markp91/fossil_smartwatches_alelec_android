package com.fossil;

import com.fossil.mc6;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ui6<T> extends vi6<T> implements Iterator<T>, xe6<cd6>, ph6 {
    @DexIgnore
    public int a;
    @DexIgnore
    public T b;
    @DexIgnore
    public Iterator<? extends T> c;
    @DexIgnore
    public xe6<? super cd6> d;

    @DexIgnore
    public final void a(xe6<? super cd6> xe6) {
        this.d = xe6;
    }

    @DexIgnore
    public final T b() {
        if (hasNext()) {
            return next();
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public af6 getContext() {
        return bf6.INSTANCE;
    }

    @DexIgnore
    public boolean hasNext() {
        while (true) {
            int i = this.a;
            if (i != 0) {
                if (i == 1) {
                    Iterator<? extends T> it = this.c;
                    if (it == null) {
                        wg6.a();
                        throw null;
                    } else if (it.hasNext()) {
                        this.a = 2;
                        return true;
                    } else {
                        this.c = null;
                    }
                } else if (i == 2 || i == 3) {
                    return true;
                } else {
                    if (i == 4) {
                        return false;
                    }
                    throw a();
                }
            }
            this.a = 5;
            xe6<? super cd6> xe6 = this.d;
            if (xe6 != null) {
                this.d = null;
                cd6 cd6 = cd6.a;
                mc6.a aVar = mc6.Companion;
                xe6.resumeWith(mc6.m1constructorimpl(cd6));
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public T next() {
        int i = this.a;
        if (i == 0 || i == 1) {
            return b();
        }
        if (i == 2) {
            this.a = 1;
            Iterator<? extends T> it = this.c;
            if (it != null) {
                return it.next();
            }
            wg6.a();
            throw null;
        } else if (i == 3) {
            this.a = 0;
            T t = this.b;
            this.b = null;
            return t;
        } else {
            throw a();
        }
    }

    @DexIgnore
    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        nc6.a(obj);
        this.a = 4;
    }

    @DexIgnore
    public final Throwable a() {
        int i = this.a;
        if (i == 4) {
            return new NoSuchElementException();
        }
        if (i == 5) {
            return new IllegalStateException("Iterator has failed.");
        }
        return new IllegalStateException("Unexpected state of the iterator: " + this.a);
    }

    @DexIgnore
    public Object a(T t, xe6<? super cd6> xe6) {
        this.b = t;
        this.a = 3;
        this.d = xe6;
        Object a2 = ff6.a();
        if (a2 == ff6.a()) {
            nf6.c(xe6);
        }
        return a2;
    }
}
