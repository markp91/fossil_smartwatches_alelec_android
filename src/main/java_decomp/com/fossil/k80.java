package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k80 extends o80 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<k80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public k80 createFromParcel(Parcel parcel) {
            return new k80(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new k80[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m32createFromParcel(Parcel parcel) {
            return new k80(parcel, (qg6) null);
        }
    }

    @DexIgnore
    public k80() {
        super(q80.MUSIC, (ce0) null, (ee0) null, 6);
    }

    @DexIgnore
    public k80(ce0 ce0) {
        super(q80.MUSIC, ce0, (ee0) null, 4);
    }

    @DexIgnore
    public /* synthetic */ k80(Parcel parcel, qg6 qg6) {
        super(parcel);
    }
}
