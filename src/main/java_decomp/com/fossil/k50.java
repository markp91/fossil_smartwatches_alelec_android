package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.charset.Charset;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k50 extends a50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<k50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final k50 a(byte[] bArr) {
            return new k50(new String(md6.a(bArr, 0, bArr.length - 1), ej6.a));
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new k50[i];
        }

        @DexIgnore
        public k50 createFromParcel(Parcel parcel) {
            parcel.readInt();
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                return new k50(readString);
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public k50(String str) {
        super(qk0.c);
        this.b = str;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.g, (Object) this.b);
    }

    @DexIgnore
    public byte[] c() {
        String str = this.b;
        Charset f = mi0.A.f();
        if (str != null) {
            byte[] bytes = str.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return md6.a(bytes, (byte) 0);
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(k50.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            return !(wg6.a(this.b, ((k50) obj).b) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.TitleEntry");
    }

    @DexIgnore
    public final String getTitle() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeString(this.b);
        }
    }
}
