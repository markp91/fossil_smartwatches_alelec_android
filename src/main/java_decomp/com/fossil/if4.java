package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class if4 extends hf4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362321, 1);
        y.put(2131362320, 2);
        y.put(2131362653, 3);
        y.put(2131362318, 4);
        y.put(2131362316, 5);
        y.put(2131362342, 6);
        y.put(2131362374, 7);
    }
    */

    @DexIgnore
    public if4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 8, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public if4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[5], objArr[4], objArr[2], objArr[1], objArr[6], objArr[7], objArr[3]);
        this.w = -1;
        this.q.setTag((Object) null);
        View view2 = view;
        a(view);
        f();
    }
}
