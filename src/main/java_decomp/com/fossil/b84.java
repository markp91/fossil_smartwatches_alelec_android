package com.fossil;

import android.view.View;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b84 extends ViewDataBinding {
    @DexIgnore
    public b84(Object obj, View view, int i, FlexibleTextView flexibleTextView) {
        super(obj, view, i);
    }
}
