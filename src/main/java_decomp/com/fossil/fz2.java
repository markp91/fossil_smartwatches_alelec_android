package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fz2 implements Parcelable.Creator<LatLngBounds> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                latLng = (LatLng) f22.a(parcel, a, LatLng.CREATOR);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                latLng2 = (LatLng) f22.a(parcel, a, LatLng.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new LatLngBounds(latLng, latLng2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
