package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.SleepSessionsRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.sleep.overview.SleepOverviewDayPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ig5 implements Factory<hg5> {
    @DexIgnore
    public static SleepOverviewDayPresenter a(gg5 gg5, SleepSummariesRepository sleepSummariesRepository, SleepSessionsRepository sleepSessionsRepository, PortfolioApp portfolioApp) {
        return new SleepOverviewDayPresenter(gg5, sleepSummariesRepository, sleepSessionsRepository, portfolioApp);
    }
}
