package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class wb4 extends vb4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C; // = new SparseIntArray();
    @DexIgnore
    public long A;

    /*
    static {
        C.put(2131362561, 1);
        C.put(2131362442, 2);
        C.put(2131362666, 3);
        C.put(2131362295, 4);
        C.put(2131363267, 5);
        C.put(2131362667, 6);
        C.put(2131362296, 7);
        C.put(2131363268, 8);
        C.put(2131362683, 9);
        C.put(2131362283, 10);
        C.put(2131362987, 11);
        C.put(2131362408, 12);
        C.put(2131363269, 13);
        C.put(2131362348, 14);
        C.put(2131362649, 15);
        C.put(2131361955, 16);
        C.put(2131362874, 17);
    }
    */

    @DexIgnore
    public wb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 18, B, C));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public wb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[16], objArr[4], objArr[7], objArr[14], objArr[10], objArr[12], objArr[2], objArr[1], objArr[15], objArr[3], objArr[6], objArr[9], objArr[0], objArr[17], objArr[11], objArr[5], objArr[8], objArr[13]);
        this.A = -1;
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
