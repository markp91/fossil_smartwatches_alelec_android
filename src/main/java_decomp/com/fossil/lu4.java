package com.fossil;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.view.CustomizeWidget;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lu4 extends RecyclerView.g<b> {
    @DexIgnore
    public CustomizeWidget a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<MicroApp> c;
    @DexIgnore
    public c d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public CustomizeWidget a;
        @DexIgnore
        public TextView b;
        @DexIgnore
        public View c;
        @DexIgnore
        public View d;
        @DexIgnore
        public MicroApp e;
        @DexIgnore
        public /* final */ /* synthetic */ lu4 f;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.a((MicroApp) obj);
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lu4$b$b")
        /* renamed from: com.fossil.lu4$b$b  reason: collision with other inner class name */
        public static final class C0028b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0028b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c d;
                if (this.a.f.getItemCount() > this.a.getAdapterPosition() && this.a.getAdapterPosition() != -1 && (d = this.a.f.d()) != null) {
                    Object obj = this.a.f.c.get(this.a.getAdapterPosition());
                    wg6.a(obj, "mData[adapterPosition]");
                    d.b((MicroApp) obj);
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements CustomizeWidget.b {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public c(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public void a(CustomizeWidget customizeWidget) {
                wg6.b(customizeWidget, "view");
                this.a.f.a = customizeWidget;
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v11, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(lu4 lu4, View view) {
            super(view);
            wg6.b(view, "view");
            this.f = lu4;
            this.c = view.findViewById(2131362603);
            this.d = view.findViewById(2131362638);
            View findViewById = view.findViewById(2131363340);
            wg6.a((Object) findViewById, "view.findViewById(R.id.wc_watch_app)");
            this.a = (CustomizeWidget) findViewById;
            View findViewById2 = view.findViewById(2131363232);
            wg6.a((Object) findViewById2, "view.findViewById(R.id.tv_watch_app_name)");
            this.b = (TextView) findViewById2;
            this.a.setOnClickListener(new a(this));
            this.d.setOnClickListener(new C0028b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(MicroApp microApp) {
            String str;
            wg6.b(microApp, "microApp");
            this.e = microApp;
            if (this.e != null) {
                this.a.c(microApp.getId());
                this.b.setText(jm4.a(PortfolioApp.get.instance(), microApp.getNameKey(), microApp.getName()));
            }
            this.a.setSelectedWc(wg6.a((Object) microApp.getId(), (Object) this.f.b));
            View view = this.d;
            wg6.a((Object) view, "ivWarning");
            view.setVisibility(!rk4.c.e(microApp.getId()) ? 0 : 8);
            if (wg6.a((Object) microApp.getId(), (Object) this.f.b)) {
                View view2 = this.c;
                wg6.a((Object) view2, "ivIndicator");
                view2.setBackground(w6.c(PortfolioApp.get.instance(), 2131230988));
            } else {
                View view3 = this.c;
                wg6.a((Object) view3, "ivIndicator");
                view3.setBackground(w6.c(PortfolioApp.get.instance(), 2131230989));
            }
            CustomizeWidget customizeWidget = this.a;
            Intent intent = new Intent();
            MicroApp microApp2 = this.e;
            if (microApp2 == null || (str = microApp2.getId()) == null) {
                str = "";
            }
            Intent putExtra = intent.putExtra("KEY_ID", str);
            wg6.a((Object) putExtra, "Intent().putExtra(Custom\u2026                   ?: \"\")");
            CustomizeWidget.a(customizeWidget, "WATCH_APP", putExtra, (View.OnDragListener) null, new c(this), 4, (Object) null);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(MicroApp microApp);

        @DexIgnore
        void b(MicroApp microApp);
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ lu4(ArrayList arrayList, c cVar, int i, qg6 qg6) {
        this((i & 1) != 0 ? new ArrayList() : arrayList, (i & 2) != 0 ? null : cVar);
    }

    @DexIgnore
    public final void c() {
        FLogger.INSTANCE.getLocal().d("MicroAppsAdapter", "dragStopped");
        CustomizeWidget customizeWidget = this.a;
        if (customizeWidget != null) {
            customizeWidget.setDragMode(false);
        }
    }

    @DexIgnore
    public final c d() {
        return this.d;
    }

    @DexIgnore
    public int getItemCount() {
        return this.c.size();
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "microAppId");
        this.b = str;
        notifyDataSetChanged();
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558689, viewGroup, false);
        wg6.a((Object) inflate, "LayoutInflater.from(pare\u2026watch_app, parent, false)");
        return new b(this, inflate);
    }

    @DexIgnore
    public lu4(ArrayList<MicroApp> arrayList, c cVar) {
        wg6.b(arrayList, "mData");
        this.c = arrayList;
        this.d = cVar;
        this.b = "empty";
    }

    @DexIgnore
    public final void a(List<MicroApp> list) {
        wg6.b(list, "data");
        this.c.clear();
        this.c.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public final int a(String str) {
        T t;
        wg6.b(str, "microAppId");
        Iterator<T> it = this.c.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((MicroApp) t).getId(), (Object) str)) {
                break;
            }
        }
        MicroApp microApp = (MicroApp) t;
        if (microApp != null) {
            return this.c.indexOf(microApp);
        }
        return -1;
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        if (getItemCount() > i && i != -1) {
            MicroApp microApp = this.c.get(i);
            wg6.a((Object) microApp, "mData[position]");
            bVar.a(microApp);
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wg6.b(cVar, "listener");
        this.d = cVar;
    }
}
