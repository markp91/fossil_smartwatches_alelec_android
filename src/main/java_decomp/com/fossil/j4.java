package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j4 implements m4 {
    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(l4 l4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3) {
        l4Var.a(new n4(colorStateList, f));
        View d = l4Var.d();
        d.setClipToOutline(true);
        d.setElevation(f2);
        c(l4Var, f3);
    }

    @DexIgnore
    public float b(l4 l4Var) {
        return j(l4Var).c();
    }

    @DexIgnore
    public void c(l4 l4Var, float f) {
        j(l4Var).a(f, l4Var.b(), l4Var.a());
        f(l4Var);
    }

    @DexIgnore
    public float d(l4 l4Var) {
        return j(l4Var).b();
    }

    @DexIgnore
    public ColorStateList e(l4 l4Var) {
        return j(l4Var).a();
    }

    @DexIgnore
    public void f(l4 l4Var) {
        if (!l4Var.b()) {
            l4Var.a(0, 0, 0, 0);
            return;
        }
        float d = d(l4Var);
        float b = b(l4Var);
        int ceil = (int) Math.ceil((double) o4.a(d, b, l4Var.a()));
        int ceil2 = (int) Math.ceil((double) o4.b(d, b, l4Var.a()));
        l4Var.a(ceil, ceil2, ceil, ceil2);
    }

    @DexIgnore
    public float g(l4 l4Var) {
        return b(l4Var) * 2.0f;
    }

    @DexIgnore
    public float h(l4 l4Var) {
        return b(l4Var) * 2.0f;
    }

    @DexIgnore
    public void i(l4 l4Var) {
        c(l4Var, d(l4Var));
    }

    @DexIgnore
    public final n4 j(l4 l4Var) {
        return (n4) l4Var.c();
    }

    @DexIgnore
    public void b(l4 l4Var, float f) {
        l4Var.d().setElevation(f);
    }

    @DexIgnore
    public void c(l4 l4Var) {
        c(l4Var, d(l4Var));
    }

    @DexIgnore
    public void a(l4 l4Var, float f) {
        j(l4Var).a(f);
    }

    @DexIgnore
    public float a(l4 l4Var) {
        return l4Var.d().getElevation();
    }

    @DexIgnore
    public void a(l4 l4Var, ColorStateList colorStateList) {
        j(l4Var).b(colorStateList);
    }
}
