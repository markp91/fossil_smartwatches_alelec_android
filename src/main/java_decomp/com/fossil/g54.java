package com.fossil;

import com.portfolio.platform.data.legacy.threedotzero.PresetRepository;
import com.portfolio.platform.data.legacy.threedotzero.SavedPreset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
/* compiled from: lambda */
public final /* synthetic */ class g54 implements Runnable {
    @DexIgnore
    private /* final */ /* synthetic */ PresetRepository.Anon21 a;
    @DexIgnore
    private /* final */ /* synthetic */ SavedPreset b;

    @DexIgnore
    public /* synthetic */ g54(PresetRepository.Anon21 anon21, SavedPreset savedPreset) {
        this.a = anon21;
        this.b = savedPreset;
    }

    @DexIgnore
    public final void run() {
        this.a.a(this.b);
    }
}
