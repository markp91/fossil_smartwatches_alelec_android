package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutSession implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutSession> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ Integer mAveragePace;
    @DexIgnore
    public /* final */ Calorie mCalorie;
    @DexIgnore
    public /* final */ Distance mDistance;
    @DexIgnore
    public /* final */ int mDuration;
    @DexIgnore
    public /* final */ int mEndtime;
    @DexIgnore
    public /* final */ ArrayList<GpsDataPoint> mGpsDataPoints;
    @DexIgnore
    public /* final */ HeartRate mHeartrate;
    @DexIgnore
    public /* final */ int mId;
    @DexIgnore
    public /* final */ int mStarttime;
    @DexIgnore
    public /* final */ ArrayList<WorkoutStateChange> mStateChanges;
    @DexIgnore
    public /* final */ Step mStep;
    @DexIgnore
    public /* final */ int mTimezoneOffsetInSecond;
    @DexIgnore
    public /* final */ WorkoutType mType;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<WorkoutSession> {
        @DexIgnore
        public WorkoutSession createFromParcel(Parcel parcel) {
            return new WorkoutSession(parcel);
        }

        @DexIgnore
        public WorkoutSession[] newArray(int i) {
            return new WorkoutSession[i];
        }
    }

    @DexIgnore
    public WorkoutSession(int i, int i2, int i3, int i4, int i5, WorkoutType workoutType, ArrayList<WorkoutStateChange> arrayList, Step step, Calorie calorie, Distance distance, HeartRate heartRate, ArrayList<GpsDataPoint> arrayList2, Integer num) {
        this.mId = i;
        this.mStarttime = i2;
        this.mEndtime = i3;
        this.mTimezoneOffsetInSecond = i4;
        this.mDuration = i5;
        this.mType = workoutType;
        this.mStateChanges = arrayList;
        this.mStep = step;
        this.mCalorie = calorie;
        this.mDistance = distance;
        this.mHeartrate = heartRate;
        this.mGpsDataPoints = arrayList2;
        this.mAveragePace = num;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        HeartRate heartRate;
        if (!(obj instanceof WorkoutSession)) {
            return false;
        }
        WorkoutSession workoutSession = (WorkoutSession) obj;
        if (this.mId != workoutSession.mId || this.mStarttime != workoutSession.mStarttime || this.mEndtime != workoutSession.mEndtime || this.mTimezoneOffsetInSecond != workoutSession.mTimezoneOffsetInSecond || this.mDuration != workoutSession.mDuration || this.mType != workoutSession.mType || !this.mStateChanges.equals(workoutSession.mStateChanges) || !this.mStep.equals(workoutSession.mStep) || !this.mCalorie.equals(workoutSession.mCalorie) || !this.mDistance.equals(workoutSession.mDistance)) {
            return false;
        }
        if (((this.mHeartrate != null || workoutSession.mHeartrate != null) && ((heartRate = this.mHeartrate) == null || !heartRate.equals(workoutSession.mHeartrate))) || !this.mGpsDataPoints.equals(workoutSession.mGpsDataPoints)) {
            return false;
        }
        if ((this.mAveragePace != null || workoutSession.mAveragePace != null) && ((num = this.mAveragePace) == null || !num.equals(workoutSession.mAveragePace))) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public Integer getAveragePace() {
        return this.mAveragePace;
    }

    @DexIgnore
    public Calorie getCalorie() {
        return this.mCalorie;
    }

    @DexIgnore
    public Distance getDistance() {
        return this.mDistance;
    }

    @DexIgnore
    public int getDuration() {
        return this.mDuration;
    }

    @DexIgnore
    public int getEndtime() {
        return this.mEndtime;
    }

    @DexIgnore
    public ArrayList<GpsDataPoint> getGpsDataPoints() {
        return this.mGpsDataPoints;
    }

    @DexIgnore
    public HeartRate getHeartrate() {
        return this.mHeartrate;
    }

    @DexIgnore
    public int getId() {
        return this.mId;
    }

    @DexIgnore
    public int getStarttime() {
        return this.mStarttime;
    }

    @DexIgnore
    public ArrayList<WorkoutStateChange> getStateChanges() {
        return this.mStateChanges;
    }

    @DexIgnore
    public Step getStep() {
        return this.mStep;
    }

    @DexIgnore
    public int getTimezoneOffsetInSecond() {
        return this.mTimezoneOffsetInSecond;
    }

    @DexIgnore
    public WorkoutType getType() {
        return this.mType;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((((((((((527 + this.mId) * 31) + this.mStarttime) * 31) + this.mEndtime) * 31) + this.mTimezoneOffsetInSecond) * 31) + this.mDuration) * 31) + this.mType.hashCode()) * 31) + this.mStateChanges.hashCode()) * 31) + this.mStep.hashCode()) * 31) + this.mCalorie.hashCode()) * 31) + this.mDistance.hashCode()) * 31;
        HeartRate heartRate = this.mHeartrate;
        int i = 0;
        int hashCode2 = (((hashCode + (heartRate == null ? 0 : heartRate.hashCode())) * 31) + this.mGpsDataPoints.hashCode()) * 31;
        Integer num = this.mAveragePace;
        if (num != null) {
            i = num.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutSession{mId=" + this.mId + ",mStarttime=" + this.mStarttime + ",mEndtime=" + this.mEndtime + ",mTimezoneOffsetInSecond=" + this.mTimezoneOffsetInSecond + ",mDuration=" + this.mDuration + ",mType=" + this.mType + ",mStateChanges=" + this.mStateChanges + ",mStep=" + this.mStep + ",mCalorie=" + this.mCalorie + ",mDistance=" + this.mDistance + ",mHeartrate=" + this.mHeartrate + ",mGpsDataPoints=" + this.mGpsDataPoints + ",mAveragePace=" + this.mAveragePace + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mId);
        parcel.writeInt(this.mStarttime);
        parcel.writeInt(this.mEndtime);
        parcel.writeInt(this.mTimezoneOffsetInSecond);
        parcel.writeInt(this.mDuration);
        parcel.writeInt(this.mType.ordinal());
        parcel.writeList(this.mStateChanges);
        this.mStep.writeToParcel(parcel, i);
        this.mCalorie.writeToParcel(parcel, i);
        this.mDistance.writeToParcel(parcel, i);
        if (this.mHeartrate != null) {
            parcel.writeByte((byte) 1);
            this.mHeartrate.writeToParcel(parcel, i);
        } else {
            parcel.writeByte((byte) 0);
        }
        parcel.writeList(this.mGpsDataPoints);
        if (this.mAveragePace != null) {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.mAveragePace.intValue());
            return;
        }
        parcel.writeByte((byte) 0);
    }

    @DexIgnore
    public WorkoutSession(Parcel parcel) {
        this.mId = parcel.readInt();
        this.mStarttime = parcel.readInt();
        this.mEndtime = parcel.readInt();
        this.mTimezoneOffsetInSecond = parcel.readInt();
        this.mDuration = parcel.readInt();
        this.mType = WorkoutType.values()[parcel.readInt()];
        this.mStateChanges = new ArrayList<>();
        parcel.readList(this.mStateChanges, WorkoutSession.class.getClassLoader());
        this.mStep = new Step(parcel);
        this.mCalorie = new Calorie(parcel);
        this.mDistance = new Distance(parcel);
        if (parcel.readByte() == 0) {
            this.mHeartrate = null;
        } else {
            this.mHeartrate = new HeartRate(parcel);
        }
        this.mGpsDataPoints = new ArrayList<>();
        parcel.readList(this.mGpsDataPoints, WorkoutSession.class.getClassLoader());
        if (parcel.readByte() == 0) {
            this.mAveragePace = null;
        } else {
            this.mAveragePace = Integer.valueOf(parcel.readInt());
        }
    }
}
