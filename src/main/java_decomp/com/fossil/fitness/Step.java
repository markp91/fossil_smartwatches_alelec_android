package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Step implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Step> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ int mTotal;
    @DexIgnore
    public /* final */ ArrayList<Short> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Step> {
        @DexIgnore
        public Step createFromParcel(Parcel parcel) {
            return new Step(parcel);
        }

        @DexIgnore
        public Step[] newArray(int i) {
            return new Step[i];
        }
    }

    @DexIgnore
    public Step(int i, ArrayList<Short> arrayList, int i2) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mTotal = i2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Step)) {
            return false;
        }
        Step step = (Step) obj;
        if (this.mResolutionInSecond == step.mResolutionInSecond && this.mValues.equals(step.mValues) && this.mTotal == step.mTotal) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public int getTotal() {
        return this.mTotal;
    }

    @DexIgnore
    public ArrayList<Short> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mResolutionInSecond) * 31) + this.mValues.hashCode()) * 31) + this.mTotal;
    }

    @DexIgnore
    public String toString() {
        return "Step{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mTotal=" + this.mTotal + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeInt(this.mTotal);
    }

    @DexIgnore
    public Step(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        this.mValues = new ArrayList<>();
        parcel.readList(this.mValues, Step.class.getClassLoader());
        this.mTotal = parcel.readInt();
    }
}
