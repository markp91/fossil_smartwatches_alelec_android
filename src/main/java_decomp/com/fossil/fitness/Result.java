package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Result implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Result> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ ArrayList<ActivitySample> mActivitySamples;
    @DexIgnore
    public /* final */ ArrayList<FitnessData> mFitnessData;
    @DexIgnore
    public /* final */ StatusCode mStatus;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Result> {
        @DexIgnore
        public Result createFromParcel(Parcel parcel) {
            return new Result(parcel);
        }

        @DexIgnore
        public Result[] newArray(int i) {
            return new Result[i];
        }
    }

    @DexIgnore
    public Result(StatusCode statusCode, ArrayList<FitnessData> arrayList, ArrayList<ActivitySample> arrayList2) {
        this.mStatus = statusCode;
        this.mFitnessData = arrayList;
        this.mActivitySamples = arrayList2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Result)) {
            return false;
        }
        Result result = (Result) obj;
        if (this.mStatus != result.mStatus || !this.mFitnessData.equals(result.mFitnessData) || !this.mActivitySamples.equals(result.mActivitySamples)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public ArrayList<ActivitySample> getActivitySamples() {
        return this.mActivitySamples;
    }

    @DexIgnore
    public ArrayList<FitnessData> getFitnessData() {
        return this.mFitnessData;
    }

    @DexIgnore
    public StatusCode getStatus() {
        return this.mStatus;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mStatus.hashCode()) * 31) + this.mFitnessData.hashCode()) * 31) + this.mActivitySamples.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Result{mStatus=" + this.mStatus + ",mFitnessData=" + this.mFitnessData + ",mActivitySamples=" + this.mActivitySamples + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mStatus.ordinal());
        parcel.writeList(this.mFitnessData);
        parcel.writeList(this.mActivitySamples);
    }

    @DexIgnore
    public Result(Parcel parcel) {
        this.mStatus = StatusCode.values()[parcel.readInt()];
        this.mFitnessData = new ArrayList<>();
        parcel.readList(this.mFitnessData, Result.class.getClassLoader());
        this.mActivitySamples = new ArrayList<>();
        parcel.readList(this.mActivitySamples, Result.class.getClassLoader());
    }
}
