package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class WorkoutStateChange implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<WorkoutStateChange> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mIndexInSecond;
    @DexIgnore
    public /* final */ WorkoutState mState;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<WorkoutStateChange> {
        @DexIgnore
        public WorkoutStateChange createFromParcel(Parcel parcel) {
            return new WorkoutStateChange(parcel);
        }

        @DexIgnore
        public WorkoutStateChange[] newArray(int i) {
            return new WorkoutStateChange[i];
        }
    }

    @DexIgnore
    public WorkoutStateChange(WorkoutState workoutState, int i) {
        this.mState = workoutState;
        this.mIndexInSecond = i;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof WorkoutStateChange)) {
            return false;
        }
        WorkoutStateChange workoutStateChange = (WorkoutStateChange) obj;
        if (this.mState == workoutStateChange.mState && this.mIndexInSecond == workoutStateChange.mIndexInSecond) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getIndexInSecond() {
        return this.mIndexInSecond;
    }

    @DexIgnore
    public WorkoutState getState() {
        return this.mState;
    }

    @DexIgnore
    public int hashCode() {
        return ((527 + this.mState.hashCode()) * 31) + this.mIndexInSecond;
    }

    @DexIgnore
    public String toString() {
        return "WorkoutStateChange{mState=" + this.mState + ",mIndexInSecond=" + this.mIndexInSecond + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mState.ordinal());
        parcel.writeInt(this.mIndexInSecond);
    }

    @DexIgnore
    public WorkoutStateChange(Parcel parcel) {
        this.mState = WorkoutState.values()[parcel.readInt()];
        this.mIndexInSecond = parcel.readInt();
    }
}
