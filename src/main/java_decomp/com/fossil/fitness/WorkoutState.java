package com.fossil.fitness;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum WorkoutState {
    START(0),
    RESUME(1),
    PAUSE(2),
    END(3),
    IDLE(4);
    
    @DexIgnore
    public /* final */ int value;

    @DexIgnore
    public WorkoutState(int i) {
        this.value = i;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
