package com.fossil.fitness;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class Distance implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<Distance> CREATOR; // = new Anon1();
    @DexIgnore
    public /* final */ int mResolutionInSecond;
    @DexIgnore
    public /* final */ double mTotal;
    @DexIgnore
    public /* final */ ArrayList<Double> mValues;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class Anon1 implements Parcelable.Creator<Distance> {
        @DexIgnore
        public Distance createFromParcel(Parcel parcel) {
            return new Distance(parcel);
        }

        @DexIgnore
        public Distance[] newArray(int i) {
            return new Distance[i];
        }
    }

    @DexIgnore
    public Distance(int i, ArrayList<Double> arrayList, double d) {
        this.mResolutionInSecond = i;
        this.mValues = arrayList;
        this.mTotal = d;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof Distance)) {
            return false;
        }
        Distance distance = (Distance) obj;
        if (this.mResolutionInSecond == distance.mResolutionInSecond && this.mValues.equals(distance.mValues) && this.mTotal == distance.mTotal) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int getResolutionInSecond() {
        return this.mResolutionInSecond;
    }

    @DexIgnore
    public double getTotal() {
        return this.mTotal;
    }

    @DexIgnore
    public ArrayList<Double> getValues() {
        return this.mValues;
    }

    @DexIgnore
    public int hashCode() {
        return ((((527 + this.mResolutionInSecond) * 31) + this.mValues.hashCode()) * 31) + ((int) (Double.doubleToLongBits(this.mTotal) ^ (Double.doubleToLongBits(this.mTotal) >>> 32)));
    }

    @DexIgnore
    public String toString() {
        return "Distance{mResolutionInSecond=" + this.mResolutionInSecond + ",mValues=" + this.mValues + ",mTotal=" + this.mTotal + "}";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.mResolutionInSecond);
        parcel.writeList(this.mValues);
        parcel.writeDouble(this.mTotal);
    }

    @DexIgnore
    public Distance(Parcel parcel) {
        this.mResolutionInSecond = parcel.readInt();
        this.mValues = new ArrayList<>();
        parcel.readList(this.mValues, Distance.class.getClassLoader());
        this.mTotal = parcel.readDouble();
    }
}
