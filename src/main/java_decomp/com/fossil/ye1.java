package com.fossil;

import com.fossil.q60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ye1 extends tg6 implements hg6<byte[], q60> {
    @DexIgnore
    public ye1(q60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(q60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailyTotalActiveMinuteConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((q60.a) this.receiver).a((byte[]) obj);
    }
}
