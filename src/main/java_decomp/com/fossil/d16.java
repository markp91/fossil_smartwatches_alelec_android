package com.fossil;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d16 extends InputStream {
    @DexIgnore
    public /* final */ InputStream a;
    @DexIgnore
    public long b;
    @DexIgnore
    public long c;
    @DexIgnore
    public long d;
    @DexIgnore
    public long e;

    @DexIgnore
    public d16(InputStream inputStream) {
        this(inputStream, 4096);
    }

    @DexIgnore
    public void a(long j) throws IOException {
        if (this.b > this.d || j < this.c) {
            throw new IOException("Cannot reset");
        }
        this.a.reset();
        a(this.c, j);
        this.b = j;
    }

    @DexIgnore
    public int available() throws IOException {
        return this.a.available();
    }

    @DexIgnore
    public long b(int i) {
        long j = this.b + ((long) i);
        if (this.d < j) {
            b(j);
        }
        return this.b;
    }

    @DexIgnore
    public void close() throws IOException {
        this.a.close();
    }

    @DexIgnore
    public void mark(int i) {
        this.e = b(i);
    }

    @DexIgnore
    public boolean markSupported() {
        return this.a.markSupported();
    }

    @DexIgnore
    public int read() throws IOException {
        int read = this.a.read();
        if (read != -1) {
            this.b++;
        }
        return read;
    }

    @DexIgnore
    public void reset() throws IOException {
        a(this.e);
    }

    @DexIgnore
    public long skip(long j) throws IOException {
        long skip = this.a.skip(j);
        this.b += skip;
        return skip;
    }

    @DexIgnore
    public d16(InputStream inputStream, int i) {
        this.e = -1;
        this.a = !inputStream.markSupported() ? new BufferedInputStream(inputStream, i) : inputStream;
    }

    @DexIgnore
    public int read(byte[] bArr) throws IOException {
        int read = this.a.read(bArr);
        if (read != -1) {
            this.b += (long) read;
        }
        return read;
    }

    @DexIgnore
    public final void b(long j) {
        try {
            if (this.c >= this.b || this.b > this.d) {
                this.c = this.b;
                this.a.mark((int) (j - this.b));
            } else {
                this.a.reset();
                this.a.mark((int) (j - this.c));
                a(this.c, this.b);
            }
            this.d = j;
        } catch (IOException e2) {
            throw new IllegalStateException("Unable to mark: " + e2);
        }
    }

    @DexIgnore
    public int read(byte[] bArr, int i, int i2) throws IOException {
        int read = this.a.read(bArr, i, i2);
        if (read != -1) {
            this.b += (long) read;
        }
        return read;
    }

    @DexIgnore
    public final void a(long j, long j2) throws IOException {
        while (j < j2) {
            long skip = this.a.skip(j2 - j);
            if (skip == 0) {
                if (read() != -1) {
                    skip = 1;
                } else {
                    return;
                }
            }
            j += skip;
        }
    }
}
