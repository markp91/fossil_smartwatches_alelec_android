package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.appbar.AppBarLayout;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleProgressBar;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class z54 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ RTLImageView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ RTLImageView E;
    @DexIgnore
    public /* final */ View F;
    @DexIgnore
    public /* final */ LinearLayout G;
    @DexIgnore
    public /* final */ FlexibleProgressBar H;
    @DexIgnore
    public /* final */ ConstraintLayout I;
    @DexIgnore
    public /* final */ RecyclerView J;
    @DexIgnore
    public /* final */ AppBarLayout q;
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ OverviewDayChart t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ FlexibleTextView w;
    @DexIgnore
    public /* final */ FlexibleTextView x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public z54(Object obj, View view, int i, AppBarLayout appBarLayout, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, OverviewDayChart overviewDayChart, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, RTLImageView rTLImageView, RTLImageView rTLImageView2, RTLImageView rTLImageView3, View view2, LinearLayout linearLayout, FlexibleProgressBar flexibleProgressBar, ConstraintLayout constraintLayout5, RecyclerView recyclerView, FlexibleTextView flexibleTextView9, View view3) {
        super(obj, view, i);
        this.q = appBarLayout;
        this.r = constraintLayout;
        this.s = constraintLayout2;
        this.t = overviewDayChart;
        this.u = flexibleTextView;
        this.v = flexibleTextView2;
        this.w = flexibleTextView3;
        this.x = flexibleTextView4;
        this.y = flexibleTextView5;
        this.z = flexibleTextView6;
        this.A = flexibleTextView7;
        this.B = flexibleTextView8;
        this.C = rTLImageView;
        this.D = rTLImageView2;
        this.E = rTLImageView3;
        this.F = view2;
        this.G = linearLayout;
        this.H = flexibleProgressBar;
        this.I = constraintLayout5;
        this.J = recyclerView;
    }
}
