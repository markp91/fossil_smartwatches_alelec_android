package com.fossil;

import com.fossil.yw3;
import com.fossil.zw3;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cp1 extends yw3<cp1, b> implements dp1 {
    @DexIgnore
    public static /* final */ cp1 f; // = new cp1();
    @DexIgnore
    public static volatile gx3<cp1> g;
    @DexIgnore
    public int d;
    @DexIgnore
    public int e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[yw3.j.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[yw3.j.NEW_MUTABLE_INSTANCE.ordinal()] = 1;
            a[yw3.j.IS_INITIALIZED.ordinal()] = 2;
            a[yw3.j.MAKE_IMMUTABLE.ordinal()] = 3;
            a[yw3.j.NEW_BUILDER.ordinal()] = 4;
            a[yw3.j.VISIT.ordinal()] = 5;
            a[yw3.j.MERGE_FROM_STREAM.ordinal()] = 6;
            a[yw3.j.GET_DEFAULT_INSTANCE.ordinal()] = 7;
            try {
                a[yw3.j.GET_PARSER.ordinal()] = 8;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends yw3.b<cp1, b> implements dp1 {
        @DexIgnore
        public /* synthetic */ b(a aVar) {
            this();
        }

        @DexIgnore
        public b a(int i) {
            d();
            ((cp1) this.b).e = i;
            return this;
        }

        @DexIgnore
        public b b(int i) {
            d();
            ((cp1) this.b).d = i;
            return this;
        }

        @DexIgnore
        public b() {
            super(cp1.f);
        }
    }

    @DexIgnore
    public enum c implements zw3.a {
        UNKNOWN_MOBILE_SUBTYPE(0),
        GPRS(1),
        EDGE(2),
        UMTS(3),
        CDMA(4),
        EVDO_0(5),
        EVDO_A(6),
        RTT(7),
        HSDPA(8),
        HSUPA(9),
        HSPA(10),
        IDEN(11),
        EVDO_B(12),
        LTE(13),
        EHRPD(14),
        HSPAP(15),
        GSM(16),
        TD_SCDMA(17),
        IWLAN(18),
        LTE_CA(19),
        COMBINED(100),
        UNRECOGNIZED(-1);
        
        @DexIgnore
        public /* final */ int zzw;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements zw3.b<c> {
        }

        /*
        static {
            UNKNOWN_MOBILE_SUBTYPE = new c("UNKNOWN_MOBILE_SUBTYPE", 0, 0);
            GPRS = new c("GPRS", 1, 1);
            EDGE = new c("EDGE", 2, 2);
            UMTS = new c("UMTS", 3, 3);
            CDMA = new c("CDMA", 4, 4);
            EVDO_0 = new c("EVDO_0", 5, 5);
            EVDO_A = new c("EVDO_A", 6, 6);
            RTT = new c("RTT", 7, 7);
            HSDPA = new c("HSDPA", 8, 8);
            HSUPA = new c("HSUPA", 9, 9);
            HSPA = new c("HSPA", 10, 10);
            IDEN = new c("IDEN", 11, 11);
            EVDO_B = new c("EVDO_B", 12, 12);
            LTE = new c("LTE", 13, 13);
            EHRPD = new c("EHRPD", 14, 14);
            HSPAP = new c("HSPAP", 15, 15);
            GSM = new c("GSM", 16, 16);
            TD_SCDMA = new c("TD_SCDMA", 17, 17);
            IWLAN = new c("IWLAN", 18, 18);
            LTE_CA = new c("LTE_CA", 19, 19);
            COMBINED = new c("COMBINED", 20, 100);
            UNRECOGNIZED = new c("UNRECOGNIZED", 21, -1);
            c[] cVarArr = {UNKNOWN_MOBILE_SUBTYPE, GPRS, EDGE, UMTS, CDMA, EVDO_0, EVDO_A, RTT, HSDPA, HSUPA, HSPA, IDEN, EVDO_B, LTE, EHRPD, HSPAP, GSM, TD_SCDMA, IWLAN, LTE_CA, COMBINED, UNRECOGNIZED};
            new a();
        }
        */

        @DexIgnore
        public c(int i) {
            this.zzw = i;
        }

        @DexIgnore
        public static c zza(int i) {
            if (i == 100) {
                return COMBINED;
            }
            switch (i) {
                case 0:
                    return UNKNOWN_MOBILE_SUBTYPE;
                case 1:
                    return GPRS;
                case 2:
                    return EDGE;
                case 3:
                    return UMTS;
                case 4:
                    return CDMA;
                case 5:
                    return EVDO_0;
                case 6:
                    return EVDO_A;
                case 7:
                    return RTT;
                case 8:
                    return HSDPA;
                case 9:
                    return HSUPA;
                case 10:
                    return HSPA;
                case 11:
                    return IDEN;
                case 12:
                    return EVDO_B;
                case 13:
                    return LTE;
                case 14:
                    return EHRPD;
                case 15:
                    return HSPAP;
                case 16:
                    return GSM;
                case 17:
                    return TD_SCDMA;
                case 18:
                    return IWLAN;
                case 19:
                    return LTE_CA;
                default:
                    return null;
            }
        }

        @DexIgnore
        public final int getNumber() {
            return this.zzw;
        }
    }

    @DexIgnore
    public enum d implements zw3.a {
        MOBILE(0),
        WIFI(1),
        MOBILE_MMS(2),
        MOBILE_SUPL(3),
        MOBILE_DUN(4),
        MOBILE_HIPRI(5),
        WIMAX(6),
        BLUETOOTH(7),
        DUMMY(8),
        ETHERNET(9),
        MOBILE_FOTA(10),
        MOBILE_IMS(11),
        MOBILE_CBS(12),
        WIFI_P2P(13),
        MOBILE_IA(14),
        MOBILE_EMERGENCY(15),
        PROXY(16),
        VPN(17),
        NONE(-1),
        UNRECOGNIZED(-1);
        
        @DexIgnore
        public /* final */ int zzu;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements zw3.b<d> {
        }

        /*
        static {
            MOBILE = new d("MOBILE", 0, 0);
            WIFI = new d("WIFI", 1, 1);
            MOBILE_MMS = new d("MOBILE_MMS", 2, 2);
            MOBILE_SUPL = new d("MOBILE_SUPL", 3, 3);
            MOBILE_DUN = new d("MOBILE_DUN", 4, 4);
            MOBILE_HIPRI = new d("MOBILE_HIPRI", 5, 5);
            WIMAX = new d("WIMAX", 6, 6);
            BLUETOOTH = new d("BLUETOOTH", 7, 7);
            DUMMY = new d("DUMMY", 8, 8);
            ETHERNET = new d("ETHERNET", 9, 9);
            MOBILE_FOTA = new d("MOBILE_FOTA", 10, 10);
            MOBILE_IMS = new d("MOBILE_IMS", 11, 11);
            MOBILE_CBS = new d("MOBILE_CBS", 12, 12);
            WIFI_P2P = new d("WIFI_P2P", 13, 13);
            MOBILE_IA = new d("MOBILE_IA", 14, 14);
            MOBILE_EMERGENCY = new d("MOBILE_EMERGENCY", 15, 15);
            PROXY = new d("PROXY", 16, 16);
            VPN = new d("VPN", 17, 17);
            NONE = new d("NONE", 18, -1);
            UNRECOGNIZED = new d("UNRECOGNIZED", 19, -1);
            d[] dVarArr = {MOBILE, WIFI, MOBILE_MMS, MOBILE_SUPL, MOBILE_DUN, MOBILE_HIPRI, WIMAX, BLUETOOTH, DUMMY, ETHERNET, MOBILE_FOTA, MOBILE_IMS, MOBILE_CBS, WIFI_P2P, MOBILE_IA, MOBILE_EMERGENCY, PROXY, VPN, NONE, UNRECOGNIZED};
            new a();
        }
        */

        @DexIgnore
        public d(int i) {
            this.zzu = i;
        }

        @DexIgnore
        public static d zza(int i) {
            switch (i) {
                case -1:
                    return NONE;
                case 0:
                    return MOBILE;
                case 1:
                    return WIFI;
                case 2:
                    return MOBILE_MMS;
                case 3:
                    return MOBILE_SUPL;
                case 4:
                    return MOBILE_DUN;
                case 5:
                    return MOBILE_HIPRI;
                case 6:
                    return WIMAX;
                case 7:
                    return BLUETOOTH;
                case 8:
                    return DUMMY;
                case 9:
                    return ETHERNET;
                case 10:
                    return MOBILE_FOTA;
                case 11:
                    return MOBILE_IMS;
                case 12:
                    return MOBILE_CBS;
                case 13:
                    return WIFI_P2P;
                case 14:
                    return MOBILE_IA;
                case 15:
                    return MOBILE_EMERGENCY;
                case 16:
                    return PROXY;
                case 17:
                    return VPN;
                default:
                    return null;
            }
        }

        @DexIgnore
        public final int getNumber() {
            return this.zzu;
        }
    }

    /*
    static {
        f.g();
    }
    */

    @DexIgnore
    public static cp1 k() {
        return f;
    }

    @DexIgnore
    public static b l() {
        return (b) f.c();
    }

    @DexIgnore
    public static gx3<cp1> m() {
        return f.e();
    }

    @DexIgnore
    public final Object a(yw3.j jVar, Object obj, Object obj2) {
        boolean z = true;
        boolean z2 = false;
        switch (a.a[jVar.ordinal()]) {
            case 1:
                return new cp1();
            case 2:
                return f;
            case 3:
                return null;
            case 4:
                return new b((a) null);
            case 5:
                yw3.k kVar = (yw3.k) obj;
                cp1 cp1 = (cp1) obj2;
                this.d = kVar.a(this.d != 0, this.d, cp1.d != 0, cp1.d);
                boolean z3 = this.e != 0;
                int i = this.e;
                if (cp1.e == 0) {
                    z = false;
                }
                this.e = kVar.a(z3, i, z, cp1.e);
                yw3.i iVar = yw3.i.a;
                return this;
            case 6:
                tw3 tw3 = (tw3) obj;
                ww3 ww3 = (ww3) obj2;
                while (!z2) {
                    try {
                        int l = tw3.l();
                        if (l != 0) {
                            if (l == 8) {
                                this.d = tw3.c();
                            } else if (l == 16) {
                                this.e = tw3.c();
                            } else if (!tw3.f(l)) {
                            }
                        }
                        z2 = true;
                    } catch (ax3 e2) {
                        throw new RuntimeException(e2.setUnfinishedMessage(this));
                    } catch (IOException e3) {
                        throw new RuntimeException(new ax3(e3.getMessage()).setUnfinishedMessage(this));
                    }
                }
                break;
            case 7:
                break;
            case 8:
                if (g == null) {
                    synchronized (cp1.class) {
                        if (g == null) {
                            g = new yw3.c(f);
                        }
                    }
                }
                return g;
            default:
                throw new UnsupportedOperationException();
        }
        return f;
    }

    @DexIgnore
    public int d() {
        int i = this.c;
        if (i != -1) {
            return i;
        }
        int i2 = 0;
        if (this.d != d.MOBILE.getNumber()) {
            i2 = 0 + uw3.c(1, this.d);
        }
        if (this.e != c.UNKNOWN_MOBILE_SUBTYPE.getNumber()) {
            i2 += uw3.c(2, this.e);
        }
        this.c = i2;
        return i2;
    }

    @DexIgnore
    public void a(uw3 uw3) throws IOException {
        if (this.d != d.MOBILE.getNumber()) {
            uw3.a(1, this.d);
        }
        if (this.e != c.UNKNOWN_MOBILE_SUBTYPE.getNumber()) {
            uw3.a(2, this.e);
        }
    }
}
