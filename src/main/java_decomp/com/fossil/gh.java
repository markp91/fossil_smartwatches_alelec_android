package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gh<T> extends vh {
    @DexIgnore
    public gh(oh ohVar) {
        super(ohVar);
    }

    @DexIgnore
    public abstract void bind(mi miVar, T t);

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public final int handle(T t) {
        mi acquire = acquire();
        try {
            bind(acquire, t);
            return acquire.s();
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(Iterable<? extends T> iterable) {
        mi acquire = acquire();
        int i = 0;
        try {
            for (Object bind : iterable) {
                bind(acquire, bind);
                i += acquire.s();
            }
            return i;
        } finally {
            release(acquire);
        }
    }

    @DexIgnore
    public final int handleMultiple(T[] tArr) {
        mi acquire = acquire();
        try {
            int i = 0;
            for (T bind : tArr) {
                bind(acquire, bind);
                i += acquire.s();
            }
            return i;
        } finally {
            release(acquire);
        }
    }
}
