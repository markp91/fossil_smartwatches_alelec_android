package com.fossil;

import com.fossil.m24;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dt4 extends m24<m24.b, a, m24.a> {
    @DexIgnore
    public /* final */ UserRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements CoroutineUseCase.d {
        @DexIgnore
        public /* final */ MFUser a;

        @DexIgnore
        public a(MFUser mFUser) {
            this.a = mFUser;
        }

        @DexIgnore
        public final MFUser a() {
            return this.a;
        }
    }

    @DexIgnore
    public dt4(UserRepository userRepository) {
        wg6.b(userRepository, "mUserRepository");
        this.d = userRepository;
    }

    @DexIgnore
    public Object a(m24.b bVar, xe6<Object> xe6) {
        return new a(this.d.getCurrentUser());
    }

    @DexIgnore
    public String c() {
        return "GetUser";
    }
}
