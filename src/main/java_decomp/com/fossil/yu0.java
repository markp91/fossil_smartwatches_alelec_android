package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum yu0 {
    UNSUPPORTED_VERSION(1),
    INVALID_FILE_DATA(2);

    @DexIgnore
    public yu0(int i) {
    }
}
