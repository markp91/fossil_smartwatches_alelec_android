package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ze1<T> extends ed1<T> {
    @DexIgnore
    public /* final */ q11 d;

    @DexIgnore
    public ze1(w31 w31, w40 w40) {
        super(w31, w40);
        this.d = q11.CRC32C;
    }

    @DexIgnore
    public q11 a() {
        return this.d;
    }

    @DexIgnore
    public ze1(w31 w31) {
        this(w31, new w40((byte) 2, (byte) 0));
    }
}
