package com.fossil;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn1 extends u31<zd0[]> {
    @DexIgnore
    public static /* final */ rn1 b; // = new rn1();

    @DexIgnore
    public rn1() {
        super(new w40((byte) 2, (byte) 0));
    }

    @DexIgnore
    public byte[] a(short s, Object obj) {
        zd0[] zd0Arr = (zd0[]) obj;
        byte[] a = a(zd0Arr);
        byte[] array = ByteBuffer.allocate(a.length + 12 + 4).order(ByteOrder.LITTLE_ENDIAN).putShort(s).put(this.a.getMajor()).put(this.a.getMinor()).putShort(0).putShort((short) b(zd0Arr)).putInt(a.length).put(a).putInt((int) h51.a.a(a, q11.CRC32C)).array();
        wg6.a(array, "result.array()");
        return array;
    }

    @DexIgnore
    public final int b(zd0[] zd0Arr) {
        ArrayList arrayList = new ArrayList(zd0Arr.length);
        for (zd0 replyMessageGroup : zd0Arr) {
            arrayList.add(replyMessageGroup.getReplyMessageGroup());
        }
        return (yd6.c(arrayList).size() * 11) + 2;
    }

    @DexIgnore
    public byte[] a(zd0[] zd0Arr) {
        int b2 = b(zd0Arr);
        ArrayList arrayList = new ArrayList(zd0Arr.length);
        for (zd0 replyMessageGroup : zd0Arr) {
            arrayList.add(replyMessageGroup.getReplyMessageGroup());
        }
        List<yd0> c = yd6.c(arrayList);
        ByteBuffer order = ByteBuffer.allocate(b(zd0Arr)).order(ByteOrder.LITTLE_ENDIAN);
        order.put((byte) 2).put((byte) 11);
        int i = 0;
        for (yd0 yd0 : c) {
            ArrayList<zd0> arrayList2 = new ArrayList<>();
            for (zd0 zd0 : zd0Arr) {
                if (wg6.a(zd0.getReplyMessageGroup(), yd0)) {
                    arrayList2.add(zd0);
                }
            }
            int i2 = 0;
            for (zd0 notificationType : arrayList2) {
                i2 |= notificationType.getNotificationType().a();
            }
            order.putShort((short) i2).put((byte) yd0.getReplyMessages().length).putInt(b2 + 12 + i).putInt(yd0.b().length);
            i += yd0.b().length;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byteArrayOutputStream.write(order.array());
        for (yd0 b3 : c) {
            byteArrayOutputStream.write(b3.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "entriesDataInByte.toByteArray()");
        return byteArray;
    }
}
