package com.fossil;

import com.fossil.b00;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class a00<R> implements b00<R> {
    @DexIgnore
    public static /* final */ a00<?> a; // = new a00<>();
    @DexIgnore
    public static /* final */ c00<?> b; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<R> implements c00<R> {
        @DexIgnore
        public b00<R> a(pr prVar, boolean z) {
            return a00.a;
        }
    }

    @DexIgnore
    public static <R> c00<R> a() {
        return b;
    }

    @DexIgnore
    public boolean a(Object obj, b00.a aVar) {
        return false;
    }
}
