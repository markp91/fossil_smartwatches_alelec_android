package com.fossil;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class hm4 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnTouchListener {
        @DexIgnore
        public long a;
        @DexIgnore
        public float b;
        @DexIgnore
        public /* final */ /* synthetic */ b c;
        @DexIgnore
        public /* final */ /* synthetic */ View d;

        @DexIgnore
        public a(hm4 hm4, b bVar, View view) {
            this.c = bVar;
            this.d = view;
        }

        @DexIgnore
        @SuppressLint({"ClickableViewAccessibility"})
        public boolean onTouch(View view, MotionEvent motionEvent) {
            int action = motionEvent.getAction();
            if (action != 0) {
                boolean z = false;
                if (action != 1) {
                    if (action == 2) {
                        if (System.currentTimeMillis() - this.a > 300) {
                            this.c.a(this.d);
                        }
                        return true;
                    } else if (action != 3) {
                        return false;
                    }
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.a > 300) {
                    this.c.b(this.d);
                } else {
                    this.c.onClick(this.d);
                }
                this.c.d(this.d);
                float x = motionEvent.getX() - this.b;
                if (x < 0.0f) {
                    z = true;
                }
                float a2 = (float) (qk4.b().a() / 3);
                float f = x / ((float) (currentTimeMillis - this.a));
                float f2 = x / 200.0f;
                if ((Math.abs(x) > ((float) (qk4.b().a() / 10)) && Math.abs(f) > f2) || x > a2) {
                    this.c.a(this.d, Boolean.valueOf(z));
                }
                this.b = 0.0f;
                this.a = 0;
                return true;
            }
            this.a = System.currentTimeMillis();
            this.c.c(this.d);
            this.b = motionEvent.getX();
            return true;
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(View view);

        @DexIgnore
        void a(View view, Boolean bool);

        @DexIgnore
        void b(View view);

        @DexIgnore
        void c(View view);

        @DexIgnore
        void d(View view);

        @DexIgnore
        void onClick(View view);
    }

    @DexIgnore
    public void a(View view, b bVar) {
        view.setOnTouchListener(new a(this, bVar, view));
    }
}
