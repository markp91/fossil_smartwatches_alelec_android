package com.fossil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class au3 {
    @DexIgnore
    public /* final */ Field a;

    @DexIgnore
    public au3(Field field) {
        yu3.a(field);
        this.a = field;
    }

    @DexIgnore
    public <T extends Annotation> T a(Class<T> cls) {
        return this.a.getAnnotation(cls);
    }
}
