package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface f86<T> {
    @DexIgnore
    public static final f86 a = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements f86<Object> {
        @DexIgnore
        public void a(Exception exc) {
        }

        @DexIgnore
        public void a(Object obj) {
        }

        @DexIgnore
        public b() {
        }
    }

    @DexIgnore
    void a(Exception exc);

    @DexIgnore
    void a(T t);
}
