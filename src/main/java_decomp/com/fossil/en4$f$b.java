package com.fossil;

import com.portfolio.platform.manager.WeatherManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class en4$f$b extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ double $currentLat;
    @DexIgnore
    public /* final */ /* synthetic */ double $currentLong;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WeatherManager.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public en4$f$b(WeatherManager.f fVar, double d, double d2, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$currentLat = d;
        this.$currentLong = d2;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        en4$f$b en4_f_b = new en4$f$b(this.this$0, this.$currentLat, this.$currentLong, xe6);
        en4_f_b.p$ = (il6) obj;
        return en4_f_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((en4$f$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            WeatherManager weatherManager = this.this$0.this$0;
            double d = this.$currentLat;
            double d2 = this.$currentLong;
            this.L$0 = il6;
            this.label = 1;
            obj = weatherManager.a(d, d2, (xe6<? super String>) this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
