package com.fossil;

import android.util.Log;
import com.fossil.bt;
import com.fossil.fs;
import com.fossil.jv;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vt implements bt, bt.a {
    @DexIgnore
    public /* final */ ct<?> a;
    @DexIgnore
    public /* final */ bt.a b;
    @DexIgnore
    public int c;
    @DexIgnore
    public ys d;
    @DexIgnore
    public Object e;
    @DexIgnore
    public volatile jv.a<?> f;
    @DexIgnore
    public zs g;

    @DexIgnore
    public vt(ct<?> ctVar, bt.a aVar) {
        this.a = ctVar;
        this.b = aVar;
    }

    @DexIgnore
    public boolean a() {
        Object obj = this.e;
        if (obj != null) {
            this.e = null;
            a(obj);
        }
        ys ysVar = this.d;
        if (ysVar != null && ysVar.a()) {
            return true;
        }
        this.d = null;
        this.f = null;
        boolean z = false;
        while (!z && c()) {
            List<jv.a<?>> g2 = this.a.g();
            int i = this.c;
            this.c = i + 1;
            this.f = g2.get(i);
            if (this.f != null && (this.a.e().a(this.f.c.b()) || this.a.c(this.f.c.getDataClass()))) {
                b(this.f);
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public final void b(jv.a<?> aVar) {
        this.f.c.a(this.a.j(), new a(aVar));
    }

    @DexIgnore
    public final boolean c() {
        return this.c < this.a.g().size();
    }

    @DexIgnore
    public void cancel() {
        jv.a<?> aVar = this.f;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements fs.a<Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jv.a a;

        @DexIgnore
        public a(jv.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(Object obj) {
            if (vt.this.a((jv.a<?>) this.a)) {
                vt.this.a((jv.a<?>) this.a, obj);
            }
        }

        @DexIgnore
        public void a(Exception exc) {
            if (vt.this.a((jv.a<?>) this.a)) {
                vt.this.a((jv.a<?>) this.a, exc);
            }
        }
    }

    @DexIgnore
    public void b() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public boolean a(jv.a<?> aVar) {
        jv.a<?> aVar2 = this.f;
        return aVar2 != null && aVar2 == aVar;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(Object obj) {
        long a2 = m00.a();
        try {
            sr<X> a3 = this.a.a(obj);
            at atVar = new at(a3, obj, this.a.i());
            this.g = new zs(this.f.a, this.a.l());
            this.a.d().a(this.g, atVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.g + ", data: " + obj + ", encoder: " + a3 + ", duration: " + m00.a(a2));
            }
            this.f.c.a();
            this.d = new ys(Collections.singletonList(this.f.a), this.a, this);
        } catch (Throwable th) {
            this.f.c.a();
            throw th;
        }
    }

    @DexIgnore
    public void a(jv.a<?> aVar, Object obj) {
        ft e2 = this.a.e();
        if (obj == null || !e2.a(aVar.c.b())) {
            bt.a aVar2 = this.b;
            vr vrVar = aVar.a;
            fs<Data> fsVar = aVar.c;
            aVar2.a(vrVar, obj, fsVar, fsVar.b(), this.g);
            return;
        }
        this.e = obj;
        this.b.b();
    }

    @DexIgnore
    public void a(jv.a<?> aVar, Exception exc) {
        bt.a aVar2 = this.b;
        zs zsVar = this.g;
        fs<Data> fsVar = aVar.c;
        aVar2.a(zsVar, exc, fsVar, fsVar.b());
    }

    @DexIgnore
    public void a(vr vrVar, Object obj, fs<?> fsVar, pr prVar, vr vrVar2) {
        this.b.a(vrVar, obj, fsVar, this.f.c.b(), vrVar);
    }

    @DexIgnore
    public void a(vr vrVar, Exception exc, fs<?> fsVar, pr prVar) {
        this.b.a(vrVar, exc, fsVar, this.f.c.b());
    }
}
