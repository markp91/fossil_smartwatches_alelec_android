package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;
    @DexIgnore
    public /* final */ RecyclerView t;

    @DexIgnore
    public zc4(Object obj, View view, int i, FlexibleTextView flexibleTextView, ImageView imageView, ConstraintLayout constraintLayout, RecyclerView recyclerView) {
        super(obj, view, i);
        this.q = flexibleTextView;
        this.r = imageView;
        this.s = constraintLayout;
        this.t = recyclerView;
    }
}
