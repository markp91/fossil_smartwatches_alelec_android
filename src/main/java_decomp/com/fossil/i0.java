package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i0 {
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat; // = 2131951617;
    @DexIgnore
    public static /* final */ int AlertDialog_AppCompat_Light; // = 2131951618;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Dialog; // = 2131951622;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_DropDownUp; // = 2131951623;
    @DexIgnore
    public static /* final */ int Animation_AppCompat_Tooltip; // = 2131951624;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat; // = 2131951757;
    @DexIgnore
    public static /* final */ int Base_AlertDialog_AppCompat_Light; // = 2131951758;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Dialog; // = 2131951759;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_DropDownUp; // = 2131951760;
    @DexIgnore
    public static /* final */ int Base_Animation_AppCompat_Tooltip; // = 2131951761;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitleBackground_AppCompat; // = 2131951764;
    @DexIgnore
    public static /* final */ int Base_DialogWindowTitle_AppCompat; // = 2131951763;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat; // = 2131951768;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body1; // = 2131951769;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Body2; // = 2131951770;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Button; // = 2131951771;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Caption; // = 2131951772;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display1; // = 2131951773;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display2; // = 2131951774;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display3; // = 2131951775;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Display4; // = 2131951776;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Headline; // = 2131951777;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Inverse; // = 2131951778;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large; // = 2131951779;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Large_Inverse; // = 2131951780;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131951781;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131951782;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium; // = 2131951783;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Medium_Inverse; // = 2131951784;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Menu; // = 2131951785;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult; // = 2131951786;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131951787;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_SearchResult_Title; // = 2131951788;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small; // = 2131951789;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Small_Inverse; // = 2131951790;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead; // = 2131951791;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Subhead_Inverse; // = 2131951792;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title; // = 2131951793;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Title_Inverse; // = 2131951794;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Tooltip; // = 2131951795;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131951796;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131951797;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131951798;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131951799;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131951800;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131951801;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131951802;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button; // = 2131951803;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131951804;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Colored; // = 2131951805;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131951806;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_DropDownItem; // = 2131951807;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131951808;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131951809;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131951810;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_Switch; // = 2131951811;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131951812;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131951817;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131951818;
    @DexIgnore
    public static /* final */ int Base_TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131951819;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat; // = 2131951853;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_ActionBar; // = 2131951854;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark; // = 2131951855;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131951856;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog; // = 2131951857;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Dialog_Alert; // = 2131951858;
    @DexIgnore
    public static /* final */ int Base_ThemeOverlay_AppCompat_Light; // = 2131951859;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat; // = 2131951820;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_CompactMenu; // = 2131951821;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog; // = 2131951822;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_DialogWhenLarge; // = 2131951826;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_Alert; // = 2131951823;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_FixedSize; // = 2131951824;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Dialog_MinWidth; // = 2131951825;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light; // = 2131951827;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DarkActionBar; // = 2131951828;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog; // = 2131951829;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_DialogWhenLarge; // = 2131951833;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_Alert; // = 2131951830;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_FixedSize; // = 2131951831;
    @DexIgnore
    public static /* final */ int Base_Theme_AppCompat_Light_Dialog_MinWidth; // = 2131951832;
    @DexIgnore
    public static /* final */ int Base_V21_ThemeOverlay_AppCompat_Dialog; // = 2131951879;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat; // = 2131951875;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Dialog; // = 2131951876;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light; // = 2131951877;
    @DexIgnore
    public static /* final */ int Base_V21_Theme_AppCompat_Light_Dialog; // = 2131951878;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat; // = 2131951880;
    @DexIgnore
    public static /* final */ int Base_V22_Theme_AppCompat_Light; // = 2131951881;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat; // = 2131951882;
    @DexIgnore
    public static /* final */ int Base_V23_Theme_AppCompat_Light; // = 2131951883;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat; // = 2131951884;
    @DexIgnore
    public static /* final */ int Base_V26_Theme_AppCompat_Light; // = 2131951885;
    @DexIgnore
    public static /* final */ int Base_V26_Widget_AppCompat_Toolbar; // = 2131951886;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat; // = 2131951887;
    @DexIgnore
    public static /* final */ int Base_V28_Theme_AppCompat_Light; // = 2131951888;
    @DexIgnore
    public static /* final */ int Base_V7_ThemeOverlay_AppCompat_Dialog; // = 2131951893;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat; // = 2131951889;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Dialog; // = 2131951890;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light; // = 2131951891;
    @DexIgnore
    public static /* final */ int Base_V7_Theme_AppCompat_Light_Dialog; // = 2131951892;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_AutoCompleteTextView; // = 2131951894;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_EditText; // = 2131951895;
    @DexIgnore
    public static /* final */ int Base_V7_Widget_AppCompat_Toolbar; // = 2131951896;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar; // = 2131951897;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_Solid; // = 2131951898;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabBar; // = 2131951899;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabText; // = 2131951900;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionBar_TabView; // = 2131951901;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton; // = 2131951902;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_CloseMode; // = 2131951903;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionButton_Overflow; // = 2131951904;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActionMode; // = 2131951905;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ActivityChooserView; // = 2131951906;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_AutoCompleteTextView; // = 2131951907;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button; // = 2131951908;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar; // = 2131951914;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ButtonBar_AlertDialog; // = 2131951915;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless; // = 2131951909;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Borderless_Colored; // = 2131951910;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131951911;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Colored; // = 2131951912;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Button_Small; // = 2131951913;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_CheckBox; // = 2131951916;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_RadioButton; // = 2131951917;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_CompoundButton_Switch; // = 2131951918;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle; // = 2131951919;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DrawerArrowToggle_Common; // = 2131951920;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_DropDownItem_Spinner; // = 2131951921;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_EditText; // = 2131951922;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ImageButton; // = 2131951923;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar; // = 2131951924;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_Solid; // = 2131951925;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabBar; // = 2131951926;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText; // = 2131951927;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131951928;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_ActionBar_TabView; // = 2131951929;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu; // = 2131951930;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131951931;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListMenuView; // = 2131951932;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListPopupWindow; // = 2131951933;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView; // = 2131951934;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_DropDown; // = 2131951935;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ListView_Menu; // = 2131951936;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu; // = 2131951937;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupMenu_Overflow; // = 2131951938;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_PopupWindow; // = 2131951939;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar; // = 2131951940;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_ProgressBar_Horizontal; // = 2131951941;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar; // = 2131951942;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Indicator; // = 2131951943;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_RatingBar_Small; // = 2131951944;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView; // = 2131951945;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SearchView_ActionBar; // = 2131951946;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar; // = 2131951947;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_SeekBar_Discrete; // = 2131951948;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner; // = 2131951949;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Spinner_Underlined; // = 2131951950;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView; // = 2131951951;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_TextView_SpinnerItem; // = 2131951952;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar; // = 2131951953;
    @DexIgnore
    public static /* final */ int Base_Widget_AppCompat_Toolbar_Button_Navigation; // = 2131951954;
    @DexIgnore
    public static /* final */ int Platform_AppCompat; // = 2131952082;
    @DexIgnore
    public static /* final */ int Platform_AppCompat_Light; // = 2131952083;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat; // = 2131952088;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Dark; // = 2131952089;
    @DexIgnore
    public static /* final */ int Platform_ThemeOverlay_AppCompat_Light; // = 2131952090;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat; // = 2131952091;
    @DexIgnore
    public static /* final */ int Platform_V21_AppCompat_Light; // = 2131952092;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat; // = 2131952093;
    @DexIgnore
    public static /* final */ int Platform_V25_AppCompat_Light; // = 2131952094;
    @DexIgnore
    public static /* final */ int Platform_Widget_AppCompat_Spinner; // = 2131952095;
    @DexIgnore
    public static /* final */ int RtlOverlay_DialogWindowTitle_AppCompat; // = 2131952104;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_ActionBar_TitleItem; // = 2131952105;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_DialogTitle_Icon; // = 2131952106;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem; // = 2131952107;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_InternalGroup; // = 2131952108;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Shortcut; // = 2131952109;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_SubmenuArrow; // = 2131952110;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Text; // = 2131952111;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_PopupMenuItem_Title; // = 2131952112;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_SearchView_MagIcon; // = 2131952118;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown; // = 2131952113;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon1; // = 2131952114;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Icon2; // = 2131952115;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Query; // = 2131952116;
    @DexIgnore
    public static /* final */ int RtlOverlay_Widget_AppCompat_Search_DropDown_Text; // = 2131952117;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton; // = 2131952119;
    @DexIgnore
    public static /* final */ int RtlUnderlay_Widget_AppCompat_ActionButton_Overflow; // = 2131952120;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat; // = 2131952162;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body1; // = 2131952163;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Body2; // = 2131952164;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Button; // = 2131952165;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Caption; // = 2131952166;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display1; // = 2131952167;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display2; // = 2131952168;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display3; // = 2131952169;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Display4; // = 2131952170;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Headline; // = 2131952171;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Inverse; // = 2131952172;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large; // = 2131952173;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Large_Inverse; // = 2131952174;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Subtitle; // = 2131952175;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_SearchResult_Title; // = 2131952176;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Large; // = 2131952177;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Light_Widget_PopupMenu_Small; // = 2131952178;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium; // = 2131952179;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Medium_Inverse; // = 2131952180;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Menu; // = 2131952181;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Subtitle; // = 2131952182;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_SearchResult_Title; // = 2131952183;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small; // = 2131952184;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Small_Inverse; // = 2131952185;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead; // = 2131952186;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Subhead_Inverse; // = 2131952187;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title; // = 2131952188;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Title_Inverse; // = 2131952189;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Tooltip; // = 2131952190;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Menu; // = 2131952191;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle; // = 2131952192;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Subtitle_Inverse; // = 2131952193;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title; // = 2131952194;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionBar_Title_Inverse; // = 2131952195;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle; // = 2131952196;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Subtitle_Inverse; // = 2131952197;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title; // = 2131952198;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_ActionMode_Title_Inverse; // = 2131952199;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button; // = 2131952200;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Borderless_Colored; // = 2131952201;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Colored; // = 2131952202;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Button_Inverse; // = 2131952203;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_DropDownItem; // = 2131952204;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Header; // = 2131952205;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Large; // = 2131952206;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_PopupMenu_Small; // = 2131952207;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_Switch; // = 2131952208;
    @DexIgnore
    public static /* final */ int TextAppearance_AppCompat_Widget_TextView_SpinnerItem; // = 2131952209;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification; // = 2131952210;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Info; // = 2131952211;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Line2; // = 2131952213;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Time; // = 2131952216;
    @DexIgnore
    public static /* final */ int TextAppearance_Compat_Notification_Title; // = 2131952218;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_ExpandedMenu_Item; // = 2131952243;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Subtitle; // = 2131952244;
    @DexIgnore
    public static /* final */ int TextAppearance_Widget_AppCompat_Toolbar_Title; // = 2131952245;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat; // = 2131952331;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_ActionBar; // = 2131952332;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark; // = 2131952333;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dark_ActionBar; // = 2131952334;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight; // = 2131952335;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_DayNight_ActionBar; // = 2131952336;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog; // = 2131952337;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Dialog_Alert; // = 2131952338;
    @DexIgnore
    public static /* final */ int ThemeOverlay_AppCompat_Light; // = 2131952339;
    @DexIgnore
    public static /* final */ int Theme_AppCompat; // = 2131952255;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_CompactMenu; // = 2131952256;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight; // = 2131952257;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DarkActionBar; // = 2131952258;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog; // = 2131952259;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_DialogWhenLarge; // = 2131952262;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_Alert; // = 2131952260;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_Dialog_MinWidth; // = 2131952261;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DayNight_NoActionBar; // = 2131952263;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog; // = 2131952264;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_DialogWhenLarge; // = 2131952267;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_Alert; // = 2131952265;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Dialog_MinWidth; // = 2131952266;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light; // = 2131952268;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DarkActionBar; // = 2131952269;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog; // = 2131952270;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_DialogWhenLarge; // = 2131952273;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_Alert; // = 2131952271;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_Dialog_MinWidth; // = 2131952272;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_Light_NoActionBar; // = 2131952274;
    @DexIgnore
    public static /* final */ int Theme_AppCompat_NoActionBar; // = 2131952275;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar; // = 2131952388;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_Solid; // = 2131952389;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabBar; // = 2131952390;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabText; // = 2131952391;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionBar_TabView; // = 2131952392;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton; // = 2131952393;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_CloseMode; // = 2131952394;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionButton_Overflow; // = 2131952395;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActionMode; // = 2131952396;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ActivityChooserView; // = 2131952397;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_AutoCompleteTextView; // = 2131952398;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button; // = 2131952399;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar; // = 2131952405;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ButtonBar_AlertDialog; // = 2131952406;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless; // = 2131952400;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Borderless_Colored; // = 2131952401;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_ButtonBar_AlertDialog; // = 2131952402;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Colored; // = 2131952403;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Button_Small; // = 2131952404;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_CheckBox; // = 2131952407;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_RadioButton; // = 2131952408;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_CompoundButton_Switch; // = 2131952409;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DrawerArrowToggle; // = 2131952410;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_DropDownItem_Spinner; // = 2131952411;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_EditText; // = 2131952412;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ImageButton; // = 2131952413;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar; // = 2131952414;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid; // = 2131952415;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_Solid_Inverse; // = 2131952416;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar; // = 2131952417;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabBar_Inverse; // = 2131952418;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText; // = 2131952419;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabText_Inverse; // = 2131952420;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView; // = 2131952421;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionBar_TabView_Inverse; // = 2131952422;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton; // = 2131952423;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_CloseMode; // = 2131952424;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionButton_Overflow; // = 2131952425;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActionMode_Inverse; // = 2131952426;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ActivityChooserView; // = 2131952427;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_AutoCompleteTextView; // = 2131952428;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_DropDownItem_Spinner; // = 2131952429;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListPopupWindow; // = 2131952430;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_ListView_DropDown; // = 2131952431;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu; // = 2131952432;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_PopupMenu_Overflow; // = 2131952433;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_SearchView; // = 2131952434;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Light_Spinner_DropDown_ActionBar; // = 2131952435;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListMenuView; // = 2131952436;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListPopupWindow; // = 2131952437;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView; // = 2131952438;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_DropDown; // = 2131952439;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ListView_Menu; // = 2131952440;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu; // = 2131952441;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupMenu_Overflow; // = 2131952442;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_PopupWindow; // = 2131952443;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar; // = 2131952444;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_ProgressBar_Horizontal; // = 2131952445;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar; // = 2131952446;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Indicator; // = 2131952447;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_RatingBar_Small; // = 2131952448;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView; // = 2131952449;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SearchView_ActionBar; // = 2131952450;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar; // = 2131952451;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_SeekBar_Discrete; // = 2131952452;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner; // = 2131952453;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown; // = 2131952454;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_DropDown_ActionBar; // = 2131952455;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Spinner_Underlined; // = 2131952456;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView; // = 2131952457;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_TextView_SpinnerItem; // = 2131952458;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar; // = 2131952459;
    @DexIgnore
    public static /* final */ int Widget_AppCompat_Toolbar_Button_Navigation; // = 2131952460;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionContainer; // = 2131952462;
    @DexIgnore
    public static /* final */ int Widget_Compat_NotificationActionText; // = 2131952463;
}
