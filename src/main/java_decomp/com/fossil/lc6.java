package com.fossil;

import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lc6<A, B> implements Serializable {
    @DexIgnore
    public /* final */ A first;
    @DexIgnore
    public /* final */ B second;

    @DexIgnore
    public lc6(A a, B b) {
        this.first = a;
        this.second = b;
    }

    @DexIgnore
    public static /* synthetic */ lc6 copy$default(lc6 lc6, A a, B b, int i, Object obj) {
        if ((i & 1) != 0) {
            a = lc6.first;
        }
        if ((i & 2) != 0) {
            b = lc6.second;
        }
        return lc6.copy(a, b);
    }

    @DexIgnore
    public final A component1() {
        return this.first;
    }

    @DexIgnore
    public final B component2() {
        return this.second;
    }

    @DexIgnore
    public final lc6<A, B> copy(A a, B b) {
        return new lc6<>(a, b);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof lc6)) {
            return false;
        }
        lc6 lc6 = (lc6) obj;
        return wg6.a((Object) this.first, (Object) lc6.first) && wg6.a((Object) this.second, (Object) lc6.second);
    }

    @DexIgnore
    public final A getFirst() {
        return this.first;
    }

    @DexIgnore
    public final B getSecond() {
        return this.second;
    }

    @DexIgnore
    public int hashCode() {
        A a = this.first;
        int i = 0;
        int hashCode = (a != null ? a.hashCode() : 0) * 31;
        B b = this.second;
        if (b != null) {
            i = b.hashCode();
        }
        return hashCode + i;
    }

    @DexIgnore
    public String toString() {
        return '(' + this.first + ", " + this.second + ')';
    }
}
