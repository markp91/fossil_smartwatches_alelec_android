package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleTextView;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d34 extends RecyclerView.g<a> {
    @DexIgnore
    public ArrayList<String> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ FlexibleTextView a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(View view) {
            super(view);
            wg6.b(view, "itemView");
            View findViewById = view.findViewById(2131362324);
            wg6.a((Object) findViewById, "itemView.findViewById(R.id.ftv_description)");
            this.a = (FlexibleTextView) findViewById;
            View findViewById2 = view.findViewById(2131362566);
            wg6.a((Object) findViewById2, "itemView.findViewById(R.id.iv_device)");
            ImageView imageView = (ImageView) findViewById2;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v0, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView] */
        public final void a(String str) {
            if (str != null) {
                this.a.setText(str);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new b((qg6) null);
    }
    */

    @DexIgnore
    public final void a(List<String> list) {
        wg6.b(list, "data");
        this.a.clear();
        this.a.addAll(list);
        notifyDataSetChanged();
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.size();
    }

    @DexIgnore
    public a onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "viewGroup");
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(2131558677, viewGroup, false);
        wg6.a((Object) inflate, "view");
        return new a(inflate);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(a aVar, int i) {
        wg6.b(aVar, "viewHolder");
        aVar.a(this.a.get(i));
    }
}
