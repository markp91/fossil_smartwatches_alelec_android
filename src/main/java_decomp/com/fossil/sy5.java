package com.fossil;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.graphics.drawable.shapes.RectShape;
import android.graphics.drawable.shapes.RoundRectShape;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sy5 extends ShapeDrawable {
    @DexIgnore
    public /* final */ Paint a;
    @DexIgnore
    public /* final */ Paint b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ RectShape e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ float i;
    @DexIgnore
    public /* final */ int j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c, d, b {
        @DexIgnore
        public String a; // = "";
        @DexIgnore
        public int b; // = -7829368;
        @DexIgnore
        public int c; // = 0;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public Typeface f; // = Typeface.create("sans-serif-light", 0);
        @DexIgnore
        public RectShape g; // = new RectShape();
        @DexIgnore
        public int h; // = -1;
        @DexIgnore
        public int i; // = -1;
        @DexIgnore
        public boolean j; // = false;
        @DexIgnore
        public boolean k; // = false;
        @DexIgnore
        public float l;

        @DexIgnore
        public c a(int i2) {
            this.d = i2;
            return this;
        }

        @DexIgnore
        public d a() {
            return this;
        }

        @DexIgnore
        public c b() {
            return this;
        }

        @DexIgnore
        public c b(int i2) {
            this.i = i2;
            return this;
        }

        @DexIgnore
        public c c(int i2) {
            this.e = i2;
            return this;
        }

        @DexIgnore
        public c d(int i2) {
            this.h = i2;
            return this;
        }

        @DexIgnore
        public c a(Typeface typeface) {
            this.f = typeface;
            return this;
        }

        @DexIgnore
        public sy5 b(String str, int i2) {
            this.b = i2;
            this.a = str;
            return new sy5(this);
        }

        @DexIgnore
        public b c() {
            this.g = new RectShape();
            return this;
        }

        @DexIgnore
        public sy5 a(String str, int i2) {
            c();
            return b(str, i2);
        }
    }

    @DexIgnore
    public interface b {
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        c a(int i);

        @DexIgnore
        c a(Typeface typeface);

        @DexIgnore
        d a();

        @DexIgnore
        c b(int i);

        @DexIgnore
        c c(int i);

        @DexIgnore
        c d(int i);
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        sy5 a(String str, int i);

        @DexIgnore
        c b();
    }

    @DexIgnore
    public sy5(a aVar) {
        super(aVar.g);
        this.e = aVar.g;
        this.f = aVar.e;
        this.g = aVar.d;
        this.i = aVar.l;
        this.c = aVar.k ? aVar.a.toUpperCase() : aVar.a;
        this.d = aVar.b;
        this.h = aVar.i;
        this.a = new Paint();
        this.a.setColor(aVar.h);
        this.a.setAntiAlias(true);
        this.a.setFakeBoldText(aVar.j);
        this.a.setStyle(Paint.Style.FILL);
        this.a.setTypeface(aVar.f);
        this.a.setTextAlign(Paint.Align.CENTER);
        this.a.setStrokeWidth((float) aVar.c);
        this.j = aVar.c;
        this.b = new Paint();
        this.b.setAntiAlias(true);
        this.b.setColor(a(this.d));
        this.b.setStyle(Paint.Style.STROKE);
        this.b.setStrokeWidth((float) this.j);
        Paint paint = getPaint();
        paint.setAntiAlias(true);
        paint.setColor(this.d);
    }

    @DexIgnore
    public final int a(int i2) {
        return Color.rgb((int) (((float) Color.red(i2)) * 0.9f), (int) (((float) Color.green(i2)) * 0.9f), (int) (((float) Color.blue(i2)) * 0.9f));
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        super.draw(canvas);
        Rect bounds = getBounds();
        if (this.j > 0) {
            a(canvas);
        }
        int save = canvas.save();
        canvas.translate((float) bounds.left, (float) bounds.top);
        int i2 = this.g;
        if (i2 < 0) {
            i2 = bounds.width();
        }
        int i3 = this.f;
        if (i3 < 0) {
            i3 = bounds.height();
        }
        int i4 = this.h;
        if (i4 < 0) {
            i4 = Math.min(i2, i3) / 2;
        }
        this.a.setTextSize((float) i4);
        canvas.drawText(this.c, (float) (i2 / 2), ((float) (i3 / 2)) - ((this.a.descent() + (this.a.ascent() / 2.0f)) + (this.a.ascent() / 6.0f)), this.a);
        canvas.restoreToCount(save);
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.f;
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.g;
    }

    @DexIgnore
    public int getOpacity() {
        return -3;
    }

    @DexIgnore
    public void setAlpha(int i2) {
        this.a.setAlpha(i2);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
    }

    @DexIgnore
    public final void a(Canvas canvas) {
        RectF rectF = new RectF(getBounds());
        int i2 = this.j;
        rectF.inset((float) (i2 / 2), (float) (i2 / 2));
        RectShape rectShape = this.e;
        if (rectShape instanceof OvalShape) {
            canvas.drawOval(rectF, this.b);
        } else if (rectShape instanceof RoundRectShape) {
            float f2 = this.i;
            canvas.drawRoundRect(rectF, f2, f2, this.b);
        } else {
            canvas.drawRect(rectF, this.b);
        }
    }

    @DexIgnore
    public static d a() {
        return new a();
    }
}
