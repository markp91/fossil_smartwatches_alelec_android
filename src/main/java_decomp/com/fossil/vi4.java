package com.fossil;

import android.text.TextUtils;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.Ringtone;
import com.portfolio.platform.data.model.diana.commutetime.AddressWrapper;
import com.portfolio.platform.data.model.diana.weather.WeatherLocationWrapper;
import com.portfolio.platform.data.model.setting.CommuteTimeSetting;
import com.portfolio.platform.data.model.setting.CommuteTimeWatchAppSetting;
import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import com.portfolio.platform.data.model.setting.WeatherWatchAppSetting;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vi4 {
    @DexIgnore
    public static final boolean a(String str, Gson gson, String str2) {
        CommuteTimeSetting commuteTimeSetting;
        CommuteTimeSetting commuteTimeSetting2;
        wg6.b(gson, "gson");
        try {
            commuteTimeSetting = (CommuteTimeSetting) gson.a(str2, CommuteTimeSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse commute time");
            commuteTimeSetting = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
        if (commuteTimeSetting == null) {
            commuteTimeSetting = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
        try {
            commuteTimeSetting2 = (CommuteTimeSetting) gson.a(str, CommuteTimeSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other commute time");
            commuteTimeSetting2 = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
        if (commuteTimeSetting2 == null) {
            commuteTimeSetting2 = new CommuteTimeSetting((String) null, (String) null, false, (String) null, 15, (qg6) null);
        }
        return wg6.a((Object) commuteTimeSetting.getAddress(), (Object) commuteTimeSetting2.getAddress()) && commuteTimeSetting.getAvoidTolls() == commuteTimeSetting2.getAvoidTolls() && wg6.a((Object) commuteTimeSetting.getFormat(), (Object) commuteTimeSetting2.getFormat());
    }

    @DexIgnore
    public static final boolean b(List<WeatherLocationWrapper> list, List<WeatherLocationWrapper> list2) {
        T t;
        wg6.b(list, "$this$isWeatherLocationWrapperListEquals");
        wg6.b(list2, "other");
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z = true;
        for (WeatherLocationWrapper next : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (wg6.a((Object) next.getId(), (Object) ((WeatherLocationWrapper) t).getId())) {
                    break;
                }
            }
            if (((WeatherLocationWrapper) t) == null) {
                z = false;
            }
        }
        return z;
    }

    @DexIgnore
    public static final boolean c(String str, Gson gson, String str2) {
        Ringtone ringtone;
        Ringtone ringtone2;
        wg6.b(gson, "gson");
        try {
            ringtone = (Ringtone) gson.a(str, Ringtone.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse ringPhoneSetting");
            ringtone = new Ringtone((String) null, (String) null, 3, (qg6) null);
        }
        if (ringtone == null) {
            ringtone = new Ringtone((String) null, (String) null, 3, (qg6) null);
        }
        try {
            ringtone2 = (Ringtone) gson.a(str2, Ringtone.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherRingPhoneSetting");
            ringtone2 = new Ringtone((String) null, (String) null, 3, (qg6) null);
        }
        if (ringtone2 == null) {
            ringtone2 = new Ringtone((String) null, (String) null, 3, (qg6) null);
        }
        return wg6.a((Object) ringtone2.getRingtoneName(), (Object) ringtone.getRingtoneName());
    }

    @DexIgnore
    public static final boolean d(String str, Gson gson, String str2) {
        SecondTimezoneSetting secondTimezoneSetting;
        SecondTimezoneSetting secondTimezoneSetting2;
        wg6.b(gson, "gson");
        try {
            secondTimezoneSetting = (SecondTimezoneSetting) gson.a(str2, SecondTimezoneSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse otherSecondTzSetting");
            secondTimezoneSetting = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
        }
        if (secondTimezoneSetting == null) {
            secondTimezoneSetting = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
        }
        try {
            secondTimezoneSetting2 = (SecondTimezoneSetting) gson.a(str, SecondTimezoneSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse secondTzSetting");
            secondTimezoneSetting2 = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
        }
        if (secondTimezoneSetting2 == null) {
            secondTimezoneSetting2 = new SecondTimezoneSetting((String) null, (String) null, 0, (String) null, 15, (qg6) null);
        }
        return wg6.a((Object) secondTimezoneSetting, (Object) secondTimezoneSetting2);
    }

    @DexIgnore
    public static final boolean e(String str, Gson gson, String str2) {
        WeatherWatchAppSetting weatherWatchAppSetting;
        WeatherWatchAppSetting weatherWatchAppSetting2;
        wg6.b(gson, "gson");
        try {
            weatherWatchAppSetting = (WeatherWatchAppSetting) gson.a(str2, WeatherWatchAppSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other weather watch app");
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        if (weatherWatchAppSetting == null) {
            weatherWatchAppSetting = new WeatherWatchAppSetting();
        }
        try {
            weatherWatchAppSetting2 = (WeatherWatchAppSetting) gson.a(str, WeatherWatchAppSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse weather watch app");
            weatherWatchAppSetting2 = new WeatherWatchAppSetting();
        }
        if (weatherWatchAppSetting2 == null) {
            weatherWatchAppSetting2 = new WeatherWatchAppSetting();
        }
        return b(weatherWatchAppSetting2.getLocations(), weatherWatchAppSetting.getLocations());
    }

    @DexIgnore
    public static final boolean b(String str, Gson gson, String str2) {
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting;
        CommuteTimeWatchAppSetting commuteTimeWatchAppSetting2;
        wg6.b(gson, "gson");
        try {
            commuteTimeWatchAppSetting = (CommuteTimeWatchAppSetting) gson.a(str2, CommuteTimeWatchAppSetting.class);
        } catch (Exception unused) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse commute time");
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
        if (commuteTimeWatchAppSetting == null) {
            commuteTimeWatchAppSetting = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
        try {
            commuteTimeWatchAppSetting2 = (CommuteTimeWatchAppSetting) gson.a(str, CommuteTimeWatchAppSetting.class);
        } catch (Exception unused2) {
            FLogger.INSTANCE.getLocal().d("CustomizeSettingExtension", "exception when parse other commute time");
            commuteTimeWatchAppSetting2 = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
        if (commuteTimeWatchAppSetting2 == null) {
            commuteTimeWatchAppSetting2 = new CommuteTimeWatchAppSetting((List) null, 1, (qg6) null);
        }
        return a(commuteTimeWatchAppSetting.getAddresses(), commuteTimeWatchAppSetting2.getAddresses());
    }

    @DexIgnore
    public static final boolean a(List<AddressWrapper> list, List<AddressWrapper> list2) {
        T t;
        boolean z;
        wg6.b(list, "$this$isDestinationListEquals");
        wg6.b(list2, "other");
        if (list2.size() != list.size()) {
            return false;
        }
        boolean z2 = true;
        for (AddressWrapper next : list) {
            Iterator<T> it = list2.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                AddressWrapper addressWrapper = (AddressWrapper) t;
                if (!wg6.a((Object) next.getAddress(), (Object) addressWrapper.getAddress()) || !wg6.a((Object) next.getName(), (Object) addressWrapper.getName()) || next.getAvoidTolls() != addressWrapper.getAvoidTolls()) {
                    z = false;
                    continue;
                } else {
                    z = true;
                    continue;
                }
                if (z) {
                    break;
                }
            }
            if (((AddressWrapper) t) == null) {
                z2 = false;
            }
        }
        return z2;
    }

    @DexIgnore
    public static final boolean a(String str) {
        return TextUtils.isEmpty(str) || wg6.a((Object) str, (Object) "{}");
    }
}
