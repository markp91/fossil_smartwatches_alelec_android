package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.PlaceManager;
import com.fossil.wearables.fsl.goaltracking.GoalTrackingSummary;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h72 extends e22 {
    @DexIgnore
    public static /* final */ h72 A; // = j("google.android.fitness.GoalV2");
    @DexIgnore
    public static /* final */ h72 B; // = j("symptom");
    @DexIgnore
    public static /* final */ h72 C; // = j("google.android.fitness.StrideModel");
    @DexIgnore
    public static /* final */ Parcelable.Creator<h72> CREATOR; // = new b82();
    @DexIgnore
    public static /* final */ h72 D; // = j("google.android.fitness.Device");
    @DexIgnore
    public static /* final */ h72 E; // = e("revolutions");
    @DexIgnore
    public static /* final */ h72 F; // = g("calories");
    @DexIgnore
    public static /* final */ h72 G; // = g("watts");
    @DexIgnore
    public static /* final */ h72 H; // = g("volume");
    @DexIgnore
    public static /* final */ h72 I; // = f("meal_type");
    @DexIgnore
    public static /* final */ h72 J; // = new h72("food_item", 3, true);
    @DexIgnore
    public static /* final */ h72 K; // = i("nutrients");
    @DexIgnore
    public static /* final */ h72 L; // = g("elevation.change");
    @DexIgnore
    public static /* final */ h72 M; // = i("elevation.gain");
    @DexIgnore
    public static /* final */ h72 N; // = i("elevation.loss");
    @DexIgnore
    public static /* final */ h72 O; // = g("floors");
    @DexIgnore
    public static /* final */ h72 P; // = i("floor.gain");
    @DexIgnore
    public static /* final */ h72 Q; // = i("floor.loss");
    @DexIgnore
    public static /* final */ h72 R; // = new h72("exercise", 3);
    @DexIgnore
    public static /* final */ h72 S; // = f("repetitions");
    @DexIgnore
    public static /* final */ h72 T; // = h("resistance");
    @DexIgnore
    public static /* final */ h72 U; // = f("resistance_type");
    @DexIgnore
    public static /* final */ h72 V; // = e("num_segments");
    @DexIgnore
    public static /* final */ h72 W; // = g(GoalTrackingSummary.COLUMN_AVERAGE);
    @DexIgnore
    public static /* final */ h72 X; // = g("max");
    @DexIgnore
    public static /* final */ h72 Y; // = g("min");
    @DexIgnore
    public static /* final */ h72 Z; // = g("low_latitude");
    @DexIgnore
    public static /* final */ h72 a0; // = g("low_longitude");
    @DexIgnore
    public static /* final */ h72 b0; // = g("high_latitude");
    @DexIgnore
    public static /* final */ h72 c0; // = g("high_longitude");
    @DexIgnore
    public static /* final */ h72 d; // = e("activity");
    @DexIgnore
    public static /* final */ h72 d0; // = e("occurrences");
    @DexIgnore
    public static /* final */ h72 e; // = g("confidence");
    @DexIgnore
    public static /* final */ h72 e0; // = e("sensor_type");
    @DexIgnore
    @Deprecated
    public static /* final */ h72 f; // = i("activity_confidence");
    @DexIgnore
    public static /* final */ h72 f0; // = new h72("timestamps", 5);
    @DexIgnore
    public static /* final */ h72 g; // = e("steps");
    @DexIgnore
    public static /* final */ h72 g0; // = new h72("sensor_values", 6);
    @DexIgnore
    public static /* final */ h72 h; // = e("duration");
    @DexIgnore
    public static /* final */ h72 h0; // = g("intensity");
    @DexIgnore
    public static /* final */ h72 i; // = f("duration");
    @DexIgnore
    public static /* final */ h72 i0; // = g("probability");
    @DexIgnore
    public static /* final */ h72 j; // = i("activity_duration.ascending");
    @DexIgnore
    public static /* final */ h72 o; // = i("activity_duration.descending");
    @DexIgnore
    public static /* final */ h72 p; // = g("bpm");
    @DexIgnore
    public static /* final */ h72 q; // = g("latitude");
    @DexIgnore
    public static /* final */ h72 r; // = g("longitude");
    @DexIgnore
    public static /* final */ h72 s; // = g(PlaceManager.PARAM_ACCURACY);
    @DexIgnore
    public static /* final */ h72 t; // = h(PlaceManager.PARAM_ALTITUDE);
    @DexIgnore
    public static /* final */ h72 u; // = g("distance");
    @DexIgnore
    public static /* final */ h72 v; // = g("height");
    @DexIgnore
    public static /* final */ h72 w; // = g("weight");
    @DexIgnore
    public static /* final */ h72 x; // = g("percentage");
    @DexIgnore
    public static /* final */ h72 y; // = g(PlaceManager.PARAM_SPEED);
    @DexIgnore
    public static /* final */ h72 z; // = g("rpm");
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ Boolean c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public static /* final */ h72 a; // = h72.g("x");
        @DexIgnore
        public static /* final */ h72 b; // = h72.g("y");
        @DexIgnore
        public static /* final */ h72 c; // = h72.g("z");
        @DexIgnore
        public static /* final */ h72 d; // = h72.k("google.android.fitness.SessionV2");
        @DexIgnore
        public static /* final */ h72 e; // = h72.j("google.android.fitness.DataPointSession");

        /*
        static {
            h72.k("debug_session");
        }
        */
    }

    /*
    static {
        g("step_length");
        g("circumference");
        e("sensor_types");
        e("sample_period");
        e("num_samples");
        e("num_dimensions");
    }
    */

    @DexIgnore
    public h72(String str, int i2) {
        this(str, i2, (Boolean) null);
    }

    @DexIgnore
    public static h72 e(String str) {
        return new h72(str, 1);
    }

    @DexIgnore
    public static h72 f(String str) {
        return new h72(str, 1, true);
    }

    @DexIgnore
    public static h72 g(String str) {
        return new h72(str, 2);
    }

    @DexIgnore
    public static h72 h(String str) {
        return new h72(str, 2, true);
    }

    @DexIgnore
    public static h72 i(String str) {
        return new h72(str, 4);
    }

    @DexIgnore
    public static h72 j(String str) {
        return new h72(str, 7);
    }

    @DexIgnore
    public static h72 k(String str) {
        return new h72(str, 7, true);
    }

    @DexIgnore
    public final String B() {
        return this.a;
    }

    @DexIgnore
    public final Boolean C() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof h72)) {
            return false;
        }
        h72 h72 = (h72) obj;
        return this.a.equals(h72.a) && this.b == h72.b;
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final int p() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        Object[] objArr = new Object[2];
        objArr[0] = this.a;
        objArr[1] = this.b == 1 ? "i" : "f";
        return String.format("%s(%s)", objArr);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i2) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, B(), false);
        g22.a(parcel, 2, p());
        g22.a(parcel, 3, C(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public h72(String str, int i2, Boolean bool) {
        w12.a(str);
        this.a = str;
        this.b = i2;
        this.c = bool;
    }
}
