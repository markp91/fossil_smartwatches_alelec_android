package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoFragment;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z65 implements MembersInjector<EditPhotoFragment> {
    @DexIgnore
    public static void a(EditPhotoFragment editPhotoFragment, w04 w04) {
        editPhotoFragment.q = w04;
    }
}
