package com.fossil;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gc {
    @DexIgnore
    public /* final */ ArrayList<Fragment> a; // = new ArrayList<>();
    @DexIgnore
    public /* final */ HashMap<String, fc> b; // = new HashMap<>();

    @DexIgnore
    public void a(List<String> list) {
        this.a.clear();
        if (list != null) {
            for (String next : list) {
                Fragment b2 = b(next);
                if (b2 != null) {
                    if (FragmentManager.d(2)) {
                        Log.v("FragmentManager", "restoreSaveState: added (" + next + "): " + b2);
                    }
                    a(b2);
                } else {
                    throw new IllegalStateException("No instantiated fragment for (" + next + ")");
                }
            }
        }
    }

    @DexIgnore
    public void b(fc fcVar) {
        Fragment e = fcVar.e();
        for (fc next : this.b.values()) {
            if (next != null) {
                Fragment e2 = next.e();
                if (e.mWho.equals(e2.mTargetWho)) {
                    e2.mTarget = e;
                    e2.mTargetWho = null;
                }
            }
        }
        this.b.put(e.mWho, (Object) null);
        String str = e.mTargetWho;
        if (str != null) {
            e.mTarget = b(str);
        }
    }

    @DexIgnore
    public void c(Fragment fragment) {
        synchronized (this.a) {
            this.a.remove(fragment);
        }
        fragment.mAdded = false;
    }

    @DexIgnore
    public void d() {
        this.b.clear();
    }

    @DexIgnore
    public ArrayList<ec> e() {
        ArrayList<ec> arrayList = new ArrayList<>(this.b.size());
        for (fc next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                ec j = next.j();
                arrayList.add(j);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "Saved state of " + e + ": " + j.q);
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public ArrayList<String> f() {
        synchronized (this.a) {
            if (this.a.isEmpty()) {
                return null;
            }
            ArrayList<String> arrayList = new ArrayList<>(this.a.size());
            Iterator<Fragment> it = this.a.iterator();
            while (it.hasNext()) {
                Fragment next = it.next();
                arrayList.add(next.mWho);
                if (FragmentManager.d(2)) {
                    Log.v("FragmentManager", "saveAllState: adding fragment (" + next.mWho + "): " + next);
                }
            }
            return arrayList;
        }
    }

    @DexIgnore
    public Fragment d(String str) {
        Fragment findFragmentByWho;
        for (fc next : this.b.values()) {
            if (next != null && (findFragmentByWho = next.e().findFragmentByWho(str)) != null) {
                return findFragmentByWho;
            }
        }
        return null;
    }

    @DexIgnore
    public List<Fragment> c() {
        ArrayList arrayList;
        if (this.a.isEmpty()) {
            return Collections.emptyList();
        }
        synchronized (this.a) {
            arrayList = new ArrayList(this.a);
        }
        return arrayList;
    }

    @DexIgnore
    public void a(fc fcVar) {
        this.b.put(fcVar.e().mWho, fcVar);
    }

    @DexIgnore
    public fc e(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public void a(Fragment fragment) {
        if (!this.a.contains(fragment)) {
            synchronized (this.a) {
                this.a.add(fragment);
            }
            fragment.mAdded = true;
            return;
        }
        throw new IllegalStateException("Fragment already added: " + fragment);
    }

    @DexIgnore
    public List<Fragment> b() {
        ArrayList arrayList = new ArrayList();
        for (fc next : this.b.values()) {
            if (next != null) {
                arrayList.add(next.e());
            } else {
                arrayList.add((Object) null);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public Fragment c(String str) {
        if (str != null) {
            for (int size = this.a.size() - 1; size >= 0; size--) {
                Fragment fragment = this.a.get(size);
                if (fragment != null && str.equals(fragment.mTag)) {
                    return fragment;
                }
            }
        }
        if (str == null) {
            return null;
        }
        for (fc next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                if (str.equals(e.mTag)) {
                    return e;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public Fragment b(int i) {
        for (int size = this.a.size() - 1; size >= 0; size--) {
            Fragment fragment = this.a.get(size);
            if (fragment != null && fragment.mFragmentId == i) {
                return fragment;
            }
        }
        for (fc next : this.b.values()) {
            if (next != null) {
                Fragment e = next.e();
                if (e.mFragmentId == i) {
                    return e;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public void a(int i) {
        Iterator<Fragment> it = this.a.iterator();
        while (it.hasNext()) {
            fc fcVar = this.b.get(it.next().mWho);
            if (fcVar != null) {
                fcVar.a(i);
            }
        }
        for (fc next : this.b.values()) {
            if (next != null) {
                next.a(i);
            }
        }
    }

    @DexIgnore
    public Fragment b(String str) {
        fc fcVar = this.b.get(str);
        if (fcVar != null) {
            return fcVar.e();
        }
        return null;
    }

    @DexIgnore
    public void a() {
        this.b.values().removeAll(Collections.singleton((Object) null));
    }

    @DexIgnore
    public Fragment b(Fragment fragment) {
        ViewGroup viewGroup = fragment.mContainer;
        View view = fragment.mView;
        if (!(viewGroup == null || view == null)) {
            for (int indexOf = this.a.indexOf(fragment) - 1; indexOf >= 0; indexOf--) {
                Fragment fragment2 = this.a.get(indexOf);
                if (fragment2.mContainer == viewGroup && fragment2.mView != null) {
                    return fragment2;
                }
            }
        }
        return null;
    }

    @DexIgnore
    public boolean a(String str) {
        return this.b.containsKey(str);
    }

    @DexIgnore
    public void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String str2 = str + "    ";
        if (!this.b.isEmpty()) {
            printWriter.print(str);
            printWriter.print("Active Fragments:");
            for (fc next : this.b.values()) {
                printWriter.print(str);
                if (next != null) {
                    Fragment e = next.e();
                    printWriter.println(e);
                    e.dump(str2, fileDescriptor, printWriter, strArr);
                } else {
                    printWriter.println("null");
                }
            }
        }
        int size = this.a.size();
        if (size > 0) {
            printWriter.print(str);
            printWriter.println("Added Fragments:");
            for (int i = 0; i < size; i++) {
                printWriter.print(str);
                printWriter.print("  #");
                printWriter.print(i);
                printWriter.print(": ");
                printWriter.println(this.a.get(i).toString());
            }
        }
    }
}
