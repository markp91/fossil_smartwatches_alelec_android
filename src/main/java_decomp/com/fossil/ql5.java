package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import com.portfolio.platform.uirenew.home.profile.help.deleteaccount.DeleteAccountPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ql5 implements Factory<pl5> {
    @DexIgnore
    public static DeleteAccountPresenter a(ml5 ml5, DeviceRepository deviceRepository, AnalyticsHelper analyticsHelper, ws4 ws4, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        return new DeleteAccountPresenter(ml5, deviceRepository, analyticsHelper, ws4, deleteLogoutUserUseCase);
    }
}
