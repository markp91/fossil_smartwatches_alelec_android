package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum d96 {
    DEVELOPER(1),
    USER_SIDELOAD(2),
    TEST_DISTRIBUTION(3),
    APP_STORE(4);
    
    @DexIgnore
    public static /* final */ String BETA_APP_PACKAGE_NAME; // = "io.crash.air";
    @DexIgnore
    public /* final */ int id;

    @DexIgnore
    public d96(int i) {
        this.id = i;
    }

    @DexIgnore
    public static d96 determineFrom(String str) {
        if (BETA_APP_PACKAGE_NAME.equals(str)) {
            return TEST_DISTRIBUTION;
        }
        if (str != null) {
            return APP_STORE;
        }
        return DEVELOPER;
    }

    @DexIgnore
    public int getId() {
        return this.id;
    }

    @DexIgnore
    public String toString() {
        return Integer.toString(this.id);
    }
}
