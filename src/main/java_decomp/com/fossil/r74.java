package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.ui.view.chart.overview.OverviewDayChart;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class r74 extends ViewDataBinding {
    @DexIgnore
    public /* final */ OverviewDayChart q;
    @DexIgnore
    public /* final */ FlexibleButton r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ View t;

    @DexIgnore
    public r74(Object obj, View view, int i, ConstraintLayout constraintLayout, OverviewDayChart overviewDayChart, FlexibleButton flexibleButton, ImageView imageView, FlexibleTextView flexibleTextView, View view2) {
        super(obj, view, i);
        this.q = overviewDayChart;
        this.r = flexibleButton;
        this.s = imageView;
        this.t = view2;
    }
}
