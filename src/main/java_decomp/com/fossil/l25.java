package com.fossil;

import com.fossil.o25;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l25 extends g25 {
    @DexIgnore
    public static /* final */ String k;
    @DexIgnore
    public static /* final */ a l; // = new a((qg6) null);
    @DexIgnore
    public /* final */ List<vx4> e; // = new ArrayList();
    @DexIgnore
    public /* final */ h25 f;
    @DexIgnore
    public /* final */ int g;
    @DexIgnore
    public /* final */ ArrayList<String> h;
    @DexIgnore
    public /* final */ z24 i;
    @DexIgnore
    public /* final */ o25 j;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return l25.k;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends xg6 implements hg6<vx4, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ l25 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(l25 l25) {
            super(1);
            this.this$0 = l25;
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Boolean.valueOf(invoke((AppWrapper) obj));
        }

        @DexIgnore
        public final boolean invoke(AppWrapper appWrapper) {
            wg6.b(appWrapper, "it");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
            if (isSelected != null) {
                return isSelected.booleanValue() && appWrapper.getCurrentHandGroup() == this.this$0.g;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<vx4, String> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final String invoke(AppWrapper appWrapper) {
            wg6.b(appWrapper, "it");
            return String.valueOf(appWrapper.getUri());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements y24.d<o25.b, y24.a> {
        @DexIgnore
        public /* final */ /* synthetic */ l25 a;

        @DexIgnore
        public d(l25 l25) {
            this.a = l25;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(o25.b bVar) {
            InstalledApp installedApp;
            wg6.b(bVar, "successResponse");
            FLogger.INSTANCE.getLocal().d(l25.l.a(), "mGetApps onSuccess");
            ArrayList<AppWrapper> arrayList = new ArrayList<>(bVar.a());
            for (AppWrapper appWrapper : arrayList) {
                if (appWrapper.getUri() != null) {
                    if (this.a.h.contains(String.valueOf(appWrapper.getUri()))) {
                        InstalledApp installedApp2 = appWrapper.getInstalledApp();
                        if (installedApp2 != null) {
                            installedApp2.setSelected(true);
                        }
                        appWrapper.setCurrentHandGroup(this.a.g);
                    } else if (appWrapper.getCurrentHandGroup() == this.a.g && (installedApp = appWrapper.getInstalledApp()) != null) {
                        installedApp.setSelected(false);
                    }
                }
            }
            this.a.j().addAll(arrayList);
            this.a.f.a(this.a.j(), this.a.g);
            this.a.f.d();
        }

        @DexIgnore
        public void a(y24.a aVar) {
            wg6.b(aVar, "errorResponse");
            FLogger.INSTANCE.getLocal().d(l25.l.a(), "mGetApps onError");
            this.a.f.d();
        }
    }

    /*
    static {
        String simpleName = l25.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationHybridAppPre\u2026er::class.java.simpleName");
        k = simpleName;
    }
    */

    @DexIgnore
    public l25(h25 h25, int i2, ArrayList<String> arrayList, z24 z24, o25 o25) {
        wg6.b(h25, "mView");
        wg6.b(arrayList, "mListAppsSelected");
        wg6.b(z24, "mUseCaseHandler");
        wg6.b(o25, "mGetHybridApp");
        this.f = h25;
        this.g = i2;
        this.h = arrayList;
        this.i = z24;
        this.j = o25;
    }

    @DexIgnore
    public void f() {
        FLogger.INSTANCE.getLocal().d(k, "start");
        if (this.e.isEmpty()) {
            this.f.e();
            this.i.a(this.j, null, new d(this));
        }
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(k, "stop");
    }

    @DexIgnore
    public int h() {
        return this.g;
    }

    @DexIgnore
    public void i() {
        this.f.a(new ArrayList(aj6.f(aj6.c(aj6.a(yd6.b(this.e), new b(this)), c.INSTANCE))));
    }

    @DexIgnore
    public final List<vx4> j() {
        return this.e;
    }

    @DexIgnore
    public void k() {
        this.f.a(this);
    }

    @DexIgnore
    public void a(AppWrapper appWrapper, boolean z) {
        T t;
        int i2;
        wg6.b(appWrapper, "appWrapper");
        FLogger.INSTANCE.getLocal().d(k, "setAppState: appWrapper=" + appWrapper + ", selected=" + z);
        Iterator<T> it = this.e.iterator();
        while (true) {
            if (!it.hasNext()) {
                t = null;
                break;
            }
            t = it.next();
            if (wg6.a((Object) ((AppWrapper) t).getUri(), (Object) appWrapper.getUri())) {
                break;
            }
        }
        AppWrapper appWrapper2 = (AppWrapper) t;
        if (appWrapper2 != null) {
            InstalledApp installedApp = appWrapper2.getInstalledApp();
            if (installedApp != null) {
                installedApp.setSelected(z);
            }
            InstalledApp installedApp2 = appWrapper2.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected == null) {
                wg6.a();
                throw null;
            } else if (isSelected.booleanValue() && appWrapper2.getCurrentHandGroup() != (i2 = this.g)) {
                appWrapper2.setCurrentHandGroup(i2);
            }
        }
    }
}
