package com.fossil;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t43 extends s63 {
    @DexIgnore
    public char c; // = 0;
    @DexIgnore
    public long d; // = -1;
    @DexIgnore
    public String e;
    @DexIgnore
    public /* final */ v43 f; // = new v43(this, 6, false, false);
    @DexIgnore
    public /* final */ v43 g; // = new v43(this, 6, true, false);
    @DexIgnore
    public /* final */ v43 h; // = new v43(this, 6, false, true);
    @DexIgnore
    public /* final */ v43 i; // = new v43(this, 5, false, false);
    @DexIgnore
    public /* final */ v43 j; // = new v43(this, 5, true, false);
    @DexIgnore
    public /* final */ v43 k; // = new v43(this, 5, false, true);
    @DexIgnore
    public /* final */ v43 l; // = new v43(this, 4, false, false);
    @DexIgnore
    public /* final */ v43 m; // = new v43(this, 3, false, false);
    @DexIgnore
    public /* final */ v43 n; // = new v43(this, 2, false, false);

    @DexIgnore
    public t43(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public static Object a(String str) {
        if (str == null) {
            return null;
        }
        return new y43(str);
    }

    @DexIgnore
    public static String b(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf == -1) {
            return str;
        }
        return str.substring(0, lastIndexOf);
    }

    @DexIgnore
    public final v43 A() {
        return this.m;
    }

    @DexIgnore
    public final v43 B() {
        return this.n;
    }

    @DexIgnore
    public final String C() {
        Pair<String, Long> a = k().d.a();
        if (a == null || a == h53.C) {
            return null;
        }
        String valueOf = String.valueOf(a.second);
        String str = (String) a.first;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(str).length());
        sb.append(valueOf);
        sb.append(":");
        sb.append(str);
        return sb.toString();
    }

    @DexIgnore
    public final boolean q() {
        return false;
    }

    @DexIgnore
    public final String s() {
        String str;
        synchronized (this) {
            if (this.e == null) {
                if (this.a.C() != null) {
                    this.e = this.a.C();
                } else {
                    this.e = cb3.v();
                }
            }
            str = this.e;
        }
        return str;
    }

    @DexIgnore
    public final v43 t() {
        return this.f;
    }

    @DexIgnore
    public final v43 u() {
        return this.g;
    }

    @DexIgnore
    public final v43 v() {
        return this.h;
    }

    @DexIgnore
    public final v43 w() {
        return this.i;
    }

    @DexIgnore
    public final v43 x() {
        return this.j;
    }

    @DexIgnore
    public final v43 y() {
        return this.k;
    }

    @DexIgnore
    public final v43 z() {
        return this.l;
    }

    @DexIgnore
    public final void a(int i2, boolean z, boolean z2, String str, Object obj, Object obj2, Object obj3) {
        if (!z && a(i2)) {
            a(i2, a(false, str, obj, obj2, obj3));
        }
        if (!z2 && i2 >= 5) {
            w12.a(str);
            u53 u = this.a.u();
            if (u == null) {
                a(6, "Scheduler not set. Not logging error/warn");
            } else if (!u.r()) {
                a(6, "Scheduler not initialized. Not logging error/warn");
            } else {
                if (i2 < 0) {
                    i2 = 0;
                }
                u.a((Runnable) new w43(this, i2 >= 9 ? 8 : i2, str, obj, obj2, obj3));
            }
        }
    }

    @DexIgnore
    public final boolean a(int i2) {
        return Log.isLoggable(s(), i2);
    }

    @DexIgnore
    public final void a(int i2, String str) {
        Log.println(i2, s(), str);
    }

    @DexIgnore
    public static String a(boolean z, String str, Object obj, Object obj2, Object obj3) {
        String str2 = "";
        if (str == null) {
            str = str2;
        }
        String a = a(z, obj);
        String a2 = a(z, obj2);
        String a3 = a(z, obj3);
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
            str2 = ": ";
        }
        if (!TextUtils.isEmpty(a)) {
            sb.append(str2);
            sb.append(a);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a2)) {
            sb.append(str2);
            sb.append(a2);
            str2 = ", ";
        }
        if (!TextUtils.isEmpty(a3)) {
            sb.append(str2);
            sb.append(a3);
        }
        return sb.toString();
    }

    @DexIgnore
    public static String a(boolean z, Object obj) {
        String className;
        String str = "";
        if (obj == null) {
            return str;
        }
        if (obj instanceof Integer) {
            obj = Long.valueOf((long) ((Integer) obj).intValue());
        }
        int i2 = 0;
        if (obj instanceof Long) {
            if (!z) {
                return String.valueOf(obj);
            }
            Long l2 = (Long) obj;
            if (Math.abs(l2.longValue()) < 100) {
                return String.valueOf(obj);
            }
            if (String.valueOf(obj).charAt(0) == '-') {
                str = "-";
            }
            String valueOf = String.valueOf(Math.abs(l2.longValue()));
            long round = Math.round(Math.pow(10.0d, (double) (valueOf.length() - 1)));
            long round2 = Math.round(Math.pow(10.0d, (double) valueOf.length()) - 1.0d);
            StringBuilder sb = new StringBuilder(str.length() + 43 + str.length());
            sb.append(str);
            sb.append(round);
            sb.append("...");
            sb.append(str);
            sb.append(round2);
            return sb.toString();
        } else if (obj instanceof Boolean) {
            return String.valueOf(obj);
        } else {
            if (obj instanceof Throwable) {
                Throwable th = (Throwable) obj;
                StringBuilder sb2 = new StringBuilder(z ? th.getClass().getName() : th.toString());
                String b = b(x53.class.getCanonicalName());
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                while (true) {
                    if (i2 >= length) {
                        break;
                    }
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    if (!stackTraceElement.isNativeMethod() && (className = stackTraceElement.getClassName()) != null && b(className).equals(b)) {
                        sb2.append(": ");
                        sb2.append(stackTraceElement);
                        break;
                    }
                    i2++;
                }
                return sb2.toString();
            } else if (obj instanceof y43) {
                return ((y43) obj).a;
            } else {
                if (z) {
                    return "-";
                }
                return String.valueOf(obj);
            }
        }
    }
}
