package com.fossil;

import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationquickresponse.QuickResponseViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zz4 implements Factory<yz4> {
    @DexIgnore
    public /* final */ Provider<QuickResponseRepository> a;
    @DexIgnore
    public /* final */ Provider<ur4> b;

    @DexIgnore
    public zz4(Provider<QuickResponseRepository> provider, Provider<ur4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static zz4 a(Provider<QuickResponseRepository> provider, Provider<ur4> provider2) {
        return new zz4(provider, provider2);
    }

    @DexIgnore
    public static yz4 b(Provider<QuickResponseRepository> provider, Provider<ur4> provider2) {
        return new QuickResponseViewModel(provider.get(), provider2.get());
    }

    @DexIgnore
    public QuickResponseViewModel get() {
        return b(this.a, this.b);
    }
}
