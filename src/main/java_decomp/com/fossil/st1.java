package com.fossil;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class st1 implements ew1 {
    @DexIgnore
    public Status a;
    @DexIgnore
    public GoogleSignInAccount b;

    @DexIgnore
    public st1(GoogleSignInAccount googleSignInAccount, Status status) {
        this.b = googleSignInAccount;
        this.a = status;
    }

    @DexIgnore
    public GoogleSignInAccount a() {
        return this.b;
    }

    @DexIgnore
    public boolean b() {
        return this.a.F();
    }

    @DexIgnore
    public Status o() {
        return this.a;
    }
}
