package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ux5 {
    @DexIgnore
    public static /* synthetic */ List a(List list, boolean z, int i, Object obj) {
        if ((i & 1) != 0) {
            z = false;
        }
        return a(list, z);
    }

    @DexIgnore
    public static final AppNotificationFilter a() {
        AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getAppName(), DianaNotificationObj.AApplicationName.Companion.getFOSSIL().getPackageName(), "", NotificationBaseObj.ANotificationType.NOTIFICATION));
        appNotificationFilter.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
        return appNotificationFilter;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0128  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x006b A[SYNTHETIC] */
    public static final List<AppNotificationFilter> a(List<vx4> list, boolean z) {
        AppNotificationFilter appNotificationFilter;
        wg6.b(list, "$this$toAppNotificationFilter");
        ArrayList arrayList = new ArrayList();
        Iterator<T> it = list.iterator();
        while (true) {
            String str = null;
            if (!it.hasNext()) {
                break;
            }
            InstalledApp installedApp = ((AppWrapper) it.next()).getInstalledApp();
            if (installedApp != null) {
                str = installedApp.getIdentifier() + installedApp.isSelected();
            }
            if (str != null) {
                arrayList.add(str);
            }
        }
        FLogger.INSTANCE.getLocal().d("NotificationExtension", "toAppNotificationFilter(), inputAppWrapper = " + arrayList + ", isAllAppEnabled=" + z);
        ArrayList arrayList2 = new ArrayList();
        Iterator<T> it2 = list.iterator();
        while (it2.hasNext()) {
            InstalledApp installedApp2 = ((AppWrapper) it2.next()).getInstalledApp();
            if (installedApp2 != null) {
                String identifier = installedApp2.getIdentifier();
                Boolean isSelected = installedApp2.isSelected();
                wg6.a((Object) isSelected, "installedApp.isSelected");
                if (isSelected.booleanValue() || z) {
                    DianaNotificationObj.AApplicationName.Companion companion = DianaNotificationObj.AApplicationName.Companion;
                    wg6.a((Object) identifier, "packageNameApp");
                    if (companion.isPackageNameSupportedBySDK(identifier)) {
                        DianaNotificationObj.AApplicationName supportedBySDKApp = DianaNotificationObj.AApplicationName.Companion.getSupportedBySDKApp(identifier);
                        appNotificationFilter = new AppNotificationFilter(new FNotification(supportedBySDKApp.getAppName(), supportedBySDKApp.getPackageName(), supportedBySDKApp.getIconFwPath(), supportedBySDKApp.getNotificationType()));
                    } else {
                        FLogger.INSTANCE.getLocal().d("toAppNotificationFilter", "app name " + installedApp2.getTitle() + " identifier " + installedApp2.getIdentifier());
                        String title = installedApp2.getTitle();
                        wg6.a((Object) title, "installedApp.title");
                        String identifier2 = installedApp2.getIdentifier();
                        wg6.a((Object) identifier2, "installedApp.identifier");
                        DianaNotificationObj.AApplicationName aApplicationName = new DianaNotificationObj.AApplicationName(title, identifier2, "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION);
                        String appName = aApplicationName.getAppName();
                        String identifier3 = installedApp2.getIdentifier();
                        wg6.a((Object) identifier3, "installedApp.identifier");
                        appNotificationFilter = new AppNotificationFilter(new FNotification(appName, identifier3, aApplicationName.getIconFwPath(), aApplicationName.getNotificationType()));
                    }
                    if (appNotificationFilter == null) {
                        arrayList2.add(appNotificationFilter);
                    }
                }
            }
            appNotificationFilter = null;
            if (appNotificationFilter == null) {
            }
        }
        ArrayList arrayList3 = new ArrayList(arrayList2);
        arrayList3.add(a());
        FLogger.INSTANCE.getLocal().d("NotificationExtension", "toAppNotificationFilter(), output = " + arrayList2);
        return arrayList3;
    }
}
