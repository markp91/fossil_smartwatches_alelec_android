package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RingProgressBar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class h84 extends ViewDataBinding {
    @DexIgnore
    public /* final */ View A;
    @DexIgnore
    public /* final */ View B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ ImageView r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ ImageView t;
    @DexIgnore
    public /* final */ ImageView u;
    @DexIgnore
    public /* final */ RingProgressBar v;
    @DexIgnore
    public /* final */ RingProgressBar w;
    @DexIgnore
    public /* final */ RingProgressBar x;
    @DexIgnore
    public /* final */ RingProgressBar y;
    @DexIgnore
    public /* final */ View z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h84(Object obj, View view, int i, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, ConstraintLayout constraintLayout3, ConstraintLayout constraintLayout4, ConstraintLayout constraintLayout5, FlexibleButton flexibleButton, ImageView imageView, ImageView imageView2, ImageView imageView3, ImageView imageView4, RingProgressBar ringProgressBar, RingProgressBar ringProgressBar2, RingProgressBar ringProgressBar3, RingProgressBar ringProgressBar4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, View view2, View view3, View view4, View view5) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = imageView;
        this.s = imageView2;
        this.t = imageView3;
        this.u = imageView4;
        this.v = ringProgressBar;
        this.w = ringProgressBar2;
        this.x = ringProgressBar3;
        this.y = ringProgressBar4;
        this.z = view2;
        this.A = view3;
        this.B = view4;
        this.C = view5;
    }
}
