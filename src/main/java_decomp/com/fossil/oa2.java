package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oa2 extends i12<pa2> {
    @DexIgnore
    public /* final */ Bundle E;

    @DexIgnore
    public oa2(Context context, Looper looper, e12 e12, lt1 lt1, wv1.b bVar, wv1.c cVar) {
        super(context, looper, 16, e12, bVar, cVar);
        if (lt1 == null) {
            this.E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    @DexIgnore
    public final String A() {
        return "com.google.android.gms.auth.service.START";
    }

    @DexIgnore
    public final /* synthetic */ IInterface a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof pa2) {
            return (pa2) queryLocalInterface;
        }
        return new qa2(iBinder);
    }

    @DexIgnore
    public final int j() {
        return nv1.GOOGLE_PLAY_SERVICES_VERSION_CODE;
    }

    @DexIgnore
    public final boolean m() {
        e12 G = G();
        return !TextUtils.isEmpty(G.b()) && !G.a((rv1<?>) kt1.c).isEmpty();
    }

    @DexIgnore
    public final Bundle v() {
        return this.E;
    }

    @DexIgnore
    public final String z() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }
}
