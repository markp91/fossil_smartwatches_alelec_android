package com.fossil;

import com.fossil.rm6;
import java.util.concurrent.CancellationException;
import java.util.concurrent.atomic.AtomicIntegerFieldUpdater;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mk6<T> extends yl6<T> implements lk6<T>, kf6 {
    @DexIgnore
    public static /* final */ AtomicIntegerFieldUpdater f; // = AtomicIntegerFieldUpdater.newUpdater(mk6.class, "_decision");
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater g; // = AtomicReferenceFieldUpdater.newUpdater(mk6.class, Object.class, "_state");
    @DexIgnore
    public volatile int _decision; // = 0;
    @DexIgnore
    public volatile Object _state; // = dk6.a;
    @DexIgnore
    public /* final */ af6 d; // = this.e.getContext();
    @DexIgnore
    public /* final */ xe6<T> e;
    @DexIgnore
    public volatile am6 parentHandle;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public mk6(xe6<? super T> xe6, int i) {
        super(i);
        wg6.b(xe6, "delegate");
        this.e = xe6;
    }

    @DexIgnore
    public void a(Object obj, Throwable th) {
        wg6.b(th, "cause");
        if (obj instanceof yk6) {
            try {
                ((yk6) obj).b.invoke(th);
            } catch (Throwable th2) {
                af6 context = getContext();
                fl6.a(context, (Throwable) new al6("Exception in cancellation handler for " + this, th2));
            }
        }
    }

    @DexIgnore
    public final xe6<T> b() {
        return this.e;
    }

    @DexIgnore
    public Object c() {
        return f();
    }

    @DexIgnore
    public final void d(Object obj) {
        throw new IllegalStateException(("Already resumed, but proposed with update " + obj).toString());
    }

    @DexIgnore
    public final Object e() {
        rm6 rm6;
        g();
        if (k()) {
            return ff6.a();
        }
        Object f2 = f();
        if (f2 instanceof vk6) {
            throw to6.a(((vk6) f2).a, (xe6<?>) this);
        } else if (this.c != 1 || (rm6 = (rm6) getContext().get(rm6.n)) == null || rm6.isActive()) {
            return c(f2);
        } else {
            CancellationException k = rm6.k();
            a(f2, (Throwable) k);
            throw to6.a(k, (xe6<?>) this);
        }
    }

    @DexIgnore
    public final Object f() {
        return this._state;
    }

    @DexIgnore
    public final void g() {
        rm6 rm6;
        if (!h() && (rm6 = (rm6) this.e.getContext().get(rm6.n)) != null) {
            rm6.start();
            am6 a = rm6.a.a(rm6, true, false, new pk6(rm6, this), 2, (Object) null);
            this.parentHandle = a;
            if (h()) {
                a.dispose();
                this.parentHandle = en6.a;
            }
        }
    }

    @DexIgnore
    public kf6 getCallerFrame() {
        xe6<T> xe6 = this.e;
        if (!(xe6 instanceof kf6)) {
            xe6 = null;
        }
        return (kf6) xe6;
    }

    @DexIgnore
    public af6 getContext() {
        return this.d;
    }

    @DexIgnore
    public StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public boolean h() {
        return !(f() instanceof fn6);
    }

    @DexIgnore
    public String i() {
        return "CancellableContinuation";
    }

    @DexIgnore
    public boolean isActive() {
        return f() instanceof fn6;
    }

    @DexIgnore
    public final boolean j() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 1) {
                    return false;
                }
                throw new IllegalStateException("Already resumed".toString());
            }
        } while (!f.compareAndSet(this, 0, 2));
        return true;
    }

    @DexIgnore
    public final boolean k() {
        do {
            int i = this._decision;
            if (i != 0) {
                if (i == 2) {
                    return false;
                }
                throw new IllegalStateException("Already suspended".toString());
            }
        } while (!f.compareAndSet(this, 0, 1));
        return true;
    }

    @DexIgnore
    public void resumeWith(Object obj) {
        a(wk6.a(obj), this.c);
    }

    @DexIgnore
    public String toString() {
        return i() + '(' + ol6.a((xe6<?>) this.e) + "){" + f() + "}@" + ol6.b(this);
    }

    @DexIgnore
    public void b(Object obj) {
        wg6.b(obj, "token");
        a(this.c);
    }

    @DexIgnore
    public <T> T c(Object obj) {
        if (obj instanceof xk6) {
            return ((xk6) obj).b;
        }
        return obj instanceof yk6 ? ((yk6) obj).a : obj;
    }

    @DexIgnore
    public final void d() {
        am6 am6 = this.parentHandle;
        if (am6 != null) {
            am6.dispose();
            this.parentHandle = en6.a;
        }
    }

    @DexIgnore
    public void b(hg6<? super Throwable, cd6> hg6) {
        Object obj;
        wg6.b(hg6, "handler");
        Throwable th = null;
        jk6 jk6 = null;
        do {
            obj = this._state;
            if (obj instanceof dk6) {
                if (jk6 == null) {
                    jk6 = a(hg6);
                }
            } else if (obj instanceof jk6) {
                a(hg6, obj);
                throw null;
            } else if (!(obj instanceof ok6)) {
                return;
            } else {
                if (((ok6) obj).b()) {
                    try {
                        if (!(obj instanceof vk6)) {
                            obj = null;
                        }
                        vk6 vk6 = (vk6) obj;
                        if (vk6 != null) {
                            th = vk6.a;
                        }
                        hg6.invoke(th);
                        return;
                    } catch (Throwable th2) {
                        fl6.a(getContext(), (Throwable) new al6("Exception in cancellation handler for " + this, th2));
                        return;
                    }
                } else {
                    a(hg6, obj);
                    throw null;
                }
            }
        } while (!g.compareAndSet(this, obj, jk6));
    }

    @DexIgnore
    public Throwable a(rm6 rm6) {
        wg6.b(rm6, "parent");
        return rm6.k();
    }

    @DexIgnore
    public final ok6 a(Throwable th, int i) {
        wg6.b(th, "exception");
        return a((Object) new vk6(th, false, 2, (qg6) null), i);
    }

    @DexIgnore
    public final void a(hg6<? super Throwable, cd6> hg6, Object obj) {
        throw new IllegalStateException(("It's prohibited to register multiple handlers, tried to register " + hg6 + ", already has " + obj).toString());
    }

    @DexIgnore
    public final jk6 a(hg6<? super Throwable, cd6> hg6) {
        return hg6 instanceof jk6 ? (jk6) hg6 : new om6(hg6);
    }

    @DexIgnore
    public final void a(int i) {
        if (!j()) {
            xl6.a(this, i);
        }
    }

    @DexIgnore
    public void a(dl6 dl6, T t) {
        wg6.b(dl6, "$this$resumeUndispatched");
        xe6<T> xe6 = this.e;
        dl6 dl62 = null;
        if (!(xe6 instanceof vl6)) {
            xe6 = null;
        }
        vl6 vl6 = (vl6) xe6;
        if (vl6 != null) {
            dl62 = vl6.g;
        }
        a((Object) t, dl62 == dl6 ? 3 : this.c);
    }

    @DexIgnore
    public boolean a(Throwable th) {
        Object obj;
        boolean z;
        do {
            obj = this._state;
            if (!(obj instanceof fn6)) {
                return false;
            }
            z = obj instanceof jk6;
        } while (!g.compareAndSet(this, obj, new ok6(this, th, z)));
        if (z) {
            try {
                ((jk6) obj).a(th);
            } catch (Throwable th2) {
                af6 context = getContext();
                fl6.a(context, (Throwable) new al6("Exception in cancellation handler for " + this, th2));
            }
        }
        d();
        a(0);
        return true;
    }

    @DexIgnore
    public final ok6 a(Object obj, int i) {
        Object obj2;
        do {
            obj2 = this._state;
            if (!(obj2 instanceof fn6)) {
                if (obj2 instanceof ok6) {
                    ok6 ok6 = (ok6) obj2;
                    if (ok6.c()) {
                        return ok6;
                    }
                }
                d(obj);
                throw null;
            }
        } while (!g.compareAndSet(this, obj2, obj));
        d();
        a(i);
        return null;
    }

    @DexIgnore
    public Object a(T t, Object obj) {
        Object obj2;
        T t2;
        do {
            obj2 = this._state;
            if (obj2 instanceof fn6) {
                if (obj == null) {
                    t2 = t;
                } else {
                    t2 = new xk6(obj, t, (fn6) obj2);
                }
            } else if (!(obj2 instanceof xk6)) {
                return null;
            } else {
                xk6 xk6 = (xk6) obj2;
                if (xk6.a != obj) {
                    return null;
                }
                if (nl6.a()) {
                    if (!(xk6.b == t)) {
                        throw new AssertionError();
                    }
                }
                return xk6.c;
            }
        } while (!g.compareAndSet(this, obj2, t2));
        d();
        return obj2;
    }
}
