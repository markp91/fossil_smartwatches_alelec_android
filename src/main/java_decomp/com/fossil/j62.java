package com.fossil;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j62 implements DynamiteModule.b.a {
    @DexIgnore
    public final int a(Context context, String str, boolean z) throws DynamiteModule.a {
        return DynamiteModule.a(context, str, z);
    }

    @DexIgnore
    public final int a(Context context, String str) {
        return DynamiteModule.a(context, str);
    }
}
