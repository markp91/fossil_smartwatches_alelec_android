package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.HomeDianaCustomizePresenter$start$1$1", f = "HomeDianaCustomizePresenter.kt", l = {}, m = "invokeSuspend")
public final class l45$h$a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDianaCustomizePresenter.h this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l45$h$a(HomeDianaCustomizePresenter.h hVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        l45$h$a l45_h_a = new l45$h$a(this.this$0, xe6);
        l45_h_a.p$ = (il6) obj;
        return l45_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((l45$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            this.this$0.this$0.g.clear();
            this.this$0.this$0.g.addAll(this.this$0.this$0.v.getAllComplicationRaw());
            this.this$0.this$0.h.clear();
            this.this$0.this$0.h.addAll(this.this$0.this$0.u.getAllWatchAppRaw());
            this.this$0.this$0.m.clear();
            return hf6.a(this.this$0.this$0.m.addAll(this.this$0.this$0.A.getAllRealDataRaw()));
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
