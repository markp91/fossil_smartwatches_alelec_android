package com.fossil;

import java.io.Closeable;
import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tc implements Closeable, il6 {
    @DexIgnore
    public /* final */ af6 a;

    @DexIgnore
    public tc(af6 af6) {
        wg6.b(af6, "context");
        this.a = af6;
    }

    @DexIgnore
    public void close() {
        vm6.a(m(), (CancellationException) null, 1, (Object) null);
    }

    @DexIgnore
    public af6 m() {
        return this.a;
    }
}
