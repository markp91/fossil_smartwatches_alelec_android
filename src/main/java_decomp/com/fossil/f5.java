package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.j5;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f5 extends n5 {
    @DexIgnore
    public int m0; // = 0;
    @DexIgnore
    public ArrayList<q5> n0; // = new ArrayList<>(4);
    @DexIgnore
    public boolean o0; // = true;

    @DexIgnore
    public void G() {
        super.G();
        this.n0.clear();
    }

    @DexIgnore
    public void H() {
        q5 q5Var;
        float f;
        q5 q5Var2;
        int i = this.m0;
        float f2 = Float.MAX_VALUE;
        if (i != 0) {
            if (i == 1) {
                q5Var = this.u.d();
            } else if (i == 2) {
                q5Var = this.t.d();
            } else if (i == 3) {
                q5Var = this.v.d();
            } else {
                return;
            }
            f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        } else {
            q5Var = this.s.d();
        }
        int size = this.n0.size();
        q5 q5Var3 = null;
        int i2 = 0;
        while (i2 < size) {
            q5 q5Var4 = this.n0.get(i2);
            if (q5Var4.b == 1) {
                int i3 = this.m0;
                if (i3 == 0 || i3 == 2) {
                    f = q5Var4.g;
                    if (f < f2) {
                        q5Var2 = q5Var4.f;
                    } else {
                        i2++;
                    }
                } else {
                    f = q5Var4.g;
                    if (f > f2) {
                        q5Var2 = q5Var4.f;
                    } else {
                        i2++;
                    }
                }
                q5Var3 = q5Var2;
                f2 = f;
                i2++;
            } else {
                return;
            }
        }
        if (z4.j() != null) {
            z4.j().y++;
        }
        q5Var.f = q5Var3;
        q5Var.g = f2;
        q5Var.a();
        int i4 = this.m0;
        if (i4 == 0) {
            this.u.d().a(q5Var3, f2);
        } else if (i4 == 1) {
            this.s.d().a(q5Var3, f2);
        } else if (i4 == 2) {
            this.v.d().a(q5Var3, f2);
        } else if (i4 == 3) {
            this.t.d().a(q5Var3, f2);
        }
    }

    @DexIgnore
    public boolean L() {
        return this.o0;
    }

    @DexIgnore
    public void a(int i) {
        q5 q5Var;
        q5 q5Var2;
        j5 j5Var = this.D;
        if (j5Var != null && ((k5) j5Var).u(2)) {
            int i2 = this.m0;
            if (i2 == 0) {
                q5Var = this.s.d();
            } else if (i2 == 1) {
                q5Var = this.u.d();
            } else if (i2 == 2) {
                q5Var = this.t.d();
            } else if (i2 == 3) {
                q5Var = this.v.d();
            } else {
                return;
            }
            q5Var.b(5);
            int i3 = this.m0;
            if (i3 == 0 || i3 == 1) {
                this.t.d().a((q5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.v.d().a((q5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            } else {
                this.s.d().a((q5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                this.u.d().a((q5) null, (float) LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            }
            this.n0.clear();
            for (int i4 = 0; i4 < this.l0; i4++) {
                j5 j5Var2 = this.k0[i4];
                if (this.o0 || j5Var2.b()) {
                    int i5 = this.m0;
                    if (i5 == 0) {
                        q5Var2 = j5Var2.s.d();
                    } else if (i5 == 1) {
                        q5Var2 = j5Var2.u.d();
                    } else if (i5 == 2) {
                        q5Var2 = j5Var2.t.d();
                    } else if (i5 != 3) {
                        q5Var2 = null;
                    } else {
                        q5Var2 = j5Var2.v.d();
                    }
                    if (q5Var2 != null) {
                        this.n0.add(q5Var2);
                        q5Var2.a(q5Var);
                    }
                }
            }
        }
    }

    @DexIgnore
    public boolean b() {
        return true;
    }

    @DexIgnore
    public void c(boolean z) {
        this.o0 = z;
    }

    @DexIgnore
    public void u(int i) {
        this.m0 = i;
    }

    @DexIgnore
    public void a(z4 z4Var) {
        i5[] i5VarArr;
        boolean z;
        int i;
        int i2;
        i5[] i5VarArr2 = this.A;
        i5VarArr2[0] = this.s;
        i5VarArr2[2] = this.t;
        i5VarArr2[1] = this.u;
        i5VarArr2[3] = this.v;
        int i3 = 0;
        while (true) {
            i5VarArr = this.A;
            if (i3 >= i5VarArr.length) {
                break;
            }
            i5VarArr[i3].i = z4Var.a((Object) i5VarArr[i3]);
            i3++;
        }
        int i4 = this.m0;
        if (i4 >= 0 && i4 < 4) {
            i5 i5Var = i5VarArr[i4];
            int i5 = 0;
            while (true) {
                if (i5 >= this.l0) {
                    z = false;
                    break;
                }
                j5 j5Var = this.k0[i5];
                if ((this.o0 || j5Var.b()) && ((((i = this.m0) == 0 || i == 1) && j5Var.k() == j5.b.MATCH_CONSTRAINT) || (((i2 = this.m0) == 2 || i2 == 3) && j5Var.r() == j5.b.MATCH_CONSTRAINT))) {
                    z = true;
                } else {
                    i5++;
                }
            }
            int i6 = this.m0;
            if (i6 == 0 || i6 == 1 ? l().k() == j5.b.WRAP_CONTENT : l().r() == j5.b.WRAP_CONTENT) {
                z = false;
            }
            for (int i7 = 0; i7 < this.l0; i7++) {
                j5 j5Var2 = this.k0[i7];
                if (this.o0 || j5Var2.b()) {
                    d5 a = z4Var.a((Object) j5Var2.A[this.m0]);
                    i5[] i5VarArr3 = j5Var2.A;
                    int i8 = this.m0;
                    i5VarArr3[i8].i = a;
                    if (i8 == 0 || i8 == 2) {
                        z4Var.b(i5Var.i, a, z);
                    } else {
                        z4Var.a(i5Var.i, a, z);
                    }
                }
            }
            int i9 = this.m0;
            if (i9 == 0) {
                z4Var.a(this.u.i, this.s.i, 0, 6);
                if (!z) {
                    z4Var.a(this.s.i, this.D.u.i, 0, 5);
                }
            } else if (i9 == 1) {
                z4Var.a(this.s.i, this.u.i, 0, 6);
                if (!z) {
                    z4Var.a(this.s.i, this.D.s.i, 0, 5);
                }
            } else if (i9 == 2) {
                z4Var.a(this.v.i, this.t.i, 0, 6);
                if (!z) {
                    z4Var.a(this.t.i, this.D.v.i, 0, 5);
                }
            } else if (i9 == 3) {
                z4Var.a(this.t.i, this.v.i, 0, 6);
                if (!z) {
                    z4Var.a(this.t.i, this.D.t.i, 0, 5);
                }
            }
        }
    }
}
