package com.fossil;

import android.util.Base64;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i8 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ List<List<byte[]>> d;
    @DexIgnore
    public /* final */ int e; // = 0;
    @DexIgnore
    public /* final */ String f; // = (this.a + "-" + this.b + "-" + this.c);

    @DexIgnore
    public i8(String str, String str2, String str3, List<List<byte[]>> list) {
        y8.a(str);
        this.a = str;
        y8.a(str2);
        this.b = str2;
        y8.a(str3);
        this.c = str3;
        y8.a(list);
        this.d = list;
    }

    @DexIgnore
    public List<List<byte[]>> a() {
        return this.d;
    }

    @DexIgnore
    public int b() {
        return this.e;
    }

    @DexIgnore
    public String c() {
        return this.f;
    }

    @DexIgnore
    public String d() {
        return this.a;
    }

    @DexIgnore
    public String e() {
        return this.b;
    }

    @DexIgnore
    public String f() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("FontRequest {mProviderAuthority: " + this.a + ", mProviderPackage: " + this.b + ", mQuery: " + this.c + ", mCertificates:");
        for (int i = 0; i < this.d.size(); i++) {
            sb.append(" [");
            List list = this.d.get(i);
            for (int i2 = 0; i2 < list.size(); i2++) {
                sb.append(" \"");
                sb.append(Base64.encodeToString((byte[]) list.get(i2), 0));
                sb.append("\"");
            }
            sb.append(" ]");
        }
        sb.append("}");
        sb.append("mCertificatesArray: " + this.e);
        return sb.toString();
    }
}
