package com.fossil;

import com.fossil.fitness.SleepState;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.ButtonService;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.ActivityIntensities;
import com.portfolio.platform.data.model.fitnessdata.FitnessDataWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepSessionWrapper;
import com.portfolio.platform.data.model.fitnessdata.SleepStateChangeWrapper;
import com.portfolio.platform.data.model.room.fitness.ActivitySample;
import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.data.model.room.sleep.SleepDistribution;
import com.portfolio.platform.data.model.sleep.SleepSessionHeartRate;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitSample;
import com.portfolio.platform.service.syncmodel.WrapperSleepStateChange;
import com.portfolio.platform.service.syncmodel.WrapperTapEventSummary;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class SyncDataExtensions {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends xg6 implements hg6<FitnessDataWrapper, Integer> {
        @DexIgnore
        public static /* final */ c INSTANCE; // = new c();

        @DexIgnore
        public c() {
            super(1);
        }

        @DexIgnore
        public final int invoke(FitnessDataWrapper fitnessDataWrapper) {
            wg6.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getTimezoneOffsetInSecond();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Integer.valueOf(invoke((FitnessDataWrapper) obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends xg6 implements hg6<FitnessDataWrapper, Long> {
        @DexIgnore
        public static /* final */ d INSTANCE; // = new d();

        @DexIgnore
        public d() {
            super(1);
        }

        @DexIgnore
        public final long invoke(FitnessDataWrapper fitnessDataWrapper) {
            wg6.b(fitnessDataWrapper, "it");
            return fitnessDataWrapper.getStartLongTime();
        }

        @DexIgnore
        public /* bridge */ /* synthetic */ Object invoke(Object obj) {
            return Long.valueOf(invoke((FitnessDataWrapper) obj));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements Comparator<T> {
        @DexIgnore
        public final int compare(T t, T t2) {
            return ue6.a(Long.valueOf(((FitnessDataWrapper) t).getStartLongTime()), Long.valueOf(((FitnessDataWrapper) t2).getStartLongTime()));
        }
    }

    @DexIgnore
    public static final List<lc6<Long, Long>> a(List<FitnessDataWrapper> list) {
        wg6.b(list, "$this$getActiveTimeList");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        yd6.a(list, new a());
        for (FitnessDataWrapper fitnessDataWrapper : list) {
            DateTime startTimeTZ = fitnessDataWrapper.getStartTimeTZ();
            int resolutionInSecond = fitnessDataWrapper.getActiveMinute().getResolutionInSecond();
            if (!fitnessDataWrapper.getActiveMinute().getValues().isEmpty()) {
                int i = 0;
                boolean z = false;
                for (T next : fitnessDataWrapper.getActiveMinute().getValues()) {
                    int i2 = i + 1;
                    if (i >= 0) {
                        boolean booleanValue = ((Boolean) next).booleanValue();
                        if (booleanValue && booleanValue != z) {
                            startTimeTZ = startTimeTZ.plus(((long) (i * resolutionInSecond)) * 1000);
                            wg6.a((Object) startTimeTZ, "startTime.plus(index * resolutionInSecond * 1000L)");
                        }
                        if (!booleanValue && booleanValue != z) {
                            arrayList.add(new lc6(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf(fitnessDataWrapper.getStartLongTime() + (((long) (i * resolutionInSecond)) * 1000))));
                        }
                        z = booleanValue;
                        i = i2;
                    } else {
                        qd6.c();
                        throw null;
                    }
                }
                if (((Boolean) yd6.f(fitnessDataWrapper.getActiveMinute().getValues())).booleanValue()) {
                    arrayList.add(new lc6(Long.valueOf(startTimeTZ.getMillis()), Long.valueOf(fitnessDataWrapper.getStartLongTime() + (((long) (fitnessDataWrapper.getActiveMinute().getValues().size() * resolutionInSecond)) * 1000))));
                }
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final List<WrapperTapEventSummary> b(List<FitnessDataWrapper> list) {
        wg6.b(list, "$this$getGoalTrackings");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        yd6.a(list, new b());
        for (FitnessDataWrapper goalTrackings : list) {
            for (WrapperTapEventSummary add : goalTrackings.getGoalTrackings()) {
                arrayList.add(add);
            }
        }
        return arrayList;
    }

    @DexIgnore
    public static final pc6<List<ActivitySample>, List<ActivitySummary>, List<GFitSample>> a(List<FitnessDataWrapper> list, String str, String str2, long j) {
        String str3;
        ArrayList arrayList;
        TimeZone timeZone;
        String str4;
        ArrayList arrayList2;
        int i;
        String str5;
        int i2;
        ActivitySample activitySample;
        ArrayList arrayList3;
        String str6;
        ArrayList arrayList4;
        String str7;
        String str8;
        String str9;
        TimeZone timeZone2;
        ArrayList arrayList5;
        String str10;
        ArrayList arrayList6;
        ArrayList arrayList7;
        ArrayList arrayList8;
        Object obj;
        TimeZone timeZone3;
        String str11;
        ArrayList arrayList9;
        TimeZone timeZone4;
        String str12;
        ArrayList arrayList10;
        int i3;
        String str13;
        int i4;
        ArrayList arrayList11;
        String str14;
        double d2;
        ActivityIntensities activityIntensities;
        ArrayList arrayList12;
        ArrayList arrayList13;
        TimeZone timeZone5;
        List<FitnessDataWrapper> list2 = list;
        wg6.b(list2, "$this$getListActivityData");
        wg6.b(str, "serial");
        wg6.b(str2, ButtonService.USER_ID);
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "getListActivityData");
        ArrayList arrayList14 = new ArrayList();
        ArrayList arrayList15 = new ArrayList();
        ArrayList arrayList16 = new ArrayList();
        if (list.isEmpty()) {
            return new pc6<>(arrayList14, arrayList15, arrayList16);
        }
        yd6.a(list2, ue6.a(c.INSTANCE, d.INSTANCE));
        FitnessDataWrapper fitnessDataWrapper = list2.get(0);
        int timezoneOffsetInSecond = fitnessDataWrapper.getTimezoneOffsetInSecond();
        DateTime withMillisOfSecond = fitnessDataWrapper.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
        DateTime plusMinutes = withMillisOfSecond.plusMinutes(1);
        wg6.a((Object) withMillisOfSecond, "startTime");
        int hourOfDay = withMillisOfSecond.getHourOfDay();
        wg6.a((Object) withMillisOfSecond, "startTime");
        int minuteOfHour = withMillisOfSecond.getMinuteOfHour();
        TimeZone a2 = bk4.a(timezoneOffsetInSecond);
        Date date = withMillisOfSecond.toLocalDateTime().toDate();
        String str15 = "startTime";
        wg6.a((Object) date, "startTime.toLocalDateTime().toDate()");
        wg6.a((Object) withMillisOfSecond, str15);
        wg6.a((Object) plusMinutes, "endTime");
        String str16 = "endTime";
        String str17 = "startTime.toLocalDateTime().toDate()";
        ArrayList arrayList17 = arrayList16;
        ArrayList arrayList18 = arrayList15;
        String str18 = str15;
        String str19 = "SyncDataExtensions";
        ActivityIntensities activityIntensities2 = r16;
        ActivityIntensities activityIntensities3 = new ActivityIntensities();
        ActivitySample activitySample2 = new ActivitySample(str2, date, withMillisOfSecond, plusMinutes, 0.0d, 0.0d, 0.0d, 0, activityIntensities2, timezoneOffsetInSecond, str, j);
        DateTime startTimeTZ = fitnessDataWrapper.getStartTimeTZ();
        double steps = activitySample2.getSteps();
        double calories = activitySample2.getCalories();
        double distance = activitySample2.getDistance();
        int activeTime = activitySample2.getActiveTime();
        List<Integer> intensitiesInArray = activitySample2.getIntensityDistInSteps().getIntensitiesInArray();
        String str20 = "timeZone";
        TimeZone timeZone6 = a2;
        wg6.a((Object) timeZone6, str20);
        ActivitySummary a3 = px5.a(startTimeTZ, steps, calories, distance, activeTime, intensitiesInArray, timeZone6.getID());
        int size = list.size();
        TimeZone timeZone7 = timeZone6;
        int i5 = 0;
        for (T next : list) {
            int i6 = i5 + 1;
            if (i5 >= 0) {
                FitnessDataWrapper fitnessDataWrapper2 = (FitnessDataWrapper) next;
                int timezoneOffsetInSecond2 = fitnessDataWrapper2.getTimezoneOffsetInSecond();
                FitnessDataWrapper fitnessDataWrapper3 = i6 < size ? list2.get(i6) : null;
                boolean z = fitnessDataWrapper3 == null || timezoneOffsetInSecond2 != fitnessDataWrapper3.getTimezoneOffsetInSecond();
                int size2 = fitnessDataWrapper2.getStep().getValues().size();
                int size3 = fitnessDataWrapper2.getCalorie().getValues().size();
                int size4 = fitnessDataWrapper2.getDistance().getValues().size();
                int size5 = fitnessDataWrapper2.getActiveMinute().getValues().size();
                String str21 = "currentSampleRaw.startTi\u2026nuteOfHour(currentMinute)";
                String str22 = "currentActivitySummary";
                if (size2 == size3 && size3 == size4 && size4 == size5) {
                    List<Short> values = fitnessDataWrapper2.getStep().getValues();
                    int resolutionInSecond = fitnessDataWrapper2.getStep().getResolutionInSecond();
                    int i7 = hourOfDay;
                    activitySample = activitySample2;
                    int i8 = 0;
                    for (T next2 : values) {
                        int i9 = i8 + 1;
                        if (i8 >= 0) {
                            short shortValue = ((Number) next2).shortValue();
                            DateTime withMillisOfSecond2 = fitnessDataWrapper2.getStartTimeTZ().plusMillis(i8 * resolutionInSecond * 1000).withSecondOfMinute(0).withMillisOfSecond(0);
                            int i10 = i6;
                            DateTime plusSeconds = withMillisOfSecond2.plusSeconds(resolutionInSecond);
                            wg6.a((Object) withMillisOfSecond2, "currentStartTime");
                            int hourOfDay2 = withMillisOfSecond2.getHourOfDay();
                            int minuteOfHour2 = withMillisOfSecond2.getMinuteOfHour();
                            int i11 = resolutionInSecond;
                            float floatValue = fitnessDataWrapper2.getCalorie().getValues().get(i8).floatValue();
                            int i12 = timezoneOffsetInSecond2;
                            double doubleValue = fitnessDataWrapper2.getDistance().getValues().get(i8).doubleValue();
                            ActivityIntensities activityIntensities4 = new ActivityIntensities();
                            TimeZone timeZone8 = timeZone7;
                            int i13 = size;
                            double d3 = (double) shortValue;
                            activityIntensities4.setIntensity(d3);
                            int resolutionInSecond2 = fitnessDataWrapper2.getActiveMinute().getValues().get(i8).booleanValue() ? fitnessDataWrapper2.getActiveMinute().getResolutionInSecond() / 60 : 0;
                            if (hourOfDay2 == i7 && minuteOfHour2 / 15 == minuteOfHour / 15) {
                                activitySample.setSteps(activitySample.getSteps() + d3);
                                activitySample.setCalories(activitySample.getCalories() + ((double) floatValue));
                                activitySample.setDistance(activitySample.getDistance() + doubleValue);
                                wg6.a((Object) plusSeconds, "currentEndTime");
                                activitySample.setEndTime(plusSeconds);
                                activitySample.setActiveTime(activitySample.getActiveTime() + resolutionInSecond2);
                                ActivityIntensities intensityDistInSteps = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps.setLight(intensityDistInSteps.getLight() + activityIntensities4.getLight());
                                ActivityIntensities intensityDistInSteps2 = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps2.setModerate(intensityDistInSteps2.getModerate() + activityIntensities4.getModerate());
                                ActivityIntensities intensityDistInSteps3 = activitySample.getIntensityDistInSteps();
                                intensityDistInSteps3.setIntense(intensityDistInSteps3.getIntense() + activityIntensities4.getIntense());
                                str13 = str21;
                                str12 = str22;
                                i7 = i7;
                                str11 = str20;
                                i4 = i10;
                                i3 = i13;
                                arrayList9 = arrayList18;
                                str14 = str19;
                                timeZone4 = timeZone8;
                                arrayList10 = arrayList14;
                                arrayList11 = arrayList17;
                            } else {
                                if (a(activitySample)) {
                                    d2 = d3;
                                    activityIntensities = activityIntensities4;
                                    arrayList12 = arrayList17;
                                    arrayList12.add(new GFitSample(rh6.a(activitySample.getSteps()), (float) activitySample.getDistance(), (float) activitySample.getCalories(), activitySample.getStartTime().getMillis(), activitySample.getEndTime().getMillis()));
                                    int minuteOfHour3 = activitySample.getStartTime().getMinuteOfHour();
                                    DateTime withMinuteOfHour = activitySample.getStartTime().withMinuteOfHour(minuteOfHour3 - (minuteOfHour3 % 15));
                                    wg6.a((Object) withMinuteOfHour, str21);
                                    activitySample.setStartTimeId(withMinuteOfHour);
                                    arrayList14.add(activitySample);
                                    if (bk4.d(a3.getDate(), activitySample.getDate())) {
                                        wg6.a((Object) a3, str22);
                                        a(a3, activitySample);
                                        arrayList13 = arrayList18;
                                        timeZone5 = timeZone8;
                                    } else {
                                        wg6.a((Object) a3, str22);
                                        arrayList13 = arrayList18;
                                        arrayList13.add(a3);
                                        DateTime dateTime = new DateTime((Object) activitySample.getDate());
                                        double steps2 = activitySample.getSteps();
                                        double calories2 = activitySample.getCalories();
                                        double distance2 = activitySample.getDistance();
                                        int activeTime2 = activitySample.getActiveTime();
                                        List<Integer> intensitiesInArray2 = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
                                        timeZone5 = timeZone8;
                                        wg6.a((Object) timeZone5, str20);
                                        a3 = px5.a(dateTime, steps2, calories2, distance2, activeTime2, intensitiesInArray2, timeZone5.getID());
                                    }
                                } else {
                                    d2 = d3;
                                    arrayList12 = arrayList17;
                                    timeZone5 = timeZone8;
                                    activityIntensities = activityIntensities4;
                                    arrayList13 = arrayList18;
                                }
                                ActivitySummary activitySummary = a3;
                                int hourOfDay3 = withMillisOfSecond2.getHourOfDay();
                                int minuteOfHour4 = withMillisOfSecond2.getMinuteOfHour();
                                String str23 = str19;
                                FLogger.INSTANCE.getLocal().d(str23, "getListActivityData - startTime=" + withMillisOfSecond2 + ", endTime=" + plusSeconds);
                                Date date2 = withMillisOfSecond2.toLocalDateTime().toDate();
                                wg6.a((Object) date2, "currentStartTime.toLocalDateTime().toDate()");
                                wg6.a((Object) plusSeconds, "currentEndTime");
                                double d4 = (double) floatValue;
                                TimeZone timeZone9 = timeZone5;
                                DateTime dateTime2 = withMillisOfSecond2;
                                str14 = str23;
                                DateTime dateTime3 = plusSeconds;
                                str12 = str22;
                                arrayList10 = arrayList14;
                                i4 = i10;
                                double d5 = doubleValue;
                                str13 = str21;
                                arrayList11 = arrayList12;
                                timeZone4 = timeZone9;
                                arrayList9 = arrayList13;
                                i3 = i13;
                                str11 = str20;
                                activitySample = new ActivitySample(str2, date2, dateTime2, dateTime3, d2, d4, d5, resolutionInSecond2, activityIntensities, i12, str, j);
                                a3 = activitySummary;
                                i7 = hourOfDay3;
                                minuteOfHour = minuteOfHour4;
                            }
                            cd6 cd6 = cd6.a;
                            str19 = str14;
                            arrayList17 = arrayList11;
                            i8 = i9;
                            i6 = i4;
                            resolutionInSecond = i11;
                            timezoneOffsetInSecond2 = i12;
                            str21 = str13;
                            size = i3;
                            arrayList14 = arrayList10;
                            str22 = str12;
                            timeZone7 = timeZone4;
                            arrayList18 = arrayList9;
                            str20 = str11;
                            List<FitnessDataWrapper> list3 = list;
                        } else {
                            qd6.c();
                            throw null;
                        }
                    }
                    arrayList2 = arrayList14;
                    str5 = str21;
                    str4 = str22;
                    i2 = i6;
                    timeZone = timeZone7;
                    i = size;
                    str3 = str20;
                    arrayList = arrayList18;
                    arrayList3 = arrayList17;
                    str6 = str19;
                    hourOfDay = i7;
                } else {
                    arrayList2 = arrayList14;
                    str5 = str21;
                    str4 = str22;
                    i2 = i6;
                    timeZone = timeZone7;
                    i = size;
                    str3 = str20;
                    arrayList = arrayList18;
                    arrayList3 = arrayList17;
                    str6 = str19;
                    FLogger.INSTANCE.getLocal().e(str6, "getSampleRawList(), steps, calories and distances data sizes do not match from SDK");
                    activitySample = activitySample2;
                }
                if (z) {
                    if (a(activitySample)) {
                        arrayList3.add(new GFitSample(rh6.a(activitySample.getSteps()), (float) activitySample.getDistance(), (float) activitySample.getCalories(), activitySample.getStartTime().getMillis(), activitySample.getEndTime().getMillis()));
                        int minuteOfHour5 = activitySample.getStartTime().getMinuteOfHour();
                        DateTime withMinuteOfHour2 = activitySample.getStartTime().withMinuteOfHour(minuteOfHour5 - (minuteOfHour5 % 15));
                        wg6.a((Object) withMinuteOfHour2, str5);
                        activitySample.setStartTimeId(withMinuteOfHour2);
                        arrayList7 = arrayList2;
                        arrayList7.add(activitySample);
                        if (bk4.d(a3.getDate(), activitySample.getDate())) {
                            wg6.a((Object) a3, str4);
                            a(a3, activitySample);
                            arrayList8 = arrayList;
                            arrayList8.add(a3);
                            str19 = str6;
                            timeZone2 = timeZone;
                            str10 = str3;
                        } else {
                            arrayList8 = arrayList;
                            wg6.a((Object) a3, str4);
                            arrayList8.add(a3);
                            DateTime dateTime4 = new DateTime((Object) activitySample.getDate());
                            double steps3 = activitySample.getSteps();
                            double calories3 = activitySample.getCalories();
                            double distance3 = activitySample.getDistance();
                            int activeTime3 = activitySample.getActiveTime();
                            List<Integer> intensitiesInArray3 = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
                            str19 = str6;
                            TimeZone timeZone10 = timeZone;
                            str10 = str3;
                            wg6.a((Object) timeZone10, str10);
                            timeZone2 = timeZone10;
                            ActivitySummary a4 = px5.a(dateTime4, steps3, calories3, distance3, activeTime3, intensitiesInArray3, timeZone10.getID());
                            wg6.a((Object) a4, "Interpolator.createActiv\u2026esInArray(), timeZone.id)");
                            arrayList8.add(a4);
                        }
                    } else {
                        str19 = str6;
                        arrayList7 = arrayList2;
                        timeZone2 = timeZone;
                        arrayList8 = arrayList;
                        str10 = str3;
                        wg6.a((Object) a3, str4);
                        arrayList8.add(a3);
                    }
                    if (fitnessDataWrapper3 != null) {
                        DateTime withMillisOfSecond3 = fitnessDataWrapper3.getStartTimeTZ().withSecondOfMinute(0).withMillisOfSecond(0);
                        DateTime plusMinutes2 = withMillisOfSecond3.plusMinutes(1);
                        String str24 = str18;
                        wg6.a((Object) withMillisOfSecond3, str24);
                        hourOfDay = withMillisOfSecond3.getHourOfDay();
                        wg6.a((Object) withMillisOfSecond3, str24);
                        minuteOfHour = withMillisOfSecond3.getMinuteOfHour();
                        TimeZone a5 = bk4.a(fitnessDataWrapper3.getTimezoneOffsetInSecond());
                        Date date3 = withMillisOfSecond3.toLocalDateTime().toDate();
                        Date date4 = date3;
                        String str25 = str17;
                        wg6.a((Object) date3, str25);
                        wg6.a((Object) withMillisOfSecond3, str24);
                        String str26 = str16;
                        wg6.a((Object) plusMinutes2, str26);
                        str9 = str26;
                        str8 = str25;
                        str7 = str24;
                        TimeZone timeZone11 = a5;
                        ArrayList arrayList19 = arrayList8;
                        ActivityIntensities activityIntensities5 = r16;
                        ActivityIntensities activityIntensities6 = new ActivityIntensities();
                        arrayList4 = arrayList7;
                        ActivitySample activitySample3 = new ActivitySample(str2, date4, withMillisOfSecond3, plusMinutes2, 0.0d, 0.0d, 0.0d, 0, activityIntensities5, timezoneOffsetInSecond, str, j);
                        Iterator it = arrayList19.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj = null;
                                break;
                            }
                            obj = it.next();
                            if (bk4.d(((ActivitySummary) obj).getDate(), a(fitnessDataWrapper3.getStartTimeTZ(), true))) {
                                break;
                            }
                        }
                        a3 = (ActivitySummary) obj;
                        if (a3 == null) {
                            DateTime startTimeTZ2 = fitnessDataWrapper3.getStartTimeTZ();
                            double steps4 = activitySample3.getSteps();
                            double calories4 = activitySample3.getCalories();
                            double distance4 = activitySample3.getDistance();
                            int activeTime4 = activitySample3.getActiveTime();
                            List<Integer> intensitiesInArray4 = activitySample3.getIntensityDistInSteps().getIntensitiesInArray();
                            timeZone3 = timeZone11;
                            wg6.a((Object) timeZone3, str10);
                            a3 = px5.a(startTimeTZ2, steps4, calories4, distance4, activeTime4, intensitiesInArray4, timeZone3.getID());
                            cd6 cd62 = cd6.a;
                            arrayList5 = arrayList19;
                        } else {
                            arrayList5 = arrayList19;
                            timeZone3 = timeZone11;
                            Boolean.valueOf(arrayList5.remove(a3));
                        }
                        timeZone2 = timeZone3;
                        activitySample = activitySample3;
                        activitySample2 = activitySample;
                        timeZone7 = timeZone2;
                        cd6 cd63 = cd6.a;
                        str20 = str10;
                        arrayList17 = arrayList3;
                        arrayList18 = arrayList5;
                        str17 = str8;
                        str18 = str7;
                        i5 = i2;
                        size = i;
                        arrayList14 = arrayList4;
                        list2 = list;
                        str16 = str9;
                    } else {
                        arrayList6 = arrayList8;
                        arrayList4 = arrayList7;
                        str9 = str16;
                        str8 = str17;
                        str7 = str18;
                    }
                } else {
                    str19 = str6;
                    str9 = str16;
                    str8 = str17;
                    str7 = str18;
                    arrayList4 = arrayList2;
                    timeZone2 = timeZone;
                    arrayList6 = arrayList;
                    str10 = str3;
                }
                activitySample2 = activitySample;
                timeZone7 = timeZone2;
                cd6 cd632 = cd6.a;
                str20 = str10;
                arrayList17 = arrayList3;
                arrayList18 = arrayList5;
                str17 = str8;
                str18 = str7;
                i5 = i2;
                size = i;
                arrayList14 = arrayList4;
                list2 = list;
                str16 = str9;
            } else {
                qd6.c();
                throw null;
            }
        }
        ArrayList arrayList20 = arrayList14;
        ArrayList arrayList21 = arrayList18;
        ArrayList arrayList22 = arrayList17;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("sampleRaw ");
        ArrayList arrayList23 = arrayList20;
        sb.append(arrayList23);
        sb.append(" \n summary ");
        sb.append(arrayList21);
        sb.append(" \n gFit ");
        sb.append(arrayList22);
        local.d(str19, sb.toString());
        return new pc6<>(arrayList23, arrayList21, arrayList22);
    }

    @DexIgnore
    public static final ActivitySummary a(ActivitySummary activitySummary, ActivitySample activitySample) {
        List list;
        ActivitySummary activitySummary2 = activitySummary;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("SyncDataExtensions", "calculateSample - currentSummary=" + activitySummary2 + ", newSample=" + activitySample);
        double steps = activitySample.getSteps() + activitySummary.getSteps();
        double calories = activitySample.getCalories() + activitySummary.getCalories();
        double distance = activitySample.getDistance() + activitySummary.getDistance();
        int activeTime = activitySample.getActiveTime() + activitySummary.getActiveTime();
        List<Integer> intensitiesInArray = activitySample.getIntensityDistInSteps().getIntensitiesInArray();
        List<Integer> intensities = activitySummary.getIntensities();
        if (intensitiesInArray.size() > intensities.size()) {
            ArrayList arrayList = new ArrayList(rd6.a(intensitiesInArray, 10));
            int i = 0;
            for (T next : intensitiesInArray) {
                int i2 = i + 1;
                if (i >= 0) {
                    arrayList.add(Integer.valueOf(((Number) next).intValue() + (i < intensities.size() ? intensities.get(i).intValue() : 0)));
                    i = i2;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            list = yd6.d(arrayList);
        } else {
            ArrayList arrayList2 = new ArrayList(rd6.a(intensities, 10));
            int i3 = 0;
            for (T next2 : intensities) {
                int i4 = i3 + 1;
                if (i3 >= 0) {
                    arrayList2.add(Integer.valueOf(((Number) next2).intValue() + (i3 < intensitiesInArray.size() ? intensitiesInArray.get(i3).intValue() : 0)));
                    i3 = i4;
                } else {
                    qd6.c();
                    throw null;
                }
            }
            list = yd6.d(arrayList2);
        }
        activitySummary2.setSteps(steps);
        activitySummary2.setCalories(calories);
        activitySummary2.setDistance(distance);
        activitySummary2.setActiveTime(activeTime);
        activitySummary2.setCreatedAt(activitySummary.getCreatedAt());
        activitySummary2.setIntensities(list);
        return activitySummary2;
    }

    @DexIgnore
    public static final List<MFSleepSession> a(List<FitnessDataWrapper> list, String str) {
        Iterator<T> it;
        SleepSessionHeartRate sleepSessionHeartRate;
        SleepState sleepState;
        List<FitnessDataWrapper> list2 = list;
        wg6.b(list2, "$this$getSleepSessions");
        wg6.b(str, "serial");
        ArrayList arrayList = new ArrayList();
        if (list.isEmpty()) {
            return arrayList;
        }
        yd6.a(list2, new e());
        Gson gson = new Gson();
        Iterator<T> it2 = list.iterator();
        while (it2.hasNext()) {
            FitnessDataWrapper fitnessDataWrapper = (FitnessDataWrapper) it2.next();
            for (SleepSessionWrapper sleepSessionWrapper : fitnessDataWrapper.getSleeps()) {
                long millis = sleepSessionWrapper.getStartTime().getMillis();
                int normalizedSleepQuality = sleepSessionWrapper.getNormalizedSleepQuality();
                SleepDistribution sleepDistribution = new SleepDistribution(sleepSessionWrapper.getAwakeMinutes(), sleepSessionWrapper.getLightSleepMinutes(), sleepSessionWrapper.getDeepSleepMinutes());
                ArrayList arrayList2 = new ArrayList();
                for (SleepStateChangeWrapper sleepStateChangeWrapper : sleepSessionWrapper.getStateChanges()) {
                    int component1 = sleepStateChangeWrapper.component1();
                    int component2 = sleepStateChangeWrapper.component2();
                    if (component1 == SleepState.AWAKE.ordinal()) {
                        sleepState = SleepState.AWAKE;
                    } else if (component1 == SleepState.SLEEP.ordinal()) {
                        sleepState = SleepState.SLEEP;
                    } else if (component1 == SleepState.DEEP_SLEEP.ordinal()) {
                        sleepState = SleepState.DEEP_SLEEP;
                    } else {
                        sleepState = SleepState.AWAKE;
                    }
                    arrayList2.add(new WrapperSleepStateChange(sleepState.ordinal(), (long) component2));
                }
                String a2 = gson.a(arrayList2);
                if (sleepSessionWrapper.getHeartRate() != null) {
                    List<Short> values = sleepSessionWrapper.getHeartRate().getValues();
                    int resolutionInSecond = sleepSessionWrapper.getHeartRate().getResolutionInSecond();
                    int size = values.size();
                    int i = 0;
                    int i2 = 0;
                    short s = Short.MAX_VALUE;
                    short s2 = Short.MIN_VALUE;
                    int i3 = 0;
                    while (i < size) {
                        Iterator<T> it3 = it2;
                        short shortValue = values.get(i).shortValue();
                        if (shortValue > s2) {
                            s2 = shortValue;
                        } else if (shortValue < s) {
                            s = shortValue;
                        }
                        if (shortValue > 0) {
                            i3 += shortValue;
                            i2++;
                        }
                        i++;
                        it2 = it3;
                    }
                    it = it2;
                    sleepSessionHeartRate = new SleepSessionHeartRate((float) (i2 > 0 ? i3 / i2 : 0), s2, s, resolutionInSecond, values);
                } else {
                    it = it2;
                    sleepSessionHeartRate = null;
                }
                long j = (long) 1000;
                int currentTimeMillis = (int) (System.currentTimeMillis() / j);
                long millis2 = sleepSessionWrapper.getEndTime().getMillis();
                long j2 = j;
                Date date = sleepSessionWrapper.getEndTime().toDate();
                wg6.a((Object) date, "sleepSession.endTime.toDate()");
                String str2 = a2;
                Gson gson2 = gson;
                int deepSleepMinutes = sleepSessionWrapper.getDeepSleepMinutes() + sleepSessionWrapper.getLightSleepMinutes();
                MFSleepSession mFSleepSession = r2;
                wg6.a((Object) str2, "sleepStatesJson");
                DateTime now = DateTime.now();
                DateTime dateTime = now;
                wg6.a((Object) now, "DateTime.now()");
                MFSleepSession mFSleepSession2 = new MFSleepSession(millis2, date, fitnessDataWrapper.getTimezoneOffsetInSecond(), str, currentTimeMillis, currentTimeMillis, (double) normalizedSleepQuality, ph4.Device.ordinal(), (int) (millis / j2), (int) (sleepSessionWrapper.getEndTime().getMillis() / j2), deepSleepMinutes, sleepDistribution, str2, sleepSessionHeartRate, dateTime);
                arrayList.add(mFSleepSession);
                gson = gson2;
                it2 = it;
            }
            String str3 = str;
        }
        FLogger.INSTANCE.getLocal().d("SyncDataExtensions", "sleepSessions " + arrayList);
        return arrayList;
    }

    @DexIgnore
    public static final boolean a(ActivitySample activitySample) {
        wg6.b(activitySample, "$this$isDataAvailable");
        double d2 = (double) 0;
        return activitySample.getSteps() > d2 || activitySample.getCalories() > d2 || activitySample.getDistance() > d2 || activitySample.getActiveTime() > 0;
    }

    @DexIgnore
    public static final Date a(DateTime dateTime, boolean z) {
        wg6.b(dateTime, "$this$toDate");
        if (z) {
            Date date = dateTime.toLocalDateTime().toDate();
            wg6.a((Object) date, "toLocalDateTime().toDate()");
            return date;
        }
        Date date2 = dateTime.toDate();
        wg6.a((Object) date2, "toDate()");
        return date2;
    }
}
