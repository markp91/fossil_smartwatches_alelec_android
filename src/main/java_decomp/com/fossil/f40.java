package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class f40 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ StackTraceElement[] c;
    @DexIgnore
    public /* final */ f40 d;

    @DexIgnore
    public f40(Throwable th, e40 e40) {
        this.a = th.getLocalizedMessage();
        this.b = th.getClass().getName();
        this.c = e40.a(th.getStackTrace());
        Throwable cause = th.getCause();
        this.d = cause != null ? new f40(cause, e40) : null;
    }
}
