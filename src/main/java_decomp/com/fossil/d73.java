package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class d73 implements Runnable {
    @DexIgnore
    public /* final */ e73 a;

    @DexIgnore
    public d73(e73 e73) {
        this.a = e73;
    }

    @DexIgnore
    public final void run() {
        e73 e73 = this.a;
        e73.g();
        if (e73.k().z.a()) {
            e73.b().A().a("Deferred Deep Link already retrieved. Not fetching again.");
            return;
        }
        long a2 = e73.k().A.a();
        e73.k().A.a(1 + a2);
        if (a2 >= 5) {
            e73.b().w().a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
            e73.k().z.a(true);
            return;
        }
        e73.a.m();
    }
}
