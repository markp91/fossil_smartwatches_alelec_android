package com.fossil;

import android.annotation.SuppressLint;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vo {
    @DexIgnore
    public static /* final */ vo b; // = new vo();
    @DexIgnore
    public static /* final */ int c; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int d;
    @DexIgnore
    public static /* final */ int e;
    @DexIgnore
    public /* final */ Executor a; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Executor {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void execute(Runnable runnable) {
            new Handler(Looper.getMainLooper()).post(runnable);
        }
    }

    /*
    static {
        int i = c;
        d = i + 1;
        e = (i * 2) + 1;
    }
    */

    @DexIgnore
    public static ExecutorService a() {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(d, e, 1, TimeUnit.SECONDS, new LinkedBlockingQueue());
        a(threadPoolExecutor, true);
        return threadPoolExecutor;
    }

    @DexIgnore
    public static Executor b() {
        return b.a;
    }

    @DexIgnore
    @SuppressLint({"NewApi"})
    public static void a(ThreadPoolExecutor threadPoolExecutor, boolean z) {
        if (Build.VERSION.SDK_INT >= 9) {
            threadPoolExecutor.allowCoreThreadTimeOut(z);
        }
    }
}
