package com.fossil;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s00 {
    @DexIgnore
    public static /* final */ g<Object> a; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g<Object> {
        @DexIgnore
        public void a(Object obj) {
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements d<List<T>> {
        @DexIgnore
        public List<T> create() {
            return new ArrayList();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements g<List<T>> {
        @DexIgnore
        public void a(List<T> list) {
            list.clear();
        }
    }

    @DexIgnore
    public interface d<T> {
        @DexIgnore
        T create();
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        u00 d();
    }

    @DexIgnore
    public interface g<T> {
        @DexIgnore
        void a(T t);
    }

    @DexIgnore
    public static <T extends f> v8<T> a(int i, d<T> dVar) {
        return a(new x8(i), dVar);
    }

    @DexIgnore
    public static <T> v8<List<T>> b() {
        return a(20);
    }

    @DexIgnore
    public static <T> v8<List<T>> a(int i) {
        return a(new x8(i), new b(), new c());
    }

    @DexIgnore
    public static <T extends f> v8<T> a(v8<T> v8Var, d<T> dVar) {
        return a(v8Var, dVar, a());
    }

    @DexIgnore
    public static <T> v8<T> a(v8<T> v8Var, d<T> dVar, g<T> gVar) {
        return new e(v8Var, dVar, gVar);
    }

    @DexIgnore
    public static <T> g<T> a() {
        return a;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e<T> implements v8<T> {
        @DexIgnore
        public /* final */ d<T> a;
        @DexIgnore
        public /* final */ g<T> b;
        @DexIgnore
        public /* final */ v8<T> c;

        @DexIgnore
        public e(v8<T> v8Var, d<T> dVar, g<T> gVar) {
            this.c = v8Var;
            this.a = dVar;
            this.b = gVar;
        }

        @DexIgnore
        public T a() {
            T a2 = this.c.a();
            if (a2 == null) {
                a2 = this.a.create();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + a2.getClass());
                }
            }
            if (a2 instanceof f) {
                ((f) a2).d().a(false);
            }
            return a2;
        }

        @DexIgnore
        public boolean a(T t) {
            if (t instanceof f) {
                ((f) t).d().a(true);
            }
            this.b.a(t);
            return this.c.a(t);
        }
    }
}
