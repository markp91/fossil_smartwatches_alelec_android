package com.fossil;

import android.os.Bundle;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mi4 extends ii4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ Bundle c;

    @DexIgnore
    public mi4(String str, int i, Bundle bundle) {
        this.a = i;
        this.b = str;
        this.c = bundle;
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public Bundle b() {
        return this.c;
    }

    @DexIgnore
    public String c() {
        return this.b;
    }
}
