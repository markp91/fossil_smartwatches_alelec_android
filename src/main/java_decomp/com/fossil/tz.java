package com.fossil;

import android.graphics.drawable.Drawable;
import androidx.recyclerview.widget.RecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class tz<T> implements yz<T> {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public jz c;

    @DexIgnore
    public tz() {
        this(RecyclerView.UNDEFINED_DURATION, RecyclerView.UNDEFINED_DURATION);
    }

    @DexIgnore
    public void a() {
    }

    @DexIgnore
    public void a(Drawable drawable) {
    }

    @DexIgnore
    public final void a(jz jzVar) {
        this.c = jzVar;
    }

    @DexIgnore
    public final void a(xz xzVar) {
    }

    @DexIgnore
    public void b() {
    }

    @DexIgnore
    public void b(Drawable drawable) {
    }

    @DexIgnore
    public final void b(xz xzVar) {
        xzVar.a(this.a, this.b);
    }

    @DexIgnore
    public void c() {
    }

    @DexIgnore
    public final jz d() {
        return this.c;
    }

    @DexIgnore
    public tz(int i, int i2) {
        if (r00.b(i, i2)) {
            this.a = i;
            this.b = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }
}
