package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g74 extends f74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j B; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray C; // = new SparseIntArray();
    @DexIgnore
    public long A;

    /*
    static {
        C.put(2131362482, 1);
        C.put(2131362442, 2);
        C.put(2131363244, 3);
        C.put(2131361902, 4);
        C.put(2131362645, 5);
        C.put(2131362097, 6);
        C.put(2131362656, 7);
        C.put(2131362615, 8);
        C.put(2131362653, 9);
        C.put(2131362291, 10);
        C.put(2131362684, 11);
        C.put(2131362886, 12);
    }
    */

    @DexIgnore
    public g74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 13, B, C));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.A = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.A != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.A = 1;
        }
        g();
    }

    @DexIgnore
    public g74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[4], objArr[6], objArr[10], objArr[2], objArr[1], objArr[8], objArr[5], objArr[9], objArr[7], objArr[11], objArr[0], objArr[12], objArr[3]);
        this.A = -1;
        this.y.setTag((Object) null);
        a(view);
        f();
    }
}
