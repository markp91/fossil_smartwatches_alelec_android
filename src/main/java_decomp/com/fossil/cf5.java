package com.fossil;

import com.portfolio.platform.data.source.FitnessDataRepository;
import com.portfolio.platform.data.source.HeartRateSummaryRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.diana.heartrate.HeartRateDailySummaryDao;
import com.portfolio.platform.data.source.local.fitness.FitnessDatabase;
import com.portfolio.platform.uirenew.home.dashboard.heartrate.DashboardHeartRatePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cf5 implements Factory<bf5> {
    @DexIgnore
    public static DashboardHeartRatePresenter a(af5 af5, HeartRateSummaryRepository heartRateSummaryRepository, FitnessDataRepository fitnessDataRepository, HeartRateDailySummaryDao heartRateDailySummaryDao, FitnessDatabase fitnessDatabase, UserRepository userRepository, u04 u04) {
        return new DashboardHeartRatePresenter(af5, heartRateSummaryRepository, fitnessDataRepository, heartRateDailySummaryDao, fitnessDatabase, userRepository, u04);
    }
}
