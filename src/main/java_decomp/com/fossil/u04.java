package com.fossil;

import android.os.Handler;
import android.os.Looper;
import com.misfit.frameworks.common.constants.Constants;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u04 {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public void execute(Runnable runnable) {
            wg6.b(runnable, Constants.COMMAND);
            this.a.post(runnable);
        }
    }

    @DexIgnore
    public u04(Executor executor, Executor executor2, Executor executor3) {
        this.a = executor;
        this.b = executor2;
    }

    @DexIgnore
    public Executor a() {
        return this.a;
    }

    @DexIgnore
    public Executor b() {
        return this.b;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public u04() {
        this(r0, r1, new a());
        ExecutorService newSingleThreadExecutor = Executors.newSingleThreadExecutor();
        wg6.a((Object) newSingleThreadExecutor, "Executors.newSingleThreadExecutor()");
        ExecutorService newFixedThreadPool = Executors.newFixedThreadPool(3);
        wg6.a((Object) newFixedThreadPool, "Executors.newFixedThreadPool(3)");
    }
}
