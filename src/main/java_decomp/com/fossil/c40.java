package com.fossil;

import com.fossil.y30;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c40 implements y30 {
    @DexIgnore
    public /* final */ File a;
    @DexIgnore
    public /* final */ File[] b;
    @DexIgnore
    public /* final */ Map<String, String> c;

    @DexIgnore
    public c40(File file) {
        this(file, Collections.emptyMap());
    }

    @DexIgnore
    public Map<String, String> a() {
        return Collections.unmodifiableMap(this.c);
    }

    @DexIgnore
    public String b() {
        String e = e();
        return e.substring(0, e.lastIndexOf(46));
    }

    @DexIgnore
    public File c() {
        return this.a;
    }

    @DexIgnore
    public File[] d() {
        return this.b;
    }

    @DexIgnore
    public String e() {
        return c().getName();
    }

    @DexIgnore
    public y30.a getType() {
        return y30.a.JAVA;
    }

    @DexIgnore
    public void remove() {
        l86 g = c86.g();
        g.d("CrashlyticsCore", "Removing report at " + this.a.getPath());
        this.a.delete();
    }

    @DexIgnore
    public c40(File file, Map<String, String> map) {
        this.a = file;
        this.b = new File[]{file};
        this.c = new HashMap(map);
        if (this.a.length() == 0) {
            this.c.putAll(z30.g);
        }
    }
}
