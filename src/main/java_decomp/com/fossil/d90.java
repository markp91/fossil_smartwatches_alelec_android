package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class d90 extends p40 implements Parcelable {
    @DexIgnore
    public /* final */ e90 a;
    @DexIgnore
    public /* final */ byte b;

    @DexIgnore
    public d90(e90 e90, byte b2) {
        this.a = e90;
        this.b = b2;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(new JSONObject(), bm0.EVENT_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.SEQUENCE, (Object) Byte.valueOf(this.b));
    }

    @DexIgnore
    public final byte b() {
        return this.b;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            d90 d90 = (d90) obj;
            return this.a == d90.a && this.b == d90.b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.DeviceEvent");
    }

    @DexIgnore
    public final e90 getDeviceEventId() {
        return this.a;
    }

    @DexIgnore
    public int hashCode() {
        return (this.a.hashCode() * 31) + this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public d90(Parcel parcel) {
        this(e90.valueOf(r0), parcel.readByte());
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            return;
        }
        wg6.a();
        throw null;
    }
}
