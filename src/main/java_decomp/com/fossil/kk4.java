package com.fossil;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import com.fossil.wv1;
import com.fossil.y62;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.data.DataType;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class kk4 {
    @DexIgnore
    public static /* final */ String c; // = "kk4";
    @DexIgnore
    public static wv1 d;
    @DexIgnore
    public static d e;
    @DexIgnore
    public an4 a;
    @DexIgnore
    public /* final */ Executor b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements wv1.c {
        @DexIgnore
        public a(kk4 kk4) {
        }

        @DexIgnore
        public void a(gv1 gv1) {
            d dVar = kk4.e;
            if (dVar != null) {
                dVar.a(gv1);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements wv1.b {
        @DexIgnore
        public b(kk4 kk4) {
        }

        @DexIgnore
        public void f(Bundle bundle) {
            d dVar = kk4.e;
            if (dVar != null) {
                dVar.a();
            }
        }

        @DexIgnore
        public void g(int i) {
            if (i == 2) {
                FLogger.INSTANCE.getLocal().d(kk4.c, "Connection lost.  Cause: Network Lost.");
            } else if (i == 1) {
                FLogger.INSTANCE.getLocal().d(kk4.c, "Connection lost.  Reason: Service Disconnected");
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            yv1 a2 = x62.e.a(kk4.d);
            d dVar = kk4.e;
            if (dVar != null) {
                dVar.a((yv1<Status>) a2);
            }
            kk4.this.a.h(false);
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();

        @DexIgnore
        void a(gv1 gv1);

        @DexIgnore
        void a(yv1<Status> yv1);
    }

    @DexIgnore
    public kk4(Context context, Executor executor, an4 an4) {
        if (d == null) {
            wv1.a aVar = new wv1.a(context);
            aVar.a(x62.b, new Scope[0]);
            aVar.a(x62.a, new Scope[0]);
            aVar.a(x62.d, new Scope[0]);
            aVar.a(new String[]{"https://www.googleapis.com/auth/fitness.activity.write", "https://www.googleapis.com/auth/fitness.body.write", "https://www.googleapis.com/auth/fitness.location.write"});
            aVar.a(new b(this));
            aVar.a(new a(this));
            d = aVar.a();
        }
        this.a = an4;
        if (f()) {
            a();
        }
        this.b = executor;
    }

    @DexIgnore
    public void a() {
        d.c();
        this.a.h(true);
    }

    @DexIgnore
    public void b() {
        d.d();
        this.a.h(false);
    }

    @DexIgnore
    public final y62 c() {
        y62.a b2 = y62.b();
        b2.a(DataType.j, 1);
        return b2.a();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public boolean d() {
        return ot1.a(ot1.a(PortfolioApp.T), c());
    }

    @DexIgnore
    public boolean e() {
        return d.g() && d();
    }

    @DexIgnore
    public boolean f() {
        return this.a.g();
    }

    @DexIgnore
    public void g() {
        this.b.execute(new c());
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void h() {
        try {
            b();
            ot1.a(PortfolioApp.T, new GoogleSignInOptions.a(GoogleSignInOptions.t).a()).j();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    @DexIgnore
    public void i() {
        this.a.h(false);
    }

    @DexIgnore
    public void a(d dVar) {
        e = dVar;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v0, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public void a(Fragment fragment) {
        ot1.a(fragment, 1, ot1.a(PortfolioApp.T), c());
    }
}
