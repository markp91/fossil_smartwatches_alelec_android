package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum du0 {
    LEGACY_ABORT_FILE((byte) 7),
    LEGACY_PUT_FILE((byte) 11),
    LEGACY_VERIFY_FILE((byte) 13),
    LEGACY_PUT_FILE_EOF((byte) 15),
    LEGACY_VERIFY_SEGMENT((byte) 16),
    LEGACY_ERASE_SEGMENT((byte) 18),
    LEGACY_GET_SIZE_WRITTEN((byte) 20),
    LEGACY_LIST_FILE((byte) 5),
    LEGACY_GET_FILE((byte) 1),
    LEGACY_ERASE_FILE((byte) 3);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public du0(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return (byte) (this.a + 1);
    }
}
