package com.fossil;

import android.annotation.TargetApi;
import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class aa6 extends ThreadPoolExecutor {
    @DexIgnore
    public static /* final */ int a; // = Runtime.getRuntime().availableProcessors();
    @DexIgnore
    public static /* final */ int b;
    @DexIgnore
    public static /* final */ int c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements ThreadFactory {
        @DexIgnore
        public /* final */ int a;

        @DexIgnore
        public a(int i) {
            this.a = i;
        }

        @DexIgnore
        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable);
            thread.setPriority(this.a);
            thread.setName("Queue");
            return thread;
        }
    }

    /*
    static {
        int i = a;
        b = i + 1;
        c = (i * 2) + 1;
    }
    */

    @DexIgnore
    public <T extends Runnable & r96 & ba6 & y96> aa6(int i, int i2, long j, TimeUnit timeUnit, s96<T> s96, ThreadFactory threadFactory) {
        super(i, i2, j, timeUnit, s96, threadFactory);
        prestartAllCoreThreads();
    }

    @DexIgnore
    public static <T extends Runnable & r96 & ba6 & y96> aa6 a(int i, int i2) {
        return new aa6(i, i2, 1, TimeUnit.SECONDS, new s96(), new a(10));
    }

    @DexIgnore
    public void afterExecute(Runnable runnable, Throwable th) {
        ba6 ba6 = (ba6) runnable;
        ba6.a(true);
        ba6.a(th);
        getQueue().recycleBlockedQueue();
        super.afterExecute(runnable, th);
    }

    @DexIgnore
    @TargetApi(9)
    public void execute(Runnable runnable) {
        if (z96.b(runnable)) {
            super.execute(runnable);
        } else {
            super.execute(newTaskFor(runnable, (Object) null));
        }
    }

    @DexIgnore
    public <T> RunnableFuture<T> newTaskFor(Runnable runnable, T t) {
        return new x96(runnable, t);
    }

    @DexIgnore
    public static aa6 a() {
        return a(b, c);
    }

    @DexIgnore
    public s96 getQueue() {
        return (s96) super.getQueue();
    }

    @DexIgnore
    public <T> RunnableFuture<T> newTaskFor(Callable<T> callable) {
        return new x96(callable);
    }
}
