package com.fossil;

import com.google.gson.JsonElement;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu3 {
    @DexIgnore
    public JsonElement a(String str) throws qu3 {
        return a((Reader) new StringReader(str));
    }

    @DexIgnore
    public JsonElement a(Reader reader) throws iu3, qu3 {
        try {
            JsonReader jsonReader = new JsonReader(reader);
            JsonElement a = a(jsonReader);
            if (!a.h()) {
                if (jsonReader.N() != rv3.END_DOCUMENT) {
                    throw new qu3("Did not consume the entire document.");
                }
            }
            return a;
        } catch (sv3 e) {
            throw new qu3((Throwable) e);
        } catch (IOException e2) {
            throw new iu3((Throwable) e2);
        } catch (NumberFormatException e3) {
            throw new qu3((Throwable) e3);
        }
    }

    @DexIgnore
    public JsonElement a(JsonReader jsonReader) throws iu3, qu3 {
        boolean D = jsonReader.D();
        jsonReader.b(true);
        try {
            JsonElement a = iv3.a(jsonReader);
            jsonReader.b(D);
            return a;
        } catch (StackOverflowError e) {
            throw new lu3("Failed parsing JSON source: " + jsonReader + " to Json", e);
        } catch (OutOfMemoryError e2) {
            throw new lu3("Failed parsing JSON source: " + jsonReader + " to Json", e2);
        } catch (Throwable th) {
            jsonReader.b(D);
            throw th;
        }
    }
}
