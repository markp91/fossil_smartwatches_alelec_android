package com.fossil;

import android.database.AbstractWindowedCursor;
import android.database.Cursor;
import android.os.Build;
import android.os.CancellationSignal;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bi {
    @DexIgnore
    @Deprecated
    public static Cursor a(oh ohVar, li liVar, boolean z) {
        return a(ohVar, liVar, z, (CancellationSignal) null);
    }

    @DexIgnore
    public static Cursor a(oh ohVar, li liVar, boolean z, CancellationSignal cancellationSignal) {
        Cursor query = ohVar.query(liVar, cancellationSignal);
        if (!z || !(query instanceof AbstractWindowedCursor)) {
            return query;
        }
        AbstractWindowedCursor abstractWindowedCursor = (AbstractWindowedCursor) query;
        int count = abstractWindowedCursor.getCount();
        return (Build.VERSION.SDK_INT < 23 || (abstractWindowedCursor.hasWindow() ? abstractWindowedCursor.getWindow().getNumRows() : count) < count) ? ai.a(abstractWindowedCursor) : query;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public static void a(ii iiVar) {
        ArrayList<String> arrayList = new ArrayList<>();
        Cursor d = iiVar.d("SELECT name FROM sqlite_master WHERE type = 'trigger'");
        while (d.moveToNext()) {
            try {
                arrayList.add(d.getString(0));
            } catch (Throwable th) {
                d.close();
                throw th;
            }
        }
        d.close();
        for (String str : arrayList) {
            if (str.startsWith("room_fts_content_sync_")) {
                iiVar.b("DROP TRIGGER IF EXISTS " + str);
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003e  */
    public static int a(File file) throws IOException {
        FileChannel fileChannel;
        try {
            ByteBuffer allocate = ByteBuffer.allocate(4);
            fileChannel = new FileInputStream(file).getChannel();
            try {
                fileChannel.tryLock(60, 4, true);
                fileChannel.position(60);
                if (fileChannel.read(allocate) == 4) {
                    allocate.rewind();
                    int i = allocate.getInt();
                    if (fileChannel != null) {
                        fileChannel.close();
                    }
                    return i;
                }
                throw new IOException("Bad database header, unable to read 4 bytes at offset 60");
            } catch (Throwable th) {
                th = th;
                if (fileChannel != null) {
                    fileChannel.close();
                }
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileChannel = null;
            if (fileChannel != null) {
            }
            throw th;
        }
    }
}
