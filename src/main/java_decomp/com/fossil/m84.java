package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class m84 extends l84 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public /* final */ NestedScrollView x;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362060, 1);
        A.put(2131363197, 2);
        A.put(2131363299, 3);
        A.put(2131362583, 4);
        A.put(2131362070, 5);
        A.put(2131363204, 6);
        A.put(2131363320, 7);
        A.put(2131362585, 8);
        A.put(2131363195, 9);
        A.put(2131363196, 10);
        A.put(2131362206, 11);
    }
    */

    @DexIgnore
    public m84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 12, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[5], objArr[11], objArr[4], objArr[8], objArr[9], objArr[10], objArr[2], objArr[6], objArr[3], objArr[7]);
        this.y = -1;
        this.x = objArr[0];
        this.x.setTag((Object) null);
        a(view);
        f();
    }
}
