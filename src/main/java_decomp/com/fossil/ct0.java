package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct0 extends ok0 {
    @DexIgnore
    public /* final */ li1 j; // = li1.HIGH;
    @DexIgnore
    public m51 k; // = m51.DISCONNECTED;

    @DexIgnore
    public ct0(at0 at0) {
        super(hm0.CONNECT_HID, at0);
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.a();
    }

    @DexIgnore
    public li1 b() {
        return this.j;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.k;
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        c(p51);
        t31 t31 = p51.a;
        if (t31.a != x11.SUCCESS) {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        } else if (this.k == m51.CONNECTED) {
            ch0 = ch0.a(this.d, (hm0) null, lf0.SUCCESS, (t31) null, 5);
        } else {
            ch0 = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        }
        this.d = ch0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r2 = ((com.fossil.sp0) r2).b;
     */
    @DexIgnore
    public boolean b(p51 p51) {
        m51 m51;
        return (p51 instanceof sp0) && (m51 == m51.CONNECTED || m51 == m51.DISCONNECTED);
    }

    @DexIgnore
    public void c(p51 p51) {
        this.k = ((sp0) p51).b;
    }
}
