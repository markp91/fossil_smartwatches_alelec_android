package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mm0 implements Parcelable.Creator<eo0> {
    @DexIgnore
    public /* synthetic */ mm0(qg6 qg6) {
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        return new eo0(parcel.readInt() != 0);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new eo0[i];
    }
}
