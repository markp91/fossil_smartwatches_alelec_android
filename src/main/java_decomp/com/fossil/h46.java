package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ r36 b;

    @DexIgnore
    public h46(Context context, r36 r36) {
        this.a = context;
        this.b = r36;
    }

    @DexIgnore
    public final void run() {
        try {
            q36.a(this.a, false, this.b);
        } catch (Throwable th) {
            q36.m.a(th);
        }
    }
}
