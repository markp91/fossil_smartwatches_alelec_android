package com.fossil;

import android.util.Log;
import com.fossil.hr;
import com.fossil.ku;
import java.io.File;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ou implements ku {
    @DexIgnore
    public /* final */ tu a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ long c;
    @DexIgnore
    public /* final */ mu d; // = new mu();
    @DexIgnore
    public hr e;

    @DexIgnore
    @Deprecated
    public ou(File file, long j) {
        this.b = file;
        this.c = j;
        this.a = new tu();
    }

    @DexIgnore
    public static ku a(File file, long j) {
        return new ou(file, j);
    }

    @DexIgnore
    public final synchronized hr a() throws IOException {
        if (this.e == null) {
            this.e = hr.a(this.b, 1, 1, this.c);
        }
        return this.e;
    }

    @DexIgnore
    public File a(vr vrVar) {
        String b2 = this.a.b(vrVar);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + b2 + " for for Key: " + vrVar);
        }
        try {
            hr.e f = a().f(b2);
            if (f != null) {
                return f.a(0);
            }
            return null;
        } catch (IOException e2) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e2);
            return null;
        }
    }

    @DexIgnore
    public void a(vr vrVar, ku.b bVar) {
        hr.c e2;
        String b2 = this.a.b(vrVar);
        this.d.a(b2);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + b2 + " for for Key: " + vrVar);
            }
            try {
                hr a2 = a();
                if (a2.f(b2) == null) {
                    e2 = a2.e(b2);
                    if (e2 != null) {
                        if (bVar.a(e2.a(0))) {
                            e2.c();
                        }
                        e2.b();
                        this.d.b(b2);
                        return;
                    }
                    throw new IllegalStateException("Had two simultaneous puts for: " + b2);
                }
            } catch (IOException e3) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e3);
                }
            } catch (Throwable th) {
                e2.b();
                throw th;
            }
        } finally {
            this.d.b(b2);
        }
    }
}
