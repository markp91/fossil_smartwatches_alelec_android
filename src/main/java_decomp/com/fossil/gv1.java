package com.fossil;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.u12;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gv1 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gv1> CREATOR; // = new h52();
    @DexIgnore
    public static /* final */ gv1 e; // = new gv1(0);
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ PendingIntent c;
    @DexIgnore
    public /* final */ String d;

    @DexIgnore
    public gv1(int i, int i2, PendingIntent pendingIntent, String str) {
        this.a = i;
        this.b = i2;
        this.c = pendingIntent;
        this.d = str;
    }

    @DexIgnore
    public static String zza(int i) {
        if (i == 99) {
            return "UNFINISHED";
        }
        if (i == 1500) {
            return "DRIVE_EXTERNAL_STORAGE_REQUIRED";
        }
        switch (i) {
            case -1:
                return "UNKNOWN";
            case 0:
                return "SUCCESS";
            case 1:
                return "SERVICE_MISSING";
            case 2:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
            case 3:
                return "SERVICE_DISABLED";
            case 4:
                return "SIGN_IN_REQUIRED";
            case 5:
                return "INVALID_ACCOUNT";
            case 6:
                return "RESOLUTION_REQUIRED";
            case 7:
                return "NETWORK_ERROR";
            case 8:
                return "INTERNAL_ERROR";
            case 9:
                return "SERVICE_INVALID";
            case 10:
                return "DEVELOPER_ERROR";
            case 11:
                return "LICENSE_CHECK_FAILED";
            default:
                switch (i) {
                    case 13:
                        return "CANCELED";
                    case 14:
                        return "TIMEOUT";
                    case 15:
                        return "INTERRUPTED";
                    case 16:
                        return "API_UNAVAILABLE";
                    case 17:
                        return "SIGN_IN_FAILED";
                    case 18:
                        return "SERVICE_UPDATING";
                    case 19:
                        return "SERVICE_MISSING_PERMISSION";
                    case 20:
                        return "RESTRICTED_PROFILE";
                    case 21:
                        return "API_VERSION_UPDATE_REQUIRED";
                    default:
                        StringBuilder sb = new StringBuilder(31);
                        sb.append("UNKNOWN_ERROR_CODE(");
                        sb.append(i);
                        sb.append(")");
                        return sb.toString();
                }
        }
    }

    @DexIgnore
    public final String B() {
        return this.d;
    }

    @DexIgnore
    public final PendingIntent C() {
        return this.c;
    }

    @DexIgnore
    public final boolean D() {
        return (this.b == 0 || this.c == null) ? false : true;
    }

    @DexIgnore
    public final boolean E() {
        return this.b == 0;
    }

    @DexIgnore
    public final void a(Activity activity, int i) throws IntentSender.SendIntentException {
        if (D()) {
            activity.startIntentSenderForResult(this.c.getIntentSender(), i, (Intent) null, 0, 0, 0);
        }
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof gv1)) {
            return false;
        }
        gv1 gv1 = (gv1) obj;
        return this.b == gv1.b && u12.a(this.c, gv1.c) && u12.a(this.d, gv1.d);
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Integer.valueOf(this.b), this.c, this.d);
    }

    @DexIgnore
    public final int p() {
        return this.b;
    }

    @DexIgnore
    public final String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("statusCode", zza(this.b));
        a2.a("resolution", this.c);
        a2.a("message", this.d);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, p());
        g22.a(parcel, 3, (Parcelable) C(), i, false);
        g22.a(parcel, 4, B(), false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public gv1(int i) {
        this(i, (PendingIntent) null, (String) null);
    }

    @DexIgnore
    public gv1(int i, PendingIntent pendingIntent) {
        this(i, pendingIntent, (String) null);
    }

    @DexIgnore
    public gv1(int i, PendingIntent pendingIntent, String str) {
        this(1, i, pendingIntent, str);
    }
}
