package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.fossil.wearables.fsl.contact.Contact;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mu4 extends RecyclerView.g<b> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public c a;
    @DexIgnore
    public List<? extends Object> b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ nz d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ tg4 a;
        @DexIgnore
        public /* final */ /* synthetic */ mu4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                c b;
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a2 = this.a.b.b;
                    if (a2 != null) {
                        Object obj = a2.get(adapterPosition);
                        if (obj instanceof wx4) {
                            wx4 wx4 = (wx4) obj;
                            Contact contact = wx4.getContact();
                            if (contact == null || contact.getContactId() != -100) {
                                Contact contact2 = wx4.getContact();
                                if ((contact2 == null || contact2.getContactId() != -200) && (b = this.a.b.a) != null) {
                                    b.a(wx4);
                                    return;
                                }
                                return;
                            }
                            return;
                        }
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mu4$b$b")
        /* renamed from: com.fossil.mu4$b$b  reason: collision with other inner class name */
        public static final class C0030b implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public C0030b(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                FLogger.INSTANCE.getLocal().d(mu4.e, "ivRemove.setOnClickListener");
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List a2 = this.a.b.b;
                    if (a2 != null) {
                        Object obj = a2.get(adapterPosition);
                        if (obj instanceof wx4) {
                            wx4 wx4 = (wx4) obj;
                            wx4.setAdded(!wx4.isAdded());
                            ILocalFLogger local = FLogger.INSTANCE.getLocal();
                            String d = mu4.e;
                            local.d(d, "isAdded=" + wx4.isAdded());
                            if (wx4.isAdded()) {
                                mu4 mu4 = this.a.b;
                                mu4.c = mu4.c + 1;
                            } else {
                                mu4 mu42 = this.a.b;
                                mu42.c = mu42.c - 1;
                            }
                        } else if (obj != null) {
                            AppWrapper appWrapper = (AppWrapper) obj;
                            InstalledApp installedApp = appWrapper.getInstalledApp();
                            if (installedApp != null) {
                                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                                Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
                                if (isSelected != null) {
                                    installedApp.setSelected(!isSelected.booleanValue());
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String d2 = mu4.e;
                            StringBuilder sb = new StringBuilder();
                            sb.append("isSelected=");
                            InstalledApp installedApp3 = appWrapper.getInstalledApp();
                            Boolean isSelected2 = installedApp3 != null ? installedApp3.isSelected() : null;
                            if (isSelected2 != null) {
                                sb.append(isSelected2.booleanValue());
                                local2.d(d2, sb.toString());
                                InstalledApp installedApp4 = appWrapper.getInstalledApp();
                                Boolean isSelected3 = installedApp4 != null ? installedApp4.isSelected() : null;
                                if (isSelected3 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (isSelected3.booleanValue()) {
                                    mu4 mu43 = this.a.b;
                                    mu43.c = mu43.c + 1;
                                } else {
                                    mu4 mu44 = this.a.b;
                                    mu44.c = mu44.c - 1;
                                }
                            } else {
                                wg6.a();
                                throw null;
                            }
                        } else {
                            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
                        }
                        c b = this.a.b.a;
                        if (b != null) {
                            b.a();
                        }
                        this.a.b.notifyItemChanged(adapterPosition);
                        return;
                    }
                    wg6.a();
                    throw null;
                }
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements mz<Drawable> {
            @DexIgnore
            public /* final */ /* synthetic */ b a;
            @DexIgnore
            public /* final */ /* synthetic */ wx4 b;

            @DexIgnore
            public c(b bVar, wx4 wx4) {
                this.a = bVar;
                this.b = wx4;
            }

            @DexIgnore
            public boolean a(Drawable drawable, Object obj, yz<Drawable> yzVar, pr prVar, boolean z) {
                FLogger.INSTANCE.getLocal().d(mu4.e, "renderContactData onResourceReady");
                this.a.a.t.setImageDrawable(drawable);
                return false;
            }

            @DexIgnore
            public boolean a(mt mtVar, Object obj, yz<Drawable> yzVar, boolean z) {
                FLogger.INSTANCE.getLocal().d(mu4.e, "renderContactData onLoadFailed");
                Contact contact = this.b.getContact();
                if (contact == null) {
                    return false;
                }
                contact.setPhotoThumbUri((String) null);
                return false;
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(mu4 mu4, tg4 tg4) {
            super(tg4.d());
            wg6.b(tg4, "binding");
            this.b = mu4;
            this.a = tg4;
            String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            String b3 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b2)) {
                this.a.v.setBackgroundColor(Color.parseColor(b2));
            }
            if (!TextUtils.isEmpty(b3)) {
                this.a.q.setBackgroundColor(Color.parseColor(b3));
            }
            this.a.q.setOnClickListener(new a(this));
            this.a.u.setOnClickListener(new C0030b(this));
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v10, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v12, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r1v3, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v19, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v25, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v28, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v37, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v56, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r2v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r0v61, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        public final void a(wx4 wx4) {
            Uri uri;
            String str;
            String str2;
            String str3;
            Contact contact;
            String str4;
            wg6.b(wx4, "contactWrapper");
            Contact contact2 = wx4.getContact();
            if (!TextUtils.isEmpty(contact2 != null ? contact2.getPhotoThumbUri() : null)) {
                Contact contact3 = wx4.getContact();
                uri = Uri.parse(contact3 != null ? contact3.getPhotoThumbUri() : null);
            } else {
                uri = null;
            }
            Contact contact4 = wx4.getContact();
            String str5 = "";
            if (contact4 == null || (str = contact4.getFirstName()) == null) {
                str = str5;
            }
            Contact contact5 = wx4.getContact();
            if (contact5 == null || (str2 = contact5.getLastName()) == null) {
                str2 = str5;
            }
            Contact contact6 = wx4.getContact();
            if (contact6 == null || contact6.getContactId() != -100) {
                Contact contact7 = wx4.getContact();
                if (contact7 == null || contact7.getContactId() != -200) {
                    String str6 = str + " " + str2;
                    mj4 a2 = jj4.a((View) this.a.t);
                    Contact contact8 = wx4.getContact();
                    lj4 a3 = a2.a((Object) new ij4(uri, contact8 != null ? contact8.getDisplayName() : null)).a((gz) this.b.d);
                    mj4 a4 = jj4.a((View) this.a.t);
                    Contact contact9 = wx4.getContact();
                    wg6.a((Object) a3.a(a4.a((Object) new ij4((Uri) null, contact9 != null ? contact9.getDisplayName() : null)).a((gz) this.b.d)).b(new c(this, wx4)).a(this.a.t), "GlideApp.with(binding.iv\u2026nto(binding.ivHybridIcon)");
                    str3 = str6;
                } else {
                    this.a.t.setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231132));
                    str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886148);
                    wg6.a((Object) str3, "LanguageHelper.getString\u2026xt__MessagesFromEveryone)");
                }
            } else {
                this.a.t.setImageDrawable(w6.c(PortfolioApp.get.instance(), 2131231131));
                str3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886147);
                wg6.a((Object) str3, "LanguageHelper.getString\u2026_Text__CallsFromEveryone)");
            }
            Object r2 = this.a.s;
            wg6.a((Object) r2, "binding.ftvHybridName");
            r2.setText(str3);
            Contact contact10 = wx4.getContact();
            if ((contact10 == null || contact10.getContactId() != -100) && ((contact = wx4.getContact()) == null || contact.getContactId() != -200)) {
                Contact contact11 = wx4.getContact();
                Boolean valueOf = contact11 != null ? Boolean.valueOf(contact11.isUseSms()) : null;
                if (valueOf != null) {
                    if (valueOf.booleanValue()) {
                        Contact contact12 = wx4.getContact();
                        Boolean valueOf2 = contact12 != null ? Boolean.valueOf(contact12.isUseCall()) : null;
                        if (valueOf2 == null) {
                            wg6.a();
                            throw null;
                        } else if (valueOf2.booleanValue()) {
                            str4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886140);
                            wg6.a((Object) str4, "LanguageHelper.getString\u2026pped_Text__CallsMessages)");
                            Object r1 = this.a.r;
                            wg6.a((Object) r1, "binding.ftvHybridFeature");
                            r1.setText(str4);
                            Object r0 = this.a.r;
                            wg6.a((Object) r0, "binding.ftvHybridFeature");
                            r0.setVisibility(0);
                        }
                    }
                    Contact contact13 = wx4.getContact();
                    Boolean valueOf3 = contact13 != null ? Boolean.valueOf(contact13.isUseSms()) : null;
                    if (valueOf3 != null) {
                        if (valueOf3.booleanValue()) {
                            str5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886141);
                            wg6.a((Object) str5, "LanguageHelper.getString\u2026nedTapped_Text__Messages)");
                        }
                        str4 = str5;
                        Contact contact14 = wx4.getContact();
                        Boolean valueOf4 = contact14 != null ? Boolean.valueOf(contact14.isUseCall()) : null;
                        if (valueOf4 != null) {
                            if (valueOf4.booleanValue()) {
                                str4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886139);
                                wg6.a((Object) str4, "LanguageHelper.getString\u2026signedTapped_Text__Calls)");
                            }
                            Object r12 = this.a.r;
                            wg6.a((Object) r12, "binding.ftvHybridFeature");
                            r12.setText(str4);
                            Object r02 = this.a.r;
                            wg6.a((Object) r02, "binding.ftvHybridFeature");
                            r02.setVisibility(0);
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                Object r03 = this.a.r;
                wg6.a((Object) r03, "binding.ftvHybridFeature");
                r03.setText(str5);
                Object r04 = this.a.r;
                wg6.a((Object) r04, "binding.ftvHybridFeature");
                r04.setVisibility(8);
            }
            if (wx4.isAdded()) {
                this.a.u.setImageResource(2131231173);
            } else {
                this.a.u.setImageResource(2131231139);
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v7, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(AppWrapper appWrapper) {
            wg6.b(appWrapper, "appWrapper");
            mj4 a2 = jj4.a((View) this.a.t);
            InstalledApp installedApp = appWrapper.getInstalledApp();
            if (installedApp != null) {
                a2.a((Object) new gj4(installedApp)).a(new nz().a(new xj4()).c()).a(this.a.t);
                Object r0 = this.a.s;
                wg6.a((Object) r0, "binding.ftvHybridName");
                InstalledApp installedApp2 = appWrapper.getInstalledApp();
                r0.setText(installedApp2 != null ? installedApp2.getTitle() : null);
                Object r02 = this.a.r;
                wg6.a((Object) r02, "binding.ftvHybridFeature");
                r02.setVisibility(8);
                InstalledApp installedApp3 = appWrapper.getInstalledApp();
                Boolean isSelected = installedApp3 != null ? installedApp3.isSelected() : null;
                if (isSelected == null) {
                    wg6.a();
                    throw null;
                } else if (isSelected.booleanValue()) {
                    this.a.u.setImageResource(2131231173);
                } else {
                    this.a.u.setImageResource(2131231139);
                }
            } else {
                wg6.a();
                throw null;
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a();

        @DexIgnore
        void a(wx4 wx4);
    }

    /*
    static {
        new a((qg6) null);
        String name = mu4.class.getName();
        wg6.a((Object) name, "NotificationContactsAndA\u2026dAdapter::class.java.name");
        e = name;
    }
    */

    @DexIgnore
    public mu4() {
        nz a2 = new nz().a(new xj4()).c().a(ft.a).a(true);
        wg6.a((Object) a2, "RequestOptions()\n       \u2026   .skipMemoryCache(true)");
        this.d = a2;
    }

    @DexIgnore
    public int getItemCount() {
        List<? extends Object> list = this.b;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public final boolean c() {
        return getItemCount() + this.c <= 12;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        tg4 a2 = tg4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemNotificationHybridBi\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    public final void a(List<? extends Object> list) {
        wg6.b(list, "data");
        this.b = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        List<? extends Object> list = this.b;
        if (list != null) {
            Object obj = list.get(i);
            if (obj instanceof wx4) {
                bVar.a((wx4) obj);
            } else if (obj != null) {
                bVar.a((AppWrapper) obj);
            } else {
                throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper");
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(c cVar) {
        wg6.b(cVar, "listener");
        this.a = cVar;
    }
}
