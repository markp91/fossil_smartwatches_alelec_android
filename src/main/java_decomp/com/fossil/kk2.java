package com.fossil;

import android.content.Context;
import android.database.ContentObserver;
import android.os.Handler;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kk2 implements fk2 {
    @DexIgnore
    public static kk2 c;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ ContentObserver b;

    @DexIgnore
    public kk2(Context context) {
        this.a = context;
        this.b = new mk2(this, (Handler) null);
        context.getContentResolver().registerContentObserver(zj2.a, true, this.b);
    }

    @DexIgnore
    public static kk2 a(Context context) {
        kk2 kk2;
        synchronized (kk2.class) {
            if (c == null) {
                c = x6.a(context, "com.google.android.providers.gsf.permission.READ_GSERVICES") == 0 ? new kk2(context) : new kk2();
            }
            kk2 = c;
        }
        return kk2;
    }

    @DexIgnore
    /* renamed from: b */
    public final String zza(String str) {
        if (this.a == null) {
            return null;
        }
        try {
            return (String) ik2.a(new jk2(this, str));
        } catch (IllegalStateException | SecurityException e) {
            String valueOf = String.valueOf(str);
            Log.e("GservicesLoader", valueOf.length() != 0 ? "Unable to read GServices for: ".concat(valueOf) : new String("Unable to read GServices for: "), e);
            return null;
        }
    }

    @DexIgnore
    public kk2() {
        this.a = null;
        this.b = null;
    }

    @DexIgnore
    public static synchronized void a() {
        synchronized (kk2.class) {
            if (!(c == null || c.a == null || c.b == null)) {
                c.a.getContentResolver().unregisterContentObserver(c.b);
            }
            c = null;
        }
    }

    @DexIgnore
    public final /* synthetic */ String a(String str) {
        return zj2.a(this.a.getContentResolver(), str, (String) null);
    }
}
