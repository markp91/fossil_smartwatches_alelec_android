package com.fossil;

import android.annotation.SuppressLint;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"CommitPrefEdits"})
public class t30 {
    @DexIgnore
    public /* final */ cb6 a;
    @DexIgnore
    public /* final */ v20 b;

    @DexIgnore
    public t30(cb6 cb6, v20 v20) {
        this.a = cb6;
        this.b = v20;
    }

    @DexIgnore
    public static t30 a(cb6 cb6, v20 v20) {
        return new t30(cb6, v20);
    }

    @DexIgnore
    public void a(boolean z) {
        cb6 cb6 = this.a;
        cb6.a(cb6.edit().putBoolean("always_send_reports_opt_in", z));
    }

    @DexIgnore
    public boolean a() {
        if (!this.a.get().contains("preferences_migration_complete")) {
            db6 db6 = new db6(this.b);
            if (!this.a.get().contains("always_send_reports_opt_in") && db6.get().contains("always_send_reports_opt_in")) {
                boolean z = db6.get().getBoolean("always_send_reports_opt_in", false);
                cb6 cb6 = this.a;
                cb6.a(cb6.edit().putBoolean("always_send_reports_opt_in", z));
            }
            cb6 cb62 = this.a;
            cb62.a(cb62.edit().putBoolean("preferences_migration_complete", true));
        }
        return this.a.get().getBoolean("always_send_reports_opt_in", false);
    }
}
