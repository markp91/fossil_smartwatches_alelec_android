package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v32 extends cb2 implements t32 {
    @DexIgnore
    public v32(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    @DexIgnore
    public final boolean a(r52 r52, x52 x52) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (Parcelable) r52);
        eb2.a(zza, (IInterface) x52);
        Parcel a = a(5, zza);
        boolean a2 = eb2.a(a);
        a.recycle();
        return a2;
    }
}
