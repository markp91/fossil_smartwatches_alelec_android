package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lj6 implements kj6 {
    @DexIgnore
    public /* final */ Matcher a;
    @DexIgnore
    public /* final */ CharSequence b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fd6<hj6> implements jj6 {
        @DexIgnore
        public /* final */ /* synthetic */ lj6 a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.lj6$a$a")
        /* renamed from: com.fossil.lj6$a$a  reason: collision with other inner class name */
        public static final class C0025a extends xg6 implements hg6<Integer, hj6> {
            @DexIgnore
            public /* final */ /* synthetic */ a this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public C0025a(a aVar) {
                super(1);
                this.this$0 = aVar;
            }

            @DexIgnore
            public /* bridge */ /* synthetic */ Object invoke(Object obj) {
                return invoke(((Number) obj).intValue());
            }

            @DexIgnore
            public final hj6 invoke(int i) {
                return this.this$0.get(i);
            }
        }

        @DexIgnore
        public a(lj6 lj6) {
            this.a = lj6;
        }

        @DexIgnore
        public /* bridge */ boolean a(hj6 hj6) {
            return super.contains(hj6);
        }

        @DexIgnore
        public final /* bridge */ boolean contains(Object obj) {
            if (obj != null ? obj instanceof hj6 : true) {
                return a((hj6) obj);
            }
            return false;
        }

        @DexIgnore
        public hj6 get(int i) {
            wh6 a2 = oj6.b(this.a.b(), i);
            if (a2.e().intValue() < 0) {
                return null;
            }
            String group = this.a.b().group(i);
            wg6.a((Object) group, "matchResult.group(index)");
            return new hj6(group, a2);
        }

        @DexIgnore
        public boolean isEmpty() {
            return false;
        }

        @DexIgnore
        public Iterator<hj6> iterator() {
            return aj6.c(yd6.b(qd6.a((Collection<?>) this)), new C0025a(this)).iterator();
        }

        @DexIgnore
        public int a() {
            return this.a.b().groupCount() + 1;
        }
    }

    @DexIgnore
    public lj6(Matcher matcher, CharSequence charSequence) {
        wg6.b(matcher, "matcher");
        wg6.b(charSequence, "input");
        this.a = matcher;
        this.b = charSequence;
        new a(this);
    }

    @DexIgnore
    public final MatchResult b() {
        return this.a;
    }

    @DexIgnore
    public String getValue() {
        String group = b().group();
        wg6.a((Object) group, "matchResult.group()");
        return group;
    }

    @DexIgnore
    public kj6 next() {
        int end = b().end() + (b().end() == b().start() ? 1 : 0);
        if (end > this.b.length()) {
            return null;
        }
        Matcher matcher = this.a.pattern().matcher(this.b);
        wg6.a((Object) matcher, "matcher.pattern().matcher(input)");
        return oj6.b(matcher, end, this.b);
    }

    @DexIgnore
    public wh6 a() {
        return oj6.b(b());
    }
}
