package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mo0 extends wm0 {
    @DexIgnore
    public /* final */ ArrayList<hl1> D; // = cw0.a(this.B, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.ASYNC}));
    @DexIgnore
    public /* final */ n70 E;

    @DexIgnore
    public mo0(ue1 ue1, q41 q41, n70 n70) {
        super(ue1, q41, eh1.NOTIFY_MUSIC_EVENT, new da1(n70, ue1));
        this.E = n70;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.D;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.MUSIC_EVENT, (Object) this.E.a());
    }
}
