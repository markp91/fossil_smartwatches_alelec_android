package com.fossil;

import com.fossil.fm6;
import java.util.concurrent.locks.LockSupport;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class gm6 extends em6 {
    @DexIgnore
    public abstract Thread F();

    @DexIgnore
    public final void G() {
        Thread F = F();
        if (Thread.currentThread() != F) {
            pn6 a = qn6.a();
            if (a != null) {
                a.a(F);
            } else {
                LockSupport.unpark(F);
            }
        }
    }

    @DexIgnore
    public final void a(long j, fm6.c cVar) {
        wg6.b(cVar, "delayedTask");
        if (nl6.a()) {
            if (!(this != pl6.g)) {
                throw new AssertionError();
            }
        }
        pl6.g.b(j, cVar);
    }
}
