package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sa1 implements Parcelable.Creator<ie1> {
    @DexIgnore
    public /* synthetic */ sa1(qg6 qg6) {
    }

    @DexIgnore
    public final byte a(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(1);
    }

    @DexIgnore
    public final byte b(short s) {
        return ByteBuffer.allocate(2).putShort(s).get(0);
    }

    @DexIgnore
    public Object createFromParcel(Parcel parcel) {
        String readString = parcel.readString();
        if (readString == null) {
            readString = "";
        }
        String str = readString;
        byte readByte = parcel.readByte();
        byte readByte2 = parcel.readByte();
        byte[] createByteArray = parcel.createByteArray();
        if (createByteArray == null) {
            createByteArray = new byte[0];
        }
        return new ie1(str, readByte, readByte2, createByteArray, parcel.readLong(), parcel.readLong(), parcel.readLong(), parcel.readInt() != 0);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new ie1[i];
    }
}
