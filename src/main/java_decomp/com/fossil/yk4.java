package com.fossil;

import android.content.Context;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.sina.weibo.sdk.statistic.LogBuilder;
import java.util.ArrayList;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yk4 {
    @DexIgnore
    public static /* final */ PortfolioApp a; // = PortfolioApp.get.instance();
    @DexIgnore
    public static /* final */ yk4 b; // = new yk4();

    @DexIgnore
    public final SpannableString a(String str, String str2, float f) {
        wg6.b(str, "bigText");
        wg6.b(str2, "smallText");
        SpannableString spannableString = new SpannableString(str + str2);
        spannableString.setSpan(new RelativeSizeSpan(f), str.length(), str.length() + str2.length(), 0);
        return spannableString;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String b(int i) {
        switch (i) {
            case 1:
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886471);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sun)");
                return a2;
            case 2:
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886468);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026ain_StepsToday_Text__Mon)");
                return a3;
            case 3:
                String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886474);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026in_StepsToday_Text__Tues)");
                return a4;
            case 4:
                String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886475);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026ain_StepsToday_Text__Wed)");
                return a5;
            case 5:
                String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886472);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026in_StepsToday_Text__Thur)");
                return a6;
            case 6:
                String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886467);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026ain_StepsToday_Text__Fri)");
                return a7;
            case 7:
                String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886470);
                wg6.a((Object) a8, "LanguageHelper.getString\u2026ain_StepsToday_Text__Sat)");
                return a8;
            default:
                return "";
        }
    }

    @DexIgnore
    public final String c(int i) {
        String b2 = tk4.b(i);
        wg6.a((Object) b2, "NumberHelper.formatNumber(steps)");
        return b2;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final CharSequence d(int i) {
        String a2 = tk4.a(i / 60);
        wg6.a((Object) a2, "NumberHelper.formatBigNumber(hours)");
        String a3 = jm4.a((Context) a, 2131886870);
        wg6.a((Object) a3, "LanguageHelper.getString\u2026_SetGoalsSleep_Label__Hr)");
        String a4 = jm4.a((Context) a, 2131886871);
        wg6.a((Object) a4, "LanguageHelper.getString\u2026SetGoalsSleep_Label__Min)");
        CharSequence concat = TextUtils.concat(new CharSequence[]{a(a2, a3, 0.7f), a(' ' + tk4.a(i % 60), a4, 0.7f)});
        wg6.a((Object) concat, "TextUtils.concat(activeH\u2026ing, remainMinutesString)");
        return concat;
    }

    @DexIgnore
    public final char a(String str) {
        if (TextUtils.isEmpty(str)) {
            return '#';
        }
        if (str != null) {
            char upperCase = Character.toUpperCase(str.charAt(0));
            if (Character.isAlphabetic(upperCase)) {
                return upperCase;
            }
            return '#';
        }
        wg6.a();
        throw null;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public final String a(int i, float f) {
        float c = tk4.c(f * ((float) 100), 1);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("getSleepDaySummaryText", "roundedPercent : " + c);
        StringBuilder sb = new StringBuilder();
        nh6 nh6 = nh6.a;
        String string = PortfolioApp.get.instance().getString(i);
        wg6.a((Object) string, "PortfolioApp.instance.getString(stringId)");
        Object[] objArr = {String.valueOf(c)};
        String format = String.format(string, Arrays.copyOf(objArr, objArr.length));
        wg6.a((Object) format, "java.lang.String.format(format, *args)");
        sb.append(format);
        sb.append(" %");
        return sb.toString();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: type inference failed for: r6v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String b(String str) {
        wg6.b(str, LogBuilder.KEY_TIME);
        String string = PortfolioApp.get.instance().getString(2131886080);
        wg6.a((Object) string, "PortfolioApp.instance.getString(R.string.AM)");
        if (xj6.a(str, string, false, 2, (Object) null)) {
            StringBuilder sb = new StringBuilder();
            String substring = str.substring(0, str.length() - 2);
            wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
            sb.append(substring);
            sb.append(jm4.a((Context) PortfolioApp.get.instance(), 2131886530));
            return sb.toString();
        }
        StringBuilder sb2 = new StringBuilder();
        String substring2 = str.substring(0, str.length() - 2);
        wg6.a((Object) substring2, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb2.append(substring2);
        sb2.append(jm4.a((Context) PortfolioApp.get.instance(), 2131886533));
        return sb2.toString();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v7, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v10, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r1v13, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final ArrayList<String> a() {
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.clear();
        arrayList.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886454));
        arrayList.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886457));
        arrayList.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886456));
        arrayList.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886458));
        arrayList.add(jm4.a((Context) PortfolioApp.get.instance(), 2131886455));
        return arrayList;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v11, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v14, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v17, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v20, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final String a(int i) {
        switch (i) {
            case 1:
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886130);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__S)");
                return a2;
            case 2:
                String a3 = jm4.a((Context) PortfolioApp.get.instance(), 2131886129);
                wg6.a((Object) a3, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__M)");
                return a3;
            case 3:
                String a4 = jm4.a((Context) PortfolioApp.get.instance(), 2131886132);
                wg6.a((Object) a4, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__T)");
                return a4;
            case 4:
                String a5 = jm4.a((Context) PortfolioApp.get.instance(), 2131886134);
                wg6.a((Object) a5, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__W)");
                return a5;
            case 5:
                String a6 = jm4.a((Context) PortfolioApp.get.instance(), 2131886133);
                wg6.a((Object) a6, "LanguageHelper.getString\u2026RepeatEnabled_Label__T_1)");
                return a6;
            case 6:
                String a7 = jm4.a((Context) PortfolioApp.get.instance(), 2131886128);
                wg6.a((Object) a7, "LanguageHelper.getString\u2026rmRepeatEnabled_Label__F)");
                return a7;
            case 7:
                String a8 = jm4.a((Context) PortfolioApp.get.instance(), 2131886131);
                wg6.a((Object) a8, "LanguageHelper.getString\u2026RepeatEnabled_Label__S_1)");
                return a8;
            default:
                return "";
        }
    }
}
