package com.fossil;

import com.fossil.ql2;
import com.fossil.rl2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ql2<MessageType extends ql2<MessageType, BuilderType>, BuilderType extends rl2<MessageType, BuilderType>> implements ro2 {
    @DexIgnore
    public int zza; // = 0;

    @DexIgnore
    public void a(int i) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public final yl2 d() {
        try {
            gm2 zzc = yl2.zzc(e());
            a(zzc.b());
            return zzc.a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public final byte[] f() {
        try {
            byte[] bArr = new byte[e()];
            pm2 a = pm2.a(bArr);
            a(a);
            a.b();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    @DexIgnore
    public int g() {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <T> void a(Iterable<T> iterable, List<? super T> list) {
        hn2.a(iterable);
        if (iterable instanceof xn2) {
            List<?> zzb = ((xn2) iterable).zzb();
            xn2 xn2 = (xn2) list;
            int size = list.size();
            for (Object next : zzb) {
                if (next == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(xn2.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = xn2.size() - 1; size2 >= size; size2--) {
                        xn2.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (next instanceof yl2) {
                    xn2.a((yl2) next);
                } else {
                    xn2.add((String) next);
                }
            }
        } else if (iterable instanceof cp2) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (T next2 : iterable) {
                if (next2 == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(next2);
            }
        }
    }
}
