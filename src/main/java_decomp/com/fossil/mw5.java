package com.fossil;

import android.os.Build;
import android.util.Base64;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.ServerSetting;
import java.nio.charset.Charset;
import java.security.KeyPair;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw5 extends m24<b, d, c> {
    @DexIgnore
    public /* final */ an4 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ String b;

        @DexIgnore
        public b(String str, String str2) {
            wg6.b(str, "aliasName");
            wg6.b(str2, ServerSetting.VALUE);
            this.a = str;
            this.b = str2;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final String b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
        @DexIgnore
        public c(int i, String str) {
            wg6.b(str, "errorMessage");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public mw5(an4 an4) {
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = an4;
    }

    @DexIgnore
    public String c() {
        return "EncryptValueKeyStoreUseCase";
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v0, types: [com.portfolio.platform.CoroutineUseCase, com.fossil.mw5] */
    public Object a(b bVar, xe6<Object> xe6) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("Start encrypt ");
        sb.append(bVar != null ? bVar.b() : null);
        sb.append(" with alias ");
        sb.append(bVar != null ? bVar.a() : null);
        local.d("EncryptValueKeyStoreUseCase", sb.toString());
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                qx5 qx5 = qx5.b;
                if (bVar != null) {
                    SecretKey d2 = qx5.d(bVar.a());
                    Cipher instance = Cipher.getInstance("AES/GCM/NoPadding");
                    instance.init(1, d2);
                    String b2 = bVar.b();
                    Charset charset = ej6.a;
                    if (b2 != null) {
                        byte[] bytes = b2.getBytes(charset);
                        wg6.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
                        byte[] doFinal = instance.doFinal(bytes);
                        wg6.a((Object) instance, "cipher");
                        String encodeToString = Base64.encodeToString(instance.getIV(), 0);
                        String encodeToString2 = Base64.encodeToString(doFinal, 0);
                        this.d.b(bVar.a(), encodeToString);
                        this.d.c(bVar.a(), encodeToString2);
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        local2.d("EncryptValueKeyStoreUseCase", "Encryption done iv " + encodeToString + " value " + encodeToString2);
                    } else {
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                qx5 qx52 = qx5.b;
                if (bVar != null) {
                    KeyPair c2 = qx52.c(bVar.a());
                    if (c2 == null) {
                        c2 = qx5.b.a(bVar.a());
                    }
                    Cipher instance2 = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                    instance2.init(1, c2.getPublic());
                    String b3 = bVar.b();
                    Charset charset2 = ej6.a;
                    if (b3 != null) {
                        byte[] bytes2 = b3.getBytes(charset2);
                        wg6.a((Object) bytes2, "(this as java.lang.String).getBytes(charset)");
                        String encodeToString3 = Base64.encodeToString(instance2.doFinal(bytes2), 0);
                        this.d.c(bVar.a(), encodeToString3);
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        local3.d("EncryptValueKeyStoreUseCase", "Encryption done value " + encodeToString3);
                    } else {
                        throw new rc6("null cannot be cast to non-null type java.lang.String");
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            }
            a(new d());
        } catch (Exception e) {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            local4.e("EncryptValueKeyStoreUseCase", "Exception when encrypt values " + e);
            a(new c(600, ""));
        }
        return new Object();
    }
}
