package com.fossil;

import android.view.View;
import android.widget.ProgressBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.viewpager2.widget.ViewPager2;
import com.google.android.material.tabs.TabLayout;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView q;
    @DexIgnore
    public /* final */ TabLayout r;
    @DexIgnore
    public /* final */ ProgressBar s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ ViewPager2 u;

    @DexIgnore
    public hb4(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, TabLayout tabLayout, ProgressBar progressBar, ConstraintLayout constraintLayout, ViewPager2 viewPager2) {
        super(obj, view, i);
        this.q = flexibleTextView3;
        this.r = tabLayout;
        this.s = progressBar;
        this.t = constraintLayout;
        this.u = viewPager2;
    }
}
