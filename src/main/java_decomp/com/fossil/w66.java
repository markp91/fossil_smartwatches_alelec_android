package com.fossil;

import com.zendesk.service.ErrorResponse;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w66 implements ErrorResponse {
    @DexIgnore
    public Throwable a;
    @DexIgnore
    public rx6 b;

    @DexIgnore
    public w66(Throwable th) {
        this.a = th;
    }

    @DexIgnore
    public static w66 a(Throwable th) {
        return new w66(th);
    }

    @DexIgnore
    public boolean b() {
        Throwable th = this.a;
        return th != null && (th instanceof IOException);
    }

    @DexIgnore
    public int o() {
        rx6 rx6 = this.b;
        if (rx6 != null) {
            return rx6.b();
        }
        return -1;
    }

    @DexIgnore
    public static w66 a(rx6 rx6) {
        return new w66(rx6);
    }

    @DexIgnore
    public w66(rx6 rx6) {
        this.b = rx6;
    }

    @DexIgnore
    public String a() {
        Throwable th = this.a;
        if (th != null) {
            return th.getMessage();
        }
        StringBuilder sb = new StringBuilder();
        rx6 rx6 = this.b;
        if (rx6 != null) {
            if (a76.b(rx6.e())) {
                sb.append(this.b.e());
            } else {
                sb.append(this.b.b());
            }
        }
        return sb.toString();
    }
}
