package com.fossil;

import com.portfolio.platform.data.source.AlarmsRepository;
import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.data.source.MicroAppRepository;
import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ct5 implements Factory<bt5> {
    @DexIgnore
    public /* final */ Provider<HybridPresetRepository> a;
    @DexIgnore
    public /* final */ Provider<MicroAppRepository> b;
    @DexIgnore
    public /* final */ Provider<DeviceRepository> c;
    @DexIgnore
    public /* final */ Provider<NotificationsRepository> d;
    @DexIgnore
    public /* final */ Provider<CategoryRepository> e;
    @DexIgnore
    public /* final */ Provider<AlarmsRepository> f;

    @DexIgnore
    public ct5(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
        this.e = provider5;
        this.f = provider6;
    }

    @DexIgnore
    public static ct5 a(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        return new ct5(provider, provider2, provider3, provider4, provider5, provider6);
    }

    @DexIgnore
    public static bt5 b(Provider<HybridPresetRepository> provider, Provider<MicroAppRepository> provider2, Provider<DeviceRepository> provider3, Provider<NotificationsRepository> provider4, Provider<CategoryRepository> provider5, Provider<AlarmsRepository> provider6) {
        return new GetHybridDeviceSettingUseCase(provider.get(), provider2.get(), provider3.get(), provider4.get(), provider5.get(), provider6.get());
    }

    @DexIgnore
    public GetHybridDeviceSettingUseCase get() {
        return b(this.a, this.b, this.c, this.d, this.e, this.f);
    }
}
