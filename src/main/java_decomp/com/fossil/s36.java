package com.fossil;

import android.content.Context;
import java.util.Map;
import java.util.Properties;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s36 extends v36 {
    @DexIgnore
    public t36 m; // = new t36();
    @DexIgnore
    public long n; // = -1;

    @DexIgnore
    public s36(Context context, int i, String str, r36 r36) {
        super(context, i, r36);
        this.m.a = str;
    }

    @DexIgnore
    public w36 a() {
        return w36.CUSTOM;
    }

    @DexIgnore
    public boolean a(JSONObject jSONObject) {
        String str;
        jSONObject.put("ei", this.m.a);
        long j = this.n;
        if (j > 0) {
            jSONObject.put("du", j);
        }
        Object obj = this.m.b;
        if (obj == null) {
            h();
            obj = this.m.c;
            str = "kv";
        } else {
            str = "ar";
        }
        jSONObject.put(str, obj);
        return true;
    }

    @DexIgnore
    public t36 g() {
        return this.m;
    }

    @DexIgnore
    public final void h() {
        Properties d;
        String str = this.m.a;
        if (str != null && (d = q36.d(str)) != null && d.size() > 0) {
            JSONObject jSONObject = this.m.c;
            if (jSONObject == null || jSONObject.length() == 0) {
                this.m.c = new JSONObject(d);
                return;
            }
            for (Map.Entry entry : d.entrySet()) {
                try {
                    this.m.c.put(entry.getKey().toString(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
