package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ad3<TResult> implements kd3<TResult> {
    @DexIgnore
    public /* final */ Executor a;
    @DexIgnore
    public /* final */ Object b; // = new Object();
    @DexIgnore
    public jc3 c;

    @DexIgnore
    public ad3(Executor executor, jc3 jc3) {
        this.a = executor;
        this.c = jc3;
    }

    @DexIgnore
    public final void onComplete(qc3 qc3) {
        if (qc3.c()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new bd3(this));
                }
            }
        }
    }
}
