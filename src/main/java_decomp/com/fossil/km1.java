package com.fossil;

import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class km1 extends p40 {
    @DexIgnore
    public /* final */ eh1 a;
    @DexIgnore
    public /* final */ sk1 b;
    @DexIgnore
    public /* final */ bn0 c;

    @DexIgnore
    public /* synthetic */ km1(eh1 eh1, sk1 sk1, bn0 bn0, int i) {
        eh1 = (i & 1) != 0 ? eh1.UNKNOWN : eh1;
        bn0 = (i & 4) != 0 ? new bn0((lx0) null, (String) null, il0.SUCCESS, (ch0) null, (sj0) null, 27) : bn0;
        this.a = eh1;
        this.b = sk1;
        this.c = bn0;
    }

    @DexIgnore
    public static /* synthetic */ km1 a(km1 km1, eh1 eh1, sk1 sk1, bn0 bn0, int i) {
        if ((i & 1) != 0) {
            eh1 = km1.a;
        }
        if ((i & 2) != 0) {
            sk1 = km1.b;
        }
        if ((i & 4) != 0) {
            bn0 = km1.c;
        }
        return km1.a(eh1, sk1, bn0);
    }

    @DexIgnore
    public final km1 a(eh1 eh1, sk1 sk1, bn0 bn0) {
        return new km1(eh1, sk1, bn0);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            cw0.a(cw0.a(jSONObject, bm0.PHASE_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.RESULT_CODE, (Object) cw0.a((Enum<?>) this.b));
            if (this.c.c != il0.SUCCESS) {
                cw0.a(jSONObject, bm0.REQUEST_RESULT, (Object) this.c.a());
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof km1)) {
            return false;
        }
        km1 km1 = (km1) obj;
        return wg6.a(this.a, km1.a) && wg6.a(this.b, km1.b) && wg6.a(this.c, km1.c);
    }

    @DexIgnore
    public int hashCode() {
        eh1 eh1 = this.a;
        int i = 0;
        int hashCode = (eh1 != null ? eh1.hashCode() : 0) * 31;
        sk1 sk1 = this.b;
        int hashCode2 = (hashCode + (sk1 != null ? sk1.hashCode() : 0)) * 31;
        bn0 bn0 = this.c;
        if (bn0 != null) {
            i = bn0.hashCode();
        }
        return hashCode2 + i;
    }

    @DexIgnore
    public String toString() {
        StringBuilder b2 = ze0.b("Result(phaseId=");
        b2.append(this.a);
        b2.append(", resultCode=");
        b2.append(this.b);
        b2.append(", requestResult=");
        b2.append(this.c);
        b2.append(")");
        return b2.toString();
    }

    @DexIgnore
    public km1(eh1 eh1, sk1 sk1, bn0 bn0) {
        this.a = eh1;
        this.b = sk1;
        this.c = bn0;
    }
}
