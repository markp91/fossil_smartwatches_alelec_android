package com.fossil;

import com.fossil.dn3;
import com.fossil.en3;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.AbstractCollection;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cn3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a<K, V> extends rk3<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient nk3<? extends List<V>> factory;

        @DexIgnore
        public a(Map<K, Collection<V>> map, nk3<? extends List<V>> nk3) {
            super(map);
            jk3.a(nk3);
            this.factory = nk3;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (nk3) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        public List<V> createCollection() {
            return (List) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b<K, V> extends xk3<K, V> {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public transient nk3<? extends Set<V>> factory;

        @DexIgnore
        public b(Map<K, Collection<V>> map, nk3<? extends Set<V>> nk3) {
            super(map);
            jk3.a(nk3);
            this.factory = nk3;
        }

        @DexIgnore
        private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
            objectInputStream.defaultReadObject();
            this.factory = (nk3) objectInputStream.readObject();
            setMap((Map) objectInputStream.readObject());
        }

        @DexIgnore
        private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
            objectOutputStream.defaultWriteObject();
            objectOutputStream.writeObject(this.factory);
            objectOutputStream.writeObject(backingMap());
        }

        @DexIgnore
        public Set<V> createCollection() {
            return (Set) this.factory.get();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c<K, V> extends AbstractCollection<Map.Entry<K, V>> {
        @DexIgnore
        public abstract zm3<K, V> a();

        @DexIgnore
        public void clear() {
            a().clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean remove(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return a().remove(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public int size() {
            return a().size();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d<K, V> extends wk3<K> {
        @DexIgnore
        public /* final */ zm3<K, V> c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends ho3<Map.Entry<K, Collection<V>>, dn3.a<K>> {

            @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.cn3$d$a$a")
            /* renamed from: com.fossil.cn3$d$a$a  reason: collision with other inner class name */
            public class C0011a extends en3.b<K> {
                @DexIgnore
                public /* final */ /* synthetic */ Map.Entry a;

                @DexIgnore
                public C0011a(a aVar, Map.Entry entry) {
                    this.a = entry;
                }

                @DexIgnore
                public int getCount() {
                    return ((Collection) this.a.getValue()).size();
                }

                @DexIgnore
                public K getElement() {
                    return this.a.getKey();
                }
            }

            @DexIgnore
            public a(d dVar, Iterator it) {
                super(it);
            }

            @DexIgnore
            public dn3.a<K> a(Map.Entry<K, Collection<V>> entry) {
                return new C0011a(this, entry);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b extends en3.d<K> {
            @DexIgnore
            public b() {
            }

            @DexIgnore
            public dn3<K> a() {
                return d.this;
            }

            @DexIgnore
            public boolean contains(Object obj) {
                if (!(obj instanceof dn3.a)) {
                    return false;
                }
                dn3.a aVar = (dn3.a) obj;
                Collection collection = d.this.c.asMap().get(aVar.getElement());
                if (collection == null || collection.size() != aVar.getCount()) {
                    return false;
                }
                return true;
            }

            @DexIgnore
            public boolean isEmpty() {
                return d.this.c.isEmpty();
            }

            @DexIgnore
            public Iterator<dn3.a<K>> iterator() {
                return d.this.entryIterator();
            }

            @DexIgnore
            public boolean remove(Object obj) {
                if (!(obj instanceof dn3.a)) {
                    return false;
                }
                dn3.a aVar = (dn3.a) obj;
                Collection collection = d.this.c.asMap().get(aVar.getElement());
                if (collection == null || collection.size() != aVar.getCount()) {
                    return false;
                }
                collection.clear();
                return true;
            }

            @DexIgnore
            public int size() {
                return d.this.distinctElements();
            }
        }

        @DexIgnore
        public d(zm3<K, V> zm3) {
            this.c = zm3;
        }

        @DexIgnore
        public void clear() {
            this.c.clear();
        }

        @DexIgnore
        public boolean contains(Object obj) {
            return this.c.containsKey(obj);
        }

        @DexIgnore
        public int count(Object obj) {
            Collection collection = (Collection) ym3.e(this.c.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            return collection.size();
        }

        @DexIgnore
        public Set<dn3.a<K>> createEntrySet() {
            return new b();
        }

        @DexIgnore
        public int distinctElements() {
            return this.c.asMap().size();
        }

        @DexIgnore
        public Set<K> elementSet() {
            return this.c.keySet();
        }

        @DexIgnore
        public Iterator<dn3.a<K>> entryIterator() {
            return new a(this, this.c.asMap().entrySet().iterator());
        }

        @DexIgnore
        public Iterator<K> iterator() {
            return ym3.a(this.c.entries().iterator());
        }

        @DexIgnore
        public int remove(Object obj, int i) {
            bl3.a(i, "occurrences");
            if (i == 0) {
                return count(obj);
            }
            Collection collection = (Collection) ym3.e(this.c.asMap(), obj);
            if (collection == null) {
                return 0;
            }
            int size = collection.size();
            if (i >= size) {
                collection.clear();
            } else {
                Iterator it = collection.iterator();
                for (int i2 = 0; i2 < i; i2++) {
                    it.next();
                    it.remove();
                }
            }
            return size;
        }
    }

    @DexIgnore
    public static <K, V> tm3<K, V> a(Map<K, Collection<V>> map, nk3<? extends List<V>> nk3) {
        return new a(map, nk3);
    }

    @DexIgnore
    public static <K, V> xn3<K, V> b(Map<K, Collection<V>> map, nk3<? extends Set<V>> nk3) {
        return new b(map, nk3);
    }

    @DexIgnore
    public static boolean a(zm3<?, ?> zm3, Object obj) {
        if (obj == zm3) {
            return true;
        }
        if (obj instanceof zm3) {
            return zm3.asMap().equals(((zm3) obj).asMap());
        }
        return false;
    }
}
