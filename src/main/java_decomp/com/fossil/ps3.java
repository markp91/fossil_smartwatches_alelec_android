package com.fossil;

import android.os.IBinder;
import android.os.Message;
import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ps3 implements ms3 {
    @DexIgnore
    public /* final */ IBinder a;

    @DexIgnore
    public ps3(IBinder iBinder) {
        this.a = iBinder;
    }

    @DexIgnore
    public final void a(Message message) throws RemoteException {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken("com.google.android.gms.iid.IMessengerCompat");
        obtain.writeInt(1);
        message.writeToParcel(obtain, 0);
        try {
            this.a.transact(1, obtain, (Parcel) null, 1);
        } finally {
            obtain.recycle();
        }
    }

    @DexIgnore
    public final IBinder asBinder() {
        return this.a;
    }
}
