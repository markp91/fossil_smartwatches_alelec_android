package com.fossil;

import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rk3<K, V> extends sk3<K, V> implements tm3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 6588350623831699109L;

    @DexIgnore
    public rk3(Map<K, Collection<V>> map) {
        super(map);
    }

    @DexIgnore
    public Map<K, Collection<V>> asMap() {
        return super.asMap();
    }

    @DexIgnore
    public abstract List<V> createCollection();

    @DexIgnore
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @DexIgnore
    public boolean put(K k, V v) {
        return super.put(k, v);
    }

    @DexIgnore
    public List<V> createUnmodifiableEmptyCollection() {
        return zl3.of();
    }

    @DexIgnore
    public List<V> get(K k) {
        return (List) super.get(k);
    }

    @DexIgnore
    public List<V> removeAll(Object obj) {
        return (List) super.removeAll(obj);
    }

    @DexIgnore
    public List<V> replaceValues(K k, Iterable<? extends V> iterable) {
        return (List) super.replaceValues(k, iterable);
    }
}
