package com.fossil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qd6 extends pd6 {
    @DexIgnore
    public static final <T> List<T> a() {
        return ae6.INSTANCE;
    }

    @DexIgnore
    public static final <T> Collection<T> b(T[] tArr) {
        wg6.b(tArr, "$this$asCollection");
        return new jd6(tArr, false);
    }

    @DexIgnore
    public static final <T> List<T> c(T... tArr) {
        wg6.b(tArr, "elements");
        return tArr.length > 0 ? md6.b(tArr) : a();
    }

    @DexIgnore
    public static final <T> List<T> d(T... tArr) {
        wg6.b(tArr, "elements");
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new jd6(tArr, true));
    }

    @DexIgnore
    public static final <T> ArrayList<T> a(T... tArr) {
        wg6.b(tArr, "elements");
        return tArr.length == 0 ? new ArrayList<>() : new ArrayList<>(new jd6(tArr, true));
    }

    @DexIgnore
    public static final <T> List<T> b(List<? extends T> list) {
        wg6.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return list;
        }
        return pd6.a(list.get(0));
    }

    @DexIgnore
    public static final void c() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    @DexIgnore
    public static final wh6 a(Collection<?> collection) {
        wg6.b(collection, "$this$indices");
        return new wh6(0, collection.size() - 1);
    }

    @DexIgnore
    public static final <T> int a(List<? extends T> list) {
        wg6.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    @DexIgnore
    public static final void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }
}
