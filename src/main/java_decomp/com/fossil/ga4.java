package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ga4 extends fa4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(2131363342, 1);
        w.put(2131362004, 2);
        w.put(2131363070, 3);
        w.put(2131363068, 4);
        w.put(2131363069, 5);
        w.put(2131363062, 6);
    }
    */

    @DexIgnore
    public ga4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public ga4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[6], objArr[4], objArr[5], objArr[3], objArr[1]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
