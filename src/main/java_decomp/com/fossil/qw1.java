package com.fossil;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.facebook.appevents.FacebookTimeSpentData;
import com.facebook.internal.FileLruCache;
import com.facebook.login.LoginStatusClient;
import com.fossil.c12;
import com.fossil.mw1;
import com.fossil.rv1;
import com.fossil.u12;
import com.fossil.uw1;
import com.fossil.wv1;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class qw1 implements Handler.Callback {
    @DexIgnore
    public static /* final */ Status n; // = new Status(4, "Sign-out occurred while this API call was in progress.");
    @DexIgnore
    public static /* final */ Status o; // = new Status(4, "The user must be signed in to make this API call.");
    @DexIgnore
    public static /* final */ Object p; // = new Object();
    @DexIgnore
    public static qw1 q;
    @DexIgnore
    public long a; // = LoginStatusClient.DEFAULT_TOAST_DURATION_MS;
    @DexIgnore
    public long b; // = 120000;
    @DexIgnore
    public long c; // = 10000;
    @DexIgnore
    public /* final */ Context d;
    @DexIgnore
    public /* final */ jv1 e;
    @DexIgnore
    public /* final */ m12 f;
    @DexIgnore
    public /* final */ AtomicInteger g; // = new AtomicInteger(1);
    @DexIgnore
    public /* final */ AtomicInteger h; // = new AtomicInteger(0);
    @DexIgnore
    public /* final */ Map<lw1<?>, a<?>> i; // = new ConcurrentHashMap(5, 0.75f, 1);
    @DexIgnore
    public hx1 j; // = null;
    @DexIgnore
    public /* final */ Set<lw1<?>> k; // = new q4();
    @DexIgnore
    public /* final */ Set<lw1<?>> l; // = new q4();
    @DexIgnore
    public /* final */ Handler m;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ez1, c12.c {
        @DexIgnore
        public /* final */ rv1.f a;
        @DexIgnore
        public /* final */ lw1<?> b;
        @DexIgnore
        public n12 c; // = null;
        @DexIgnore
        public Set<Scope> d; // = null;
        @DexIgnore
        public boolean e; // = false;

        @DexIgnore
        public b(rv1.f fVar, lw1<?> lw1) {
            this.a = fVar;
            this.b = lw1;
        }

        @DexIgnore
        public final void a(gv1 gv1) {
            qw1.this.m.post(new ty1(this, gv1));
        }

        @DexIgnore
        public final void b(gv1 gv1) {
            ((a) qw1.this.i.get(this.b)).b(gv1);
        }

        @DexIgnore
        public final void a(n12 n12, Set<Scope> set) {
            if (n12 == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                b(new gv1(4));
                return;
            }
            this.c = n12;
            this.d = set;
            a();
        }

        @DexIgnore
        public final void a() {
            n12 n12;
            if (this.e && (n12 = this.c) != null) {
                this.a.a(n12, this.d);
            }
        }
    }

    @DexIgnore
    public qw1(Context context, Looper looper, jv1 jv1) {
        this.d = context;
        this.m = new bb2(looper, this);
        this.e = jv1;
        this.f = new m12(jv1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(6));
    }

    @DexIgnore
    public static qw1 a(Context context) {
        qw1 qw1;
        synchronized (p) {
            if (q == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                q = new qw1(context.getApplicationContext(), handlerThread.getLooper(), jv1.a());
            }
            qw1 = q;
        }
        return qw1;
    }

    @DexIgnore
    public static void d() {
        synchronized (p) {
            if (q != null) {
                qw1 qw1 = q;
                qw1.h.incrementAndGet();
                qw1.m.sendMessageAtFrontOfQueue(qw1.m.obtainMessage(10));
            }
        }
    }

    @DexIgnore
    public static qw1 e() {
        qw1 qw1;
        synchronized (p) {
            w12.a(q, (Object) "Must guarantee manager is non-null before using getInstance");
            qw1 = q;
        }
        return qw1;
    }

    @DexIgnore
    public final int b() {
        return this.g.getAndIncrement();
    }

    @DexIgnore
    public final void c() {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(3));
    }

    @DexIgnore
    public boolean handleMessage(Message message) {
        a aVar;
        int i2 = message.what;
        long j2 = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
        switch (i2) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j2 = 10000;
                }
                this.c = j2;
                this.m.removeMessages(12);
                for (lw1<?> obtainMessage : this.i.keySet()) {
                    Handler handler = this.m;
                    handler.sendMessageDelayed(handler.obtainMessage(12, obtainMessage), this.c);
                }
                break;
            case 2:
                uz1 uz1 = (uz1) message.obj;
                Iterator<lw1<?>> it = uz1.b().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        lw1 next = it.next();
                        a aVar2 = this.i.get(next);
                        if (aVar2 == null) {
                            uz1.a(next, new gv1(13), (String) null);
                            break;
                        } else if (aVar2.c()) {
                            uz1.a(next, gv1.e, aVar2.f().g());
                        } else if (aVar2.n() != null) {
                            uz1.a(next, aVar2.n(), (String) null);
                        } else {
                            aVar2.a(uz1);
                            aVar2.a();
                        }
                    }
                }
            case 3:
                for (a next2 : this.i.values()) {
                    next2.m();
                    next2.a();
                }
                break;
            case 4:
            case 8:
            case 13:
                yy1 yy1 = (yy1) message.obj;
                a aVar3 = this.i.get(yy1.c.a());
                if (aVar3 == null) {
                    b(yy1.c);
                    aVar3 = this.i.get(yy1.c.a());
                }
                if (aVar3.d() && this.h.get() != yy1.b) {
                    yy1.a.a(n);
                    aVar3.k();
                    break;
                } else {
                    aVar3.a(yy1.a);
                    break;
                }
                break;
            case 5:
                int i3 = message.arg1;
                gv1 gv1 = (gv1) message.obj;
                Iterator<a<?>> it2 = this.i.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = it2.next();
                        if (aVar.b() == i3) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i3);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b2 = this.e.b(gv1.p());
                    String B = gv1.B();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b2).length() + 69 + String.valueOf(B).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b2);
                    sb2.append(": ");
                    sb2.append(B);
                    aVar.a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (s42.a() && (this.d.getApplicationContext() instanceof Application)) {
                    mw1.a((Application) this.d.getApplicationContext());
                    mw1.b().a((mw1.a) new my1(this));
                    if (!mw1.b().b(true)) {
                        this.c = FacebookTimeSpentData.APP_ACTIVATE_SUPPRESSION_PERIOD_IN_MILLISECONDS;
                        break;
                    }
                }
                break;
            case 7:
                b((vv1<?>) (vv1) message.obj);
                break;
            case 9:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).e();
                    break;
                }
                break;
            case 10:
                for (lw1<?> remove : this.l) {
                    this.i.remove(remove).k();
                }
                this.l.clear();
                break;
            case 11:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).g();
                    break;
                }
                break;
            case 12:
                if (this.i.containsKey(message.obj)) {
                    this.i.get(message.obj).q();
                    break;
                }
                break;
            case 14:
                ix1 ix1 = (ix1) message.obj;
                lw1<?> a2 = ix1.a();
                if (this.i.containsKey(a2)) {
                    ix1.b().a(Boolean.valueOf(this.i.get(a2).a(false)));
                    break;
                } else {
                    ix1.b().a(false);
                    break;
                }
            case 15:
                c cVar = (c) message.obj;
                if (this.i.containsKey(cVar.a)) {
                    this.i.get(cVar.a).a(cVar);
                    break;
                }
                break;
            case 16:
                c cVar2 = (c) message.obj;
                if (this.i.containsKey(cVar2.a)) {
                    this.i.get(cVar2.a).b(cVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i2);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @DexIgnore
    public final void b(vv1<?> vv1) {
        lw1<?> a2 = vv1.a();
        a aVar = this.i.get(a2);
        if (aVar == null) {
            aVar = new a(vv1);
            this.i.put(a2, aVar);
        }
        if (aVar.d()) {
            this.l.add(a2);
        }
        aVar.a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a<O extends rv1.d> implements wv1.b, wv1.c, c02 {
        @DexIgnore
        public /* final */ Queue<az1> a; // = new LinkedList();
        @DexIgnore
        public /* final */ rv1.f b;
        @DexIgnore
        public /* final */ rv1.b c;
        @DexIgnore
        public /* final */ lw1<O> d;
        @DexIgnore
        public /* final */ k02 e;
        @DexIgnore
        public /* final */ Set<uz1> f; // = new HashSet();
        @DexIgnore
        public /* final */ Map<uw1.a<?>, zy1> g; // = new HashMap();
        @DexIgnore
        public /* final */ int h;
        @DexIgnore
        public /* final */ dz1 i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public /* final */ List<c> o; // = new ArrayList();
        @DexIgnore
        public gv1 p; // = null;

        @DexIgnore
        public a(vv1<O> vv1) {
            this.b = vv1.a(qw1.this.m.getLooper(), (a<O>) this);
            rv1.f fVar = this.b;
            if (fVar instanceof b22) {
                this.c = ((b22) fVar).H();
            } else {
                this.c = fVar;
            }
            this.d = vv1.a();
            this.e = new k02();
            this.h = vv1.g();
            if (this.b.m()) {
                this.i = vv1.a(qw1.this.d, qw1.this.m);
            } else {
                this.i = null;
            }
        }

        @DexIgnore
        public final void a(gv1 gv1, rv1<?> rv1, boolean z) {
            if (Looper.myLooper() == qw1.this.m.getLooper()) {
                a(gv1);
            } else {
                qw1.this.m.post(new oy1(this, gv1));
            }
        }

        @DexIgnore
        public final void b(gv1 gv1) {
            w12.a(qw1.this.m);
            this.b.a();
            a(gv1);
        }

        @DexIgnore
        public final boolean c(gv1 gv1) {
            synchronized (qw1.p) {
                if (qw1.this.j == null || !qw1.this.k.contains(this.d)) {
                    return false;
                }
                qw1.this.j.b(gv1, this.h);
                return true;
            }
        }

        @DexIgnore
        public final void d(gv1 gv1) {
            for (uz1 next : this.f) {
                String str = null;
                if (u12.a(gv1, gv1.e)) {
                    str = this.b.g();
                }
                next.a(this.d, gv1, str);
            }
            this.f.clear();
        }

        @DexIgnore
        public final void e() {
            w12.a(qw1.this.m);
            if (this.j) {
                a();
            }
        }

        @DexIgnore
        public final void f(Bundle bundle) {
            if (Looper.myLooper() == qw1.this.m.getLooper()) {
                h();
            } else {
                qw1.this.m.post(new ny1(this));
            }
        }

        @DexIgnore
        public final void g(int i2) {
            if (Looper.myLooper() == qw1.this.m.getLooper()) {
                i();
            } else {
                qw1.this.m.post(new py1(this));
            }
        }

        @DexIgnore
        public final void h() {
            m();
            d(gv1.e);
            o();
            Iterator<zy1> it = this.g.values().iterator();
            while (it.hasNext()) {
                zy1 next = it.next();
                if (a(next.a.c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.a.a(this.c, new rc3());
                    } catch (DeadObjectException unused) {
                        g(1);
                        this.b.a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            j();
            p();
        }

        @DexIgnore
        public final void i() {
            m();
            this.j = true;
            this.e.c();
            qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 9, this.d), qw1.this.a);
            qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 11, this.d), qw1.this.b);
            qw1.this.f.a();
        }

        @DexIgnore
        public final void j() {
            ArrayList arrayList = new ArrayList(this.a);
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                az1 az1 = (az1) obj;
                if (!this.b.c()) {
                    return;
                }
                if (b(az1)) {
                    this.a.remove(az1);
                }
            }
        }

        @DexIgnore
        public final void k() {
            w12.a(qw1.this.m);
            a(qw1.n);
            this.e.b();
            for (uw1.a sz1 : (uw1.a[]) this.g.keySet().toArray(new uw1.a[this.g.size()])) {
                a((az1) new sz1(sz1, new rc3()));
            }
            d(new gv1(4));
            if (this.b.c()) {
                this.b.a((c12.e) new ry1(this));
            }
        }

        @DexIgnore
        public final Map<uw1.a<?>, zy1> l() {
            return this.g;
        }

        @DexIgnore
        public final void m() {
            w12.a(qw1.this.m);
            this.p = null;
        }

        @DexIgnore
        public final gv1 n() {
            w12.a(qw1.this.m);
            return this.p;
        }

        @DexIgnore
        public final void o() {
            if (this.j) {
                qw1.this.m.removeMessages(11, this.d);
                qw1.this.m.removeMessages(9, this.d);
                this.j = false;
            }
        }

        @DexIgnore
        public final void p() {
            qw1.this.m.removeMessages(12, this.d);
            qw1.this.m.sendMessageDelayed(qw1.this.m.obtainMessage(12, this.d), qw1.this.c);
        }

        @DexIgnore
        public final boolean q() {
            return a(true);
        }

        @DexIgnore
        public final ac3 r() {
            dz1 dz1 = this.i;
            if (dz1 == null) {
                return null;
            }
            return dz1.q();
        }

        @DexIgnore
        public final void a(gv1 gv1) {
            w12.a(qw1.this.m);
            dz1 dz1 = this.i;
            if (dz1 != null) {
                dz1.r();
            }
            m();
            qw1.this.f.a();
            d(gv1);
            if (gv1.p() == 4) {
                a(qw1.o);
            } else if (this.a.isEmpty()) {
                this.p = gv1;
            } else if (!c(gv1) && !qw1.this.b(gv1, this.h)) {
                if (gv1.p() == 18) {
                    this.j = true;
                }
                if (this.j) {
                    qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 9, this.d), qw1.this.a);
                    return;
                }
                String a2 = this.d.a();
                String valueOf = String.valueOf(gv1);
                StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 63 + String.valueOf(valueOf).length());
                sb.append("API: ");
                sb.append(a2);
                sb.append(" is not available on this device. Connection failed with: ");
                sb.append(valueOf);
                a(new Status(17, sb.toString()));
            }
        }

        @DexIgnore
        public final boolean b(az1 az1) {
            if (!(az1 instanceof ey1)) {
                c(az1);
                return true;
            }
            ey1 ey1 = (ey1) az1;
            iv1 a2 = a(ey1.b(this));
            if (a2 == null) {
                c(az1);
                return true;
            } else if (ey1.c(this)) {
                c cVar = new c(this.d, a2, (my1) null);
                int indexOf = this.o.indexOf(cVar);
                if (indexOf >= 0) {
                    c cVar2 = this.o.get(indexOf);
                    qw1.this.m.removeMessages(15, cVar2);
                    qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 15, cVar2), qw1.this.a);
                    return false;
                }
                this.o.add(cVar);
                qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 15, cVar), qw1.this.a);
                qw1.this.m.sendMessageDelayed(Message.obtain(qw1.this.m, 16, cVar), qw1.this.b);
                gv1 gv1 = new gv1(2, (PendingIntent) null);
                if (c(gv1)) {
                    return false;
                }
                qw1.this.b(gv1, this.h);
                return false;
            } else {
                ey1.a((RuntimeException) new jw1(a2));
                return false;
            }
        }

        @DexIgnore
        public final rv1.f f() {
            return this.b;
        }

        @DexIgnore
        public final void g() {
            Status status;
            w12.a(qw1.this.m);
            if (this.j) {
                o();
                if (qw1.this.e.c(qw1.this.d) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                a(status);
                this.b.a();
            }
        }

        @DexIgnore
        public final boolean d() {
            return this.b.m();
        }

        @DexIgnore
        public final void c(az1 az1) {
            az1.a(this.e, d());
            try {
                az1.a((a<?>) this);
            } catch (DeadObjectException unused) {
                g(1);
                this.b.a();
            }
        }

        @DexIgnore
        public final boolean c() {
            return this.b.c();
        }

        @DexIgnore
        public final void a(az1 az1) {
            w12.a(qw1.this.m);
            if (!this.b.c()) {
                this.a.add(az1);
                gv1 gv1 = this.p;
                if (gv1 == null || !gv1.D()) {
                    a();
                } else {
                    a(this.p);
                }
            } else if (b(az1)) {
                p();
            } else {
                this.a.add(az1);
            }
        }

        @DexIgnore
        public final int b() {
            return this.h;
        }

        @DexIgnore
        public final void b(c cVar) {
            iv1[] b2;
            if (this.o.remove(cVar)) {
                qw1.this.m.removeMessages(15, cVar);
                qw1.this.m.removeMessages(16, cVar);
                iv1 b3 = cVar.b;
                ArrayList arrayList = new ArrayList(this.a.size());
                for (az1 az1 : this.a) {
                    if ((az1 instanceof ey1) && (b2 = ((ey1) az1).b(this)) != null && h42.a(b2, b3)) {
                        arrayList.add(az1);
                    }
                }
                int size = arrayList.size();
                int i2 = 0;
                while (i2 < size) {
                    Object obj = arrayList.get(i2);
                    i2++;
                    az1 az12 = (az1) obj;
                    this.a.remove(az12);
                    az12.a((RuntimeException) new jw1(b3));
                }
            }
        }

        @DexIgnore
        public final void a(Status status) {
            w12.a(qw1.this.m);
            for (az1 a2 : this.a) {
                a2.a(status);
            }
            this.a.clear();
        }

        @DexIgnore
        public final boolean a(boolean z) {
            w12.a(qw1.this.m);
            if (!this.b.c() || this.g.size() != 0) {
                return false;
            }
            if (this.e.a()) {
                if (z) {
                    p();
                }
                return false;
            }
            this.b.a();
            return true;
        }

        @DexIgnore
        public final void a() {
            w12.a(qw1.this.m);
            if (!this.b.c() && !this.b.f()) {
                int a2 = qw1.this.f.a(qw1.this.d, this.b);
                if (a2 != 0) {
                    a(new gv1(a2, (PendingIntent) null));
                    return;
                }
                b bVar = new b(this.b, this.d);
                if (this.b.m()) {
                    this.i.a((ez1) bVar);
                }
                this.b.a((c12.c) bVar);
            }
        }

        @DexIgnore
        public final void a(uz1 uz1) {
            w12.a(qw1.this.m);
            this.f.add(uz1);
        }

        @DexIgnore
        public final iv1 a(iv1[] iv1Arr) {
            if (!(iv1Arr == null || iv1Arr.length == 0)) {
                iv1[] k = this.b.k();
                if (k == null) {
                    k = new iv1[0];
                }
                p4 p4Var = new p4(k.length);
                for (iv1 iv1 : k) {
                    p4Var.put(iv1.p(), Long.valueOf(iv1.B()));
                }
                for (iv1 iv12 : iv1Arr) {
                    if (!p4Var.containsKey(iv12.p()) || ((Long) p4Var.get(iv12.p())).longValue() < iv12.B()) {
                        return iv12;
                    }
                }
            }
            return null;
        }

        @DexIgnore
        public final void a(c cVar) {
            if (!this.o.contains(cVar) || this.j) {
                return;
            }
            if (!this.b.c()) {
                a();
            } else {
                j();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c {
        @DexIgnore
        public /* final */ lw1<?> a;
        @DexIgnore
        public /* final */ iv1 b;

        @DexIgnore
        public c(lw1<?> lw1, iv1 iv1) {
            this.a = lw1;
            this.b = iv1;
        }

        @DexIgnore
        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof c)) {
                c cVar = (c) obj;
                if (!u12.a(this.a, cVar.a) || !u12.a(this.b, cVar.b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        @DexIgnore
        public final int hashCode() {
            return u12.a(this.a, this.b);
        }

        @DexIgnore
        public final String toString() {
            u12.a a2 = u12.a((Object) this);
            a2.a(FileLruCache.HEADER_CACHEKEY_KEY, this.a);
            a2.a("feature", this.b);
            return a2.toString();
        }

        @DexIgnore
        public /* synthetic */ c(lw1 lw1, iv1 iv1, my1 my1) {
            this(lw1, iv1);
        }
    }

    @DexIgnore
    public final void b(hx1 hx1) {
        synchronized (p) {
            if (this.j == hx1) {
                this.j = null;
                this.k.clear();
            }
        }
    }

    @DexIgnore
    public final void a(vv1<?> vv1) {
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(7, vv1));
    }

    @DexIgnore
    public final void a(hx1 hx1) {
        synchronized (p) {
            if (this.j != hx1) {
                this.j = hx1;
                this.k.clear();
            }
            this.k.addAll(hx1.h());
        }
    }

    @DexIgnore
    public final boolean b(gv1 gv1, int i2) {
        return this.e.a(this.d, gv1, i2);
    }

    @DexIgnore
    public final qc3<Map<lw1<?>, String>> a(Iterable<? extends xv1<?>> iterable) {
        uz1 uz1 = new uz1(iterable);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(2, uz1));
        return uz1.a();
    }

    @DexIgnore
    public final void a() {
        this.h.incrementAndGet();
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(10));
    }

    @DexIgnore
    public final <O extends rv1.d> void a(vv1<O> vv1, int i2, nw1<? extends ew1, rv1.b> nw1) {
        oz1 oz1 = new oz1(i2, nw1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new yy1(oz1, this.h.get(), vv1)));
    }

    @DexIgnore
    public final <O extends rv1.d, ResultT> void a(vv1<O> vv1, int i2, bx1<rv1.b, ResultT> bx1, rc3<ResultT> rc3, zw1 zw1) {
        qz1 qz1 = new qz1(i2, bx1, rc3, zw1);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(4, new yy1(qz1, this.h.get(), vv1)));
    }

    @DexIgnore
    public final <O extends rv1.d> qc3<Void> a(vv1<O> vv1, xw1<rv1.b, ?> xw1, dx1<rv1.b, ?> dx1) {
        rc3 rc3 = new rc3();
        rz1 rz1 = new rz1(new zy1(xw1, dx1), rc3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(8, new yy1(rz1, this.h.get(), vv1)));
        return rc3.a();
    }

    @DexIgnore
    public final <O extends rv1.d> qc3<Boolean> a(vv1<O> vv1, uw1.a<?> aVar) {
        rc3 rc3 = new rc3();
        sz1 sz1 = new sz1(aVar, rc3);
        Handler handler = this.m;
        handler.sendMessage(handler.obtainMessage(13, new yy1(sz1, this.h.get(), vv1)));
        return rc3.a();
    }

    @DexIgnore
    public final PendingIntent a(lw1<?> lw1, int i2) {
        ac3 r;
        a aVar = this.i.get(lw1);
        if (aVar == null || (r = aVar.r()) == null) {
            return null;
        }
        return PendingIntent.getActivity(this.d, i2, r.l(), 134217728);
    }

    @DexIgnore
    public final void a(gv1 gv1, int i2) {
        if (!b(gv1, i2)) {
            Handler handler = this.m;
            handler.sendMessage(handler.obtainMessage(5, i2, 0, gv1));
        }
    }
}
