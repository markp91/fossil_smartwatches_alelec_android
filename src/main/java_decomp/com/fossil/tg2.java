package com.fossil;

import android.app.PendingIntent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tg2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<tg2> CREATOR; // = new ug2();
    @DexIgnore
    public int a;
    @DexIgnore
    public rg2 b;
    @DexIgnore
    public ex2 c;
    @DexIgnore
    public PendingIntent d;
    @DexIgnore
    public bx2 e;
    @DexIgnore
    public ag2 f;

    @DexIgnore
    public tg2(int i, rg2 rg2, IBinder iBinder, PendingIntent pendingIntent, IBinder iBinder2, IBinder iBinder3) {
        this.a = i;
        this.b = rg2;
        ag2 ag2 = null;
        this.c = iBinder == null ? null : fx2.a(iBinder);
        this.d = pendingIntent;
        this.e = iBinder2 == null ? null : cx2.a(iBinder2);
        if (!(iBinder3 == null || iBinder3 == null)) {
            IInterface queryLocalInterface = iBinder3.queryLocalInterface("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
            ag2 = queryLocalInterface instanceof ag2 ? (ag2) queryLocalInterface : new cg2(iBinder3);
        }
        this.f = ag2;
    }

    @DexIgnore
    public static tg2 a(bx2 bx2, ag2 ag2) {
        return new tg2(2, (rg2) null, (IBinder) null, (PendingIntent) null, bx2.asBinder(), ag2 != null ? ag2.asBinder() : null);
    }

    @DexIgnore
    public static tg2 a(ex2 ex2, ag2 ag2) {
        return new tg2(2, (rg2) null, ex2.asBinder(), (PendingIntent) null, (IBinder) null, ag2 != null ? ag2.asBinder() : null);
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, (Parcelable) this.b, i, false);
        ex2 ex2 = this.c;
        IBinder iBinder = null;
        g22.a(parcel, 3, ex2 == null ? null : ex2.asBinder(), false);
        g22.a(parcel, 4, (Parcelable) this.d, i, false);
        bx2 bx2 = this.e;
        g22.a(parcel, 5, bx2 == null ? null : bx2.asBinder(), false);
        ag2 ag2 = this.f;
        if (ag2 != null) {
            iBinder = ag2.asBinder();
        }
        g22.a(parcel, 6, iBinder, false);
        g22.a(parcel, a2);
    }
}
