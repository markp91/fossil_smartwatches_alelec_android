package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface xa {
    @DexIgnore
    ColorStateList getSupportImageTintList();

    @DexIgnore
    PorterDuff.Mode getSupportImageTintMode();

    @DexIgnore
    void setSupportImageTintList(ColorStateList colorStateList);

    @DexIgnore
    void setSupportImageTintMode(PorterDuff.Mode mode);
}
