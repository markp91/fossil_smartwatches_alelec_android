package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ll6 {
    DEFAULT,
    LAZY,
    ATOMIC,
    UNDISPATCHED;

    @DexIgnore
    public final <T> void invoke(hg6<? super xe6<? super T>, ? extends Object> hg6, xe6<? super T> xe6) {
        wg6.b(hg6, "block");
        wg6.b(xe6, "completion");
        int i = kl6.a[ordinal()];
        if (i == 1) {
            cp6.a(hg6, xe6);
        } else if (i == 2) {
            ze6.a(hg6, xe6);
        } else if (i == 3) {
            dp6.a(hg6, xe6);
        } else if (i != 4) {
            throw new kc6();
        }
    }

    @DexIgnore
    public final boolean isLazy() {
        return this == LAZY;
    }

    @DexIgnore
    public final <R, T> void invoke(ig6<? super R, ? super xe6<? super T>, ? extends Object> ig6, R r, xe6<? super T> xe6) {
        wg6.b(ig6, "block");
        wg6.b(xe6, "completion");
        int i = kl6.b[ordinal()];
        if (i == 1) {
            cp6.a(ig6, r, xe6);
        } else if (i == 2) {
            ze6.a(ig6, r, xe6);
        } else if (i == 3) {
            dp6.a(ig6, r, xe6);
        } else if (i != 4) {
            throw new kc6();
        }
    }
}
