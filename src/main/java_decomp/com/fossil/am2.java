package com.fossil;

import java.util.Comparator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am2 implements Comparator<yl2> {
    @DexIgnore
    public final /* synthetic */ int compare(Object obj, Object obj2) {
        yl2 yl2 = (yl2) obj;
        yl2 yl22 = (yl2) obj2;
        hm2 hm2 = (hm2) yl2.iterator();
        hm2 hm22 = (hm2) yl22.iterator();
        while (hm2.hasNext() && hm22.hasNext()) {
            int compare = Integer.compare(yl2.a(hm2.zza()), yl2.a(hm22.zza()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(yl2.zza(), yl22.zza());
    }
}
