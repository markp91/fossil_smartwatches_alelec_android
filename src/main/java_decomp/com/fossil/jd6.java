package com.fossil;

import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jd6<T> implements Collection<T>, ph6 {
    @DexIgnore
    public /* final */ T[] a;
    @DexIgnore
    public /* final */ boolean b;

    @DexIgnore
    public jd6(T[] tArr, boolean z) {
        wg6.b(tArr, "values");
        this.a = tArr;
        this.b = z;
    }

    @DexIgnore
    public int a() {
        return this.a.length;
    }

    @DexIgnore
    public boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return nd6.a(this.a, obj);
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isEmpty() {
        return this.a.length == 0;
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return kg6.a(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public final Object[] toArray() {
        return pd6.a(this.a, this.b);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }
}
