package com.fossil;

import java.lang.reflect.Field;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mf6 {
    @DexIgnore
    public static final lf6 a(gf6 gf6) {
        return (lf6) gf6.getClass().getAnnotation(lf6.class);
    }

    @DexIgnore
    public static final int b(gf6 gf6) {
        try {
            Field declaredField = gf6.getClass().getDeclaredField("label");
            wg6.a((Object) declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(gf6);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception unused) {
            return -1;
        }
    }

    @DexIgnore
    public static final StackTraceElement c(gf6 gf6) {
        int i;
        String str;
        wg6.b(gf6, "$this$getStackTraceElementImpl");
        lf6 a = a(gf6);
        if (a == null) {
            return null;
        }
        a(1, a.v());
        int b = b(gf6);
        if (b < 0) {
            i = -1;
        } else {
            i = a.l()[b];
        }
        String b2 = of6.c.b(gf6);
        if (b2 == null) {
            str = a.c();
        } else {
            str = b2 + '/' + a.c();
        }
        return new StackTraceElement(str, a.m(), a.f(), i);
    }

    @DexIgnore
    public static final void a(int i, int i2) {
        if (i2 > i) {
            throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
        }
    }
}
