package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum w31 {
    OTA((byte) 0),
    ACTIVITY_FILE((byte) 1),
    HARDWARE_LOG((byte) 2),
    FONT((byte) 3),
    MUSIC_CONTROL((byte) 4),
    UI_SCRIPT((byte) 5),
    MICRO_APP((byte) 6),
    ASSET((byte) 7),
    DEVICE_CONFIG((byte) 8),
    NOTIFICATION((byte) 9),
    ALARM((byte) 10),
    DEVICE_INFO((byte) 11),
    NOTIFICATION_FILTER((byte) 12),
    UI_PACKAGE_FILE((byte) 13),
    WATCH_PARAMETERS_FILE((byte) 14),
    LUTS_FILE((byte) 15),
    RATE_FILE((byte) 16),
    DATA_COLLECTION_FILE((byte) 17),
    REPLY_MESSAGES_FILE((byte) 19),
    ALL_FILE((byte) 255);
    
    @DexIgnore
    public static /* final */ a21 x; // = null;
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ byte b;

    /*
    static {
        x = new a21((qg6) null);
    }
    */

    @DexIgnore
    public w31(byte b2) {
        this.b = b2;
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.b, (byte) 255});
        wg6.a(wrap, "ByteBuffer.wrap(byteArrayOf(id, (0xFF).toByte()))");
        this.a = wrap.getShort();
    }

    @DexIgnore
    public final short a(byte b2) {
        ByteBuffer wrap = ByteBuffer.wrap(new byte[]{this.b, b2});
        wg6.a(wrap, "ByteBuffer.wrap(byteArrayOf(id, index))");
        return wrap.getShort();
    }
}
