package com.fossil;

import java.util.Iterator;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr0 {
    @DexIgnore
    public /* final */ LinkedList<ok0> a; // = new LinkedList<>();
    @DexIgnore
    public ok0 b;
    @DexIgnore
    public /* final */ ue1 c;

    @DexIgnore
    public hr0(ue1 ue1) {
        this.c = ue1;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0021, code lost:
        r3.a.add(r4);
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    public final void a(ok0 ok0) {
        ok0 poll;
        cc0 cc0 = cc0.DEBUG;
        new Object[1][0] = cw0.a((Enum<?>) ok0.h);
        synchronized (this.a) {
            this.a.add(c(ok0), ok0);
            cd6 cd6 = cd6.a;
        }
        if (this.b == null && (poll = this.a.poll()) != null) {
            b(poll);
        }
    }

    @DexIgnore
    public final void b(ok0 ok0) {
        this.b = ok0;
        ok0 ok02 = this.b;
        if (ok02 != null) {
            ok02.g = new xn0(this);
            ue1 ue1 = this.c;
            if (!ok02.c) {
                cc0 cc0 = cc0.DEBUG;
                hn1<p51> c2 = ok02.c();
                if (c2 != null) {
                    c2.a((ef0<p51>) ok02);
                }
                ok02.a(ue1);
            }
        }
    }

    @DexIgnore
    public final int c(ok0 ok0) {
        synchronized (this.a) {
            int size = this.a.size();
            for (int i = 0; i < size; i++) {
                if (ok0.b().compareTo(this.a.get(i).b()) > 0) {
                    return i;
                }
            }
            int size2 = this.a.size();
            return size2;
        }
    }

    @DexIgnore
    public final void a(lf0 lf0) {
        LinkedList linkedList = new LinkedList(this.a);
        this.a.clear();
        ok0 ok0 = this.b;
        if (ok0 != null) {
            ok0.a(lf0);
        }
        Iterator it = linkedList.iterator();
        while (it.hasNext()) {
            ((ok0) it.next()).a(lf0);
        }
    }
}
