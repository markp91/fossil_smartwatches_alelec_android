package com.fossil;

import android.content.Context;
import android.content.res.ColorStateList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m4 {
    @DexIgnore
    float a(l4 l4Var);

    @DexIgnore
    void a();

    @DexIgnore
    void a(l4 l4Var, float f);

    @DexIgnore
    void a(l4 l4Var, Context context, ColorStateList colorStateList, float f, float f2, float f3);

    @DexIgnore
    void a(l4 l4Var, ColorStateList colorStateList);

    @DexIgnore
    float b(l4 l4Var);

    @DexIgnore
    void b(l4 l4Var, float f);

    @DexIgnore
    void c(l4 l4Var);

    @DexIgnore
    void c(l4 l4Var, float f);

    @DexIgnore
    float d(l4 l4Var);

    @DexIgnore
    ColorStateList e(l4 l4Var);

    @DexIgnore
    void f(l4 l4Var);

    @DexIgnore
    float g(l4 l4Var);

    @DexIgnore
    float h(l4 l4Var);

    @DexIgnore
    void i(l4 l4Var);
}
