package com.fossil;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import java.util.UUID;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mg0 extends an1 {
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ BluetoothGattDescriptor d;

    @DexIgnore
    public mg0(BluetoothDevice bluetoothDevice, int i, int i2, BluetoothGattDescriptor bluetoothGattDescriptor) {
        super(bluetoothDevice, i);
        this.c = i2;
        this.d = bluetoothGattDescriptor;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x001a, code lost:
        r2 = r2.getUuid();
     */
    @DexIgnore
    public JSONObject a() {
        UUID uuid;
        JSONObject a = cw0.a(super.a(), bm0.OFFSET, (Object) Integer.valueOf(this.c));
        bm0 bm0 = bm0.CHARACTERISTIC;
        BluetoothGattCharacteristic characteristic = this.d.getCharacteristic();
        String uuid2 = (characteristic == null || uuid == null) ? null : uuid.toString();
        return cw0.a(cw0.a(a, bm0, (Object) uuid2), bm0.DESCRIPTOR, (Object) this.d.getUuid().toString());
    }
}
