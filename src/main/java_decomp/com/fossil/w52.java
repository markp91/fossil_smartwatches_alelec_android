package com.fossil;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.fossil.y52;
import java.util.LinkedList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class w52<T extends y52> {
    @DexIgnore
    public T a;
    @DexIgnore
    public Bundle b;
    @DexIgnore
    public LinkedList<a> c;
    @DexIgnore
    public /* final */ a62<T> d; // = new c62(this);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(y52 y52);

        @DexIgnore
        int getState();
    }

    @DexIgnore
    public static void b(FrameLayout frameLayout) {
        jv1 a2 = jv1.a();
        Context context = frameLayout.getContext();
        int c2 = a2.c(context);
        String b2 = f12.b(context, c2);
        String a3 = f12.a(context, c2);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(b2);
        linearLayout.addView(textView);
        Intent a4 = a2.a(context, c2, (String) null);
        if (a4 != null) {
            Button button = new Button(context);
            button.setId(16908313);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(a3);
            linearLayout.addView(button);
            button.setOnClickListener(new f62(context, a4));
        }
    }

    @DexIgnore
    public T a() {
        return this.a;
    }

    @DexIgnore
    public abstract void a(a62<T> a62);

    @DexIgnore
    public void c() {
        T t = this.a;
        if (t != null) {
            t.e();
        } else {
            a(2);
        }
    }

    @DexIgnore
    public void d() {
        T t = this.a;
        if (t != null) {
            t.onLowMemory();
        }
    }

    @DexIgnore
    public void e() {
        T t = this.a;
        if (t != null) {
            t.onPause();
        } else {
            a(5);
        }
    }

    @DexIgnore
    public void f() {
        a((Bundle) null, (a) new h62(this));
    }

    @DexIgnore
    public void g() {
        a((Bundle) null, (a) new i62(this));
    }

    @DexIgnore
    public void h() {
        T t = this.a;
        if (t != null) {
            t.c();
        } else {
            a(4);
        }
    }

    @DexIgnore
    public final void a(int i) {
        while (!this.c.isEmpty() && this.c.getLast().getState() >= i) {
            this.c.removeLast();
        }
    }

    @DexIgnore
    public final void a(Bundle bundle, a aVar) {
        T t = this.a;
        if (t != null) {
            aVar.a(t);
            return;
        }
        if (this.c == null) {
            this.c = new LinkedList<>();
        }
        this.c.add(aVar);
        if (bundle != null) {
            Bundle bundle2 = this.b;
            if (bundle2 == null) {
                this.b = (Bundle) bundle.clone();
            } else {
                bundle2.putAll(bundle);
            }
        }
        a(this.d);
    }

    @DexIgnore
    public void a(Activity activity, Bundle bundle, Bundle bundle2) {
        a(bundle2, (a) new e62(this, activity, bundle, bundle2));
    }

    @DexIgnore
    public void a(Bundle bundle) {
        a(bundle, (a) new d62(this, bundle));
    }

    @DexIgnore
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        a(bundle, (a) new g62(this, frameLayout, layoutInflater, viewGroup, bundle));
        if (this.a == null) {
            a(frameLayout);
        }
        return frameLayout;
    }

    @DexIgnore
    public void a(FrameLayout frameLayout) {
        b(frameLayout);
    }

    @DexIgnore
    public void b() {
        T t = this.a;
        if (t != null) {
            t.b();
        } else {
            a(1);
        }
    }

    @DexIgnore
    public void b(Bundle bundle) {
        T t = this.a;
        if (t != null) {
            t.a(bundle);
            return;
        }
        Bundle bundle2 = this.b;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
    }
}
