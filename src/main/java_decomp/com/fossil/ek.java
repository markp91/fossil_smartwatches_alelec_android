package com.fossil;

import android.graphics.Matrix;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ek {
    @DexIgnore
    public static /* final */ kk a;
    @DexIgnore
    public static /* final */ Property<View, Float> b; // = new a(Float.class, "translationAlpha");
    @DexIgnore
    public static /* final */ Property<View, Rect> c; // = new b(Rect.class, "clipBounds");

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends Property<View, Float> {
        @DexIgnore
        public a(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Float get(View view) {
            return Float.valueOf(ek.c(view));
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Float f) {
            ek.a(view, f.floatValue());
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends Property<View, Rect> {
        @DexIgnore
        public b(Class cls, String str) {
            super(cls, str);
        }

        @DexIgnore
        /* renamed from: a */
        public Rect get(View view) {
            return x9.i(view);
        }

        @DexIgnore
        /* renamed from: a */
        public void set(View view, Rect rect) {
            x9.a(view, rect);
        }
    }

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 29) {
            a = new jk();
        } else if (i >= 23) {
            a = new ik();
        } else if (i >= 22) {
            a = new hk();
        } else if (i >= 21) {
            a = new gk();
        } else if (i >= 19) {
            a = new fk();
        } else {
            a = new kk();
        }
    }
    */

    @DexIgnore
    public static void a(View view, float f) {
        a.a(view, f);
    }

    @DexIgnore
    public static dk b(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new ck(view);
        }
        return bk.c(view);
    }

    @DexIgnore
    public static float c(View view) {
        return a.b(view);
    }

    @DexIgnore
    public static ok d(View view) {
        if (Build.VERSION.SDK_INT >= 18) {
            return new nk(view);
        }
        return new mk(view.getWindowToken());
    }

    @DexIgnore
    public static void e(View view) {
        a.c(view);
    }

    @DexIgnore
    public static void a(View view) {
        a.a(view);
    }

    @DexIgnore
    public static void c(View view, Matrix matrix) {
        a.c(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i) {
        a.a(view, i);
    }

    @DexIgnore
    public static void a(View view, Matrix matrix) {
        a.a(view, matrix);
    }

    @DexIgnore
    public static void b(View view, Matrix matrix) {
        a.b(view, matrix);
    }

    @DexIgnore
    public static void a(View view, int i, int i2, int i3, int i4) {
        a.a(view, i, i2, i3, i4);
    }
}
