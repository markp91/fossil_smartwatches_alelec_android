package com.fossil;

import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pw4 {
    @DexIgnore
    public /* final */ float a;
    @DexIgnore
    public /* final */ float b;
    @DexIgnore
    public /* final */ float c;
    @DexIgnore
    public /* final */ float d;
    @DexIgnore
    public /* final */ b e;
    @DexIgnore
    public /* final */ PointF f; // = new PointF();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[b.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|20) */
        /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        /*
        static {
            a[b.TOP_LEFT.ordinal()] = 1;
            a[b.TOP_RIGHT.ordinal()] = 2;
            a[b.BOTTOM_LEFT.ordinal()] = 3;
            a[b.BOTTOM_RIGHT.ordinal()] = 4;
            a[b.LEFT.ordinal()] = 5;
            a[b.TOP.ordinal()] = 6;
            a[b.RIGHT.ordinal()] = 7;
            a[b.BOTTOM.ordinal()] = 8;
            a[b.CENTER.ordinal()] = 9;
        }
        */
    }

    @DexIgnore
    public enum b {
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        LEFT,
        TOP,
        RIGHT,
        BOTTOM,
        CENTER
    }

    /*
    static {
        new Matrix();
    }
    */

    @DexIgnore
    public pw4(b bVar, ow4 ow4, float f2, float f3) {
        this.e = bVar;
        this.a = ow4.e();
        this.b = ow4.d();
        this.c = ow4.c();
        this.d = ow4.b();
        a(ow4.f(), f2, f3);
    }

    @DexIgnore
    public static float a(float f2, float f3, float f4, float f5) {
        return (f4 - f2) / (f5 - f3);
    }

    @DexIgnore
    public void a(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, boolean z, float f5) {
        PointF pointF = this.f;
        float f6 = f2 + pointF.x;
        float f7 = f3 + pointF.y;
        if (this.e == b.CENTER) {
            a(rectF, f6, f7, rectF2, i, i2, f4);
        } else if (z) {
            a(rectF, f6, f7, rectF2, i, i2, f4, f5);
        } else {
            b(rectF, f6, f7, rectF2, i, i2, f4);
        }
    }

    @DexIgnore
    public final void b(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4) {
        switch (a.a[this.e.ordinal()]) {
            case 1:
                RectF rectF3 = rectF;
                RectF rectF4 = rectF2;
                float f5 = f4;
                b(rectF3, f3, rectF4, f5, 0.0f, false, false);
                a(rectF3, f2, rectF4, f5, 0.0f, false, false);
                return;
            case 2:
                RectF rectF5 = rectF;
                RectF rectF6 = rectF2;
                b(rectF5, f3, rectF6, f4, 0.0f, false, false);
                b(rectF5, f2, rectF6, i, f4, 0.0f, false, false);
                return;
            case 3:
                RectF rectF7 = rectF;
                RectF rectF8 = rectF2;
                a(rectF7, f3, rectF8, i2, f4, 0.0f, false, false);
                a(rectF7, f2, rectF8, f4, 0.0f, false, false);
                return;
            case 4:
                RectF rectF9 = rectF;
                RectF rectF10 = rectF2;
                float f6 = f4;
                a(rectF9, f3, rectF10, i2, f6, 0.0f, false, false);
                b(rectF9, f2, rectF10, i, f6, 0.0f, false, false);
                return;
            case 5:
                a(rectF, f2, rectF2, f4, 0.0f, false, false);
                return;
            case 6:
                b(rectF, f3, rectF2, f4, 0.0f, false, false);
                return;
            case 7:
                b(rectF, f2, rectF2, i, f4, 0.0f, false, false);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, 0.0f, false, false);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void c(RectF rectF, RectF rectF2, float f2) {
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4 + f2) {
            rectF.offset(f4 - f3, 0.0f);
        }
        float f5 = rectF.top;
        float f6 = rectF2.top;
        if (f5 < f6 + f2) {
            rectF.offset(0.0f, f6 - f5);
        }
        float f7 = rectF.right;
        float f8 = rectF2.right;
        if (f7 > f8 - f2) {
            rectF.offset(f8 - f7, 0.0f);
        }
        float f9 = rectF.bottom;
        float f10 = rectF2.bottom;
        if (f9 > f10 - f2) {
            rectF.offset(0.0f, f10 - f9);
        }
    }

    @DexIgnore
    public final void d(RectF rectF, float f2) {
        rectF.top = rectF.bottom - (rectF.width() / f2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0044, code lost:
        r3 = r3 - r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        r4 = r2.f;
        r4.x = r1;
        r4.y = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0026, code lost:
        r1 = r3 - r4;
     */
    @DexIgnore
    public final void a(RectF rectF, float f2, float f3) {
        float f4;
        float f5;
        float f6 = 0.0f;
        switch (a.a[this.e.ordinal()]) {
            case 1:
                f6 = rectF.left - f2;
                f4 = rectF.top;
                break;
            case 2:
                f6 = rectF.right - f2;
                f4 = rectF.top;
                break;
            case 3:
                f6 = rectF.left - f2;
                f4 = rectF.bottom;
                break;
            case 4:
                f6 = rectF.right - f2;
                f4 = rectF.bottom;
                break;
            case 5:
                f5 = rectF.left;
                break;
            case 6:
                f4 = rectF.top;
                break;
            case 7:
                f5 = rectF.right;
                break;
            case 8:
                f4 = rectF.bottom;
                break;
            case 9:
                f6 = rectF.centerX() - f2;
                f4 = rectF.centerY();
                break;
            default:
                float f7 = 0.0f;
                break;
        }
    }

    @DexIgnore
    public final void c(RectF rectF, float f2) {
        rectF.right = rectF.left + (rectF.height() * f2);
    }

    @DexIgnore
    public final void b(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.x -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.right;
        if (f2 > f6) {
            this.f.x -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.right;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.left;
        float f9 = this.a;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.left;
        float f11 = this.c;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.right;
        if (f12 - f2 < f3) {
            f2 = f12;
        }
        if (f4 > 0.0f) {
            float f13 = rectF.left;
            float f14 = (f2 - f13) / f4;
            float f15 = this.b;
            if (f14 < f15) {
                f2 = Math.min(rectF2.right, f13 + (f15 * f4));
                f14 = (f2 - rectF.left) / f4;
            }
            float f16 = this.d;
            if (f14 > f16) {
                f2 = Math.min(rectF2.right, rectF.left + (f16 * f4));
                f14 = (f2 - rectF.left) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.bottom;
                    float f18 = rectF2.top;
                    if (f17 - f14 < f18) {
                        f2 = Math.min(rectF2.right, rectF.left + ((f17 - f18) * f4));
                        f14 = (f2 - rectF.left) / f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.top;
                    float f20 = rectF2.bottom;
                    if (f14 + f19 > f20) {
                        f2 = Math.min(f2, Math.min(rectF2.right, rectF.left + ((f20 - f19) * f4)));
                    }
                }
            } else {
                f2 = Math.min(f2, Math.min(rectF2.right, rectF.left + (rectF2.height() * f4)));
            }
        }
        rectF.right = f2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0056, code lost:
        if ((r0 + r9) <= r10.bottom) goto L_0x0062;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002d, code lost:
        if ((r1 + r8) <= r10.right) goto L_0x0039;
     */
    @DexIgnore
    public final void a(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4) {
        float centerX = f2 - rectF.centerX();
        float centerY = f3 - rectF.centerY();
        float f5 = rectF.left;
        if (f5 + centerX >= 0.0f) {
            float f6 = rectF.right;
            if (f6 + centerX <= ((float) i)) {
                if (f5 + centerX >= rectF2.left) {
                }
            }
        }
        centerX /= 1.05f;
        this.f.x -= centerX / 2.0f;
        float f7 = rectF.top;
        if (f7 + centerY >= 0.0f) {
            float f8 = rectF.bottom;
            if (f8 + centerY <= ((float) i2)) {
                if (f7 + centerY >= rectF2.top) {
                }
            }
        }
        centerY /= 1.05f;
        this.f.y -= centerY / 2.0f;
        rectF.offset(centerX, centerY);
        c(rectF, rectF2, f4);
    }

    @DexIgnore
    public final void a(RectF rectF, float f2, float f3, RectF rectF2, int i, int i2, float f4, float f5) {
        RectF rectF3 = rectF;
        float f6 = f2;
        float f7 = f3;
        RectF rectF4 = rectF2;
        float f8 = f5;
        switch (a.a[this.e.ordinal()]) {
            case 1:
                if (a(f2, f7, rectF3.right, rectF3.bottom) < f8) {
                    b(rectF, f3, rectF2, f4, f5, true, false);
                    b(rectF, f8);
                    return;
                }
                a(rectF, f2, rectF2, f4, f5, true, false);
                d(rectF, f8);
                return;
            case 2:
                if (a(rectF3.left, f7, f2, rectF3.bottom) < f8) {
                    b(rectF, f3, rectF2, f4, f5, false, true);
                    c(rectF, f8);
                    return;
                }
                b(rectF, f2, rectF2, i, f4, f5, true, false);
                d(rectF, f8);
                return;
            case 3:
                if (a(f2, rectF3.top, rectF3.right, f7) < f8) {
                    a(rectF, f3, rectF2, i2, f4, f5, true, false);
                    b(rectF, f8);
                    return;
                }
                a(rectF, f2, rectF2, f4, f5, false, true);
                a(rectF, f8);
                return;
            case 4:
                if (a(rectF3.left, rectF3.top, f2, f7) < f8) {
                    a(rectF, f3, rectF2, i2, f4, f5, false, true);
                    c(rectF, f8);
                    return;
                }
                b(rectF, f2, rectF2, i, f4, f5, false, true);
                a(rectF, f8);
                return;
            case 5:
                a(rectF, f2, rectF2, f4, f5, true, true);
                b(rectF, rectF4, f8);
                return;
            case 6:
                b(rectF, f3, rectF2, f4, f5, true, true);
                a(rectF, rectF4, f8);
                return;
            case 7:
                b(rectF, f2, rectF2, i, f4, f5, true, true);
                b(rectF, rectF4, f8);
                return;
            case 8:
                a(rectF, f3, rectF2, i2, f4, f5, true, true);
                a(rectF, rectF4, f8);
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void b(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < 0.0f) {
            f2 /= 1.05f;
            this.f.y -= f2 / 1.1f;
        }
        float f5 = rectF2.top;
        if (f2 < f5) {
            this.f.y -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.top;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.bottom;
        float f8 = this.b;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.bottom;
        float f10 = this.d;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.top;
        if (f2 - f11 < f3) {
            f2 = f11;
        }
        if (f4 > 0.0f) {
            float f12 = rectF.bottom;
            float f13 = (f12 - f2) * f4;
            float f14 = this.a;
            if (f13 < f14) {
                f2 = Math.max(rectF2.top, f12 - (f14 / f4));
                f13 = (rectF.bottom - f2) * f4;
            }
            float f15 = this.c;
            if (f13 > f15) {
                f2 = Math.max(rectF2.top, rectF.bottom - (f15 / f4));
                f13 = (rectF.bottom - f2) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.right;
                    float f17 = rectF2.left;
                    if (f16 - f13 < f17) {
                        f2 = Math.max(rectF2.top, rectF.bottom - ((f16 - f17) / f4));
                        f13 = (rectF.bottom - f2) * f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.left;
                    float f19 = rectF2.right;
                    if (f13 + f18 > f19) {
                        f2 = Math.max(f2, Math.max(rectF2.top, rectF.bottom - ((f19 - f18) / f4)));
                    }
                }
            } else {
                f2 = Math.max(f2, Math.max(rectF2.top, rectF.bottom - (rectF2.width() / f4)));
            }
        }
        rectF.top = f2;
    }

    @DexIgnore
    public final void a(RectF rectF, float f2, RectF rectF2, float f3, float f4, boolean z, boolean z2) {
        if (f2 < 0.0f) {
            f2 /= 1.05f;
            this.f.x -= f2 / 1.1f;
        }
        float f5 = rectF2.left;
        if (f2 < f5) {
            this.f.x -= (f2 - f5) / 2.0f;
        }
        float f6 = rectF2.left;
        if (f2 - f6 < f3) {
            f2 = f6;
        }
        float f7 = rectF.right;
        float f8 = this.a;
        if (f7 - f2 < f8) {
            f2 = f7 - f8;
        }
        float f9 = rectF.right;
        float f10 = this.c;
        if (f9 - f2 > f10) {
            f2 = f9 - f10;
        }
        float f11 = rectF2.left;
        if (f2 - f11 < f3) {
            f2 = f11;
        }
        if (f4 > 0.0f) {
            float f12 = rectF.right;
            float f13 = (f12 - f2) / f4;
            float f14 = this.b;
            if (f13 < f14) {
                f2 = Math.max(rectF2.left, f12 - (f14 * f4));
                f13 = (rectF.right - f2) / f4;
            }
            float f15 = this.d;
            if (f13 > f15) {
                f2 = Math.max(rectF2.left, rectF.right - (f15 * f4));
                f13 = (rectF.right - f2) / f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f16 = rectF.bottom;
                    float f17 = rectF2.top;
                    if (f16 - f13 < f17) {
                        f2 = Math.max(rectF2.left, rectF.right - ((f16 - f17) * f4));
                        f13 = (rectF.right - f2) / f4;
                    }
                }
                if (z2) {
                    float f18 = rectF.top;
                    float f19 = rectF2.bottom;
                    if (f13 + f18 > f19) {
                        f2 = Math.max(f2, Math.max(rectF2.left, rectF.right - ((f19 - f18) * f4)));
                    }
                }
            } else {
                f2 = Math.max(f2, Math.max(rectF2.left, rectF.right - (rectF2.height() * f4)));
            }
        }
        rectF.left = f2;
    }

    @DexIgnore
    public final void b(RectF rectF, float f2) {
        rectF.left = rectF.right - (rectF.height() * f2);
    }

    @DexIgnore
    public final void b(RectF rectF, RectF rectF2, float f2) {
        rectF.inset(0.0f, (rectF.height() - (rectF.width() / f2)) / 2.0f);
        float f3 = rectF.top;
        float f4 = rectF2.top;
        if (f3 < f4) {
            rectF.offset(0.0f, f4 - f3);
        }
        float f5 = rectF.bottom;
        float f6 = rectF2.bottom;
        if (f5 > f6) {
            rectF.offset(0.0f, f6 - f5);
        }
    }

    @DexIgnore
    public final void a(RectF rectF, float f2, RectF rectF2, int i, float f3, float f4, boolean z, boolean z2) {
        float f5 = (float) i;
        if (f2 > f5) {
            f2 = ((f2 - f5) / 1.05f) + f5;
            this.f.y -= (f2 - f5) / 1.1f;
        }
        float f6 = rectF2.bottom;
        if (f2 > f6) {
            this.f.y -= (f2 - f6) / 2.0f;
        }
        float f7 = rectF2.bottom;
        if (f7 - f2 < f3) {
            f2 = f7;
        }
        float f8 = rectF.top;
        float f9 = this.b;
        if (f2 - f8 < f9) {
            f2 = f8 + f9;
        }
        float f10 = rectF.top;
        float f11 = this.d;
        if (f2 - f10 > f11) {
            f2 = f10 + f11;
        }
        float f12 = rectF2.bottom;
        if (f12 - f2 < f3) {
            f2 = f12;
        }
        if (f4 > 0.0f) {
            float f13 = rectF.top;
            float f14 = (f2 - f13) * f4;
            float f15 = this.a;
            if (f14 < f15) {
                f2 = Math.min(rectF2.bottom, f13 + (f15 / f4));
                f14 = (f2 - rectF.top) * f4;
            }
            float f16 = this.c;
            if (f14 > f16) {
                f2 = Math.min(rectF2.bottom, rectF.top + (f16 / f4));
                f14 = (f2 - rectF.top) * f4;
            }
            if (!z || !z2) {
                if (z) {
                    float f17 = rectF.right;
                    float f18 = rectF2.left;
                    if (f17 - f14 < f18) {
                        f2 = Math.min(rectF2.bottom, rectF.top + ((f17 - f18) / f4));
                        f14 = (f2 - rectF.top) * f4;
                    }
                }
                if (z2) {
                    float f19 = rectF.left;
                    float f20 = rectF2.right;
                    if (f14 + f19 > f20) {
                        f2 = Math.min(f2, Math.min(rectF2.bottom, rectF.top + ((f20 - f19) / f4)));
                    }
                }
            } else {
                f2 = Math.min(f2, Math.min(rectF2.bottom, rectF.top + (rectF2.width() / f4)));
            }
        }
        rectF.bottom = f2;
    }

    @DexIgnore
    public final void a(RectF rectF, float f2) {
        rectF.bottom = rectF.top + (rectF.width() / f2);
    }

    @DexIgnore
    public final void a(RectF rectF, RectF rectF2, float f2) {
        rectF.inset((rectF.width() - (rectF.height() * f2)) / 2.0f, 0.0f);
        float f3 = rectF.left;
        float f4 = rectF2.left;
        if (f3 < f4) {
            rectF.offset(f4 - f3, 0.0f);
        }
        float f5 = rectF.right;
        float f6 = rectF2.right;
        if (f5 > f6) {
            rectF.offset(f6 - f5, 0.0f);
        }
    }
}
