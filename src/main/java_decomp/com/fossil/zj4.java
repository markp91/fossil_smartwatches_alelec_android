package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zj4 {
    @DexIgnore
    public static float a(float f, float f2) {
        return g((f * 12.0f) + f2);
    }

    @DexIgnore
    public static int a(int i) {
        return i * 12;
    }

    @DexIgnore
    public static lc6<Integer, Integer> b(float f) {
        int round = (int) Math.round(((double) f) / 2.54d);
        return new lc6<>(Integer.valueOf(round / 12), Integer.valueOf(round % 12));
    }

    @DexIgnore
    public static float c(float f) {
        return ((float) Math.round((((double) (f - 32.0f)) * 0.5555555555555556d) * 100.0d)) / 100.0f;
    }

    @DexIgnore
    public static float d(float f) {
        return f / 100.0f;
    }

    @DexIgnore
    public static float e(float f) {
        return f * 0.001f;
    }

    @DexIgnore
    public static float f(float f) {
        return f * 0.00220462f;
    }

    @DexIgnore
    public static float g(float f) {
        return f * 2.54f;
    }

    @DexIgnore
    public static float h(float f) {
        return f * 1000.0f;
    }

    @DexIgnore
    public static float i(float f) {
        return f * 0.001f;
    }

    @DexIgnore
    public static float j(float f) {
        return f * 6.21371E-4f;
    }

    @DexIgnore
    public static float k(float f) {
        return f * 453.592f;
    }

    @DexIgnore
    public static float a(float f) {
        return ((float) Math.round(((((double) f) * 1.8d) + 32.0d) * 100.0d)) / 100.0f;
    }
}
