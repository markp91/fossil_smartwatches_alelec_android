package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.NotificationSummaryDialView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class bc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ ImageView q;
    @DexIgnore
    public /* final */ NotificationSummaryDialView r;
    @DexIgnore
    public /* final */ ConstraintLayout s;

    @DexIgnore
    public bc4(Object obj, View view, int i, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, ImageView imageView, NotificationSummaryDialView notificationSummaryDialView, ConstraintLayout constraintLayout) {
        super(obj, view, i);
        this.q = imageView;
        this.r = notificationSummaryDialView;
        this.s = constraintLayout;
    }
}
