package com.fossil;

import com.portfolio.platform.uirenew.home.customize.diana.theme.EditPhotoViewModel;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c75 implements Factory<b75> {
    @DexIgnore
    public static /* final */ c75 a; // = new c75();

    @DexIgnore
    public static c75 a() {
        return a;
    }

    @DexIgnore
    public static EditPhotoViewModel b() {
        return new EditPhotoViewModel();
    }

    @DexIgnore
    public EditPhotoViewModel get() {
        return b();
    }
}
