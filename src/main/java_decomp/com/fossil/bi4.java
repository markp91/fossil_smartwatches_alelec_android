package com.fossil;

import com.misfit.frameworks.buttonservice.model.complicationapp.WeatherComplicationAppInfo;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum bi4 {
    CLEAR_DAY(WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY, "clear-day"),
    CLEAR_NIGHT(WeatherComplicationAppInfo.WeatherCondition.CLEAR_NIGHT, "clear-night"),
    RAIN(WeatherComplicationAppInfo.WeatherCondition.RAIN, "rain"),
    SNOW(WeatherComplicationAppInfo.WeatherCondition.SNOW, "snow"),
    SLEET(WeatherComplicationAppInfo.WeatherCondition.SLEET, "sleet"),
    WIND(WeatherComplicationAppInfo.WeatherCondition.WIND, "wind"),
    FOG(WeatherComplicationAppInfo.WeatherCondition.FOG, "fog"),
    CLOUDY(WeatherComplicationAppInfo.WeatherCondition.CLOUDY, "cloudy"),
    PARTLY_CLOUDY_DAY(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_DAY, "partly-cloudy-day"),
    PARTLY_CLOUDY_NIGHT(WeatherComplicationAppInfo.WeatherCondition.PARTLY_CLOUDY_NIGHT, "partly-cloudy-night");
    
    @DexIgnore
    public static /* final */ a Companion; // = null;
    @DexIgnore
    public /* final */ String mConditionDescription;
    @DexIgnore
    public /* final */ WeatherComplicationAppInfo.WeatherCondition mConditionValue;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:7:0x001b, code lost:
            r6 = com.fossil.bi4.access$getMConditionValue$p(r3);
         */
        @DexIgnore
        public final WeatherComplicationAppInfo.WeatherCondition a(String str) {
            bi4 bi4;
            WeatherComplicationAppInfo.WeatherCondition access$getMConditionValue$p;
            bi4[] values = bi4.values();
            int length = values.length;
            int i = 0;
            while (true) {
                if (i >= length) {
                    bi4 = null;
                    break;
                }
                bi4 = values[i];
                if (wg6.a((Object) bi4.mConditionDescription, (Object) str)) {
                    break;
                }
                i++;
            }
            return (bi4 == null || access$getMConditionValue$p == null) ? WeatherComplicationAppInfo.WeatherCondition.CLEAR_DAY : access$getMConditionValue$p;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        Companion = new a((qg6) null);
    }
    */

    @DexIgnore
    public bi4(WeatherComplicationAppInfo.WeatherCondition weatherCondition, String str) {
        this.mConditionValue = weatherCondition;
        this.mConditionDescription = str;
    }

    @DexIgnore
    public static final WeatherComplicationAppInfo.WeatherCondition getConditionValue(String str) {
        return Companion.a(str);
    }
}
