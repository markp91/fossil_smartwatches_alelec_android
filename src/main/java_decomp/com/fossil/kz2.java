package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kz2 implements Parcelable.Creator<dz2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            if (f22.a(a) != 2) {
                f22.v(parcel, a);
            } else {
                i = f22.q(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new dz2(i);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new dz2[i];
    }
}
