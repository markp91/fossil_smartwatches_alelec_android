package com.fossil;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oh2 extends jh2 implements mh2 {
    @DexIgnore
    public oh2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    @DexIgnore
    public final x52 zza(Bitmap bitmap) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) bitmap);
        Parcel a = a(6, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
