package com.fossil;

import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.drawable.Drawable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x0 extends Drawable implements Drawable.Callback {
    @DexIgnore
    public Drawable a;

    @DexIgnore
    public x0(Drawable drawable) {
        a(drawable);
    }

    @DexIgnore
    public Drawable a() {
        return this.a;
    }

    @DexIgnore
    public void draw(Canvas canvas) {
        this.a.draw(canvas);
    }

    @DexIgnore
    public int getChangingConfigurations() {
        return this.a.getChangingConfigurations();
    }

    @DexIgnore
    public Drawable getCurrent() {
        return this.a.getCurrent();
    }

    @DexIgnore
    public int getIntrinsicHeight() {
        return this.a.getIntrinsicHeight();
    }

    @DexIgnore
    public int getIntrinsicWidth() {
        return this.a.getIntrinsicWidth();
    }

    @DexIgnore
    public int getMinimumHeight() {
        return this.a.getMinimumHeight();
    }

    @DexIgnore
    public int getMinimumWidth() {
        return this.a.getMinimumWidth();
    }

    @DexIgnore
    public int getOpacity() {
        return this.a.getOpacity();
    }

    @DexIgnore
    public boolean getPadding(Rect rect) {
        return this.a.getPadding(rect);
    }

    @DexIgnore
    public int[] getState() {
        return this.a.getState();
    }

    @DexIgnore
    public Region getTransparentRegion() {
        return this.a.getTransparentRegion();
    }

    @DexIgnore
    public void invalidateDrawable(Drawable drawable) {
        invalidateSelf();
    }

    @DexIgnore
    public boolean isAutoMirrored() {
        return o7.f(this.a);
    }

    @DexIgnore
    public boolean isStateful() {
        return this.a.isStateful();
    }

    @DexIgnore
    public void jumpToCurrentState() {
        o7.g(this.a);
    }

    @DexIgnore
    public void onBoundsChange(Rect rect) {
        this.a.setBounds(rect);
    }

    @DexIgnore
    public boolean onLevelChange(int i) {
        return this.a.setLevel(i);
    }

    @DexIgnore
    public void scheduleDrawable(Drawable drawable, Runnable runnable, long j) {
        scheduleSelf(runnable, j);
    }

    @DexIgnore
    public void setAlpha(int i) {
        this.a.setAlpha(i);
    }

    @DexIgnore
    public void setAutoMirrored(boolean z) {
        o7.a(this.a, z);
    }

    @DexIgnore
    public void setChangingConfigurations(int i) {
        this.a.setChangingConfigurations(i);
    }

    @DexIgnore
    public void setColorFilter(ColorFilter colorFilter) {
        this.a.setColorFilter(colorFilter);
    }

    @DexIgnore
    public void setDither(boolean z) {
        this.a.setDither(z);
    }

    @DexIgnore
    public void setFilterBitmap(boolean z) {
        this.a.setFilterBitmap(z);
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        o7.a(this.a, f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        o7.a(this.a, i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        return this.a.setState(iArr);
    }

    @DexIgnore
    public void setTint(int i) {
        o7.b(this.a, i);
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        o7.a(this.a, colorStateList);
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        o7.a(this.a, mode);
    }

    @DexIgnore
    public boolean setVisible(boolean z, boolean z2) {
        return super.setVisible(z, z2) || this.a.setVisible(z, z2);
    }

    @DexIgnore
    public void unscheduleDrawable(Drawable drawable, Runnable runnable) {
        unscheduleSelf(runnable);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        Drawable drawable2 = this.a;
        if (drawable2 != null) {
            drawable2.setCallback((Drawable.Callback) null);
        }
        this.a = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
    }
}
