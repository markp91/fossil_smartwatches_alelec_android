package com.fossil;

import android.content.Context;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class bb6 implements ab6 {
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public bb6(i86 i86) {
        if (i86.d() != null) {
            this.a = i86.d();
            i86.i();
            "Android/" + this.a.getPackageName();
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    @DexIgnore
    public File a() {
        return a(this.a.getFilesDir());
    }

    @DexIgnore
    public File a(File file) {
        if (file == null) {
            c86.g().d("Fabric", "Null File");
            return null;
        } else if (file.exists() || file.mkdirs()) {
            return file;
        } else {
            c86.g().w("Fabric", "Couldn't create file");
            return null;
        }
    }
}
