package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pm3 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ll3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ kk3 c;

        @DexIgnore
        public a(Iterable iterable, kk3 kk3) {
            this.b = iterable;
            this.c = kk3;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return qm3.c(this.b.iterator(), this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends ll3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ ck3 c;

        @DexIgnore
        public b(Iterable iterable, ck3 ck3) {
            this.b = iterable;
            this.c = ck3;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            return qm3.a(this.b.iterator(), this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ll3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore
        public c(List list, int i) {
            this.b = list;
            this.c = i;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            int min = Math.min(this.b.size(), this.c);
            List list = this.b;
            return list.subList(min, list.size()).iterator();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends ll3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterable b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements Iterator<T> {
            @DexIgnore
            public boolean a; // = true;
            @DexIgnore
            public /* final */ /* synthetic */ Iterator b;

            @DexIgnore
            public a(d dVar, Iterator it) {
                this.b = it;
            }

            @DexIgnore
            public boolean hasNext() {
                return this.b.hasNext();
            }

            @DexIgnore
            public T next() {
                T next = this.b.next();
                this.a = false;
                return next;
            }

            @DexIgnore
            public void remove() {
                bl3.a(!this.a);
                this.b.remove();
            }
        }

        @DexIgnore
        public d(Iterable iterable, int i) {
            this.b = iterable;
            this.c = i;
        }

        @DexIgnore
        public Iterator<T> iterator() {
            Iterator it = this.b.iterator();
            qm3.a((Iterator<?>) it, this.c);
            return new a(this, it);
        }
    }

    @DexIgnore
    public static <T> T[] a(Iterable<? extends T> iterable, T[] tArr) {
        return a(iterable).toArray(tArr);
    }

    @DexIgnore
    public static <T> T b(Iterable<T> iterable) {
        return qm3.b(iterable.iterator());
    }

    @DexIgnore
    public static Object[] c(Iterable<?> iterable) {
        return a(iterable).toArray();
    }

    @DexIgnore
    public static String d(Iterable<?> iterable) {
        return qm3.d(iterable.iterator());
    }

    @DexIgnore
    public static <T> Iterable<T> b(Iterable<T> iterable, kk3<? super T> kk3) {
        jk3.a(iterable);
        jk3.a(kk3);
        return new a(iterable, kk3);
    }

    @DexIgnore
    public static <E> Collection<E> a(Iterable<E> iterable) {
        return iterable instanceof Collection ? (Collection) iterable : um3.a(iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection, Iterable<? extends T> iterable) {
        if (iterable instanceof Collection) {
            return collection.addAll(cl3.a(iterable));
        }
        jk3.a(iterable);
        return qm3.a(collection, iterable.iterator());
    }

    @DexIgnore
    public static <T> boolean a(Iterable<T> iterable, kk3<? super T> kk3) {
        return qm3.a(iterable.iterator(), kk3);
    }

    @DexIgnore
    public static <F, T> Iterable<T> a(Iterable<F> iterable, ck3<? super F, ? extends T> ck3) {
        jk3.a(iterable);
        jk3.a(ck3);
        return new b(iterable, ck3);
    }

    @DexIgnore
    public static <T> T a(Iterable<? extends T> iterable, T t) {
        return qm3.b(iterable.iterator(), t);
    }

    @DexIgnore
    public static <T> Iterable<T> a(Iterable<T> iterable, int i) {
        jk3.a(iterable);
        jk3.a(i >= 0, (Object) "number to skip cannot be negative");
        if (iterable instanceof List) {
            return new c((List) iterable, i);
        }
        return new d(iterable, i);
    }
}
