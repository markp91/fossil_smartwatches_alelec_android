package com.fossil;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.gms.fitness.data.MapValue;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class l72 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<l72> CREATOR; // = new r72();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public float c;
    @DexIgnore
    public String d;
    @DexIgnore
    public Map<String, MapValue> e;
    @DexIgnore
    public int[] f;
    @DexIgnore
    public float[] g;
    @DexIgnore
    public byte[] h;

    @DexIgnore
    public l72(int i) {
        this(i, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, (Bundle) null, (int[]) null, (float[]) null, (byte[]) null);
    }

    @DexIgnore
    public final int B() {
        boolean z = true;
        if (this.a != 1) {
            z = false;
        }
        w12.b(z, "Value is not in int format");
        return Float.floatToRawIntBits(this.c);
    }

    @DexIgnore
    public final int C() {
        return this.a;
    }

    @DexIgnore
    public final boolean D() {
        return this.b;
    }

    @DexIgnore
    @Deprecated
    public final void a(float f2) {
        w12.b(this.a == 2, "Attempting to set an float value to a field that is not in FLOAT format.  Please check the data type definition and use the right format.");
        this.b = true;
        this.c = f2;
    }

    @DexIgnore
    @Deprecated
    public final void c(int i) {
        w12.b(this.a == 1, "Attempting to set an int value to a field that is not in INT32 format.  Please check the data type definition and use the right format.");
        this.b = true;
        this.c = Float.intBitsToFloat(i);
    }

    @DexIgnore
    @Deprecated
    public final void e(String str) {
        c(pe2.a(str));
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof l72)) {
            return false;
        }
        l72 l72 = (l72) obj;
        int i = this.a;
        if (i == l72.a && this.b == l72.b) {
            switch (i) {
                case 1:
                    if (B() == l72.B()) {
                        return true;
                    }
                    break;
                case 2:
                    return this.c == l72.c;
                case 3:
                    return u12.a(this.d, l72.d);
                case 4:
                    return u12.a(this.e, l72.e);
                case 5:
                    return Arrays.equals(this.f, l72.f);
                case 6:
                    return Arrays.equals(this.g, l72.g);
                case 7:
                    return Arrays.equals(this.h, l72.h);
                default:
                    if (this.c == l72.c) {
                        return true;
                    }
                    break;
            }
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Float.valueOf(this.c), this.d, this.e, this.f, this.g, this.h);
    }

    @DexIgnore
    public final float p() {
        w12.b(this.a == 2, "Value is not in float format");
        return this.c;
    }

    @DexIgnore
    public final String toString() {
        if (!this.b) {
            return "unset";
        }
        switch (this.a) {
            case 1:
                return Integer.toString(B());
            case 2:
                return Float.toString(this.c);
            case 3:
                return this.d;
            case 4:
                return new TreeMap(this.e).toString();
            case 5:
                return Arrays.toString(this.f);
            case 6:
                return Arrays.toString(this.g);
            case 7:
                byte[] bArr = this.h;
                return q42.a(bArr, 0, bArr.length, false);
            default:
                return "unknown";
        }
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        Bundle bundle;
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, C());
        g22.a(parcel, 2, D());
        g22.a(parcel, 3, this.c);
        g22.a(parcel, 4, this.d, false);
        Map<String, MapValue> map = this.e;
        if (map == null) {
            bundle = null;
        } else {
            Bundle bundle2 = new Bundle(map.size());
            for (Map.Entry next : this.e.entrySet()) {
                bundle2.putParcelable((String) next.getKey(), (Parcelable) next.getValue());
            }
            bundle = bundle2;
        }
        g22.a(parcel, 5, bundle, false);
        g22.a(parcel, 6, this.f, false);
        g22.a(parcel, 7, this.g, false);
        g22.a(parcel, 8, this.h, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public l72(int i, boolean z, float f2, String str, Bundle bundle, int[] iArr, float[] fArr, byte[] bArr) {
        p4 p4Var;
        this.a = i;
        this.b = z;
        this.c = f2;
        this.d = str;
        if (bundle == null) {
            p4Var = null;
        } else {
            bundle.setClassLoader(MapValue.class.getClassLoader());
            p4Var = new p4(bundle.size());
            for (String str2 : bundle.keySet()) {
                p4Var.put(str2, (MapValue) bundle.getParcelable(str2));
            }
        }
        this.e = p4Var;
        this.f = iArr;
        this.g = fArr;
        this.h = bArr;
    }
}
