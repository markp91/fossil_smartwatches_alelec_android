package com.fossil;

import com.portfolio.platform.data.source.remote.AuthApiGuestService;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class np4 implements Factory<mp4> {
    @DexIgnore
    public /* final */ Provider<zm4> a;
    @DexIgnore
    public /* final */ Provider<AuthApiGuestService> b;
    @DexIgnore
    public /* final */ Provider<an4> c;

    @DexIgnore
    public np4(Provider<zm4> provider, Provider<AuthApiGuestService> provider2, Provider<an4> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    @DexIgnore
    public static np4 a(Provider<zm4> provider, Provider<AuthApiGuestService> provider2, Provider<an4> provider3) {
        return new np4(provider, provider2, provider3);
    }

    @DexIgnore
    public static mp4 b(Provider<zm4> provider, Provider<AuthApiGuestService> provider2, Provider<an4> provider3) {
        return new mp4(provider.get(), provider2.get(), provider3.get());
    }

    @DexIgnore
    public mp4 get() {
        return b(this.a, this.b, this.c);
    }
}
