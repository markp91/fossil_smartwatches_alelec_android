package com.fossil;

import android.text.TextUtils;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i73 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ long a;
    @DexIgnore
    public /* final */ /* synthetic */ e73 b;

    @DexIgnore
    public i73(e73 e73, long j) {
        this.b = e73;
        this.a = j;
    }

    @DexIgnore
    public final void run() {
        e73 e73 = this.b;
        long j = this.a;
        e73.g();
        e73.e();
        e73.w();
        e73.b().A().a("Resetting analytics data (FE)");
        m93 t = e73.t();
        t.g();
        t.e.a();
        if (e73.l().i(e73.p().A())) {
            e73.k().j.a(j);
        }
        if (!TextUtils.isEmpty(e73.k().B.a())) {
            e73.k().B.a((String) null);
        }
        boolean g = e73.a.g();
        if (!e73.l().p()) {
            e73.k().d(!g);
        }
        e73.q().C();
        e73.h = !g;
        this.b.q().a((AtomicReference<String>) new AtomicReference());
    }
}
