package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ln0 {
    DAY_UNKNOWN((byte) 0),
    DAY_MONDAY((byte) 1),
    DAY_TUESDAY((byte) 2),
    DAY_WEDNESDAY((byte) 3),
    DAY_THURSDAY((byte) 4),
    DAY_FRIDAY((byte) 5),
    DAY_SATURDAY((byte) 6),
    DAY_SUNDAY((byte) 7);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public ln0(byte b) {
        this.a = b;
    }
}
