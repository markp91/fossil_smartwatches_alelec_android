package com.fossil;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fm3<K, V> extends vl3<V> {
    @DexIgnore
    public /* final */ bm3<K, V> map;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends jo3<V> {
        @DexIgnore
        public /* final */ jo3<Map.Entry<K, V>> a; // = fm3.this.map.entrySet().iterator();

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public V next() {
            return this.a.next().getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends tl3<V> {
        @DexIgnore
        public /* final */ /* synthetic */ zl3 val$entryList;

        @DexIgnore
        public b(zl3 zl3) {
            this.val$entryList = zl3;
        }

        @DexIgnore
        public vl3<V> delegateCollection() {
            return fm3.this;
        }

        @DexIgnore
        public V get(int i) {
            return ((Map.Entry) this.val$entryList.get(i)).getValue();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c<V> implements Serializable {
        @DexIgnore
        public static /* final */ long serialVersionUID; // = 0;
        @DexIgnore
        public /* final */ bm3<?, V> map;

        @DexIgnore
        public c(bm3<?, V> bm3) {
            this.map = bm3;
        }

        @DexIgnore
        public Object readResolve() {
            return this.map.values();
        }
    }

    @DexIgnore
    public fm3(bm3<K, V> bm3) {
        this.map = bm3;
    }

    @DexIgnore
    public zl3<V> asList() {
        return new b(this.map.entrySet().asList());
    }

    @DexIgnore
    public boolean contains(Object obj) {
        return obj != null && qm3.a((Iterator<?>) iterator(), obj);
    }

    @DexIgnore
    public boolean isPartialView() {
        return true;
    }

    @DexIgnore
    public int size() {
        return this.map.size();
    }

    @DexIgnore
    public Object writeReplace() {
        return new c(this.map);
    }

    @DexIgnore
    public jo3<V> iterator() {
        return new a();
    }
}
