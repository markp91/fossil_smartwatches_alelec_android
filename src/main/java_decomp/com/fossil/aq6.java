package com.fossil;

import com.fossil.tq6;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class aq6 {
    @DexIgnore
    public /* final */ tq6 a;
    @DexIgnore
    public /* final */ oq6 b;
    @DexIgnore
    public /* final */ SocketFactory c;
    @DexIgnore
    public /* final */ bq6 d;
    @DexIgnore
    public /* final */ List<wq6> e;
    @DexIgnore
    public /* final */ List<jq6> f;
    @DexIgnore
    public /* final */ ProxySelector g;
    @DexIgnore
    public /* final */ Proxy h;
    @DexIgnore
    public /* final */ SSLSocketFactory i;
    @DexIgnore
    public /* final */ HostnameVerifier j;
    @DexIgnore
    public /* final */ fq6 k;

    @DexIgnore
    public aq6(String str, int i2, oq6 oq6, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, fq6 fq6, bq6 bq6, Proxy proxy, List<wq6> list, List<jq6> list2, ProxySelector proxySelector) {
        tq6.a aVar = new tq6.a();
        aVar.f(sSLSocketFactory != null ? "https" : "http");
        aVar.b(str);
        aVar.a(i2);
        this.a = aVar.a();
        if (oq6 != null) {
            this.b = oq6;
            if (socketFactory != null) {
                this.c = socketFactory;
                if (bq6 != null) {
                    this.d = bq6;
                    if (list != null) {
                        this.e = fr6.a(list);
                        if (list2 != null) {
                            this.f = fr6.a(list2);
                            if (proxySelector != null) {
                                this.g = proxySelector;
                                this.h = proxy;
                                this.i = sSLSocketFactory;
                                this.j = hostnameVerifier;
                                this.k = fq6;
                                return;
                            }
                            throw new NullPointerException("proxySelector == null");
                        }
                        throw new NullPointerException("connectionSpecs == null");
                    }
                    throw new NullPointerException("protocols == null");
                }
                throw new NullPointerException("proxyAuthenticator == null");
            }
            throw new NullPointerException("socketFactory == null");
        }
        throw new NullPointerException("dns == null");
    }

    @DexIgnore
    public fq6 a() {
        return this.k;
    }

    @DexIgnore
    public List<jq6> b() {
        return this.f;
    }

    @DexIgnore
    public oq6 c() {
        return this.b;
    }

    @DexIgnore
    public HostnameVerifier d() {
        return this.j;
    }

    @DexIgnore
    public List<wq6> e() {
        return this.e;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof aq6) {
            aq6 aq6 = (aq6) obj;
            return this.a.equals(aq6.a) && a(aq6);
        }
    }

    @DexIgnore
    public Proxy f() {
        return this.h;
    }

    @DexIgnore
    public bq6 g() {
        return this.d;
    }

    @DexIgnore
    public ProxySelector h() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (((((((((((527 + this.a.hashCode()) * 31) + this.b.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f.hashCode()) * 31) + this.g.hashCode()) * 31;
        Proxy proxy = this.h;
        int i2 = 0;
        int hashCode2 = (hashCode + (proxy != null ? proxy.hashCode() : 0)) * 31;
        SSLSocketFactory sSLSocketFactory = this.i;
        int hashCode3 = (hashCode2 + (sSLSocketFactory != null ? sSLSocketFactory.hashCode() : 0)) * 31;
        HostnameVerifier hostnameVerifier = this.j;
        int hashCode4 = (hashCode3 + (hostnameVerifier != null ? hostnameVerifier.hashCode() : 0)) * 31;
        fq6 fq6 = this.k;
        if (fq6 != null) {
            i2 = fq6.hashCode();
        }
        return hashCode4 + i2;
    }

    @DexIgnore
    public SocketFactory i() {
        return this.c;
    }

    @DexIgnore
    public SSLSocketFactory j() {
        return this.i;
    }

    @DexIgnore
    public tq6 k() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Address{");
        sb.append(this.a.g());
        sb.append(":");
        sb.append(this.a.k());
        if (this.h != null) {
            sb.append(", proxy=");
            sb.append(this.h);
        } else {
            sb.append(", proxySelector=");
            sb.append(this.g);
        }
        sb.append("}");
        return sb.toString();
    }

    @DexIgnore
    public boolean a(aq6 aq6) {
        return this.b.equals(aq6.b) && this.d.equals(aq6.d) && this.e.equals(aq6.e) && this.f.equals(aq6.f) && this.g.equals(aq6.g) && fr6.a((Object) this.h, (Object) aq6.h) && fr6.a((Object) this.i, (Object) aq6.i) && fr6.a((Object) this.j, (Object) aq6.j) && fr6.a((Object) this.k, (Object) aq6.k) && k().k() == aq6.k().k();
    }
}
