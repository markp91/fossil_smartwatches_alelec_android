package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fd2 extends uc2 implements gd2 {
    @DexIgnore
    public fd2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IGoogleFitHistoryApi");
    }

    @DexIgnore
    public final void a(n82 n82) throws RemoteException {
        Parcel zza = zza();
        sd2.a(zza, (Parcelable) n82);
        a(2, zza);
    }
}
