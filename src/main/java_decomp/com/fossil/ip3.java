package com.fossil;

import java.util.concurrent.Executor;
import java.util.concurrent.Future;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface ip3<V> extends Future<V> {
    @DexIgnore
    void a(Runnable runnable, Executor executor);
}
