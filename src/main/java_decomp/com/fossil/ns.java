package com.fossil;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import com.fossil.fs;
import java.io.FileNotFoundException;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ns<T> implements fs<T> {
    @DexIgnore
    public /* final */ Uri a;
    @DexIgnore
    public /* final */ ContentResolver b;
    @DexIgnore
    public T c;

    @DexIgnore
    public ns(ContentResolver contentResolver, Uri uri) {
        this.b = contentResolver;
        this.a = uri;
    }

    @DexIgnore
    public abstract T a(Uri uri, ContentResolver contentResolver) throws FileNotFoundException;

    @DexIgnore
    public final void a(br brVar, fs.a<? super T> aVar) {
        try {
            this.c = a(this.a, this.b);
            aVar.a(this.c);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e);
            }
            aVar.a((Exception) e);
        }
    }

    @DexIgnore
    public abstract void a(T t) throws IOException;

    @DexIgnore
    public pr b() {
        return pr.LOCAL;
    }

    @DexIgnore
    public void cancel() {
    }

    @DexIgnore
    public void a() {
        T t = this.c;
        if (t != null) {
            try {
                a(t);
            } catch (IOException unused) {
            }
        }
    }
}
