package com.fossil;

import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.portfolio.platform.service.BleCommandResultManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.service.BleCommandResultManager$retrieveBleResult$1$1$1", f = "BleCommandResultManager.kt", l = {}, m = "invokeSuspend")
public final class op4$e$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.a $bleResult;
    @DexIgnore
    public /* final */ /* synthetic */ CommunicateMode $communicateMode;
    @DexIgnore
    public /* final */ /* synthetic */ BleCommandResultManager.d $observerList;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public op4$e$a(BleCommandResultManager.d dVar, CommunicateMode communicateMode, BleCommandResultManager.a aVar, xe6 xe6) {
        super(2, xe6);
        this.$observerList = dVar;
        this.$communicateMode = communicateMode;
        this.$bleResult = aVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        op4$e$a op4_e_a = new op4$e$a(this.$observerList, this.$communicateMode, this.$bleResult, xe6);
        op4_e_a.p$ = (il6) obj;
        return op4_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((op4$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            for (BleCommandResultManager.b a : this.$observerList.a()) {
                a.a(this.$communicateMode, this.$bleResult.a());
            }
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
