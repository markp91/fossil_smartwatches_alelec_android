package com.fossil;

import com.fossil.af6;
import com.fossil.nn6;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gl6 extends ve6 implements nn6<String> {
    @DexIgnore
    public static /* final */ a b; // = new a((qg6) null);
    @DexIgnore
    public /* final */ long a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements af6.c<gl6> {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public gl6(long j) {
        super(b);
        this.a = j;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof gl6) {
                if (this.a == ((gl6) obj).a) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public <R> R fold(R r, ig6<? super R, ? super af6.b, ? extends R> ig6) {
        wg6.b(ig6, "operation");
        return nn6.a.a(this, r, ig6);
    }

    @DexIgnore
    public <E extends af6.b> E get(af6.c<E> cVar) {
        wg6.b(cVar, "key");
        return nn6.a.a(this, cVar);
    }

    @DexIgnore
    public int hashCode() {
        long j = this.a;
        return (int) (j ^ (j >>> 32));
    }

    @DexIgnore
    public af6 minusKey(af6.c<?> cVar) {
        wg6.b(cVar, "key");
        return nn6.a.b(this, cVar);
    }

    @DexIgnore
    public final long o() {
        return this.a;
    }

    @DexIgnore
    public af6 plus(af6 af6) {
        wg6.b(af6, "context");
        return nn6.a.a(this, af6);
    }

    @DexIgnore
    public String toString() {
        return "CoroutineId(" + this.a + ')';
    }

    @DexIgnore
    public String a(af6 af6) {
        String str;
        wg6.b(af6, "context");
        hl6 hl6 = (hl6) af6.get(hl6.b);
        if (hl6 == null || (str = hl6.o()) == null) {
            str = "coroutine";
        }
        Thread currentThread = Thread.currentThread();
        wg6.a((Object) currentThread, "currentThread");
        String name = currentThread.getName();
        wg6.a((Object) name, "oldName");
        int b2 = yj6.b((CharSequence) name, " @", 0, false, 6, (Object) null);
        if (b2 < 0) {
            b2 = name.length();
        }
        StringBuilder sb = new StringBuilder(str.length() + b2 + 10);
        String substring = name.substring(0, b2);
        wg6.a((Object) substring, "(this as java.lang.Strin\u2026ing(startIndex, endIndex)");
        sb.append(substring);
        sb.append(" @");
        sb.append(str);
        sb.append('#');
        sb.append(this.a);
        String sb2 = sb.toString();
        wg6.a((Object) sb2, "StringBuilder(capacity).\u2026builderAction).toString()");
        currentThread.setName(sb2);
        return name;
    }

    @DexIgnore
    public void a(af6 af6, String str) {
        wg6.b(af6, "context");
        wg6.b(str, "oldState");
        Thread currentThread = Thread.currentThread();
        wg6.a((Object) currentThread, "Thread.currentThread()");
        currentThread.setName(str);
    }
}
