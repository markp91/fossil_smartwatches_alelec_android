package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gi3 extends SparseArray<Parcelable> implements Parcelable {
    @DexIgnore
    public static /* final */ Parcelable.Creator<gi3> CREATOR; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Parcelable.ClassLoaderCreator<gi3> {
        @DexIgnore
        public gi3[] newArray(int i) {
            return new gi3[i];
        }

        @DexIgnore
        public gi3 createFromParcel(Parcel parcel, ClassLoader classLoader) {
            return new gi3(parcel, classLoader);
        }

        @DexIgnore
        public gi3 createFromParcel(Parcel parcel) {
            return new gi3(parcel, (ClassLoader) null);
        }
    }

    @DexIgnore
    public gi3() {
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int size = size();
        int[] iArr = new int[size];
        Parcelable[] parcelableArr = new Parcelable[size];
        for (int i2 = 0; i2 < size; i2++) {
            iArr[i2] = keyAt(i2);
            parcelableArr[i2] = (Parcelable) valueAt(i2);
        }
        parcel.writeInt(size);
        parcel.writeIntArray(iArr);
        parcel.writeParcelableArray(parcelableArr, i);
    }

    @DexIgnore
    public gi3(Parcel parcel, ClassLoader classLoader) {
        int readInt = parcel.readInt();
        int[] iArr = new int[readInt];
        parcel.readIntArray(iArr);
        Parcelable[] readParcelableArray = parcel.readParcelableArray(classLoader);
        for (int i = 0; i < readInt; i++) {
            put(iArr[i], readParcelableArray[i]);
        }
    }
}
