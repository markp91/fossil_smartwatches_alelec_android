package com.fossil;

import android.os.Build;
import android.util.Log;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.service.ErrorResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t66 {
    @DexIgnore
    public static /* final */ TimeZone a; // = TimeZone.getTimeZone("UTC");
    @DexIgnore
    public static /* final */ List<c> b; // = new ArrayList();
    @DexIgnore
    public static c c;
    @DexIgnore
    public static boolean d; // = false;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements c {
        @DexIgnore
        public final boolean a(String str) {
            return a76.b(str) && (str.endsWith("Provider") || str.endsWith("Service"));
        }

        @DexIgnore
        public void a(d dVar, String str, String str2, Throwable th) {
            String a = u66.a(str);
            if (a(str) && d.ERROR == dVar) {
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US);
                simpleDateFormat.setTimeZone(t66.a);
                Log.println(d.ERROR.priority, a, "Time in UTC: " + simpleDateFormat.format(new Date()));
            }
            if (th != null) {
                str2 = str2 + a76.b + Log.getStackTraceString(th);
            }
            for (String println : u66.a(str2, 4000)) {
                Log.println(dVar == null ? d.INFO.priority : dVar.priority, a, println);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements c {
        @DexIgnore
        public void a(d dVar, String str, String str2, Throwable th) {
            StringBuilder sb = new StringBuilder(100);
            sb.append("[");
            sb.append(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).format(new Date()));
            sb.append("]");
            sb.append(" ");
            if (dVar == null) {
                dVar = d.INFO;
            }
            sb.append(u66.a(dVar.priority));
            sb.append(ZendeskConfig.SLASH);
            if (!a76.b(str)) {
                str = "UNKNOWN";
            }
            sb.append(str);
            sb.append(": ");
            sb.append(str2);
            System.out.println(sb.toString());
            if (th != null) {
                th.printStackTrace(System.out);
            }
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        void a(d dVar, String str, String str2, Throwable th);
    }

    @DexIgnore
    public enum d {
        VERBOSE(2),
        DEBUG(3),
        INFO(4),
        WARN(5),
        ERROR(6);
        
        @DexIgnore
        public /* final */ int priority;

        @DexIgnore
        public d(int i) {
            this.priority = i;
        }
    }

    /*
    static {
        b bVar;
        try {
            Class.forName("android.os.Build");
            if (Build.VERSION.SDK_INT != 0) {
                c = new a();
            }
            if (c == null) {
                bVar = new b();
                c = bVar;
            }
        } catch (ClassNotFoundException unused) {
            if (c == null) {
                bVar = new b();
            }
        } catch (Throwable th) {
            if (c == null) {
                c = new b();
            }
            throw th;
        }
    }
    */

    @DexIgnore
    public static boolean b() {
        return d;
    }

    @DexIgnore
    public static void c(String str, String str2, Throwable th, Object... objArr) {
        a(d.WARN, str, str2, th, objArr);
    }

    @DexIgnore
    public static void d(String str, String str2, Object... objArr) {
        a(d.WARN, str, str2, (Throwable) null, objArr);
    }

    @DexIgnore
    public static void a(boolean z) {
        d = z;
    }

    @DexIgnore
    public static void b(String str, String str2, Object... objArr) {
        a(d.ERROR, str, str2, (Throwable) null, objArr);
    }

    @DexIgnore
    public static void c(String str, String str2, Object... objArr) {
        a(d.INFO, str, str2, (Throwable) null, objArr);
    }

    @DexIgnore
    public static void a(String str, String str2, Object... objArr) {
        a(d.DEBUG, str, str2, (Throwable) null, objArr);
    }

    @DexIgnore
    public static void b(String str, String str2, Throwable th, Object... objArr) {
        a(d.ERROR, str, str2, th, objArr);
    }

    @DexIgnore
    public static void a(String str, String str2, Throwable th, Object... objArr) {
        a(d.DEBUG, str, str2, th, objArr);
    }

    @DexIgnore
    public static void a(String str, ErrorResponse errorResponse) {
        StringBuilder sb = new StringBuilder();
        if (errorResponse != null) {
            sb.append("Network Error: ");
            sb.append(errorResponse.b());
            sb.append(", Status Code: ");
            sb.append(errorResponse.o());
            if (a76.b(errorResponse.a())) {
                sb.append(", Reason: ");
                sb.append(errorResponse.a());
            }
        }
        String sb2 = sb.toString();
        d dVar = d.ERROR;
        if (!a76.b(sb2)) {
            sb2 = "Unknown error";
        }
        a(dVar, str, sb2, (Throwable) null, new Object[0]);
    }

    @DexIgnore
    public static void a(d dVar, String str, String str2, Throwable th, Object... objArr) {
        if (objArr != null && objArr.length > 0) {
            str2 = String.format(Locale.US, str2, objArr);
        }
        if (d) {
            c.a(dVar, str, str2, th);
            for (c a2 : b) {
                a2.a(dVar, str, str2, th);
            }
        }
    }
}
