package com.fossil;

import com.portfolio.platform.data.model.room.fitness.ActivitySummary;
import com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$2$1", f = "ActiveTimeDetailPresenter.kt", l = {130}, m = "invokeSuspend")
public final class gh5$h$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ActiveTimeDetailPresenter.h this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.details.activetime.ActiveTimeDetailPresenter$start$2$1$summary$1", f = "ActiveTimeDetailPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super ActivitySummary>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ gh5$h$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(gh5$h$a gh5_h_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = gh5_h_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                ActiveTimeDetailPresenter activeTimeDetailPresenter = this.this$0.this$0.a;
                return activeTimeDetailPresenter.b(activeTimeDetailPresenter.g, (List<ActivitySummary>) this.this$0.this$0.a.k);
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public gh5$h$a(ActiveTimeDetailPresenter.h hVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = hVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        gh5$h$a gh5_h_a = new gh5$h$a(this.this$0, xe6);
        gh5_h_a.p$ = (il6) obj;
        return gh5_h_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((gh5$h$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            dl6 a3 = this.this$0.a.b();
            a aVar = new a(this, (xe6) null);
            this.L$0 = il6;
            this.label = 1;
            obj = gk6.a(a3, aVar, this);
            if (obj == a2) {
                return a2;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ActivitySummary activitySummary = (ActivitySummary) obj;
        if (this.this$0.a.m == null || (!wg6.a((Object) this.this$0.a.m, (Object) activitySummary))) {
            this.this$0.a.m = activitySummary;
            this.this$0.a.s.a(this.this$0.a.o, this.this$0.a.m);
            if (this.this$0.a.i && this.this$0.a.j) {
                rm6 unused = this.this$0.a.m();
            }
        }
        return cd6.a;
    }
}
