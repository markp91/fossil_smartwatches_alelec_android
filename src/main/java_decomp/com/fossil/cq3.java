package com.fossil;

import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class cq3 implements ys3 {
    @DexIgnore
    public /* final */ Set a;

    @DexIgnore
    public cq3(Set set) {
        this.a = set;
    }

    @DexIgnore
    public static ys3 a(Set set) {
        return new cq3(set);
    }

    @DexIgnore
    public Object get() {
        return eq3.a(this.a);
    }
}
