package com.fossil;

import java.util.concurrent.CancellationException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rn6 extends CancellationException {
    @DexIgnore
    public /* final */ rm6 coroutine;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public rn6(String str, rm6 rm6) {
        super(str);
        wg6.b(str, "message");
        this.coroutine = rm6;
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public rn6(String str) {
        this(str, (rm6) null);
        wg6.b(str, "message");
    }
}
