package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.CompoundButton;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class i2 {
    @DexIgnore
    public /* final */ CompoundButton a;
    @DexIgnore
    public ColorStateList b; // = null;
    @DexIgnore
    public PorterDuff.Mode c; // = null;
    @DexIgnore
    public boolean d; // = false;
    @DexIgnore
    public boolean e; // = false;
    @DexIgnore
    public boolean f;

    @DexIgnore
    public i2(CompoundButton compoundButton) {
        this.a = compoundButton;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0031 A[SYNTHETIC, Splitter:B:12:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0058 A[Catch:{ all -> 0x0080 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x006b A[Catch:{ all -> 0x0080 }] */
    public void a(AttributeSet attributeSet, int i) {
        boolean z;
        int resourceId;
        int resourceId2;
        TypedArray obtainStyledAttributes = this.a.getContext().obtainStyledAttributes(attributeSet, j0.CompoundButton, i, 0);
        try {
            if (obtainStyledAttributes.hasValue(j0.CompoundButton_buttonCompat) && (resourceId2 = obtainStyledAttributes.getResourceId(j0.CompoundButton_buttonCompat, 0)) != 0) {
                try {
                    this.a.setButtonDrawable(u0.c(this.a.getContext(), resourceId2));
                    z = true;
                } catch (Resources.NotFoundException unused) {
                }
                if (!z) {
                    if (obtainStyledAttributes.hasValue(j0.CompoundButton_android_button) && (resourceId = obtainStyledAttributes.getResourceId(j0.CompoundButton_android_button, 0)) != 0) {
                        this.a.setButtonDrawable(u0.c(this.a.getContext(), resourceId));
                    }
                }
                if (obtainStyledAttributes.hasValue(j0.CompoundButton_buttonTint)) {
                    oa.a(this.a, obtainStyledAttributes.getColorStateList(j0.CompoundButton_buttonTint));
                }
                if (obtainStyledAttributes.hasValue(j0.CompoundButton_buttonTintMode)) {
                    oa.a(this.a, s2.a(obtainStyledAttributes.getInt(j0.CompoundButton_buttonTintMode, -1), (PorterDuff.Mode) null));
                }
            }
            z = false;
            if (!z) {
            }
            if (obtainStyledAttributes.hasValue(j0.CompoundButton_buttonTint)) {
            }
            if (obtainStyledAttributes.hasValue(j0.CompoundButton_buttonTintMode)) {
            }
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    @DexIgnore
    public ColorStateList b() {
        return this.b;
    }

    @DexIgnore
    public PorterDuff.Mode c() {
        return this.c;
    }

    @DexIgnore
    public void d() {
        if (this.f) {
            this.f = false;
            return;
        }
        this.f = true;
        a();
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.b = colorStateList;
        this.d = true;
        a();
    }

    @DexIgnore
    public void a(PorterDuff.Mode mode) {
        this.c = mode;
        this.e = true;
        a();
    }

    @DexIgnore
    public void a() {
        Drawable a2 = oa.a(this.a);
        if (a2 == null) {
            return;
        }
        if (this.d || this.e) {
            Drawable mutate = o7.i(a2).mutate();
            if (this.d) {
                o7.a(mutate, this.b);
            }
            if (this.e) {
                o7.a(mutate, this.c);
            }
            if (mutate.isStateful()) {
                mutate.setState(this.a.getDrawableState());
            }
            this.a.setButtonDrawable(mutate);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = com.fossil.oa.a(r2.a);
     */
    @DexIgnore
    public int a(int i) {
        Drawable a2;
        return (Build.VERSION.SDK_INT >= 17 || a2 == null) ? i : i + a2.getIntrinsicWidth();
    }
}
