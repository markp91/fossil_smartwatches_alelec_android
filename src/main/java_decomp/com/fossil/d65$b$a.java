package com.fossil;

import com.portfolio.platform.data.model.setting.SecondTimezoneSetting;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.complications.details.secondtimezone.search.SearchSecondTimezonePresenter$start$1$rawDataList$1", f = "SearchSecondTimezonePresenter.kt", l = {}, m = "invokeSuspend")
public final class d65$b$a extends sf6 implements ig6<il6, xe6<? super ArrayList<SecondTimezoneSetting>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public d65$b$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        d65$b$a d65_b_a = new d65$b$a(xe6);
        d65_b_a.p$ = (il6) obj;
        return d65_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((d65$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return tj4.f.g();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
