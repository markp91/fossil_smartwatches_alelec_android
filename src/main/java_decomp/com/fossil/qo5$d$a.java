package com.fossil;

import com.portfolio.platform.data.model.Theme;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel$updateNewName$1$1", f = "UserCustomizeThemeViewModel.kt", l = {43, 45}, m = "invokeSuspend")
public final class qo5$d$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UserCustomizeThemeViewModel.d this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public qo5$d$a(UserCustomizeThemeViewModel.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        qo5$d$a qo5_d_a = new qo5$d$a(this.this$0, xe6);
        qo5_d_a.p$ = (il6) obj;
        return qo5_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((qo5$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        il6 il6;
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 = this.p$;
            this.L$0 = il6;
            this.label = 1;
            obj = this.this$0.this$0.c.getThemeById((String) this.this$0.$id.element, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i == 2) {
            Theme theme = (Theme) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
            return cd6.a;
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        if (obj != null) {
            Theme theme2 = (Theme) obj;
            theme2.setName(this.this$0.$name);
            ThemeRepository b = this.this$0.this$0.c;
            this.L$0 = il6;
            this.L$1 = theme2;
            this.label = 2;
            if (b.upsertUserTheme(theme2, this) == a) {
                return a;
            }
            return cd6.a;
        }
        wg6.a();
        throw null;
    }
}
