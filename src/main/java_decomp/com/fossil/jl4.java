package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.helper.AnalyticsHelper;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jl4 {
    @DexIgnore
    public String a; // = "";
    @DexIgnore
    public String b; // = "";
    @DexIgnore
    public AnalyticsHelper c;
    @DexIgnore
    public Map<String, String> d; // = new HashMap();
    @DexIgnore
    public Map<String, Object> e; // = new HashMap();
    @DexIgnore
    public volatile boolean f;
    @DexIgnore
    public long g; // = -1;
    @DexIgnore
    public b h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void Z0();

        @DexIgnore
        void b1();
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public jl4(AnalyticsHelper analyticsHelper, String str, String str2) {
        wg6.b(analyticsHelper, "analyticsHelper");
        wg6.b(str, "traceName");
        this.c = analyticsHelper;
        String valueOf = String.valueOf(System.currentTimeMillis());
        if (str2 != null) {
            valueOf = str2 + "_" + valueOf;
        }
        this.a = valueOf;
        this.b = str;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final boolean b() {
        return this.f;
    }

    @DexIgnore
    public final void c() {
        this.h = null;
    }

    @DexIgnore
    public final void d() {
        if (this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is already started");
            return;
        }
        this.f = true;
        Map<String, String> map = this.d;
        if (map != null) {
            String put = map.put("trace_id", this.a);
        }
        AnalyticsHelper analyticsHelper = this.c;
        if (analyticsHelper != null) {
            analyticsHelper.a(this.b + "_start", (Map<String, ? extends Object>) this.d);
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            map2.clear();
        }
        this.g = System.currentTimeMillis();
        b bVar = this.h;
        if (bVar != null) {
            bVar.Z0();
        }
    }

    @DexIgnore
    public String toString() {
        return "Tracer name: " + this.b + ", id: " + this.a + ", running: " + this.f;
    }

    @DexIgnore
    public final jl4 a(String str, String str2) {
        wg6.b(str, "paramName");
        wg6.b(str2, "paramValue");
        Map<String, String> map = this.d;
        if (map != null) {
            String put = map.put(str, str2);
        }
        return this;
    }

    @DexIgnore
    public final jl4 a(b bVar) {
        wg6.b(bVar, "tracingCallback");
        this.h = bVar;
        return this;
    }

    @DexIgnore
    public final void a(hl4 hl4) {
        wg6.b(hl4, "analyticsEvent");
        hl4.a("trace_id", this.a);
        hl4.a();
    }

    @DexIgnore
    public final void a(String str) {
        wg6.b(str, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put(Constants.RESULT, str);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.a(this.b + "_end", (Map<String, ? extends Object>) map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.b1();
        }
    }

    @DexIgnore
    public final void a(String str, boolean z, String str2) {
        wg6.b(str, "serial");
        wg6.b(str2, "errorCode");
        if (!this.f) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            local.d("FireBaseTrace", "Tracing " + this.b + " is not started");
            return;
        }
        this.f = false;
        if (this.g > 0) {
            long currentTimeMillis = System.currentTimeMillis() - this.g;
            Map<String, Object> map = this.e;
            if (map != null) {
                map.put("trace_id", this.a);
                map.put("duration", Integer.valueOf((int) currentTimeMillis));
                map.put("cache_hit", Integer.valueOf(z ? 1 : 0));
                map.put("Serial_Number", str);
                map.put(Constants.RESULT, str2);
                AnalyticsHelper analyticsHelper = this.c;
                if (analyticsHelper != null) {
                    analyticsHelper.a(this.b + "_end", (Map<String, ? extends Object>) map);
                }
            }
        }
        b bVar = this.h;
        if (bVar != null) {
            bVar.b1();
        }
    }
}
