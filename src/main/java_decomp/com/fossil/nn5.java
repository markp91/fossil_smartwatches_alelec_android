package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeButtonViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nn5 implements Factory<mn5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public nn5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static nn5 a(Provider<ThemeRepository> provider) {
        return new nn5(provider);
    }

    @DexIgnore
    public static mn5 b(Provider<ThemeRepository> provider) {
        return new CustomizeButtonViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeButtonViewModel get() {
        return b(this.a);
    }
}
