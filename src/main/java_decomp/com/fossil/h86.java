package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h86<Result> extends v96<Void, Void, Result> {
    @DexIgnore
    public /* final */ i86<Result> s;

    @DexIgnore
    public h86(i86<Result> i86) {
        this.s = i86;
    }

    @DexIgnore
    public void c(Result result) {
        this.s.a(result);
        this.s.d.a((Exception) new g86(this.s.h() + " Initialization was cancelled"));
    }

    @DexIgnore
    public void d(Result result) {
        this.s.b(result);
        this.s.d.a(result);
    }

    @DexIgnore
    public void f() {
        super.f();
        o96 a = a("onPreExecute");
        try {
            boolean m = this.s.m();
            a.c();
            if (m) {
                return;
            }
        } catch (ca6 e) {
            throw e;
        } catch (Exception e2) {
            c86.g().e("Fabric", "Failure onPreExecute()", e2);
            a.c();
        } catch (Throwable th) {
            a.c();
            b(true);
            throw th;
        }
        b(true);
    }

    @DexIgnore
    public u96 getPriority() {
        return u96.HIGH;
    }

    @DexIgnore
    public Result a(Void... voidArr) {
        o96 a = a("doInBackground");
        Result c = !e() ? this.s.c() : null;
        a.c();
        return c;
    }

    @DexIgnore
    public final o96 a(String str) {
        o96 o96 = new o96(this.s.h() + "." + str, "KitInitialization");
        o96.b();
        return o96;
    }
}
