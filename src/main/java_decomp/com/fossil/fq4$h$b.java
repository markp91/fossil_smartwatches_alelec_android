package com.fossil;

import androidx.lifecycle.LiveData;
import com.portfolio.platform.service.musiccontrol.MusicControlComponent;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fq4$h$b<T> implements ld<S> {
    @DexIgnore
    public /* final */ /* synthetic */ MusicControlComponent.h a;
    @DexIgnore
    public /* final */ /* synthetic */ LiveData b;

    @DexIgnore
    public fq4$h$b(MusicControlComponent.h hVar, LiveData liveData) {
        this.a = hVar;
        this.b = liveData;
    }

    @DexIgnore
    public final void onChanged(Y y) {
        this.a.b(new lc6(this.b.a(), y));
    }
}
