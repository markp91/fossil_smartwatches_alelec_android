package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w93 {
    @DexIgnore
    public /* final */ k42 a;
    @DexIgnore
    public long b;

    @DexIgnore
    public w93(k42 k42) {
        w12.a(k42);
        this.a = k42;
    }

    @DexIgnore
    public final void a() {
        this.b = this.a.c();
    }

    @DexIgnore
    public final void b() {
        this.b = 0;
    }

    @DexIgnore
    public final boolean a(long j) {
        if (this.b != 0 && this.a.c() - this.b < 3600000) {
            return false;
        }
        return true;
    }
}
