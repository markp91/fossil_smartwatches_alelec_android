package com.fossil;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x12 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<x12> CREATOR; // = new g32();
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Account b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ GoogleSignInAccount d;

    @DexIgnore
    public x12(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.a = i;
        this.b = account;
        this.c = i2;
        this.d = googleSignInAccount;
    }

    @DexIgnore
    public int B() {
        return this.c;
    }

    @DexIgnore
    public GoogleSignInAccount C() {
        return this.d;
    }

    @DexIgnore
    public Account p() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, (Parcelable) p(), i, false);
        g22.a(parcel, 3, B());
        g22.a(parcel, 4, (Parcelable) C(), i, false);
        g22.a(parcel, a2);
    }

    @DexIgnore
    public x12(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
