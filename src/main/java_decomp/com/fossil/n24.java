package com.fossil;

import com.fossil.m24;
import com.fossil.mc6;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.CoroutineUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n24 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements m24.e<P, E> {
        @DexIgnore
        public /* final */ /* synthetic */ lk6 a;
        @DexIgnore
        public /* final */ /* synthetic */ CoroutineUseCase b;

        @DexIgnore
        public a(lk6 lk6, CoroutineUseCase coroutineUseCase, CoroutineUseCase.b bVar) {
            this.a = lk6;
            this.b = coroutineUseCase;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(P p) {
            wg6.b(p, "responseValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "success continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                lk6 lk6 = this.a;
                mc6.a aVar = mc6.Companion;
                lk6.resumeWith(mc6.m1constructorimpl(p));
            }
        }

        @DexIgnore
        public void a(E e) {
            wg6.b(e, "errorValue");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String c = this.b.c();
            local.d(c, "fail continuation " + this.a.hashCode());
            if (this.a.isActive()) {
                lk6 lk6 = this.a;
                mc6.a aVar = mc6.Companion;
                lk6.resumeWith(mc6.m1constructorimpl(e));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.CoroutineUseCase$e, com.fossil.n24$a] */
    public static final <R extends m24.b, P extends m24.d, E extends m24.a> Object a(m24<? super R, P, E> m24, R r, xe6<? super m24.c> xe6) {
        mk6 mk6 = new mk6(ef6.a(xe6), 1);
        m24.a(r, new a(mk6, m24, r));
        Object e = mk6.e();
        if (e == ff6.a()) {
            nf6.c(xe6);
        }
        return e;
    }
}
