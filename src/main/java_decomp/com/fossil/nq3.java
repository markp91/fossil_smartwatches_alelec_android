package com.fossil;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nq3 extends tp3 {
    @DexIgnore
    public /* final */ Set<Class<?>> a;
    @DexIgnore
    public /* final */ Set<Class<?>> b;
    @DexIgnore
    public /* final */ Set<Class<?>> c;
    @DexIgnore
    public /* final */ Set<Class<?>> d;
    @DexIgnore
    public /* final */ Set<Class<?>> e;
    @DexIgnore
    public /* final */ xp3 f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements qq3 {
        @DexIgnore
        public a(Set<Class<?>> set, qq3 qq3) {
        }
    }

    @DexIgnore
    public nq3(wp3<?> wp3, xp3 xp3) {
        HashSet hashSet = new HashSet();
        HashSet hashSet2 = new HashSet();
        HashSet hashSet3 = new HashSet();
        HashSet hashSet4 = new HashSet();
        for (gq3 next : wp3.a()) {
            if (next.b()) {
                if (next.d()) {
                    hashSet3.add(next.a());
                } else {
                    hashSet.add(next.a());
                }
            } else if (next.d()) {
                hashSet4.add(next.a());
            } else {
                hashSet2.add(next.a());
            }
        }
        if (!wp3.d().isEmpty()) {
            hashSet.add(qq3.class);
        }
        this.a = Collections.unmodifiableSet(hashSet);
        this.b = Collections.unmodifiableSet(hashSet2);
        this.c = Collections.unmodifiableSet(hashSet3);
        this.d = Collections.unmodifiableSet(hashSet4);
        this.e = wp3.d();
        this.f = xp3;
    }

    @DexIgnore
    public <T> T a(Class<T> cls) {
        if (this.a.contains(cls)) {
            T a2 = this.f.a(cls);
            if (!cls.equals(qq3.class)) {
                return a2;
            }
            return new a(this.e, (qq3) a2);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency %s.", new Object[]{cls}));
    }

    @DexIgnore
    public <T> ys3<T> b(Class<T> cls) {
        if (this.b.contains(cls)) {
            return this.f.b(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<%s>.", new Object[]{cls}));
    }

    @DexIgnore
    public <T> ys3<Set<T>> c(Class<T> cls) {
        if (this.d.contains(cls)) {
            return this.f.c(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Provider<Set<%s>>.", new Object[]{cls}));
    }

    @DexIgnore
    public <T> Set<T> d(Class<T> cls) {
        if (this.c.contains(cls)) {
            return this.f.d(cls);
        }
        throw new IllegalArgumentException(String.format("Attempting to request an undeclared dependency Set<%s>.", new Object[]{cls}));
    }
}
