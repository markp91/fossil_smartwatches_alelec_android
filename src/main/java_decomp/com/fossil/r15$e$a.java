package com.fossil;

import com.fossil.l35;
import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.NotificationContactsAndAppsAssignedPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r15$e$a implements y24.d<l35.c, y24.a> {
    @DexIgnore
    public /* final */ /* synthetic */ NotificationContactsAndAppsAssignedPresenter.e a;

    @DexIgnore
    public r15$e$a(NotificationContactsAndAppsAssignedPresenter.e eVar) {
        this.a = eVar;
    }

    @DexIgnore
    /* renamed from: a */
    public void onSuccess(l35.c cVar) {
        wg6.b(cVar, "successResponse");
        FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), ".Inside mSaveAllNotification onSuccess");
        this.a.a.o.d();
        NotificationContactsAndAppsAssignedPresenter.e eVar = this.a;
        if (eVar.d) {
            eVar.a.o.close();
        }
    }

    @DexIgnore
    public void a(y24.a aVar) {
        wg6.b(aVar, "errorResponse");
        FLogger.INSTANCE.getLocal().d(NotificationContactsAndAppsAssignedPresenter.w.a(), ".Inside mSaveAllNotification onError");
        this.a.a.o.d();
    }
}
