package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ge1 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ lc1 CREATOR; // = new lc1((qg6) null);
    @DexIgnore
    public /* final */ qn0 a;
    @DexIgnore
    public /* final */ dn1 b;
    @DexIgnore
    public /* final */ ip0 c;
    @DexIgnore
    public /* final */ ar0 d;
    @DexIgnore
    public /* final */ short e;

    @DexIgnore
    public ge1(qn0 qn0, dn1 dn1, ip0 ip0, ar0 ar0, short s) {
        this.a = qn0;
        this.b = dn1;
        this.c = ip0;
        this.d = ar0;
        this.e = s;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.HAND_ID, (Object) cw0.a((Enum<?>) this.a)), bm0.DIRECTION, (Object) cw0.a((Enum<?>) this.b)), bm0.MOVING_TYPE, (Object) cw0.a((Enum<?>) this.c)), bm0.SPEED, (Object) cw0.a((Enum<?>) this.d)), bm0.ROTATION, (Object) Short.valueOf(this.e));
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(ge1.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            ge1 ge1 = (ge1) obj;
            return this.a == ge1.a && this.b == ge1.b && this.c == ge1.c && this.d == ge1.d && this.e == ge1.e;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.animation.HandAnimation");
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return ((this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31)) * 31) + this.e;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
        if (parcel != null) {
            parcel.writeString(this.c.name());
        }
        if (parcel != null) {
            parcel.writeString(this.d.name());
        }
        if (parcel != null) {
            parcel.writeInt(this.e);
        }
    }
}
