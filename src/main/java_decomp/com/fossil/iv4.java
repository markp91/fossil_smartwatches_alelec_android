package com.fossil;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.manager.ThemeManager;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iv4 extends RecyclerView.g<b> implements Filterable {
    @DexIgnore
    public List<vx4> a; // = new ArrayList();
    @DexIgnore
    public int b;
    @DexIgnore
    public List<vx4> c;
    @DexIgnore
    public a d;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(AppWrapper appWrapper, boolean z);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ rf4 a;
        @DexIgnore
        public /* final */ /* synthetic */ iv4 b;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a implements View.OnClickListener {
            @DexIgnore
            public /* final */ /* synthetic */ b a;

            @DexIgnore
            public a(b bVar) {
                this.a = bVar;
            }

            @DexIgnore
            public final void onClick(View view) {
                int adapterPosition = this.a.getAdapterPosition();
                if (adapterPosition != -1) {
                    List c = this.a.b.c;
                    if (c != null) {
                        InstalledApp installedApp = ((AppWrapper) c.get(adapterPosition)).getInstalledApp();
                        Boolean isSelected = installedApp != null ? installedApp.isSelected() : null;
                        if (isSelected != null) {
                            boolean booleanValue = isSelected.booleanValue();
                            if (booleanValue) {
                                List c2 = this.a.b.c;
                                if (c2 == null) {
                                    wg6.a();
                                    throw null;
                                } else if (((AppWrapper) c2.get(adapterPosition)).getCurrentHandGroup() != this.a.b.b) {
                                    a d = this.a.b.d;
                                    if (d != null) {
                                        List c3 = this.a.b.c;
                                        if (c3 != null) {
                                            d.a((AppWrapper) c3.get(adapterPosition), booleanValue);
                                            return;
                                        } else {
                                            wg6.a();
                                            throw null;
                                        }
                                    } else {
                                        return;
                                    }
                                }
                            }
                            a d2 = this.a.b.d;
                            if (d2 != null) {
                                List c4 = this.a.b.c;
                                if (c4 != null) {
                                    d2.a((AppWrapper) c4.get(adapterPosition), !booleanValue);
                                } else {
                                    wg6.a();
                                    throw null;
                                }
                            }
                        } else {
                            wg6.a();
                            throw null;
                        }
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r2v3, types: [android.widget.CompoundButton, com.portfolio.platform.view.FlexibleSwitchCompat] */
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(iv4 iv4, rf4 rf4) {
            super(rf4.d());
            wg6.b(rf4, "binding");
            this.b = iv4;
            this.a = rf4;
            this.a.v.setOnClickListener(new a(this));
            String b2 = ThemeManager.l.a().b("nonBrandSeparatorLine");
            String b3 = ThemeManager.l.a().b(Explore.COLUMN_BACKGROUND);
            if (!TextUtils.isEmpty(b3)) {
                this.a.q.setBackgroundColor(Color.parseColor(b3));
            }
            if (!TextUtils.isEmpty(b2)) {
                this.a.u.setBackgroundColor(Color.parseColor(b2));
            }
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v9, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r7v2, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r7v4, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r0v17, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        /* JADX WARNING: type inference failed for: r4v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: type inference failed for: r7v11, types: [com.portfolio.platform.view.FlexibleTextView, android.widget.TextView, java.lang.Object] */
        public final void a(AppWrapper appWrapper) {
            wg6.b(appWrapper, "appWrapper");
            ImageView imageView = this.a.t;
            wg6.a((Object) imageView, "binding.ivAppIcon");
            jj4.a(imageView.getContext()).a(appWrapper.getUri()).a(new nz().c().a(ft.a).a(true)).c().a(this.a.t);
            Object r0 = this.a.r;
            wg6.a((Object) r0, "binding.ftvAppName");
            InstalledApp installedApp = appWrapper.getInstalledApp();
            r0.setText(installedApp != null ? installedApp.getTitle() : null);
            FlexibleSwitchCompat flexibleSwitchCompat = this.a.v;
            wg6.a((Object) flexibleSwitchCompat, "binding.swEnabled");
            InstalledApp installedApp2 = appWrapper.getInstalledApp();
            Boolean isSelected = installedApp2 != null ? installedApp2.isSelected() : null;
            if (isSelected != null) {
                flexibleSwitchCompat.setChecked(isSelected.booleanValue());
                if (appWrapper.getCurrentHandGroup() == 0 || appWrapper.getCurrentHandGroup() == this.b.b) {
                    Object r7 = this.a.s;
                    wg6.a((Object) r7, "binding.ftvAssigned");
                    r7.setText("");
                    Object r72 = this.a.s;
                    wg6.a((Object) r72, "binding.ftvAssigned");
                    r72.setVisibility(8);
                    return;
                }
                Object r02 = this.a.s;
                wg6.a((Object) r02, "binding.ftvAssigned");
                nh6 nh6 = nh6.a;
                String a2 = jm4.a((Context) PortfolioApp.get.instance(), 2131886150);
                wg6.a((Object) a2, "LanguageHelper.getString\u2026s_Text__AssignedToNumber)");
                Object[] objArr = {Integer.valueOf(appWrapper.getCurrentHandGroup())};
                String format = String.format(a2, Arrays.copyOf(objArr, objArr.length));
                wg6.a((Object) format, "java.lang.String.format(format, *args)");
                r02.setText(format);
                Object r73 = this.a.s;
                wg6.a((Object) r73, "binding.ftvAssigned");
                r73.setVisibility(0);
                FlexibleSwitchCompat flexibleSwitchCompat2 = this.a.v;
                wg6.a((Object) flexibleSwitchCompat2, "binding.swEnabled");
                flexibleSwitchCompat2.setChecked(false);
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c extends Filter {
        @DexIgnore
        public /* final */ /* synthetic */ iv4 a;

        @DexIgnore
        public c(iv4 iv4) {
            this.a = iv4;
        }

        @DexIgnore
        public Filter.FilterResults performFiltering(CharSequence charSequence) {
            String str;
            String title;
            wg6.b(charSequence, "constraint");
            Filter.FilterResults filterResults = new Filter.FilterResults();
            if (TextUtils.isEmpty(charSequence)) {
                filterResults.values = this.a.a;
            } else {
                ArrayList arrayList = new ArrayList();
                String obj = charSequence.toString();
                if (obj != null) {
                    String lowerCase = obj.toLowerCase();
                    wg6.a((Object) lowerCase, "(this as java.lang.String).toLowerCase()");
                    for (AppWrapper appWrapper : this.a.a) {
                        InstalledApp installedApp = appWrapper.getInstalledApp();
                        if (installedApp == null || (title = installedApp.getTitle()) == null) {
                            str = null;
                        } else if (title != null) {
                            str = title.toLowerCase();
                            wg6.a((Object) str, "(this as java.lang.String).toLowerCase()");
                        } else {
                            throw new rc6("null cannot be cast to non-null type java.lang.String");
                        }
                        if (str != null && yj6.a((CharSequence) str, (CharSequence) lowerCase, false, 2, (Object) null)) {
                            arrayList.add(appWrapper);
                        }
                    }
                    filterResults.values = arrayList;
                } else {
                    throw new rc6("null cannot be cast to non-null type java.lang.String");
                }
            }
            return filterResults;
        }

        @DexIgnore
        public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
            wg6.b(charSequence, "charSequence");
            wg6.b(filterResults, "results");
            this.a.c = (List) filterResults.values;
            this.a.notifyDataSetChanged();
        }
    }

    @DexIgnore
    public Filter getFilter() {
        return new c(this);
    }

    @DexIgnore
    public int getItemCount() {
        List<vx4> list = this.c;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        wg6.b(viewGroup, "parent");
        rf4 a2 = rf4.a(LayoutInflater.from(viewGroup.getContext()), viewGroup, false);
        wg6.a((Object) a2, "ItemAppHybridNotificatio\u2026.context), parent, false)");
        return new b(this, a2);
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        wg6.b(bVar, "holder");
        List<vx4> list = this.c;
        if (list != null) {
            bVar.a(list.get(i));
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final void a(List<vx4> list, int i) {
        wg6.b(list, "listAppWrapper");
        this.b = i;
        this.a = list;
        this.c = list;
        notifyDataSetChanged();
    }

    @DexIgnore
    public final void a(a aVar) {
        wg6.b(aVar, "listener");
        this.d = aVar;
    }
}
