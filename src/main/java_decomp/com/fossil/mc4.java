package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mc4 extends lc4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j K; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray L; // = new SparseIntArray();
    @DexIgnore
    public long J;

    /*
    static {
        L.put(2131362561, 1);
        L.put(2131362442, 2);
        L.put(2131362978, 3);
        L.put(2131362369, 4);
        L.put(2131362984, 5);
        L.put(2131362038, 6);
        L.put(2131362372, 7);
        L.put(2131362678, 8);
        L.put(2131362373, 9);
        L.put(2131363270, 10);
        L.put(2131362370, 11);
        L.put(2131362677, 12);
        L.put(2131362371, 13);
        L.put(2131363271, 14);
        L.put(2131362411, 15);
        L.put(2131362685, 16);
        L.put(2131362412, 17);
        L.put(2131363272, 18);
        L.put(2131362409, 19);
        L.put(2131362064, 20);
        L.put(2131362376, 21);
        L.put(2131362985, 22);
        L.put(2131362041, 23);
        L.put(2131362368, 24);
        L.put(2131362431, 25);
        L.put(2131362990, 26);
        L.put(2131362075, 27);
        L.put(2131362410, 28);
        L.put(2131362303, 29);
        L.put(2131362982, 30);
        L.put(2131362014, 31);
        L.put(2131362427, 32);
        L.put(2131362223, 33);
    }
    */

    @DexIgnore
    public mc4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 34, K, L));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.J = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.J != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.J = 1;
        }
        g();
    }

    @DexIgnore
    public mc4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[31], objArr[6], objArr[23], objArr[20], objArr[27], objArr[33], objArr[29], objArr[24], objArr[4], objArr[11], objArr[13], objArr[7], objArr[9], objArr[21], objArr[19], objArr[28], objArr[15], objArr[17], objArr[32], objArr[25], objArr[2], objArr[1], objArr[12], objArr[8], objArr[16], objArr[0], objArr[3], objArr[30], objArr[5], objArr[22], objArr[26], objArr[10], objArr[14], objArr[18]);
        this.J = -1;
        this.B.setTag((Object) null);
        a(view);
        f();
    }
}
