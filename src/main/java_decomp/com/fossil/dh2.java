package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dh2 implements Parcelable.Creator<ch2> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        sw2 sw2 = ch2.e;
        List<d12> list = ch2.d;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                sw2 = (sw2) f22.a(parcel, a, sw2.CREATOR);
            } else if (a2 == 2) {
                list = f22.c(parcel, a, d12.CREATOR);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                str = f22.e(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new ch2(sw2, list, str);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ch2[i];
    }
}
