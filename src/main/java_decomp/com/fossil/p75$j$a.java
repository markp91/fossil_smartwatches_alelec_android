package com.fossil;

import com.portfolio.platform.data.model.Category;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.WatchAppsPresenter$start$1$allCategory$1", f = "WatchAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class p75$j$a extends sf6 implements ig6<il6, xe6<? super List<? extends Category>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppsPresenter.j this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public p75$j$a(WatchAppsPresenter.j jVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = jVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        p75$j$a p75_j_a = new p75$j$a(this.this$0, xe6);
        p75_j_a.p$ = (il6) obj;
        return p75_j_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((p75$j$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.t.getAllCategories();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
