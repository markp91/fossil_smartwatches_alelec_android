package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class q94 extends p94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362820, 1);
        x.put(2131362442, 2);
        x.put(2131363327, 3);
        x.put(2131362511, 4);
        x.put(2131362210, 5);
    }
    */

    @DexIgnore
    public q94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 6, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public q94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[5], objArr[2], objArr[4], objArr[1], objArr[3]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
