package com.fossil;

import android.net.Uri;
import android.util.Log;
import com.google.android.gms.common.data.DataHolder;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oe3 extends v02 implements yd3 {
    @DexIgnore
    public /* final */ int d;

    @DexIgnore
    public oe3(DataHolder dataHolder, int i, int i2) {
        super(dataHolder, i);
        this.d = i2;
    }

    @DexIgnore
    public final Map<String, zd3> a() {
        HashMap hashMap = new HashMap(this.d);
        for (int i = 0; i < this.d; i++) {
            ne3 ne3 = new ne3(this.a, this.b + i);
            if (ne3.a() != null) {
                hashMap.put(ne3.a(), ne3);
            }
        }
        return hashMap;
    }

    @DexIgnore
    public final byte[] b() {
        return a("data");
    }

    @DexIgnore
    public final Uri c() {
        return Uri.parse(c("path"));
    }

    @DexIgnore
    public final String toString() {
        Object obj;
        boolean isLoggable = Log.isLoggable("DataItem", 3);
        byte[] b = b();
        Map<String, zd3> a = a();
        StringBuilder sb = new StringBuilder("DataItemRef{ ");
        String valueOf = String.valueOf(c());
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 4);
        sb2.append("uri=");
        sb2.append(valueOf);
        sb.append(sb2.toString());
        if (b == null) {
            obj = "null";
        } else {
            obj = Integer.valueOf(b.length);
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 9);
        sb3.append(", dataSz=");
        sb3.append(valueOf2);
        sb.append(sb3.toString());
        int size = a.size();
        StringBuilder sb4 = new StringBuilder(23);
        sb4.append(", numAssets=");
        sb4.append(size);
        sb.append(sb4.toString());
        if (isLoggable && !a.isEmpty()) {
            sb.append(", assets=[");
            String str = "";
            for (Map.Entry next : a.entrySet()) {
                String str2 = (String) next.getKey();
                String id = ((zd3) next.getValue()).getId();
                StringBuilder sb5 = new StringBuilder(str.length() + 2 + String.valueOf(str2).length() + String.valueOf(id).length());
                sb5.append(str);
                sb5.append(str2);
                sb5.append(": ");
                sb5.append(id);
                sb.append(sb5.toString());
                str = ", ";
            }
            sb.append("]");
        }
        sb.append(" }");
        return sb.toString();
    }
}
