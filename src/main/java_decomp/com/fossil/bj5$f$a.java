package com.fossil;

import com.fossil.ej5;
import com.portfolio.platform.data.model.room.sleep.MFSleepSession;
import com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.details.sleep.SleepDetailPresenter$showHeartRateSleepSessionChart$1$data$1", f = "SleepDetailPresenter.kt", l = {}, m = "invokeSuspend")
public final class bj5$f$a extends sf6 implements ig6<il6, xe6<? super ArrayList<ej5.a>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ SleepDetailPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bj5$f$a(SleepDetailPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        bj5$f$a bj5_f_a = new bj5$f$a(this.this$0, xe6);
        bj5_f_a.p$ = (il6) obj;
        return bj5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((bj5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            SleepDetailPresenter sleepDetailPresenter = this.this$0.this$0;
            return sleepDetailPresenter.a((List<MFSleepSession>) sleepDetailPresenter.o);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
