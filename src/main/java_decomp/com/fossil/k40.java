package com.fossil;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattServer;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.login.LoginStatusClient;
import com.facebook.places.PlaceManager;
import com.facebook.places.model.PlaceFields;
import com.fossil.q40;
import com.fossil.r40;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k40 {
    @DexIgnore
    public static /* final */ Handler a;
    @DexIgnore
    public static /* final */ BluetoothAdapter b; // = BluetoothAdapter.getDefaultAdapter();
    @DexIgnore
    public static f c;
    @DexIgnore
    public static /* final */ Hashtable<String, v40<r40, km1>> d; // = new Hashtable<>();
    @DexIgnore
    public static /* final */ ScanCallback e; // = new i();
    @DexIgnore
    public static /* final */ LinkedHashMap<b, o40> f; // = new LinkedHashMap<>();
    @DexIgnore
    public static /* final */ BroadcastReceiver g; // = new g();
    @DexIgnore
    public static /* final */ BroadcastReceiver h; // = new h();
    @DexIgnore
    public static boolean i;
    @DexIgnore
    @SuppressLint({"StaticFieldLeak"})
    public static fu0 j;
    @DexIgnore
    public static a k;
    @DexIgnore
    public static /* final */ k40 l; // = new k40();

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(k40 k40, c cVar, c cVar2);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void onDeviceFound(q40 q40, int i);

        @DexIgnore
        void onStartScanFailed(l40 l40);
    }

    @DexIgnore
    public enum c {
        DISABLED(10),
        ENABLING(11),
        ENABLED(12),
        DISABLING(13);
        
        @DexIgnore
        public static /* final */ a c; // = null;
        @DexIgnore
        public /* final */ int a;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
            }

            @DexIgnore
            public final c a(int i) {
                c cVar;
                c[] values = c.values();
                int length = values.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        cVar = null;
                        break;
                    }
                    cVar = values[i2];
                    if (cVar.a() == i) {
                        break;
                    }
                    i2++;
                }
                return cVar != null ? cVar : c.DISABLED;
            }
        }

        /*
        static {
            c = new a((qg6) null);
        }
        */

        @DexIgnore
        public c(int i) {
            this.a = i;
        }

        @DexIgnore
        public final int a() {
            return this.a;
        }
    }

    @DexIgnore
    public enum d {
        INCOMPLETE_16_BIT_SERVICE_CLASS_UUID((byte) 2),
        COMPLETE_16_BIT_SERVICE_CLASS_UUID((byte) 3),
        INCOMPLETE_32_BIT_SERVICE_CLASS_UUID((byte) 4),
        COMPLETE_32_BIT_SERVICE_CLASS_UUID((byte) 5),
        INCOMPLETE_128_BIT_SERVICE_CLASS_UUID((byte) 6),
        COMPLETE_128_BIT_SERVICE_CLASS_UUID((byte) 7),
        COMPLETE_LOCAL_NAME((byte) 9),
        SERVICE_DATA_16_BIT_SERVICE_CLASS_UUID((byte) 22),
        MANUFACTURER_SPECIFIC_DATA((byte) 255);
        
        @DexIgnore
        public static /* final */ qg0 l; // = null;
        @DexIgnore
        public /* final */ byte a;

        /*
        static {
            l = new qg0((qg6) null);
        }
        */

        @DexIgnore
        public d(byte b) {
            this.a = b;
        }
    }

    @DexIgnore
    public enum e {
        START_SCAN,
        START_SCAN_FAILED,
        STOP_SCAN,
        DEVICE_RETRIEVED,
        DEVICE_FOUND,
        GET_GATT_CONNECTED_DEVICES,
        GET_HID_CONNECTED_DEVICES,
        BOND_STATE_CHANGED,
        BLUETOOTH_STATE_CHANGED
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f implements Runnable {
        @DexIgnore
        public boolean a;

        @DexIgnore
        public final JSONArray a(List<BluetoothDevice> list) {
            JSONArray jSONArray = new JSONArray();
            for (BluetoothDevice bluetoothDevice : list) {
                JSONObject jSONObject = new JSONObject();
                bm0 bm0 = bm0.NAME;
                Object name = bluetoothDevice.getName();
                if (name == null) {
                    name = JSONObject.NULL;
                }
                jSONArray.put(cw0.a(cw0.a(cw0.a(jSONObject, bm0, name), bm0.MAC_ADDRESS, (Object) bluetoothDevice.getAddress()), bm0.CURRENT_BOND_STATE, (Object) mw0.e.a(bluetoothDevice.getBondState())));
            }
            return jSONArray;
        }

        @DexIgnore
        public void run() {
            if (!this.a) {
                oa1 oa1 = oa1.a;
                List<BluetoothDevice> a2 = k40.l.a();
                oa1 oa12 = oa1.a;
                new Object[1][0] = yd6.a(a2, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, hi0.a, 31, (Object) null);
                qs0.h.a(new nn0(cw0.a((Enum<?>) e.GET_GATT_CONNECTED_DEVICES), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.GATT_CONNECTED_DEVICES, (Object) a(a2)), 448));
                List<BluetoothDevice> b = k40.l.b();
                oa1 oa13 = oa1.a;
                new Object[1][0] = yd6.a(b, (CharSequence) null, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, bk0.a, 31, (Object) null);
                qs0.h.a(new nn0(cw0.a((Enum<?>) e.GET_HID_CONNECTED_DEVICES), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.HID_CONNECTED_DEVICES, (Object) a(b)), 448));
                ArrayList arrayList = new ArrayList(a2);
                arrayList.addAll(b);
                for (BluetoothDevice bluetoothDevice : yd6.c(arrayList)) {
                    if (bluetoothDevice.getType() != 1) {
                        ut0 ut0 = ut0.c;
                        String address = bluetoothDevice.getAddress();
                        wg6.a(address, "bluetoothDevice.address");
                        String b2 = ut0.b(address);
                        if (b2 != null) {
                            ii1 a3 = ut0.c.a(bluetoothDevice, b2);
                            k40 k40 = k40.l;
                            if (k40.b(a3)) {
                                k40.a(a3, 0);
                            }
                        } else {
                            k40 k402 = k40.l;
                            if (k40.d.get(bluetoothDevice.getAddress()) == null) {
                                ii1 a4 = ut0.c.a(bluetoothDevice, "");
                                d41 d41 = d41.AUTO_CONNECT;
                                fm0.f.f();
                                HashMap a5 = he6.a(new lc6[]{qc6.a(d41, false), qc6.a(d41.CONNECTION_TIME_OUT, 30000L)});
                                oa1 oa14 = oa1.a;
                                v40<r40, km1> b3 = a4.b((HashMap<d41, Object>) a5);
                                k40 k403 = k40.l;
                                k40.d.put(bluetoothDevice.getAddress(), b3);
                                b3.c((hg6<? super r40, cd6>) new wl0(bluetoothDevice, a4)).b(new pn0(bluetoothDevice));
                            }
                        }
                    }
                }
                k40 k404 = k40.l;
                k40.a.postDelayed(this, LoginStatusClient.DEFAULT_TOAST_DURATION_MS);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            String action;
            if (context != null && intent != null && (action = intent.getAction()) != null && action.hashCode() == -1530327060 && action.equals("android.bluetooth.adapter.action.STATE_CHANGED")) {
                k40.l.a(c.c.a(intent.getIntExtra("android.bluetooth.adapter.extra.PREVIOUS_STATE", RecyclerView.UNDEFINED_DURATION)), c.c.a(intent.getIntExtra("android.bluetooth.adapter.extra.STATE", RecyclerView.UNDEFINED_DURATION)));
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends BroadcastReceiver {
        @DexIgnore
        public void onReceive(Context context, Intent intent) {
            if (context != null && intent != null) {
                BluetoothDevice bluetoothDevice = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                int intExtra = intent.getIntExtra("android.bluetooth.device.extra.PREVIOUS_BOND_STATE", -1);
                int intExtra2 = intent.getIntExtra("android.bluetooth.device.extra.BOND_STATE", -1);
                if (bluetoothDevice != null) {
                    k40.l.a(bluetoothDevice, intExtra, intExtra2);
                }
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i extends ScanCallback {
        @DexIgnore
        public void onScanFailed(int i) {
            k40.l.a(new l40(m40.e.a(i)));
        }

        /* JADX WARNING: Code restructure failed: missing block: B:2:0x0003, code lost:
            r1 = r25.getScanRecord();
         */
        @DexIgnore
        public void onScanResult(int i, ScanResult scanResult) {
            ScanRecord scanRecord;
            byte[] bytes = (scanResult == null || scanRecord == null) ? null : scanRecord.getBytes();
            BluetoothDevice device = scanResult != null ? scanResult.getDevice() : null;
            if (bytes != null && device != null) {
                if (k40.l.d(bytes)) {
                    String f = k40.l.f(bytes);
                    if (r40.t.a(f)) {
                        ii1 a = ut0.c.a(device, f);
                        if (k40.l.b(a)) {
                            k40.l.a(a, scanResult.getRssi());
                        }
                    }
                } else if (k40.l.e(bytes)) {
                    ii1 a2 = ut0.c.a(device, "");
                    a2.r = r40.a(a2.r, k40.l.a(bytes), (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (w40) null, (w40) null, (w40) null, (LinkedHashMap) null, (LinkedHashMap) null, (r40.a) null, (s60[]) null, (w40) null, (String) null, (w40) null, cw0.a(k40.l.b(bytes), (String) null, 1), 131070);
                    a2.r.a(s40.WEAR_OS);
                    k40.l.a(a2, scanResult.getRssi());
                }
            }
        }
    }

    /*
    static {
        Looper myLooper = Looper.myLooper();
        if (myLooper == null) {
            myLooper = Looper.getMainLooper();
        }
        if (myLooper != null) {
            a = new Handler(myLooper);
            return;
        }
        wg6.a();
        throw null;
    }
    */

    @DexIgnore
    public final void a(List<ScanFilter> list, ScanSettings scanSettings) {
        BluetoothAdapter bluetoothAdapter = b;
        BluetoothLeScanner bluetoothLeScanner = bluetoothAdapter != null ? bluetoothAdapter.getBluetoothLeScanner() : null;
        if (bluetoothLeScanner == null) {
            a(new l40(m40.UNKNOWN_ERROR));
        } else {
            bluetoothLeScanner.startScan(list, scanSettings, e);
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> b() {
        return s81.d.a();
    }

    @DexIgnore
    public final void c() {
        int i2;
        b[] bVarArr;
        synchronized (f) {
            Set<b> keySet = f.keySet();
            wg6.a(keySet, "activeLeScanCallbacks.keys");
            Object[] array = keySet.toArray(new b[0]);
            if (array != null) {
                bVarArr = (b[]) array;
                cd6 cd6 = cd6.a;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        for (b a2 : bVarArr) {
            l.a(a2);
        }
    }

    @DexIgnore
    public final boolean d(byte[] bArr) {
        HashMap<d, String[]> c2 = c(bArr);
        String[] strArr = c2.get(d.INCOMPLETE_128_BIT_SERVICE_CLASS_UUID);
        if (strArr != null && nd6.a(strArr, mi0.A.x())) {
            return true;
        }
        String[] strArr2 = c2.get(d.COMPLETE_128_BIT_SERVICE_CLASS_UUID);
        if (strArr2 == null || !nd6.a(strArr2, mi0.A.x())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final boolean e(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = cw0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (d.l.a(bArr[i3]) == d.MANUFACTURER_SPECIFIC_DATA && b2 >= 3) {
                ByteBuffer order = ByteBuffer.wrap(md6.a(md6.a(bArr, i3, i3 + b2), 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                wg6.a(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == 224) {
                    return true;
                }
            }
            i2 = b2 + i3;
        }
        return false;
    }

    @DexIgnore
    public final String f(byte[] bArr) {
        byte[] bArr2;
        byte b2;
        if (bArr == null) {
            return new String();
        }
        int i2 = 0;
        while (true) {
            if (i2 >= bArr.length) {
                break;
            }
            int i3 = i2 + 1;
            short b3 = cw0.b(bArr[i2]);
            if (b3 != 0 && (b2 = bArr[i3]) != ((byte) 0)) {
                if (b2 == -1 && b3 >= 13) {
                    int i4 = i3 + 1 + 2;
                    bArr2 = Arrays.copyOfRange(bArr, i4, i4 + 10);
                    wg6.a(bArr2, "Arrays.copyOfRange(adver\u2026RER_SERIAL_NUMBER_LENGTH)");
                    break;
                }
                i2 = b3 + i3;
            } else {
                break;
            }
        }
        bArr2 = new byte[0];
        return new String(bArr2, ej6.a);
    }

    @DexIgnore
    public final void b(b bVar) {
        qs0.h.a(new nn0(cw0.a((Enum<?>) e.STOP_SCAN), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(new JSONObject(), bm0.SCAN_CALLBACK, (Object) cw0.a(bVar.hashCode())), 448));
        a(bVar);
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0067  */
    public final q40 a(String str, String str2) throws IllegalArgumentException {
        boolean z;
        ii1 ii1;
        Object obj;
        ii1 ii12;
        String str3 = str;
        String str4 = str2;
        oa1 oa1 = oa1.a;
        Object[] objArr = {str3, str4};
        JSONObject a2 = cw0.a(cw0.a(new JSONObject(), bm0.SERIAL_NUMBER, (Object) str3), bm0.MAC_ADDRESS, (Object) str4);
        if (!do1.b.a()) {
            qs0.h.a(new nn0(cw0.a((Enum<?>) e.DEVICE_RETRIEVED), og0.CENTRAL_EVENT, "", "", "", false, (String) null, (r40) null, (bw0) null, cw0.a(a2, bm0.ERROR_DETAIL, (Object) "SdkLogManager.init must be call with application context first."), 448));
            oa1 oa12 = oa1.a;
            throw new IllegalArgumentException("SdkLogManager.init must be call with application context first.");
        } else if (r40.t.a(str3)) {
            if (s40.c.a(str3) != s40.UNKNOWN) {
                String a3 = ut0.c.a(str3);
                oa1 oa13 = oa1.a;
                new Object[1][0] = a3;
                if (BluetoothAdapter.checkBluetoothAddress(a3)) {
                    BluetoothAdapter bluetoothAdapter = b;
                    BluetoothDevice remoteDevice = bluetoothAdapter != null ? bluetoothAdapter.getRemoteDevice(a3) : null;
                    if (remoteDevice != null) {
                        ii12 = ut0.c.a(remoteDevice, str3);
                        z = true;
                        if (ii12 == null) {
                            oa1 oa14 = oa1.a;
                            if (BluetoothAdapter.checkBluetoothAddress(str2)) {
                                BluetoothAdapter bluetoothAdapter2 = b;
                                BluetoothDevice remoteDevice2 = bluetoothAdapter2 != null ? bluetoothAdapter2.getRemoteDevice(str4) : null;
                                if (remoteDevice2 != null) {
                                    ii1 = ut0.c.a(remoteDevice2, str3);
                                }
                            } else {
                                oa1 oa15 = oa1.a;
                                new Object[1][0] = str4;
                            }
                        }
                        ii1 = ii12;
                    }
                }
                ii12 = null;
                z = false;
                if (ii12 == null) {
                }
                ii1 = ii12;
            } else {
                ii1 = null;
                z = false;
            }
            if (ii1 != null && !b(ii1)) {
                oa1 oa16 = oa1.a;
                new Object[1][0] = a(ii1);
                ii1 = null;
            }
            cw0.a(a2, bm0.IS_CACHED, (Object) Boolean.valueOf(z));
            bm0 bm0 = bm0.DEVICE;
            if (ii1 != null) {
                obj = a(ii1);
            } else {
                obj = JSONObject.NULL;
            }
            cw0.a(a2, bm0, obj);
            qs0.h.a(new nn0(cw0.a((Enum<?>) e.DEVICE_RETRIEVED), og0.CENTRAL_EVENT, "", "", "", ii1 != null, (String) null, (r40) null, (bw0) null, a2, 448));
            oa1 oa17 = oa1.a;
            new Object[1][0] = ii1 != null ? a(ii1) : "null";
            return ii1;
        } else {
            qs0 qs0 = qs0.h;
            nn0 nn0 = r6;
            nn0 nn02 = new nn0(cw0.a((Enum<?>) e.DEVICE_RETRIEVED), og0.CENTRAL_EVENT, "", "", "", false, (String) null, (r40) null, (bw0) null, cw0.a(a2, bm0.ERROR_DETAIL, (Object) "Invalid serial number."), 448);
            qs0.a(nn0);
            oa1 oa18 = oa1.a;
            new Object[1][0] = str3;
            throw new IllegalArgumentException("serialNumber(" + str3 + ") is invalid.");
        }
    }

    @DexIgnore
    public final c d() {
        if (b == null) {
            return c.DISABLED;
        }
        return c.c.a(b.getState());
    }

    @DexIgnore
    public final boolean b(ii1 ii1) {
        return i || ii1.r.getDeviceType() != s40.UNKNOWN;
    }

    @DexIgnore
    public final HashMap<d, String[]> c(byte[] bArr) {
        int i2;
        int i3;
        HashMap<d, String[]> hashMap = new HashMap<>();
        int i4 = 0;
        while (i4 < bArr.length) {
            int i5 = i4 + 1;
            short b2 = cw0.b((byte) (bArr[i4] & ((byte) 255)));
            if (b2 != 0 && (i2 = i5 + 1) <= bArr.length) {
                d a2 = d.l.a(bArr[i5]);
                if (a2 != null) {
                    switch (hp0.a[a2.ordinal()]) {
                        case 1:
                        case 2:
                            i3 = 2;
                            break;
                        case 3:
                        case 4:
                            i3 = 4;
                            break;
                        case 5:
                        case 6:
                            i3 = 16;
                            break;
                    }
                }
                i3 = 0;
                if (a2 != null && i3 > 0) {
                    byte[] copyOfRange = Arrays.copyOfRange(bArr, i2, i5 + b2);
                    ArrayList arrayList = new ArrayList();
                    String[] strArr = hashMap.get(a2);
                    if (strArr != null) {
                        wg6.a(strArr, "currentServiceUUIDs");
                        vd6.a(arrayList, strArr);
                    }
                    wg6.a(copyOfRange, "serviceUUIDRaw");
                    byte[][] a3 = cw0.a(copyOfRange, i3);
                    ArrayList arrayList2 = new ArrayList(a3.length);
                    for (byte[] c2 : a3) {
                        arrayList2.add(cw0.a(nd6.c(c2), (String) null, 1));
                    }
                    arrayList.addAll(arrayList2);
                    Object[] array = arrayList.toArray(new String[0]);
                    if (array != null) {
                        hashMap.put(a2, array);
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                    }
                }
                i4 = b2 + i5;
            }
            return hashMap;
        }
        return hashMap;
    }

    @DexIgnore
    public final byte[] b(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = cw0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                break;
            }
            if (d.l.a(bArr[i3]) == d.SERVICE_DATA_16_BIT_SERVICE_CLASS_UUID && b2 >= 3) {
                byte[] a2 = md6.a(bArr, i3, i3 + b2);
                ByteBuffer order = ByteBuffer.wrap(md6.a(a2, 1, 3)).order(ByteOrder.LITTLE_ENDIAN);
                wg6.a(order, "ByteBuffer.wrap(data.cop\u2026(ByteOrder.LITTLE_ENDIAN)");
                if (order.getShort() == -468) {
                    return md6.a(a2, 3, b2);
                }
            }
            i2 = b2 + i3;
        }
        return new byte[0];
    }

    @DexIgnore
    public final void b(Context context) {
        if (j == null) {
            j = new fu0(context);
        }
        fu0 fu0 = j;
        if (fu0 != null) {
            hu0 hu0 = new hu0(fu0.g);
            if (fu0.c) {
                BluetoothGattServer bluetoothGattServer = fu0.a;
                if (bluetoothGattServer != null && bluetoothGattServer.addService(hu0)) {
                    fu0.b.add(hu0);
                }
            } else {
                fu0.b.add(hu0);
            }
            if (!fu0.c) {
                cy0.d.a(new ms0(fu0));
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c2  */
    public final void a(o40 o40, b bVar) throws IllegalStateException {
        boolean z;
        boolean z2;
        boolean z3;
        o40 o402 = o40;
        b bVar2 = bVar;
        qs0 qs0 = qs0.h;
        nn0 nn0 = r4;
        nn0 nn02 = new nn0(cw0.a((Enum<?>) e.START_SCAN), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.SCAN_FILTER, (Object) o40.a()), bm0.SCAN_CALLBACK, (Object) cw0.a(bVar.hashCode())), 448);
        qs0.a(nn0);
        Context a2 = gk0.f.a();
        if (a2 != null) {
            if (Build.VERSION.SDK_INT >= 23) {
                if (!(ne1.a.a(a2, new String[]{"android.permission.ACCESS_COARSE_LOCATION"}) || ne1.a.a(a2, new String[]{"android.permission.ACCESS_FINE_LOCATION"}))) {
                    a(bVar2, new l40(m40.LOCATION_PERMISSION_NOT_GRANTED));
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= 23) {
                LocationManager locationManager = (LocationManager) a2.getSystemService(PlaceFields.LOCATION);
                if (locationManager != null) {
                    if (Build.VERSION.SDK_INT >= 28) {
                        z = locationManager.isLocationEnabled();
                    } else {
                        try {
                            z2 = locationManager.isProviderEnabled("gps");
                        } catch (SecurityException e2) {
                            qs0.h.a(e2);
                            z2 = false;
                        }
                        try {
                            z3 = locationManager.isProviderEnabled("network");
                        } catch (SecurityException e3) {
                            qs0.h.a(e3);
                            z3 = false;
                        }
                        if (z2 || z3) {
                            z = true;
                        }
                    }
                    if (!z) {
                        a(bVar2, new l40(m40.LOCATION_SERVICE_NOT_ENABLED));
                        return;
                    }
                }
                z = false;
                if (!z) {
                }
            }
            qs0.h.a(new nn0(cw0.a((Enum<?>) e.START_SCAN), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.SCAN_FILTER, (Object) o40.a()), bm0.SCAN_CALLBACK, (Object) cw0.a(bVar.hashCode())), 448));
            a(bVar2, o40);
            return;
        }
        qs0 qs02 = qs0.h;
        nn0 nn03 = r6;
        nn0 nn04 = new nn0(cw0.a((Enum<?>) e.START_SCAN_FAILED), og0.CENTRAL_EVENT, "", "", "", false, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SCAN_FILTER, (Object) o40.a()), bm0.SCAN_CALLBACK, (Object) cw0.a(bVar.hashCode())), bm0.ERROR_DETAIL, (Object) "SdkLogManager.init must be call with application context first."), 448);
        qs02.a(nn03);
        oa1 oa1 = oa1.a;
        Object[] objArr = {p40.a(o40, 0, 1, (Object) null), cw0.a(bVar.hashCode())};
        throw new IllegalArgumentException("SdkLogManager.init must be call with application context first.");
    }

    @DexIgnore
    public final void a(b bVar, o40 o40) {
        oa1 oa1 = oa1.a;
        boolean z = false;
        Object[] objArr = {p40.a(o40, 0, 1, (Object) null), cw0.a(bVar.hashCode())};
        if (d() != c.ENABLED) {
            a(bVar, new l40(m40.BLUETOOTH_OFF));
            return;
        }
        synchronized (f) {
            f.put(bVar, o40);
            if (f.size() == 1) {
                z = true;
            }
            cd6 cd6 = cd6.a;
        }
        if (z) {
            ArrayList arrayList = new ArrayList();
            ScanSettings build = new ScanSettings.Builder().setScanMode(2).build();
            wg6.a(build, "ScanSettings.Builder()\n \u2026MODE_LOW_LATENCY).build()");
            a((List<ScanFilter>) arrayList, build);
            c = new f();
            f fVar = c;
            if (fVar != null) {
                fVar.run();
            }
        }
    }

    @DexIgnore
    public final void a(b bVar) {
        boolean z;
        BluetoothLeScanner bluetoothLeScanner;
        oa1 oa1 = oa1.a;
        new Object[1][0] = cw0.a(bVar.hashCode());
        synchronized (f) {
            f.remove(bVar);
            z = f.size() == 0;
            cd6 cd6 = cd6.a;
        }
        if (z) {
            f fVar = c;
            if (fVar != null) {
                fVar.a = true;
            }
            f fVar2 = c;
            if (fVar2 != null) {
                a.removeCallbacks(fVar2);
            }
            c = null;
            Collection<v40<r40, km1>> values = d.values();
            wg6.a(values, "deviceInFetchingInformation.values");
            Object[] array = values.toArray(new v40[0]);
            if (array != null) {
                for (Object obj : array) {
                    ((v40) obj).a(new km1((eh1) null, sk1.INTERRUPTED, (bn0) null, 5));
                }
                BluetoothAdapter bluetoothAdapter = b;
                if (!(bluetoothAdapter == null || (bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner()) == null)) {
                    bluetoothLeScanner.stopScan(e);
                }
                ut0.c.a();
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore
    public final List<BluetoothDevice> a() {
        ArrayList arrayList = new ArrayList();
        Context a2 = gk0.f.a();
        List<BluetoothDevice> list = null;
        BluetoothManager bluetoothManager = (BluetoothManager) (a2 != null ? a2.getSystemService(PlaceManager.PARAM_BLUETOOTH) : null);
        if (bluetoothManager != null) {
            list = bluetoothManager.getConnectedDevices(7);
        }
        if (list != null) {
            arrayList.addAll(list);
        }
        return arrayList;
    }

    @DexIgnore
    public static final /* synthetic */ void a(k40 k40, ii1 ii1) {
        if (k40.b(ii1)) {
            k40.a(ii1, 0);
        }
    }

    @DexIgnore
    public final String a(byte[] bArr) {
        int i2 = 0;
        while (i2 < bArr.length) {
            int i3 = i2 + 1;
            short b2 = cw0.b((byte) (bArr[i2] & ((byte) 255)));
            if (b2 == 0 || i3 + 1 > bArr.length) {
                return "";
            }
            if (d.l.a(bArr[i3]) == d.COMPLETE_LOCAL_NAME) {
                return new String(md6.a(md6.a(bArr, i3, i3 + b2), 1, b2), mi0.A.f());
            }
            i2 = b2 + i3;
        }
        return "";
    }

    @DexIgnore
    public final void a(Context context) {
        context.registerReceiver(g, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED"));
        context.registerReceiver(h, new IntentFilter("android.bluetooth.device.action.BOND_STATE_CHANGED"));
        b(context);
    }

    @DexIgnore
    public final void a(c cVar, c cVar2) {
        BluetoothGattServer bluetoothGattServer;
        c cVar3 = cVar;
        c cVar4 = cVar2;
        oa1 oa1 = oa1.a;
        Object[] objArr = {cVar3, cVar4};
        qs0 qs0 = qs0.h;
        nn0 nn0 = r6;
        nn0 nn02 = new nn0(cw0.a((Enum<?>) e.BLUETOOTH_STATE_CHANGED), og0.SYSTEM_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.PREV_STATE, (Object) cw0.a((Enum<?>) cVar)), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) cVar2)), 448);
        qs0.a(nn0);
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_STATE", cVar3);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_STATE", cVar4);
        Context a2 = gk0.f.a();
        if (a2 != null) {
            ce.a(a2).a(intent);
        }
        int i2 = hp0.b[cVar2.ordinal()];
        if (i2 == 1) {
            Context a3 = gk0.f.a();
            if (a3 != null) {
                l.b(a3);
            }
        } else if (i2 == 2 || i2 == 3 || i2 == 4) {
            c();
            fu0 fu0 = j;
            if (!(fu0 == null || (bluetoothGattServer = fu0.a) == null)) {
                for (h11 e2 : fu0.b) {
                    e2.e();
                }
                fu0.b.clear();
                bluetoothGattServer.close();
                fu0.a = null;
                fu0.c = false;
                oa1 oa12 = oa1.a;
                qs0.h.a(new nn0(cw0.a((Enum<?>) pj1.GATT_SERVER_STOPPED), og0.GATT_SERVER_EVENT, "", "", ze0.a("UUID.randomUUID().toString()"), fu0.c, (String) null, (r40) null, (bw0) null, (JSONObject) null, 960));
            }
            oa1 oa13 = oa1.a;
            new Object[1][0] = Integer.valueOf(f.size());
        }
        a aVar = k;
        if (aVar != null) {
            aVar.a(this, cVar3, cVar4);
        }
    }

    @DexIgnore
    public final void a(BluetoothDevice bluetoothDevice, int i2, int i3) {
        String str;
        int i4 = i2;
        int i5 = i3;
        oa1 oa1 = oa1.a;
        Object[] objArr = new Object[5];
        objArr[0] = bluetoothDevice.getAddress();
        String str2 = "BOND_BONDED";
        switch (i4) {
            case 10:
                str = "BOND_NONE";
                break;
            case 11:
                str = "BOND_BONDING";
                break;
            case 12:
                str = str2;
                break;
            default:
                str = "UNKNOWN";
                break;
        }
        objArr[1] = str;
        objArr[2] = Integer.valueOf(i2);
        switch (i5) {
            case 10:
                str2 = "BOND_NONE";
                break;
            case 11:
                str2 = "BOND_BONDING";
                break;
            case 12:
                break;
            default:
                str2 = "UNKNOWN";
                break;
        }
        objArr[3] = str2;
        objArr[4] = Integer.valueOf(i3);
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) e.BOND_STATE_CHANGED);
        og0 og0 = og0.CENTRAL_EVENT;
        String address = bluetoothDevice.getAddress();
        String str3 = address != null ? address : "";
        JSONObject jSONObject = new JSONObject();
        bm0 bm0 = bm0.MAC_ADDRESS;
        String address2 = bluetoothDevice.getAddress();
        if (address2 == null) {
            address2 = "";
        }
        qs0.a(new nn0(a2, og0, str3, "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(jSONObject, bm0, (Object) address2), bm0.PREV_STATE, (Object) cw0.a((Enum<?>) q40.a.b.a(i4))), bm0.NEW_STATE, (Object) cw0.a((Enum<?>) q40.a.b.a(i5))), 448));
        Intent intent = new Intent();
        intent.setAction("com.fossil.blesdk.adapter.BluetoothLeAdapter.action.BOND_STATE_CHANGED");
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.BLUETOOTH_DEVICE", bluetoothDevice);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.PREVIOUS_BOND_STATE", i4);
        intent.putExtra("com.fossil.blesdk.adapter.BluetoothLeAdapter.extra.NEW_BOND_STATE", i5);
        Context a3 = gk0.f.a();
        if (a3 != null) {
            ce.a(a3).a(intent);
        }
    }

    @DexIgnore
    public final void a(ii1 ii1, int i2) {
        Set<Map.Entry<b, o40>> entrySet;
        synchronized (f) {
            entrySet = f.entrySet();
            wg6.a(entrySet, "activeLeScanCallbacks.entries");
            cd6 cd6 = cd6.a;
        }
        for (Map.Entry entry : entrySet) {
            if (((o40) entry.getValue()).a(ii1)) {
                l.a((b) entry.getKey(), ii1, i2);
            }
        }
    }

    @DexIgnore
    public final void a(b bVar, ii1 ii1, int i2) {
        ii1 ii12 = ii1;
        oa1 oa1 = oa1.a;
        Object[] objArr = {ii12.r.getMacAddress(), ii12.r.getSerialNumber(), Integer.valueOf(i2)};
        qs0.h.a(new nn0(cw0.a((Enum<?>) e.DEVICE_FOUND), og0.CENTRAL_EVENT, "", "", "", true, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SCAN_CALLBACK, (Object) cw0.a(bVar.hashCode())), bm0.DEVICE, (Object) a(ii12)), bm0.RSSI, (Object) Integer.valueOf(i2)), 448));
        bVar.onDeviceFound(ii1, i2);
    }

    @DexIgnore
    public final void a(l40 l40) {
        Set<b> keySet;
        synchronized (f) {
            keySet = f.keySet();
            wg6.a(keySet, "activeLeScanCallbacks.keys");
            cd6 cd6 = cd6.a;
        }
        for (b a2 : keySet) {
            l.a(a2, l40);
        }
    }

    @DexIgnore
    public final void a(b bVar, l40 l40) {
        qs0.h.a(new nn0(cw0.a((Enum<?>) e.START_SCAN_FAILED), og0.CENTRAL_EVENT, "", "", "", false, (String) null, (r40) null, (bw0) null, cw0.a(cw0.a(new JSONObject(), bm0.SCAN_CALLBACK, (Object) String.valueOf(bVar.hashCode())), bm0.SCAN_ERROR, (Object) l40.a()), 448));
        oa1 oa1 = oa1.a;
        Object[] objArr = {cw0.a(bVar.hashCode()), l40.a(2)};
        bVar.onStartScanFailed(l40);
    }

    @DexIgnore
    public final JSONObject a(ii1 ii1) {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.SERIAL_NUMBER, (Object) ii1.r.getSerialNumber()), bm0.STATE, (Object) cw0.a((Enum<?>) ii1.s)), bm0.MAC_ADDRESS, (Object) ii1.r.getMacAddress()), bm0.CURRENT_BOND_STATE, (Object) cw0.a((Enum<?>) q40.a.b.a(ii1.c.getBondState())));
    }
}
