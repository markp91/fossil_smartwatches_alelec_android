package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je3 implements Parcelable.Creator<ie3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        int i = 0;
        ke3 ke3 = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                ke3 = (ke3) f22.a(parcel, a, ke3.CREATOR);
            } else if (a2 == 3) {
                i = f22.q(parcel, a);
            } else if (a2 == 4) {
                i2 = f22.q(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                i3 = f22.q(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new ie3(ke3, i, i2, i3);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ie3[i];
    }
}
