package com.fossil;

import com.fossil.y24;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.ServerSettingList;
import com.portfolio.platform.data.source.ServerSettingDataSource;
import com.portfolio.platform.data.source.ServerSettingRepository;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jt4 extends y24<a.b, a.c, a.C0017a> {
    @DexIgnore
    public static /* final */ String e;
    @DexIgnore
    public static /* final */ a f; // = new a((qg6) null);
    @DexIgnore
    public /* final */ ServerSettingRepository d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.jt4$a$a")
        /* renamed from: com.fossil.jt4$a$a  reason: collision with other inner class name */
        public static final class C0017a implements y24.a {
            @DexIgnore
            public C0017a(int i) {
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class b implements y24.b {
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class c implements y24.c {
            @DexIgnore
            public c(ServerSettingList serverSettingList) {
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return jt4.e;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements ServerSettingDataSource.OnGetServerSettingList {
        @DexIgnore
        public /* final */ /* synthetic */ jt4 a;

        @DexIgnore
        public b(jt4 jt4) {
            this.a = jt4;
        }

        @DexIgnore
        public void onFailed(int i) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = jt4.f.a();
            local.e(a2, "executeUseCase - onFailed. ErrorCode = " + i);
            this.a.a().a(new a.C0017a(i));
        }

        @DexIgnore
        public void onSuccess(ServerSettingList serverSettingList) {
            FLogger.INSTANCE.getLocal().d(jt4.f.a(), "executeUseCase - onSuccess");
            this.a.a().onSuccess(new a.c(serverSettingList));
        }
    }

    /*
    static {
        String simpleName = jt4.class.getSimpleName();
        wg6.a((Object) simpleName, "GetServerSettingUseCase::class.java.simpleName");
        e = simpleName;
    }
    */

    @DexIgnore
    public jt4(ServerSettingRepository serverSettingRepository) {
        wg6.b(serverSettingRepository, "serverSettingRepository");
        this.d = serverSettingRepository;
    }

    @DexIgnore
    public void a(a.b bVar) {
        this.d.getServerSettingList(new b(this));
    }
}
