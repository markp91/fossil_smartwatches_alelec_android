package com.fossil;

import android.os.Bundle;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class r43 extends s63 {
    @DexIgnore
    public static /* final */ AtomicReference<String[]> c; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> d; // = new AtomicReference<>();
    @DexIgnore
    public static /* final */ AtomicReference<String[]> e; // = new AtomicReference<>();

    @DexIgnore
    public r43(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, x63.b, x63.a, c);
    }

    @DexIgnore
    public final String b(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        return a(str, w63.b, w63.a, d);
    }

    @DexIgnore
    public final String c(String str) {
        if (str == null) {
            return null;
        }
        if (!s()) {
            return str;
        }
        if (!str.startsWith("_exp_")) {
            return a(str, z63.b, z63.a, e);
        }
        return "experiment_id" + "(" + str + ")";
    }

    @DexIgnore
    public final boolean q() {
        return false;
    }

    @DexIgnore
    public final boolean s() {
        d();
        return this.a.z() && this.a.b().a(3);
    }

    @DexIgnore
    public static String a(String str, String[] strArr, String[] strArr2, AtomicReference<String[]> atomicReference) {
        String str2;
        w12.a(strArr);
        w12.a(strArr2);
        w12.a(atomicReference);
        w12.a(strArr.length == strArr2.length);
        for (int i = 0; i < strArr.length; i++) {
            if (ma3.d(str, strArr[i])) {
                synchronized (atomicReference) {
                    String[] strArr3 = atomicReference.get();
                    if (strArr3 == null) {
                        strArr3 = new String[strArr2.length];
                        atomicReference.set(strArr3);
                    }
                    if (strArr3[i] == null) {
                        strArr3[i] = strArr2[i] + "(" + strArr[i] + ")";
                    }
                    str2 = strArr3[i];
                }
                return str2;
            }
        }
        return str;
    }

    @DexIgnore
    public final String a(j03 j03) {
        if (j03 == null) {
            return null;
        }
        if (!s()) {
            return j03.toString();
        }
        return "origin=" + j03.c + ",name=" + a(j03.a) + ",params=" + a(j03.b);
    }

    @DexIgnore
    public final String a(g03 g03) {
        if (g03 == null) {
            return null;
        }
        if (!s()) {
            return g03.toString();
        }
        return "Event{appId='" + g03.a + "', name='" + a(g03.b) + "', params=" + a(g03.f) + "}";
    }

    @DexIgnore
    public final String a(i03 i03) {
        if (i03 == null) {
            return null;
        }
        if (!s()) {
            return i03.toString();
        }
        return a(i03.zzb());
    }

    @DexIgnore
    public final String a(Bundle bundle) {
        if (bundle == null) {
            return null;
        }
        if (!s()) {
            return bundle.toString();
        }
        StringBuilder sb = new StringBuilder();
        for (String str : bundle.keySet()) {
            if (sb.length() != 0) {
                sb.append(", ");
            } else {
                sb.append("Bundle[{");
            }
            sb.append(b(str));
            sb.append("=");
            sb.append(bundle.get(str));
        }
        sb.append("}]");
        return sb.toString();
    }
}
