package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ch2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<ch2> CREATOR; // = new dh2();
    @DexIgnore
    public static /* final */ List<d12> d; // = Collections.emptyList();
    @DexIgnore
    public static /* final */ sw2 e; // = new sw2();
    @DexIgnore
    public sw2 a;
    @DexIgnore
    public List<d12> b;
    @DexIgnore
    public String c;

    @DexIgnore
    public ch2(sw2 sw2, List<d12> list, String str) {
        this.a = sw2;
        this.b = list;
        this.c = str;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (!(obj instanceof ch2)) {
            return false;
        }
        ch2 ch2 = (ch2) obj;
        return u12.a(this.a, ch2.a) && u12.a(this.b, ch2.b) && u12.a(this.c, ch2.c);
    }

    @DexIgnore
    public final int hashCode() {
        return this.a.hashCode();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, (Parcelable) this.a, i, false);
        g22.c(parcel, 2, this.b, false);
        g22.a(parcel, 3, this.c, false);
        g22.a(parcel, a2);
    }
}
