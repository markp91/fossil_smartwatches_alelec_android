package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface t62 extends IInterface {
    @DexIgnore
    x52 a(x52 x52, String str, int i, x52 x522) throws RemoteException;

    @DexIgnore
    x52 b(x52 x52, String str, int i, x52 x522) throws RemoteException;
}
