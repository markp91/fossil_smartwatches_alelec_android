package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class qx0 extends vv0 {
    @DexIgnore
    public byte[] J; // = new byte[0];
    @DexIgnore
    public byte K; // = -1;
    @DexIgnore
    public boolean L;
    @DexIgnore
    public boolean M;
    @DexIgnore
    public rg1 N; // = rg1.FTD;

    @DexIgnore
    public qx0(du0 du0, short s, lx0 lx0, ue1 ue1, int i) {
        super(du0, s, lx0, ue1, i);
    }

    @DexIgnore
    public final JSONObject a(byte[] bArr) {
        JSONObject jSONObject = new JSONObject();
        if (!this.L) {
            this.L = true;
            this.C = this.v.c != il0.SUCCESS;
            byte[] array = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN).put((byte) 8).array();
            wg6.a(array, "ByteBuffer.allocate(1)\n \u2026                 .array()");
            b(array);
        } else {
            this.C = true;
        }
        return jSONObject;
    }

    @DexIgnore
    public abstract void c(byte[] bArr);

    @DexIgnore
    public final boolean c(sg1 sg1) {
        return sg1.a == this.N;
    }

    @DexIgnore
    public final void e(sg1 sg1) {
        if (!this.L) {
            super.e(sg1);
            return;
        }
        byte[] bArr = sg1.b;
        this.C = true;
        this.g.add(new ne0(0, sg1.a, bArr, new JSONObject(), 1));
    }

    @DexIgnore
    public final void f(sg1 sg1) {
        byte[] bArr;
        if (this.s) {
            bArr = lg0.b.a(this.y.t, this.N, sg1.b);
        } else {
            bArr = sg1.b;
        }
        boolean z = false;
        int b = cw0.b(cw0.b((byte) (bArr[0] & 63)));
        int b2 = cw0.b(cw0.b((byte) (((byte) (this.K + 1)) & 63)));
        if (b == b2) {
            a(this.p);
            this.K = (byte) b2;
            this.J = cw0.a(this.J, md6.a(bArr, 1, bArr.length));
            if (((byte) (bArr[0] & Byte.MIN_VALUE)) != ((byte) 0)) {
                z = true;
            }
            this.M = z;
            if (this.M) {
                c(this.J);
                t();
                return;
            }
            return;
        }
        this.v = bn0.a(this.v, (lx0) null, (String) null, il0.MISS_PACKAGE, (ch0) null, (sj0) null, 27);
        this.C = true;
    }

    @DexIgnore
    public void t() {
    }
}
