package com.fossil;

import java.util.List;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface nn2<E> extends List<E>, RandomAccess {
    @DexIgnore
    void k();

    @DexIgnore
    nn2<E> zza(int i);

    @DexIgnore
    boolean zza();
}
