package com.fossil;

import com.portfolio.platform.data.InactivityNudgeTimeModel;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter$save$1$inactivityNudgeList$1", f = "NotificationWatchRemindersPresenter.kt", l = {}, m = "invokeSuspend")
public final class m05$b$a extends sf6 implements ig6<il6, xe6<? super List<? extends InactivityNudgeTimeModel>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationWatchRemindersPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m05$b$a(NotificationWatchRemindersPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        m05$b$a m05_b_a = new m05$b$a(this.this$0, xe6);
        m05_b_a.p$ = (il6) obj;
        return m05_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((m05$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.q.getInactivityNudgeTimeDao().getListInactivityNudgeTimeModel();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
