package com.fossil;

import com.fossil.a70;
import com.fossil.h60;
import com.fossil.v60;
import com.fossil.x60;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b70 {
    @DexIgnore
    public /* final */ ArrayList<r60> a; // = new ArrayList<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements hg6<r60, Boolean> {
        @DexIgnore
        public /* final */ /* synthetic */ r60 a;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(r60 r60) {
            super(1);
            this.a = r60;
        }

        @DexIgnore
        public Object invoke(Object obj) {
            return Boolean.valueOf(((r60) obj).getKey() == this.a.getKey());
        }
    }

    @DexIgnore
    public final void a(r60 r60) {
        vd6.a(this.a, new a(r60));
        this.a.add(r60);
    }

    @DexIgnore
    public final b70 b(long j) throws IllegalArgumentException {
        a((r60) new l60(j));
        return this;
    }

    @DexIgnore
    public final b70 c(long j) throws IllegalArgumentException {
        a((r60) new m60(j));
        return this;
    }

    @DexIgnore
    public final b70 d(long j) throws IllegalArgumentException {
        a((r60) new o60(j));
        return this;
    }

    @DexIgnore
    public final b70 e(long j) throws IllegalArgumentException {
        a((r60) new p60(j));
        return this;
    }

    @DexIgnore
    public final b70 b(int i) throws IllegalArgumentException {
        a((r60) new q60(i));
        return this;
    }

    @DexIgnore
    public final r60[] a() {
        Object[] array = this.a.toArray(new r60[0]);
        if (array != null) {
            return (r60[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final b70 a(byte b, h60.a aVar, short s, short s2, h60.b bVar) throws IllegalArgumentException {
        a((r60) new h60(b, aVar, s, s2, bVar));
        return this;
    }

    @DexIgnore
    public final b70 a(int i) throws IllegalArgumentException {
        a((r60) new j60(i));
        return this;
    }

    @DexIgnore
    public final b70 a(long j) throws IllegalArgumentException {
        a((r60) new k60(j));
        return this;
    }

    @DexIgnore
    public final b70 a(int i, int i2, int i3, int i4) throws IllegalArgumentException {
        a((r60) new n60(i, i2, i3, i4));
        return this;
    }

    @DexIgnore
    public final b70 a(h70 h70, c70 c70, f70 f70, i70 i70, e70 e70) {
        a((r60) new t60(h70, c70, f70, i70, e70));
        return this;
    }

    @DexIgnore
    public final b70 a(v60.a aVar) {
        a((r60) new v60(aVar));
        return this;
    }

    @DexIgnore
    public final b70 a(byte b, byte b2, byte b3, byte b4, short s, x60.a aVar) throws IllegalArgumentException {
        a((r60) new x60(b, b2, b3, b4, s, aVar));
        return this;
    }

    @DexIgnore
    public final b70 a(short s) throws IllegalArgumentException {
        a((r60) new y60(s));
        return this;
    }

    @DexIgnore
    public final b70 a(long j, short s, short s2) throws IllegalArgumentException {
        a((r60) new z60(j, s, s2));
        return this;
    }

    @DexIgnore
    public final b70 a(a70.a aVar) {
        a((r60) new a70(aVar));
        return this;
    }
}
