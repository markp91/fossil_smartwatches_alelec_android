package com.fossil;

import android.content.Context;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.PinObject;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class go4 extends BaseDbProvider implements fo4 {
    @DexIgnore
    public go4(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    public void a(PinObject pinObject) {
        if (pinObject != null) {
            try {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = this.TAG;
                local.d(str, "Unpin new object - uuid=" + pinObject.getUuid() + ", className=" + pinObject.getClassName());
                g().delete(pinObject);
            } catch (Exception e) {
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str2 = this.TAG;
                local2.e(str2, "Error inside " + this.TAG + ".unpin - e=" + e);
            }
        }
    }

    @DexIgnore
    public List<PinObject> b(String str) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder<PinObject, Integer> queryBuilder = g().queryBuilder();
            Where<PinObject, Integer> where = queryBuilder.where();
            where.eq(PinObject.COLUMN_CLASS_NAME, str);
            queryBuilder.setWhere(where);
            List<PinObject> query = g().query(queryBuilder.prepare());
            if (query == null || query.isEmpty()) {
                return arrayList;
            }
            return query;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str2 = this.TAG;
            local.e(str2, "Error inside " + this.TAG + ".find - e=" + e);
            return arrayList;
        }
    }

    @DexIgnore
    public final Dao<PinObject, Integer> g() throws SQLException {
        return this.databaseHelper.getDao(PinObject.class);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{PinObject.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }
}
