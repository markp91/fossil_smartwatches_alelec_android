package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class al6 extends RuntimeException {
    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public al6(String str, Throwable th) {
        super(str, th);
        wg6.b(str, "message");
        wg6.b(th, "cause");
    }
}
