package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;
import com.facebook.places.model.PlaceFields;
import java.util.Calendar;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s0 {
    @DexIgnore
    public static s0 d;
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ LocationManager b;
    @DexIgnore
    public /* final */ a c; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public long b;
        @DexIgnore
        public long c;
        @DexIgnore
        public long d;
        @DexIgnore
        public long e;
        @DexIgnore
        public long f;
    }

    @DexIgnore
    public s0(Context context, LocationManager locationManager) {
        this.a = context;
        this.b = locationManager;
    }

    @DexIgnore
    public static s0 a(Context context) {
        if (d == null) {
            Context applicationContext = context.getApplicationContext();
            d = new s0(applicationContext, (LocationManager) applicationContext.getSystemService(PlaceFields.LOCATION));
        }
        return d;
    }

    @DexIgnore
    public boolean b() {
        a aVar = this.c;
        if (c()) {
            return aVar.a;
        }
        Location a2 = a();
        if (a2 != null) {
            a(a2);
            return aVar.a;
        }
        Log.i("TwilightManager", "Could not get last known location. This is probably because the app does not have any location permissions. Falling back to hardcoded sunrise/sunset values.");
        int i = Calendar.getInstance().get(11);
        return i < 6 || i >= 22;
    }

    @DexIgnore
    public final boolean c() {
        return this.c.f > System.currentTimeMillis();
    }

    @DexIgnore
    @SuppressLint({"MissingPermission"})
    public final Location a() {
        Location location = null;
        Location a2 = x6.a(this.a, "android.permission.ACCESS_COARSE_LOCATION") == 0 ? a("network") : null;
        if (x6.a(this.a, "android.permission.ACCESS_FINE_LOCATION") == 0) {
            location = a("gps");
        }
        return (location == null || a2 == null) ? location != null ? location : a2 : location.getTime() > a2.getTime() ? location : a2;
    }

    @DexIgnore
    public final Location a(String str) {
        try {
            if (this.b.isProviderEnabled(str)) {
                return this.b.getLastKnownLocation(str);
            }
            return null;
        } catch (Exception e) {
            Log.d("TwilightManager", "Failed to get last known location", e);
            return null;
        }
    }

    @DexIgnore
    public final void a(Location location) {
        long j;
        a aVar = this.c;
        long currentTimeMillis = System.currentTimeMillis();
        r0 a2 = r0.a();
        r0 r0Var = a2;
        r0Var.a(currentTimeMillis - 86400000, location.getLatitude(), location.getLongitude());
        long j2 = a2.a;
        r0Var.a(currentTimeMillis, location.getLatitude(), location.getLongitude());
        boolean z = a2.c == 1;
        long j3 = a2.b;
        long j4 = j2;
        long j5 = a2.a;
        long j6 = j3;
        boolean z2 = z;
        a2.a(86400000 + currentTimeMillis, location.getLatitude(), location.getLongitude());
        long j7 = a2.b;
        if (j6 == -1 || j5 == -1) {
            j = 43200000 + currentTimeMillis;
        } else {
            j = (currentTimeMillis > j5 ? 0 + j7 : currentTimeMillis > j6 ? 0 + j5 : 0 + j6) + 60000;
        }
        aVar.a = z2;
        aVar.b = j4;
        aVar.c = j6;
        aVar.d = j5;
        aVar.e = j7;
        aVar.f = j;
    }
}
