package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ks0 extends vv0 {
    @DexIgnore
    public long J;
    @DexIgnore
    public /* final */ boolean K;
    @DexIgnore
    public /* final */ long L;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ks0(long j, short s, ue1 ue1, int i, int i2) {
        super(du0.LEGACY_ERASE_SEGMENT, s, lx0.LEGACY_ERASE_SEGMENT, ue1, (i2 & 8) != 0 ? 3 : i);
        this.L = j;
        this.K = true;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 4) {
            this.J = cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getInt(0));
            cw0.a(jSONObject, bm0.NEW_SIZE_WRITTEN, (Object) Long.valueOf(this.J));
        }
        return jSONObject;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(super.h(), bm0.PAGE_OFFSET, (Object) Long.valueOf(this.L));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_SIZE_WRITTEN, (Object) Long.valueOf(this.J));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.L).array();
        wg6.a(array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        return array;
    }

    @DexIgnore
    public boolean q() {
        return this.K;
    }
}
