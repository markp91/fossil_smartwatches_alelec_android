package com.fossil;

import java.util.Vector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hn1<T> {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public /* final */ Vector<ef0<T>> b; // = new Vector<>();

    @DexIgnore
    public final synchronized void a(ef0<T> ef0) {
        if (!this.b.contains(ef0)) {
            this.b.addElement(ef0);
        }
    }

    @DexIgnore
    public final synchronized void b(ef0<T> ef0) {
        this.b.removeElement(ef0);
    }

    @DexIgnore
    public final synchronized void c() {
        this.a = true;
    }

    @DexIgnore
    public final synchronized boolean b() {
        return this.a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0023, code lost:
        if (r4 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0025, code lost:
        r1 = ((com.fossil.ef0[]) r0.element).length;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002a, code lost:
        r1 = r1 - 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002c, code lost:
        if (r1 < 0) goto L_0x0038;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002e, code lost:
        ((com.fossil.ef0[]) r0.element)[r1].a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    @DexIgnore
    public final void a(T t) {
        jh6 jh6 = new jh6();
        synchronized (this) {
            if (b()) {
                Object[] array = this.b.toArray(new ef0[0]);
                if (array != null) {
                    jh6.element = (ef0[]) array;
                    a();
                    cd6 cd6 = cd6.a;
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            }
        }
    }

    @DexIgnore
    public final synchronized void a() {
        this.a = false;
    }
}
