package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oq3<T> {
    @DexIgnore
    public /* final */ Class<T> a;
    @DexIgnore
    public /* final */ T b;

    @DexIgnore
    public T a() {
        return this.b;
    }

    @DexIgnore
    public Class<T> b() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return String.format("Event{type: %s, payload: %s}", new Object[]{this.a, this.b});
    }
}
