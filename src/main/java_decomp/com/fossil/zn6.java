package com.fossil;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import com.fossil.mc6;
import java.lang.reflect.Constructor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zn6 {
    /*
    static {
        Object obj;
        try {
            mc6.a aVar = mc6.Companion;
            Looper mainLooper = Looper.getMainLooper();
            wg6.a((Object) mainLooper, "Looper.getMainLooper()");
            obj = mc6.m1constructorimpl(new xn6(a(mainLooper, true), "Main"));
        } catch (Throwable th) {
            mc6.a aVar2 = mc6.Companion;
            obj = mc6.m1constructorimpl(nc6.a(th));
        }
        if (mc6.m6isFailureimpl(obj)) {
            obj = null;
        }
        yn6 yn6 = (yn6) obj;
    }
    */

    @DexIgnore
    public static final Handler a(Looper looper, boolean z) {
        int i;
        wg6.b(looper, "$this$asHandler");
        if (!z || (i = Build.VERSION.SDK_INT) < 16) {
            return new Handler(looper);
        }
        if (i >= 28) {
            Object invoke = Handler.class.getDeclaredMethod("createAsync", new Class[]{Looper.class}).invoke((Object) null, new Object[]{looper});
            if (invoke != null) {
                return (Handler) invoke;
            }
            throw new rc6("null cannot be cast to non-null type android.os.Handler");
        }
        Class<Handler> cls = Handler.class;
        try {
            Constructor<Handler> declaredConstructor = cls.getDeclaredConstructor(new Class[]{Looper.class, Handler.Callback.class, Boolean.TYPE});
            wg6.a((Object) declaredConstructor, "Handler::class.java.getD\u2026:class.javaPrimitiveType)");
            Handler newInstance = declaredConstructor.newInstance(new Object[]{looper, null, true});
            wg6.a((Object) newInstance, "constructor.newInstance(this, null, true)");
            return newInstance;
        } catch (NoSuchMethodException unused) {
            return new Handler(looper);
        }
    }
}
