package com.fossil;

import com.fossil.n60;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class f01 extends tg6 implements hg6<byte[], n60> {
    @DexIgnore
    public f01(n60.a aVar) {
        super(1, aVar);
    }

    @DexIgnore
    public final String getName() {
        return "objectFromData";
    }

    @DexIgnore
    public final hi6 getOwner() {
        return kh6.a(n60.a.class);
    }

    @DexIgnore
    public final String getSignature() {
        return "objectFromData$blesdk_productionRelease([B)Lcom/fossil/blesdk/device/data/config/DailySleepConfig;";
    }

    @DexIgnore
    public Object invoke(Object obj) {
        return ((n60.a) this.receiver).a((byte[]) obj);
    }
}
