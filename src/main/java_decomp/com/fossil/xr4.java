package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.SetVibrationStrengthUseCase;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xr4 implements Factory<wr4> {
    @DexIgnore
    public /* final */ Provider<DeviceRepository> a;
    @DexIgnore
    public /* final */ Provider<an4> b;

    @DexIgnore
    public xr4(Provider<DeviceRepository> provider, Provider<an4> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    @DexIgnore
    public static xr4 a(Provider<DeviceRepository> provider, Provider<an4> provider2) {
        return new xr4(provider, provider2);
    }

    @DexIgnore
    public static wr4 b(Provider<DeviceRepository> provider, Provider<an4> provider2) {
        return new SetVibrationStrengthUseCase(provider.get(), provider2.get());
    }

    @DexIgnore
    public SetVibrationStrengthUseCase get() {
        return b(this.a, this.b);
    }
}
