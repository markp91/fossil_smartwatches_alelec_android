package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rr3 {
    @DexIgnore
    public static /* final */ long a; // = TimeUnit.MINUTES.toMillis(1);
    @DexIgnore
    public static /* final */ Object b; // = new Object();
    @DexIgnore
    public static dc3 c;

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0031, code lost:
        return r4;
     */
    @DexIgnore
    public static ComponentName a(Context context, Intent intent) {
        synchronized (b) {
            if (c == null) {
                dc3 dc3 = new dc3(context, 1, "wake:com.google.firebase.iid.WakeLockHolder");
                c = dc3;
                dc3.a(true);
            }
            boolean booleanExtra = intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false);
            a(intent, true);
            ComponentName startService = context.startService(intent);
            if (startService == null) {
                return null;
            }
            if (!booleanExtra) {
                c.a(a);
            }
        }
    }

    @DexIgnore
    public static void a(Intent intent, boolean z) {
        intent.putExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", z);
    }

    @DexIgnore
    public static void a(Intent intent) {
        synchronized (b) {
            if (c != null && intent.getBooleanExtra("com.google.firebase.iid.WakeLockHolder.wakefulintent", false)) {
                a(intent, false);
                c.a();
            }
        }
    }
}
