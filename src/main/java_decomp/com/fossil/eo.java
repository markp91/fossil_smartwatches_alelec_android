package com.fossil;

import android.database.Cursor;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eo implements Cdo {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hh<co> {
        @DexIgnore
        public a(eo eoVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(mi miVar, co coVar) {
            String str = coVar.a;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            String str2 = coVar.b;
            if (str2 == null) {
                miVar.a(2);
            } else {
                miVar.a(2, str2);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkTag`(`tag`,`work_spec_id`) VALUES (?,?)";
        }
    }

    @DexIgnore
    public eo(oh ohVar) {
        this.a = ohVar;
        this.b = new a(this, ohVar);
    }

    @DexIgnore
    public void a(co coVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(coVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public List<String> a(String str) {
        rh b2 = rh.b("SELECT DISTINCT tag FROM worktag WHERE work_spec_id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }
}
