package com.fossil;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.fossil.i;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c4 extends Service {
    @DexIgnore
    public i.a a; // = new a(this);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends i.a {
        @DexIgnore
        public a(c4 c4Var) {
        }

        @DexIgnore
        public void a(g gVar, Bundle bundle) throws RemoteException {
            gVar.e(bundle);
        }

        @DexIgnore
        public void a(g gVar, String str, Bundle bundle) throws RemoteException {
            gVar.a(str, bundle);
        }
    }

    @DexIgnore
    public IBinder onBind(Intent intent) {
        return this.a;
    }
}
