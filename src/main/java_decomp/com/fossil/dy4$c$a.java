package com.fossil;

import com.portfolio.platform.PortfolioApp;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationapps.NotificationAppsPresenter$start$1$1", f = "NotificationAppsPresenter.kt", l = {}, m = "invokeSuspend")
public final class dy4$c$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;

    @DexIgnore
    public dy4$c$a(xe6 xe6) {
        super(2, xe6);
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        dy4$c$a dy4_c_a = new dy4$c$a(xe6);
        dy4_c_a.p$ = (il6) obj;
        return dy4_c_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((dy4$c$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            PortfolioApp.get.instance().M();
            return cd6.a;
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
