package com.fossil;

import android.graphics.Bitmap;
import android.os.Build;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ju implements hu {
    @DexIgnore
    public static /* final */ Bitmap.Config[] d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] e; // = d;
    @DexIgnore
    public static /* final */ Bitmap.Config[] f; // = {Bitmap.Config.RGB_565};
    @DexIgnore
    public static /* final */ Bitmap.Config[] g; // = {Bitmap.Config.ARGB_4444};
    @DexIgnore
    public static /* final */ Bitmap.Config[] h; // = {Bitmap.Config.ALPHA_8};
    @DexIgnore
    public /* final */ c a; // = new c();
    @DexIgnore
    public /* final */ du<b, Bitmap> b; // = new du<>();
    @DexIgnore
    public /* final */ Map<Bitmap.Config, NavigableMap<Integer, Integer>> c; // = new HashMap();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static /* synthetic */ class a {
        @DexIgnore
        public static /* final */ /* synthetic */ int[] a; // = new int[Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /*
        static {
            a[Bitmap.Config.ARGB_8888.ordinal()] = 1;
            a[Bitmap.Config.RGB_565.ordinal()] = 2;
            a[Bitmap.Config.ARGB_4444.ordinal()] = 3;
            try {
                a[Bitmap.Config.ALPHA_8.ordinal()] = 4;
            } catch (NoSuchFieldError unused) {
            }
        }
        */
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends zt<b> {
        @DexIgnore
        public b a(int i, Bitmap.Config config) {
            b bVar = (b) b();
            bVar.a(i, config);
            return bVar;
        }

        @DexIgnore
        public b a() {
            return new b(this);
        }
    }

    /*
    static {
        Bitmap.Config[] configArr = {Bitmap.Config.ARGB_8888, null};
        if (Build.VERSION.SDK_INT >= 26) {
            configArr = (Bitmap.Config[]) Arrays.copyOf(configArr, configArr.length + 1);
            configArr[configArr.length - 1] = Bitmap.Config.RGBA_F16;
        }
        d = configArr;
    }
    */

    @DexIgnore
    public void a(Bitmap bitmap) {
        b a2 = this.a.a(r00.a(bitmap), bitmap.getConfig());
        this.b.a(a2, bitmap);
        NavigableMap<Integer, Integer> a3 = a(bitmap.getConfig());
        Integer num = (Integer) a3.get(Integer.valueOf(a2.b));
        Integer valueOf = Integer.valueOf(a2.b);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        a3.put(valueOf, Integer.valueOf(i));
    }

    @DexIgnore
    public String b(int i, int i2, Bitmap.Config config) {
        return b(r00.a(i, i2, config), config);
    }

    @DexIgnore
    public String c(Bitmap bitmap) {
        return b(r00.a(bitmap), bitmap.getConfig());
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy{groupedMap=");
        sb.append(this.b);
        sb.append(", sortedSizes=(");
        for (Map.Entry next : this.c.entrySet()) {
            sb.append(next.getKey());
            sb.append('[');
            sb.append(next.getValue());
            sb.append("], ");
        }
        if (!this.c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")}");
        return sb.toString();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements iu {
        @DexIgnore
        public /* final */ c a;
        @DexIgnore
        public int b;
        @DexIgnore
        public Bitmap.Config c;

        @DexIgnore
        public b(c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(int i, Bitmap.Config config) {
            this.b = i;
            this.c = config;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            if (this.b != bVar.b || !r00.b((Object) this.c, (Object) bVar.c)) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public int hashCode() {
            int i = this.b * 31;
            Bitmap.Config config = this.c;
            return i + (config != null ? config.hashCode() : 0);
        }

        @DexIgnore
        public String toString() {
            return ju.b(this.b, this.c);
        }

        @DexIgnore
        public void a() {
            this.a.a(this);
        }
    }

    @DexIgnore
    public int b(Bitmap bitmap) {
        return r00.a(bitmap);
    }

    @DexIgnore
    public static String b(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    @DexIgnore
    public static Bitmap.Config[] b(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16.equals(config)) {
            return e;
        }
        int i = a.a[config.ordinal()];
        if (i == 1) {
            return d;
        }
        if (i == 2) {
            return f;
        }
        if (i == 3) {
            return g;
        }
        if (i == 4) {
            return h;
        }
        return new Bitmap.Config[]{config};
    }

    @DexIgnore
    public Bitmap a(int i, int i2, Bitmap.Config config) {
        b a2 = a(r00.a(i, i2, config), config);
        Bitmap a3 = this.b.a(a2);
        if (a3 != null) {
            a(Integer.valueOf(a2.b), a3);
            a3.reconfigure(i, i2, config);
        }
        return a3;
    }

    @DexIgnore
    public final b a(int i, Bitmap.Config config) {
        b a2 = this.a.a(i, config);
        Bitmap.Config[] b2 = b(config);
        int length = b2.length;
        int i2 = 0;
        while (i2 < length) {
            Bitmap.Config config2 = b2[i2];
            Integer ceilingKey = a(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else {
                if (ceilingKey.intValue() == i) {
                    if (config2 == null) {
                        if (config == null) {
                            return a2;
                        }
                    } else if (config2.equals(config)) {
                        return a2;
                    }
                }
                this.a.a(a2);
                return this.a.a(ceilingKey.intValue(), config2);
            }
        }
        return a2;
    }

    @DexIgnore
    public Bitmap a() {
        Bitmap a2 = this.b.a();
        if (a2 != null) {
            a(Integer.valueOf(r00.a(a2)), a2);
        }
        return a2;
    }

    @DexIgnore
    public final void a(Integer num, Bitmap bitmap) {
        NavigableMap<Integer, Integer> a2 = a(bitmap.getConfig());
        Integer num2 = (Integer) a2.get(num);
        if (num2 == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + num + ", removed: " + c(bitmap) + ", this: " + this);
        } else if (num2.intValue() == 1) {
            a2.remove(num);
        } else {
            a2.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    @DexIgnore
    public final NavigableMap<Integer, Integer> a(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.c.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.c.put(config, treeMap);
        return treeMap;
    }
}
