package com.fossil;

import android.content.Context;
import android.text.format.DateFormat;
import android.util.SparseIntArray;
import com.fossil.m24;
import com.fossil.yv4;
import com.fossil.zv4;
import com.misfit.frameworks.buttonservice.communite.CommunicateMode;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.Explore;
import com.portfolio.platform.data.model.MFUser;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.local.alarm.Alarm;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.service.BleCommandResultManager;
import com.portfolio.platform.uirenew.alarm.AlarmFragment;
import com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm;
import com.portfolio.platform.uirenew.alarm.usecase.SetAlarms;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class AlarmPresenter extends pv4 {
    @DexIgnore
    public static /* final */ String u;
    @DexIgnore
    public static /* final */ a v; // = new a((qg6) null);
    @DexIgnore
    public String e;
    @DexIgnore
    public String f; // = "";
    @DexIgnore
    public int g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public SparseIntArray i;
    @DexIgnore
    public boolean j;
    @DexIgnore
    public MFUser k;
    @DexIgnore
    public String l;
    @DexIgnore
    public /* final */ qv4 m;
    @DexIgnore
    public /* final */ String n;
    @DexIgnore
    public /* final */ ArrayList<Alarm> o;
    @DexIgnore
    public /* final */ Alarm p;
    @DexIgnore
    public /* final */ SetAlarms q;
    @DexIgnore
    public /* final */ AlarmHelper r;
    @DexIgnore
    public /* final */ DeleteAlarm s;
    @DexIgnore
    public /* final */ UserRepository t;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final String a() {
            return AlarmPresenter.u;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements m24.e<yv4.e, yv4.c> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public b(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* renamed from: a */
        public void onSuccess(DeleteAlarm.e eVar) {
            wg6.b(eVar, "responseValue");
            AlarmHelper c = this.a.r;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            c.d(applicationContext);
            this.a.m.a();
            this.a.m.x();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void a(DeleteAlarm.c cVar) {
            wg6.b(cVar, "errorValue");
            AlarmHelper c = this.a.r;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            c.d(applicationContext);
            this.a.m.a();
            int b = cVar.b();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.v.a();
            local.d(a2, "deleteAlarm() - deleteAlarm - onError - lastErrorCode = " + b);
            if (b != 1101) {
                if (b == 8888) {
                    this.a.m.c();
                    return;
                } else if (!(b == 1112 || b == 1113)) {
                    this.a.m.X();
                    return;
                }
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(cVar.a());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            qv4 n = this.a.m;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                n.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements m24.e<zv4.d, zv4.b> {
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter a;

        @DexIgnore
        public c(AlarmPresenter alarmPresenter) {
            this.a = alarmPresenter;
        }

        @DexIgnore
        /* renamed from: a */
        public void onSuccess(SetAlarms.d dVar) {
            wg6.b(dVar, "responseValue");
            FLogger.INSTANCE.getLocal().d(AlarmPresenter.v.a(), "saveAlarm SetAlarms onSuccess");
            this.a.m.a();
            Alarm b = this.a.p;
            if (b == null) {
                b = dVar.a();
            }
            this.a.m.a(b, true);
            this.a.m();
        }

        @DexIgnore
        public void a(SetAlarms.b bVar) {
            wg6.b(bVar, "errorValue");
            this.a.l = bVar.a().getUri();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = AlarmPresenter.v.a();
            local.d(a2, "saveAlarm SetAlarms onError - uri: " + this.a.l);
            this.a.m.a();
            int c = bVar.c();
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a3 = AlarmPresenter.v.a();
            local2.d(a3, "saveAlarm - SetAlarms - onError - lastErrorCode = " + c);
            if (c != 1101) {
                if (c == 8888) {
                    this.a.m.c();
                    return;
                } else if (!(c == 1112 || c == 1113)) {
                    if (this.a.p == null) {
                        this.a.m.a(bVar.a(), false);
                        return;
                    } else {
                        this.a.m.X();
                        return;
                    }
                }
            }
            List<uh4> convertBLEPermissionErrorCode = uh4.convertBLEPermissionErrorCode(bVar.b());
            wg6.a((Object) convertBLEPermissionErrorCode, "PermissionCodes.convertB\u2026sionErrorCode(errorCodes)");
            qv4 n = this.a.m;
            Object[] array = convertBLEPermissionErrorCode.toArray(new uh4[0]);
            if (array != null) {
                uh4[] uh4Arr = (uh4[]) array;
                n.a((uh4[]) Arrays.copyOf(uh4Arr, uh4Arr.length));
                return;
            }
            throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1", f = "AlarmPresenter.kt", l = {59}, m = "invokeSuspend")
    public static final class d extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ AlarmPresenter this$0;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.uirenew.alarm.AlarmPresenter$start$1$1", f = "AlarmPresenter.kt", l = {}, m = "invokeSuspend")
        public static final class a extends sf6 implements ig6<il6, xe6<? super MFUser>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ d this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = dVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                return aVar;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    return this.this$0.this$0.t.getCurrentUser();
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(AlarmPresenter alarmPresenter, xe6 xe6) {
            super(2, xe6);
            this.this$0 = alarmPresenter;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            d dVar = new d(this.this$0, xe6);
            dVar.p$ = (il6) obj;
            return dVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((d) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            AlarmPresenter alarmPresenter;
            Object a2 = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(AlarmPresenter.v.a(), "start");
                PortfolioApp.get.b((Object) il6);
                this.this$0.q.f();
                this.this$0.s.f();
                BleCommandResultManager.d.a(CommunicateMode.SET_LIST_ALARM);
                AlarmPresenter alarmPresenter2 = this.this$0;
                dl6 a3 = alarmPresenter2.c();
                a aVar = new a(this, (xe6) null);
                this.L$0 = il6;
                this.L$1 = alarmPresenter2;
                this.label = 1;
                obj = gk6.a(a3, aVar, this);
                if (obj == a2) {
                    return a2;
                }
                alarmPresenter = alarmPresenter2;
            } else if (i == 1) {
                alarmPresenter = (AlarmPresenter) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            alarmPresenter.k = (MFUser) obj;
            MFUser l = this.this$0.k;
            if (!(l == null || l.getUserId() == null)) {
                this.this$0.m.t(this.this$0.p != null);
                if (this.this$0.i == null) {
                    this.this$0.i = new SparseIntArray();
                }
                if (this.this$0.e == null) {
                    if (this.this$0.p != null) {
                        AlarmPresenter alarmPresenter3 = this.this$0;
                        alarmPresenter3.e = alarmPresenter3.p.getTitle();
                        AlarmPresenter alarmPresenter4 = this.this$0;
                        alarmPresenter4.f = alarmPresenter4.p.getMessage();
                        AlarmPresenter alarmPresenter5 = this.this$0;
                        alarmPresenter5.g = alarmPresenter5.p.getTotalMinutes();
                        AlarmPresenter alarmPresenter6 = this.this$0;
                        alarmPresenter6.h = alarmPresenter6.p.isRepeated();
                        AlarmPresenter alarmPresenter7 = this.this$0;
                        alarmPresenter7.i = alarmPresenter7.a(alarmPresenter7.p.getDays());
                        AlarmPresenter alarmPresenter8 = this.this$0;
                        alarmPresenter8.j = alarmPresenter8.p.isActive();
                        this.this$0.p();
                        this.this$0.m.N();
                    } else {
                        this.this$0.j = true;
                        this.this$0.h = false;
                        this.this$0.i = new SparseIntArray();
                        this.this$0.n();
                        this.this$0.p();
                    }
                }
                qv4 n = this.this$0.m;
                String j = this.this$0.e;
                if (j == null) {
                    j = "";
                }
                n.a(j);
                this.this$0.m.m(this.this$0.f);
                this.this$0.m.b(this.this$0.g);
                this.this$0.m.k(this.this$0.h);
                qv4 n2 = this.this$0.m;
                SparseIntArray e = this.this$0.i;
                if (e != null) {
                    n2.a(e);
                } else {
                    wg6.a();
                    throw null;
                }
            }
            return cd6.a;
        }
    }

    /*
    static {
        String simpleName = AlarmPresenter.class.getSimpleName();
        wg6.a((Object) simpleName, "AlarmPresenter::class.java.simpleName");
        u = simpleName;
    }
    */

    @DexIgnore
    public AlarmPresenter(qv4 qv4, String str, ArrayList<Alarm> arrayList, Alarm alarm, zv4 zv4, rj4 rj4, yv4 yv4, UserRepository userRepository) {
        wg6.b(qv4, "mView");
        wg6.b(str, "mDeviceId");
        wg6.b(arrayList, "mAlarms");
        wg6.b(zv4, "mSetAlarms");
        wg6.b(rj4, "mAlarmHelper");
        wg6.b(yv4, "mDeleteAlarm");
        wg6.b(userRepository, "mUserRepository");
        this.m = qv4;
        this.n = str;
        this.o = arrayList;
        this.p = alarm;
        this.q = zv4;
        this.r = rj4;
        this.s = yv4;
        this.t = userRepository;
    }

    @DexIgnore
    public void f() {
        rm6 unused = ik6.b(e(), (af6) null, (ll6) null, new d(this, (xe6) null), 3, (Object) null);
    }

    @DexIgnore
    public void g() {
        FLogger.INSTANCE.getLocal().d(u, "stop");
        this.q.g();
        this.s.g();
        PortfolioApp.get.c(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r0v1, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.DeleteAlarm] */
    /* JADX WARNING: type inference failed for: r2v1, types: [com.fossil.AlarmPresenter$b, com.portfolio.platform.CoroutineUseCase$e] */
    public void h() {
        this.m.b();
        Object r0 = this.s;
        String str = this.n;
        ArrayList<Alarm> arrayList = this.o;
        Alarm alarm = this.p;
        if (alarm != null) {
            r0.a(new DeleteAlarm.d(str, arrayList, alarm), new b(this));
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v16, types: [com.portfolio.platform.CoroutineUseCase, com.portfolio.platform.uirenew.alarm.usecase.SetAlarms] */
    /* JADX WARNING: type inference failed for: r2v10, types: [com.fossil.AlarmPresenter$c, com.portfolio.platform.CoroutineUseCase$e] */
    public void i() {
        Alarm alarm;
        xm4 xm4 = xm4.d;
        qv4 qv4 = this.m;
        if (qv4 == null) {
            throw new rc6("null cannot be cast to non-null type com.portfolio.platform.uirenew.alarm.AlarmFragment");
        } else if (xm4.a(xm4, ((AlarmFragment) qv4).getContext(), "SET_ALARMS", false, false, false, 28, (Object) null)) {
            this.m.b();
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            String u2 = bk4.u(instance.getTime());
            String str = this.l;
            if (str == null) {
                StringBuilder sb = new StringBuilder();
                MFUser mFUser = this.k;
                sb.append(mFUser != null ? mFUser.getUserId() : null);
                sb.append(':');
                Calendar instance2 = Calendar.getInstance();
                wg6.a((Object) instance2, "Calendar.getInstance()");
                sb.append(instance2.getTimeInMillis());
                str = sb.toString();
            }
            String str2 = str;
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str3 = u;
            local.d(str3, "saveAlarm - uri: " + str2);
            int[] a2 = this.h ? a(this.i) : new int[0];
            String title = this.m.getTitle();
            String C = this.m.C();
            this.f = this.m.C();
            int i2 = this.g;
            int i3 = i2 / 60;
            int i4 = i2 % 60;
            if (a2 != null) {
                boolean z = this.h;
                wg6.a((Object) u2, "createdAt");
                Alarm alarm2 = r3;
                String str4 = C;
                Alarm alarm3 = new Alarm((String) null, str2, title, C, i3, i4, a2, true, z, u2, u2, 0, 2048, (qg6) null);
                Alarm alarm4 = this.p;
                if (alarm4 != null) {
                    alarm4.setActive(this.j);
                    this.p.setTitle(title);
                    this.p.setMessage(str4);
                    this.p.setTotalMinutes(this.g);
                    this.p.setRepeated(this.h);
                    this.p.setDays(a2);
                    Iterator<Alarm> it = this.o.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        Alarm next = it.next();
                        if (wg6.a((Object) next.getUri(), (Object) this.p.getUri())) {
                            ArrayList<Alarm> arrayList = this.o;
                            arrayList.set(arrayList.indexOf(next), this.p);
                            break;
                        }
                    }
                    alarm = alarm2;
                } else {
                    alarm = alarm2;
                    this.o.add(alarm);
                }
                FLogger.INSTANCE.getLocal().d(u, "saveAlarm SetAlarms");
                Object r1 = this.q;
                String str5 = this.n;
                ArrayList<Alarm> arrayList2 = this.o;
                Alarm alarm5 = this.p;
                if (alarm5 != null) {
                    alarm = alarm5;
                }
                r1.a(new SetAlarms.c(str5, arrayList2, alarm), new c(this));
                return;
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final boolean j() {
        Alarm alarm = this.p;
        if ((alarm != null ? alarm.getDays() : null) == null) {
            return true;
        }
        SparseIntArray sparseIntArray = this.i;
        if (sparseIntArray != null) {
            int size = sparseIntArray.size();
            int[] days = this.p.getDays();
            if (days == null) {
                wg6.a();
                throw null;
            } else if (size != days.length) {
                return true;
            } else {
                int[] days2 = this.p.getDays();
                if (days2 != null) {
                    int length = days2.length;
                    int i2 = 0;
                    while (i2 < length) {
                        int i3 = days2[i2];
                        SparseIntArray sparseIntArray2 = this.i;
                        if (sparseIntArray2 == null) {
                            wg6.a();
                            throw null;
                        } else if (sparseIntArray2.indexOfKey(i3) < 0) {
                            return true;
                        } else {
                            i2++;
                        }
                    }
                    return false;
                }
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public final boolean k() {
        if (this.p == null) {
            return true;
        }
        String str = this.f;
        if (str != null) {
            String obj = yj6.d(str).toString();
            String message = this.p.getMessage();
            if (message != null) {
                return true ^ wg6.a((Object) obj, (Object) yj6.d(message).toString());
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
        throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
    }

    @DexIgnore
    public final boolean l() {
        String str;
        if (this.p == null || (str = this.e) == null) {
            return true;
        }
        if (str == null) {
            wg6.a();
            throw null;
        } else if (str != null) {
            String obj = yj6.d(str).toString();
            String title = this.p.getTitle();
            if (title != null) {
                return true ^ wg6.a((Object) obj, (Object) yj6.d(title).toString());
            }
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        } else {
            throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void m() {
        FLogger.INSTANCE.getLocal().d(u, "onSetAlarmsSuccess()");
        this.r.d(PortfolioApp.get.instance());
        PortfolioApp.get.instance().l(this.n);
    }

    @DexIgnore
    public final void n() {
        int i2 = Calendar.getInstance().get(10);
        int i3 = Calendar.getInstance().get(12);
        boolean z = true;
        if (Calendar.getInstance().get(9) != 1) {
            z = false;
        }
        b(String.valueOf(i2), String.valueOf(i3), z);
    }

    @DexIgnore
    public void o() {
        this.m.a(this);
    }

    @DexIgnore
    public final void p() {
        if (this.p == null || l() || k() || this.h != this.p.isRepeated() || this.g != this.p.getTotalMinutes() || j()) {
            this.m.d(true);
        } else {
            this.m.d(false);
        }
    }

    @DexIgnore
    public void b(String str) {
        wg6.b(str, Explore.COLUMN_TITLE);
        this.e = str;
        p();
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0077  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x008b  */
    public final void b(String str, String str2, boolean z) {
        int i2;
        int i3;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str3 = u;
        local.d(str3, "updateTime: hourValue = " + str + ", minuteValue = " + str2 + ", isPM = " + z);
        int i4 = 0;
        try {
            Integer valueOf = Integer.valueOf(str);
            wg6.a((Object) valueOf, "Integer.valueOf(hourValue)");
            i3 = valueOf.intValue();
            try {
                Integer valueOf2 = Integer.valueOf(str2);
                wg6.a((Object) valueOf2, "Integer.valueOf(minuteValue)");
                i2 = valueOf2.intValue();
            } catch (Exception e2) {
                e = e2;
                ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                String str4 = u;
                local2.e(str4, "Exception when parse time e=" + e);
                i2 = 0;
                if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
                }
            }
        } catch (Exception e3) {
            e = e3;
            i3 = 0;
            ILocalFLogger local22 = FLogger.INSTANCE.getLocal();
            String str42 = u;
            local22.e(str42, "Exception when parse time e=" + e);
            i2 = 0;
            if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            }
        }
        if (DateFormat.is24HourFormat(PortfolioApp.get.instance())) {
            if (z) {
                i4 = i3 == 12 ? 12 : i3 + 12;
            } else if (i3 != 12) {
                i4 = i3;
            }
            this.g = (i4 * 60) + i2;
            return;
        }
        if (z) {
            i4 = i3 == 12 ? 12 : i3 + 12;
        } else if (i3 != 12) {
            i4 = i3;
        }
        this.g = (i4 * 60) + i2;
    }

    @DexIgnore
    public void a(boolean z, int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = u;
        local.d(str, "updateDaysRepeat: isStateEnabled = " + z + ", day = " + i2);
        if (z) {
            SparseIntArray sparseIntArray = this.i;
            if (sparseIntArray != null) {
                sparseIntArray.put(i2, i2);
            } else {
                wg6.a();
                throw null;
            }
        } else {
            SparseIntArray sparseIntArray2 = this.i;
            if (sparseIntArray2 != null) {
                sparseIntArray2.delete(i2);
            } else {
                wg6.a();
                throw null;
            }
        }
        p();
        SparseIntArray sparseIntArray3 = this.i;
        if (sparseIntArray3 == null) {
            wg6.a();
            throw null;
        } else if (sparseIntArray3.size() == 0) {
            this.h = false;
            this.m.k(false);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        this.h = z;
        if (this.p != null) {
            p();
        }
        if (this.h) {
            SparseIntArray sparseIntArray = this.i;
            if (sparseIntArray == null) {
                wg6.a();
                throw null;
            } else if (sparseIntArray.size() == 0) {
                this.i = a(new int[]{2, 3, 4, 5, 6, 7, 1});
                qv4 qv4 = this.m;
                SparseIntArray sparseIntArray2 = this.i;
                if (sparseIntArray2 != null) {
                    qv4.a(sparseIntArray2);
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
    }

    @DexIgnore
    public void a(String str, String str2, boolean z) {
        wg6.b(str, "hourValue");
        wg6.b(str2, "minuteValue");
        b(str, str2, z);
        if (this.p != null) {
            p();
        }
    }

    @DexIgnore
    public void a(String str) {
        wg6.b(str, "message");
        this.f = str;
        p();
    }

    @DexIgnore
    public final SparseIntArray a(int[] iArr) {
        int i2 = 0;
        int length = iArr != null ? iArr.length : 0;
        if (length <= 0) {
            return new SparseIntArray();
        }
        SparseIntArray sparseIntArray = new SparseIntArray();
        while (i2 < length) {
            if (iArr != null) {
                int i3 = iArr[i2];
                sparseIntArray.put(i3, i3);
                i2++;
            } else {
                wg6.a();
                throw null;
            }
        }
        return sparseIntArray;
    }

    @DexIgnore
    public final int[] a(SparseIntArray sparseIntArray) {
        int size = sparseIntArray != null ? sparseIntArray.size() : 0;
        if (size <= 0) {
            return null;
        }
        int i2 = size;
        int i3 = 0;
        while (i3 < i2) {
            if (sparseIntArray != null) {
                if (sparseIntArray.keyAt(i3) != sparseIntArray.valueAt(i3)) {
                    sparseIntArray.removeAt(i3);
                    i2--;
                    i3--;
                }
                i3++;
            } else {
                wg6.a();
                throw null;
            }
        }
        if (sparseIntArray != null) {
            int size2 = sparseIntArray.size();
            if (size2 <= 0) {
                return null;
            }
            int[] iArr = new int[size2];
            for (int i4 = 0; i4 < size2; i4++) {
                iArr[i4] = sparseIntArray.valueAt(i4);
            }
            return iArr;
        }
        wg6.a();
        throw null;
    }
}
