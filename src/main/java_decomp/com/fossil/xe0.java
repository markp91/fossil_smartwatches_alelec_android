package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import com.facebook.places.PlaceManager;
import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import org.json.JSONObject;
import retrofit2.Retrofit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xe0 {
    @DexIgnore
    public wj1 a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public /* final */ ArrayList<File> c; // = new ArrayList<>();
    @DexIgnore
    public wh1 d;
    @DexIgnore
    public dr0 e;
    @DexIgnore
    public long f; // = 60000;
    @DexIgnore
    public /* final */ ag1 g;
    @DexIgnore
    public ge0 h;
    @DexIgnore
    public /* final */ fp0 i;
    @DexIgnore
    public /* final */ Handler j;
    @DexIgnore
    public boolean k;

    @DexIgnore
    public xe0(ag1 ag1, ge0 ge0, fp0 fp0, Handler handler, boolean z) {
        this.g = ag1;
        this.h = ge0;
        this.i = fp0;
        this.j = handler;
        this.k = z;
        a(this.h);
    }

    @DexIgnore
    public final void b() {
        synchronized (Boolean.valueOf(this.b)) {
            this.c.clear();
            this.b = false;
            cd6 cd6 = cd6.a;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009e, code lost:
        if (r3.hasTransport(1) != false) goto L_0x00c6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00c4, code lost:
        if (r3.getNetworkId() != -1) goto L_0x00c6;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0076  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00cb  */
    public final void c() {
        String str;
        boolean z;
        boolean z2;
        Context applicationContext;
        Network activeNetwork;
        NetworkCapabilities networkCapabilities;
        wj1 wj1 = this.a;
        if (wj1 == null && (!xj6.a(this.h.c())) && (!xj6.a(this.h.a())) && (!xj6.a(this.h.b()))) {
            oa1 oa1 = oa1.a;
            a(this.h);
        }
        if (wj1 == null) {
            str = "Invalid end point";
        } else {
            Context a2 = gk0.f.a();
            Object obj = null;
            ConnectivityManager connectivityManager = (ConnectivityManager) (a2 != null ? a2.getSystemService("connectivity") : null);
            if (Build.VERSION.SDK_INT >= 23) {
                if (!(connectivityManager == null || (activeNetwork = connectivityManager.getActiveNetwork()) == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)) == null || !networkCapabilities.hasCapability(18))) {
                    z = true;
                    if (z) {
                        str = "Network is not available";
                    } else {
                        if (this.k) {
                            Context a3 = gk0.f.a();
                            if (Build.VERSION.SDK_INT >= 23) {
                                if (a3 != null) {
                                    obj = a3.getSystemService("connectivity");
                                }
                                ConnectivityManager connectivityManager2 = (ConnectivityManager) obj;
                                if (connectivityManager2 != null) {
                                    Network activeNetwork2 = connectivityManager2.getActiveNetwork();
                                    if (activeNetwork2 != null) {
                                        NetworkCapabilities networkCapabilities2 = connectivityManager2.getNetworkCapabilities(activeNetwork2);
                                        if (networkCapabilities2 != null) {
                                        }
                                    }
                                }
                                z2 = false;
                                if (!z2) {
                                    str = "Wifi is not available";
                                }
                            } else {
                                if (!(a3 == null || (applicationContext = a3.getApplicationContext()) == null)) {
                                    obj = applicationContext.getSystemService(PlaceManager.PARAM_WIFI);
                                }
                                WifiManager wifiManager = (WifiManager) obj;
                                if (wifiManager != null) {
                                    WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                                    if (wifiManager.isWifiEnabled()) {
                                        if (connectionInfo != null) {
                                        }
                                    }
                                }
                                z2 = false;
                                if (!z2) {
                                }
                            }
                            z2 = true;
                            if (!z2) {
                            }
                        }
                        str = new String();
                    }
                }
            } else if (connectivityManager != null) {
                connectivityManager.getActiveNetworkInfo();
            }
            z = false;
            if (z) {
            }
        }
        if ((str.length() > 0) || wj1 == null) {
            oa1 oa12 = oa1.a;
            new Object[1][0] = str;
            b();
            return;
        }
        File file = (File) yd6.e(this.c);
        if (file != null) {
            JSONObject b2 = this.i.b(file);
            if (b2.length() > 0) {
                String c2 = this.h.c();
                String jSONObject = b2.toString();
                wg6.a(jSONObject, "logFileInJSON.toString()");
                wj1.a(c2, jSONObject).a(new kl1(file, this, wj1));
                return;
            }
            this.c.remove(file);
            file.delete();
            this.j.post(new bn1(this, wj1));
            return;
        }
        b();
    }

    @DexIgnore
    public final synchronized boolean a(ge0 ge0) {
        wj1 wj1;
        oa1 oa1 = oa1.a;
        StringBuilder b2 = ze0.b("updateEndPoint: url=");
        b2.append(ge0.c());
        b2.append(", ");
        b2.append("access=");
        b2.append(ge0.a());
        b2.append(", secret=");
        b2.append(ge0.b());
        b2.append('.');
        b2.toString();
        if (ge0.c().length() == 0) {
            return false;
        }
        this.h = ge0;
        String b3 = ge0.b();
        String a2 = ge0.a();
        OkHttpClient.b bVar = new OkHttpClient.b();
        bVar.b(new ql1(a2, b3));
        bVar.a(30000, TimeUnit.MILLISECONDS);
        bVar.b(60000, TimeUnit.MILLISECONDS);
        try {
            Retrofit.b bVar2 = new Retrofit.b();
            bVar2.a(bVar.a());
            bVar2.a(iy6.a());
            bVar2.a("http://localhost/");
            wj1 = (wj1) bVar2.a().a(wj1.class);
        } catch (Exception e2) {
            qs0.h.a(e2);
            wj1 = null;
        }
        if (wj1 != null) {
            this.a = wj1;
            oa1 oa12 = oa1.a;
            StringBuilder b4 = ze0.b("updateEndPoint: success, client=");
            b4.append(this.a);
            b4.toString();
            return true;
        }
        oa1 oa13 = oa1.a;
        return false;
    }

    @DexIgnore
    public final void a() {
        synchronized (Boolean.valueOf(this.b)) {
            if (!this.b) {
                this.b = true;
                this.c.clear();
                vd6.a(this.c, nd6.a(this.g.b()));
                c();
            }
        }
    }
}
