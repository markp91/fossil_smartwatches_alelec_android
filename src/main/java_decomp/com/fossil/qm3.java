package com.fossil;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qm3 {
    @DexIgnore
    public static /* final */ ko3<Object> a; // = new c();
    @DexIgnore
    public static /* final */ Iterator<Object> b; // = new d();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends pk3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Object[] c;
        @DexIgnore
        public /* final */ /* synthetic */ int d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i, int i2, Object[] objArr, int i3) {
            super(i, i2);
            this.c = objArr;
            this.d = i3;
        }

        @DexIgnore
        public T a(int i) {
            return this.c[this.d + i];
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends jo3<T> {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public /* final */ /* synthetic */ Object b;

        @DexIgnore
        public b(Object obj) {
            this.b = obj;
        }

        @DexIgnore
        public boolean hasNext() {
            return !this.a;
        }

        @DexIgnore
        public T next() {
            if (!this.a) {
                this.a = true;
                return this.b;
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends ko3<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public boolean hasPrevious() {
            return false;
        }

        @DexIgnore
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int nextIndex() {
            return 0;
        }

        @DexIgnore
        public Object previous() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public int previousIndex() {
            return -1;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Iterator<Object> {
        @DexIgnore
        public boolean hasNext() {
            return false;
        }

        @DexIgnore
        public Object next() {
            throw new NoSuchElementException();
        }

        @DexIgnore
        public void remove() {
            bl3.a(false);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends jo3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator a;

        @DexIgnore
        public e(Iterator it) {
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a.hasNext();
        }

        @DexIgnore
        public T next() {
            return this.a.next();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f extends qk3<T> {
        @DexIgnore
        public /* final */ /* synthetic */ Iterator c;
        @DexIgnore
        public /* final */ /* synthetic */ kk3 d;

        @DexIgnore
        public f(Iterator it, kk3 kk3) {
            this.c = it;
            this.d = kk3;
        }

        @DexIgnore
        public T a() {
            while (this.c.hasNext()) {
                T next = this.c.next();
                if (this.d.apply(next)) {
                    return next;
                }
            }
            return b();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class g extends ho3<F, T> {
        @DexIgnore
        public /* final */ /* synthetic */ ck3 b;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(Iterator it, ck3 ck3) {
            super(it);
            this.b = ck3;
        }

        @DexIgnore
        public T a(F f) {
            return this.b.apply(f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h<E> implements kn3<E> {
        @DexIgnore
        public /* final */ Iterator<? extends E> a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public E c;

        @DexIgnore
        public h(Iterator<? extends E> it) {
            jk3.a(it);
            this.a = it;
        }

        @DexIgnore
        public boolean hasNext() {
            return this.b || this.a.hasNext();
        }

        @DexIgnore
        public E next() {
            if (!this.b) {
                return this.a.next();
            }
            E e = this.c;
            this.b = false;
            this.c = null;
            return e;
        }

        @DexIgnore
        public E peek() {
            if (!this.b) {
                this.c = this.a.next();
                this.b = true;
            }
            return this.c;
        }

        @DexIgnore
        public void remove() {
            jk3.b(!this.b, (Object) "Can't remove after you've peeked at next");
            this.a.remove();
        }
    }

    @DexIgnore
    public static <T> jo3<T> a() {
        return b();
    }

    @DexIgnore
    public static <T> ko3<T> b() {
        return a;
    }

    @DexIgnore
    public static <T> Iterator<T> c() {
        return b;
    }

    @DexIgnore
    public static String d(Iterator<?> it) {
        ek3 ek3 = cl3.a;
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        ek3.a(sb, it);
        sb.append(']');
        return sb.toString();
    }

    @DexIgnore
    public static <T> jo3<T> e(Iterator<? extends T> it) {
        jk3.a(it);
        if (it instanceof jo3) {
            return (jo3) it;
        }
        return new e(it);
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Object obj) {
        return b(it, lk3.a(obj));
    }

    @DexIgnore
    public static boolean b(Iterator<?> it, Collection<?> collection) {
        return e(it, lk3.a(lk3.a(collection)));
    }

    @DexIgnore
    public static <T> jo3<T> c(Iterator<T> it, kk3<? super T> kk3) {
        jk3.a(it);
        jk3.a(kk3);
        return new f(it, kk3);
    }

    @DexIgnore
    public static <T> int d(Iterator<T> it, kk3<? super T> kk3) {
        jk3.a(kk3, (Object) "predicate");
        int i = 0;
        while (it.hasNext()) {
            if (kk3.apply(it.next())) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @DexIgnore
    public static boolean a(Iterator<?> it, Collection<?> collection) {
        return e(it, lk3.a(collection));
    }

    @DexIgnore
    public static <T> T b(Iterator<T> it) {
        T next = it.next();
        if (!it.hasNext()) {
            return next;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("expected one element but was: <");
        sb.append(next);
        for (int i = 0; i < 4 && it.hasNext(); i++) {
            sb.append(", ");
            sb.append(it.next());
        }
        if (it.hasNext()) {
            sb.append(", ...");
        }
        sb.append('>');
        throw new IllegalArgumentException(sb.toString());
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0006  */
    public static boolean a(Iterator<?> it, Iterator<?> it2) {
        while (it.hasNext()) {
            if (!it2.hasNext() || !gk3.a(it.next(), it2.next())) {
                return false;
            }
            while (it.hasNext()) {
            }
        }
        return !it2.hasNext();
    }

    @DexIgnore
    public static <T> kn3<T> c(Iterator<? extends T> it) {
        if (it instanceof h) {
            return (h) it;
        }
        return new h(it);
    }

    @DexIgnore
    public static <T> boolean e(Iterator<T> it, kk3<? super T> kk3) {
        jk3.a(kk3);
        boolean z = false;
        while (it.hasNext()) {
            if (kk3.apply(it.next())) {
                it.remove();
                z = true;
            }
        }
        return z;
    }

    @DexIgnore
    public static <T> boolean a(Collection<T> collection, Iterator<? extends T> it) {
        jk3.a(collection);
        jk3.a(it);
        boolean z = false;
        while (it.hasNext()) {
            z |= collection.add(it.next());
        }
        return z;
    }

    @DexIgnore
    public static <T> boolean b(Iterator<T> it, kk3<? super T> kk3) {
        return d(it, kk3) != -1;
    }

    @DexIgnore
    public static <T> T b(Iterator<? extends T> it, T t) {
        return it.hasNext() ? it.next() : t;
    }

    @DexIgnore
    public static <T> boolean a(Iterator<T> it, kk3<? super T> kk3) {
        jk3.a(kk3);
        while (it.hasNext()) {
            if (!kk3.apply(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static <F, T> Iterator<T> a(Iterator<F> it, ck3<? super F, ? extends T> ck3) {
        jk3.a(ck3);
        return new g(it, ck3);
    }

    @DexIgnore
    public static int a(Iterator<?> it, int i) {
        jk3.a(it);
        int i2 = 0;
        jk3.a(i >= 0, (Object) "numberToAdvance must be nonnegative");
        while (i2 < i && it.hasNext()) {
            it.next();
            i2++;
        }
        return i2;
    }

    @DexIgnore
    public static void a(Iterator<?> it) {
        jk3.a(it);
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
    }

    @DexIgnore
    @SafeVarargs
    public static <T> jo3<T> a(T... tArr) {
        return a(tArr, 0, tArr.length, 0);
    }

    @DexIgnore
    public static <T> ko3<T> a(T[] tArr, int i, int i2, int i3) {
        jk3.a(i2 >= 0);
        jk3.b(i, i + i2, tArr.length);
        jk3.b(i3, i2);
        if (i2 == 0) {
            return b();
        }
        return new a(i2, i3, tArr, i);
    }

    @DexIgnore
    public static <T> jo3<T> a(T t) {
        return new b(t);
    }
}
