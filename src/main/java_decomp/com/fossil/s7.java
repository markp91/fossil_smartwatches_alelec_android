package com.fossil;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Outline;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.Log;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s7 extends r7 {
    @DexIgnore
    public static Method h;

    @DexIgnore
    public s7(Drawable drawable) {
        super(drawable);
        d();
    }

    @DexIgnore
    public boolean b() {
        if (Build.VERSION.SDK_INT != 21) {
            return false;
        }
        Drawable drawable = this.f;
        if ((drawable instanceof GradientDrawable) || (drawable instanceof DrawableContainer) || (drawable instanceof InsetDrawable) || (drawable instanceof RippleDrawable)) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final void d() {
        if (h == null) {
            try {
                h = Drawable.class.getDeclaredMethod("isProjected", new Class[0]);
            } catch (Exception e) {
                Log.w("WrappedDrawableApi21", "Failed to retrieve Drawable#isProjected() method", e);
            }
        }
    }

    @DexIgnore
    public Rect getDirtyBounds() {
        return this.f.getDirtyBounds();
    }

    @DexIgnore
    public void getOutline(Outline outline) {
        this.f.getOutline(outline);
    }

    @DexIgnore
    public boolean isProjected() {
        Method method;
        Drawable drawable = this.f;
        if (!(drawable == null || (method = h) == null)) {
            try {
                return ((Boolean) method.invoke(drawable, new Object[0])).booleanValue();
            } catch (Exception e) {
                Log.w("WrappedDrawableApi21", "Error calling Drawable#isProjected() method", e);
            }
        }
        return false;
    }

    @DexIgnore
    public void setHotspot(float f, float f2) {
        this.f.setHotspot(f, f2);
    }

    @DexIgnore
    public void setHotspotBounds(int i, int i2, int i3, int i4) {
        this.f.setHotspotBounds(i, i2, i3, i4);
    }

    @DexIgnore
    public boolean setState(int[] iArr) {
        if (!super.setState(iArr)) {
            return false;
        }
        invalidateSelf();
        return true;
    }

    @DexIgnore
    public void setTint(int i) {
        if (b()) {
            super.setTint(i);
        } else {
            this.f.setTint(i);
        }
    }

    @DexIgnore
    public void setTintList(ColorStateList colorStateList) {
        if (b()) {
            super.setTintList(colorStateList);
        } else {
            this.f.setTintList(colorStateList);
        }
    }

    @DexIgnore
    public void setTintMode(PorterDuff.Mode mode) {
        if (b()) {
            super.setTintMode(mode);
        } else {
            this.f.setTintMode(mode);
        }
    }

    @DexIgnore
    public s7(t7 t7Var, Resources resources) {
        super(t7Var, resources);
        d();
    }
}
