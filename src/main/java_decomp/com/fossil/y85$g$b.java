package com.fossil;

import android.text.TextUtils;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.source.HybridPresetRepository;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import java.util.Comparator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class y85$g$b<T> implements ld<String> {
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter.g a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<T> implements ld<List<? extends HybridPreset>> {
        @DexIgnore
        public /* final */ /* synthetic */ y85$g$b a;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.y85$g$b$a$a")
        /* renamed from: com.fossil.y85$g$b$a$a  reason: collision with other inner class name */
        public static final class C0056a<T> implements Comparator<T> {
            @DexIgnore
            public final int compare(T t, T t2) {
                return ue6.a(Boolean.valueOf(((HybridPreset) t2).isActive()), Boolean.valueOf(((HybridPreset) t).isActive()));
            }
        }

        @DexIgnore
        public a(y85$g$b y85_g_b) {
            this.a = y85_g_b;
        }

        @DexIgnore
        /* renamed from: a */
        public final void onChanged(List<HybridPreset> list) {
            if (list != null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("HomeHybridCustomizePresenter", "onObserve on preset list change " + list);
                List<T> a2 = yd6.a(list, new C0056a());
                boolean a3 = wg6.a((Object) a2, (Object) this.a.a.this$0.g) ^ true;
                int itemCount = this.a.a.this$0.n.getItemCount();
                if (a3 || a2.size() != itemCount - 1) {
                    ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                    local2.d("HomeHybridCustomizePresenter", "process change - " + a3 + " - itemCount: " + itemCount + " - sortedPresetSize: " + a2.size());
                    if (this.a.a.this$0.k == 1) {
                        this.a.a.this$0.a((List<HybridPreset>) a2);
                    } else {
                        this.a.a.this$0.l = qc6.a(true, a2);
                    }
                }
            }
        }
    }

    @DexIgnore
    public y85$g$b(HomeHybridCustomizePresenter.g gVar) {
        this.a = gVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(String str) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("HomeHybridCustomizePresenter", "on active serial change " + str + " mCurrentHomeTab " + this.a.this$0.k);
        if (!TextUtils.isEmpty(str)) {
            HomeHybridCustomizePresenter homeHybridCustomizePresenter = this.a.this$0;
            HybridPresetRepository e = homeHybridCustomizePresenter.p;
            if (str != null) {
                homeHybridCustomizePresenter.e = e.getPresetListAsLiveData(str);
                this.a.this$0.e.a(this.a.this$0.n, new a(this));
                this.a.this$0.n.a(false);
                return;
            }
            wg6.a();
            throw null;
        }
        this.a.this$0.n.a(true);
    }
}
