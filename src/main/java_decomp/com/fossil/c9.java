package com.fossil;

import android.graphics.Point;
import android.view.MotionEvent;
import android.view.View;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c9 {
    @DexIgnore
    public /* final */ View a;
    @DexIgnore
    public /* final */ c b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e;
    @DexIgnore
    public /* final */ View.OnLongClickListener f; // = new a();
    @DexIgnore
    public /* final */ View.OnTouchListener g; // = new b();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnLongClickListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onLongClick(View view) {
            return c9.this.a(view);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements View.OnTouchListener {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public boolean onTouch(View view, MotionEvent motionEvent) {
            return c9.this.a(view, motionEvent);
        }
    }

    @DexIgnore
    public interface c {
        @DexIgnore
        boolean a(View view, c9 c9Var);
    }

    @DexIgnore
    public c9(View view, c cVar) {
        this.a = view;
        this.b = cVar;
    }

    @DexIgnore
    public void a() {
        this.a.setOnLongClickListener(this.f);
        this.a.setOnTouchListener(this.g);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0018, code lost:
        if (r2 != 3) goto L_0x004f;
     */
    @DexIgnore
    public boolean a(View view, MotionEvent motionEvent) {
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    if (j9.a(motionEvent, 8194) && (motionEvent.getButtonState() & 1) != 0 && !this.e && !(this.c == x && this.d == y)) {
                        this.c = x;
                        this.d = y;
                        this.e = this.b.a(view, this);
                        return this.e;
                    }
                }
            }
            this.e = false;
        } else {
            this.c = x;
            this.d = y;
        }
        return false;
    }

    @DexIgnore
    public boolean a(View view) {
        return this.b.a(view, this);
    }

    @DexIgnore
    public void a(Point point) {
        point.set(this.c, this.d);
    }
}
