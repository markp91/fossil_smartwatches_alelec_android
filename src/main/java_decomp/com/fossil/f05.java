package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersActivity;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationwatchreminders.NotificationWatchRemindersPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class f05 implements MembersInjector<NotificationWatchRemindersActivity> {
    @DexIgnore
    public static void a(NotificationWatchRemindersActivity notificationWatchRemindersActivity, NotificationWatchRemindersPresenter notificationWatchRemindersPresenter) {
        notificationWatchRemindersActivity.B = notificationWatchRemindersPresenter;
    }
}
