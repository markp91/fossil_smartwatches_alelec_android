package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fe3 implements Parcelable.Creator<ee3> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        ArrayList<te3> arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                str = f22.e(parcel, a);
            } else if (a2 != 3) {
                f22.v(parcel, a);
            } else {
                arrayList = f22.c(parcel, a, te3.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new ee3(str, arrayList);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new ee3[i];
    }
}
