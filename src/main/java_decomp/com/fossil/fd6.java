package com.fossil;

import java.util.Collection;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fd6<E> implements Collection<E>, ph6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements hg6<E, CharSequence> {
        @DexIgnore
        public /* final */ /* synthetic */ fd6 this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fd6 fd6) {
            super(1);
            this.this$0 = fd6;
        }

        @DexIgnore
        public final CharSequence invoke(E e) {
            return e == this.this$0 ? "(this Collection)" : String.valueOf(e);
        }
    }

    @DexIgnore
    public abstract int a();

    @DexIgnore
    public boolean add(E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean contains(Object obj) {
        if (isEmpty()) {
            return false;
        }
        for (Object a2 : this) {
            if (wg6.a(a2, obj)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it = collection.iterator();
        while (it.hasNext()) {
            if (!contains(it.next())) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public boolean isEmpty() {
        return size() == 0;
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public String toString() {
        return yd6.a(this, ", ", "[", "]", 0, (CharSequence) null, new a(this), 24, (Object) null);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        wg6.b(tArr, "array");
        T[] a2 = pg6.a(this, tArr);
        if (a2 != null) {
            return a2;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }
}
