package com.fossil;

import com.portfolio.platform.data.source.CategoryRepository;
import com.portfolio.platform.uirenew.home.customize.diana.complications.ComplicationsPresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u45 implements Factory<t45> {
    @DexIgnore
    public static ComplicationsPresenter a(q45 q45, CategoryRepository categoryRepository) {
        return new ComplicationsPresenter(q45, categoryRepository);
    }
}
