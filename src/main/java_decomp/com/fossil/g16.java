package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum g16 {
    NO_CACHE(1),
    NO_STORE(2),
    OFFLINE(4);
    
    @DexIgnore
    public /* final */ int index;

    @DexIgnore
    public g16(int i) {
        this.index = i;
    }

    @DexIgnore
    public static boolean isOfflineOnly(int i) {
        return (i & OFFLINE.index) != 0;
    }

    @DexIgnore
    public static boolean shouldReadFromDiskCache(int i) {
        return (i & NO_CACHE.index) == 0;
    }

    @DexIgnore
    public static boolean shouldWriteToDiskCache(int i) {
        return (i & NO_STORE.index) == 0;
    }
}
