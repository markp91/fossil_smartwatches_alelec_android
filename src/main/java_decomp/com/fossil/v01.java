package com.fossil;

import java.util.ArrayList;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class v01 extends if1 {
    @DexIgnore
    public /* final */ ArrayList<hl1> B; // = cw0.a(this.i, (ArrayList<hl1>) qd6.a(new hl1[]{hl1.FILE_CONFIG, hl1.TRANSFER_DATA}));
    @DexIgnore
    public /* final */ short C;

    @DexIgnore
    public v01(ue1 ue1, q41 q41, eh1 eh1, short s, String str) {
        super(ue1, q41, eh1, str);
        this.C = s;
    }

    @DexIgnore
    public boolean a(if1 if1) {
        return !(if1 instanceof v01) || ((v01) if1).C != this.C;
    }

    @DexIgnore
    public ArrayList<hl1> f() {
        return this.B;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(super.i(), bm0.FILE_HANDLE, (Object) cw0.a(this.C)), bm0.FILE_HANDLE_DESCRIPTION, (Object) g01.d.a(this.C));
    }

    @DexIgnore
    public boolean a(qv0 qv0) {
        lx0 lx0 = qv0 != null ? qv0.x : null;
        if (lx0 != null && bz0.a[lx0.ordinal()] == 1) {
            return true;
        }
        return false;
    }
}
