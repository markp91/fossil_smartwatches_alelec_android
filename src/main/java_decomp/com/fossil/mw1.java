package com.fossil;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mw1 implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {
    @DexIgnore
    public static /* final */ mw1 e; // = new mw1();
    @DexIgnore
    public /* final */ AtomicBoolean a; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ AtomicBoolean b; // = new AtomicBoolean();
    @DexIgnore
    public /* final */ ArrayList<a> c; // = new ArrayList<>();
    @DexIgnore
    public boolean d; // = false;

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(boolean z);
    }

    @DexIgnore
    public static void a(Application application) {
        synchronized (e) {
            if (!e.d) {
                application.registerActivityLifecycleCallbacks(e);
                application.registerComponentCallbacks(e);
                e.d = true;
            }
        }
    }

    @DexIgnore
    public static mw1 b() {
        return e;
    }

    @DexIgnore
    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.a.compareAndSet(true, false);
        this.b.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    @DexIgnore
    public final void onActivityDestroyed(Activity activity) {
    }

    @DexIgnore
    public final void onActivityPaused(Activity activity) {
    }

    @DexIgnore
    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.a.compareAndSet(true, false);
        this.b.set(true);
        if (compareAndSet) {
            a(false);
        }
    }

    @DexIgnore
    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    @DexIgnore
    public final void onActivityStarted(Activity activity) {
    }

    @DexIgnore
    public final void onActivityStopped(Activity activity) {
    }

    @DexIgnore
    public final void onConfigurationChanged(Configuration configuration) {
    }

    @DexIgnore
    public final void onLowMemory() {
    }

    @DexIgnore
    public final void onTrimMemory(int i) {
        if (i == 20 && this.a.compareAndSet(false, true)) {
            this.b.set(true);
            a(true);
        }
    }

    @DexIgnore
    @TargetApi(16)
    public final boolean b(boolean z) {
        if (!this.b.get()) {
            if (!s42.c()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.b.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.a.set(true);
            }
        }
        return a();
    }

    @DexIgnore
    public final boolean a() {
        return this.a.get();
    }

    @DexIgnore
    public final void a(a aVar) {
        synchronized (e) {
            this.c.add(aVar);
        }
    }

    @DexIgnore
    public final void a(boolean z) {
        synchronized (e) {
            ArrayList<a> arrayList = this.c;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                a aVar = arrayList.get(i);
                i++;
                aVar.a(z);
            }
        }
    }
}
