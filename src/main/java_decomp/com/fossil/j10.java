package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j10 extends a10<j10> {
    @DexIgnore
    public /* final */ String c;

    @DexIgnore
    public j10(String str) {
        if (str != null) {
            this.c = this.a.a(str);
            return;
        }
        throw new NullPointerException("eventName must not be null");
    }

    @DexIgnore
    public String b() {
        return this.c;
    }

    @DexIgnore
    public String toString() {
        return "{eventName:\"" + this.c + '\"' + ", customAttributes:" + this.b + "}";
    }
}
