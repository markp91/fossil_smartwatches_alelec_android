package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s91 extends xg6 implements ig6<qv0, Float, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ hm1 a;
    @DexIgnore
    public /* final */ /* synthetic */ byte[] b;
    @DexIgnore
    public /* final */ /* synthetic */ long c;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public s91(hm1 hm1, byte[] bArr, long j) {
        super(2);
        this.a = hm1;
        this.b = bArr;
        this.c = j;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        qv0 qv0 = (qv0) obj;
        hm1 hm1 = this.a;
        hm1.D = this.c + ((long) (((Number) obj2).floatValue() * ((float) this.b.length)));
        float f = (((float) hm1.D) * 1.0f) / ((float) hm1.C);
        if (Math.abs(f - hm1.E) > this.a.P || f == 1.0f) {
            hm1 hm12 = this.a;
            hm12.E = f;
            hm12.a(f);
        }
        return cd6.a;
    }
}
