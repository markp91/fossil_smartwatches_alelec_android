package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ym2 {
    DOUBLE(0, an2.SCALAR, sn2.DOUBLE),
    FLOAT(1, an2.SCALAR, sn2.FLOAT),
    INT64(2, an2.SCALAR, sn2.LONG),
    UINT64(3, an2.SCALAR, sn2.LONG),
    INT32(4, an2.SCALAR, sn2.INT),
    FIXED64(5, an2.SCALAR, sn2.LONG),
    FIXED32(6, an2.SCALAR, sn2.INT),
    BOOL(7, an2.SCALAR, sn2.BOOLEAN),
    STRING(8, an2.SCALAR, sn2.STRING),
    MESSAGE(9, an2.SCALAR, sn2.MESSAGE),
    BYTES(10, an2.SCALAR, sn2.BYTE_STRING),
    UINT32(11, an2.SCALAR, sn2.INT),
    ENUM(12, an2.SCALAR, sn2.ENUM),
    SFIXED32(13, an2.SCALAR, sn2.INT),
    SFIXED64(14, an2.SCALAR, sn2.LONG),
    SINT32(15, an2.SCALAR, sn2.INT),
    SINT64(16, an2.SCALAR, sn2.LONG),
    GROUP(17, an2.SCALAR, sn2.MESSAGE),
    DOUBLE_LIST(18, an2.VECTOR, sn2.DOUBLE),
    FLOAT_LIST(19, an2.VECTOR, sn2.FLOAT),
    INT64_LIST(20, an2.VECTOR, sn2.LONG),
    UINT64_LIST(21, an2.VECTOR, sn2.LONG),
    INT32_LIST(22, an2.VECTOR, sn2.INT),
    FIXED64_LIST(23, an2.VECTOR, sn2.LONG),
    FIXED32_LIST(24, an2.VECTOR, sn2.INT),
    BOOL_LIST(25, an2.VECTOR, sn2.BOOLEAN),
    STRING_LIST(26, an2.VECTOR, sn2.STRING),
    MESSAGE_LIST(27, an2.VECTOR, sn2.MESSAGE),
    BYTES_LIST(28, an2.VECTOR, sn2.BYTE_STRING),
    UINT32_LIST(29, an2.VECTOR, sn2.INT),
    ENUM_LIST(30, an2.VECTOR, sn2.ENUM),
    SFIXED32_LIST(31, an2.VECTOR, sn2.INT),
    SFIXED64_LIST(32, an2.VECTOR, sn2.LONG),
    SINT32_LIST(33, an2.VECTOR, sn2.INT),
    SINT64_LIST(34, an2.VECTOR, sn2.LONG),
    DOUBLE_LIST_PACKED(35, an2.PACKED_VECTOR, sn2.DOUBLE),
    FLOAT_LIST_PACKED(36, an2.PACKED_VECTOR, sn2.FLOAT),
    INT64_LIST_PACKED(37, an2.PACKED_VECTOR, sn2.LONG),
    UINT64_LIST_PACKED(38, an2.PACKED_VECTOR, sn2.LONG),
    INT32_LIST_PACKED(39, an2.PACKED_VECTOR, sn2.INT),
    FIXED64_LIST_PACKED(40, an2.PACKED_VECTOR, sn2.LONG),
    FIXED32_LIST_PACKED(41, an2.PACKED_VECTOR, sn2.INT),
    BOOL_LIST_PACKED(42, an2.PACKED_VECTOR, sn2.BOOLEAN),
    UINT32_LIST_PACKED(43, an2.PACKED_VECTOR, sn2.INT),
    ENUM_LIST_PACKED(44, an2.PACKED_VECTOR, sn2.ENUM),
    SFIXED32_LIST_PACKED(45, an2.PACKED_VECTOR, sn2.INT),
    SFIXED64_LIST_PACKED(46, an2.PACKED_VECTOR, sn2.LONG),
    SINT32_LIST_PACKED(47, an2.PACKED_VECTOR, sn2.INT),
    SINT64_LIST_PACKED(48, an2.PACKED_VECTOR, sn2.LONG),
    GROUP_LIST(49, an2.VECTOR, sn2.MESSAGE),
    MAP(50, an2.MAP, sn2.VOID);
    
    @DexIgnore
    public static /* final */ ym2[] b0; // = null;
    @DexIgnore
    public /* final */ sn2 zzaz;
    @DexIgnore
    public /* final */ int zzba;
    @DexIgnore
    public /* final */ an2 zzbb;
    @DexIgnore
    public /* final */ Class<?> zzbc;
    @DexIgnore
    public /* final */ boolean zzbd;

    /*
    static {
        int i;
        ym2[] values = values();
        b0 = new ym2[values.length];
        for (ym2 ym2 : values) {
            b0[ym2.zzba] = ym2;
        }
    }
    */

    @DexIgnore
    public ym2(int i, an2 an2, sn2 sn2) {
        int i2;
        this.zzba = i;
        this.zzbb = an2;
        this.zzaz = sn2;
        int i3 = cn2.a[an2.ordinal()];
        if (i3 == 1) {
            this.zzbc = sn2.zza();
        } else if (i3 != 2) {
            this.zzbc = null;
        } else {
            this.zzbc = sn2.zza();
        }
        boolean z = false;
        if (!(an2 != an2.SCALAR || (i2 = cn2.b[sn2.ordinal()]) == 1 || i2 == 2 || i2 == 3)) {
            z = true;
        }
        this.zzbd = z;
    }

    @DexIgnore
    public final int zza() {
        return this.zzba;
    }
}
