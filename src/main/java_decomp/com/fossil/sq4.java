package com.fossil;

import android.content.Intent;
import android.content.res.Resources;
import android.view.View;
import android.widget.TextView;
import androidx.core.app.SharedElementCallback;
import com.portfolio.platform.PortfolioApp;
import java.util.Iterator;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sq4 extends SharedElementCallback {
    @DexIgnore
    public float b;
    @DexIgnore
    public float c;
    @DexIgnore
    public /* final */ Intent d;
    @DexIgnore
    public /* final */ PortfolioApp e;

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public sq4(Intent intent, PortfolioApp portfolioApp) {
        wg6.b(intent, "intent");
        wg6.b(portfolioApp, "mApp");
        this.d = intent;
        this.e = portfolioApp;
        Resources resources = this.e.getResources();
        this.b = (float) resources.getDimensionPixelSize(2131165725);
        this.c = (float) resources.getDimensionPixelSize(2131165721);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r6v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void a(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        Integer num;
        if (list2 != null) {
            Iterator<? extends View> it = list2.iterator();
            while (true) {
                num = null;
                if (!it.hasNext()) {
                    break;
                }
                ((View) it.next()).setTag(2131363004, (Object) null);
            }
            if (list != null) {
                num = Integer.valueOf(list.indexOf(this.e.getString(2131887348)));
            }
            if (num != null) {
                Object obj = list2.get(num.intValue());
                if (obj != null) {
                    TextView textView = (TextView) obj;
                    int measuredWidth = textView.getMeasuredWidth();
                    int measuredHeight = textView.getMeasuredHeight();
                    textView.setTextSize(0, this.c);
                    textView.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
                    int measuredWidth2 = (textView.getMeasuredWidth() - measuredWidth) / 2;
                    int measuredHeight2 = (textView.getMeasuredHeight() - measuredHeight) / 2;
                    textView.layout(textView.getLeft() - measuredWidth2, textView.getTop() - measuredHeight2, textView.getRight() + measuredWidth2, textView.getBottom() + measuredHeight2);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r9v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    public void b(List<String> list, List<? extends View> list2, List<? extends View> list3) {
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                String str = list.get(i);
                if (this.d.hasExtra(str) && list2 != null) {
                    ((View) list2.get(i)).setTag(2131363004, this.d.getBundleExtra(str));
                }
            }
            int indexOf = list.indexOf(this.e.getString(2131887348));
            if (list2 != null) {
                Object obj = list2.get(indexOf);
                if (obj != null) {
                    ((TextView) obj).setTextSize(0, this.b);
                    return;
                }
                throw new rc6("null cannot be cast to non-null type android.widget.TextView");
            }
        }
    }
}
