package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us4 implements Factory<ts4> {
    @DexIgnore
    public static ts4 a(NotificationsRepository notificationsRepository) {
        return new ts4(notificationsRepository);
    }
}
