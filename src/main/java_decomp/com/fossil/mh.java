package com.fossil;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import androidx.room.MultiInstanceInvalidationService;
import com.fossil.ih;
import com.fossil.jh;
import com.fossil.lh;
import java.util.Set;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class mh {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public /* final */ lh d;
    @DexIgnore
    public /* final */ lh.c e;
    @DexIgnore
    public jh f;
    @DexIgnore
    public /* final */ Executor g;
    @DexIgnore
    public /* final */ ih h; // = new a();
    @DexIgnore
    public /* final */ AtomicBoolean i; // = new AtomicBoolean(false);
    @DexIgnore
    public /* final */ ServiceConnection j; // = new b();
    @DexIgnore
    public /* final */ Runnable k; // = new c();
    @DexIgnore
    public /* final */ Runnable l; // = new d();
    @DexIgnore
    public /* final */ Runnable m; // = new e();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ih.a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.mh$a$a")
        /* renamed from: com.fossil.mh$a$a  reason: collision with other inner class name */
        public class C0030a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ String[] a;

            @DexIgnore
            public C0030a(String[] strArr) {
                this.a = strArr;
            }

            @DexIgnore
            public void run() {
                mh.this.d.a(this.a);
            }
        }

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void a(String[] strArr) {
            mh.this.g.execute(new C0030a(strArr));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements ServiceConnection {
        @DexIgnore
        public b() {
        }

        @DexIgnore
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mh.this.f = jh.a.a(iBinder);
            mh mhVar = mh.this;
            mhVar.g.execute(mhVar.k);
        }

        @DexIgnore
        public void onServiceDisconnected(ComponentName componentName) {
            mh mhVar = mh.this;
            mhVar.g.execute(mhVar.l);
            mh.this.f = null;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Runnable {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public void run() {
            try {
                jh jhVar = mh.this.f;
                if (jhVar != null) {
                    mh.this.c = jhVar.a(mh.this.h, mh.this.b);
                    mh.this.d.a(mh.this.e);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot register multi-instance invalidation callback", e);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements Runnable {
        @DexIgnore
        public d() {
        }

        @DexIgnore
        public void run() {
            mh mhVar = mh.this;
            mhVar.d.c(mhVar.e);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements Runnable {
        @DexIgnore
        public e() {
        }

        @DexIgnore
        public void run() {
            mh mhVar = mh.this;
            mhVar.d.c(mhVar.e);
            try {
                jh jhVar = mh.this.f;
                if (jhVar != null) {
                    jhVar.a(mh.this.h, mh.this.c);
                }
            } catch (RemoteException e) {
                Log.w("ROOM", "Cannot unregister multi-instance invalidation callback", e);
            }
            mh mhVar2 = mh.this;
            mhVar2.a.unbindService(mhVar2.j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends lh.c {
        @DexIgnore
        public f(String[] strArr) {
            super(strArr);
        }

        @DexIgnore
        public boolean isRemote() {
            return true;
        }

        @DexIgnore
        public void onInvalidated(Set<String> set) {
            if (!mh.this.i.get()) {
                try {
                    jh jhVar = mh.this.f;
                    if (jhVar != null) {
                        jhVar.a(mh.this.c, (String[]) set.toArray(new String[0]));
                    }
                } catch (RemoteException e) {
                    Log.w("ROOM", "Cannot broadcast invalidation", e);
                }
            }
        }
    }

    @DexIgnore
    public mh(Context context, String str, lh lhVar, Executor executor) {
        this.a = context.getApplicationContext();
        this.b = str;
        this.d = lhVar;
        this.g = executor;
        this.e = new f(lhVar.b);
        this.a.bindService(new Intent(this.a, MultiInstanceInvalidationService.class), this.j, 1);
    }

    @DexIgnore
    public void a() {
        if (this.i.compareAndSet(false, true)) {
            this.g.execute(this.m);
        }
    }
}
