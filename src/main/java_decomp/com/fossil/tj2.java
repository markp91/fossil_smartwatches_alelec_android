package com.fossil;

import com.fossil.fn2;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tj2 extends fn2<tj2, a> implements to2 {
    @DexIgnore
    public static /* final */ tj2 zzf;
    @DexIgnore
    public static volatile yo2<tj2> zzg;
    @DexIgnore
    public int zzc;
    @DexIgnore
    public int zzd;
    @DexIgnore
    public on2 zze; // = fn2.l();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends fn2.b<tj2, a> implements to2 {
        @DexIgnore
        public a() {
            super(tj2.zzf);
        }

        @DexIgnore
        public final a a(int i) {
            f();
            ((tj2) this.b).c(i);
            return this;
        }

        @DexIgnore
        public final a j() {
            f();
            ((tj2) this.b).s();
            return this;
        }

        @DexIgnore
        public /* synthetic */ a(vj2 vj2) {
            this();
        }

        @DexIgnore
        public final a a(long j) {
            f();
            ((tj2) this.b).a(j);
            return this;
        }

        @DexIgnore
        public final a a(Iterable<? extends Long> iterable) {
            f();
            ((tj2) this.b).a(iterable);
            return this;
        }
    }

    /*
    static {
        tj2 tj2 = new tj2();
        zzf = tj2;
        fn2.a(tj2.class, tj2);
    }
    */

    @DexIgnore
    public static a t() {
        return (a) zzf.h();
    }

    @DexIgnore
    public final void a(long j) {
        r();
        this.zze.a(j);
    }

    @DexIgnore
    public final long b(int i) {
        return this.zze.zzb(i);
    }

    @DexIgnore
    public final void c(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    @DexIgnore
    public final boolean n() {
        return (this.zzc & 1) != 0;
    }

    @DexIgnore
    public final int o() {
        return this.zzd;
    }

    @DexIgnore
    public final List<Long> p() {
        return this.zze;
    }

    @DexIgnore
    public final int q() {
        return this.zze.size();
    }

    @DexIgnore
    public final void r() {
        if (!this.zze.zza()) {
            this.zze = fn2.a(this.zze);
        }
    }

    @DexIgnore
    public final void s() {
        this.zze = fn2.l();
    }

    @DexIgnore
    public final void a(Iterable<? extends Long> iterable) {
        r();
        ql2.a(iterable, this.zze);
    }

    @DexIgnore
    public final Object a(int i, Object obj, Object obj2) {
        switch (vj2.a[i - 1]) {
            case 1:
                return new tj2();
            case 2:
                return new a((vj2) null);
            case 3:
                return fn2.a((ro2) zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\u0004\u0000\u0002\u0014", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                yo2<tj2> yo2 = zzg;
                if (yo2 == null) {
                    synchronized (tj2.class) {
                        yo2 = zzg;
                        if (yo2 == null) {
                            yo2 = new fn2.a<>(zzf);
                            zzg = yo2;
                        }
                    }
                }
                return yo2;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
