package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.diana.heartrate.HeartRateSample;
import com.portfolio.platform.uirenew.home.details.heartrate.HeartRateDetailPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class si5$h$b<T> implements ld<yx5<? extends List<HeartRateSample>>> {
    @DexIgnore
    public /* final */ /* synthetic */ HeartRateDetailPresenter.h a;

    @DexIgnore
    public si5$h$b(HeartRateDetailPresenter.h hVar) {
        this.a = hVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(yx5<? extends List<HeartRateSample>> yx5) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("start - sampleTransformations -- status=");
        wh4 wh4 = null;
        sb.append(yx5 != null ? yx5.f() : null);
        sb.append(", resource=");
        sb.append(yx5 != null ? (List) yx5.d() : null);
        sb.append(", status=");
        if (yx5 != null) {
            wh4 = yx5.f();
        }
        sb.append(wh4);
        local.d("HeartRateDetailPresenter", sb.toString());
        if (yx5 != null && yx5.f() != wh4.DATABASE_LOADING) {
            this.a.this$0.i = (List) yx5.d();
            rm6 unused = this.a.this$0.m();
        }
    }
}
