package com.fossil;

import com.portfolio.platform.data.model.room.microapp.MicroApp;
import com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.hybrid.HomeHybridCustomizePresenter$start$1$1", f = "HomeHybridCustomizePresenter.kt", l = {}, m = "invokeSuspend")
public final class y85$g$a extends sf6 implements ig6<il6, xe6<? super List<? extends MicroApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeHybridCustomizePresenter.g this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public y85$g$a(HomeHybridCustomizePresenter.g gVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = gVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        y85$g$a y85_g_a = new y85$g$a(this.this$0, xe6);
        y85_g_a.p$ = (il6) obj;
        return y85_g_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((y85$g$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.o.getAllMicroApp(this.this$0.this$0.m.e());
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
