package com.fossil;

import com.fossil.yv1;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.BasePendingResult;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class k02 {
    @DexIgnore
    public /* final */ Map<BasePendingResult<?>, Boolean> a; // = Collections.synchronizedMap(new WeakHashMap());
    @DexIgnore
    public /* final */ Map<rc3<?>, Boolean> b; // = Collections.synchronizedMap(new WeakHashMap());

    @DexIgnore
    public final void a(BasePendingResult<? extends ew1> basePendingResult, boolean z) {
        this.a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.a((yv1.a) new gx1(this, basePendingResult));
    }

    @DexIgnore
    public final void b() {
        a(false, qw1.n);
    }

    @DexIgnore
    public final void c() {
        a(true, lz1.d);
    }

    @DexIgnore
    public final <TResult> void a(rc3<TResult> rc3, boolean z) {
        this.b.put(rc3, Boolean.valueOf(z));
        rc3.a().a(new fx1(this, rc3));
    }

    @DexIgnore
    public final boolean a() {
        return !this.a.isEmpty() || !this.b.isEmpty();
    }

    @DexIgnore
    public final void a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.a) {
            hashMap = new HashMap(this.a);
        }
        synchronized (this.b) {
            hashMap2 = new HashMap(this.b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).b(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((rc3) entry2.getKey()).b((Exception) new sv1(status));
            }
        }
    }
}
