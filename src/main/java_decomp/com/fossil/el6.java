package com.fossil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import kotlinx.coroutines.CoroutineExceptionHandler;
import kotlinx.coroutines.android.AndroidExceptionPreHandler;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class el6 {
    @DexIgnore
    public static /* final */ List<CoroutineExceptionHandler> a;

    /*
    static {
        Iterator it = Arrays.asList(new CoroutineExceptionHandler[]{new AndroidExceptionPreHandler()}).iterator();
        wg6.a((Object) it, "ServiceLoader.load(\n    \u2026.classLoader\n).iterator()");
        a = aj6.f(yi6.a(it));
    }
    */

    @DexIgnore
    public static final void a(af6 af6, Throwable th) {
        wg6.b(af6, "context");
        wg6.b(th, "exception");
        for (CoroutineExceptionHandler handleException : a) {
            try {
                handleException.handleException(af6, th);
            } catch (Throwable th2) {
                Thread currentThread = Thread.currentThread();
                wg6.a((Object) currentThread, "currentThread");
                currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, fl6.a(th, th2));
            }
        }
        Thread currentThread2 = Thread.currentThread();
        wg6.a((Object) currentThread2, "currentThread");
        currentThread2.getUncaughtExceptionHandler().uncaughtException(currentThread2, th);
    }
}
