package com.fossil;

import android.util.Base64;
import com.fossil.kp1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class rp1 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {
        @DexIgnore
        public abstract a a(go1 go1);

        @DexIgnore
        public abstract a a(String str);

        @DexIgnore
        public abstract a a(byte[] bArr);

        @DexIgnore
        public abstract rp1 a();
    }

    @DexIgnore
    public static a d() {
        kp1.b bVar = new kp1.b();
        bVar.a(go1.DEFAULT);
        return bVar;
    }

    @DexIgnore
    public rp1 a(go1 go1) {
        a d = d();
        d.a(a());
        d.a(go1);
        d.a(b());
        return d.a();
    }

    @DexIgnore
    public abstract String a();

    @DexIgnore
    public abstract byte[] b();

    @DexIgnore
    public abstract go1 c();

    @DexIgnore
    public final String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = a();
        objArr[1] = c();
        objArr[2] = b() == null ? "" : Base64.encodeToString(b(), 2);
        return String.format("TransportContext(%s, %s, %s)", objArr);
    }
}
