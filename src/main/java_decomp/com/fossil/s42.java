package com.fossil;

import android.os.Build;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class s42 {
    @DexIgnore
    public static boolean a() {
        return true;
    }

    @DexIgnore
    public static boolean b() {
        return true;
    }

    @DexIgnore
    public static boolean c() {
        return true;
    }

    @DexIgnore
    public static boolean d() {
        return Build.VERSION.SDK_INT >= 18;
    }

    @DexIgnore
    public static boolean e() {
        return Build.VERSION.SDK_INT >= 19;
    }

    @DexIgnore
    public static boolean f() {
        return Build.VERSION.SDK_INT >= 20;
    }

    @DexIgnore
    public static boolean g() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @DexIgnore
    public static boolean h() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @DexIgnore
    public static boolean i() {
        return Build.VERSION.SDK_INT >= 26;
    }

    @DexIgnore
    public static boolean j() {
        return Build.VERSION.SDK_INT >= 28;
    }
}
