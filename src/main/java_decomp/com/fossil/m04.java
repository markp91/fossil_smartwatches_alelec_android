package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m04 {
    @DexIgnore
    public static /* final */ int[][] a; // = {new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] b; // = {new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};
    @DexIgnore
    public static /* final */ int[][] c; // = {new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, 102, -1, -1}, new int[]{6, 28, 54, 80, 106, -1, -1}, new int[]{6, 32, 58, 84, 110, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, 102, 126, -1}, new int[]{6, 26, 52, 78, 104, 130, -1}, new int[]{6, 30, 56, 82, 108, 134, -1}, new int[]{6, 34, 60, 86, 112, 138, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, 102, 126, 150}, new int[]{6, 24, 50, 76, 102, 128, 154}, new int[]{6, 28, 54, 80, 106, 132, 158}, new int[]{6, 32, 58, 84, 110, 136, 162}, new int[]{6, 26, 54, 82, 110, 138, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};
    @DexIgnore
    public static /* final */ int[][] d; // = {new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};

    @DexIgnore
    public static void a(j04 j04) {
        j04.a((byte) -1);
    }

    @DexIgnore
    public static void b(j04 j04) throws yx3 {
        if (j04.a(8, j04.b() - 8) != 0) {
            j04.a(8, j04.b() - 8, 1);
            return;
        }
        throw new yx3();
    }

    @DexIgnore
    public static boolean b(int i) {
        return i == -1;
    }

    @DexIgnore
    public static void c(h04 h04, j04 j04) throws yx3 {
        if (h04.c() >= 7) {
            hy3 hy3 = new hy3();
            a(h04, hy3);
            int i = 0;
            int i2 = 17;
            while (i < 6) {
                int i3 = i2;
                for (int i4 = 0; i4 < 3; i4++) {
                    boolean b2 = hy3.b(i3);
                    i3--;
                    j04.a(i, (j04.b() - 11) + i4, b2);
                    j04.a((j04.b() - 11) + i4, i, b2);
                }
                i++;
                i2 = i3;
            }
        }
    }

    @DexIgnore
    public static void d(j04 j04) {
        int i = 8;
        while (i < j04.c() - 8) {
            int i2 = i + 1;
            int i3 = i2 % 2;
            if (b((int) j04.a(i, 6))) {
                j04.a(i, 6, i3);
            }
            if (b((int) j04.a(6, i))) {
                j04.a(6, i, i3);
            }
            i = i2;
        }
    }

    @DexIgnore
    public static void a(hy3 hy3, f04 f04, h04 h04, int i, j04 j04) throws yx3 {
        a(j04);
        a(h04, j04);
        a(f04, i, j04);
        c(h04, j04);
        a(hy3, i, j04);
    }

    @DexIgnore
    public static void b(int i, int i2, j04 j04) {
        for (int i3 = 0; i3 < 5; i3++) {
            for (int i4 = 0; i4 < 5; i4++) {
                j04.a(i + i4, i2 + i3, b[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void b(h04 h04, j04 j04) {
        if (h04.c() >= 2) {
            int c2 = h04.c() - 1;
            int[][] iArr = c;
            int[] iArr2 = iArr[c2];
            int length = iArr[c2].length;
            for (int i = 0; i < length; i++) {
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = iArr2[i];
                    int i4 = iArr2[i2];
                    if (!(i4 == -1 || i3 == -1 || !b((int) j04.a(i4, i3)))) {
                        b(i4 - 2, i3 - 2, j04);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static void a(h04 h04, j04 j04) throws yx3 {
        c(j04);
        b(j04);
        b(h04, j04);
        d(j04);
    }

    @DexIgnore
    public static void c(int i, int i2, j04 j04) {
        for (int i3 = 0; i3 < 7; i3++) {
            for (int i4 = 0; i4 < 7; i4++) {
                j04.a(i + i4, i2 + i3, a[i3][i4]);
            }
        }
    }

    @DexIgnore
    public static void d(int i, int i2, j04 j04) throws yx3 {
        int i3 = 0;
        while (i3 < 7) {
            int i4 = i2 + i3;
            if (b((int) j04.a(i, i4))) {
                j04.a(i, i4, 0);
                i3++;
            } else {
                throw new yx3();
            }
        }
    }

    @DexIgnore
    public static void c(j04 j04) throws yx3 {
        int length = a[0].length;
        c(0, 0, j04);
        c(j04.c() - length, 0, j04);
        c(0, j04.c() - length, j04);
        a(0, 7, j04);
        a(j04.c() - 8, 7, j04);
        a(0, j04.c() - 8, j04);
        d(7, 0, j04);
        d((j04.b() - 7) - 1, 0, j04);
        d(7, j04.b() - 7, j04);
    }

    @DexIgnore
    public static void a(f04 f04, int i, j04 j04) throws yx3 {
        hy3 hy3 = new hy3();
        a(f04, i, hy3);
        for (int i2 = 0; i2 < hy3.a(); i2++) {
            boolean b2 = hy3.b((hy3.a() - 1) - i2);
            int[][] iArr = d;
            j04.a(iArr[i2][0], iArr[i2][1], b2);
            if (i2 < 8) {
                j04.a((j04.c() - i2) - 1, 8, b2);
            } else {
                j04.a(8, (j04.b() - 7) + (i2 - 8), b2);
            }
        }
    }

    @DexIgnore
    public static void a(hy3 hy3, int i, j04 j04) throws yx3 {
        boolean z;
        int c2 = j04.c() - 1;
        int b2 = j04.b() - 1;
        int i2 = 0;
        int i3 = -1;
        while (c2 > 0) {
            if (c2 == 6) {
                c2--;
            }
            while (b2 >= 0 && b2 < j04.b()) {
                int i4 = i2;
                for (int i5 = 0; i5 < 2; i5++) {
                    int i6 = c2 - i5;
                    if (b((int) j04.a(i6, b2))) {
                        if (i4 < hy3.a()) {
                            z = hy3.b(i4);
                            i4++;
                        } else {
                            z = false;
                        }
                        if (i != -1 && l04.a(i, i6, b2)) {
                            z = !z;
                        }
                        j04.a(i6, b2, z);
                    }
                }
                b2 += i3;
                i2 = i4;
            }
            i3 = -i3;
            b2 += i3;
            c2 -= 2;
        }
        if (i2 != hy3.a()) {
            throw new yx3("Not all bits consumed: " + i2 + '/' + hy3.a());
        }
    }

    @DexIgnore
    public static int a(int i) {
        return 32 - Integer.numberOfLeadingZeros(i);
    }

    @DexIgnore
    public static int a(int i, int i2) {
        if (i2 != 0) {
            int a2 = a(i2);
            int i3 = i << (a2 - 1);
            while (a(i3) >= a2) {
                i3 ^= i2 << (a(i3) - a2);
            }
            return i3;
        }
        throw new IllegalArgumentException("0 polynomial");
    }

    @DexIgnore
    public static void a(f04 f04, int i, hy3 hy3) throws yx3 {
        if (n04.b(i)) {
            int bits = (f04.getBits() << 3) | i;
            hy3.a(bits, 5);
            hy3.a(a(bits, 1335), 10);
            hy3 hy32 = new hy3();
            hy32.a(21522, 15);
            hy3.b(hy32);
            if (hy3.a() != 15) {
                throw new yx3("should not happen but we got: " + hy3.a());
            }
            return;
        }
        throw new yx3("Invalid mask pattern");
    }

    @DexIgnore
    public static void a(h04 h04, hy3 hy3) throws yx3 {
        hy3.a(h04.c(), 6);
        hy3.a(a(h04.c(), 7973), 12);
        if (hy3.a() != 18) {
            throw new yx3("should not happen but we got: " + hy3.a());
        }
    }

    @DexIgnore
    public static void a(int i, int i2, j04 j04) throws yx3 {
        int i3 = 0;
        while (i3 < 8) {
            int i4 = i + i3;
            if (b((int) j04.a(i4, i2))) {
                j04.a(i4, i2, 0);
                i3++;
            } else {
                throw new yx3();
            }
        }
    }
}
