package com.fossil;

import com.portfolio.platform.manager.login.MFLoginWechatManager;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g24 implements Factory<jn4> {
    @DexIgnore
    public /* final */ b14 a;

    @DexIgnore
    public g24(b14 b14) {
        this.a = b14;
    }

    @DexIgnore
    public static g24 a(b14 b14) {
        return new g24(b14);
    }

    @DexIgnore
    public static MFLoginWechatManager b(b14 b14) {
        return c(b14);
    }

    @DexIgnore
    public static MFLoginWechatManager c(b14 b14) {
        MFLoginWechatManager q = b14.q();
        z76.a(q, "Cannot return null from a non-@Nullable @Provides method");
        return q;
    }

    @DexIgnore
    public MFLoginWechatManager get() {
        return b(this.a);
    }
}
