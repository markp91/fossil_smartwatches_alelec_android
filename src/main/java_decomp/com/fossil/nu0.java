package com.fossil;

import android.os.Parcel;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nu0 extends uj1 {
    @DexIgnore
    public static /* final */ vs0 CREATOR; // = new vs0((qg6) null);
    @DexIgnore
    public /* final */ ts0 b;

    @DexIgnore
    public nu0(ts0 ts0) {
        super(ck0.VIBE);
        this.b = ts0;
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(super.a(), bm0.VIBE_PATTERN, (Object) cw0.a((Enum<?>) this.b));
    }

    @DexIgnore
    public byte[] b() {
        ByteBuffer order = ByteBuffer.allocate(1).order(ByteOrder.LITTLE_ENDIAN);
        wg6.a(order, "ByteBuffer.allocate(1)\n \u2026(ByteOrder.LITTLE_ENDIAN)");
        order.put(this.b.a);
        byte[] array = order.array();
        wg6.a(array, "byteBuffer.array()");
        return array;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(nu0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((nu0) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.microapp.instruction.VibeInstr");
    }

    @DexIgnore
    public int hashCode() {
        return this.b.hashCode();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
        }
    }

    @DexIgnore
    public /* synthetic */ nu0(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            this.b = ts0.valueOf(readString);
            return;
        }
        wg6.a();
        throw null;
    }
}
