package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fl2<T> extends cl2<T> {
    @DexIgnore
    public /* final */ T zza;

    @DexIgnore
    public fl2(T t) {
        this.zza = t;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj instanceof fl2) {
            return this.zza.equals(((fl2) obj).zza);
        }
        return false;
    }

    @DexIgnore
    public final int hashCode() {
        return this.zza.hashCode() + 1502476572;
    }

    @DexIgnore
    public final String toString() {
        String valueOf = String.valueOf(this.zza);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }

    @DexIgnore
    public final boolean zza() {
        return true;
    }

    @DexIgnore
    public final T zzb() {
        return this.zza;
    }
}
