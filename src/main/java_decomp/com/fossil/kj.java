package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kj {
    @DexIgnore
    public static /* final */ int action_container; // = 2131361864;
    @DexIgnore
    public static /* final */ int action_divider; // = 2131361866;
    @DexIgnore
    public static /* final */ int action_image; // = 2131361867;
    @DexIgnore
    public static /* final */ int action_text; // = 2131361873;
    @DexIgnore
    public static /* final */ int actions; // = 2131361874;
    @DexIgnore
    public static /* final */ int async; // = 2131361894;
    @DexIgnore
    public static /* final */ int blocking; // = 2131361914;
    @DexIgnore
    public static /* final */ int chronometer; // = 2131361999;
    @DexIgnore
    public static /* final */ int forever; // = 2131362262;
    @DexIgnore
    public static /* final */ int ghost_view; // = 2131362459;
    @DexIgnore
    public static /* final */ int ghost_view_holder; // = 2131362460;
    @DexIgnore
    public static /* final */ int icon; // = 2131362499;
    @DexIgnore
    public static /* final */ int icon_group; // = 2131362500;
    @DexIgnore
    public static /* final */ int info; // = 2131362512;
    @DexIgnore
    public static /* final */ int italic; // = 2131362524;
    @DexIgnore
    public static /* final */ int line1; // = 2131362654;
    @DexIgnore
    public static /* final */ int line3; // = 2131362655;
    @DexIgnore
    public static /* final */ int normal; // = 2131362740;
    @DexIgnore
    public static /* final */ int notification_background; // = 2131362741;
    @DexIgnore
    public static /* final */ int notification_main_column; // = 2131362742;
    @DexIgnore
    public static /* final */ int notification_main_column_container; // = 2131362743;
    @DexIgnore
    public static /* final */ int parent_matrix; // = 2131362775;
    @DexIgnore
    public static /* final */ int right_icon; // = 2131362838;
    @DexIgnore
    public static /* final */ int right_side; // = 2131362839;
    @DexIgnore
    public static /* final */ int save_non_transition_alpha; // = 2131362900;
    @DexIgnore
    public static /* final */ int save_overlay_view; // = 2131362901;
    @DexIgnore
    public static /* final */ int tag_transition_group; // = 2131363001;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_event_manager; // = 2131363002;
    @DexIgnore
    public static /* final */ int tag_unhandled_key_listeners; // = 2131363003;
    @DexIgnore
    public static /* final */ int text; // = 2131363009;
    @DexIgnore
    public static /* final */ int text2; // = 2131363010;
    @DexIgnore
    public static /* final */ int time; // = 2131363031;
    @DexIgnore
    public static /* final */ int title; // = 2131363033;
    @DexIgnore
    public static /* final */ int transition_current_scene; // = 2131363044;
    @DexIgnore
    public static /* final */ int transition_layout_save; // = 2131363045;
    @DexIgnore
    public static /* final */ int transition_position; // = 2131363046;
    @DexIgnore
    public static /* final */ int transition_scene_layoutid_cache; // = 2131363047;
    @DexIgnore
    public static /* final */ int transition_transform; // = 2131363048;
}
