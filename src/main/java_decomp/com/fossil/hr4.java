package com.fossil;

import com.portfolio.platform.data.model.Explore;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class hr4 extends er4 {
    @DexIgnore
    public String d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public hr4(String str, String str2, String str3) {
        super(str, str2);
        wg6.b(str, "tagName");
        wg6.b(str2, Explore.COLUMN_TITLE);
        wg6.b(str3, "text");
        this.d = str3;
    }

    @DexIgnore
    public final void b(String str) {
        wg6.b(str, "<set-?>");
        this.d = str;
    }

    @DexIgnore
    public final String d() {
        return this.d;
    }
}
