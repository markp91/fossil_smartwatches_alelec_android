package com.fossil;

import com.fossil.gf;
import com.fossil.xe;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lf<A, B> extends gf<B> {
    @DexIgnore
    public /* final */ gf<A> a;
    @DexIgnore
    public /* final */ v3<List<A>, List<B>> b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends gf.b<A> {
        @DexIgnore
        public /* final */ /* synthetic */ gf.b a;

        @DexIgnore
        public a(gf.b bVar) {
            this.a = bVar;
        }

        @DexIgnore
        public void a(List<A> list, int i, int i2) {
            this.a.a(xe.convert(lf.this.b, list), i, i2);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends gf.e<A> {
        @DexIgnore
        public /* final */ /* synthetic */ gf.e a;

        @DexIgnore
        public b(gf.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(xe.convert(lf.this.b, list));
        }
    }

    @DexIgnore
    public lf(gf<A> gfVar, v3<List<A>, List<B>> v3Var) {
        this.a = gfVar;
        this.b = v3Var;
    }

    @DexIgnore
    public void addInvalidatedCallback(xe.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadInitial(gf.d dVar, gf.b<B> bVar) {
        this.a.loadInitial(dVar, new a(bVar));
    }

    @DexIgnore
    public void loadRange(gf.g gVar, gf.e<B> eVar) {
        this.a.loadRange(gVar, new b(eVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(xe.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
