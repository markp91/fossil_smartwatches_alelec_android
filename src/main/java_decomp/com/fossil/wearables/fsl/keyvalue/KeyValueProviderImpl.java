package com.fossil.wearables.fsl.keyvalue;

import android.content.Context;
import com.facebook.internal.FileLruCache;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class KeyValueProviderImpl extends BaseDbProvider implements KeyValueProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "keyvalue.db";

    @DexIgnore
    public KeyValueProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<KeyValue, Integer> getKeyValueDao() throws SQLException {
        return this.databaseHelper.getDao(KeyValue.class);
    }

    @DexIgnore
    public List<KeyValue> getAllKeyValues() {
        ArrayList arrayList = new ArrayList();
        try {
            return getKeyValueDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{KeyValue.class};
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return null;
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public KeyValue getKeyValueById(int i) {
        try {
            return (KeyValue) getKeyValueDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public String getValueByKey(String str) {
        KeyValue keyValue;
        try {
            QueryBuilder queryBuilder = getKeyValueDao().queryBuilder();
            queryBuilder.where().eq(FileLruCache.HEADER_CACHEKEY_KEY, str);
            List query = getKeyValueDao().query(queryBuilder.prepare());
            if (query == null || query.size() <= 0 || (keyValue = (KeyValue) query.get(0)) == null) {
                return null;
            }
            return keyValue.getValue();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public void removeAllKeyValues() {
        for (KeyValue removeKeyValue : getAllKeyValues()) {
            removeKeyValue(removeKeyValue);
        }
    }

    @DexIgnore
    public void removeKeyValue(KeyValue keyValue) {
        if (keyValue != null) {
            try {
                getKeyValueDao().delete(keyValue);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveKeyValue(KeyValue keyValue) {
        KeyValue keyValue2;
        if (keyValue != null) {
            try {
                QueryBuilder queryBuilder = getKeyValueDao().queryBuilder();
                queryBuilder.where().eq(FileLruCache.HEADER_CACHEKEY_KEY, keyValue.getKey());
                List query = getKeyValueDao().query(queryBuilder.prepare());
                if (!(query == null || query.size() <= 0 || (keyValue2 = (KeyValue) query.get(0)) == null)) {
                    keyValue.setDbRowId(keyValue2.getDbRowId());
                }
                getKeyValueDao().createOrUpdate(keyValue);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
