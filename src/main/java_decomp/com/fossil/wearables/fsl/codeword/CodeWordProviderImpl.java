package com.fossil.wearables.fsl.codeword;

import android.content.Context;
import android.util.Log;
import com.fossil.wearables.fsl.shared.BaseDbProvider;
import com.fossil.wearables.fsl.shared.UpgradeCommand;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CodeWordProviderImpl extends BaseDbProvider implements CodeWordProvider {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "codeword.db";
    @DexIgnore
    public List<WordGroup> cache;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class Anon1 extends HashMap<Integer, UpgradeCommand> {
        @DexIgnore
        public Anon1() {
        }
    }

    @DexIgnore
    public CodeWordProviderImpl(Context context, String str) {
        super(context, str);
    }

    @DexIgnore
    private Dao<Word, Integer> getWordDao() throws SQLException {
        return this.databaseHelper.getDao(Word.class);
    }

    @DexIgnore
    private Dao<WordGroup, Integer> getWordGroupDao() throws SQLException {
        return this.databaseHelper.getDao(WordGroup.class);
    }

    @DexIgnore
    public List<WordGroup> getAllWordGroups() {
        try {
            if (!this.isCacheDirty) {
                Log.d(this.TAG, "We have a cache hit in code words! Yay!");
                return this.cache;
            }
            Log.d(this.TAG, "Cache miss in code words :(");
            List<WordGroup> queryForAll = getWordGroupDao().queryForAll();
            String str = this.TAG;
            Log.d(str, "New word group is " + queryForAll);
            if (queryForAll == null) {
                return new ArrayList();
            }
            this.cache = queryForAll;
            this.isCacheDirty = false;
            return queryForAll;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.lang.Class<?>[]} */
    /* JADX WARNING: Multi-variable type inference failed */
    public Class<?>[] getDbEntities() {
        return new Class[]{WordGroup.class, Word.class};
    }

    @DexIgnore
    public String getDbPath() {
        return this.databaseHelper.getDbPath();
    }

    @DexIgnore
    public Map<Integer, UpgradeCommand> getDbUpgrades() {
        return new Anon1();
    }

    @DexIgnore
    public int getDbVersion() {
        return 1;
    }

    @DexIgnore
    public Word getWord(int i) {
        try {
            return (Word) getWordDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public WordGroup getWordGroup(int i) {
        try {
            return (WordGroup) getWordGroupDao().queryForId(Integer.valueOf(i));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @DexIgnore
    public List<Word> getWords(int i) {
        ArrayList arrayList = new ArrayList();
        try {
            QueryBuilder queryBuilder = getWordDao().queryBuilder();
            queryBuilder.where().eq("word_group_id", Integer.valueOf(i));
            return getWordDao().query(queryBuilder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
            return arrayList;
        }
    }

    @DexIgnore
    public List<Word> getWordsThatContainString(String str) {
        String str2 = this.TAG;
        Log.d(str2, "Looking for code words in: " + str);
        ArrayList arrayList = new ArrayList();
        List<WordGroup> allWordGroups = getAllWordGroups();
        if (allWordGroups != null) {
            for (int i = 0; i < allWordGroups.size(); i++) {
                WordGroup wordGroup = allWordGroups.get(i);
                if (wordGroup == null || !wordGroup.isEnabled()) {
                    Log.e(this.TAG, "Word group is null!");
                } else {
                    arrayList.addAll(wordGroup.containsString(str));
                }
            }
        }
        String str3 = this.TAG;
        Log.d(str3, "Found " + arrayList.size() + " code words");
        return arrayList;
    }

    @DexIgnore
    public void removeAllWordGroups() {
        setCacheToDirty();
        for (WordGroup removeWordGroup : getAllWordGroups()) {
            removeWordGroup(removeWordGroup);
        }
    }

    @DexIgnore
    public void removeWord(Word word) {
        setCacheToDirty();
        if (word != null) {
            try {
                getWordDao().delete(word);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void removeWordGroup(WordGroup wordGroup) {
        setCacheToDirty();
        if (wordGroup != null) {
            try {
                for (Word removeWord : wordGroup.getWords()) {
                    removeWord(removeWord);
                }
                getWordGroupDao().delete(wordGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveWord(Word word) {
        Word word2;
        setCacheToDirty();
        if (word != null) {
            try {
                QueryBuilder queryBuilder = getWordDao().queryBuilder();
                queryBuilder.where().eq("value", word.getValue());
                List query = getWordDao().query(queryBuilder.prepare());
                if (!(query == null || query.size() <= 0 || (word2 = (Word) query.get(0)) == null)) {
                    word.setDbRowId(word2.getDbRowId());
                }
                getWordDao().createOrUpdate(word);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @DexIgnore
    public void saveWordGroup(WordGroup wordGroup) {
        setCacheToDirty();
        if (wordGroup != null) {
            try {
                WordGroup wordGroup2 = (WordGroup) getWordGroupDao().queryForSameId(wordGroup);
                if (wordGroup2 != null) {
                    wordGroup.setDbRowId(wordGroup2.getDbRowId());
                }
                getWordGroupDao().createOrUpdate(wordGroup);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
