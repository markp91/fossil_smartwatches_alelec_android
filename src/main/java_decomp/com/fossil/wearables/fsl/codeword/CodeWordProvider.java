package com.fossil.wearables.fsl.codeword;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface CodeWordProvider extends BaseProvider {
    @DexIgnore
    List<WordGroup> getAllWordGroups();

    @DexIgnore
    Word getWord(int i);

    @DexIgnore
    WordGroup getWordGroup(int i);

    @DexIgnore
    List<Word> getWords(int i);

    @DexIgnore
    List<Word> getWordsThatContainString(String str);

    @DexIgnore
    void removeAllWordGroups();

    @DexIgnore
    void removeWord(Word word);

    @DexIgnore
    void removeWordGroup(WordGroup wordGroup);

    @DexIgnore
    void saveWord(Word word);

    @DexIgnore
    void saveWordGroup(WordGroup wordGroup);
}
