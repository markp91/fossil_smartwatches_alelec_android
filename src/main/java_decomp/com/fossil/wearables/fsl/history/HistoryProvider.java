package com.fossil.wearables.fsl.history;

import java.util.Calendar;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface HistoryProvider {
    @DexIgnore
    List<HistoryItem> getAllItems();

    @DexIgnore
    List<HistoryItem> getItems(Calendar calendar);

    @DexIgnore
    List<HistoryItem> getLatestItems(long j);

    @DexIgnore
    void removeAllItems();

    @DexIgnore
    void removeItem(HistoryItem historyItem);

    @DexIgnore
    void removeItems(List<HistoryItem> list);

    @DexIgnore
    void saveItem(HistoryItem historyItem);
}
