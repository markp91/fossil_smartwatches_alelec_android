package com.fossil.wearables.fsl.goal;

import com.fossil.wearables.fsl.BaseProvider;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface GoalProvider extends BaseProvider {
    @DexIgnore
    List<Goal> getAllGoals();

    @DexIgnore
    List<Goal> getAllGoals(long j);

    @DexIgnore
    Goal getGoal(int i);

    @DexIgnore
    void removeGoal(Goal goal);

    @DexIgnore
    void removeGoalInterval(GoalInterval goalInterval);

    @DexIgnore
    void saveGoal(Goal goal);

    @DexIgnore
    void saveGoalInterval(GoalInterval goalInterval);
}
