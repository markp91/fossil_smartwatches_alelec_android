package com.fossil.wearables.fsl.sleep;

import com.fossil.wearables.fsl.shared.BaseModel;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@DatabaseTable(tableName = "sleep_session")
public class MFSleepSession extends BaseModel {
    @DexIgnore
    public static /* final */ String COLUMN_BOOKMARK_TIME; // = "bookmarkTime";
    @DexIgnore
    public static /* final */ String COLUMN_CREATE_AT; // = "createdAt";
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_DAY; // = "day";
    @DexIgnore
    public static /* final */ String COLUMN_DEVICE_SERIAL_NUMBER; // = "deviceSerialNumber";
    @DexIgnore
    public static /* final */ String COLUMN_EDITED_END_TIME; // = "editedEndTime";
    @DexIgnore
    public static /* final */ String COLUMN_EDITED_SLEEP_MINUTES; // = "editedSleepMinutes";
    @DexIgnore
    public static /* final */ String COLUMN_EDITED_START_TIME; // = "editedStartTime";
    @DexIgnore
    public static /* final */ String COLUMN_EDITED_STATE_DIST_IN_MINUTE; // = "editedSleepStateDistInMinute";
    @DexIgnore
    public static /* final */ String COLUMN_NORMALIZED_SLEEP_QUALITY; // = "normalizedSleepQuality";
    @DexIgnore
    public static /* final */ String COLUMN_OBJECT_ID; // = "objectId";
    @DexIgnore
    public static /* final */ String COLUMN_OWNER; // = "owner";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_REAL_END_TIME; // = "realEndTime";
    @DexIgnore
    public static /* final */ String COLUMN_REAL_SLEEP_MINUTES; // = "realSleepMinutes";
    @DexIgnore
    public static /* final */ String COLUMN_REAL_START_TIME; // = "realStartTime";
    @DexIgnore
    public static /* final */ String COLUMN_REAL_STATE_DIST_IN_MINUTE; // = "realSleepStateDistInMinute";
    @DexIgnore
    public static /* final */ String COLUMN_SLEEP_STATES; // = "sleepStates";
    @DexIgnore
    public static /* final */ String COLUMN_SOURCE; // = "source";
    @DexIgnore
    public static /* final */ String COLUMN_SYNC_TIME; // = "syncTime";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_OFFSET; // = "timezoneOffset";
    @DexIgnore
    public static /* final */ String COLUMN_UDPATE_AT; // = "updatedAt";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sleep_session";
    @DexIgnore
    @DatabaseField(columnName = "bookmarkTime")
    public int bookmarkTime;
    @DexIgnore
    @DatabaseField(columnName = "createdAt")
    public DateTime createdAt;
    @DexIgnore
    @DatabaseField(columnName = "date")
    public long date;
    @DexIgnore
    @DatabaseField(columnName = "day")
    public String day;
    @DexIgnore
    @DatabaseField(columnName = "deviceSerialNumber")
    public String deviceSerialNumber;
    @DexIgnore
    @DatabaseField(columnName = "editedEndTime")
    public int editedEndTime;
    @DexIgnore
    @DatabaseField(columnName = "editedSleepMinutes")
    public int editedSleepMinutes;
    @DexIgnore
    @DatabaseField(columnName = "editedSleepStateDistInMinute")
    public String editedSleepStateDistInMinute;
    @DexIgnore
    @DatabaseField(columnName = "editedStartTime")
    public int editedStartTime;
    @DexIgnore
    @DatabaseField(columnName = "normalizedSleepQuality")
    public double normalizedSleepQuality;
    @DexIgnore
    @DatabaseField(columnName = "objectId")
    public String objectId;
    @DexIgnore
    @DatabaseField(columnName = "owner")
    public String owner;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = "realEndTime")
    public int realEndTime;
    @DexIgnore
    @DatabaseField(columnName = "realSleepMinutes")
    public int realSleepMinutes;
    @DexIgnore
    @DatabaseField(columnName = "realSleepStateDistInMinute")
    public String realSleepStateDistInMinute;
    @DexIgnore
    @DatabaseField(columnName = "realStartTime")
    public int realStartTime;
    @DexIgnore
    @DatabaseField(columnName = "sleepStates")
    public String sleepStates;
    @DexIgnore
    @DatabaseField(columnName = "source")
    public int source;
    @DexIgnore
    @DatabaseField(columnName = "syncTime")
    public int syncTime;
    @DexIgnore
    @DatabaseField(columnName = "timezoneOffset")
    public int timezoneOffset;
    @DexIgnore
    @DatabaseField(columnName = "updatedAt")
    public DateTime updatedAt;

    @DexIgnore
    public MFSleepSession() {
    }

    @DexIgnore
    public int getBookmarkTime() {
        return this.bookmarkTime;
    }

    @DexIgnore
    public DateTime getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public long getDate() {
        return this.date;
    }

    @DexIgnore
    public String getDay() {
        return this.day;
    }

    @DexIgnore
    public String getDeviceSerialNumber() {
        return this.deviceSerialNumber;
    }

    @DexIgnore
    public int getEditedEndTime() {
        return this.editedEndTime;
    }

    @DexIgnore
    public int getEditedSleepMinutes() {
        return this.editedSleepMinutes;
    }

    @DexIgnore
    public String getEditedSleepStateDistInMinute() {
        return this.editedSleepStateDistInMinute;
    }

    @DexIgnore
    public int getEditedStartTime() {
        return this.editedStartTime;
    }

    @DexIgnore
    public double getNormalizedSleepQuality() {
        return this.normalizedSleepQuality;
    }

    @DexIgnore
    public String getObjectId() {
        return this.objectId;
    }

    @DexIgnore
    public String getOwner() {
        return this.owner;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public int getRealEndTime() {
        return this.realEndTime;
    }

    @DexIgnore
    public int getRealSleepMinutes() {
        return this.realSleepMinutes;
    }

    @DexIgnore
    public String getRealSleepStateDistInMinute() {
        return this.realSleepStateDistInMinute;
    }

    @DexIgnore
    public int getRealStartTime() {
        return this.realStartTime;
    }

    @DexIgnore
    public String getSleepStates() {
        return this.sleepStates;
    }

    @DexIgnore
    public int getSource() {
        return this.source;
    }

    @DexIgnore
    public int getSyncTime() {
        return this.syncTime;
    }

    @DexIgnore
    public int getTimezoneOffset() {
        return this.timezoneOffset;
    }

    @DexIgnore
    public DateTime getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public void setBookmarkTime(int i) {
        this.bookmarkTime = i;
    }

    @DexIgnore
    public void setCreatedAt(DateTime dateTime) {
        this.createdAt = dateTime;
    }

    @DexIgnore
    public void setDate(long j) {
        this.date = j;
    }

    @DexIgnore
    public void setDay(String str) {
        this.day = str;
    }

    @DexIgnore
    public void setDeviceSerialNumber(String str) {
        this.deviceSerialNumber = str;
    }

    @DexIgnore
    public void setEditedEndTime(int i) {
        this.editedEndTime = i;
    }

    @DexIgnore
    public void setEditedSleepMinutes(int i) {
        this.editedSleepMinutes = i;
    }

    @DexIgnore
    public void setEditedSleepStateDistInMinute(String str) {
        this.editedSleepStateDistInMinute = str;
    }

    @DexIgnore
    public void setEditedStartTime(int i) {
        this.editedStartTime = i;
    }

    @DexIgnore
    public void setNormalizedSleepQuality(double d) {
        this.normalizedSleepQuality = d;
    }

    @DexIgnore
    public void setObjectId(String str) {
        this.objectId = str;
    }

    @DexIgnore
    public void setOwner(String str) {
        this.owner = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setRealEndTime(int i) {
        this.realEndTime = i;
    }

    @DexIgnore
    public void setRealSleepMinutes(int i) {
        this.realSleepMinutes = i;
    }

    @DexIgnore
    public void setRealSleepStateDistInMinute(String str) {
        this.realSleepStateDistInMinute = str;
    }

    @DexIgnore
    public void setRealStartTime(int i) {
        this.realStartTime = i;
    }

    @DexIgnore
    public void setSleepStates(String str) {
        this.sleepStates = str;
    }

    @DexIgnore
    public void setSource(int i) {
        this.source = i;
    }

    @DexIgnore
    public void setSyncTime(int i) {
        this.syncTime = i;
    }

    @DexIgnore
    public void setTimezoneOffset(int i) {
        this.timezoneOffset = i;
    }

    @DexIgnore
    public void setUpdatedAt(DateTime dateTime) {
        this.updatedAt = dateTime;
    }

    @DexIgnore
    public String toString() {
        return "MFSleepSession{date=" + this.date + ", timezoneOffset=" + this.timezoneOffset + ", day='" + this.day + '\'' + ", owner='" + this.owner + '\'' + ", objectId='" + this.objectId + '\'' + ", deviceSerialNumber='" + this.deviceSerialNumber + '\'' + ", syncTime=" + this.syncTime + ", bookmarkTime=" + this.bookmarkTime + ", normalizedSleepQuality=" + this.normalizedSleepQuality + ", source=" + this.source + ", realStartTime=" + this.realStartTime + ", realEndTime=" + this.realEndTime + ", realSleepMinutes=" + this.realSleepMinutes + ", realSleepStateDistInMinute='" + this.realSleepStateDistInMinute + '\'' + ", editedStartTime=" + this.editedStartTime + ", editedEndTime=" + this.editedEndTime + ", editedSleepMinutes=" + this.editedSleepMinutes + ", editedSleepStateDistInMinute='" + this.editedSleepStateDistInMinute + '\'' + ", sleepStates='" + this.sleepStates + '\'' + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + '}';
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, int i2, int i3, double d, int i4, int i5, int i6, int i7, String str2, String str3, DateTime dateTime) {
        this.date = j;
        this.timezoneOffset = i;
        this.deviceSerialNumber = str;
        this.syncTime = i2;
        this.bookmarkTime = i3;
        this.normalizedSleepQuality = d;
        this.source = i4;
        this.realStartTime = i5;
        this.realEndTime = i6;
        this.realSleepMinutes = i7;
        this.realSleepStateDistInMinute = str2;
        this.sleepStates = str3;
        this.updatedAt = dateTime;
        this.editedStartTime = i5;
        this.editedEndTime = i6;
        this.editedSleepMinutes = i7;
        this.editedSleepStateDistInMinute = str2;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, int i2, double d, int i3, int i4, int i5, int i6, String str2, String str3, DateTime dateTime, boolean z) {
        this.date = j;
        this.timezoneOffset = i;
        this.deviceSerialNumber = str;
        this.bookmarkTime = i2;
        this.normalizedSleepQuality = d;
        this.source = i3;
        this.editedStartTime = i4;
        this.editedEndTime = i5;
        this.editedSleepMinutes = i6;
        this.editedSleepStateDistInMinute = str2;
        this.sleepStates = str3;
        if (!z) {
            this.realStartTime = i4;
            this.realEndTime = i5;
            this.realSleepMinutes = i6;
            this.realSleepStateDistInMinute = str2;
        }
        this.updatedAt = dateTime;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, String str2, String str3, int i2, int i3, double d, int i4, int i5, int i6, int i7, String str4, int i8, int i9, int i10, String str5, String str6, DateTime dateTime, DateTime dateTime2) {
        this.date = j;
        this.timezoneOffset = i;
        this.owner = str;
        this.objectId = str2;
        this.deviceSerialNumber = str3;
        this.syncTime = i2;
        this.bookmarkTime = i3;
        this.normalizedSleepQuality = d;
        this.source = i4;
        this.realStartTime = i5;
        this.realEndTime = i6;
        this.realSleepMinutes = i7;
        this.realSleepStateDistInMinute = str4;
        this.editedStartTime = i8;
        this.editedEndTime = i9;
        this.editedSleepMinutes = i10;
        this.editedSleepStateDistInMinute = str5;
        this.sleepStates = str6;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, String str2, int i2, int i3, double d, int i4, int i5, int i6, int i7, String str3, String str4, DateTime dateTime) {
        int i8 = i5;
        int i9 = i6;
        int i10 = i7;
        String str5 = str3;
        this.date = j;
        this.timezoneOffset = i;
        this.day = str;
        this.deviceSerialNumber = str2;
        this.syncTime = i2;
        this.bookmarkTime = i3;
        this.normalizedSleepQuality = d;
        this.source = i4;
        this.realStartTime = i8;
        this.realEndTime = i9;
        this.realSleepMinutes = i10;
        this.realSleepStateDistInMinute = str5;
        this.sleepStates = str4;
        this.updatedAt = dateTime;
        this.editedStartTime = i8;
        this.editedEndTime = i9;
        this.editedSleepMinutes = i10;
        this.editedSleepStateDistInMinute = str5;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, String str2, int i2, double d, int i3, int i4, int i5, int i6, String str3, String str4, DateTime dateTime, boolean z) {
        int i7 = i4;
        int i8 = i5;
        int i9 = i6;
        String str5 = str3;
        this.date = j;
        this.timezoneOffset = i;
        this.day = str;
        this.deviceSerialNumber = str2;
        this.bookmarkTime = i2;
        this.normalizedSleepQuality = d;
        this.source = i3;
        this.editedStartTime = i7;
        this.editedEndTime = i8;
        this.editedSleepMinutes = i9;
        this.editedSleepStateDistInMinute = str5;
        this.sleepStates = str4;
        if (!z) {
            this.realStartTime = i7;
            this.realEndTime = i8;
            this.realSleepMinutes = i9;
            this.realSleepStateDistInMinute = str5;
        }
        this.updatedAt = dateTime;
        this.pinType = 0;
    }

    @DexIgnore
    public MFSleepSession(long j, int i, String str, String str2, String str3, String str4, int i2, int i3, double d, int i4, int i5, int i6, int i7, String str5, int i8, int i9, int i10, String str6, String str7, DateTime dateTime, DateTime dateTime2) {
        this.date = j;
        this.timezoneOffset = i;
        this.day = str2;
        this.owner = str;
        this.objectId = str3;
        this.deviceSerialNumber = str4;
        this.syncTime = i2;
        this.bookmarkTime = i3;
        this.normalizedSleepQuality = d;
        this.source = i4;
        this.realStartTime = i5;
        this.realEndTime = i6;
        this.realSleepMinutes = i7;
        this.realSleepStateDistInMinute = str5;
        this.editedStartTime = i8;
        this.editedEndTime = i9;
        this.editedSleepMinutes = i10;
        this.editedSleepStateDistInMinute = str6;
        this.sleepStates = str7;
        this.createdAt = dateTime;
        this.updatedAt = dateTime2;
        this.pinType = 0;
    }
}
