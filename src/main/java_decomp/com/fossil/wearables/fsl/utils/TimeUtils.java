package com.fossil.wearables.fsl.utils;

import com.fossil.oh;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class TimeUtils {
    @DexIgnore
    public static /* final */ String SIMPLE_FORMAT_YYYY_MM_DD; // = "yyyy-MM-dd";
    @DexIgnore
    public static /* final */ String TAG; // = "TimeUtils";

    @DexIgnore
    public static Date getEndOfDay(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(11, 23);
        instance.set(12, 59);
        instance.set(13, 59);
        instance.set(14, oh.MAX_BIND_PARAMETER_CNT);
        return instance.getTime();
    }

    @DexIgnore
    public static Date getStartOfDay(Date date) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.set(11, 0);
        instance.set(12, 0);
        instance.set(13, 0);
        instance.set(14, 0);
        return instance.getTime();
    }

    @DexIgnore
    public static String getStringCurrentTimeYYYYMMDD() {
        return new SimpleDateFormat(SIMPLE_FORMAT_YYYY_MM_DD, Locale.US).format(new Date());
    }

    @DexIgnore
    public static String getStringTimeYYYYMMDD(long j) {
        return new SimpleDateFormat(SIMPLE_FORMAT_YYYY_MM_DD, Locale.US).format(new Date(j));
    }

    @DexIgnore
    public static int getTimezoneOffset() {
        return TimeZone.getDefault().getOffset(Calendar.getInstance().getTimeInMillis()) / 1000;
    }
}
