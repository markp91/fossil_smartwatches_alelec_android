package com.fossil.wearables.fsl.location;

import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.ForeignCollectionField;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class LocationGroup extends BaseFeatureModel {
    @DexIgnore
    @ForeignCollectionField(eager = true)
    public ForeignCollection<Location> locations;

    @DexIgnore
    public List<Location> getLocations() {
        ArrayList arrayList = new ArrayList();
        ForeignCollection<Location> foreignCollection = this.locations;
        return (foreignCollection == null || foreignCollection.size() <= 0) ? arrayList : new ArrayList(this.locations);
    }
}
