package com.fossil.wearables.fsl.goaltracking;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTrackingURI {
    @DexIgnore
    public static /* final */ SimpleDateFormat DATE_FORMAT; // = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    @DexIgnore
    public static String uriEventSchemePrefix; // = "urn:fsl:goal_tracking_event";
    @DexIgnore
    public static String uriSchemePrefix; // = "urn:fsl:goal_tracking";

    @DexIgnore
    public static URI generateEventURI(GoalTrackingEvent goalTrackingEvent) {
        StringBuffer stringBuffer = new StringBuffer(uriEventSchemePrefix);
        stringBuffer.append(":" + DATE_FORMAT.format(new Date()));
        return URI.create(stringBuffer.toString());
    }

    @DexIgnore
    public static URI generateURI(GoalTracking goalTracking) {
        StringBuffer stringBuffer = new StringBuffer(uriSchemePrefix);
        stringBuffer.append(":" + DATE_FORMAT.format(new Date()));
        stringBuffer.append(":");
        stringBuffer.append(goalTracking.getTarget());
        stringBuffer.append(":");
        stringBuffer.append(goalTracking.getFrequency());
        return URI.create(stringBuffer.toString());
    }
}
