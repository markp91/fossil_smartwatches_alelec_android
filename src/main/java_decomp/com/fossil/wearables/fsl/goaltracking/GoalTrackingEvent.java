package com.fossil.wearables.fsl.goaltracking;

import com.j256.ormlite.field.DatabaseField;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class GoalTrackingEvent {
    @DexIgnore
    public static /* final */ String COLUMN_AUTO_DETECTED; // = "autoDetected";
    @DexIgnore
    public static /* final */ String COLUMN_COUNTER; // = "counter";
    @DexIgnore
    public static /* final */ String COLUMN_DATE; // = "date";
    @DexIgnore
    public static /* final */ String COLUMN_GOAL_ID; // = "goalId";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_TRACKED_AT; // = "trackedAt";
    @DexIgnore
    public static /* final */ String COLUMN_URI; // = "uri";
    @DexIgnore
    @DatabaseField(columnName = "autoDetected")
    public boolean autoDetected;
    @DexIgnore
    @DatabaseField(columnName = "counter")
    public int counter;
    @DexIgnore
    @DatabaseField(columnName = "date")
    public int date;
    @DexIgnore
    @DatabaseField(columnName = "goalId", foreign = true, foreignAutoRefresh = true)
    public GoalTracking goalTracking;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType; // = 0;
    @DexIgnore
    @DatabaseField(columnName = "trackedAt")
    public long trackedAt;
    @DexIgnore
    @DatabaseField(columnName = "uri", id = true)
    public String uri; // = GoalTrackingURI.generateEventURI(this).toASCIIString();

    @DexIgnore
    public GoalTrackingEvent() {
    }

    @DexIgnore
    public int getCounter() {
        return this.counter;
    }

    @DexIgnore
    public int getDate() {
        return this.date;
    }

    @DexIgnore
    public GoalTracking getGoalTracking() {
        return this.goalTracking;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public long getTrackedAt() {
        return this.trackedAt;
    }

    @DexIgnore
    public String getUri() {
        return this.uri;
    }

    @DexIgnore
    public boolean isAutoDetected() {
        return this.autoDetected;
    }

    @DexIgnore
    public void setAutoDetected(boolean z) {
        this.autoDetected = z;
    }

    @DexIgnore
    public void setCounter(int i) {
        this.counter = i;
    }

    @DexIgnore
    public void setDate(int i) {
        this.date = i;
    }

    @DexIgnore
    public void setGoalTracking(GoalTracking goalTracking2) {
        this.goalTracking = goalTracking2;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setTrackedAt(long j) {
        this.trackedAt = j;
    }

    @DexIgnore
    public void setUri(String str) {
        this.uri = str;
    }

    @DexIgnore
    public GoalTrackingEvent(int i, long j, GoalTracking goalTracking2) {
        this.date = i;
        this.trackedAt = j;
        this.goalTracking = goalTracking2;
    }
}
