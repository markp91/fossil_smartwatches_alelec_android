package com.fossil.wearables.fsl.fitness;

import com.fossil.wearables.fsl.fitness.exception.InvalidTimezoneIDException;
import com.fossil.wearables.fsl.utils.DateStringSQLiteType;
import com.j256.ormlite.field.DatabaseField;
import java.io.Serializable;
import java.net.URI;
import java.util.Date;
import java.util.TimeZone;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class SampleRaw implements Serializable {
    @DexIgnore
    public static /* final */ String COLUMN_CALORIES; // = "calories";
    @DexIgnore
    public static /* final */ String COLUMN_DISTANCE; // = "distance";
    @DexIgnore
    public static /* final */ String COLUMN_END_TIME; // = "endTime";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_MOVEMENT_TYPE_VALUE; // = "movementTypeValue";
    @DexIgnore
    public static /* final */ String COLUMN_PIN_TYPE; // = "pinType";
    @DexIgnore
    public static /* final */ String COLUMN_SOURCE_ID; // = "sourceId";
    @DexIgnore
    public static /* final */ String COLUMN_SOURCE_TYPE_VALUE; // = "sourceTypeValue";
    @DexIgnore
    public static /* final */ String COLUMN_START_TIME; // = "startTime";
    @DexIgnore
    public static /* final */ String COLUMN_STEPS; // = "steps";
    @DexIgnore
    public static /* final */ String COLUMN_TIMEZONE_ID; // = "timeZoneID";
    @DexIgnore
    public static /* final */ String COLUMN_UA_PIN_TYPE; // = "uaPinType";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "sampleraw";
    @DexIgnore
    @DatabaseField(columnName = "calories")
    public double calories;
    @DexIgnore
    @DatabaseField(columnName = "distance")
    public double distance;
    @DexIgnore
    @DatabaseField(columnName = "endTime", persisterClass = DateStringSQLiteType.class)
    public Date endTime;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = "movementTypeValue")
    public String movementTypeValue;
    @DexIgnore
    @DatabaseField(columnName = "pinType")
    public int pinType;
    @DexIgnore
    @DatabaseField(columnName = "sourceId")
    public String sourceId;
    @DexIgnore
    @DatabaseField(columnName = "sourceTypeValue")
    public String sourceTypeValue;
    @DexIgnore
    @DatabaseField(columnName = "startTime", persisterClass = DateStringSQLiteType.class)
    public Date startTime;
    @DexIgnore
    @DatabaseField(columnName = "steps")
    public double steps;
    @DexIgnore
    @DatabaseField(columnName = "timeZoneID")
    public String timeZoneID;
    @DexIgnore
    @DatabaseField(columnName = "uaPinType")
    public int uaPinType;
    @DexIgnore
    public URI uri;

    @DexIgnore
    public SampleRaw() {
    }

    @DexIgnore
    private void init(Date date, Date date2, TimeZone timeZone, String str, FitnessSourceType fitnessSourceType, FitnessMovementType fitnessMovementType, double d, double d2, double d3) {
        this.sourceTypeValue = fitnessSourceType.getName();
        if (fitnessSourceType == FitnessSourceType.Mock) {
            this.sourceId = "000000000000000";
        } else {
            this.sourceId = str;
        }
        this.startTime = date;
        this.endTime = date2;
        this.steps = d;
        this.calories = d2;
        this.distance = d3;
        this.movementTypeValue = fitnessMovementType.getName();
        this.timeZoneID = timeZone.getID();
        this.pinType = 0;
        this.uri = FitnessURI.generateURI(this);
        this.id = this.uri.toASCIIString();
    }

    @DexIgnore
    public double getCalories() {
        return this.calories;
    }

    @DexIgnore
    public double getDistance() {
        return this.distance;
    }

    @DexIgnore
    public long getDuration() {
        return this.endTime.getTime() - this.startTime.getTime();
    }

    @DexIgnore
    public Date getEndTime() {
        return this.endTime;
    }

    @DexIgnore
    public FitnessMovementType getMovementType() {
        return FitnessMovementType.valueFor(this.movementTypeValue);
    }

    @DexIgnore
    public String getMovementTypeValue() {
        return this.movementTypeValue;
    }

    @DexIgnore
    public int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public String getSourceId() {
        return this.sourceId;
    }

    @DexIgnore
    public FitnessSourceType getSourceType() {
        return FitnessSourceType.valueOf(this.sourceTypeValue);
    }

    @DexIgnore
    public String getSourceTypeValue() {
        return this.sourceTypeValue;
    }

    @DexIgnore
    public Date getStartTime() {
        return this.startTime;
    }

    @DexIgnore
    public double getSteps() {
        return this.steps;
    }

    @DexIgnore
    public TimeZone getTimeZone() throws InvalidTimezoneIDException {
        TimeZone timeZone = TimeZone.getTimeZone(getTimeZoneID());
        if (timeZone != null && timeZone.getID().equals(getTimeZoneID())) {
            return timeZone;
        }
        throw new InvalidTimezoneIDException(getTimeZoneID());
    }

    @DexIgnore
    public String getTimeZoneID() {
        return this.timeZoneID;
    }

    @DexIgnore
    public int getUaPinType() {
        return this.uaPinType;
    }

    @DexIgnore
    public URI getUri() {
        String str;
        if (this.uri == null && (str = this.id) != null) {
            this.uri = URI.create(str);
        }
        return this.uri;
    }

    @DexIgnore
    public void setCalories(double d) {
        this.calories = d;
    }

    @DexIgnore
    public void setDistance(double d) {
        this.distance = d;
    }

    @DexIgnore
    public void setEndTime(Date date) {
        this.endTime = date;
    }

    @DexIgnore
    public void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public void setStartTime(Date date) {
        this.startTime = date;
    }

    @DexIgnore
    public void setSteps(double d) {
        this.steps = d;
    }

    @DexIgnore
    public void setTimeZone(String str) throws InvalidTimezoneIDException {
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || !timeZone.getID().equals(str)) {
            throw new InvalidTimezoneIDException(str);
        }
        this.timeZoneID = timeZone.getID();
    }

    @DexIgnore
    public void setUaPinType(int i) {
        this.uaPinType = i;
    }

    @DexIgnore
    public String toString() {
        return "[SampleRaw: startTime=" + this.startTime + ", endTime=" + this.endTime + ", steps=" + this.steps + ", calories=" + this.calories + ", distance=" + this.distance + ", source=" + this.sourceId + ", movementType=" + this.movementTypeValue + ", pinType=" + this.pinType + ", uaPinType=" + this.uaPinType + "]";
    }

    @DexIgnore
    public SampleRaw(Date date, Date date2, String str, String str2, FitnessSourceType fitnessSourceType, FitnessMovementType fitnessMovementType, double d, double d2, double d3) throws InvalidTimezoneIDException {
        String str3 = str;
        TimeZone timeZone = TimeZone.getTimeZone(str);
        if (timeZone == null || !timeZone.getID().equals(str3)) {
            throw new InvalidTimezoneIDException(str3);
        }
        init(date, date2, timeZone, str2, fitnessSourceType, fitnessMovementType, d, d2, d3);
    }

    @DexIgnore
    public SampleRaw(Date date, Date date2, TimeZone timeZone, String str, FitnessSourceType fitnessSourceType, FitnessMovementType fitnessMovementType, double d, double d2, double d3) {
        init(date, date2, timeZone, str, fitnessSourceType, fitnessMovementType, d, d2, d3);
    }
}
