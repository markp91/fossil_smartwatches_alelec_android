package com.fossil.wearables.fsl.fitness;

import com.j256.ormlite.field.DatabaseField;
import java.net.URI;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class DailyGoal {
    @DexIgnore
    public static /* final */ String COLUMN_DAY; // = "day";
    @DexIgnore
    public static /* final */ String COLUMN_ID; // = "id";
    @DexIgnore
    public static /* final */ String COLUMN_MONTH; // = "month";
    @DexIgnore
    public static /* final */ String COLUMN_STEPS; // = "steps";
    @DexIgnore
    public static /* final */ String COLUMN_YEAR; // = "year";
    @DexIgnore
    public static /* final */ String TABLE_NAME; // = "dailygoal";
    @DexIgnore
    @DatabaseField(columnName = "day")
    public int day;
    @DexIgnore
    @DatabaseField(columnName = "id", id = true)
    public String id;
    @DexIgnore
    @DatabaseField(columnName = "month")
    public int month;
    @DexIgnore
    @DatabaseField(columnName = "steps")
    public int steps;
    @DexIgnore
    public URI uri;
    @DexIgnore
    @DatabaseField(columnName = "year")
    public int year;

    @DexIgnore
    public DailyGoal() {
    }

    @DexIgnore
    public int getDay() {
        return this.day;
    }

    @DexIgnore
    public int getMonth() {
        return this.month;
    }

    @DexIgnore
    public int getSteps() {
        return this.steps;
    }

    @DexIgnore
    public URI getUri() {
        String str;
        if (this.uri == null && (str = this.id) != null) {
            this.uri = URI.create(str);
        }
        return this.uri;
    }

    @DexIgnore
    public int getYear() {
        return this.year;
    }

    @DexIgnore
    public String toString() {
        return "[DailyGoal: date=" + this.year + "/" + this.month + "/" + this.day + "steps=" + this.steps + "]";
    }

    @DexIgnore
    public DailyGoal(int i, int i2, int i3, int i4) {
        this.year = i;
        this.month = i2;
        this.day = i3;
        this.steps = i4;
        this.uri = FitnessURI.generateURI(this);
        this.id = this.uri.toASCIIString();
    }
}
