package com.fossil.wearables.fsl.fitness;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class FitnessProviderFactory {
    @DexIgnore
    public static /* final */ String DB_NAME; // = "fitness.db";

    @DexIgnore
    public static FitnessProvider createInstance(Context context, String str) {
        return new FitnessProviderImpl(context, str);
    }
}
