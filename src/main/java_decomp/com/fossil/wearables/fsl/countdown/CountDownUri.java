package com.fossil.wearables.fsl.countdown;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class CountDownUri {
    @DexIgnore
    public static /* final */ SimpleDateFormat DATE_FORMAT; // = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
    @DexIgnore
    public static String uriSchemePrefix; // = "urn:fsl:countdown";

    @DexIgnore
    public static URI generateURI(CountDown countDown) {
        StringBuffer stringBuffer = new StringBuffer(uriSchemePrefix);
        stringBuffer.append(":" + DATE_FORMAT.format(new Date()));
        return URI.create(stringBuffer.toString());
    }
}
