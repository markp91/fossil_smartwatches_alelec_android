package com.fossil.wearables.fsl.countdown;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum CountDownType {
    CUSTOM(0),
    BIRTHDAY(1),
    WEDDING(2),
    VACATION(3),
    FESTIVAL(4);
    
    @DexIgnore
    public int value;

    @DexIgnore
    public CountDownType(int i) {
        this.value = i;
    }

    @DexIgnore
    public static CountDownType fromInt(int i) {
        for (CountDownType countDownType : values()) {
            if (countDownType.getValue() == i) {
                return countDownType;
            }
        }
        return CUSTOM;
    }

    @DexIgnore
    public int getValue() {
        return this.value;
    }
}
