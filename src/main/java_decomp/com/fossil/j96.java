package com.fossil;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j96 {
    @DexIgnore
    public static /* final */ Pattern m; // = Pattern.compile("[^\\p{Alnum}]");
    @DexIgnore
    public static /* final */ String n; // = Pattern.quote(ZendeskConfig.SLASH);
    @DexIgnore
    public /* final */ ReentrantLock a; // = new ReentrantLock();
    @DexIgnore
    public /* final */ k96 b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ boolean d;
    @DexIgnore
    public /* final */ Context e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;
    @DexIgnore
    public /* final */ Collection<i86> h;
    @DexIgnore
    public t86 i;
    @DexIgnore
    public s86 j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public i96 l;

    @DexIgnore
    public enum a {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        
        @DexIgnore
        public /* final */ int protobufIndex;

        @DexIgnore
        public a(int i) {
            this.protobufIndex = i;
        }
    }

    @DexIgnore
    public j96(Context context, String str, String str2, Collection<i86> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection != null) {
            this.e = context;
            this.f = str;
            this.g = str2;
            this.h = collection;
            this.b = new k96();
            this.i = new t86(context);
            this.l = new i96();
            this.c = z86.a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            if (!this.c) {
                l86 g2 = c86.g();
                g2.d("Fabric", "Device ID collection disabled for " + context.getPackageName());
            }
            this.d = z86.a(context, "com.crashlytics.CollectUserIdentifiers", true);
            if (!this.d) {
                l86 g3 = c86.g();
                g3.d("Fabric", "User information collection disabled for " + context.getPackageName());
            }
        } else {
            throw new IllegalArgumentException("kits must not be null");
        }
    }

    @DexIgnore
    public boolean a() {
        return this.d;
    }

    @DexIgnore
    public final String b(String str) {
        return str.replaceAll(n, "");
    }

    @DexIgnore
    public synchronized s86 c() {
        if (!this.k) {
            this.j = this.i.a();
            this.k = true;
        }
        return this.j;
    }

    @DexIgnore
    public String d() {
        return this.f;
    }

    @DexIgnore
    public String e() {
        String str = this.g;
        if (str != null) {
            return str;
        }
        SharedPreferences i2 = z86.i(this.e);
        a(i2);
        String string = i2.getString("crashlytics.installation.id", (String) null);
        return string == null ? b(i2) : string;
    }

    @DexIgnore
    public Map<a, String> f() {
        HashMap hashMap = new HashMap();
        for (i86 next : this.h) {
            if (next instanceof e96) {
                for (Map.Entry next2 : ((e96) next).b().entrySet()) {
                    a(hashMap, (a) next2.getKey(), (String) next2.getValue());
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    @DexIgnore
    public String g() {
        return this.b.a(this.e);
    }

    @DexIgnore
    public String h() {
        return String.format(Locale.US, "%s/%s", new Object[]{b(Build.MANUFACTURER), b(Build.MODEL)});
    }

    @DexIgnore
    public String i() {
        return b(Build.VERSION.INCREMENTAL);
    }

    @DexIgnore
    public String j() {
        return b(Build.VERSION.RELEASE);
    }

    @DexIgnore
    public String k() {
        return j() + ZendeskConfig.SLASH + i();
    }

    @DexIgnore
    public Boolean l() {
        if (m()) {
            return b();
        }
        return null;
    }

    @DexIgnore
    public boolean m() {
        return this.c && !this.l.e(this.e);
    }

    @DexIgnore
    public final String a(String str) {
        if (str == null) {
            return null;
        }
        return m.matcher(str).replaceAll("").toLowerCase(Locale.US);
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final String b(SharedPreferences sharedPreferences) {
        this.a.lock();
        try {
            String string = sharedPreferences.getString("crashlytics.installation.id", (String) null);
            if (string == null) {
                string = a(UUID.randomUUID().toString());
                sharedPreferences.edit().putString("crashlytics.installation.id", string).commit();
            }
            return string;
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final void a(SharedPreferences sharedPreferences) {
        s86 c2 = c();
        if (c2 != null) {
            a(sharedPreferences, c2.a);
        }
    }

    @DexIgnore
    @SuppressLint({"CommitPrefEdits"})
    public final void a(SharedPreferences sharedPreferences, String str) {
        this.a.lock();
        try {
            if (!TextUtils.isEmpty(str)) {
                String string = sharedPreferences.getString("crashlytics.advertising.id", (String) null);
                if (TextUtils.isEmpty(string)) {
                    sharedPreferences.edit().putString("crashlytics.advertising.id", str).commit();
                } else if (!string.equals(str)) {
                    sharedPreferences.edit().remove("crashlytics.installation.id").putString("crashlytics.advertising.id", str).commit();
                }
                this.a.unlock();
            }
        } finally {
            this.a.unlock();
        }
    }

    @DexIgnore
    public final Boolean b() {
        s86 c2 = c();
        if (c2 != null) {
            return Boolean.valueOf(c2.b);
        }
        return null;
    }

    @DexIgnore
    public final void a(Map<a, String> map, a aVar, String str) {
        if (str != null) {
            map.put(aVar, str);
        }
    }
}
