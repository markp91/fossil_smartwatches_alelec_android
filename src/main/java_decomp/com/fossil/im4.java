package com.fossil;

import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.LruCache;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class im4 {
    @DexIgnore
    public static /* final */ im4 c; // = new im4();
    @DexIgnore
    public LruCache<String, String> a; // = new LruCache<>(5242880);
    @DexIgnore
    public LruCache<String, BitmapDrawable> b; // = new a(this, 5242880);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends LruCache<String, BitmapDrawable> {
        @DexIgnore
        public a(im4 im4, int i) {
            super(i);
        }

        @DexIgnore
        /* renamed from: a */
        public int sizeOf(String str, BitmapDrawable bitmapDrawable) {
            return bitmapDrawable.getBitmap().getByteCount() / 1024;
        }
    }

    @DexIgnore
    public static im4 b() {
        return c;
    }

    @DexIgnore
    public void a(String str, String str2) {
        this.a.put(str, str2);
    }

    @DexIgnore
    public String c(String str) {
        return this.a.remove(str);
    }

    @DexIgnore
    public String d(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        String str2 = this.a.get(str);
        if (TextUtils.isEmpty(str2)) {
            try {
                str2 = nm4.b(str);
                if (str2 != null) {
                    this.a.put(str, str2);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (TextUtils.isEmpty(str2)) {
            return str2;
        }
        if (str2.contains("\\n")) {
            str2 = str2.replace("\\n", "\n");
        }
        return str2.replaceAll("\\\\", "");
    }

    @DexIgnore
    public boolean a(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        return !TextUtils.isEmpty(this.a.get(str));
    }

    @DexIgnore
    public BitmapDrawable b(String str) {
        return this.b.get(str);
    }

    @DexIgnore
    public void a() {
        this.a.evictAll();
        this.b.evictAll();
    }

    @DexIgnore
    public void a(String str, BitmapDrawable bitmapDrawable) {
        this.b.put(str, bitmapDrawable);
    }
}
