package com.fossil;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.fossil.e12;
import com.fossil.rv1;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ox1 implements gy1 {
    @DexIgnore
    public /* final */ jy1 a;
    @DexIgnore
    public /* final */ Lock b;
    @DexIgnore
    public /* final */ Context c;
    @DexIgnore
    public /* final */ kv1 d;
    @DexIgnore
    public gv1 e;
    @DexIgnore
    public int f;
    @DexIgnore
    public int g; // = 0;
    @DexIgnore
    public int h;
    @DexIgnore
    public /* final */ Bundle i; // = new Bundle();
    @DexIgnore
    public /* final */ Set<rv1.c> j; // = new HashSet();
    @DexIgnore
    public ac3 k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public n12 o;
    @DexIgnore
    public boolean p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public /* final */ e12 r;
    @DexIgnore
    public /* final */ Map<rv1<?>, Boolean> s;
    @DexIgnore
    public /* final */ rv1.a<? extends ac3, lb3> t;
    @DexIgnore
    public ArrayList<Future<?>> u; // = new ArrayList<>();

    @DexIgnore
    public ox1(jy1 jy1, e12 e12, Map<rv1<?>, Boolean> map, kv1 kv1, rv1.a<? extends ac3, lb3> aVar, Lock lock, Context context) {
        this.a = jy1;
        this.r = e12;
        this.s = map;
        this.d = kv1;
        this.t = aVar;
        this.b = lock;
        this.c = context;
    }

    @DexIgnore
    public static String b(int i2) {
        return i2 != 0 ? i2 != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    @DexIgnore
    public final void a(xb3 xb3) {
        if (a(0)) {
            gv1 p2 = xb3.p();
            if (p2.E()) {
                y12 B = xb3.B();
                gv1 B2 = B.B();
                if (!B2.E()) {
                    String valueOf = String.valueOf(B2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GACConnecting", sb.toString(), new Exception());
                    b(B2);
                    return;
                }
                this.n = true;
                this.o = B.p();
                this.p = B.C();
                this.q = B.D();
                e();
            } else if (a(p2)) {
                g();
                e();
            } else {
                b(p2);
            }
        }
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t2) {
        this.a.r.i.add(t2);
        return t2;
    }

    @DexIgnore
    public final void b() {
    }

    @DexIgnore
    public final void c() {
        this.a.g.clear();
        this.m = false;
        this.e = null;
        this.g = 0;
        this.l = true;
        this.n = false;
        this.p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (rv1 next : this.s.keySet()) {
            rv1.f fVar = this.a.f.get(next.a());
            z |= next.c().a() == 1;
            boolean booleanValue = this.s.get(next).booleanValue();
            if (fVar.m()) {
                this.m = true;
                if (booleanValue) {
                    this.j.add(next.a());
                } else {
                    this.l = false;
                }
            }
            hashMap.put(fVar, new qx1(this, next, booleanValue));
        }
        if (z) {
            this.m = false;
        }
        if (this.m) {
            this.r.a(Integer.valueOf(System.identityHashCode(this.a.r)));
            vx1 vx1 = new vx1(this, (nx1) null);
            rv1.a<? extends ac3, lb3> aVar = this.t;
            Context context = this.c;
            Looper f2 = this.a.r.f();
            e12 e12 = this.r;
            this.k = (ac3) aVar.a(context, f2, e12, e12.j(), vx1, vx1);
        }
        this.h = this.a.f.size();
        this.u.add(ky1.a().submit(new px1(this, hashMap)));
    }

    @DexIgnore
    public final boolean d() {
        this.h--;
        int i2 = this.h;
        if (i2 > 0) {
            return false;
        }
        if (i2 < 0) {
            Log.w("GACConnecting", this.a.r.q());
            Log.wtf("GACConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            b(new gv1(8, (PendingIntent) null));
            return false;
        }
        gv1 gv1 = this.e;
        if (gv1 == null) {
            return true;
        }
        this.a.q = this.f;
        b(gv1);
        return false;
    }

    @DexIgnore
    public final void e() {
        if (this.h == 0) {
            if (!this.m || this.n) {
                ArrayList arrayList = new ArrayList();
                this.g = 1;
                this.h = this.a.f.size();
                for (rv1.c next : this.a.f.keySet()) {
                    if (!this.a.g.containsKey(next)) {
                        arrayList.add(this.a.f.get(next));
                    } else if (d()) {
                        f();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.u.add(ky1.a().submit(new ux1(this, arrayList)));
                }
            }
        }
    }

    @DexIgnore
    public final void f(Bundle bundle) {
        if (a(1)) {
            if (bundle != null) {
                this.i.putAll(bundle);
            }
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final void g(int i2) {
        b(new gv1(8, (PendingIntent) null));
    }

    @DexIgnore
    public final void h() {
        ArrayList<Future<?>> arrayList = this.u;
        int size = arrayList.size();
        int i2 = 0;
        while (i2 < size) {
            Future<?> future = arrayList.get(i2);
            i2++;
            future.cancel(true);
        }
        this.u.clear();
    }

    @DexIgnore
    public final Set<Scope> i() {
        e12 e12 = this.r;
        if (e12 == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(e12.i());
        Map<rv1<?>, e12.b> f2 = this.r.f();
        for (rv1 next : f2.keySet()) {
            if (!this.a.g.containsKey(next.a())) {
                hashSet.addAll(f2.get(next).a);
            }
        }
        return hashSet;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    @DexIgnore
    public final void b(gv1 gv1, rv1<?> rv1, boolean z) {
        boolean z2;
        int a2 = rv1.c().a();
        boolean z3 = false;
        if (z) {
            if (!gv1.D() && this.d.a(gv1.p()) == null) {
                z2 = false;
            } else {
                z2 = true;
            }
        }
        if (this.e == null || a2 < this.f) {
            z3 = true;
        }
        if (z3) {
            this.e = gv1;
            this.f = a2;
        }
        this.a.g.put(rv1.a(), gv1);
    }

    @DexIgnore
    public final void g() {
        this.m = false;
        this.a.r.q = Collections.emptySet();
        for (rv1.c next : this.j) {
            if (!this.a.g.containsKey(next)) {
                this.a.g.put(next, new gv1(17, (PendingIntent) null));
            }
        }
    }

    @DexIgnore
    public final void f() {
        this.a.i();
        ky1.a().execute(new nx1(this));
        ac3 ac3 = this.k;
        if (ac3 != null) {
            if (this.p) {
                ac3.a(this.o, this.q);
            }
            a(false);
        }
        for (rv1.c<?> cVar : this.a.g.keySet()) {
            this.a.f.get(cVar).a();
        }
        this.a.s.a(this.i.isEmpty() ? null : this.i);
    }

    @DexIgnore
    public final void b(gv1 gv1) {
        h();
        a(!gv1.D());
        this.a.a(gv1);
        this.a.s.a(gv1);
    }

    @DexIgnore
    public final void a(gv1 gv1, rv1<?> rv1, boolean z) {
        if (a(1)) {
            b(gv1, rv1, z);
            if (d()) {
                f();
            }
        }
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t2) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final boolean a() {
        h();
        a(true);
        this.a.a((gv1) null);
        return true;
    }

    @DexIgnore
    public final boolean a(gv1 gv1) {
        return this.l && !gv1.D();
    }

    @DexIgnore
    public final void a(boolean z) {
        ac3 ac3 = this.k;
        if (ac3 != null) {
            if (ac3.c() && z) {
                this.k.h();
            }
            this.k.a();
            if (this.r.k()) {
                this.k = null;
            }
            this.o = null;
        }
    }

    @DexIgnore
    public final boolean a(int i2) {
        if (this.g == i2) {
            return true;
        }
        Log.w("GACConnecting", this.a.r.q());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GACConnecting", sb.toString());
        int i3 = this.h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i3);
        Log.w("GACConnecting", sb2.toString());
        String b2 = b(this.g);
        String b3 = b(i2);
        StringBuilder sb3 = new StringBuilder(String.valueOf(b2).length() + 70 + String.valueOf(b3).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(b2);
        sb3.append(" but received callback for step ");
        sb3.append(b3);
        Log.e("GACConnecting", sb3.toString(), new Exception());
        b(new gv1(8, (PendingIntent) null));
        return false;
    }
}
