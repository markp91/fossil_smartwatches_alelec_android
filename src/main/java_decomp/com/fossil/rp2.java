package com.fossil;

import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rp2 implements Iterator<Map.Entry<K, V>> {
    @DexIgnore
    public int a;
    @DexIgnore
    public boolean b;
    @DexIgnore
    public Iterator<Map.Entry<K, V>> c;
    @DexIgnore
    public /* final */ /* synthetic */ jp2 d;

    @DexIgnore
    public rp2(jp2 jp2) {
        this.d = jp2;
        this.a = -1;
    }

    @DexIgnore
    public final boolean hasNext() {
        if (this.a + 1 < this.d.b.size() || (!this.d.c.isEmpty() && zza().hasNext())) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public final /* synthetic */ Object next() {
        this.b = true;
        int i = this.a + 1;
        this.a = i;
        if (i < this.d.b.size()) {
            return (Map.Entry) this.d.b.get(this.a);
        }
        return (Map.Entry) zza().next();
    }

    @DexIgnore
    public final void remove() {
        if (this.b) {
            this.b = false;
            this.d.f();
            if (this.a < this.d.b.size()) {
                jp2 jp2 = this.d;
                int i = this.a;
                this.a = i - 1;
                Object unused = jp2.b(i);
                return;
            }
            zza().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    @DexIgnore
    public final Iterator<Map.Entry<K, V>> zza() {
        if (this.c == null) {
            this.c = this.d.c.entrySet().iterator();
        }
        return this.c;
    }

    @DexIgnore
    public /* synthetic */ rp2(jp2 jp2, ip2 ip2) {
        this(jp2);
    }
}
