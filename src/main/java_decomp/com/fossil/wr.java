package com.fossil;

import java.security.MessageDigest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wr<T> {
    @DexIgnore
    public static /* final */ b<Object> e; // = new a();
    @DexIgnore
    public /* final */ T a;
    @DexIgnore
    public /* final */ b<T> b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public volatile byte[] d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements b<Object> {
        @DexIgnore
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    }

    @DexIgnore
    public interface b<T> {
        @DexIgnore
        void a(byte[] bArr, T t, MessageDigest messageDigest);
    }

    @DexIgnore
    public wr(String str, T t, b<T> bVar) {
        q00.a(str);
        this.c = str;
        this.a = t;
        q00.a(bVar);
        this.b = bVar;
    }

    @DexIgnore
    public static <T> wr<T> a(String str) {
        return new wr<>(str, (Object) null, c());
    }

    @DexIgnore
    public static <T> b<T> c() {
        return e;
    }

    @DexIgnore
    public final byte[] b() {
        if (this.d == null) {
            this.d = this.c.getBytes(vr.a);
        }
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof wr) {
            return this.c.equals(((wr) obj).c);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Option{key='" + this.c + '\'' + '}';
    }

    @DexIgnore
    public static <T> wr<T> a(String str, T t) {
        return new wr<>(str, t, c());
    }

    @DexIgnore
    public static <T> wr<T> a(String str, T t, b<T> bVar) {
        return new wr<>(str, t, bVar);
    }

    @DexIgnore
    public T a() {
        return this.a;
    }

    @DexIgnore
    public void a(T t, MessageDigest messageDigest) {
        this.b.a(b(), t, messageDigest);
    }
}
