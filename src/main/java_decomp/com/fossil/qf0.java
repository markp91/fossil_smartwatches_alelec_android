package com.fossil;

import java.io.ByteArrayOutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qf0 extends jb1<z70[], z70[]> {
    @DexIgnore
    public static /* final */ u31<z70[]>[] b; // = {new dm1(), new tn1()};
    @DexIgnore
    public static /* final */ ed1<z70[]>[] c; // = new ed1[0];
    @DexIgnore
    public static /* final */ qf0 d; // = new qf0();

    @DexIgnore
    public ed1<z70[]>[] b() {
        return c;
    }

    @DexIgnore
    public u31<z70[]>[] a() {
        return b;
    }

    @DexIgnore
    public final byte[] a(NotificationFilter[] notificationFilterArr) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        for (NotificationFilter b2 : notificationFilterArr) {
            byteArrayOutputStream.write(b2.b());
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        wg6.a(byteArray, "entriesData.toByteArray()");
        return byteArray;
    }
}
