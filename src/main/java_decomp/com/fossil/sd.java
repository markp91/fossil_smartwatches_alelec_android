package com.fossil;

import androidx.lifecycle.LiveData;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sd {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements ld<X> {
        @DexIgnore
        public /* final */ /* synthetic */ jd a;
        @DexIgnore
        public /* final */ /* synthetic */ v3 b;

        @DexIgnore
        public a(jd jdVar, v3 v3Var) {
            this.a = jdVar;
            this.b = v3Var;
        }

        @DexIgnore
        public void onChanged(X x) {
            this.a.b(this.b.apply(x));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements ld<X> {
        @DexIgnore
        public LiveData<Y> a;
        @DexIgnore
        public /* final */ /* synthetic */ v3 b;
        @DexIgnore
        public /* final */ /* synthetic */ jd c;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements ld<Y> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void onChanged(Y y) {
                b.this.c.b(y);
            }
        }

        @DexIgnore
        public b(v3 v3Var, jd jdVar) {
            this.b = v3Var;
            this.c = jdVar;
        }

        @DexIgnore
        public void onChanged(X x) {
            LiveData<Y> liveData = (LiveData) this.b.apply(x);
            LiveData<Y> liveData2 = this.a;
            if (liveData2 != liveData) {
                if (liveData2 != null) {
                    this.c.a(liveData2);
                }
                this.a = liveData;
                LiveData<Y> liveData3 = this.a;
                if (liveData3 != null) {
                    this.c.a(liveData3, new a());
                }
            }
        }
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> a(LiveData<X> liveData, v3<X, Y> v3Var) {
        jd jdVar = new jd();
        jdVar.a(liveData, new a(jdVar, v3Var));
        return jdVar;
    }

    @DexIgnore
    public static <X, Y> LiveData<Y> b(LiveData<X> liveData, v3<X, LiveData<Y>> v3Var) {
        jd jdVar = new jd();
        jdVar.a(liveData, new b(v3Var, jdVar));
        return jdVar;
    }
}
