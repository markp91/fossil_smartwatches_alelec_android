package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bn6 extends kn6 {
    @DexIgnore
    public ig6<? super il6, ? super xe6<? super cd6>, ? extends Object> d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public bn6(af6 af6, ig6<? super il6, ? super xe6<? super cd6>, ? extends Object> ig6) {
        super(af6, false);
        wg6.b(af6, "parentContext");
        wg6.b(ig6, "block");
        this.d = ig6;
    }

    @DexIgnore
    public void o() {
        ig6<? super il6, ? super xe6<? super cd6>, ? extends Object> ig6 = this.d;
        if (ig6 != null) {
            this.d = null;
            cp6.a(ig6, this, this);
            return;
        }
        throw new IllegalStateException("Already started".toString());
    }
}
