package com.fossil;

import com.fossil.xe;
import com.fossil.ye;
import java.util.IdentityHashMap;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jf<K, A, B> extends ye<K, B> {
    @DexIgnore
    public /* final */ ye<K, A> a;
    @DexIgnore
    public /* final */ v3<List<A>, List<B>> b;
    @DexIgnore
    public /* final */ IdentityHashMap<B, K> c; // = new IdentityHashMap<>();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends ye.c<A> {
        @DexIgnore
        public /* final */ /* synthetic */ ye.c a;

        @DexIgnore
        public a(ye.c cVar) {
            this.a = cVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(jf.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends ye.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ ye.a a;

        @DexIgnore
        public b(ye.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(jf.this.a(list));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends ye.a<A> {
        @DexIgnore
        public /* final */ /* synthetic */ ye.a a;

        @DexIgnore
        public c(ye.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public void a(List<A> list) {
            this.a.a(jf.this.a(list));
        }
    }

    @DexIgnore
    public jf(ye<K, A> yeVar, v3<List<A>, List<B>> v3Var) {
        this.a = yeVar;
        this.b = v3Var;
    }

    @DexIgnore
    public List<B> a(List<A> list) {
        List<B> convert = xe.convert(this.b, list);
        synchronized (this.c) {
            for (int i = 0; i < convert.size(); i++) {
                this.c.put(convert.get(i), this.a.getKey(list.get(i)));
            }
        }
        return convert;
    }

    @DexIgnore
    public void addInvalidatedCallback(xe.c cVar) {
        this.a.addInvalidatedCallback(cVar);
    }

    @DexIgnore
    public K getKey(B b2) {
        K k;
        synchronized (this.c) {
            k = this.c.get(b2);
        }
        return k;
    }

    @DexIgnore
    public void invalidate() {
        this.a.invalidate();
    }

    @DexIgnore
    public boolean isInvalid() {
        return this.a.isInvalid();
    }

    @DexIgnore
    public void loadAfter(ye.f<K> fVar, ye.a<B> aVar) {
        this.a.loadAfter(fVar, new b(aVar));
    }

    @DexIgnore
    public void loadBefore(ye.f<K> fVar, ye.a<B> aVar) {
        this.a.loadBefore(fVar, new c(aVar));
    }

    @DexIgnore
    public void loadInitial(ye.e<K> eVar, ye.c<B> cVar) {
        this.a.loadInitial(eVar, new a(cVar));
    }

    @DexIgnore
    public void removeInvalidatedCallback(xe.c cVar) {
        this.a.removeInvalidatedCallback(cVar);
    }
}
