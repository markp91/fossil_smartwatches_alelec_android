package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q60 extends r60 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public static /* final */ int c; // = 65535;
    @DexIgnore
    public /* final */ int b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<q60> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final q60 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 2) {
                return new q60(cw0.b(ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN).getShort(0)));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Invalid data size: "), bArr.length, ", require: 2"));
        }

        @DexIgnore
        public q60 createFromParcel(Parcel parcel) {
            return new q60(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new q60[i];
        }

        @DexIgnore
        /* renamed from: createFromParcel  reason: collision with other method in class */
        public Object m53createFromParcel(Parcel parcel) {
            return new q60(parcel, (qg6) null);
        }
    }

    /*
    static {
        mh6 mh6 = mh6.a;
    }
    */

    @DexIgnore
    public q60(int i) throws IllegalArgumentException {
        super(s60.DAILY_TOTAL_ACTIVE_MINUTE);
        this.b = i;
        e();
    }

    @DexIgnore
    public byte[] c() {
        byte[] array = ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) this.b).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026                 .array()");
        return array;
    }

    @DexIgnore
    public final void e() throws IllegalArgumentException {
        int i = c;
        int i2 = this.b;
        if (!(i2 >= 0 && i >= i2)) {
            StringBuilder b2 = ze0.b("minute(");
            b2.append(this.b);
            b2.append(") is out of range ");
            b2.append("[0, ");
            throw new IllegalArgumentException(ze0.a(b2, c, "]."));
        }
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(q60.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return this.b == ((q60) obj).b;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.config.DailyTotalActiveMinuteConfig");
    }

    @DexIgnore
    public final int getMinute() {
        return this.b;
    }

    @DexIgnore
    public int hashCode() {
        return this.b;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
    }

    @DexIgnore
    public Integer d() {
        return Integer.valueOf(this.b);
    }

    @DexIgnore
    public /* synthetic */ q60(Parcel parcel, qg6 qg6) {
        super(parcel);
        this.b = parcel.readInt();
        e();
    }
}
