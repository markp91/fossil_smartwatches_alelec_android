package com.fossil;

import android.os.Bundle;
import com.fossil.rv1;
import java.util.Collections;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xx1 implements gy1 {
    @DexIgnore
    public /* final */ jy1 a;

    @DexIgnore
    public xx1(jy1 jy1) {
        this.a = jy1;
    }

    @DexIgnore
    public final <A extends rv1.b, T extends nw1<? extends ew1, A>> T a(T t) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    @DexIgnore
    public final void a(gv1 gv1, rv1<?> rv1, boolean z) {
    }

    @DexIgnore
    public final boolean a() {
        return true;
    }

    @DexIgnore
    public final <A extends rv1.b, R extends ew1, T extends nw1<R, A>> T b(T t) {
        this.a.r.i.add(t);
        return t;
    }

    @DexIgnore
    public final void c() {
        for (rv1.f a2 : this.a.f.values()) {
            a2.a();
        }
        this.a.r.q = Collections.emptySet();
    }

    @DexIgnore
    public final void f(Bundle bundle) {
    }

    @DexIgnore
    public final void g(int i) {
    }

    @DexIgnore
    public final void b() {
        this.a.h();
    }
}
