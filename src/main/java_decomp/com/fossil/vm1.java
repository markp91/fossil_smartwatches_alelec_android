package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm1 {
    @DexIgnore
    public /* synthetic */ vm1(qg6 qg6) {
    }

    @DexIgnore
    public final re0 a(byte b) {
        re0 re0;
        re0[] values = re0.values();
        int length = values.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                re0 = null;
                break;
            }
            re0 = values[i];
            if (re0.b == b) {
                break;
            }
            i++;
        }
        return re0 != null ? re0 : re0.FIRMWARE_INTERNAL_ERROR;
    }
}
