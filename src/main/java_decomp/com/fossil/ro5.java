package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.UserCustomizeThemeViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ro5 implements Factory<qo5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public ro5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ro5 a(Provider<ThemeRepository> provider) {
        return new ro5(provider);
    }

    @DexIgnore
    public static qo5 b(Provider<ThemeRepository> provider) {
        return new UserCustomizeThemeViewModel(provider.get());
    }

    @DexIgnore
    public UserCustomizeThemeViewModel get() {
        return b(this.a);
    }
}
