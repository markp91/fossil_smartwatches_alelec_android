package com.fossil;

import com.portfolio.platform.data.source.NotificationsRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.receiver.AppPackageInstallReceiver;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class no4 implements MembersInjector<mo4> {
    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, NotificationsRepository notificationsRepository) {
        appPackageInstallReceiver.a = notificationsRepository;
    }

    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, an4 an4) {
        appPackageInstallReceiver.b = an4;
    }

    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, cj4 cj4) {
        appPackageInstallReceiver.c = cj4;
    }

    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, d15 d15) {
        appPackageInstallReceiver.d = d15;
    }

    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, mz4 mz4) {
        appPackageInstallReceiver.e = mz4;
    }

    @DexIgnore
    public static void a(AppPackageInstallReceiver appPackageInstallReceiver, NotificationSettingsDatabase notificationSettingsDatabase) {
        appPackageInstallReceiver.f = notificationSettingsDatabase;
    }
}
