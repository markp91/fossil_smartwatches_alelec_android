package com.fossil;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.cardview.widget.CardView;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class pf4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CardView q;
    @DexIgnore
    public /* final */ FlexibleTextView r;
    @DexIgnore
    public /* final */ FlexibleTextView s;
    @DexIgnore
    public /* final */ FlexibleTextView t;
    @DexIgnore
    public /* final */ FlexibleSwitchCompat u;

    @DexIgnore
    public pf4(Object obj, View view, int i, CardView cardView, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleSwitchCompat flexibleSwitchCompat) {
        super(obj, view, i);
        this.q = cardView;
        this.r = flexibleTextView;
        this.s = flexibleTextView2;
        this.t = flexibleTextView3;
        this.u = flexibleSwitchCompat;
    }

    @DexIgnore
    public static pf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z) {
        return a(layoutInflater, viewGroup, z, kb.a());
    }

    @DexIgnore
    @Deprecated
    public static pf4 a(LayoutInflater layoutInflater, ViewGroup viewGroup, boolean z, Object obj) {
        return ViewDataBinding.a(layoutInflater, 2131558638, viewGroup, z, obj);
    }
}
