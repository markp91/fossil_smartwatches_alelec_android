package com.fossil;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.facebook.places.internal.LocationScannerImpl;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jj3 {
    @DexIgnore
    @Deprecated
    public float a;
    @DexIgnore
    @Deprecated
    public float b;
    @DexIgnore
    @Deprecated
    public float c;
    @DexIgnore
    @Deprecated
    public float d;
    @DexIgnore
    @Deprecated
    public float e;
    @DexIgnore
    @Deprecated
    public float f;
    @DexIgnore
    public /* final */ List<f> g; // = new ArrayList();
    @DexIgnore
    public /* final */ List<g> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends g {
        @DexIgnore
        public /* final */ /* synthetic */ List b;
        @DexIgnore
        public /* final */ /* synthetic */ Matrix c;

        @DexIgnore
        public a(jj3 jj3, List list, Matrix matrix) {
            this.b = list;
            this.c = matrix;
        }

        @DexIgnore
        public void a(Matrix matrix, vi3 vi3, int i, Canvas canvas) {
            for (g a : this.b) {
                a.a(this.c, vi3, i, canvas);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends g {
        @DexIgnore
        public /* final */ d b;

        @DexIgnore
        public b(d dVar) {
            this.b = dVar;
        }

        @DexIgnore
        public void a(Matrix matrix, vi3 vi3, int i, Canvas canvas) {
            float e = this.b.d();
            float f = this.b.e();
            vi3.a(canvas, matrix, new RectF(this.b.b(), this.b.f(), this.b.c(), this.b.a()), i, e, f);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d extends f {
        @DexIgnore
        public static /* final */ RectF h; // = new RectF();
        @DexIgnore
        @Deprecated
        public float b;
        @DexIgnore
        @Deprecated
        public float c;
        @DexIgnore
        @Deprecated
        public float d;
        @DexIgnore
        @Deprecated
        public float e;
        @DexIgnore
        @Deprecated
        public float f;
        @DexIgnore
        @Deprecated
        public float g;

        @DexIgnore
        public d(float f2, float f3, float f4, float f5) {
            b(f2);
            f(f3);
            c(f4);
            a(f5);
        }

        @DexIgnore
        public final float c() {
            return this.d;
        }

        @DexIgnore
        public final float d() {
            return this.f;
        }

        @DexIgnore
        public final float e() {
            return this.g;
        }

        @DexIgnore
        public final float f() {
            return this.c;
        }

        @DexIgnore
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            h.set(b(), f(), c(), a());
            path.arcTo(h, d(), e(), false);
            path.transform(matrix);
        }

        @DexIgnore
        public final float b() {
            return this.b;
        }

        @DexIgnore
        public final void c(float f2) {
            this.d = f2;
        }

        @DexIgnore
        public final void d(float f2) {
            this.f = f2;
        }

        @DexIgnore
        public final void e(float f2) {
            this.g = f2;
        }

        @DexIgnore
        public final void f(float f2) {
            this.c = f2;
        }

        @DexIgnore
        public final void b(float f2) {
            this.b = f2;
        }

        @DexIgnore
        public final float a() {
            return this.e;
        }

        @DexIgnore
        public final void a(float f2) {
            this.e = f2;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class f {
        @DexIgnore
        public /* final */ Matrix a; // = new Matrix();

        @DexIgnore
        public abstract void a(Matrix matrix, Path path);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class g {
        @DexIgnore
        public static /* final */ Matrix a; // = new Matrix();

        @DexIgnore
        public abstract void a(Matrix matrix, vi3 vi3, int i, Canvas canvas);

        @DexIgnore
        public final void a(vi3 vi3, int i, Canvas canvas) {
            a(a, vi3, i, canvas);
        }
    }

    @DexIgnore
    public jj3() {
        b(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5) {
        f(f2);
        g(f3);
        d(f2);
        e(f3);
        b(f4);
        c((f4 + f5) % 360.0f);
        this.g.clear();
        this.h.clear();
    }

    @DexIgnore
    public void b(float f2, float f3) {
        a(f2, f3, 270.0f, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public float c() {
        return this.c;
    }

    @DexIgnore
    public float d() {
        return this.d;
    }

    @DexIgnore
    public float e() {
        return this.a;
    }

    @DexIgnore
    public float f() {
        return this.b;
    }

    @DexIgnore
    public final void g(float f2) {
        this.b = f2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e extends f {
        @DexIgnore
        public float b;
        @DexIgnore
        public float c;

        @DexIgnore
        public void a(Matrix matrix, Path path) {
            Matrix matrix2 = this.a;
            matrix.invert(matrix2);
            path.transform(matrix2);
            path.lineTo(this.b, this.c);
            path.transform(matrix);
        }
    }

    @DexIgnore
    public final float b() {
        return this.f;
    }

    @DexIgnore
    public final void c(float f2) {
        this.f = f2;
    }

    @DexIgnore
    public final void d(float f2) {
        this.c = f2;
    }

    @DexIgnore
    public final void e(float f2) {
        this.d = f2;
    }

    @DexIgnore
    public final void f(float f2) {
        this.a = f2;
    }

    @DexIgnore
    public final void b(float f2) {
        this.e = f2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends g {
        @DexIgnore
        public /* final */ e b;
        @DexIgnore
        public /* final */ float c;
        @DexIgnore
        public /* final */ float d;

        @DexIgnore
        public c(e eVar, float f, float f2) {
            this.b = eVar;
            this.c = f;
            this.d = f2;
        }

        @DexIgnore
        public void a(Matrix matrix, vi3 vi3, int i, Canvas canvas) {
            RectF rectF = new RectF(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (float) Math.hypot((double) (this.b.c - this.d), (double) (this.b.b - this.c)), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
            Matrix matrix2 = new Matrix(matrix);
            matrix2.preTranslate(this.c, this.d);
            matrix2.preRotate(a());
            vi3.a(canvas, matrix2, rectF, i);
        }

        @DexIgnore
        public float a() {
            return (float) Math.toDegrees(Math.atan((double) ((this.b.c - this.d) / (this.b.b - this.c))));
        }
    }

    @DexIgnore
    public void a(float f2, float f3) {
        e eVar = new e();
        float unused = eVar.b = f2;
        float unused2 = eVar.c = f3;
        this.g.add(eVar);
        c cVar = new c(eVar, c(), d());
        a(cVar, cVar.a() + 270.0f, cVar.a() + 270.0f);
        d(f2);
        e(f3);
    }

    @DexIgnore
    public void a(float f2, float f3, float f4, float f5, float f6, float f7) {
        d dVar = new d(f2, f3, f4, f5);
        dVar.d(f6);
        dVar.e(f7);
        this.g.add(dVar);
        b bVar = new b(dVar);
        float f8 = f6 + f7;
        boolean z = f7 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (z) {
            f6 = (f6 + 180.0f) % 360.0f;
        }
        a(bVar, f6, z ? (180.0f + f8) % 360.0f : f8);
        double d2 = (double) f8;
        d(((f2 + f4) * 0.5f) + (((f4 - f2) / 2.0f) * ((float) Math.cos(Math.toRadians(d2)))));
        e(((f3 + f5) * 0.5f) + (((f5 - f3) / 2.0f) * ((float) Math.sin(Math.toRadians(d2)))));
    }

    @DexIgnore
    public void a(Matrix matrix, Path path) {
        int size = this.g.size();
        for (int i = 0; i < size; i++) {
            this.g.get(i).a(matrix, path);
        }
    }

    @DexIgnore
    public g a(Matrix matrix) {
        a(b());
        return new a(this, new ArrayList(this.h), matrix);
    }

    @DexIgnore
    public final void a(g gVar, float f2, float f3) {
        a(f2);
        this.h.add(gVar);
        b(f3);
    }

    @DexIgnore
    public final void a(float f2) {
        if (a() != f2) {
            float a2 = ((f2 - a()) + 360.0f) % 360.0f;
            if (a2 <= 180.0f) {
                d dVar = new d(c(), d(), c(), d());
                dVar.d(a());
                dVar.e(a2);
                this.h.add(new b(dVar));
                b(f2);
            }
        }
    }

    @DexIgnore
    public final float a() {
        return this.e;
    }
}
