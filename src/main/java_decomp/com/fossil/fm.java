package com.fossil;

import androidx.lifecycle.MutableLiveData;
import com.fossil.wl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class fm implements wl {
    @DexIgnore
    public /* final */ MutableLiveData<wl.b> c; // = new MutableLiveData<>();
    @DexIgnore
    public /* final */ so<wl.b.c> d; // = so.e();

    @DexIgnore
    public fm() {
        a(wl.b);
    }

    @DexIgnore
    public void a(wl.b bVar) {
        this.c.a(bVar);
        if (bVar instanceof wl.b.c) {
            this.d.b((wl.b.c) bVar);
        } else if (bVar instanceof wl.b.a) {
            this.d.a(((wl.b.a) bVar).a());
        }
    }
}
