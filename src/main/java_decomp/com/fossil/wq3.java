package com.fossil;

import android.os.IBinder;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class wq3 implements Runnable {
    @DexIgnore
    public /* final */ us3 a;
    @DexIgnore
    public /* final */ IBinder b;

    @DexIgnore
    public wq3(us3 us3, IBinder iBinder) {
        this.a = us3;
        this.b = iBinder;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x001e, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0020, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0021, code lost:
        r0.a(0, r1.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0029, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x002b, code lost:
        throw r1;
     */
    @DexIgnore
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:5:0x000a, B:9:0x000f] */
    public final void run() {
        us3 us3 = this.a;
        IBinder iBinder = this.b;
        synchronized (us3) {
            if (iBinder == null) {
                us3.a(0, "Null service connection");
                return;
            }
            us3.c = new ar3(iBinder);
            us3.a = 2;
            us3.a();
        }
    }
}
