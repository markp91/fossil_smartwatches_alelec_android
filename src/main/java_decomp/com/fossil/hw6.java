package com.fossil;

import java.io.ObjectStreamException;
import java.io.Serializable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class hw6 implements xv6, Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 7535258609338176893L;
    @DexIgnore
    public String name;

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public Object readResolve() throws ObjectStreamException {
        return yv6.a(getName());
    }
}
