package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p82 implements Parcelable.Creator<o82> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        ArrayList<f72> arrayList = null;
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                arrayList = f22.c(parcel, a, f72.CREATOR);
            } else if (a2 != 2) {
                f22.v(parcel, a);
            } else {
                status = (Status) f22.a(parcel, a, Status.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new o82(arrayList, status);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new o82[i];
    }
}
