package com.fossil;

import android.os.Parcel;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class wy2 extends kh2 implements vy2 {
    @DexIgnore
    public wy2() {
        super("com.google.android.gms.maps.internal.IOnCameraMoveStartedListener");
    }

    @DexIgnore
    public final boolean a(int i, Parcel parcel, Parcel parcel2, int i2) throws RemoteException {
        if (i != 1) {
            return false;
        }
        a(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
