package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p32 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public /* final */ int c; // = 129;

    @DexIgnore
    public p32(String str, String str2, boolean z, int i) {
        this.b = str;
        this.a = str2;
    }

    @DexIgnore
    public final String a() {
        return this.b;
    }

    @DexIgnore
    public final int b() {
        return this.c;
    }

    @DexIgnore
    public final String c() {
        return this.a;
    }
}
