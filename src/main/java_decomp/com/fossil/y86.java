package com.fossil;

import android.os.Process;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class y86 implements Runnable {
    @DexIgnore
    public abstract void a();

    @DexIgnore
    public final void run() {
        Process.setThreadPriority(10);
        a();
    }
}
