package com.fossil;

import com.fossil.fr1;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class br1 extends fr1 {
    @DexIgnore
    public /* final */ zs1 a;
    @DexIgnore
    public /* final */ Map<go1, fr1.b> b;

    @DexIgnore
    public br1(zs1 zs1, Map<go1, fr1.b> map) {
        if (zs1 != null) {
            this.a = zs1;
            if (map != null) {
                this.b = map;
                return;
            }
            throw new NullPointerException("Null values");
        }
        throw new NullPointerException("Null clock");
    }

    @DexIgnore
    public zs1 a() {
        return this.a;
    }

    @DexIgnore
    public Map<go1, fr1.b> b() {
        return this.b;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof fr1)) {
            return false;
        }
        fr1 fr1 = (fr1) obj;
        if (!this.a.equals(fr1.a()) || !this.b.equals(fr1.b())) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public int hashCode() {
        return ((this.a.hashCode() ^ 1000003) * 1000003) ^ this.b.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "SchedulerConfig{clock=" + this.a + ", values=" + this.b + "}";
    }
}
