package com.fossil;

import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class n35 {
    @DexIgnore
    public String a;
    @DexIgnore
    public String b;
    @DexIgnore
    public ArrayList<o35> c;
    @DexIgnore
    public boolean d;

    @DexIgnore
    public n35(String str, String str2, ArrayList<o35> arrayList, boolean z) {
        wg6.b(str, "mPresetId");
        wg6.b(str2, "mPresetName");
        wg6.b(arrayList, "mMicroApps");
        this.a = str;
        this.b = str2;
        this.c = arrayList;
        this.d = z;
    }

    @DexIgnore
    public final ArrayList<o35> a() {
        return this.c;
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final String c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.d;
    }
}
