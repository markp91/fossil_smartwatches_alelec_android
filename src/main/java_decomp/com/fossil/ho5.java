package com.fossil;

import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.user.CustomizeSleepChartViewModel;
import dagger.internal.Factory;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ho5 implements Factory<go5> {
    @DexIgnore
    public /* final */ Provider<ThemeRepository> a;

    @DexIgnore
    public ho5(Provider<ThemeRepository> provider) {
        this.a = provider;
    }

    @DexIgnore
    public static ho5 a(Provider<ThemeRepository> provider) {
        return new ho5(provider);
    }

    @DexIgnore
    public static go5 b(Provider<ThemeRepository> provider) {
        return new CustomizeSleepChartViewModel(provider.get());
    }

    @DexIgnore
    public CustomizeSleepChartViewModel get() {
        return b(this.a);
    }
}
