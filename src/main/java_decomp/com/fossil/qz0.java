package com.fossil;

import android.database.Cursor;
import android.os.CancellationSignal;
import com.fossil.blesdk.database.SdkDatabase;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qz0 {
    @DexIgnore
    public static /* final */ qz0 a; // = new qz0();

    @DexIgnore
    public final List<ie1> a(String str, byte b) {
        ArrayList arrayList;
        JSONArray jSONArray;
        Integer num;
        char c;
        w81 a2;
        String str2 = str;
        byte b2 = b;
        SdkDatabase a3 = SdkDatabase.e.a();
        if (a3 == null || (a2 = a3.a()) == null) {
            arrayList = null;
        } else {
            rh b3 = rh.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ?", 2);
            if (str2 == null) {
                b3.a(1);
            } else {
                b3.a(1, str2);
            }
            b3.a(2, (long) b2);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = bi.a(a2.a, b3, false, (CancellationSignal) null);
            try {
                int b4 = ai.b(a4, "id");
                int b5 = ai.b(a4, "deviceMacAddress");
                int b6 = ai.b(a4, "fileType");
                int b7 = ai.b(a4, "fileIndex");
                int b8 = ai.b(a4, "rawData");
                int b9 = ai.b(a4, "fileLength");
                int b10 = ai.b(a4, "fileCrc");
                int b11 = ai.b(a4, "createdTimeStamp");
                int b12 = ai.b(a4, "isCompleted");
                arrayList = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    int i = b5;
                    ie1 ie1 = new ie1(a4.getString(b5), (byte) a4.getShort(b6), (byte) a4.getShort(b7), a4.getBlob(b8), a4.getLong(b9), a4.getLong(b10), a4.getLong(b11), a4.getInt(b12) != 0);
                    ie1.a = a4.getInt(b4);
                    arrayList.add(ie1);
                    b5 = i;
                }
            } finally {
                a4.close();
                b3.c();
            }
        }
        boolean z = arrayList != null;
        String str3 = z ? "" : "Database instance is null";
        JSONObject a5 = cw0.a(new g01(b2, (byte) 255).a(), bm0.IS_COMPLETED, JSONObject.NULL);
        bm0 bm0 = bm0.FILES;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new ie1[0]);
            if (array != null) {
                jSONArray = cw0.a((p40[]) (ie1[]) array);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        a(xx0.QUERY, z, str, cw0.a(a5, bm0, (Object) jSONArray), str3);
        oa1 oa1 = oa1.a;
        Object[] objArr = new Object[3];
        objArr[0] = str2;
        objArr[1] = Byte.valueOf(b);
        if (arrayList != null) {
            num = Integer.valueOf(arrayList.size());
            c = 2;
        } else {
            c = 2;
            num = null;
        }
        objArr[c] = num;
        return arrayList != null ? arrayList : qd6.a();
    }

    @DexIgnore
    public final List<ie1> b(String str, byte b) {
        ArrayList arrayList;
        JSONArray jSONArray;
        Integer num;
        char c;
        w81 a2;
        String str2 = str;
        byte b2 = b;
        SdkDatabase a3 = SdkDatabase.e.a();
        if (a3 == null || (a2 = a3.a()) == null) {
            arrayList = null;
        } else {
            rh b3 = rh.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ? and isCompleted = 0", 2);
            if (str2 == null) {
                b3.a(1);
            } else {
                b3.a(1, str2);
            }
            b3.a(2, (long) b2);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = bi.a(a2.a, b3, false, (CancellationSignal) null);
            try {
                int b4 = ai.b(a4, "id");
                int b5 = ai.b(a4, "deviceMacAddress");
                int b6 = ai.b(a4, "fileType");
                int b7 = ai.b(a4, "fileIndex");
                int b8 = ai.b(a4, "rawData");
                int b9 = ai.b(a4, "fileLength");
                int b10 = ai.b(a4, "fileCrc");
                int b11 = ai.b(a4, "createdTimeStamp");
                int b12 = ai.b(a4, "isCompleted");
                arrayList = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    int i = b5;
                    ie1 ie1 = new ie1(a4.getString(b5), (byte) a4.getShort(b6), (byte) a4.getShort(b7), a4.getBlob(b8), a4.getLong(b9), a4.getLong(b10), a4.getLong(b11), a4.getInt(b12) != 0);
                    ie1.a = a4.getInt(b4);
                    arrayList.add(ie1);
                    b5 = i;
                }
            } finally {
                a4.close();
                b3.c();
            }
        }
        boolean z = arrayList != null;
        String str3 = z ? "" : "Database instance is null";
        JSONObject a5 = cw0.a(new g01(b2, (byte) 255).a(), bm0.IS_COMPLETED, (Object) false);
        bm0 bm0 = bm0.FILES;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new ie1[0]);
            if (array != null) {
                jSONArray = cw0.a((p40[]) (ie1[]) array);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        a(xx0.QUERY, z, str, cw0.a(a5, bm0, (Object) jSONArray), str3);
        oa1 oa1 = oa1.a;
        Object[] objArr = new Object[3];
        objArr[0] = str2;
        objArr[1] = Byte.valueOf(b);
        if (arrayList != null) {
            num = Integer.valueOf(arrayList.size());
            c = 2;
        } else {
            c = 2;
            num = null;
        }
        objArr[c] = num;
        return arrayList != null ? arrayList : qd6.a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r0 = r0.a();
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0052  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00aa  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00af A[ORIG_RETURN, RETURN, SYNTHETIC] */
    public final long a(ie1 ie1) {
        String str;
        boolean z;
        String str2;
        w81 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        Long valueOf = (a3 == null || a2 == null) ? null : Long.valueOf(a2.a(ie1));
        if (valueOf != null && ci6.a(Long.MIN_VALUE, 0).b(valueOf.longValue())) {
            str2 = "Insert Fail";
        } else if (valueOf == null) {
            str2 = "Database instance is null";
        } else {
            str = "";
            z = true;
            a(xx0.INSERT, z, ie1.b, cw0.a(cw0.a(new JSONObject(), bm0.FILE, (Object) ie1.a(true)), bm0.ROW_ID, valueOf == null ? valueOf : -1), str);
            oa1 oa1 = oa1.a;
            Object[] objArr = {ie1.b, Byte.valueOf(ie1.c), Byte.valueOf(ie1.d), Long.valueOf(ie1.g), Integer.valueOf(ie1.e.length), Long.valueOf(ie1.f), Boolean.valueOf(ie1.i), valueOf};
            if (valueOf == null) {
                return valueOf.longValue();
            }
            return -1;
        }
        str = str2;
        z = false;
        a(xx0.INSERT, z, ie1.b, cw0.a(cw0.a(new JSONObject(), bm0.FILE, (Object) ie1.a(true)), bm0.ROW_ID, valueOf == null ? valueOf : -1), str);
        oa1 oa12 = oa1.a;
        Object[] objArr2 = {ie1.b, Byte.valueOf(ie1.c), Byte.valueOf(ie1.d), Long.valueOf(ie1.g), Integer.valueOf(ie1.e.length), Long.valueOf(ie1.f), Boolean.valueOf(ie1.i), valueOf};
        if (valueOf == null) {
        }
    }

    @DexIgnore
    public final List<ie1> b(String str, byte b, byte b2) {
        ArrayList arrayList;
        JSONArray jSONArray;
        Integer num;
        char c;
        w81 a2;
        String str2 = str;
        byte b3 = b;
        byte b4 = b2;
        SdkDatabase a3 = SdkDatabase.e.a();
        if (a3 == null || (a2 = a3.a()) == null) {
            arrayList = null;
        } else {
            rh b5 = rh.b("select `DeviceFile`.`id` AS `id`, `DeviceFile`.`deviceMacAddress` AS `deviceMacAddress`, `DeviceFile`.`fileType` AS `fileType`, `DeviceFile`.`fileIndex` AS `fileIndex`, `DeviceFile`.`rawData` AS `rawData`, `DeviceFile`.`fileLength` AS `fileLength`, `DeviceFile`.`fileCrc` AS `fileCrc`, `DeviceFile`.`createdTimeStamp` AS `createdTimeStamp`, `DeviceFile`.`isCompleted` AS `isCompleted` from DeviceFile where deviceMacAddress = ? and fileType = ? and fileIndex = ? and isCompleted = 0", 3);
            if (str2 == null) {
                b5.a(1);
            } else {
                b5.a(1, str2);
            }
            b5.a(2, (long) b3);
            b5.a(3, (long) b4);
            a2.a.assertNotSuspendingTransaction();
            Cursor a4 = bi.a(a2.a, b5, false, (CancellationSignal) null);
            try {
                int b6 = ai.b(a4, "id");
                int b7 = ai.b(a4, "deviceMacAddress");
                int b8 = ai.b(a4, "fileType");
                int b9 = ai.b(a4, "fileIndex");
                int b10 = ai.b(a4, "rawData");
                int b11 = ai.b(a4, "fileLength");
                int b12 = ai.b(a4, "fileCrc");
                int b13 = ai.b(a4, "createdTimeStamp");
                int b14 = ai.b(a4, "isCompleted");
                arrayList = new ArrayList(a4.getCount());
                while (a4.moveToNext()) {
                    int i = b7;
                    int i2 = b8;
                    ie1 ie1 = new ie1(a4.getString(b7), (byte) a4.getShort(b8), (byte) a4.getShort(b9), a4.getBlob(b10), a4.getLong(b11), a4.getLong(b12), a4.getLong(b13), a4.getInt(b14) != 0);
                    ie1.a = a4.getInt(b6);
                    arrayList.add(ie1);
                    b7 = i;
                    b8 = i2;
                }
            } finally {
                a4.close();
                b5.c();
            }
        }
        boolean z = arrayList != null;
        String str3 = z ? "" : "Database instance is null";
        JSONObject a5 = cw0.a(new g01(b3, b4).a(), bm0.IS_COMPLETED, (Object) false);
        bm0 bm0 = bm0.FILES;
        if (arrayList != null) {
            Object[] array = arrayList.toArray(new ie1[0]);
            if (array != null) {
                jSONArray = cw0.a((p40[]) (ie1[]) array);
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            jSONArray = new JSONArray();
        }
        a(xx0.QUERY, z, str, cw0.a(a5, bm0, (Object) jSONArray), str3);
        oa1 oa1 = oa1.a;
        Object[] objArr = new Object[4];
        objArr[0] = str2;
        objArr[1] = Byte.valueOf(b);
        objArr[2] = Byte.valueOf(b2);
        if (arrayList != null) {
            num = Integer.valueOf(arrayList.size());
            c = 3;
        } else {
            c = 3;
            num = null;
        }
        objArr[c] = num;
        return arrayList != null ? arrayList : qd6.a();
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final int a(String str, byte b, byte b2) {
        Integer num;
        String str2;
        boolean z;
        w81 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        if (a3 == null || (a2 = a3.a()) == null) {
            num = null;
        } else {
            a2.a.assertNotSuspendingTransaction();
            mi acquire = a2.c.acquire();
            if (str == null) {
                acquire.a(1);
            } else {
                acquire.a(1, str);
            }
            acquire.a(2, (long) b);
            acquire.a(3, (long) b2);
            a2.a.beginTransaction();
            try {
                int s = acquire.s();
                a2.a.setTransactionSuccessful();
                a2.a.endTransaction();
                a2.c.release(acquire);
                num = Integer.valueOf(s);
            } catch (Throwable th) {
                a2.a.endTransaction();
                a2.c.release(acquire);
                throw th;
            }
        }
        if (num == null) {
            str2 = "Database instance is null";
            z = false;
        } else {
            str2 = "";
            z = true;
        }
        a(xx0.DELETE, z, str, cw0.a(cw0.a(new g01(b, b2).a(), bm0.IS_COMPLETED, (Object) false), bm0.NUMBER_OF_DELETED_ROW, (Object) num != null ? num : -1), str2);
        oa1 oa1 = oa1.a;
        Object[] objArr = {str, Byte.valueOf(b), Byte.valueOf(b2), num};
        if (num != null) {
            return num.intValue();
        }
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
        r0 = r0.a();
     */
    @DexIgnore
    public final int a(String str) {
        String str2;
        boolean z;
        w81 a2;
        SdkDatabase a3 = SdkDatabase.e.a();
        Integer valueOf = (a3 == null || a2 == null) ? null : Integer.valueOf(a2.a(str));
        if (valueOf == null) {
            str2 = "Database instance is null";
            z = false;
        } else {
            str2 = "";
            z = true;
        }
        a(xx0.DELETE_ALL, z, str, cw0.a(new JSONObject(), bm0.NUMBER_OF_DELETED_ROW, (Object) valueOf != null ? valueOf : -1), str2);
        oa1 oa1 = oa1.a;
        Object[] objArr = {str, valueOf};
        if (valueOf != null) {
            return valueOf.intValue();
        }
        return 0;
    }

    @DexIgnore
    public final void a(xx0 xx0, boolean z, String str, JSONObject jSONObject, String str2) {
        Object obj;
        JSONObject jSONObject2;
        qs0 qs0 = qs0.h;
        String a2 = cw0.a((Enum<?>) xx0);
        og0 og0 = og0.DATABASE;
        bm0 bm0 = bm0.MESSAGE;
        if (!xj6.a(str2)) {
            jSONObject2 = jSONObject;
            obj = str2;
        } else {
            obj = JSONObject.NULL;
            jSONObject2 = jSONObject;
        }
        qs0.a(new nn0(a2, og0, str, "", "", z, (String) null, (r40) null, (bw0) null, cw0.a(jSONObject2, bm0, obj), 448));
    }
}
