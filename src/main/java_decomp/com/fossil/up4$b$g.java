package com.fossil;

import android.content.Context;
import android.view.View;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.debug.DebugActivity;
import java.lang.ref.WeakReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class up4$b$g implements View.OnClickListener {
    @DexIgnore
    public /* final */ /* synthetic */ ShakeFeedbackService.b a;

    @DexIgnore
    public up4$b$g(ShakeFeedbackService.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    public final void onClick(View view) {
        DebugActivity.a aVar = DebugActivity.P;
        WeakReference b = this.a.a.a;
        if (b != null) {
            Object obj = b.get();
            if (obj != null) {
                wg6.a(obj, "contextWeakReference!!.get()!!");
                aVar.a((Context) obj);
                qg3 a2 = this.a.a.c;
                if (a2 != null) {
                    a2.dismiss();
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
