package com.fossil;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class am {
    @DexIgnore
    public UUID a;
    @DexIgnore
    public a b;
    @DexIgnore
    public pl c;
    @DexIgnore
    public Set<String> d;
    @DexIgnore
    public pl e;
    @DexIgnore
    public int f;

    @DexIgnore
    public enum a {
        ENQUEUED,
        RUNNING,
        SUCCEEDED,
        FAILED,
        BLOCKED,
        CANCELLED;

        @DexIgnore
        public boolean isFinished() {
            return this == SUCCEEDED || this == FAILED || this == CANCELLED;
        }
    }

    @DexIgnore
    public am(UUID uuid, a aVar, pl plVar, List<String> list, pl plVar2, int i) {
        this.a = uuid;
        this.b = aVar;
        this.c = plVar;
        this.d = new HashSet(list);
        this.e = plVar2;
        this.f = i;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || am.class != obj.getClass()) {
            return false;
        }
        am amVar = (am) obj;
        if (this.f == amVar.f && this.a.equals(amVar.a) && this.b == amVar.b && this.c.equals(amVar.c) && this.d.equals(amVar.d)) {
            return this.e.equals(amVar.e);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return (((((((((this.a.hashCode() * 31) + this.b.hashCode()) * 31) + this.c.hashCode()) * 31) + this.d.hashCode()) * 31) + this.e.hashCode()) * 31) + this.f;
    }

    @DexIgnore
    public String toString() {
        return "WorkInfo{mId='" + this.a + '\'' + ", mState=" + this.b + ", mOutputData=" + this.c + ", mTags=" + this.d + ", mProgress=" + this.e + '}';
    }
}
