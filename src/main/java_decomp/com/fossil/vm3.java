package com.fossil;

import com.fossil.fk3;
import com.fossil.wm3;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vm3 {
    @DexIgnore
    public boolean a;
    @DexIgnore
    public int b; // = -1;
    @DexIgnore
    public int c; // = -1;
    @DexIgnore
    public wm3.p d;
    @DexIgnore
    public wm3.p e;
    @DexIgnore
    public ak3<Object> f;

    @DexIgnore
    public vm3 a(ak3<Object> ak3) {
        jk3.b(this.f == null, "key equivalence was already set to %s", (Object) this.f);
        jk3.a(ak3);
        this.f = ak3;
        this.a = true;
        return this;
    }

    @DexIgnore
    public vm3 b(int i) {
        boolean z = true;
        jk3.b(this.b == -1, "initial capacity was already set to %s", this.b);
        if (i < 0) {
            z = false;
        }
        jk3.a(z);
        this.b = i;
        return this;
    }

    @DexIgnore
    public ak3<Object> c() {
        return (ak3) fk3.a(this.f, d().defaultEquivalence());
    }

    @DexIgnore
    public wm3.p d() {
        return (wm3.p) fk3.a(this.d, wm3.p.STRONG);
    }

    @DexIgnore
    public wm3.p e() {
        return (wm3.p) fk3.a(this.e, wm3.p.STRONG);
    }

    @DexIgnore
    public <K, V> ConcurrentMap<K, V> f() {
        if (!this.a) {
            return new ConcurrentHashMap(b(), 0.75f, a());
        }
        return wm3.create(this);
    }

    @DexIgnore
    public vm3 g() {
        a(wm3.p.WEAK);
        return this;
    }

    @DexIgnore
    public String toString() {
        fk3.b a2 = fk3.a(this);
        int i = this.b;
        if (i != -1) {
            a2.a("initialCapacity", i);
        }
        int i2 = this.c;
        if (i2 != -1) {
            a2.a("concurrencyLevel", i2);
        }
        wm3.p pVar = this.d;
        if (pVar != null) {
            a2.a("keyStrength", (Object) zj3.a(pVar.toString()));
        }
        wm3.p pVar2 = this.e;
        if (pVar2 != null) {
            a2.a("valueStrength", (Object) zj3.a(pVar2.toString()));
        }
        if (this.f != null) {
            a2.b("keyEquivalence");
        }
        return a2.toString();
    }

    @DexIgnore
    public vm3 a(int i) {
        boolean z = true;
        jk3.b(this.c == -1, "concurrency level was already set to %s", this.c);
        if (i <= 0) {
            z = false;
        }
        jk3.a(z);
        this.c = i;
        return this;
    }

    @DexIgnore
    public int b() {
        int i = this.b;
        if (i == -1) {
            return 16;
        }
        return i;
    }

    @DexIgnore
    public vm3 b(wm3.p pVar) {
        jk3.b(this.e == null, "Value strength was already set to %s", (Object) this.e);
        jk3.a(pVar);
        this.e = pVar;
        if (pVar != wm3.p.STRONG) {
            this.a = true;
        }
        return this;
    }

    @DexIgnore
    public int a() {
        int i = this.c;
        if (i == -1) {
            return 4;
        }
        return i;
    }

    @DexIgnore
    public vm3 a(wm3.p pVar) {
        jk3.b(this.d == null, "Key strength was already set to %s", (Object) this.d);
        jk3.a(pVar);
        this.d = pVar;
        if (pVar != wm3.p.STRONG) {
            this.a = true;
        }
        return this;
    }
}
