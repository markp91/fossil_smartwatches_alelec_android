package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sc3 {
    @DexIgnore
    public static /* final */ Executor a; // = new a();
    @DexIgnore
    public static /* final */ Executor b; // = new nd3();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Executor {
        @DexIgnore
        public /* final */ Handler a; // = new Handler(Looper.getMainLooper());

        @DexIgnore
        public final void execute(Runnable runnable) {
            this.a.post(runnable);
        }
    }
}
