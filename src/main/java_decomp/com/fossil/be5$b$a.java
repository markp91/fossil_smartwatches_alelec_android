package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.goaltracking.GoalTrackingSummary;
import com.portfolio.platform.uirenew.home.dashboard.goaltracking.DashboardGoalTrackingPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class be5$b$a<T> implements ld<cf<GoalTrackingSummary>> {
    @DexIgnore
    public /* final */ /* synthetic */ DashboardGoalTrackingPresenter.b a;

    @DexIgnore
    public be5$b$a(DashboardGoalTrackingPresenter.b bVar) {
        this.a = bVar;
    }

    @DexIgnore
    /* renamed from: a */
    public final void onChanged(cf<GoalTrackingSummary> cfVar) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        StringBuilder sb = new StringBuilder();
        sb.append("getSummariesPaging observer size=");
        sb.append(cfVar != null ? Integer.valueOf(cfVar.size()) : null);
        local.d("DashboardGoalTrackingPresenter", sb.toString());
        if (cfVar != null) {
            this.a.this$0.k().a(cfVar);
        }
    }
}
