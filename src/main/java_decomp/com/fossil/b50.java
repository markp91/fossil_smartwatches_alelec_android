package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.k70;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class b50 extends a50 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ byte c;
    @DexIgnore
    public /* final */ Set<k70> d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<b50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public final b50 a(byte[] bArr) throws IllegalArgumentException {
            if (bArr.length == 3) {
                boolean z = false;
                byte b = bArr[0];
                Set<k70> a = k70.d.a(bArr[0]);
                if (((byte) (((byte) 128) & bArr[1])) != 0) {
                    z = true;
                }
                byte b2 = bArr[1];
                byte b3 = (byte) (bArr[1] & 63);
                byte b4 = (byte) (bArr[2] & 31);
                if (z) {
                    return new i50(b4, b3, a);
                }
                if (a.isEmpty() || a.size() == k70.values().length) {
                    return new e50(b4, b3, (k70) null);
                }
                return new e50(b4, b3, (k70) yd6.d(a));
            }
            throw new IllegalArgumentException(ze0.a(ze0.b("Size("), bArr.length, " not equal to 3."));
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new b50[i];
        }

        @DexIgnore
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: com.fossil.k70} */
        /* JADX WARNING: Multi-variable type inference failed */
        public b50 createFromParcel(Parcel parcel) {
            parcel.readInt();
            byte readByte = parcel.readByte();
            byte readByte2 = parcel.readByte();
            k70.a aVar = k70.d;
            String[] createStringArray = parcel.createStringArray();
            k70 k70 = null;
            if (createStringArray != null) {
                wg6.a(createStringArray, "parcel.createStringArray()!!");
                LinkedHashSet linkedHashSet = new LinkedHashSet(nd6.g(aVar.a(createStringArray)));
                boolean z = false;
                boolean z2 = parcel.readInt() != 0;
                boolean z3 = parcel.readInt() != 0;
                if (parcel.readInt() != 0) {
                    z = true;
                }
                if (z2) {
                    return new i50(readByte, readByte2, linkedHashSet, z3, z);
                }
                if (!linkedHashSet.isEmpty() && linkedHashSet.size() != k70.values().length) {
                    k70 = yd6.d(linkedHashSet);
                }
                return new e50(readByte, readByte2, k70);
            }
            wg6.a();
            throw null;
        }
    }

    @DexIgnore
    public b50(byte b2, byte b3, Set<? extends k70> set, boolean z, boolean z2, boolean z3) throws IllegalArgumentException {
        super(qk0.FIRE_TIME);
        this.b = b2;
        this.c = b3;
        this.d = set;
        this.e = z;
        this.f = z2;
        this.g = z3;
        byte b4 = this.b;
        boolean z4 = true;
        if (b4 >= 0 && 23 >= b4) {
            byte b5 = this.c;
            if (!((b5 < 0 || 59 < b5) ? false : z4)) {
                throw new IllegalArgumentException(ze0.a(ze0.b("minute("), this.c, ") is out of range ", "[0, 59]."));
            }
            return;
        }
        throw new IllegalArgumentException(ze0.a(ze0.b("hour("), this.b, ") is out of range ", "[0, 23]."));
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject a2 = cw0.a(cw0.a(super.a(), bm0.HOUR, (Object) Byte.valueOf(this.b)), bm0.MINUTE, (Object) Byte.valueOf(this.c));
        bm0 bm0 = bm0.DAYS;
        Set<k70> set = this.d;
        JSONArray jSONArray = new JSONArray();
        for (k70 a3 : set) {
            jSONArray.put(cw0.a((Enum<?>) a3));
        }
        return cw0.a(cw0.a(cw0.a(cw0.a(a2, bm0, (Object) jSONArray), bm0.REPEAT, (Object) Boolean.valueOf(this.e)), bm0.ENABLE, (Object) Boolean.valueOf(this.g)), bm0.ALLOW_SNOOZE, (Object) Boolean.valueOf(this.f));
    }

    @DexIgnore
    public byte[] c() {
        byte[] bArr = new byte[3];
        byte b2 = this.e ? (byte) 128 : (byte) 0;
        byte b3 = this.f ? (byte) 64 : (byte) 0;
        bArr[0] = (byte) ((this.g ? (byte) 128 : (byte) 0) | cw0.a((Set<? extends k70>) this.d));
        bArr[1] = (byte) (((byte) (b2 | b3)) | this.c);
        bArr[2] = this.b;
        return bArr;
    }

    @DexIgnore
    public final Set<k70> d() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (!super.equals(obj)) {
            return true;
        }
        if (obj != null) {
            b50 b50 = (b50) obj;
            return this.b == b50.b && this.c == b50.c && cw0.a((Set<? extends k70>) this.d) == cw0.a((Set<? extends k70>) b50.d) && this.e == b50.e && this.f == b50.f && this.g == b50.g;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.alarm.FireTime");
    }

    @DexIgnore
    public final byte getHour() {
        return this.b;
    }

    @DexIgnore
    public final byte getMinute() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = Boolean.valueOf(this.e).hashCode();
        int hashCode3 = Boolean.valueOf(this.f).hashCode();
        return Boolean.valueOf(this.g).hashCode() + ((hashCode3 + ((hashCode2 + ((hashCode + (((((super.hashCode() * 31) + this.b) * 31) + this.c) * 31)) * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeByte(this.c);
        }
        if (parcel != null) {
            Object[] array = this.d.toArray(new k70[0]);
            if (array != null) {
                k70[] k70Arr = (k70[]) array;
                ArrayList arrayList = new ArrayList(k70Arr.length);
                for (k70 name : k70Arr) {
                    arrayList.add(name.name());
                }
                Object[] array2 = arrayList.toArray(new String[0]);
                if (array2 != null) {
                    parcel.writeStringArray((String[]) array2);
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
                }
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        if (parcel != null) {
            parcel.writeInt(this.e ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.f ? 1 : 0);
        }
        if (parcel != null) {
            parcel.writeInt(this.g ? 1 : 0);
        }
    }
}
