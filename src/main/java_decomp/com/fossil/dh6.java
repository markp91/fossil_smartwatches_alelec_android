package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dh6 extends ch6 {
    @DexIgnore
    public /* final */ String name;
    @DexIgnore
    public /* final */ hi6 owner;
    @DexIgnore
    public /* final */ String signature;

    @DexIgnore
    public dh6(hi6 hi6, String str, String str2) {
        this.owner = hi6;
        this.name = str;
        this.signature = str2;
    }

    @DexIgnore
    public Object get(Object obj) {
        return getGetter().call(obj);
    }

    @DexIgnore
    public String getName() {
        return this.name;
    }

    @DexIgnore
    public hi6 getOwner() {
        return this.owner;
    }

    @DexIgnore
    public String getSignature() {
        return this.signature;
    }
}
