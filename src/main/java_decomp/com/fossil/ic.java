package com.fossil;

import android.graphics.Rect;
import android.os.Build;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.core.app.SharedElementCallback;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.fossil.hc;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ic {
    @DexIgnore
    public static /* final */ int[] a; // = {0, 3, 0, 1, 5, 4, 7, 6, 9, 8, 10};
    @DexIgnore
    public static /* final */ kc b; // = (Build.VERSION.SDK_INT >= 21 ? new jc() : null);
    @DexIgnore
    public static /* final */ kc c; // = a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ g a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ z7 c;

        @DexIgnore
        public a(g gVar, Fragment fragment, z7 z7Var) {
            this.a = gVar;
            this.b = fragment;
            this.c = z7Var;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList a;

        @DexIgnore
        public b(ArrayList arrayList) {
            this.a = arrayList;
        }

        @DexIgnore
        public void run() {
            ic.a((ArrayList<View>) this.a, 4);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ g a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ z7 c;

        @DexIgnore
        public c(g gVar, Fragment fragment, z7 z7Var) {
            this.a = gVar;
            this.b = fragment;
            this.c = z7Var;
        }

        @DexIgnore
        public void run() {
            this.a.a(this.b, this.c);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class d implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Object a;
        @DexIgnore
        public /* final */ /* synthetic */ kc b;
        @DexIgnore
        public /* final */ /* synthetic */ View c;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList f;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList g;
        @DexIgnore
        public /* final */ /* synthetic */ Object h;

        @DexIgnore
        public d(Object obj, kc kcVar, View view, Fragment fragment, ArrayList arrayList, ArrayList arrayList2, ArrayList arrayList3, Object obj2) {
            this.a = obj;
            this.b = kcVar;
            this.c = view;
            this.d = fragment;
            this.e = arrayList;
            this.f = arrayList2;
            this.g = arrayList3;
            this.h = obj2;
        }

        @DexIgnore
        public void run() {
            Object obj = this.a;
            if (obj != null) {
                this.b.b(obj, this.c);
                this.f.addAll(ic.a(this.b, this.a, this.d, (ArrayList<View>) this.e, this.c));
            }
            if (this.g != null) {
                if (this.h != null) {
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(this.c);
                    this.b.a(this.h, (ArrayList<View>) this.g, (ArrayList<View>) arrayList);
                }
                this.g.clear();
                this.g.add(this.c);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class e implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ Fragment a;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment b;
        @DexIgnore
        public /* final */ /* synthetic */ boolean c;
        @DexIgnore
        public /* final */ /* synthetic */ p4 d;
        @DexIgnore
        public /* final */ /* synthetic */ View e;
        @DexIgnore
        public /* final */ /* synthetic */ kc f;
        @DexIgnore
        public /* final */ /* synthetic */ Rect g;

        @DexIgnore
        public e(Fragment fragment, Fragment fragment2, boolean z, p4 p4Var, View view, kc kcVar, Rect rect) {
            this.a = fragment;
            this.b = fragment2;
            this.c = z;
            this.d = p4Var;
            this.e = view;
            this.f = kcVar;
            this.g = rect;
        }

        @DexIgnore
        public void run() {
            ic.a(this.a, this.b, this.c, (p4<String, View>) this.d, false);
            View view = this.e;
            if (view != null) {
                this.f.a(view, this.g);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class f implements Runnable {
        @DexIgnore
        public /* final */ /* synthetic */ kc a;
        @DexIgnore
        public /* final */ /* synthetic */ p4 b;
        @DexIgnore
        public /* final */ /* synthetic */ Object c;
        @DexIgnore
        public /* final */ /* synthetic */ h d;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList e;
        @DexIgnore
        public /* final */ /* synthetic */ View f;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment g;
        @DexIgnore
        public /* final */ /* synthetic */ Fragment h;
        @DexIgnore
        public /* final */ /* synthetic */ boolean i;
        @DexIgnore
        public /* final */ /* synthetic */ ArrayList j;
        @DexIgnore
        public /* final */ /* synthetic */ Object o;
        @DexIgnore
        public /* final */ /* synthetic */ Rect p;

        @DexIgnore
        public f(kc kcVar, p4 p4Var, Object obj, h hVar, ArrayList arrayList, View view, Fragment fragment, Fragment fragment2, boolean z, ArrayList arrayList2, Object obj2, Rect rect) {
            this.a = kcVar;
            this.b = p4Var;
            this.c = obj;
            this.d = hVar;
            this.e = arrayList;
            this.f = view;
            this.g = fragment;
            this.h = fragment2;
            this.i = z;
            this.j = arrayList2;
            this.o = obj2;
            this.p = rect;
        }

        @DexIgnore
        public void run() {
            p4<String, View> a2 = ic.a(this.a, (p4<String, String>) this.b, this.c, this.d);
            if (a2 != null) {
                this.e.addAll(a2.values());
                this.e.add(this.f);
            }
            ic.a(this.g, this.h, this.i, a2, false);
            Object obj = this.c;
            if (obj != null) {
                this.a.b(obj, (ArrayList<View>) this.j, (ArrayList<View>) this.e);
                View a3 = ic.a(a2, this.d, this.o, this.i);
                if (a3 != null) {
                    this.a.a(a3, this.p);
                }
            }
        }
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        void a(Fragment fragment, z7 z7Var);

        @DexIgnore
        void b(Fragment fragment, z7 z7Var);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class h {
        @DexIgnore
        public Fragment a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public tb c;
        @DexIgnore
        public Fragment d;
        @DexIgnore
        public boolean e;
        @DexIgnore
        public tb f;
    }

    @DexIgnore
    public static kc a() {
        try {
            return (kc) Class.forName("com.fossil.zi").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        r12 = r4.a;
     */
    @DexIgnore
    public static void b(FragmentManager fragmentManager, int i, h hVar, View view, p4<String, String> p4Var, g gVar) {
        Fragment fragment;
        Fragment fragment2;
        kc a2;
        Object obj;
        FragmentManager fragmentManager2 = fragmentManager;
        h hVar2 = hVar;
        View view2 = view;
        g gVar2 = gVar;
        ViewGroup viewGroup = fragmentManager2.p.a() ? (ViewGroup) fragmentManager2.p.a(i) : null;
        if (viewGroup != null && (a2 = a(fragment2, fragment)) != null) {
            boolean z = hVar2.b;
            boolean z2 = hVar2.e;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            Object a3 = a(a2, fragment, z);
            Object b2 = b(a2, (fragment2 = hVar2.d), z2);
            Object obj2 = a3;
            ViewGroup viewGroup2 = viewGroup;
            ArrayList arrayList3 = arrayList2;
            ArrayList arrayList4 = arrayList;
            Object b3 = b(a2, viewGroup, view, p4Var, hVar, arrayList2, arrayList, obj2, b2);
            Object obj3 = obj2;
            if (obj3 == null && b3 == null) {
                obj = b2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = b2;
            }
            ArrayList<View> a4 = a(a2, obj, fragment2, (ArrayList<View>) arrayList3, view2);
            ArrayList<View> a5 = a(a2, obj3, fragment, (ArrayList<View>) arrayList4, view2);
            a(a5, 4);
            Fragment fragment3 = fragment;
            ArrayList<View> arrayList5 = a4;
            Object a6 = a(a2, obj3, obj, b3, fragment3, z);
            if (!(fragment2 == null || arrayList5 == null || (arrayList5.size() <= 0 && arrayList3.size() <= 0))) {
                z7 z7Var = new z7();
                g gVar3 = gVar;
                gVar3.b(fragment2, z7Var);
                a2.a(fragment2, a6, z7Var, new a(gVar3, fragment2, z7Var));
            }
            if (a6 != null) {
                a(a2, obj, fragment2, arrayList5);
                ArrayList<String> a7 = a2.a((ArrayList<View>) arrayList4);
                a2.a(a6, obj3, a5, obj, arrayList5, b3, arrayList4);
                ViewGroup viewGroup3 = viewGroup2;
                a2.a(viewGroup3, a6);
                a2.a(viewGroup3, arrayList3, arrayList4, a7, p4Var);
                a(a5, 0);
                a2.b(b3, (ArrayList<View>) arrayList3, (ArrayList<View>) arrayList4);
            }
        }
    }

    @DexIgnore
    public static void a(FragmentManager fragmentManager, ArrayList<tb> arrayList, ArrayList<Boolean> arrayList2, int i, int i2, boolean z, g gVar) {
        FragmentManager fragmentManager2 = fragmentManager;
        ArrayList<tb> arrayList3 = arrayList;
        ArrayList<Boolean> arrayList4 = arrayList2;
        int i3 = i2;
        boolean z2 = z;
        if (fragmentManager2.n >= 1) {
            SparseArray sparseArray = new SparseArray();
            for (int i4 = i; i4 < i3; i4++) {
                tb tbVar = arrayList3.get(i4);
                if (arrayList4.get(i4).booleanValue()) {
                    b(tbVar, (SparseArray<h>) sparseArray, z2);
                } else {
                    a(tbVar, (SparseArray<h>) sparseArray, z2);
                }
            }
            if (sparseArray.size() != 0) {
                View view = new View(fragmentManager2.o.c());
                int size = sparseArray.size();
                for (int i5 = 0; i5 < size; i5++) {
                    int keyAt = sparseArray.keyAt(i5);
                    p4<String, String> a2 = a(keyAt, arrayList3, arrayList4, i, i3);
                    h hVar = (h) sparseArray.valueAt(i5);
                    if (z2) {
                        b(fragmentManager, keyAt, hVar, view, a2, gVar);
                    } else {
                        a(fragmentManager, keyAt, hVar, view, a2, gVar);
                    }
                }
            }
        }
    }

    @DexIgnore
    public static p4<String, String> a(int i, ArrayList<tb> arrayList, ArrayList<Boolean> arrayList2, int i2, int i3) {
        ArrayList<String> arrayList3;
        ArrayList<String> arrayList4;
        p4<String, String> p4Var = new p4<>();
        for (int i4 = i3 - 1; i4 >= i2; i4--) {
            tb tbVar = arrayList.get(i4);
            if (tbVar.b(i)) {
                boolean booleanValue = arrayList2.get(i4).booleanValue();
                ArrayList<String> arrayList5 = tbVar.n;
                if (arrayList5 != null) {
                    int size = arrayList5.size();
                    if (booleanValue) {
                        arrayList3 = tbVar.n;
                        arrayList4 = tbVar.o;
                    } else {
                        ArrayList<String> arrayList6 = tbVar.n;
                        arrayList3 = tbVar.o;
                        arrayList4 = arrayList6;
                    }
                    for (int i5 = 0; i5 < size; i5++) {
                        String str = arrayList4.get(i5);
                        String str2 = arrayList3.get(i5);
                        String remove = p4Var.remove(str2);
                        if (remove != null) {
                            p4Var.put(str, remove);
                        } else {
                            p4Var.put(str, str2);
                        }
                    }
                }
            }
        }
        return p4Var;
    }

    @DexIgnore
    public static Object b(kc kcVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReturnTransition();
        } else {
            obj = fragment.getExitTransition();
        }
        return kcVar.b(obj);
    }

    @DexIgnore
    public static Object b(kc kcVar, ViewGroup viewGroup, View view, p4<String, String> p4Var, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        Object obj3;
        Object obj4;
        Rect rect;
        View view2;
        kc kcVar2 = kcVar;
        View view3 = view;
        p4<String, String> p4Var2 = p4Var;
        h hVar2 = hVar;
        ArrayList<View> arrayList3 = arrayList;
        ArrayList<View> arrayList4 = arrayList2;
        Object obj5 = obj;
        Fragment fragment = hVar2.a;
        Fragment fragment2 = hVar2.d;
        if (fragment != null) {
            fragment.requireView().setVisibility(0);
        }
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar2.b;
        if (p4Var.isEmpty()) {
            obj3 = null;
        } else {
            obj3 = a(kcVar, fragment, fragment2, z);
        }
        p4<String, View> b2 = b(kcVar, p4Var2, obj3, hVar2);
        p4<String, View> a2 = a(kcVar, p4Var2, obj3, hVar2);
        if (p4Var.isEmpty()) {
            if (b2 != null) {
                b2.clear();
            }
            if (a2 != null) {
                a2.clear();
            }
            obj4 = null;
        } else {
            a(arrayList3, b2, (Collection<String>) p4Var.keySet());
            a(arrayList4, a2, p4Var.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            arrayList4.add(view3);
            kcVar.b(obj4, view3, arrayList3);
            a(kcVar, obj4, obj2, b2, hVar2.e, hVar2.f);
            Rect rect2 = new Rect();
            View a3 = a(a2, hVar2, obj5, z);
            if (a3 != null) {
                kcVar.a(obj5, rect2);
            }
            rect = rect2;
            view2 = a3;
        } else {
            view2 = null;
            rect = null;
        }
        t9.a(viewGroup, new e(fragment, fragment2, z, a2, view2, kcVar, rect));
        return obj4;
    }

    @DexIgnore
    public static void a(kc kcVar, Object obj, Fragment fragment, ArrayList<View> arrayList) {
        if (fragment != null && obj != null && fragment.mAdded && fragment.mHidden && fragment.mHiddenChanged) {
            fragment.setHideReplaced(true);
            kcVar.a(obj, fragment.getView(), arrayList);
            t9.a(fragment.mContainer, new b(arrayList));
        }
    }

    @DexIgnore
    public static void a(FragmentManager fragmentManager, int i, h hVar, View view, p4<String, String> p4Var, g gVar) {
        Fragment fragment;
        Fragment fragment2;
        kc a2;
        Object obj;
        FragmentManager fragmentManager2 = fragmentManager;
        h hVar2 = hVar;
        View view2 = view;
        p4<String, String> p4Var2 = p4Var;
        g gVar2 = gVar;
        ViewGroup viewGroup = fragmentManager2.p.a() ? (ViewGroup) fragmentManager2.p.a(i) : null;
        if (viewGroup != null && (a2 = a(fragment2, fragment)) != null) {
            boolean z = hVar2.b;
            boolean z2 = hVar2.e;
            Object a3 = a(a2, (fragment = hVar2.a), z);
            Object b2 = b(a2, (fragment2 = hVar2.d), z2);
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = arrayList;
            Object obj2 = b2;
            Object obj3 = a3;
            kc kcVar = a2;
            Fragment fragment3 = fragment2;
            Object a4 = a(a2, viewGroup, view, p4Var, hVar, (ArrayList<View>) arrayList3, (ArrayList<View>) arrayList2, obj3, obj2);
            Object obj4 = obj3;
            if (obj4 == null && a4 == null) {
                obj = obj2;
                if (obj == null) {
                    return;
                }
            } else {
                obj = obj2;
            }
            ArrayList arrayList4 = arrayList3;
            ArrayList<View> a5 = a(kcVar, obj, fragment3, (ArrayList<View>) arrayList4, view2);
            Object obj5 = (a5 == null || a5.isEmpty()) ? null : obj;
            kcVar.a(obj4, view2);
            Object a6 = a(kcVar, obj4, obj5, a4, fragment, hVar2.b);
            if (!(fragment3 == null || a5 == null || (a5.size() <= 0 && arrayList4.size() <= 0))) {
                z7 z7Var = new z7();
                gVar2.b(fragment3, z7Var);
                kcVar.a(fragment3, a6, z7Var, new c(gVar2, fragment3, z7Var));
            }
            if (a6 != null) {
                ArrayList arrayList5 = new ArrayList();
                kc kcVar2 = kcVar;
                kcVar2.a(a6, obj4, arrayList5, obj5, a5, a4, arrayList2);
                a(kcVar2, viewGroup, fragment, view, (ArrayList<View>) arrayList2, obj4, (ArrayList<View>) arrayList5, obj5, a5);
                ArrayList arrayList6 = arrayList2;
                p4<String, String> p4Var3 = p4Var;
                kcVar.a((View) viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) p4Var3);
                kcVar.a(viewGroup, a6);
                kcVar.a(viewGroup, (ArrayList<View>) arrayList6, (Map<String, String>) p4Var3);
            }
        }
    }

    @DexIgnore
    public static p4<String, View> b(kc kcVar, p4<String, String> p4Var, Object obj, h hVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        if (p4Var.isEmpty() || obj == null) {
            p4Var.clear();
            return null;
        }
        Fragment fragment = hVar.d;
        p4<String, View> p4Var2 = new p4<>();
        kcVar.a((Map<String, View>) p4Var2, fragment.requireView());
        tb tbVar = hVar.f;
        if (hVar.e) {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = tbVar.o;
        } else {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = tbVar.n;
        }
        if (arrayList != null) {
            p4Var2.a(arrayList);
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) p4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view = p4Var2.get(str);
                if (view == null) {
                    p4Var.remove(str);
                } else if (!str.equals(x9.v(view))) {
                    p4Var.put(x9.v(view), p4Var.remove(str));
                }
            }
        } else {
            p4Var.a(p4Var2.keySet());
        }
        return p4Var2;
    }

    @DexIgnore
    public static void a(kc kcVar, ViewGroup viewGroup, Fragment fragment, View view, ArrayList<View> arrayList, Object obj, ArrayList<View> arrayList2, Object obj2, ArrayList<View> arrayList3) {
        ViewGroup viewGroup2 = viewGroup;
        t9.a(viewGroup, new d(obj, kcVar, view, fragment, arrayList, arrayList2, arrayList3, obj2));
    }

    @DexIgnore
    public static kc a(Fragment fragment, Fragment fragment2) {
        ArrayList arrayList = new ArrayList();
        if (fragment != null) {
            Object exitTransition = fragment.getExitTransition();
            if (exitTransition != null) {
                arrayList.add(exitTransition);
            }
            Object returnTransition = fragment.getReturnTransition();
            if (returnTransition != null) {
                arrayList.add(returnTransition);
            }
            Object sharedElementReturnTransition = fragment.getSharedElementReturnTransition();
            if (sharedElementReturnTransition != null) {
                arrayList.add(sharedElementReturnTransition);
            }
        }
        if (fragment2 != null) {
            Object enterTransition = fragment2.getEnterTransition();
            if (enterTransition != null) {
                arrayList.add(enterTransition);
            }
            Object reenterTransition = fragment2.getReenterTransition();
            if (reenterTransition != null) {
                arrayList.add(reenterTransition);
            }
            Object sharedElementEnterTransition = fragment2.getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {
                arrayList.add(sharedElementEnterTransition);
            }
        }
        if (arrayList.isEmpty()) {
            return null;
        }
        kc kcVar = b;
        if (kcVar != null && a(kcVar, (List<Object>) arrayList)) {
            return b;
        }
        kc kcVar2 = c;
        if (kcVar2 != null && a(kcVar2, (List<Object>) arrayList)) {
            return c;
        }
        if (b == null && c == null) {
            return null;
        }
        throw new IllegalArgumentException("Invalid Transition types");
    }

    @DexIgnore
    public static void b(tb tbVar, SparseArray<h> sparseArray, boolean z) {
        if (tbVar.r.p.a()) {
            for (int size = tbVar.a.size() - 1; size >= 0; size--) {
                a(tbVar, tbVar.a.get(size), sparseArray, true, z);
            }
        }
    }

    @DexIgnore
    public static boolean a(kc kcVar, List<Object> list) {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            if (!kcVar.a(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    @DexIgnore
    public static Object a(kc kcVar, Fragment fragment, Fragment fragment2, boolean z) {
        Object obj;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        if (z) {
            obj = fragment2.getSharedElementReturnTransition();
        } else {
            obj = fragment.getSharedElementEnterTransition();
        }
        return kcVar.c(kcVar.b(obj));
    }

    @DexIgnore
    public static Object a(kc kcVar, Fragment fragment, boolean z) {
        Object obj;
        if (fragment == null) {
            return null;
        }
        if (z) {
            obj = fragment.getReenterTransition();
        } else {
            obj = fragment.getEnterTransition();
        }
        return kcVar.b(obj);
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, p4<String, View> p4Var, Collection<String> collection) {
        for (int size = p4Var.size() - 1; size >= 0; size--) {
            View e2 = p4Var.e(size);
            if (collection.contains(x9.v(e2))) {
                arrayList.add(e2);
            }
        }
    }

    @DexIgnore
    public static Object a(kc kcVar, ViewGroup viewGroup, View view, p4<String, String> p4Var, h hVar, ArrayList<View> arrayList, ArrayList<View> arrayList2, Object obj, Object obj2) {
        p4<String, String> p4Var2;
        Object obj3;
        Object obj4;
        Rect rect;
        kc kcVar2 = kcVar;
        h hVar2 = hVar;
        ArrayList<View> arrayList3 = arrayList;
        Object obj5 = obj;
        Fragment fragment = hVar2.a;
        Fragment fragment2 = hVar2.d;
        if (fragment == null || fragment2 == null) {
            return null;
        }
        boolean z = hVar2.b;
        if (p4Var.isEmpty()) {
            p4Var2 = p4Var;
            obj3 = null;
        } else {
            obj3 = a(kcVar2, fragment, fragment2, z);
            p4Var2 = p4Var;
        }
        p4<String, View> b2 = b(kcVar2, p4Var2, obj3, hVar2);
        if (p4Var.isEmpty()) {
            obj4 = null;
        } else {
            arrayList3.addAll(b2.values());
            obj4 = obj3;
        }
        if (obj5 == null && obj2 == null && obj4 == null) {
            return null;
        }
        a(fragment, fragment2, z, b2, true);
        if (obj4 != null) {
            rect = new Rect();
            kcVar2.b(obj4, view, arrayList3);
            a(kcVar, obj4, obj2, b2, hVar2.e, hVar2.f);
            if (obj5 != null) {
                kcVar2.a(obj5, rect);
            }
        } else {
            rect = null;
        }
        f fVar = r0;
        f fVar2 = new f(kcVar, p4Var, obj4, hVar, arrayList2, view, fragment, fragment2, z, arrayList, obj, rect);
        t9.a(viewGroup, fVar);
        return obj4;
    }

    @DexIgnore
    public static p4<String, View> a(kc kcVar, p4<String, String> p4Var, Object obj, h hVar) {
        SharedElementCallback sharedElementCallback;
        ArrayList<String> arrayList;
        String a2;
        Fragment fragment = hVar.a;
        View view = fragment.getView();
        if (p4Var.isEmpty() || obj == null || view == null) {
            p4Var.clear();
            return null;
        }
        p4<String, View> p4Var2 = new p4<>();
        kcVar.a((Map<String, View>) p4Var2, view);
        tb tbVar = hVar.c;
        if (hVar.b) {
            sharedElementCallback = fragment.getExitTransitionCallback();
            arrayList = tbVar.n;
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
            arrayList = tbVar.o;
        }
        if (arrayList != null) {
            p4Var2.a(arrayList);
            p4Var2.a(p4Var.values());
        }
        if (sharedElementCallback != null) {
            sharedElementCallback.a((List<String>) arrayList, (Map<String, View>) p4Var2);
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                String str = arrayList.get(size);
                View view2 = p4Var2.get(str);
                if (view2 == null) {
                    String a3 = a(p4Var, str);
                    if (a3 != null) {
                        p4Var.remove(a3);
                    }
                } else if (!str.equals(x9.v(view2)) && (a2 = a(p4Var, str)) != null) {
                    p4Var.put(a2, x9.v(view2));
                }
            }
        } else {
            a(p4Var, p4Var2);
        }
        return p4Var2;
    }

    @DexIgnore
    public static String a(p4<String, String> p4Var, String str) {
        int size = p4Var.size();
        for (int i = 0; i < size; i++) {
            if (str.equals(p4Var.e(i))) {
                return p4Var.c(i);
            }
        }
        return null;
    }

    @DexIgnore
    public static View a(p4<String, View> p4Var, h hVar, Object obj, boolean z) {
        ArrayList<String> arrayList;
        String str;
        tb tbVar = hVar.c;
        if (obj == null || p4Var == null || (arrayList = tbVar.n) == null || arrayList.isEmpty()) {
            return null;
        }
        if (z) {
            str = tbVar.n.get(0);
        } else {
            str = tbVar.o.get(0);
        }
        return p4Var.get(str);
    }

    @DexIgnore
    public static void a(kc kcVar, Object obj, Object obj2, p4<String, View> p4Var, boolean z, tb tbVar) {
        String str;
        ArrayList<String> arrayList = tbVar.n;
        if (arrayList != null && !arrayList.isEmpty()) {
            if (z) {
                str = tbVar.o.get(0);
            } else {
                str = tbVar.n.get(0);
            }
            View view = p4Var.get(str);
            kcVar.c(obj, view);
            if (obj2 != null) {
                kcVar.c(obj2, view);
            }
        }
    }

    @DexIgnore
    public static void a(p4<String, String> p4Var, p4<String, View> p4Var2) {
        for (int size = p4Var.size() - 1; size >= 0; size--) {
            if (!p4Var2.containsKey(p4Var.e(size))) {
                p4Var.d(size);
            }
        }
    }

    @DexIgnore
    public static void a(Fragment fragment, Fragment fragment2, boolean z, p4<String, View> p4Var, boolean z2) {
        SharedElementCallback sharedElementCallback;
        int i;
        if (z) {
            sharedElementCallback = fragment2.getEnterTransitionCallback();
        } else {
            sharedElementCallback = fragment.getEnterTransitionCallback();
        }
        if (sharedElementCallback != null) {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            if (p4Var == null) {
                i = 0;
            } else {
                i = p4Var.size();
            }
            for (int i2 = 0; i2 < i; i2++) {
                arrayList2.add(p4Var.c(i2));
                arrayList.add(p4Var.e(i2));
            }
            if (z2) {
                sharedElementCallback.b(arrayList2, arrayList, (List<View>) null);
            } else {
                sharedElementCallback.a((List<String>) arrayList2, (List<View>) arrayList, (List<View>) null);
            }
        }
    }

    @DexIgnore
    public static ArrayList<View> a(kc kcVar, Object obj, Fragment fragment, ArrayList<View> arrayList, View view) {
        if (obj == null) {
            return null;
        }
        ArrayList<View> arrayList2 = new ArrayList<>();
        View view2 = fragment.getView();
        if (view2 != null) {
            kcVar.a(arrayList2, view2);
        }
        if (arrayList != null) {
            arrayList2.removeAll(arrayList);
        }
        if (arrayList2.isEmpty()) {
            return arrayList2;
        }
        arrayList2.add(view);
        kcVar.a(obj, arrayList2);
        return arrayList2;
    }

    @DexIgnore
    public static void a(ArrayList<View> arrayList, int i) {
        if (arrayList != null) {
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                arrayList.get(size).setVisibility(i);
            }
        }
    }

    @DexIgnore
    public static Object a(kc kcVar, Object obj, Object obj2, Object obj3, Fragment fragment, boolean z) {
        boolean z2;
        if (obj == null || obj2 == null || fragment == null) {
            z2 = true;
        } else {
            z2 = z ? fragment.getAllowReturnTransitionOverlap() : fragment.getAllowEnterTransitionOverlap();
        }
        if (z2) {
            return kcVar.b(obj2, obj, obj3);
        }
        return kcVar.a(obj2, obj, obj3);
    }

    @DexIgnore
    public static void a(tb tbVar, SparseArray<h> sparseArray, boolean z) {
        int size = tbVar.a.size();
        for (int i = 0; i < size; i++) {
            a(tbVar, tbVar.a.get(i), sparseArray, false, z);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0039, code lost:
        if (r0.mAdded != false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x006e, code lost:
        r9 = true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x008a, code lost:
        if (r0.mHidden == false) goto L_0x008c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x008c, code lost:
        r9 = true;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:69:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x00d9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:96:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    public static void a(tb tbVar, hc.a aVar, SparseArray<h> sparseArray, boolean z, boolean z2) {
        int i;
        boolean z3;
        boolean z4;
        boolean z5;
        h hVar;
        FragmentManager fragmentManager;
        boolean z6;
        Fragment fragment = aVar.b;
        if (fragment != null && (i = fragment.mContainerId) != 0) {
            int i2 = z ? a[aVar.a] : aVar.a;
            boolean z7 = false;
            if (i2 != 1) {
                if (i2 != 3) {
                    if (i2 != 4) {
                        if (i2 != 5) {
                            if (i2 != 6) {
                                if (i2 != 7) {
                                    z3 = false;
                                    z5 = false;
                                    z4 = false;
                                    hVar = sparseArray.get(i);
                                    if (z7) {
                                        hVar = a(hVar, sparseArray, i);
                                        hVar.a = fragment;
                                        hVar.b = z;
                                        hVar.c = tbVar;
                                    }
                                    if (!z2 && z3) {
                                        if (hVar != null && hVar.d == fragment) {
                                            hVar.d = null;
                                        }
                                        fragmentManager = tbVar.r;
                                        if (fragment.mState < 1 && fragmentManager.n >= 1 && !tbVar.p) {
                                            fragmentManager.o(fragment);
                                            fragmentManager.a(fragment, 1);
                                        }
                                    }
                                    if (z4 && (hVar == null || hVar.d == null)) {
                                        hVar = a(hVar, sparseArray, i);
                                        hVar.d = fragment;
                                        hVar.e = z;
                                        hVar.f = tbVar;
                                    }
                                    if (z2 && z5 && hVar != null && hVar.a == fragment) {
                                        hVar.a = null;
                                        return;
                                    }
                                    return;
                                }
                            }
                        } else if (z2) {
                            if (fragment.mHiddenChanged) {
                                if (!fragment.mHidden) {
                                }
                            }
                            z6 = false;
                            z7 = z6;
                            z3 = true;
                            z5 = false;
                            z4 = false;
                            hVar = sparseArray.get(i);
                            if (z7) {
                            }
                            hVar.d = null;
                            fragmentManager = tbVar.r;
                            fragmentManager.o(fragment);
                            fragmentManager.a(fragment, 1);
                            hVar = a(hVar, sparseArray, i);
                            hVar.d = fragment;
                            hVar.e = z;
                            hVar.f = tbVar;
                            if (z2) {
                                return;
                            }
                            return;
                        } else {
                            z6 = fragment.mHidden;
                            z7 = z6;
                            z3 = true;
                            z5 = false;
                            z4 = false;
                            hVar = sparseArray.get(i);
                            if (z7) {
                            }
                            hVar.d = null;
                            fragmentManager = tbVar.r;
                            fragmentManager.o(fragment);
                            fragmentManager.a(fragment, 1);
                            hVar = a(hVar, sparseArray, i);
                            hVar.d = fragment;
                            hVar.e = z;
                            hVar.f = tbVar;
                            if (z2) {
                            }
                        }
                    } else if (!z2) {
                        boolean z8 = false;
                        z4 = z8;
                        z3 = false;
                        z5 = true;
                        hVar = sparseArray.get(i);
                        if (z7) {
                        }
                        hVar.d = null;
                        fragmentManager = tbVar.r;
                        fragmentManager.o(fragment);
                        fragmentManager.a(fragment, 1);
                        hVar = a(hVar, sparseArray, i);
                        hVar.d = fragment;
                        hVar.e = z;
                        hVar.f = tbVar;
                        if (z2) {
                        }
                    } else {
                        boolean z82 = false;
                        z4 = z82;
                        z3 = false;
                        z5 = true;
                        hVar = sparseArray.get(i);
                        if (z7) {
                        }
                        hVar.d = null;
                        fragmentManager = tbVar.r;
                        fragmentManager.o(fragment);
                        fragmentManager.a(fragment, 1);
                        hVar = a(hVar, sparseArray, i);
                        hVar.d = fragment;
                        hVar.e = z;
                        hVar.f = tbVar;
                        if (z2) {
                        }
                    }
                }
                if (!z2) {
                    boolean z822 = false;
                    z4 = z822;
                    z3 = false;
                    z5 = true;
                    hVar = sparseArray.get(i);
                    if (z7) {
                    }
                    hVar.d = null;
                    fragmentManager = tbVar.r;
                    fragmentManager.o(fragment);
                    fragmentManager.a(fragment, 1);
                    hVar = a(hVar, sparseArray, i);
                    hVar.d = fragment;
                    hVar.e = z;
                    hVar.f = tbVar;
                    if (z2) {
                    }
                } else {
                    boolean z8222 = false;
                    z4 = z8222;
                    z3 = false;
                    z5 = true;
                    hVar = sparseArray.get(i);
                    if (z7) {
                    }
                    hVar.d = null;
                    fragmentManager = tbVar.r;
                    fragmentManager.o(fragment);
                    fragmentManager.a(fragment, 1);
                    hVar = a(hVar, sparseArray, i);
                    hVar.d = fragment;
                    hVar.e = z;
                    hVar.f = tbVar;
                    if (z2) {
                    }
                }
            }
            if (z2) {
                z6 = fragment.mIsNewlyAdded;
                z7 = z6;
                z3 = true;
                z5 = false;
                z4 = false;
                hVar = sparseArray.get(i);
                if (z7) {
                }
                hVar.d = null;
                fragmentManager = tbVar.r;
                fragmentManager.o(fragment);
                fragmentManager.a(fragment, 1);
                hVar = a(hVar, sparseArray, i);
                hVar.d = fragment;
                hVar.e = z;
                hVar.f = tbVar;
                if (z2) {
                }
            } else {
                if (!fragment.mAdded) {
                }
                z6 = false;
                z7 = z6;
                z3 = true;
                z5 = false;
                z4 = false;
                hVar = sparseArray.get(i);
                if (z7) {
                }
                hVar.d = null;
                fragmentManager = tbVar.r;
                fragmentManager.o(fragment);
                fragmentManager.a(fragment, 1);
                hVar = a(hVar, sparseArray, i);
                hVar.d = fragment;
                hVar.e = z;
                hVar.f = tbVar;
                if (z2) {
                }
            }
        }
    }

    @DexIgnore
    public static h a(h hVar, SparseArray<h> sparseArray, int i) {
        if (hVar != null) {
            return hVar;
        }
        h hVar2 = new h();
        sparseArray.put(i, hVar2);
        return hVar2;
    }
}
