package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;
import com.fossil.ov2;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ai2 extends ov2.a {
    @DexIgnore
    public /* final */ /* synthetic */ cs2 e;
    @DexIgnore
    public /* final */ /* synthetic */ ov2 f;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ai2(ov2 ov2, cs2 cs2) {
        super(ov2);
        this.f = ov2;
        this.e = cs2;
    }

    @DexIgnore
    public final void a() throws RemoteException {
        this.f.g.getCachedAppInstanceId(this.e);
    }

    @DexIgnore
    public final void b() {
        this.e.d((Bundle) null);
    }
}
