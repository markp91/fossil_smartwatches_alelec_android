package com.fossil;

import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactActivity;
import com.portfolio.platform.uirenew.home.alerts.hybrid.details.contact.NotificationHybridContactPresenter;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class p25 implements MembersInjector<NotificationHybridContactActivity> {
    @DexIgnore
    public static void a(NotificationHybridContactActivity notificationHybridContactActivity, NotificationHybridContactPresenter notificationHybridContactPresenter) {
        notificationHybridContactActivity.B = notificationHybridContactPresenter;
    }
}
