package com.fossil;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vu6 {
    /*
    static {
        char c = File.separatorChar;
        xu6 xu6 = new xu6(4);
        PrintWriter printWriter = new PrintWriter(xu6);
        printWriter.println();
        xu6.toString();
        printWriter.close();
    }
    */

    @DexIgnore
    public static void a(InputStream inputStream) {
        a((Closeable) inputStream);
    }

    @DexIgnore
    public static byte[] b(InputStream inputStream) throws IOException {
        wu6 wu6 = new wu6();
        a(inputStream, (OutputStream) wu6);
        return wu6.k();
    }

    @DexIgnore
    public static void a(OutputStream outputStream) {
        a((Closeable) outputStream);
    }

    @DexIgnore
    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException unused) {
            }
        }
    }

    @DexIgnore
    public static byte[] a(InputStream inputStream, long j) throws IOException {
        if (j <= 2147483647L) {
            return a(inputStream, (int) j);
        }
        throw new IllegalArgumentException("Size cannot be greater than Integer max value: " + j);
    }

    @DexIgnore
    public static long b(InputStream inputStream, OutputStream outputStream) throws IOException {
        return a(inputStream, outputStream, new byte[4096]);
    }

    @DexIgnore
    public static byte[] a(InputStream inputStream, int i) throws IOException {
        if (i >= 0) {
            int i2 = 0;
            if (i == 0) {
                return new byte[0];
            }
            byte[] bArr = new byte[i];
            while (i2 < i) {
                int read = inputStream.read(bArr, i2, i - i2);
                if (read == -1) {
                    break;
                }
                i2 += read;
            }
            if (i2 == i) {
                return bArr;
            }
            throw new IOException("Unexpected readed size. current: " + i2 + ", excepted: " + i);
        }
        throw new IllegalArgumentException("Size must be equal or greater than zero: " + i);
    }

    @DexIgnore
    public static int a(InputStream inputStream, OutputStream outputStream) throws IOException {
        long b = b(inputStream, outputStream);
        if (b > 2147483647L) {
            return -1;
        }
        return (int) b;
    }

    @DexIgnore
    public static long a(InputStream inputStream, OutputStream outputStream, byte[] bArr) throws IOException {
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr);
            if (-1 == read) {
                return j;
            }
            outputStream.write(bArr, 0, read);
            j += (long) read;
        }
    }
}
