package com.fossil;

import com.fossil.jn3;
import java.io.Serializable;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class kl3<T> extends jn3<T> implements Serializable {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ bm3<T, Integer> rankMap;

    @DexIgnore
    public kl3(List<T> list) {
        this(ym3.a(list));
    }

    @DexIgnore
    public final int a(T t) {
        Integer num = this.rankMap.get(t);
        if (num != null) {
            return num.intValue();
        }
        throw new jn3.c(t);
    }

    @DexIgnore
    public int compare(T t, T t2) {
        return a(t) - a(t2);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj instanceof kl3) {
            return this.rankMap.equals(((kl3) obj).rankMap);
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return this.rankMap.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "Ordering.explicit(" + this.rankMap.keySet() + ")";
    }

    @DexIgnore
    public kl3(bm3<T, Integer> bm3) {
        this.rankMap = bm3;
    }
}
