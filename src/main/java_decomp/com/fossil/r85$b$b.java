package com.fossil;

import com.portfolio.platform.data.model.diana.WatchApp;
import com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.watchapps.search.WatchAppSearchPresenter$start$1$allWatchApps$1", f = "WatchAppSearchPresenter.kt", l = {}, m = "invokeSuspend")
public final class r85$b$b extends sf6 implements ig6<il6, xe6<? super List<? extends WatchApp>>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ WatchAppSearchPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public r85$b$b(WatchAppSearchPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        r85$b$b r85_b_b = new r85$b$b(this.this$0, xe6);
        r85_b_b.p$ = (il6) obj;
        return r85_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((r85$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.l.getAllWatchAppRaw();
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
