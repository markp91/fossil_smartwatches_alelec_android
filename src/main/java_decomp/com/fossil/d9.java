package com.fossil;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d9 {
    @DexIgnore
    public /* final */ a a;

    @DexIgnore
    public interface a {
        @DexIgnore
        boolean a(MotionEvent motionEvent);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b implements a {
        @DexIgnore
        public static /* final */ int v; // = ViewConfiguration.getLongPressTimeout();
        @DexIgnore
        public static /* final */ int w; // = ViewConfiguration.getTapTimeout();
        @DexIgnore
        public static /* final */ int x; // = ViewConfiguration.getDoubleTapTimeout();
        @DexIgnore
        public int a;
        @DexIgnore
        public int b;
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ Handler e;
        @DexIgnore
        public /* final */ GestureDetector.OnGestureListener f;
        @DexIgnore
        public GestureDetector.OnDoubleTapListener g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public boolean j;
        @DexIgnore
        public boolean k;
        @DexIgnore
        public boolean l;
        @DexIgnore
        public MotionEvent m;
        @DexIgnore
        public MotionEvent n;
        @DexIgnore
        public boolean o;
        @DexIgnore
        public float p;
        @DexIgnore
        public float q;
        @DexIgnore
        public float r;
        @DexIgnore
        public float s;
        @DexIgnore
        public boolean t;
        @DexIgnore
        public VelocityTracker u;

        @DexIgnore
        public b(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            if (handler != null) {
                this.e = new a(handler);
            } else {
                this.e = new a();
            }
            this.f = onGestureListener;
            if (onGestureListener instanceof GestureDetector.OnDoubleTapListener) {
                a((GestureDetector.OnDoubleTapListener) onGestureListener);
            }
            a(context);
        }

        @DexIgnore
        public final void a(Context context) {
            if (context == null) {
                throw new IllegalArgumentException("Context must not be null");
            } else if (this.f != null) {
                this.t = true;
                ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
                int scaledTouchSlop = viewConfiguration.getScaledTouchSlop();
                int scaledDoubleTapSlop = viewConfiguration.getScaledDoubleTapSlop();
                this.c = viewConfiguration.getScaledMinimumFlingVelocity();
                this.d = viewConfiguration.getScaledMaximumFlingVelocity();
                this.a = scaledTouchSlop * scaledTouchSlop;
                this.b = scaledDoubleTapSlop * scaledDoubleTapSlop;
            } else {
                throw new IllegalArgumentException("OnGestureListener must not be null");
            }
        }

        @DexIgnore
        public final void b() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.o = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        @DexIgnore
        public void c() {
            this.e.removeMessages(3);
            this.i = false;
            this.j = true;
            this.f.onLongPress(this.m);
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a extends Handler {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public void handleMessage(Message message) {
                int i = message.what;
                if (i == 1) {
                    b bVar = b.this;
                    bVar.f.onShowPress(bVar.m);
                } else if (i == 2) {
                    b.this.c();
                } else if (i == 3) {
                    b bVar2 = b.this;
                    GestureDetector.OnDoubleTapListener onDoubleTapListener = bVar2.g;
                    if (onDoubleTapListener == null) {
                        return;
                    }
                    if (!bVar2.h) {
                        onDoubleTapListener.onSingleTapConfirmed(bVar2.m);
                    } else {
                        bVar2.i = true;
                    }
                } else {
                    throw new RuntimeException("Unknown message " + message);
                }
            }

            @DexIgnore
            public a(Handler handler) {
                super(handler.getLooper());
            }
        }

        @DexIgnore
        public void a(GestureDetector.OnDoubleTapListener onDoubleTapListener) {
            this.g = onDoubleTapListener;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:100:0x0208  */
        /* JADX WARNING: Removed duplicated region for block: B:103:0x021f  */
        public boolean a(MotionEvent motionEvent) {
            boolean z;
            MotionEvent motionEvent2;
            MotionEvent motionEvent3;
            boolean z2;
            boolean z3;
            GestureDetector.OnDoubleTapListener onDoubleTapListener;
            int action = motionEvent.getAction();
            if (this.u == null) {
                this.u = VelocityTracker.obtain();
            }
            this.u.addMovement(motionEvent);
            int i2 = action & 255;
            boolean z4 = i2 == 6;
            int actionIndex = z4 ? motionEvent.getActionIndex() : -1;
            int pointerCount = motionEvent.getPointerCount();
            float f2 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            float f3 = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
            for (int i3 = 0; i3 < pointerCount; i3++) {
                if (actionIndex != i3) {
                    f2 += motionEvent.getX(i3);
                    f3 += motionEvent.getY(i3);
                }
            }
            float f4 = (float) (z4 ? pointerCount - 1 : pointerCount);
            float f5 = f2 / f4;
            float f6 = f3 / f4;
            if (i2 != 0) {
                if (i2 == 1) {
                    this.h = false;
                    MotionEvent obtain = MotionEvent.obtain(motionEvent);
                    if (this.o) {
                        z3 = this.g.onDoubleTapEvent(motionEvent) | false;
                    } else {
                        if (this.j) {
                            this.e.removeMessages(3);
                            this.j = false;
                        } else if (this.k) {
                            boolean onSingleTapUp = this.f.onSingleTapUp(motionEvent);
                            if (this.i && (onDoubleTapListener = this.g) != null) {
                                onDoubleTapListener.onSingleTapConfirmed(motionEvent);
                            }
                            z3 = onSingleTapUp;
                        } else {
                            VelocityTracker velocityTracker = this.u;
                            int pointerId = motionEvent.getPointerId(0);
                            velocityTracker.computeCurrentVelocity(1000, (float) this.d);
                            float yVelocity = velocityTracker.getYVelocity(pointerId);
                            float xVelocity = velocityTracker.getXVelocity(pointerId);
                            if (Math.abs(yVelocity) > ((float) this.c) || Math.abs(xVelocity) > ((float) this.c)) {
                                z3 = this.f.onFling(this.m, motionEvent, xVelocity, yVelocity);
                            }
                        }
                        z3 = false;
                    }
                    MotionEvent motionEvent4 = this.n;
                    if (motionEvent4 != null) {
                        motionEvent4.recycle();
                    }
                    this.n = obtain;
                    VelocityTracker velocityTracker2 = this.u;
                    if (velocityTracker2 != null) {
                        velocityTracker2.recycle();
                        this.u = null;
                    }
                    this.o = false;
                    this.i = false;
                    this.e.removeMessages(1);
                    this.e.removeMessages(2);
                } else if (i2 != 2) {
                    if (i2 == 3) {
                        a();
                        return false;
                    } else if (i2 == 5) {
                        this.p = f5;
                        this.r = f5;
                        this.q = f6;
                        this.s = f6;
                        b();
                        return false;
                    } else if (i2 != 6) {
                        return false;
                    } else {
                        this.p = f5;
                        this.r = f5;
                        this.q = f6;
                        this.s = f6;
                        this.u.computeCurrentVelocity(1000, (float) this.d);
                        int actionIndex2 = motionEvent.getActionIndex();
                        int pointerId2 = motionEvent.getPointerId(actionIndex2);
                        float xVelocity2 = this.u.getXVelocity(pointerId2);
                        float yVelocity2 = this.u.getYVelocity(pointerId2);
                        for (int i4 = 0; i4 < pointerCount; i4++) {
                            if (i4 != actionIndex2) {
                                int pointerId3 = motionEvent.getPointerId(i4);
                                if ((this.u.getXVelocity(pointerId3) * xVelocity2) + (this.u.getYVelocity(pointerId3) * yVelocity2) < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                                    this.u.clear();
                                    return false;
                                }
                            }
                        }
                        return false;
                    }
                } else if (this.j) {
                    return false;
                } else {
                    float f7 = this.p - f5;
                    float f8 = this.q - f6;
                    if (this.o) {
                        return false | this.g.onDoubleTapEvent(motionEvent);
                    }
                    if (this.k) {
                        int i5 = (int) (f5 - this.r);
                        int i6 = (int) (f6 - this.s);
                        int i7 = (i5 * i5) + (i6 * i6);
                        if (i7 > this.a) {
                            z2 = this.f.onScroll(this.m, motionEvent, f7, f8);
                            this.p = f5;
                            this.q = f6;
                            this.k = false;
                            this.e.removeMessages(3);
                            this.e.removeMessages(1);
                            this.e.removeMessages(2);
                        } else {
                            z2 = false;
                        }
                        if (i7 > this.a) {
                            this.l = false;
                        }
                    } else if (Math.abs(f7) < 1.0f && Math.abs(f8) < 1.0f) {
                        return false;
                    } else {
                        boolean onScroll = this.f.onScroll(this.m, motionEvent, f7, f8);
                        this.p = f5;
                        this.q = f6;
                        return onScroll;
                    }
                }
                return z2;
            }
            if (this.g != null) {
                boolean hasMessages = this.e.hasMessages(3);
                if (hasMessages) {
                    this.e.removeMessages(3);
                }
                MotionEvent motionEvent5 = this.m;
                if (motionEvent5 == null || (motionEvent3 = this.n) == null || !hasMessages || !a(motionEvent5, motionEvent3, motionEvent)) {
                    this.e.sendEmptyMessageDelayed(3, (long) x);
                } else {
                    this.o = true;
                    z = this.g.onDoubleTap(this.m) | false | this.g.onDoubleTapEvent(motionEvent);
                    this.p = f5;
                    this.r = f5;
                    this.q = f6;
                    this.s = f6;
                    motionEvent2 = this.m;
                    if (motionEvent2 != null) {
                        motionEvent2.recycle();
                    }
                    this.m = MotionEvent.obtain(motionEvent);
                    this.k = true;
                    this.l = true;
                    this.h = true;
                    this.j = false;
                    this.i = false;
                    if (this.t) {
                        this.e.removeMessages(2);
                        this.e.sendEmptyMessageAtTime(2, this.m.getDownTime() + ((long) w) + ((long) v));
                    }
                    this.e.sendEmptyMessageAtTime(1, this.m.getDownTime() + ((long) w));
                    return z | this.f.onDown(motionEvent);
                }
            }
            z = false;
            this.p = f5;
            this.r = f5;
            this.q = f6;
            this.s = f6;
            motionEvent2 = this.m;
            if (motionEvent2 != null) {
            }
            this.m = MotionEvent.obtain(motionEvent);
            this.k = true;
            this.l = true;
            this.h = true;
            this.j = false;
            this.i = false;
            if (this.t) {
            }
            this.e.sendEmptyMessageAtTime(1, this.m.getDownTime() + ((long) w));
            return z | this.f.onDown(motionEvent);
        }

        @DexIgnore
        public final void a() {
            this.e.removeMessages(1);
            this.e.removeMessages(2);
            this.e.removeMessages(3);
            this.u.recycle();
            this.u = null;
            this.o = false;
            this.h = false;
            this.k = false;
            this.l = false;
            this.i = false;
            if (this.j) {
                this.j = false;
            }
        }

        @DexIgnore
        public final boolean a(MotionEvent motionEvent, MotionEvent motionEvent2, MotionEvent motionEvent3) {
            if (!this.l || motionEvent3.getEventTime() - motionEvent2.getEventTime() > ((long) x)) {
                return false;
            }
            int x2 = ((int) motionEvent.getX()) - ((int) motionEvent3.getX());
            int y = ((int) motionEvent.getY()) - ((int) motionEvent3.getY());
            if ((x2 * x2) + (y * y) < this.b) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements a {
        @DexIgnore
        public /* final */ GestureDetector a;

        @DexIgnore
        public c(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
            this.a = new GestureDetector(context, onGestureListener, handler);
        }

        @DexIgnore
        public boolean a(MotionEvent motionEvent) {
            return this.a.onTouchEvent(motionEvent);
        }
    }

    @DexIgnore
    public d9(Context context, GestureDetector.OnGestureListener onGestureListener) {
        this(context, onGestureListener, (Handler) null);
    }

    @DexIgnore
    public boolean a(MotionEvent motionEvent) {
        return this.a.a(motionEvent);
    }

    @DexIgnore
    public d9(Context context, GestureDetector.OnGestureListener onGestureListener, Handler handler) {
        if (Build.VERSION.SDK_INT > 17) {
            this.a = new c(context, onGestureListener, handler);
        } else {
            this.a = new b(context, onGestureListener, handler);
        }
    }
}
