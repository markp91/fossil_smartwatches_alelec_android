package com.fossil;

import com.fossil.mz4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.details.notificationcallsandmessages.NotificationCallsAndMessagesPresenter$setRuleToDevice$1$contactAppDeffer$1", f = "NotificationCallsAndMessagesPresenter.kt", l = {328}, m = "invokeSuspend")
public final class oy4$f$a extends sf6 implements ig6<il6, xe6<? super List<? extends AppNotificationFilter>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ NotificationCallsAndMessagesPresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public oy4$f$a(NotificationCallsAndMessagesPresenter.f fVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        oy4$f$a oy4_f_a = new oy4$f$a(this.this$0, xe6);
        oy4_f_a.p$ = (il6) obj;
        return oy4_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((oy4$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.util.List} */
    /* JADX WARNING: type inference failed for: r1v10, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        List list = null;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            Object e = this.this$0.this$0.v;
            this.L$0 = il6;
            this.L$1 = null;
            this.label = 1;
            obj = n24.a(e, null, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            list = this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        CoroutineUseCase.c cVar = (CoroutineUseCase.c) obj;
        if (cVar instanceof mz4.d) {
            list = new ArrayList();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String a2 = NotificationCallsAndMessagesPresenter.E.a();
            StringBuilder sb = new StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            mz4.d dVar = (mz4.d) cVar;
            sb.append(dVar.a().size());
            local.d(a2, sb.toString());
            this.this$0.this$0.g = yd6.d(dVar.a());
            if (this.this$0.this$0.t.G() == 0) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "call everyone");
                DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                list.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
            } else if (this.this$0.this$0.t.G() == 1) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "call favorite");
                int size = this.this$0.this$0.g.size();
                for (int i2 = 0; i2 < size; i2++) {
                    ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.g.get(i2);
                    DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType()));
                    List contacts = contactGroup.getContacts();
                    wg6.a((Object) contacts, "item.contacts");
                    if (!contacts.isEmpty()) {
                        Object obj2 = contactGroup.getContacts().get(0);
                        wg6.a(obj2, "item.contacts[0]");
                        appNotificationFilter.setSender(((Contact) obj2).getDisplayName());
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String a3 = NotificationCallsAndMessagesPresenter.E.a();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append("getTypeAllowFromCall - item ");
                        sb2.append(i2);
                        sb2.append(" name = ");
                        Object obj3 = contactGroup.getContacts().get(0);
                        wg6.a(obj3, "item.contacts[0]");
                        sb2.append(((Contact) obj3).getDisplayName());
                        local2.d(a3, sb2.toString());
                        list.add(appNotificationFilter);
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "call no one");
            }
            if (this.this$0.this$0.t.M() == 0) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "message everyone");
                DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                list.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
            } else if (this.this$0.this$0.t.M() == 1) {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "message favorite");
                int size2 = this.this$0.this$0.g.size();
                for (int i3 = 0; i3 < size2; i3++) {
                    ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.g.get(i3);
                    DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType()));
                    List contacts2 = contactGroup2.getContacts();
                    wg6.a((Object) contacts2, "item.contacts");
                    if (!contacts2.isEmpty()) {
                        Object obj4 = contactGroup2.getContacts().get(0);
                        wg6.a(obj4, "item.contacts[0]");
                        appNotificationFilter2.setSender(((Contact) obj4).getDisplayName());
                        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                        String a4 = NotificationCallsAndMessagesPresenter.E.a();
                        StringBuilder sb3 = new StringBuilder();
                        sb3.append("getTypeAllowFromMessage - item ");
                        sb3.append(i3);
                        sb3.append(" name = ");
                        Object obj5 = contactGroup2.getContacts().get(0);
                        wg6.a(obj5, "item.contacts[0]");
                        sb3.append(((Contact) obj5).getDisplayName());
                        local3.d(a4, sb3.toString());
                        list.add(appNotificationFilter2);
                    }
                }
            } else {
                FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "message no one");
            }
        } else if (cVar instanceof mz4.b) {
            FLogger.INSTANCE.getLocal().d(NotificationCallsAndMessagesPresenter.E.a(), "GetAllContactGroup onError");
        }
        return list;
    }
}
