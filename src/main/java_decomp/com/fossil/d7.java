package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.util.TypedValue;
import com.fossil.a7;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d7 {
    @DexIgnore
    public static Drawable a(Resources resources, int i, Resources.Theme theme) throws Resources.NotFoundException {
        if (Build.VERSION.SDK_INT >= 21) {
            return resources.getDrawable(i, theme);
        }
        return resources.getDrawable(i);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a {

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.d7$a$a")
        /* renamed from: com.fossil.d7$a$a  reason: collision with other inner class name */
        public class C0014a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ Typeface a;

            @DexIgnore
            public C0014a(Typeface typeface) {
                this.a = typeface;
            }

            @DexIgnore
            public void run() {
                a.this.a(this.a);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class b implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ int a;

            @DexIgnore
            public b(int i) {
                this.a = i;
            }

            @DexIgnore
            public void run() {
                a.this.a(this.a);
            }
        }

        @DexIgnore
        public abstract void a(int i);

        @DexIgnore
        public abstract void a(Typeface typeface);

        @DexIgnore
        public final void a(Typeface typeface, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new C0014a(typeface));
        }

        @DexIgnore
        public final void a(int i, Handler handler) {
            if (handler == null) {
                handler = new Handler(Looper.getMainLooper());
            }
            handler.post(new b(i));
        }
    }

    @DexIgnore
    public static Typeface a(Context context, int i) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return a(context, i, new TypedValue(), 0, (a) null, (Handler) null, false);
    }

    @DexIgnore
    public static void a(Context context, int i, a aVar, Handler handler) throws Resources.NotFoundException {
        y8.a(aVar);
        if (context.isRestricted()) {
            aVar.a(-4, handler);
            return;
        }
        a(context, i, new TypedValue(), 0, aVar, handler, false);
    }

    @DexIgnore
    public static Typeface a(Context context, int i, TypedValue typedValue, int i2, a aVar) throws Resources.NotFoundException {
        if (context.isRestricted()) {
            return null;
        }
        return a(context, i, typedValue, i2, aVar, (Handler) null, true);
    }

    @DexIgnore
    public static Typeface a(Context context, int i, TypedValue typedValue, int i2, a aVar, Handler handler, boolean z) {
        Resources resources = context.getResources();
        resources.getValue(i, typedValue, true);
        Typeface a2 = a(context, resources, typedValue, i, i2, aVar, handler, z);
        if (a2 != null || aVar != null) {
            return a2;
        }
        throw new Resources.NotFoundException("Font resource ID #0x" + Integer.toHexString(i) + " could not be retrieved.");
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a3  */
    public static Typeface a(Context context, Resources resources, TypedValue typedValue, int i, int i2, a aVar, Handler handler, boolean z) {
        Resources resources2 = resources;
        TypedValue typedValue2 = typedValue;
        int i3 = i;
        int i4 = i2;
        a aVar2 = aVar;
        Handler handler2 = handler;
        CharSequence charSequence = typedValue2.string;
        if (charSequence != null) {
            String charSequence2 = charSequence.toString();
            if (!charSequence2.startsWith("res/")) {
                if (aVar2 != null) {
                    aVar2.a(-3, handler2);
                }
                return null;
            }
            Typeface b = h7.b(resources2, i3, i4);
            if (b != null) {
                if (aVar2 != null) {
                    aVar2.a(b, handler2);
                }
                return b;
            }
            try {
                if (charSequence2.toLowerCase().endsWith(".xml")) {
                    a7.a a2 = a7.a((XmlPullParser) resources2.getXml(i3), resources2);
                    if (a2 != null) {
                        return h7.a(context, a2, resources, i, i2, aVar, handler, z);
                    }
                    Log.e("ResourcesCompat", "Failed to find font-family tag");
                    if (aVar2 != null) {
                        aVar2.a(-3, handler2);
                    }
                    return null;
                }
                Context context2 = context;
                Typeface a3 = h7.a(context, resources2, i3, charSequence2, i4);
                if (aVar2 != null) {
                    if (a3 != null) {
                        aVar2.a(a3, handler2);
                    } else {
                        aVar2.a(-3, handler2);
                    }
                }
                return a3;
            } catch (XmlPullParserException e) {
                Log.e("ResourcesCompat", "Failed to parse xml resource " + charSequence2, e);
                if (aVar2 != null) {
                    aVar2.a(-3, handler2);
                }
                return null;
            } catch (IOException e2) {
                Log.e("ResourcesCompat", "Failed to read xml resource " + charSequence2, e2);
                if (aVar2 != null) {
                }
                return null;
            }
        } else {
            throw new Resources.NotFoundException("Resource \"" + resources2.getResourceName(i3) + "\" (" + Integer.toHexString(i) + ") is not a Font: " + typedValue2);
        }
    }
}
