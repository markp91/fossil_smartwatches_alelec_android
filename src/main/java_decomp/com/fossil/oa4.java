package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class oa4 extends na4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j t; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray u; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout r;
    @DexIgnore
    public long s;

    /*
    static {
        u.put(2131361966, 1);
    }
    */

    @DexIgnore
    public oa4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 2, t, u));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.s = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.s != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.s = 1;
        }
        g();
    }

    @DexIgnore
    public oa4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1]);
        this.s = -1;
        this.r = objArr[0];
        this.r.setTag((Object) null);
        a(view);
        f();
    }
}
