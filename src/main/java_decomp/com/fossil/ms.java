package com.fossil;

import com.fossil.gs;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ms implements gs<InputStream> {
    @DexIgnore
    public /* final */ zw a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements gs.a<InputStream> {
        @DexIgnore
        public /* final */ xt a;

        @DexIgnore
        public a(xt xtVar) {
            this.a = xtVar;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }

        @DexIgnore
        public gs<InputStream> a(InputStream inputStream) {
            return new ms(inputStream, this.a);
        }
    }

    @DexIgnore
    public ms(InputStream inputStream, xt xtVar) {
        this.a = new zw(inputStream, xtVar);
        this.a.mark(5242880);
    }

    @DexIgnore
    public void a() {
        this.a.l();
    }

    @DexIgnore
    public void c() {
        this.a.k();
    }

    @DexIgnore
    public InputStream b() throws IOException {
        this.a.reset();
        return this.a;
    }
}
