package com.fossil;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ag1 {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ String b;
    @DexIgnore
    public int c;
    @DexIgnore
    public long d;
    @DexIgnore
    public /* final */ Handler e;
    @DexIgnore
    public /* final */ String f;
    @DexIgnore
    public /* final */ String g;

    @DexIgnore
    public ag1(String str, int i, long j, Handler handler, String str2, String str3) {
        this.b = str;
        this.c = i;
        this.d = j;
        this.e = handler;
        this.f = str2;
        this.g = str3;
        String str4 = this.b;
        Context a2 = gk0.f.a();
        int i2 = 0;
        SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str4, 0) : null;
        this.a = sharedPreferences != null ? sharedPreferences.getInt("file_count", 0) : i2;
    }

    @DexIgnore
    public final String a(int i) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.b);
        sb.append(File.separatorChar);
        nh6 nh6 = nh6.a;
        Object[] objArr = {this.f, Integer.valueOf(i), this.g};
        String format = String.format("%s_%010d.%s", Arrays.copyOf(objArr, objArr.length));
        wg6.a(format, "java.lang.String.format(format, *args)");
        sb.append(format);
        return sb.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (com.fossil.wg6.a(com.fossil.cg6.a(r6), r12.g) != false) goto L_0x0058;
     */
    @DexIgnore
    public final File[] b() {
        File d2 = d();
        if (d2 == null || !d2.exists()) {
            oa1 oa1 = oa1.a;
            return new File[0];
        }
        f();
        File[] listFiles = d2.listFiles();
        if (listFiles == null) {
            listFiles = new File[0];
        }
        File c2 = c();
        ArrayList arrayList = new ArrayList();
        int length = listFiles.length;
        for (int i = 0; i < length; i++) {
            File file = listFiles[i];
            boolean z = true;
            if ((!wg6.a(file, c2)) && file.isFile()) {
                String name = file.getName();
                wg6.a(name, "it.name");
                if (xj6.c(name, this.f, false, 2, (Object) null)) {
                }
            }
            z = false;
            if (z) {
                arrayList.add(file);
            }
        }
        Object[] array = arrayList.toArray(new File[0]);
        if (array != null) {
            return (File[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final File c() {
        File file;
        synchronized (Integer.valueOf(this.a)) {
            String a2 = a(this.a);
            Context a3 = gk0.f.a();
            file = a3 != null ? new File(a3.getFilesDir(), a2) : null;
        }
        return file;
    }

    @DexIgnore
    public final File d() {
        String str = this.b;
        Context a2 = gk0.f.a();
        if (a2 != null) {
            return new File(a2.getFilesDir(), str);
        }
        return null;
    }

    @DexIgnore
    public final File e() {
        File d2 = d();
        if (d2 == null) {
            return null;
        }
        if (!d2.exists()) {
            d2.mkdirs();
        }
        File c2 = c();
        if (c2 == null) {
            return null;
        }
        if (c2.length() < ((long) this.c)) {
            return c2;
        }
        f();
        a();
        return e();
    }

    @DexIgnore
    public final void f() {
        SharedPreferences.Editor edit;
        synchronized (Integer.valueOf(this.a)) {
            long j = (long) Integer.MAX_VALUE;
            this.a = (int) (((((long) this.a) + 1) + j) % j);
            String str = this.b;
            Context a2 = gk0.f.a();
            SharedPreferences sharedPreferences = a2 != null ? a2.getSharedPreferences(str, 0) : null;
            if (!(sharedPreferences == null || (edit = sharedPreferences.edit()) == null)) {
                SharedPreferences.Editor putInt = edit.putInt("file_count", this.a);
                if (putInt != null) {
                    putInt.apply();
                    cd6 cd6 = cd6.a;
                }
            }
        }
    }

    @DexIgnore
    public final void a() {
        List c2 = nd6.c(b(), new ee1());
        int i = 0;
        long j = 0;
        while (i < c2.size() && j < this.d) {
            File file = (File) yd6.a(c2, i);
            j += file != null ? file.length() : 0;
            i++;
        }
        int size = c2.size();
        while (i < size) {
            File file2 = (File) yd6.a(c2, i);
            if (file2 != null) {
                file2.delete();
            }
            i++;
        }
    }
}
