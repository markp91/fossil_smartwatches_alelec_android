package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import androidx.constraintlayout.widget.Barrier;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Guideline;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomEditGoalView;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class jd4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleTextView A;
    @DexIgnore
    public /* final */ FlexibleTextView B;
    @DexIgnore
    public /* final */ FlexibleTextView C;
    @DexIgnore
    public /* final */ RTLImageView D;
    @DexIgnore
    public /* final */ LinearLayout E;
    @DexIgnore
    public /* final */ LinearLayout F;
    @DexIgnore
    public /* final */ ConstraintLayout G;
    @DexIgnore
    public /* final */ ScrollView H;
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public /* final */ CustomEditGoalView r;
    @DexIgnore
    public /* final */ CustomEditGoalView s;
    @DexIgnore
    public /* final */ CustomEditGoalView t;
    @DexIgnore
    public /* final */ CustomEditGoalView u;
    @DexIgnore
    public /* final */ FlexibleEditText v;
    @DexIgnore
    public /* final */ FlexibleEditText w;
    @DexIgnore
    public /* final */ FlexibleEditText x;
    @DexIgnore
    public /* final */ FlexibleTextView y;
    @DexIgnore
    public /* final */ FlexibleTextView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public jd4(Object obj, View view, int i, Barrier barrier, ConstraintLayout constraintLayout, CustomEditGoalView customEditGoalView, CustomEditGoalView customEditGoalView2, CustomEditGoalView customEditGoalView3, CustomEditGoalView customEditGoalView4, FlexibleEditText flexibleEditText, FlexibleEditText flexibleEditText2, FlexibleEditText flexibleEditText3, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, Guideline guideline, RTLImageView rTLImageView, LinearLayout linearLayout, LinearLayout linearLayout2, ConstraintLayout constraintLayout2, ScrollView scrollView, FlexibleTextView flexibleTextView6, View view2, View view3, Guideline guideline2) {
        super(obj, view, i);
        this.q = constraintLayout;
        this.r = customEditGoalView;
        this.s = customEditGoalView2;
        this.t = customEditGoalView3;
        this.u = customEditGoalView4;
        this.v = flexibleEditText;
        this.w = flexibleEditText2;
        this.x = flexibleEditText3;
        this.y = flexibleTextView;
        this.z = flexibleTextView2;
        this.A = flexibleTextView3;
        this.B = flexibleTextView4;
        this.C = flexibleTextView5;
        this.D = rTLImageView;
        this.E = linearLayout;
        this.F = linearLayout2;
        this.G = constraintLayout2;
        this.H = scrollView;
    }
}
