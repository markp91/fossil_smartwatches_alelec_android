package com.fossil;

import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tn2<K> implements Map.Entry<K, Object> {
    @DexIgnore
    public Map.Entry<K, rn2> a;

    @DexIgnore
    public tn2(Map.Entry<K, rn2> entry) {
        this.a = entry;
    }

    @DexIgnore
    public final K getKey() {
        return this.a.getKey();
    }

    @DexIgnore
    public final Object getValue() {
        if (this.a.getValue() == null) {
            return null;
        }
        rn2.c();
        throw null;
    }

    @DexIgnore
    public final Object setValue(Object obj) {
        if (obj instanceof ro2) {
            return this.a.getValue().a((ro2) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
