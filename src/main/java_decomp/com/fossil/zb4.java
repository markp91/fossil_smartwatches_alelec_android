package com.fossil;

import android.view.View;
import android.widget.ImageView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class zb4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleButton q;
    @DexIgnore
    public /* final */ FlexibleEditText r;
    @DexIgnore
    public /* final */ ImageView s;
    @DexIgnore
    public /* final */ RTLImageView t;
    @DexIgnore
    public /* final */ ConstraintLayout u;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView v;

    @DexIgnore
    public zb4(Object obj, View view, int i, FlexibleButton flexibleButton, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, ImageView imageView, RTLImageView rTLImageView, ConstraintLayout constraintLayout, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleButton;
        this.r = flexibleEditText;
        this.s = imageView;
        this.t = rTLImageView;
        this.u = constraintLayout;
        this.v = alphabetFastScrollRecyclerView;
    }
}
