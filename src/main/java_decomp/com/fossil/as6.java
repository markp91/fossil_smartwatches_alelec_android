package com.fossil;

import java.io.IOException;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.Response;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class as6 implements Interceptor.Chain {
    @DexIgnore
    public /* final */ List<Interceptor> a;
    @DexIgnore
    public /* final */ tr6 b;
    @DexIgnore
    public /* final */ wr6 c;
    @DexIgnore
    public /* final */ pr6 d;
    @DexIgnore
    public /* final */ int e;
    @DexIgnore
    public /* final */ yq6 f;
    @DexIgnore
    public /* final */ dq6 g;
    @DexIgnore
    public /* final */ pq6 h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ int j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public int l;

    @DexIgnore
    public as6(List<Interceptor> list, tr6 tr6, wr6 wr6, pr6 pr6, int i2, yq6 yq6, dq6 dq6, pq6 pq6, int i3, int i4, int i5) {
        this.a = list;
        this.d = pr6;
        this.b = tr6;
        this.c = wr6;
        this.e = i2;
        this.f = yq6;
        this.g = dq6;
        this.h = pq6;
        this.i = i3;
        this.j = i4;
        this.k = i5;
    }

    @DexIgnore
    public int a() {
        return this.j;
    }

    @DexIgnore
    public int b() {
        return this.k;
    }

    @DexIgnore
    public hq6 c() {
        return this.d;
    }

    @DexIgnore
    public int d() {
        return this.i;
    }

    @DexIgnore
    public dq6 e() {
        return this.g;
    }

    @DexIgnore
    public pq6 f() {
        return this.h;
    }

    @DexIgnore
    public wr6 g() {
        return this.c;
    }

    @DexIgnore
    public tr6 h() {
        return this.b;
    }

    @DexIgnore
    public yq6 t() {
        return this.f;
    }

    @DexIgnore
    public Response a(yq6 yq6) throws IOException {
        return a(yq6, this.b, this.c, this.d);
    }

    @DexIgnore
    public Response a(yq6 yq6, tr6 tr6, wr6 wr6, pr6 pr6) throws IOException {
        if (this.e < this.a.size()) {
            this.l++;
            if (this.c != null && !this.d.a(yq6.g())) {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must retain the same host and port");
            } else if (this.c == null || this.l <= 1) {
                as6 as6 = new as6(this.a, tr6, wr6, pr6, this.e + 1, yq6, this.g, this.h, this.i, this.j, this.k);
                Interceptor interceptor = this.a.get(this.e);
                Response intercept = interceptor.intercept(as6);
                if (wr6 != null && this.e + 1 < this.a.size() && as6.l != 1) {
                    throw new IllegalStateException("network interceptor " + interceptor + " must call proceed() exactly once");
                } else if (intercept == null) {
                    throw new NullPointerException("interceptor " + interceptor + " returned null");
                } else if (intercept.k() != null) {
                    return intercept;
                } else {
                    throw new IllegalStateException("interceptor " + interceptor + " returned a response with no body");
                }
            } else {
                throw new IllegalStateException("network interceptor " + this.a.get(this.e - 1) + " must call proceed() exactly once");
            }
        } else {
            throw new AssertionError();
        }
    }
}
