package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class q35 {
    @DexIgnore
    public /* final */ q45 a;
    @DexIgnore
    public /* final */ m75 b;
    @DexIgnore
    public /* final */ r65 c;

    @DexIgnore
    public q35(q45 q45, m75 m75, r65 r65) {
        wg6.b(q45, "mComplicationsContractView");
        wg6.b(m75, "mWatchAppsContractView");
        wg6.b(r65, "mCustomizeThemeContractView");
        this.a = q45;
        this.b = m75;
        this.c = r65;
    }

    @DexIgnore
    public final q45 a() {
        return this.a;
    }

    @DexIgnore
    public final r65 b() {
        return this.c;
    }

    @DexIgnore
    public final m75 c() {
        return this.b;
    }
}
