package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationHandMovingConfig extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ short a;
    @DexIgnore
    public /* final */ short b;
    @DexIgnore
    public /* final */ short c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<b80> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new NotificationHandMovingConfig((short) parcel.readInt(), (short) parcel.readInt(), (short) parcel.readInt(), parcel.readInt());
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new NotificationHandMovingConfig[i];
        }
    }

    @DexIgnore
    public NotificationHandMovingConfig(short s, short s2, short s3, int i) throws IllegalArgumentException {
        this.a = s;
        this.b = s2;
        this.c = s3;
        this.d = i;
        short s4 = this.a;
        boolean z = false;
        if (s4 == -2 || s4 == -1 || (s4 >= 0 && 359 >= s4)) {
            short s5 = this.b;
            if (s5 == -2 || s5 == -1 || (s5 >= 0 && 359 >= s5)) {
                short s6 = this.c;
                if (s6 == -2 || s6 == -1 || (s6 >= 0 && 359 >= s6)) {
                    int i2 = this.d;
                    if (1000 <= i2 && 60000 >= i2) {
                        z = true;
                    }
                    if (!z) {
                        throw new IllegalArgumentException(ze0.a(ze0.b("durationInMs ("), this.d, ") is out of range ", "[1000, 60000]."));
                    }
                    return;
                }
                StringBuilder b2 = ze0.b("subeyeDegree (");
                b2.append(this.c);
                b2.append(") must be equal to ");
                b2.append("-2 or -1 or in range");
                b2.append("[0, 359].");
                throw new IllegalArgumentException(b2.toString());
            }
            StringBuilder b3 = ze0.b("minuteDegree (");
            b3.append(this.b);
            b3.append(") must be equal to ");
            b3.append("-2 or -1 or in range");
            b3.append("[0, 359].");
            throw new IllegalArgumentException(b3.toString());
        }
        StringBuilder b4 = ze0.b("hourDegree (");
        b4.append(this.a);
        b4.append(") must be equal to ");
        b4.append("-2 or -1 or in range");
        b4.append("[0, 359].");
        throw new IllegalArgumentException(b4.toString());
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.HOUR_DEGREE, (Object) Short.valueOf(this.a)), bm0.MINUTE_DEGREE, (Object) Short.valueOf(this.b)), bm0.SUBEYE_DEGREE, (Object) Short.valueOf(this.c)), bm0.DURATION_IN_MS, (Object) Integer.valueOf(this.d));
    }

    @DexIgnore
    public final byte[] b() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putShort(this.a).putShort(this.b).putShort(this.c).putShort((short) this.d).array();
        wg6.a(array, "ByteBuffer.allocate(DATA\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(NotificationHandMovingConfig.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationHandMovingConfig notificationHandMovingConfig = (NotificationHandMovingConfig) obj;
            return this.a == notificationHandMovingConfig.a && this.b == notificationHandMovingConfig.b && this.c == notificationHandMovingConfig.c && this.d == notificationHandMovingConfig.d;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationHandMovingConfig");
    }

    @DexIgnore
    public final int getDurationInMs() {
        return this.d;
    }

    @DexIgnore
    public final short getHourDegree() {
        return this.a;
    }

    @DexIgnore
    public final short getMinuteDegree() {
        return this.b;
    }

    @DexIgnore
    public final short getSubeyeDegree() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return (((((this.a * 31) + this.b) * 31) + this.c) * 31) + this.d;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeInt(this.a);
        }
        if (parcel != null) {
            parcel.writeInt(this.b);
        }
        if (parcel != null) {
            parcel.writeInt(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
    }
}
