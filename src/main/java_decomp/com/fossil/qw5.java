package com.fossil;

import android.text.TextUtils;
import com.fossil.wearables.fsl.sleep.MFSleepSession;
import com.fossil.wearables.fsl.sleep.MFSleepSessionProvider;
import com.google.gson.Gson;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.PinObject;
import com.portfolio.platform.data.model.room.fitness.SampleRaw;
import com.portfolio.platform.data.source.ActivitiesRepository;
import com.portfolio.platform.helper.GsonConvertDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.DateTime;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qw5 {
    @DexIgnore
    public static /* final */ String b;
    @DexIgnore
    public ActivitiesRepository a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = qw5.class.getSimpleName();
        wg6.a((Object) simpleName, "MigrateDataActivitiesAnd\u2026ps::class.java.simpleName");
        b = simpleName;
    }
    */

    @DexIgnore
    public qw5() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    public final void a() {
        if (!TextUtils.isEmpty(rx5.a().f(PortfolioApp.get.instance()))) {
            fo4 j = zm4.p.a().j();
            List<PinObject> b2 = j.b("FitnessSample");
            ArrayList arrayList = new ArrayList();
            for (PinObject next : b2) {
                Gson gson = new Gson();
                wg6.a((Object) next, "pin");
                SampleRaw sampleRaw = (SampleRaw) gson.a(next.getJsonData(), SampleRaw.class);
                if (sampleRaw != null) {
                    arrayList.add(sampleRaw);
                }
                j.a(next);
            }
            ActivitiesRepository activitiesRepository = this.a;
            if (activitiesRepository != null) {
                activitiesRepository.updateActivityPinType(arrayList, 1);
                List<PinObject> b3 = j.b("MFSleepSessionUpload");
                ArrayList arrayList2 = new ArrayList();
                for (PinObject next2 : b3) {
                    du3 du3 = new du3();
                    du3.a(DateTime.class, new GsonConvertDateTime());
                    Gson a2 = du3.a();
                    wg6.a((Object) next2, "pin");
                    MFSleepSession mFSleepSession = (MFSleepSession) a2.a(next2.getJsonData(), MFSleepSession.class);
                    if (mFSleepSession != null) {
                        arrayList2.add(mFSleepSession);
                    }
                    j.a(next2);
                }
                MFSleepSessionProvider m = zm4.p.a().m();
                Iterator it = arrayList2.iterator();
                while (it.hasNext()) {
                    MFSleepSession mFSleepSession2 = (MFSleepSession) it.next();
                    wg6.a((Object) mFSleepSession2, "sleepSession");
                    mFSleepSession2.setPinType(1);
                    m.updateSleepSessionPinType(mFSleepSession2, 1);
                }
                return;
            }
            wg6.d("mActivityRepository");
            throw null;
        }
        FLogger.INSTANCE.getLocal().d(b, "There is no user here. No need to migrate");
    }
}
