package com.fossil;

import android.database.Cursor;
import com.facebook.internal.ServerProtocol;
import com.fossil.am;
import com.fossil.zn;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bo implements ao {
    @DexIgnore
    public /* final */ oh a;
    @DexIgnore
    public /* final */ hh b;
    @DexIgnore
    public /* final */ vh c;
    @DexIgnore
    public /* final */ vh d;
    @DexIgnore
    public /* final */ vh e;
    @DexIgnore
    public /* final */ vh f;
    @DexIgnore
    public /* final */ vh g;
    @DexIgnore
    public /* final */ vh h;
    @DexIgnore
    public /* final */ vh i;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends hh<zn> {
        @DexIgnore
        public a(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        /* renamed from: a */
        public void bind(mi miVar, zn znVar) {
            String str = znVar.a;
            if (str == null) {
                miVar.a(1);
            } else {
                miVar.a(1, str);
            }
            miVar.a(2, (long) fo.a(znVar.b));
            String str2 = znVar.c;
            if (str2 == null) {
                miVar.a(3);
            } else {
                miVar.a(3, str2);
            }
            String str3 = znVar.d;
            if (str3 == null) {
                miVar.a(4);
            } else {
                miVar.a(4, str3);
            }
            byte[] a = pl.a(znVar.e);
            if (a == null) {
                miVar.a(5);
            } else {
                miVar.a(5, a);
            }
            byte[] a2 = pl.a(znVar.f);
            if (a2 == null) {
                miVar.a(6);
            } else {
                miVar.a(6, a2);
            }
            miVar.a(7, znVar.g);
            miVar.a(8, znVar.h);
            miVar.a(9, znVar.i);
            miVar.a(10, (long) znVar.k);
            miVar.a(11, (long) fo.a(znVar.l));
            miVar.a(12, znVar.m);
            miVar.a(13, znVar.n);
            miVar.a(14, znVar.o);
            miVar.a(15, znVar.p);
            nl nlVar = znVar.j;
            if (nlVar != null) {
                miVar.a(16, (long) fo.a(nlVar.b()));
                miVar.a(17, nlVar.g() ? 1 : 0);
                miVar.a(18, nlVar.h() ? 1 : 0);
                miVar.a(19, nlVar.f() ? 1 : 0);
                miVar.a(20, nlVar.i() ? 1 : 0);
                miVar.a(21, nlVar.c());
                miVar.a(22, nlVar.d());
                byte[] a3 = fo.a(nlVar.a());
                if (a3 == null) {
                    miVar.a(23);
                } else {
                    miVar.a(23, a3);
                }
            } else {
                miVar.a(16);
                miVar.a(17);
                miVar.a(18);
                miVar.a(19);
                miVar.a(20);
                miVar.a(21);
                miVar.a(22);
                miVar.a(23);
            }
        }

        @DexIgnore
        public String createQuery() {
            return "INSERT OR IGNORE INTO `WorkSpec`(`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`period_start_time`,`minimum_retention_duration`,`schedule_requested_at`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends vh {
        @DexIgnore
        public b(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c extends vh {
        @DexIgnore
        public c(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET output=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d extends vh {
        @DexIgnore
        public d(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET period_start_time=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e extends vh {
        @DexIgnore
        public e(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=run_attempt_count+1 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class f extends vh {
        @DexIgnore
        public f(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET run_attempt_count=0 WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class g extends vh {
        @DexIgnore
        public g(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=? WHERE id=?";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class h extends vh {
        @DexIgnore
        public h(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "UPDATE workspec SET schedule_requested_at=-1 WHERE state NOT IN (2, 3, 5)";
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class i extends vh {
        @DexIgnore
        public i(bo boVar, oh ohVar) {
            super(ohVar);
        }

        @DexIgnore
        public String createQuery() {
            return "DELETE FROM workspec WHERE state IN (2, 3, 5) AND (SELECT COUNT(*)=0 FROM dependency WHERE     prerequisite_id=id AND     work_spec_id NOT IN         (SELECT id FROM workspec WHERE state IN (2, 3, 5)))";
        }
    }

    @DexIgnore
    public bo(oh ohVar) {
        this.a = ohVar;
        this.b = new a(this, ohVar);
        this.c = new b(this, ohVar);
        this.d = new c(this, ohVar);
        this.e = new d(this, ohVar);
        this.f = new e(this, ohVar);
        this.g = new f(this, ohVar);
        this.h = new g(this, ohVar);
        this.i = new h(this, ohVar);
        new i(this, ohVar);
    }

    @DexIgnore
    public void a(zn znVar) {
        this.a.assertNotSuspendingTransaction();
        this.a.beginTransaction();
        try {
            this.b.insert(znVar);
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
        }
    }

    @DexIgnore
    public void b(String str) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.c.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            acquire.s();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.c.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c(String str) {
        rh b2 = rh.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5) AND id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int d() {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.i.acquire();
        this.a.beginTransaction();
        try {
            int s = acquire.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
            this.i.release(acquire);
        }
    }

    @DexIgnore
    public zn e(String str) {
        rh rhVar;
        zn znVar;
        String str2 = str;
        rh b2 = rh.b("SELECT * FROM workspec WHERE id=?", 1);
        if (str2 == null) {
            b2.a(1);
        } else {
            b2.a(1, str2);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, ServerProtocol.DIALOG_PARAM_STATE);
            int b5 = ai.b(a2, "worker_class_name");
            int b6 = ai.b(a2, "input_merger_class_name");
            int b7 = ai.b(a2, "input");
            int b8 = ai.b(a2, "output");
            int b9 = ai.b(a2, "initial_delay");
            int b10 = ai.b(a2, "interval_duration");
            int b11 = ai.b(a2, "flex_duration");
            int b12 = ai.b(a2, "run_attempt_count");
            int b13 = ai.b(a2, "backoff_policy");
            int b14 = ai.b(a2, "backoff_delay_duration");
            int b15 = ai.b(a2, "period_start_time");
            int b16 = ai.b(a2, "minimum_retention_duration");
            rhVar = b2;
            try {
                int b17 = ai.b(a2, "schedule_requested_at");
                int b18 = ai.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ai.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ai.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ai.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ai.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ai.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ai.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ai.b(a2, "content_uri_triggers");
                if (a2.moveToFirst()) {
                    String string = a2.getString(b3);
                    String string2 = a2.getString(b5);
                    int i9 = b9;
                    nl nlVar = new nl();
                    nlVar.a(fo.b(a2.getInt(b18)));
                    nlVar.b(a2.getInt(b19) != 0);
                    nlVar.c(a2.getInt(b20) != 0);
                    nlVar.a(a2.getInt(b21) != 0);
                    nlVar.d(a2.getInt(b22) != 0);
                    nlVar.a(a2.getLong(b23));
                    nlVar.b(a2.getLong(b24));
                    nlVar.a(fo.a(a2.getBlob(b25)));
                    znVar = new zn(string, string2);
                    znVar.b = fo.c(a2.getInt(b4));
                    znVar.d = a2.getString(b6);
                    znVar.e = pl.b(a2.getBlob(b7));
                    znVar.f = pl.b(a2.getBlob(b8));
                    znVar.g = a2.getLong(i9);
                    znVar.h = a2.getLong(i8);
                    znVar.i = a2.getLong(i7);
                    znVar.k = a2.getInt(i6);
                    znVar.l = fo.a(a2.getInt(i5));
                    znVar.m = a2.getLong(i4);
                    znVar.n = a2.getLong(i3);
                    znVar.o = a2.getLong(i2);
                    znVar.p = a2.getLong(b17);
                    znVar.j = nlVar;
                } else {
                    znVar = null;
                }
                a2.close();
                rhVar.c();
                return znVar;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b2;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int f(String str) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.g.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int s = acquire.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
            this.g.release(acquire);
        }
    }

    @DexIgnore
    public List<pl> g(String str) {
        rh b2 = rh.b("SELECT output FROM workspec WHERE id IN (SELECT prerequisite_id FROM dependency WHERE work_spec_id=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(pl.b(a2.getBlob(0)));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int h(String str) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.f.acquire();
        if (str == null) {
            acquire.a(1);
        } else {
            acquire.a(1, str);
        }
        this.a.beginTransaction();
        try {
            int s = acquire.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
            this.f.release(acquire);
        }
    }

    @DexIgnore
    public void a(String str, pl plVar) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.d.acquire();
        byte[] a2 = pl.a(plVar);
        if (a2 == null) {
            acquire.a(1);
        } else {
            acquire.a(1, a2);
        }
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.s();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.d.release(acquire);
        }
    }

    @DexIgnore
    public am.a d(String str) {
        rh b2 = rh.b("SELECT state FROM workspec WHERE id=?", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            return a2.moveToFirst() ? fo.c(a2.getInt(0)) : null;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public void b(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.e.acquire();
        acquire.a(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            acquire.s();
            this.a.setTransactionSuccessful();
        } finally {
            this.a.endTransaction();
            this.e.release(acquire);
        }
    }

    @DexIgnore
    public List<String> c() {
        rh b2 = rh.b("SELECT id FROM workspec WHERE state NOT IN (2, 3, 5)", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                arrayList.add(a2.getString(0));
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public int a(String str, long j) {
        this.a.assertNotSuspendingTransaction();
        mi acquire = this.h.acquire();
        acquire.a(1, j);
        if (str == null) {
            acquire.a(2);
        } else {
            acquire.a(2, str);
        }
        this.a.beginTransaction();
        try {
            int s = acquire.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
            this.h.release(acquire);
        }
    }

    @DexIgnore
    public List<zn> b() {
        rh rhVar;
        rh b2 = rh.b("SELECT * FROM workspec WHERE state=1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, ServerProtocol.DIALOG_PARAM_STATE);
            int b5 = ai.b(a2, "worker_class_name");
            int b6 = ai.b(a2, "input_merger_class_name");
            int b7 = ai.b(a2, "input");
            int b8 = ai.b(a2, "output");
            int b9 = ai.b(a2, "initial_delay");
            int b10 = ai.b(a2, "interval_duration");
            int b11 = ai.b(a2, "flex_duration");
            int b12 = ai.b(a2, "run_attempt_count");
            int b13 = ai.b(a2, "backoff_policy");
            int b14 = ai.b(a2, "backoff_delay_duration");
            int b15 = ai.b(a2, "period_start_time");
            int b16 = ai.b(a2, "minimum_retention_duration");
            rhVar = b2;
            try {
                int b17 = ai.b(a2, "schedule_requested_at");
                int b18 = ai.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ai.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ai.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ai.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ai.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ai.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ai.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ai.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    nl nlVar = new nl();
                    int i13 = b18;
                    nlVar.a(fo.b(a2.getInt(b18)));
                    nlVar.b(a2.getInt(b19) != 0);
                    nlVar.c(a2.getInt(b20) != 0);
                    nlVar.a(a2.getInt(b21) != 0);
                    nlVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    nlVar.a(a2.getLong(b23));
                    nlVar.b(a2.getLong(b24));
                    nlVar.a(fo.a(a2.getBlob(b25)));
                    zn znVar = new zn(string, string2);
                    znVar.b = fo.c(a2.getInt(b4));
                    znVar.d = a2.getString(b6);
                    znVar.e = pl.b(a2.getBlob(b7));
                    int i16 = i10;
                    znVar.f = pl.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    znVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    znVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    znVar.i = a2.getLong(i20);
                    int i21 = i6;
                    znVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    znVar.l = fo.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    znVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    znVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    znVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    znVar.p = a2.getLong(i29);
                    znVar.j = nlVar;
                    arrayList.add(znVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b2;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<zn.b> a(String str) {
        rh b2 = rh.b("SELECT id, state FROM workspec WHERE id IN (SELECT work_spec_id FROM workname WHERE name=?)", 1);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, ServerProtocol.DIALOG_PARAM_STATE);
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                zn.b bVar = new zn.b();
                bVar.a = a2.getString(b3);
                bVar.b = fo.c(a2.getInt(b4));
                arrayList.add(bVar);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.c();
        }
    }

    @DexIgnore
    public List<zn> a(int i2) {
        rh rhVar;
        rh b2 = rh.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at=-1 LIMIT (SELECT MAX(?-COUNT(*), 0) FROM workspec WHERE schedule_requested_at<>-1 AND state NOT IN (2, 3, 5))", 1);
        b2.a(1, (long) i2);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, ServerProtocol.DIALOG_PARAM_STATE);
            int b5 = ai.b(a2, "worker_class_name");
            int b6 = ai.b(a2, "input_merger_class_name");
            int b7 = ai.b(a2, "input");
            int b8 = ai.b(a2, "output");
            int b9 = ai.b(a2, "initial_delay");
            int b10 = ai.b(a2, "interval_duration");
            int b11 = ai.b(a2, "flex_duration");
            int b12 = ai.b(a2, "run_attempt_count");
            int b13 = ai.b(a2, "backoff_policy");
            int b14 = ai.b(a2, "backoff_delay_duration");
            int b15 = ai.b(a2, "period_start_time");
            int b16 = ai.b(a2, "minimum_retention_duration");
            rhVar = b2;
            try {
                int b17 = ai.b(a2, "schedule_requested_at");
                int b18 = ai.b(a2, "required_network_type");
                int i3 = b16;
                int b19 = ai.b(a2, "requires_charging");
                int i4 = b15;
                int b20 = ai.b(a2, "requires_device_idle");
                int i5 = b14;
                int b21 = ai.b(a2, "requires_battery_not_low");
                int i6 = b13;
                int b22 = ai.b(a2, "requires_storage_not_low");
                int i7 = b12;
                int b23 = ai.b(a2, "trigger_content_update_delay");
                int i8 = b11;
                int b24 = ai.b(a2, "trigger_max_content_delay");
                int i9 = b10;
                int b25 = ai.b(a2, "content_uri_triggers");
                int i10 = b9;
                int i11 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i12 = b3;
                    String string2 = a2.getString(b5);
                    int i13 = b5;
                    nl nlVar = new nl();
                    int i14 = b18;
                    nlVar.a(fo.b(a2.getInt(b18)));
                    nlVar.b(a2.getInt(b19) != 0);
                    nlVar.c(a2.getInt(b20) != 0);
                    nlVar.a(a2.getInt(b21) != 0);
                    nlVar.d(a2.getInt(b22) != 0);
                    int i15 = b21;
                    int i16 = b19;
                    nlVar.a(a2.getLong(b23));
                    nlVar.b(a2.getLong(b24));
                    nlVar.a(fo.a(a2.getBlob(b25)));
                    zn znVar = new zn(string, string2);
                    znVar.b = fo.c(a2.getInt(b4));
                    znVar.d = a2.getString(b6);
                    znVar.e = pl.b(a2.getBlob(b7));
                    int i17 = i11;
                    znVar.f = pl.b(a2.getBlob(i17));
                    i11 = i17;
                    int i18 = i16;
                    int i19 = i10;
                    znVar.g = a2.getLong(i19);
                    i10 = i19;
                    int i20 = i9;
                    znVar.h = a2.getLong(i20);
                    i9 = i20;
                    int i21 = b20;
                    int i22 = i8;
                    znVar.i = a2.getLong(i22);
                    int i23 = i7;
                    znVar.k = a2.getInt(i23);
                    int i24 = i6;
                    i7 = i23;
                    znVar.l = fo.a(a2.getInt(i24));
                    i8 = i22;
                    int i25 = i5;
                    int i26 = i21;
                    znVar.m = a2.getLong(i25);
                    int i27 = i25;
                    i6 = i24;
                    int i28 = i4;
                    znVar.n = a2.getLong(i28);
                    i4 = i28;
                    int i29 = i3;
                    znVar.o = a2.getLong(i29);
                    i3 = i29;
                    int i30 = i27;
                    int i31 = b17;
                    znVar.p = a2.getLong(i31);
                    znVar.j = nlVar;
                    arrayList.add(znVar);
                    b17 = i31;
                    b19 = i18;
                    b20 = i26;
                    b5 = i13;
                    b21 = i15;
                    b18 = i14;
                    i5 = i30;
                    b3 = i12;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b2;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public List<zn> a() {
        rh rhVar;
        rh b2 = rh.b("SELECT * FROM workspec WHERE state=0 AND schedule_requested_at<>-1", 0);
        this.a.assertNotSuspendingTransaction();
        Cursor a2 = bi.a(this.a, b2, false);
        try {
            int b3 = ai.b(a2, "id");
            int b4 = ai.b(a2, ServerProtocol.DIALOG_PARAM_STATE);
            int b5 = ai.b(a2, "worker_class_name");
            int b6 = ai.b(a2, "input_merger_class_name");
            int b7 = ai.b(a2, "input");
            int b8 = ai.b(a2, "output");
            int b9 = ai.b(a2, "initial_delay");
            int b10 = ai.b(a2, "interval_duration");
            int b11 = ai.b(a2, "flex_duration");
            int b12 = ai.b(a2, "run_attempt_count");
            int b13 = ai.b(a2, "backoff_policy");
            int b14 = ai.b(a2, "backoff_delay_duration");
            int b15 = ai.b(a2, "period_start_time");
            int b16 = ai.b(a2, "minimum_retention_duration");
            rhVar = b2;
            try {
                int b17 = ai.b(a2, "schedule_requested_at");
                int b18 = ai.b(a2, "required_network_type");
                int i2 = b16;
                int b19 = ai.b(a2, "requires_charging");
                int i3 = b15;
                int b20 = ai.b(a2, "requires_device_idle");
                int i4 = b14;
                int b21 = ai.b(a2, "requires_battery_not_low");
                int i5 = b13;
                int b22 = ai.b(a2, "requires_storage_not_low");
                int i6 = b12;
                int b23 = ai.b(a2, "trigger_content_update_delay");
                int i7 = b11;
                int b24 = ai.b(a2, "trigger_max_content_delay");
                int i8 = b10;
                int b25 = ai.b(a2, "content_uri_triggers");
                int i9 = b9;
                int i10 = b8;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    String string = a2.getString(b3);
                    int i11 = b3;
                    String string2 = a2.getString(b5);
                    int i12 = b5;
                    nl nlVar = new nl();
                    int i13 = b18;
                    nlVar.a(fo.b(a2.getInt(b18)));
                    nlVar.b(a2.getInt(b19) != 0);
                    nlVar.c(a2.getInt(b20) != 0);
                    nlVar.a(a2.getInt(b21) != 0);
                    nlVar.d(a2.getInt(b22) != 0);
                    int i14 = b20;
                    int i15 = b19;
                    nlVar.a(a2.getLong(b23));
                    nlVar.b(a2.getLong(b24));
                    nlVar.a(fo.a(a2.getBlob(b25)));
                    zn znVar = new zn(string, string2);
                    znVar.b = fo.c(a2.getInt(b4));
                    znVar.d = a2.getString(b6);
                    znVar.e = pl.b(a2.getBlob(b7));
                    int i16 = i10;
                    znVar.f = pl.b(a2.getBlob(i16));
                    i10 = i16;
                    int i17 = i15;
                    int i18 = i9;
                    znVar.g = a2.getLong(i18);
                    i9 = i18;
                    int i19 = i8;
                    znVar.h = a2.getLong(i19);
                    i8 = i19;
                    int i20 = i7;
                    znVar.i = a2.getLong(i20);
                    int i21 = i6;
                    znVar.k = a2.getInt(i21);
                    int i22 = i5;
                    i6 = i21;
                    znVar.l = fo.a(a2.getInt(i22));
                    i7 = i20;
                    int i23 = i4;
                    int i24 = b4;
                    znVar.m = a2.getLong(i23);
                    int i25 = i23;
                    i5 = i22;
                    int i26 = i3;
                    znVar.n = a2.getLong(i26);
                    i3 = i26;
                    int i27 = i2;
                    znVar.o = a2.getLong(i27);
                    i2 = i27;
                    int i28 = i25;
                    int i29 = b17;
                    znVar.p = a2.getLong(i29);
                    znVar.j = nlVar;
                    arrayList.add(znVar);
                    b17 = i29;
                    b19 = i17;
                    b3 = i11;
                    b5 = i12;
                    b20 = i14;
                    b18 = i13;
                    int i30 = i24;
                    i4 = i28;
                    b4 = i30;
                }
                a2.close();
                rhVar.c();
                return arrayList;
            } catch (Throwable th) {
                th = th;
                a2.close();
                rhVar.c();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            rhVar = b2;
            a2.close();
            rhVar.c();
            throw th;
        }
    }

    @DexIgnore
    public int a(am.a aVar, String... strArr) {
        this.a.assertNotSuspendingTransaction();
        StringBuilder a2 = ei.a();
        a2.append("UPDATE workspec SET state=");
        a2.append("?");
        a2.append(" WHERE id IN (");
        ei.a(a2, strArr.length);
        a2.append(")");
        mi compileStatement = this.a.compileStatement(a2.toString());
        compileStatement.a(1, (long) fo.a(aVar));
        int i2 = 2;
        for (String str : strArr) {
            if (str == null) {
                compileStatement.a(i2);
            } else {
                compileStatement.a(i2, str);
            }
            i2++;
        }
        this.a.beginTransaction();
        try {
            int s = compileStatement.s();
            this.a.setTransactionSuccessful();
            return s;
        } finally {
            this.a.endTransaction();
        }
    }
}
