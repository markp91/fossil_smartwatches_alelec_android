package com.fossil;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ov1 {
    @DexIgnore
    public static ov1 b;
    @DexIgnore
    public /* final */ Context a;

    @DexIgnore
    public ov1(Context context) {
        this.a = context.getApplicationContext();
    }

    @DexIgnore
    public static ov1 a(Context context) {
        w12.a(context);
        synchronized (ov1.class) {
            if (b == null) {
                j52.a(context);
                b = new ov1(context);
            }
        }
        return b;
    }

    @DexIgnore
    public boolean a(int i) {
        t52 t52;
        String[] a2 = g52.b(this.a).a(i);
        if (a2 != null && a2.length != 0) {
            t52 = null;
            for (String a3 : a2) {
                t52 = a(a3, i);
                if (t52.a) {
                    break;
                }
            }
        } else {
            t52 = t52.a("no pkgs");
        }
        t52.b();
        return t52.a;
    }

    @DexIgnore
    public static boolean a(PackageInfo packageInfo, boolean z) {
        l52 l52;
        if (!(packageInfo == null || packageInfo.signatures == null)) {
            if (z) {
                l52 = a(packageInfo, o52.a);
            } else {
                l52 = a(packageInfo, o52.a[0]);
            }
            if (l52 != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public boolean a(PackageInfo packageInfo) {
        if (packageInfo == null) {
            return false;
        }
        if (a(packageInfo, false)) {
            return true;
        }
        if (a(packageInfo, true)) {
            if (nv1.honorsDebugCertificates(this.a)) {
                return true;
            }
            Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
        }
        return false;
    }

    @DexIgnore
    public final t52 a(String str, int i) {
        try {
            PackageInfo a2 = g52.b(this.a).a(str, 64, i);
            boolean honorsDebugCertificates = nv1.honorsDebugCertificates(this.a);
            if (a2 == null) {
                return t52.a("null pkg");
            }
            if (a2.signatures.length != 1) {
                return t52.a("single cert required");
            }
            m52 m52 = new m52(a2.signatures[0].toByteArray());
            String str2 = a2.packageName;
            t52 a3 = j52.a(str2, m52, honorsDebugCertificates, false);
            return (!a3.a || a2.applicationInfo == null || (a2.applicationInfo.flags & 2) == 0 || !j52.a(str2, m52, false, true).a) ? a3 : t52.a("debuggable release cert app rejected");
        } catch (PackageManager.NameNotFoundException unused) {
            String valueOf = String.valueOf(str);
            return t52.a(valueOf.length() != 0 ? "no pkg ".concat(valueOf) : new String("no pkg "));
        }
    }

    @DexIgnore
    public static l52 a(PackageInfo packageInfo, l52... l52Arr) {
        Signature[] signatureArr = packageInfo.signatures;
        if (signatureArr == null) {
            return null;
        }
        if (signatureArr.length != 1) {
            Log.w("GoogleSignatureVerifier", "Package has more than one signature.");
            return null;
        }
        m52 m52 = new m52(signatureArr[0].toByteArray());
        for (int i = 0; i < l52Arr.length; i++) {
            if (l52Arr[i].equals(m52)) {
                return l52Arr[i];
            }
        }
        return null;
    }
}
