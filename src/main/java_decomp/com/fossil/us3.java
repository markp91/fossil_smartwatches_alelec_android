package com.fossil;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.util.SparseArray;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class us3 implements ServiceConnection {
    @DexIgnore
    public int a;
    @DexIgnore
    public /* final */ Messenger b;
    @DexIgnore
    public ar3 c;
    @DexIgnore
    public /* final */ Queue<cr3<?>> d;
    @DexIgnore
    public /* final */ SparseArray<cr3<?>> e;
    @DexIgnore
    public /* final */ /* synthetic */ ts3 f;

    @DexIgnore
    public us3(ts3 ts3) {
        this.f = ts3;
        this.a = 0;
        this.b = new Messenger(new kb2(Looper.getMainLooper(), new xs3(this)));
        this.d = new ArrayDeque();
        this.e = new SparseArray<>();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002f, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0096, code lost:
        return true;
     */
    @DexIgnore
    public final synchronized boolean a(cr3<?> cr3) {
        int i = this.a;
        if (i == 0) {
            this.d.add(cr3);
            w12.b(this.a == 0);
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Starting bind to GmsCore");
            }
            this.a = 1;
            Intent intent = new Intent("com.google.android.c2dm.intent.REGISTER");
            intent.setPackage("com.google.android.gms");
            if (!b42.a().a(this.f.a, intent, this, 1)) {
                a(0, "Unable to bind to service");
            } else {
                this.f.b.schedule(new ws3(this), 30, TimeUnit.SECONDS);
            }
        } else if (i == 1) {
            this.d.add(cr3);
            return true;
        } else if (i == 2) {
            this.d.add(cr3);
            a();
            return true;
        } else if (i != 3) {
            if (i != 4) {
                int i2 = this.a;
                StringBuilder sb = new StringBuilder(26);
                sb.append("Unknown state: ");
                sb.append(i2);
                throw new IllegalStateException(sb.toString());
            }
        }
    }

    @DexIgnore
    public final synchronized void b() {
        if (this.a == 2 && this.d.isEmpty() && this.e.size() == 0) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Finished handling requests, unbinding");
            }
            this.a = 3;
            b42.a().a(this.f.a, this);
        }
    }

    @DexIgnore
    public final synchronized void c() {
        if (this.a == 1) {
            a(1, "Timed out while binding");
        }
    }

    @DexIgnore
    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service connected");
        }
        this.f.b.execute(new wq3(this, iBinder));
    }

    @DexIgnore
    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("MessengerIpcClient", 2)) {
            Log.v("MessengerIpcClient", "Service disconnected");
        }
        this.f.b.execute(new yq3(this));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0052, code lost:
        r5 = r5.getData();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x005d, code lost:
        if (r5.getBoolean("unsupported", false) == false) goto L_0x006b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005f, code lost:
        r1.a(new com.fossil.br3(4, "Not supported by GmsCore"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006b, code lost:
        r1.a(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        return true;
     */
    @DexIgnore
    public final boolean a(Message message) {
        int i = message.arg1;
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            StringBuilder sb = new StringBuilder(41);
            sb.append("Received response to request: ");
            sb.append(i);
            Log.d("MessengerIpcClient", sb.toString());
        }
        synchronized (this) {
            cr3 cr3 = this.e.get(i);
            if (cr3 == null) {
                StringBuilder sb2 = new StringBuilder(50);
                sb2.append("Received response for unknown request: ");
                sb2.append(i);
                Log.w("MessengerIpcClient", sb2.toString());
                return true;
            }
            this.e.remove(i);
            b();
        }
    }

    @DexIgnore
    public final void a() {
        this.f.b.execute(new vq3(this));
    }

    @DexIgnore
    public final synchronized void a(int i, String str) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(str);
            Log.d("MessengerIpcClient", valueOf.length() != 0 ? "Disconnected: ".concat(valueOf) : new String("Disconnected: "));
        }
        int i2 = this.a;
        if (i2 == 0) {
            throw new IllegalStateException();
        } else if (i2 == 1 || i2 == 2) {
            if (Log.isLoggable("MessengerIpcClient", 2)) {
                Log.v("MessengerIpcClient", "Unbinding service");
            }
            this.a = 4;
            b42.a().a(this.f.a, this);
            br3 br3 = new br3(i, str);
            for (cr3 a2 : this.d) {
                a2.a(br3);
            }
            this.d.clear();
            for (int i3 = 0; i3 < this.e.size(); i3++) {
                this.e.valueAt(i3).a(br3);
            }
            this.e.clear();
        } else if (i2 == 3) {
            this.a = 4;
        } else if (i2 != 4) {
            int i4 = this.a;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Unknown state: ");
            sb.append(i4);
            throw new IllegalStateException(sb.toString());
        }
    }

    @DexIgnore
    public final synchronized void a(int i) {
        cr3 cr3 = this.e.get(i);
        if (cr3 != null) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Timing out request: ");
            sb.append(i);
            Log.w("MessengerIpcClient", sb.toString());
            this.e.remove(i);
            cr3.a(new br3(3, "Timed out waiting for response"));
            b();
        }
    }
}
