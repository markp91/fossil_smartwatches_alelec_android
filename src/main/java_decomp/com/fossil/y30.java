package com.fossil;

import java.io.File;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface y30 {

    @DexIgnore
    public enum a {
        JAVA,
        NATIVE
    }

    @DexIgnore
    Map<String, String> a();

    @DexIgnore
    String b();

    @DexIgnore
    File c();

    @DexIgnore
    File[] d();

    @DexIgnore
    String e();

    @DexIgnore
    a getType();

    @DexIgnore
    void remove();
}
