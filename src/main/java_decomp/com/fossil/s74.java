package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class s74 extends r74 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public /* final */ NestedScrollView u;
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362021, 1);
        x.put(2131363106, 2);
        x.put(2131363291, 3);
        x.put(2131362576, 4);
        x.put(2131362149, 5);
        x.put(2131362206, 6);
    }
    */

    @DexIgnore
    public s74(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public s74(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1], objArr[5], objArr[6], objArr[4], objArr[2], objArr[3]);
        this.v = -1;
        this.u = objArr[0];
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
