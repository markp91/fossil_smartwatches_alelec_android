package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.x52;
import com.google.android.gms.maps.model.LatLng;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dy2 extends jh2 implements qx2 {
    @DexIgnore
    public dy2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    @DexIgnore
    public final x52 a(float f) throws RemoteException {
        Parcel zza = zza();
        zza.writeFloat(f);
        Parcel a = a(4, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final x52 a(LatLng latLng, float f) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) latLng);
        zza.writeFloat(f);
        Parcel a = a(9, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
