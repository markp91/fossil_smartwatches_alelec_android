package com.fossil;

import com.portfolio.platform.uirenew.home.customize.hybrid.edit.HybridCustomizeEditActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h95 implements MembersInjector<HybridCustomizeEditActivity> {
    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, o95 o95) {
        hybridCustomizeEditActivity.B = o95;
    }

    @DexIgnore
    public static void a(HybridCustomizeEditActivity hybridCustomizeEditActivity, w04 w04) {
        hybridCustomizeEditActivity.C = w04;
    }
}
