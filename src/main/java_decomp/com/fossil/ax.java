package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ax implements zr<Uri, Bitmap> {
    @DexIgnore
    public /* final */ kx a;
    @DexIgnore
    public /* final */ au b;

    @DexIgnore
    public ax(kx kxVar, au auVar) {
        this.a = kxVar;
        this.b = auVar;
    }

    @DexIgnore
    public boolean a(Uri uri, xr xrVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @DexIgnore
    public rt<Bitmap> a(Uri uri, int i, int i2, xr xrVar) {
        rt<Drawable> a2 = this.a.a(uri, i, i2, xrVar);
        if (a2 == null) {
            return null;
        }
        return qw.a(this.b, a2.get(), i, i2);
    }
}
