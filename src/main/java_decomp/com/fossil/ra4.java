package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.CustomizeWidget;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ra4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ CustomizeWidget A;
    @DexIgnore
    public /* final */ RTLImageView q;
    @DexIgnore
    public /* final */ RelativeLayout r;
    @DexIgnore
    public /* final */ RelativeLayout s;
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ View w;
    @DexIgnore
    public /* final */ View x;
    @DexIgnore
    public /* final */ CustomizeWidget y;
    @DexIgnore
    public /* final */ CustomizeWidget z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ra4(Object obj, View view, int i, RTLImageView rTLImageView, RelativeLayout relativeLayout, RelativeLayout relativeLayout2, RelativeLayout relativeLayout3, ConstraintLayout constraintLayout, RTLImageView rTLImageView2, RTLImageView rTLImageView3, RTLImageView rTLImageView4, LinearLayout linearLayout, RelativeLayout relativeLayout4, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, View view2, View view3, CustomizeWidget customizeWidget, CustomizeWidget customizeWidget2, CustomizeWidget customizeWidget3) {
        super(obj, view, i);
        this.q = rTLImageView;
        this.r = relativeLayout;
        this.s = relativeLayout2;
        this.t = relativeLayout4;
        this.u = flexibleTextView;
        this.v = flexibleTextView4;
        this.w = view2;
        this.x = view3;
        this.y = customizeWidget;
        this.z = customizeWidget2;
        this.A = customizeWidget3;
    }
}
