package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class p21 extends xk1 {
    @DexIgnore
    public /* final */ byte[] R;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ p21(ue1 ue1, q41 q41, eh1 eh1, boolean z, short s, byte[] bArr, float f, String str, int i) {
        super(ue1, q41, eh1, z, s, (r0 & 64) != 0 ? 0.001f : f, (r0 & 128) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
        int i2 = i;
        this.R = bArr;
    }

    @DexIgnore
    public byte[] n() {
        return this.R;
    }

    @DexIgnore
    public p21(ue1 ue1, q41 q41, eh1 eh1, boolean z, short s, byte[] bArr, float f, String str) {
        super(ue1, q41, eh1, z, s, f, str);
        this.R = bArr;
    }
}
