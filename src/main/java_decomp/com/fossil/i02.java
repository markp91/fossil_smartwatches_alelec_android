package com.fossil;

import android.util.Log;
import com.fossil.rv1;
import java.util.Collections;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class i02 implements kc3<Map<lw1<?>, String>> {
    @DexIgnore
    public /* final */ /* synthetic */ g02 a;

    @DexIgnore
    public i02(g02 g02) {
        this.a = g02;
    }

    @DexIgnore
    public final void onComplete(qc3<Map<lw1<?>, String>> qc3) {
        this.a.f.lock();
        try {
            if (this.a.r) {
                if (qc3.e()) {
                    Map unused = this.a.s = new p4(this.a.a.size());
                    for (h02 a2 : this.a.a.values()) {
                        this.a.s.put(a2.a(), gv1.e);
                    }
                } else if (qc3.a() instanceof tv1) {
                    tv1 tv1 = (tv1) qc3.a();
                    if (this.a.p) {
                        Map unused2 = this.a.s = new p4(this.a.a.size());
                        for (h02 h02 : this.a.a.values()) {
                            lw1 a3 = h02.a();
                            gv1 connectionResult = tv1.getConnectionResult((vv1<? extends rv1.d>) h02);
                            if (this.a.a((h02<?>) h02, connectionResult)) {
                                this.a.s.put(a3, new gv1(16));
                            } else {
                                this.a.s.put(a3, connectionResult);
                            }
                        }
                    } else {
                        Map unused3 = this.a.s = tv1.zaj();
                    }
                    gv1 unused4 = this.a.v = this.a.j();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", qc3.a());
                    Map unused5 = this.a.s = Collections.emptyMap();
                    gv1 unused6 = this.a.v = new gv1(8);
                }
                if (this.a.t != null) {
                    this.a.s.putAll(this.a.t);
                    gv1 unused7 = this.a.v = this.a.j();
                }
                if (this.a.v == null) {
                    this.a.h();
                    this.a.i();
                } else {
                    boolean unused8 = this.a.r = false;
                    this.a.e.a(this.a.v);
                }
                this.a.i.signalAll();
                this.a.f.unlock();
            }
        } finally {
            this.a.f.unlock();
        }
    }
}
