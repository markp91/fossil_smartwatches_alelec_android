package com.fossil;

import android.os.IInterface;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface r62 extends IInterface {
    @DexIgnore
    int a(x52 x52, String str, boolean z) throws RemoteException;

    @DexIgnore
    x52 a(x52 x52, String str, int i) throws RemoteException;

    @DexIgnore
    int b(x52 x52, String str, boolean z) throws RemoteException;

    @DexIgnore
    x52 b(x52 x52, String str, int i) throws RemoteException;

    @DexIgnore
    int p() throws RemoteException;
}
