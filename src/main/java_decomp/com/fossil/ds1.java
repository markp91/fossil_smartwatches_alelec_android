package com.fossil;

import android.database.sqlite.SQLiteDatabase;
import com.fossil.rs1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final /* synthetic */ class ds1 implements rs1.b {
    @DexIgnore
    public /* final */ long a;

    @DexIgnore
    public ds1(long j) {
        this.a = j;
    }

    @DexIgnore
    public static rs1.b a(long j) {
        return new ds1(j);
    }

    @DexIgnore
    public Object apply(Object obj) {
        return Integer.valueOf(((SQLiteDatabase) obj).delete("events", "timestamp_ms < ?", new String[]{String.valueOf(this.a)}));
    }
}
