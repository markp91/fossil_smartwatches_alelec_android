package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface k66 {
    @DexIgnore
    void d(String str, String str2);

    @DexIgnore
    void e(String str, String str2);

    @DexIgnore
    void e(String str, String str2, Throwable th);

    @DexIgnore
    void setLoggable(boolean z);

    @DexIgnore
    void w(String str, String str2);
}
