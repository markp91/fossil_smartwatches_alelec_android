package com.fossil;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class sr6 {
    @DexIgnore
    public /* final */ aq6 a;
    @DexIgnore
    public /* final */ qr6 b;
    @DexIgnore
    public /* final */ dq6 c;
    @DexIgnore
    public /* final */ pq6 d;
    @DexIgnore
    public List<Proxy> e; // = Collections.emptyList();
    @DexIgnore
    public int f;
    @DexIgnore
    public List<InetSocketAddress> g; // = Collections.emptyList();
    @DexIgnore
    public /* final */ List<ar6> h; // = new ArrayList();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ List<ar6> a;
        @DexIgnore
        public int b; // = 0;

        @DexIgnore
        public a(List<ar6> list) {
            this.a = list;
        }

        @DexIgnore
        public List<ar6> a() {
            return new ArrayList(this.a);
        }

        @DexIgnore
        public boolean b() {
            return this.b < this.a.size();
        }

        @DexIgnore
        public ar6 c() {
            if (b()) {
                List<ar6> list = this.a;
                int i = this.b;
                this.b = i + 1;
                return list.get(i);
            }
            throw new NoSuchElementException();
        }
    }

    @DexIgnore
    public sr6(aq6 aq6, qr6 qr6, dq6 dq6, pq6 pq6) {
        this.a = aq6;
        this.b = qr6;
        this.c = dq6;
        this.d = pq6;
        a(aq6.k(), aq6.f());
    }

    @DexIgnore
    public boolean a() {
        return b() || !this.h.isEmpty();
    }

    @DexIgnore
    public final boolean b() {
        return this.f < this.e.size();
    }

    @DexIgnore
    public a c() throws IOException {
        if (a()) {
            ArrayList arrayList = new ArrayList();
            while (b()) {
                Proxy d2 = d();
                int size = this.g.size();
                for (int i = 0; i < size; i++) {
                    ar6 ar6 = new ar6(this.a, d2, this.g.get(i));
                    if (this.b.c(ar6)) {
                        this.h.add(ar6);
                    } else {
                        arrayList.add(ar6);
                    }
                }
                if (!arrayList.isEmpty()) {
                    break;
                }
            }
            if (arrayList.isEmpty()) {
                arrayList.addAll(this.h);
                this.h.clear();
            }
            return new a(arrayList);
        }
        throw new NoSuchElementException();
    }

    @DexIgnore
    public final Proxy d() throws IOException {
        if (b()) {
            List<Proxy> list = this.e;
            int i = this.f;
            this.f = i + 1;
            Proxy proxy = list.get(i);
            a(proxy);
            return proxy;
        }
        throw new SocketException("No route to " + this.a.k().g() + "; exhausted proxy configurations: " + this.e);
    }

    @DexIgnore
    public void a(ar6 ar6, IOException iOException) {
        if (!(ar6.b().type() == Proxy.Type.DIRECT || this.a.h() == null)) {
            this.a.h().connectFailed(this.a.k().o(), ar6.b().address(), iOException);
        }
        this.b.b(ar6);
    }

    @DexIgnore
    public final void a(tq6 tq6, Proxy proxy) {
        List<Proxy> list;
        if (proxy != null) {
            this.e = Collections.singletonList(proxy);
        } else {
            List<Proxy> select = this.a.h().select(tq6.o());
            if (select == null || select.isEmpty()) {
                list = fr6.a((T[]) new Proxy[]{Proxy.NO_PROXY});
            } else {
                list = fr6.a(select);
            }
            this.e = list;
        }
        this.f = 0;
    }

    @DexIgnore
    public final void a(Proxy proxy) throws IOException {
        String str;
        int i;
        this.g = new ArrayList();
        if (proxy.type() == Proxy.Type.DIRECT || proxy.type() == Proxy.Type.SOCKS) {
            str = this.a.k().g();
            i = this.a.k().k();
        } else {
            SocketAddress address = proxy.address();
            if (address instanceof InetSocketAddress) {
                InetSocketAddress inetSocketAddress = (InetSocketAddress) address;
                str = a(inetSocketAddress);
                i = inetSocketAddress.getPort();
            } else {
                throw new IllegalArgumentException("Proxy.address() is not an InetSocketAddress: " + address.getClass());
            }
        }
        if (i < 1 || i > 65535) {
            throw new SocketException("No route to " + str + ":" + i + "; port is out of range");
        } else if (proxy.type() == Proxy.Type.SOCKS) {
            this.g.add(InetSocketAddress.createUnresolved(str, i));
        } else {
            this.d.a(this.c, str);
            List<InetAddress> a2 = this.a.c().a(str);
            if (!a2.isEmpty()) {
                this.d.a(this.c, str, a2);
                int size = a2.size();
                for (int i2 = 0; i2 < size; i2++) {
                    this.g.add(new InetSocketAddress(a2.get(i2), i));
                }
                return;
            }
            throw new UnknownHostException(this.a.c() + " returned no addresses for " + str);
        }
    }

    @DexIgnore
    public static String a(InetSocketAddress inetSocketAddress) {
        InetAddress address = inetSocketAddress.getAddress();
        if (address == null) {
            return inetSocketAddress.getHostName();
        }
        return address.getHostAddress();
    }
}
