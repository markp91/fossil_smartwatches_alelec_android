package com.fossil;

import dagger.internal.Factory;
import java.util.concurrent.Executor;
import javax.inject.Provider;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qr1 implements Factory<pr1> {
    @DexIgnore
    public /* final */ Provider<Executor> a;
    @DexIgnore
    public /* final */ Provider<ur1> b;
    @DexIgnore
    public /* final */ Provider<rr1> c;
    @DexIgnore
    public /* final */ Provider<ys1> d;

    @DexIgnore
    public qr1(Provider<Executor> provider, Provider<ur1> provider2, Provider<rr1> provider3, Provider<ys1> provider4) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
        this.d = provider4;
    }

    @DexIgnore
    public static qr1 a(Provider<Executor> provider, Provider<ur1> provider2, Provider<rr1> provider3, Provider<ys1> provider4) {
        return new qr1(provider, provider2, provider3, provider4);
    }

    @DexIgnore
    public pr1 get() {
        return new pr1((Executor) this.a.get(), (ur1) this.b.get(), (rr1) this.c.get(), (ys1) this.d.get());
    }
}
