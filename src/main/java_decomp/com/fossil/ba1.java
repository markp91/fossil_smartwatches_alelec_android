package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ba1 extends xg6 implements ig6<qv0, Float, cd6> {
    @DexIgnore
    public /* final */ /* synthetic */ xk1 a;
    @DexIgnore
    public /* final */ /* synthetic */ long b;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ba1(xk1 xk1, long j) {
        super(2);
        this.a = xk1;
        this.b = j;
    }

    @DexIgnore
    public Object invoke(Object obj, Object obj2) {
        qv0 qv0 = (qv0) obj;
        xk1 xk1 = this.a;
        xk1.E = xk1.H + ((long) (((Number) obj2).floatValue() * ((float) this.b)));
        float o = (((float) xk1.E) * 1.0f) / ((float) xk1.o());
        if (Math.abs(o - this.a.F) > this.a.Q || o == 1.0f) {
            xk1 xk12 = this.a;
            xk12.F = o;
            xk12.a(o);
        }
        return cd6.a;
    }
}
