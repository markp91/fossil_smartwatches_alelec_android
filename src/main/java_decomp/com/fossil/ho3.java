package com.fossil;

import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ho3<F, T> implements Iterator<T> {
    @DexIgnore
    public /* final */ Iterator<? extends F> a;

    @DexIgnore
    public ho3(Iterator<? extends F> it) {
        jk3.a(it);
        this.a = it;
    }

    @DexIgnore
    public abstract T a(F f);

    @DexIgnore
    public final boolean hasNext() {
        return this.a.hasNext();
    }

    @DexIgnore
    public final T next() {
        return a(this.a.next());
    }

    @DexIgnore
    public final void remove() {
        this.a.remove();
    }
}
