package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sf4 extends rf4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j x; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray y; // = new SparseIntArray();
    @DexIgnore
    public long w;

    /*
    static {
        y.put(2131362536, 1);
        y.put(2131362690, 2);
        y.put(2131362298, 3);
        y.put(2131362302, 4);
        y.put(2131362983, 5);
        y.put(2131362653, 6);
    }
    */

    @DexIgnore
    public sf4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, x, y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.w = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.w != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.w = 1;
        }
        g();
    }

    @DexIgnore
    public sf4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[0], objArr[3], objArr[4], objArr[1], objArr[6], objArr[2], objArr[5]);
        this.w = -1;
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
