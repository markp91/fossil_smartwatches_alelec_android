package com.fossil;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.fossil.eq1;
import com.fossil.fq1;
import java.util.ArrayList;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class lr1 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ dq1 b;
    @DexIgnore
    public /* final */ ur1 c;
    @DexIgnore
    public /* final */ rr1 d;
    @DexIgnore
    public /* final */ Executor e;
    @DexIgnore
    public /* final */ ys1 f;
    @DexIgnore
    public /* final */ zs1 g;

    @DexIgnore
    public lr1(Context context, dq1 dq1, ur1 ur1, rr1 rr1, Executor executor, ys1 ys1, zs1 zs1) {
        this.a = context;
        this.b = dq1;
        this.c = ur1;
        this.d = rr1;
        this.e = executor;
        this.f = ys1;
        this.g = zs1;
    }

    @DexIgnore
    public boolean a() {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.a.getSystemService("connectivity")).getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @DexIgnore
    public void a(rp1 rp1, int i, Runnable runnable) {
        this.e.execute(gr1.a(this, rp1, i, runnable));
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:6|7) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002f, code lost:
        r5.run();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0032, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0022, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        r2.d.a(r3, r4 + 1);
     */
    @DexIgnore
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x0024 */
    public static /* synthetic */ void a(lr1 lr1, rp1 rp1, int i, Runnable runnable) {
        ys1 ys1 = lr1.f;
        ur1 ur1 = lr1.c;
        ur1.getClass();
        ys1.a(jr1.a(ur1));
        if (!lr1.a()) {
            lr1.f.a(kr1.a(lr1, rp1, i));
        } else {
            lr1.a(rp1, i);
        }
        runnable.run();
    }

    @DexIgnore
    public void a(rp1 rp1, int i) {
        fq1 fq1;
        lq1 a2 = this.b.a(rp1.a());
        Iterable<zr1> iterable = (Iterable) this.f.a(hr1.a(this, rp1));
        if (iterable.iterator().hasNext()) {
            if (a2 == null) {
                mq1.a("Uploader", "Unknown backend for %s, deleting event batch for it...", (Object) rp1);
                fq1 = fq1.c();
            } else {
                ArrayList arrayList = new ArrayList();
                for (zr1 a3 : iterable) {
                    arrayList.add(a3.a());
                }
                eq1.a c2 = eq1.c();
                c2.a((Iterable<np1>) arrayList);
                c2.a(rp1.b());
                fq1 = a2.a(c2.a());
            }
            this.f.a(ir1.a(this, fq1, iterable, rp1, i));
        }
    }

    @DexIgnore
    public static /* synthetic */ Object a(lr1 lr1, fq1 fq1, Iterable iterable, rp1 rp1, int i) {
        if (fq1.b() == fq1.a.TRANSIENT_ERROR) {
            lr1.c.b((Iterable<zr1>) iterable);
            lr1.d.a(rp1, i + 1);
            return null;
        }
        lr1.c.a((Iterable<zr1>) iterable);
        if (fq1.b() == fq1.a.OK) {
            lr1.c.a(rp1, lr1.g.a() + fq1.a());
        }
        if (!lr1.c.c(rp1)) {
            return null;
        }
        lr1.d.a(rp1, 1);
        return null;
    }
}
