package com.fossil;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cz {
    @DexIgnore
    public static /* final */ pt<?, ?, ?> c; // = new pt(Object.class, Object.class, Object.class, Collections.singletonList(new et(Object.class, Object.class, Object.class, Collections.emptyList(), new ey(), (v8<List<Throwable>>) null)), (v8<List<Throwable>>) null);
    @DexIgnore
    public /* final */ p4<p00, pt<?, ?, ?>> a; // = new p4<>();
    @DexIgnore
    public /* final */ AtomicReference<p00> b; // = new AtomicReference<>();

    @DexIgnore
    public boolean a(pt<?, ?, ?> ptVar) {
        return c.equals(ptVar);
    }

    @DexIgnore
    public final p00 b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        p00 andSet = this.b.getAndSet((Object) null);
        if (andSet == null) {
            andSet = new p00();
        }
        andSet.a(cls, cls2, cls3);
        return andSet;
    }

    @DexIgnore
    public <Data, TResource, Transcode> pt<Data, TResource, Transcode> a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        pt<Data, TResource, Transcode> ptVar;
        p00 b2 = b(cls, cls2, cls3);
        synchronized (this.a) {
            ptVar = this.a.get(b2);
        }
        this.b.set(b2);
        return ptVar;
    }

    @DexIgnore
    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, pt<?, ?, ?> ptVar) {
        synchronized (this.a) {
            p4<p00, pt<?, ?, ?>> p4Var = this.a;
            p00 p00 = new p00(cls, cls2, cls3);
            if (ptVar == null) {
                ptVar = c;
            }
            p4Var.put(p00, ptVar);
        }
    }
}
