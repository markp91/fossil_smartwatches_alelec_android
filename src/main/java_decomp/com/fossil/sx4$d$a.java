package com.fossil;

import com.fossil.mz4;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1", f = "HomeAlertsPresenter.kt", l = {351, 357, 366, 374}, m = "invokeSuspend")
public final class sx4$d$a extends sf6 implements ig6<il6, xe6<? super List<? extends AppNotificationFilter>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public Object L$2;
    @DexIgnore
    public Object L$3;
    @DexIgnore
    public Object L$4;
    @DexIgnore
    public Object L$5;
    @DexIgnore
    public Object L$6;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeAlertsPresenter.d this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $notificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sx4$d$a this$0;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.sx4$d$a$a$a")
        /* renamed from: com.fossil.sx4$d$a$a$a  reason: collision with other inner class name */
        public static final class C0037a implements Runnable {
            @DexIgnore
            public /* final */ /* synthetic */ a a;

            @DexIgnore
            public C0037a(a aVar) {
                this.a = aVar;
            }

            @DexIgnore
            public final void run() {
                this.a.this$0.this$0.this$0.r.getNotificationSettingsDao().insertListNotificationSettings(this.a.$notificationSettings);
            }
        }

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(sx4$d$a sx4_d_a, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sx4_d_a;
            this.$notificationSettings = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$notificationSettings, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.this$0.this$0.r.runInTransaction(new C0037a(this));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$2", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class b extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ jh6 $contactMessageAppFilters;
        @DexIgnore
        public /* final */ /* synthetic */ List $listNotificationSettings;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sx4$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(sx4$d$a sx4_d_a, List list, jh6 jh6, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sx4_d_a;
            this.$listNotificationSettings = list;
            this.$contactMessageAppFilters = jh6;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            b bVar = new b(this.this$0, this.$listNotificationSettings, this.$contactMessageAppFilters, xe6);
            bVar.p$ = (il6) obj;
            return bVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                for (NotificationSettingsModel notificationSettingsModel : this.$listNotificationSettings) {
                    int component2 = notificationSettingsModel.component2();
                    if (notificationSettingsModel.component3()) {
                        String a = this.this$0.this$0.this$0.a(component2);
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String a2 = HomeAlertsPresenter.x.a();
                        local.d(a2, "CALL settingsTypeName=" + a);
                        if (component2 == 0) {
                            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                            String a3 = HomeAlertsPresenter.x.a();
                            local2.d(a3, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL());
                            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                            ((List) this.$contactMessageAppFilters.element).add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
                        } else if (component2 == 1) {
                            int size = this.this$0.this$0.this$0.k.size();
                            for (int i = 0; i < size; i++) {
                                ContactGroup contactGroup = (ContactGroup) this.this$0.this$0.this$0.k.get(i);
                                ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                                String a4 = HomeAlertsPresenter.x.a();
                                StringBuilder sb = new StringBuilder();
                                sb.append("mListAppNotificationFilter add PHONE item - ");
                                sb.append(i);
                                sb.append(" name = ");
                                Object obj2 = contactGroup.getContacts().get(0);
                                wg6.a(obj2, "item.contacts[0]");
                                sb.append(((Contact) obj2).getDisplayName());
                                local3.d(a4, sb.toString());
                                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                                List contacts = contactGroup.getContacts();
                                wg6.a((Object) contacts, "item.contacts");
                                if (!contacts.isEmpty()) {
                                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                                    Object obj3 = contactGroup.getContacts().get(0);
                                    wg6.a(obj3, "item.contacts[0]");
                                    appNotificationFilter.setSender(((Contact) obj3).getDisplayName());
                                    ((List) this.$contactMessageAppFilters.element).add(appNotificationFilter);
                                }
                            }
                        }
                    } else {
                        String a5 = this.this$0.this$0.this$0.a(component2);
                        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
                        String a6 = HomeAlertsPresenter.x.a();
                        local4.d(a6, "MESSAGE settingsTypeName=" + a5);
                        if (component2 == 0) {
                            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
                            String a7 = HomeAlertsPresenter.x.a();
                            local5.d(a7, "mListAppNotificationFilter add - " + DianaNotificationObj.AApplicationName.Companion.getMESSAGES());
                            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                            ((List) this.$contactMessageAppFilters.element).add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
                        } else if (component2 == 1) {
                            int size2 = this.this$0.this$0.this$0.k.size();
                            for (int i2 = 0; i2 < size2; i2++) {
                                ContactGroup contactGroup2 = (ContactGroup) this.this$0.this$0.this$0.k.get(i2);
                                ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
                                String a8 = HomeAlertsPresenter.x.a();
                                StringBuilder sb2 = new StringBuilder();
                                sb2.append("mListAppNotificationFilter add MESSAGE item - ");
                                sb2.append(i2);
                                sb2.append(" name = ");
                                Object obj4 = contactGroup2.getContacts().get(0);
                                wg6.a(obj4, "item.contacts[0]");
                                sb2.append(((Contact) obj4).getDisplayName());
                                local6.d(a8, sb2.toString());
                                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                                List contacts2 = contactGroup2.getContacts();
                                wg6.a((Object) contacts2, "item.contacts");
                                if (!contacts2.isEmpty()) {
                                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                                    Object obj5 = contactGroup2.getContacts().get(0);
                                    wg6.a(obj5, "item.contacts[0]");
                                    appNotificationFilter2.setSender(((Contact) obj5).getDisplayName());
                                    ((List) this.$contactMessageAppFilters.element).add(appNotificationFilter2);
                                }
                            }
                        }
                    }
                }
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.alerts.diana.HomeAlertsPresenter$setNotificationFilterToDevice$1$contactAppDeffer$1$listNotificationSettings$1", f = "HomeAlertsPresenter.kt", l = {}, m = "invokeSuspend")
    public static final class c extends sf6 implements ig6<il6, xe6<? super List<? extends NotificationSettingsModel>>, Object> {
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ sx4$d$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(sx4$d$a sx4_d_a, xe6 xe6) {
            super(2, xe6);
            this.this$0 = sx4_d_a;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            c cVar = new c(this.this$0, xe6);
            cVar.p$ = (il6) obj;
            return cVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                return this.this$0.this$0.this$0.r.getNotificationSettingsDao().getListNotificationSettingsNoLiveData();
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public sx4$d$a(HomeAlertsPresenter.d dVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = dVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        sx4$d$a sx4_d_a = new sx4$d$a(this.this$0, xe6);
        sx4_d_a.p$ = (il6) obj;
        return sx4_d_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((sx4$d$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r7v4, types: [com.fossil.mz4, com.portfolio.platform.CoroutineUseCase] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00fd  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x013b  */
    public final Object invokeSuspend(Object obj) {
        jh6 jh6;
        il6 il6;
        CoroutineUseCase.c cVar;
        List list;
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il62 = this.p$;
            jh6 = new jh6();
            jh6.element = null;
            Object i2 = this.this$0.this$0.p;
            this.L$0 = il62;
            this.L$1 = jh6;
            this.label = 1;
            Object a3 = n24.a(i2, null, this);
            if (a3 == a2) {
                return a2;
            }
            Object obj2 = a3;
            il6 = il62;
            obj = obj2;
        } else if (i == 1) {
            jh6 = (jh6) this.L$1;
            il6 = (il6) this.L$0;
            nc6.a(obj);
        } else if (i != 2) {
            if (i == 3) {
                NotificationSettingsModel notificationSettingsModel = (NotificationSettingsModel) this.L$6;
                NotificationSettingsModel notificationSettingsModel2 = (NotificationSettingsModel) this.L$5;
                List list2 = (List) this.L$4;
            } else if (i != 4) {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            List list3 = (List) this.L$3;
            CoroutineUseCase.c cVar2 = (CoroutineUseCase.c) this.L$2;
            il6 il63 = (il6) this.L$0;
            nc6.a(obj);
            jh6 = (jh6) this.L$1;
            return jh6.element;
        } else {
            il6 = (il6) this.L$0;
            nc6.a(obj);
            jh6 jh62 = (jh6) this.L$1;
            cVar = (CoroutineUseCase.c) this.L$2;
            jh6 = jh62;
            list = (List) obj;
            if (!list.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                NotificationSettingsModel notificationSettingsModel3 = new NotificationSettingsModel("AllowCallsFrom", 0, true);
                NotificationSettingsModel notificationSettingsModel4 = new NotificationSettingsModel("AllowMessagesFrom", 0, false);
                arrayList.add(notificationSettingsModel3);
                arrayList.add(notificationSettingsModel4);
                dl6 c2 = this.this$0.this$0.c();
                a aVar = new a(this, arrayList, (xe6) null);
                this.L$0 = il6;
                this.L$1 = jh6;
                this.L$2 = cVar;
                this.L$3 = list;
                this.L$4 = arrayList;
                this.L$5 = notificationSettingsModel3;
                this.L$6 = notificationSettingsModel4;
                this.label = 3;
                if (gk6.a(c2, aVar, this) == a2) {
                    return a2;
                }
            } else {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String a4 = HomeAlertsPresenter.x.a();
                local.d(a4, "listNotificationSettings.size = " + list.size());
                dl6 a5 = this.this$0.this$0.b();
                b bVar = new b(this, list, jh6, (xe6) null);
                this.L$0 = il6;
                this.L$1 = jh6;
                this.L$2 = cVar;
                this.L$3 = list;
                this.label = 4;
                if (gk6.a(a5, bVar, this) == a2) {
                    return a2;
                }
            }
            return jh6.element;
        }
        CoroutineUseCase.c cVar3 = (CoroutineUseCase.c) obj;
        if (cVar3 instanceof mz4.d) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String a6 = HomeAlertsPresenter.x.a();
            StringBuilder sb = new StringBuilder();
            sb.append("GetAllContactGroup onSuccess, size = ");
            mz4.d dVar = (mz4.d) cVar3;
            sb.append(dVar.a().size());
            local2.d(a6, sb.toString());
            jh6.element = new ArrayList();
            this.this$0.this$0.k.clear();
            this.this$0.this$0.k = yd6.d(dVar.a());
            dl6 c3 = this.this$0.this$0.c();
            c cVar4 = new c(this, (xe6) null);
            this.L$0 = il6;
            this.L$1 = jh6;
            this.L$2 = cVar3;
            this.label = 2;
            Object a7 = gk6.a(c3, cVar4, this);
            if (a7 == a2) {
                return a2;
            }
            Object obj3 = a7;
            cVar = cVar3;
            obj = obj3;
            list = (List) obj;
            if (!list.isEmpty()) {
            }
            return jh6.element;
        }
        if (cVar3 instanceof mz4.b) {
            FLogger.INSTANCE.getLocal().d(HomeAlertsPresenter.x.a(), "GetAllContactGroup onError");
        }
        return jh6.element;
    }
}
