package com.fossil;

import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter$start$1$1", f = "UpdateFirmwarePresenter.kt", l = {}, m = "invokeSuspend")
public final class br5$f$a extends sf6 implements ig6<il6, xe6<? super Device>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ String $activeSerial;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ UpdateFirmwarePresenter.f this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public br5$f$a(UpdateFirmwarePresenter.f fVar, String str, xe6 xe6) {
        super(2, xe6);
        this.this$0 = fVar;
        this.$activeSerial = str;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        br5$f$a br5_f_a = new br5$f$a(this.this$0, this.$activeSerial, xe6);
        br5_f_a.p$ = (il6) obj;
        return br5_f_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((br5$f$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.n().getDeviceBySerial(this.$activeSerial);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
