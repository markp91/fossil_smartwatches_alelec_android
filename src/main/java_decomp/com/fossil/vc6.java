package com.fossil;

import java.util.Arrays;
import java.util.Collection;
import java.util.NoSuchElementException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class vc6 implements Collection<uc6>, ph6 {
    @DexIgnore
    public /* final */ int[] a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends pe6 {
        @DexIgnore
        public int a;
        @DexIgnore
        public /* final */ int[] b;

        @DexIgnore
        public a(int[] iArr) {
            wg6.b(iArr, "array");
            this.b = iArr;
        }

        @DexIgnore
        public int a() {
            int i = this.a;
            int[] iArr = this.b;
            if (i < iArr.length) {
                this.a = i + 1;
                int i2 = iArr[i];
                uc6.c(i2);
                return i2;
            }
            throw new NoSuchElementException(String.valueOf(i));
        }

        @DexIgnore
        public boolean hasNext() {
            return this.a < this.b.length;
        }
    }

    @DexIgnore
    public static boolean a(int[] iArr, Object obj) {
        return (obj instanceof vc6) && wg6.a((Object) iArr, (Object) ((vc6) obj).b());
    }

    @DexIgnore
    public static int b(int[] iArr) {
        if (iArr != null) {
            return Arrays.hashCode(iArr);
        }
        return 0;
    }

    @DexIgnore
    public static boolean c(int[] iArr) {
        return iArr.length == 0;
    }

    @DexIgnore
    public static pe6 d(int[] iArr) {
        return new a(iArr);
    }

    @DexIgnore
    public static String e(int[] iArr) {
        return "UIntArray(storage=" + Arrays.toString(iArr) + ")";
    }

    @DexIgnore
    public boolean a(int i) {
        return a(this.a, i);
    }

    @DexIgnore
    public /* synthetic */ boolean add(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean addAll(Collection<? extends uc6> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* synthetic */ int[] b() {
        return this.a;
    }

    @DexIgnore
    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ boolean contains(Object obj) {
        if (obj instanceof uc6) {
            return a(((uc6) obj).a());
        }
        return false;
    }

    @DexIgnore
    public boolean containsAll(Collection<? extends Object> collection) {
        return a(this.a, (Collection<uc6>) collection);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        return a(this.a, obj);
    }

    @DexIgnore
    public int hashCode() {
        return b(this.a);
    }

    @DexIgnore
    public boolean isEmpty() {
        return c(this.a);
    }

    @DexIgnore
    public pe6 iterator() {
        return d(this.a);
    }

    @DexIgnore
    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    @DexIgnore
    public final /* bridge */ int size() {
        return a();
    }

    @DexIgnore
    public Object[] toArray() {
        return pg6.a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        return pg6.a(this, tArr);
    }

    @DexIgnore
    public String toString() {
        return e(this.a);
    }

    @DexIgnore
    public int a() {
        return a(this.a);
    }

    @DexIgnore
    public static int a(int[] iArr) {
        return iArr.length;
    }

    @DexIgnore
    public static boolean a(int[] iArr, int i) {
        return nd6.a(iArr, i);
    }

    @DexIgnore
    public static boolean a(int[] iArr, Collection<uc6> collection) {
        boolean z;
        wg6.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        for (T next : collection) {
            if (!(next instanceof uc6) || !nd6.a(iArr, ((uc6) next).a())) {
                z = false;
                continue;
            } else {
                z = true;
                continue;
            }
            if (!z) {
                return false;
            }
        }
        return true;
    }
}
