package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mn1 implements Parcelable.Creator<si0> {
    @DexIgnore
    public /* synthetic */ mn1(qg6 qg6) {
    }

    @DexIgnore
    public si0 createFromParcel(Parcel parcel) {
        return new si0(parcel, (qg6) null);
    }

    @DexIgnore
    public Object[] newArray(int i) {
        return new si0[i];
    }

    @DexIgnore
    /* renamed from: createFromParcel  reason: collision with other method in class */
    public Object m44createFromParcel(Parcel parcel) {
        return new si0(parcel, (qg6) null);
    }
}
