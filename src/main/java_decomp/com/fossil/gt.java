package com.fossil;

import android.util.Log;
import com.fossil.dt;
import com.fossil.ku;
import com.fossil.lt;
import com.fossil.ru;
import com.fossil.s00;
import java.util.Map;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class gt implements it, ru.a, lt.a {
    @DexIgnore
    public static /* final */ boolean i; // = Log.isLoggable("Engine", 2);
    @DexIgnore
    public /* final */ ot a;
    @DexIgnore
    public /* final */ kt b;
    @DexIgnore
    public /* final */ ru c;
    @DexIgnore
    public /* final */ b d;
    @DexIgnore
    public /* final */ ut e;
    @DexIgnore
    public /* final */ c f;
    @DexIgnore
    public /* final */ a g;
    @DexIgnore
    public /* final */ ws h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public /* final */ dt.e a;
        @DexIgnore
        public /* final */ v8<dt<?>> b; // = s00.a(150, new C0018a());
        @DexIgnore
        public int c;

        @DexEdit(defaultAction = DexAction.IGNORE, target = "com.fossil.gt$a$a")
        /* renamed from: com.fossil.gt$a$a  reason: collision with other inner class name */
        public class C0018a implements s00.d<dt<?>> {
            @DexIgnore
            public C0018a() {
            }

            @DexIgnore
            public dt<?> create() {
                a aVar = a.this;
                return new dt<>(aVar.a, aVar.b);
            }
        }

        @DexIgnore
        public a(dt.e eVar) {
            this.a = eVar;
        }

        @DexIgnore
        public <R> dt<R> a(yq yqVar, Object obj, jt jtVar, vr vrVar, int i, int i2, Class<?> cls, Class<R> cls2, br brVar, ft ftVar, Map<Class<?>, bs<?>> map, boolean z, boolean z2, boolean z3, xr xrVar, dt.b<R> bVar) {
            dt<R> a2 = this.b.a();
            q00.a(a2);
            dt<R> dtVar = a2;
            int i3 = this.c;
            int i4 = i3;
            this.c = i3 + 1;
            dtVar.a(yqVar, obj, jtVar, vrVar, i, i2, cls, cls2, brVar, ftVar, map, z, z2, z3, xrVar, bVar, i4);
            return dtVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b {
        @DexIgnore
        public /* final */ uu a;
        @DexIgnore
        public /* final */ uu b;
        @DexIgnore
        public /* final */ uu c;
        @DexIgnore
        public /* final */ uu d;
        @DexIgnore
        public /* final */ it e;
        @DexIgnore
        public /* final */ lt.a f;
        @DexIgnore
        public /* final */ v8<ht<?>> g; // = s00.a(150, new a());

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public class a implements s00.d<ht<?>> {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public ht<?> create() {
                b bVar = b.this;
                return new ht(bVar.a, bVar.b, bVar.c, bVar.d, bVar.e, bVar.f, bVar.g);
            }
        }

        @DexIgnore
        public b(uu uuVar, uu uuVar2, uu uuVar3, uu uuVar4, it itVar, lt.a aVar) {
            this.a = uuVar;
            this.b = uuVar2;
            this.c = uuVar3;
            this.d = uuVar4;
            this.e = itVar;
            this.f = aVar;
        }

        @DexIgnore
        public <R> ht<R> a(vr vrVar, boolean z, boolean z2, boolean z3, boolean z4) {
            ht<R> a2 = this.g.a();
            q00.a(a2);
            ht<R> htVar = a2;
            htVar.a(vrVar, z, z2, z3, z4);
            return htVar;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c implements dt.e {
        @DexIgnore
        public /* final */ ku.a a;
        @DexIgnore
        public volatile ku b;

        @DexIgnore
        public c(ku.a aVar) {
            this.a = aVar;
        }

        @DexIgnore
        public ku a() {
            if (this.b == null) {
                synchronized (this) {
                    if (this.b == null) {
                        this.b = this.a.build();
                    }
                    if (this.b == null) {
                        this.b = new lu();
                    }
                }
            }
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d {
        @DexIgnore
        public /* final */ ht<?> a;
        @DexIgnore
        public /* final */ oz b;

        @DexIgnore
        public d(oz ozVar, ht<?> htVar) {
            this.b = ozVar;
            this.a = htVar;
        }

        @DexIgnore
        public void a() {
            synchronized (gt.this) {
                this.a.c(this.b);
            }
        }
    }

    @DexIgnore
    public gt(ru ruVar, ku.a aVar, uu uuVar, uu uuVar2, uu uuVar3, uu uuVar4, boolean z) {
        this(ruVar, aVar, uuVar, uuVar2, uuVar3, uuVar4, (ot) null, (kt) null, (ws) null, (b) null, (a) null, (ut) null, z);
    }

    @DexIgnore
    public <R> d a(yq yqVar, Object obj, vr vrVar, int i2, int i3, Class<?> cls, Class<R> cls2, br brVar, ft ftVar, Map<Class<?>, bs<?>> map, boolean z, boolean z2, xr xrVar, boolean z3, boolean z4, boolean z5, boolean z6, oz ozVar, Executor executor) {
        long a2 = i ? m00.a() : 0;
        jt a3 = this.b.a(obj, vrVar, i2, i3, map, cls, cls2, xrVar);
        synchronized (this) {
            lt<?> a4 = a(a3, z3, a2);
            if (a4 == null) {
                d a5 = a(yqVar, obj, vrVar, i2, i3, cls, cls2, brVar, ftVar, map, z, z2, xrVar, z3, z4, z5, z6, ozVar, executor, a3, a2);
                return a5;
            }
            ozVar.a(a4, pr.MEMORY_CACHE);
            return null;
        }
    }

    @DexIgnore
    public final lt<?> b(vr vrVar) {
        lt<?> b2 = this.h.b(vrVar);
        if (b2 != null) {
            b2.d();
        }
        return b2;
    }

    @DexIgnore
    public final lt<?> c(vr vrVar) {
        lt<?> a2 = a(vrVar);
        if (a2 != null) {
            a2.d();
            this.h.a(vrVar, a2);
        }
        return a2;
    }

    @DexIgnore
    public gt(ru ruVar, ku.a aVar, uu uuVar, uu uuVar2, uu uuVar3, uu uuVar4, ot otVar, kt ktVar, ws wsVar, b bVar, a aVar2, ut utVar, boolean z) {
        this.c = ruVar;
        ku.a aVar3 = aVar;
        this.f = new c(aVar);
        ws wsVar2 = wsVar == null ? new ws(z) : wsVar;
        this.h = wsVar2;
        wsVar2.a((lt.a) this);
        this.b = ktVar == null ? new kt() : ktVar;
        this.a = otVar == null ? new ot() : otVar;
        this.d = bVar == null ? new b(uuVar, uuVar2, uuVar3, uuVar4, this, this) : bVar;
        this.g = aVar2 == null ? new a(this.f) : aVar2;
        this.e = utVar == null ? new ut() : utVar;
        ruVar.a((ru.a) this);
    }

    @DexIgnore
    public void b(rt<?> rtVar) {
        if (rtVar instanceof lt) {
            ((lt) rtVar).g();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    @DexIgnore
    public final <R> d a(yq yqVar, Object obj, vr vrVar, int i2, int i3, Class<?> cls, Class<R> cls2, br brVar, ft ftVar, Map<Class<?>, bs<?>> map, boolean z, boolean z2, xr xrVar, boolean z3, boolean z4, boolean z5, boolean z6, oz ozVar, Executor executor, jt jtVar, long j) {
        oz ozVar2 = ozVar;
        Executor executor2 = executor;
        jt jtVar2 = jtVar;
        long j2 = j;
        ht<?> a2 = this.a.a((vr) jtVar2, z6);
        if (a2 != null) {
            a2.a(ozVar2, executor2);
            if (i) {
                a("Added to existing load", j2, (vr) jtVar2);
            }
            return new d(ozVar2, a2);
        }
        ht a3 = this.d.a(jtVar, z3, z4, z5, z6);
        ht htVar = a3;
        jt jtVar3 = jtVar2;
        dt<R> a4 = this.g.a(yqVar, obj, jtVar, vrVar, i2, i3, cls, cls2, brVar, ftVar, map, z, z2, z6, xrVar, a3);
        this.a.a((vr) jtVar3, (ht<?>) htVar);
        ht htVar2 = htVar;
        jt jtVar4 = jtVar3;
        oz ozVar3 = ozVar;
        htVar2.a(ozVar3, executor);
        htVar2.b(a4);
        if (i) {
            a("Started new load", j, (vr) jtVar4);
        }
        return new d(ozVar3, htVar2);
    }

    @DexIgnore
    public final lt<?> a(jt jtVar, boolean z, long j) {
        if (!z) {
            return null;
        }
        lt<?> b2 = b((vr) jtVar);
        if (b2 != null) {
            if (i) {
                a("Loaded resource from active resources", j, (vr) jtVar);
            }
            return b2;
        }
        lt<?> c2 = c(jtVar);
        if (c2 == null) {
            return null;
        }
        if (i) {
            a("Loaded resource from cache", j, (vr) jtVar);
        }
        return c2;
    }

    @DexIgnore
    public static void a(String str, long j, vr vrVar) {
        Log.v("Engine", str + " in " + m00.a(j) + "ms, key: " + vrVar);
    }

    @DexIgnore
    public final lt<?> a(vr vrVar) {
        rt<?> a2 = this.c.a(vrVar);
        if (a2 == null) {
            return null;
        }
        if (a2 instanceof lt) {
            return (lt) a2;
        }
        return new lt<>(a2, true, true, vrVar, this);
    }

    @DexIgnore
    public synchronized void a(ht<?> htVar, vr vrVar, lt<?> ltVar) {
        if (ltVar != null) {
            if (ltVar.f()) {
                this.h.a(vrVar, ltVar);
            }
        }
        this.a.b(vrVar, htVar);
    }

    @DexIgnore
    public synchronized void a(ht<?> htVar, vr vrVar) {
        this.a.b(vrVar, htVar);
    }

    @DexIgnore
    public void a(rt<?> rtVar) {
        this.e.a(rtVar, true);
    }

    @DexIgnore
    public void a(vr vrVar, lt<?> ltVar) {
        this.h.a(vrVar);
        if (ltVar.f()) {
            this.c.a(vrVar, ltVar);
        } else {
            this.e.a(ltVar, false);
        }
    }
}
