package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum xh4 {
    BLUETOOTH_DISABLED("001"),
    LOCATION_SERVICE_DISABLED("002"),
    LOCATION_ACCESS_DISABLED("003"),
    FILE_NOT_READY("004"),
    FIRMWARE_NOT_MATCH("005"),
    COMMAND_TIMEOUT("006"),
    USER_CANCELLED("007"),
    SESSION_INTERRUPTED("008"),
    APP_KILLED("009"),
    BUTTON_SERVICE_CRASH("010"),
    APP_LAYER_CRASH("011"),
    NETWORK_ERROR("012"),
    SERVER_MAINTENANCE("013"),
    BACKGROUND_LOCATION_ACCESS_DISABLED("014"),
    UNKNOWN("999");
    
    @DexIgnore
    public /* final */ String code;

    @DexIgnore
    public xh4(String str) {
        this.code = str;
    }

    @DexIgnore
    public final String getCode() {
        return this.code;
    }
}
