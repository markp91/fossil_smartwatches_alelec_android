package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.google.android.gms.maps.model.CameraPosition;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qy2 extends jh2 implements rx2 {
    @DexIgnore
    public qy2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    @DexIgnore
    public final void a(x52 x52) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        b(4, zza);
    }

    @DexIgnore
    public final void c(x52 x52) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        b(5, zza);
    }

    @DexIgnore
    public final void clear() throws RemoteException {
        b(14, zza());
    }

    @DexIgnore
    public final CameraPosition n() throws RemoteException {
        Parcel a = a(1, zza());
        CameraPosition cameraPosition = (CameraPosition) lh2.a(a, CameraPosition.CREATOR);
        a.recycle();
        return cameraPosition;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    public final xx2 o() throws RemoteException {
        xx2 xx2;
        Parcel a = a(25, zza());
        IBinder readStrongBinder = a.readStrongBinder();
        if (readStrongBinder == null) {
            xx2 = null;
        } else {
            Object queryLocalInterface = readStrongBinder.queryLocalInterface("com.google.android.gms.maps.internal.IUiSettingsDelegate");
            if (queryLocalInterface instanceof xx2) {
                xx2 = queryLocalInterface;
            } else {
                xx2 = new jy2(readStrongBinder);
            }
        }
        a.recycle();
        return xx2;
    }

    @DexIgnore
    public final void a(x52 x52, my2 my2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) x52);
        lh2.a(zza, (IInterface) my2);
        b(6, zza);
    }

    @DexIgnore
    public final ph2 a(az2 az2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) az2);
        Parcel a = a(11, zza);
        ph2 a2 = qh2.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void a(vy2 vy2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) vy2);
        b(96, zza);
    }

    @DexIgnore
    public final void a(ty2 ty2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) ty2);
        b(99, zza);
    }
}
