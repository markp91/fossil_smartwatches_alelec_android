package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import java.util.LinkedHashSet;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class od1 extends if1 {
    @DexIgnore
    public static /* final */ m21 C; // = new m21((qg6) null);
    @DexIgnore
    public /* final */ ae0 B;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ od1(ue1 ue1, q41 q41, ae0 ae0, String str, int i) {
        super(ue1, q41, eh1.SET_DEVICE_PRESET, (i & 8) != 0 ? ze0.a("UUID.randomUUID().toString()") : str);
        this.B = ae0;
    }

    @DexIgnore
    public void h() {
        boolean z = false;
        if (!(this.B.getPresetItems().length == 0)) {
            l50[] m = m();
            if (m.length == 0) {
                z = true;
            }
            if (!z) {
                try {
                    m21 m21 = C;
                    w40 w40 = this.x.a().i().get(Short.valueOf(w31.ASSET.a));
                    if (w40 == null) {
                        w40 = mi0.A.g();
                    }
                    if1.a((if1) this, (if1) new p21(this.w, this.x, eh1.PUT_BACKGROUND_IMAGE_DATA, true, 1792, m21.a(m, 1792, w40), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 64), (hg6) new i41(this), (hg6) new e61(this), (ig6) new b81(this), (hg6) null, (hg6) null, 48, (Object) null);
                } catch (sw0 e) {
                    qs0.h.a(e);
                    a(km1.a(this.v, (eh1) null, sk1.UNSUPPORTED_FORMAT, (bn0) null, 5));
                }
            } else {
                n();
            }
        } else {
            a(km1.a(this.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5));
        }
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.PRESET, (Object) this.B.a());
    }

    @DexIgnore
    public final l50[] m() {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        for (be0 be0 : this.B.getPresetItems()) {
            if (be0 instanceof m50) {
                linkedHashSet.addAll(((m50) be0).d());
            }
        }
        Object[] array = yd6.c(linkedHashSet).toArray(new l50[0]);
        if (array != null) {
            return (l50[]) array;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public final void n() {
        JSONObject jSONObject;
        ue1 ue1 = this.w;
        q41 q41 = this.x;
        eh1 eh1 = eh1.PUT_PRESET_CONFIG;
        try {
            w40 w40 = q41.a().i().get(Short.valueOf(w31.ASSET.a));
            if (w40 == null) {
                w40 = mi0.A.g();
            }
            wg6.a(w40, "delegate.deviceInformati\u2026tant.DEFAULT_FILE_VERSION");
            JSONObject jSONObject2 = new JSONObject();
            for (be0 be0 : this.B.getPresetItems()) {
                be0.a(w40);
                jSONObject2 = cw0.a(jSONObject2, be0.b());
            }
            jSONObject = new JSONObject().put("push", new JSONObject().put("set", jSONObject2));
            wg6.a(jSONObject, "JSONObject().put(UIScrip\u2026T, assignmentJsonObject))");
        } catch (JSONException e) {
            qs0.h.a(e);
            jSONObject = new JSONObject();
        }
        if1.a((if1) this, (if1) new le0(ue1, q41, eh1, jSONObject, false, LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, this.z, 48), (hg6) new y91(this), (hg6) new tb1(this), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
    }
}
