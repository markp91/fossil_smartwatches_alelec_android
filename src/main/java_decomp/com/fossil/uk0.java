package com.fossil;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uk0 {
    @DexIgnore
    public /* final */ ws0<if1> a; // = new ws0<>(em1.a);
    @DexIgnore
    public /* final */ ws0<if1> b; // = new ws0<>(un1.a);
    @DexIgnore
    public Hashtable<if1, wy0> c; // = new Hashtable<>();
    @DexIgnore
    public /* final */ Object d; // = new Object();
    @DexIgnore
    public nj1 e;

    @DexIgnore
    public uk0(nj1 nj1) {
        this.e = nj1;
    }

    @DexIgnore
    public final void a(nj1 nj1) {
        synchronized (this.d) {
            this.e = nj1;
            cd6 cd6 = cd6.a;
        }
    }

    @DexIgnore
    public final if1[] b() {
        if1[] if1Arr;
        synchronized (this.d) {
            Object[] array = this.a.toArray(new if1[0]);
            if (array != null) {
                if1Arr = (if1[]) array;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return if1Arr;
    }

    @DexIgnore
    public final if1[] c() {
        if1[] if1Arr;
        synchronized (this.d) {
            Object[] array = this.b.toArray(new if1[0]);
            if (array != null) {
                if1Arr = (if1[]) array;
            } else {
                throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
            }
        }
        return if1Arr;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0079, code lost:
        if (r0.e.a(r2) != false) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a0, code lost:
        if (r0.e.a(r2) != false) goto L_0x00a2;
     */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a7  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0008 A[SYNTHETIC] */
    public final void d() {
        boolean z;
        if1 if1;
        Iterator<if1> it = this.a.iterator();
        while (it.hasNext()) {
            if1 next = it.next();
            if (next.t) {
                this.a.remove(next);
                this.c.remove(next);
            } else {
                wg6.a(next, "phase");
                if (next.f().contains(hl1.TRANSFER_DATA)) {
                    boolean z2 = false;
                    for (if1 next2 : this.b) {
                        if (next2.f().contains(hl1.TRANSFER_DATA) && next2.e().compareTo(next.e()) < 0) {
                            next2.a(sk1.INTERRUPTED);
                            z2 = true;
                        }
                        if (!next2.a(next)) {
                            z2 = true;
                        }
                    }
                    if (!z2) {
                    }
                    z = false;
                    if (z) {
                        this.a.remove(next);
                        this.b.b(next);
                        next.a((hg6<? super if1, cd6>) new rf0(this));
                        wy0 wy0 = this.c.get(next);
                        if (wy0 != null) {
                            if1 if12 = wy0.a;
                            if12.a(if12.q);
                            if1 if13 = wy0.a;
                            if (!if13.t) {
                                ue1 ue1 = if13.w;
                                cc0 cc0 = cc0.DEBUG;
                                String str = if13.a;
                                Object[] objArr = {if13.z, if13.i().toString(2)};
                                qs0 qs0 = qs0.h;
                                String a2 = cw0.a((Enum<?>) wy0.a.y);
                                og0 og0 = og0.PHASE_START;
                                if1 if14 = wy0.a;
                                String str2 = if14.w.t;
                                String a3 = cw0.a((Enum<?>) if14.y);
                                if1 if15 = wy0.a;
                                nn0 nn0 = r5;
                                nn0 nn02 = new nn0(a2, og0, str2, a3, if15.z, true, (String) null, (r40) null, (bw0) null, if15.i(), 448);
                                qs0.a(nn0);
                                if1 if16 = wy0.a;
                                if16.v = km1.a(if16.v, (eh1) null, sk1.SUCCESS, (bn0) null, 5);
                                if1 if17 = wy0.a;
                                ue1 ue12 = if17.w;
                                j71 j71 = if17.c;
                                if (!ue12.h.contains(j71)) {
                                    ue12.h.add(j71);
                                }
                                Iterator<T> it2 = wy0.a.d.iterator();
                                while (it2.hasNext()) {
                                    ((hg6) it2.next()).invoke(wy0.a);
                                }
                                wy0.a.l = System.currentTimeMillis();
                                if1 if18 = wy0.a;
                                if18.j();
                                if (if18.c()) {
                                    ue1 ue13 = if18.w;
                                    if1.a(if18, (if1) new u51(ue13, if18.x, nq0.SIXTEEN_BYTES_MSB_ECDH_SHARED_SECRET_KEY, ue0.b.a(ue13.t).a(), if18.z), (hg6) new hv0(if18), (hg6) new cx0(if18), (ig6) null, (hg6) null, (hg6) null, 56, (Object) null);
                                } else {
                                    if18.h();
                                }
                            }
                        }
                    }
                } else {
                    Iterator<if1> it3 = this.b.iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            if1 = null;
                            break;
                        }
                        if1 = it3.next();
                        if (!if1.a(next)) {
                            break;
                        }
                    }
                    if (if1 == null) {
                    }
                    z = false;
                    if (z) {
                    }
                }
                z = true;
                if (z) {
                }
            }
        }
    }

    @DexIgnore
    public final ArrayList<if1> a() {
        ArrayList<if1> arrayList;
        synchronized (this.d) {
            arrayList = new ArrayList<>();
            arrayList.addAll(this.a);
            arrayList.addAll(this.b);
        }
        return arrayList;
    }

    @DexIgnore
    public final void b(sk1 sk1, eh1[] eh1Arr) {
        a(sk1, (hg6<? super if1, Boolean>) new aj0(eh1Arr));
    }

    @DexIgnore
    public final void a(sk1 sk1, hg6<? super if1, Boolean> hg6) {
        for (if1 next : this.b) {
            wg6.a(next, "phase");
            if (((Boolean) hg6.invoke(next)).booleanValue()) {
                next.a(sk1);
                this.b.remove(next);
            }
        }
    }

    @DexIgnore
    public final void a(sk1 sk1, eh1[] eh1Arr) {
        ih0 ih0 = new ih0(eh1Arr);
        a(sk1, (hg6<? super if1, Boolean>) ih0);
        for (if1 next : this.a) {
            wg6.a(next, "phase");
            if (((Boolean) ih0.invoke(next)).booleanValue()) {
                next.a(sk1);
                this.a.remove(next);
            }
        }
    }
}
