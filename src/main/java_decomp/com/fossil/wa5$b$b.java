package com.fossil;

import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$deviceName$1", f = "HomeDashboardPresenter.kt", l = {}, m = "invokeSuspend")
public final class wa5$b$b extends sf6 implements ig6<il6, xe6<? super String>, Object> {
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wa5$b$b(HomeDashboardPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wa5$b$b wa5_b_b = new wa5$b$b(this.this$0, xe6);
        wa5_b_b.p$ = (il6) obj;
        return wa5_b_b;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wa5$b$b) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        ff6.a();
        if (this.label == 0) {
            nc6.a(obj);
            return this.this$0.this$0.y.getDeviceNameBySerial(this.this$0.this$0.g);
        }
        throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
    }
}
