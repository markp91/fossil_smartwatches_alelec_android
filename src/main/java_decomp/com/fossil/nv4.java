package com.fossil;

import com.portfolio.platform.uirenew.alarm.AlarmActivity;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class nv4 implements MembersInjector<AlarmActivity> {
    @DexIgnore
    public static void a(AlarmActivity alarmActivity, AlarmPresenter alarmPresenter) {
        alarmActivity.B = alarmPresenter;
    }
}
