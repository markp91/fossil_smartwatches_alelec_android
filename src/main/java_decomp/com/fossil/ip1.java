package com.fossil;

import com.fossil.np1;
import java.util.Arrays;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ip1 extends np1 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Integer b;
    @DexIgnore
    public /* final */ byte[] c;
    @DexIgnore
    public /* final */ long d;
    @DexIgnore
    public /* final */ long e;
    @DexIgnore
    public /* final */ Map<String, String> f;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends np1.a {
        @DexIgnore
        public String a;
        @DexIgnore
        public Integer b;
        @DexIgnore
        public byte[] c;
        @DexIgnore
        public Long d;
        @DexIgnore
        public Long e;
        @DexIgnore
        public Map<String, String> f;

        @DexIgnore
        public np1.a a(String str) {
            if (str != null) {
                this.a = str;
                return this;
            }
            throw new NullPointerException("Null transportName");
        }

        @DexIgnore
        public np1.a b(long j) {
            this.e = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public Map<String, String> b() {
            Map<String, String> map = this.f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        @DexIgnore
        public np1.a a(Integer num) {
            this.b = num;
            return this;
        }

        @DexIgnore
        public np1.a a(byte[] bArr) {
            if (bArr != null) {
                this.c = bArr;
                return this;
            }
            throw new NullPointerException("Null payload");
        }

        @DexIgnore
        public np1.a a(long j) {
            this.d = Long.valueOf(j);
            return this;
        }

        @DexIgnore
        public np1.a a(Map<String, String> map) {
            if (map != null) {
                this.f = map;
                return this;
            }
            throw new NullPointerException("Null autoMetadata");
        }

        @DexIgnore
        public np1 a() {
            String str = "";
            if (this.a == null) {
                str = str + " transportName";
            }
            if (this.c == null) {
                str = str + " payload";
            }
            if (this.d == null) {
                str = str + " eventMillis";
            }
            if (this.e == null) {
                str = str + " uptimeMillis";
            }
            if (this.f == null) {
                str = str + " autoMetadata";
            }
            if (str.isEmpty()) {
                return new ip1(this.a, this.b, this.c, this.d.longValue(), this.e.longValue(), this.f);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    @DexIgnore
    public Map<String, String> a() {
        return this.f;
    }

    @DexIgnore
    public Integer b() {
        return this.b;
    }

    @DexIgnore
    public long c() {
        return this.d;
    }

    @DexIgnore
    public byte[] e() {
        return this.c;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof np1)) {
            return false;
        }
        np1 np1 = (np1) obj;
        if (this.a.equals(np1.f()) && ((num = this.b) != null ? num.equals(np1.b()) : np1.b() == null)) {
            if (!Arrays.equals(this.c, np1 instanceof ip1 ? ((ip1) np1).c : np1.e()) || this.d != np1.c() || this.e != np1.g() || !this.f.equals(np1.a())) {
                return false;
            }
            return true;
        }
        return false;
    }

    @DexIgnore
    public String f() {
        return this.a;
    }

    @DexIgnore
    public long g() {
        return this.e;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = (this.a.hashCode() ^ 1000003) * 1000003;
        Integer num = this.b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        long j = this.d;
        long j2 = this.e;
        return ((((((((hashCode ^ hashCode2) * 1000003) ^ Arrays.hashCode(this.c)) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f.hashCode();
    }

    @DexIgnore
    public String toString() {
        return "EventInternal{transportName=" + this.a + ", code=" + this.b + ", payload=" + Arrays.toString(this.c) + ", eventMillis=" + this.d + ", uptimeMillis=" + this.e + ", autoMetadata=" + this.f + "}";
    }

    @DexIgnore
    public ip1(String str, Integer num, byte[] bArr, long j, long j2, Map<String, String> map) {
        this.a = str;
        this.b = num;
        this.c = bArr;
        this.d = j;
        this.e = j2;
        this.f = map;
    }
}
