package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.fitness.data.DataSet;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class m82 implements Parcelable.Creator<n82> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        DataSet dataSet = null;
        IBinder iBinder = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                dataSet = (DataSet) f22.a(parcel, a, DataSet.CREATOR);
            } else if (a2 == 2) {
                iBinder = f22.p(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                z = f22.i(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new n82(dataSet, iBinder, z);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new n82[i];
    }
}
