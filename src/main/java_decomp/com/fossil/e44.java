package com.fossil;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.data.model.thirdparty.googlefit.GFitWODistance;
import java.lang.reflect.Type;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class e44 {
    @DexIgnore
    public /* final */ Gson a; // = new Gson();
    @DexIgnore
    public /* final */ Type b; // = new b().getType();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends TypeToken<List<? extends GFitWODistance>> {
    }

    /*
    static {
        new a((qg6) null);
    }
    */

    @DexIgnore
    public final String a(List<GFitWODistance> list) {
        wg6.b(list, "distances");
        if (list.isEmpty()) {
            return "";
        }
        try {
            String a2 = this.a.a(list, this.b);
            wg6.a((Object) a2, "mGson.toJson(distances, mType)");
            return a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toString: ");
            e.printStackTrace();
            sb.append(cd6.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return "";
        }
    }

    @DexIgnore
    public final List<GFitWODistance> a(String str) {
        wg6.b(str, "data");
        if (str.length() == 0) {
            return qd6.a();
        }
        try {
            Object a2 = this.a.a(str, this.b);
            wg6.a(a2, "mGson.fromJson(data, mType)");
            return (List) a2;
        } catch (Exception e) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("toListGFitWODistance: ");
            e.printStackTrace();
            sb.append(cd6.a);
            local.e("GFitWODistancesConverter", sb.toString());
            return qd6.a();
        }
    }
}
