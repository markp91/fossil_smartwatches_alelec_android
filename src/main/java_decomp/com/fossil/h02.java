package com.fossil;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.fossil.qw1;
import com.fossil.rv1;
import com.fossil.rv1.d;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h02<O extends rv1.d> extends vv1<O> {
    @DexIgnore
    public /* final */ rv1.f j;
    @DexIgnore
    public /* final */ a02 k;
    @DexIgnore
    public /* final */ e12 l;
    @DexIgnore
    public /* final */ rv1.a<? extends ac3, lb3> m;

    @DexIgnore
    public h02(Context context, rv1<O> rv1, Looper looper, rv1.f fVar, a02 a02, e12 e12, rv1.a<? extends ac3, lb3> aVar) {
        super(context, rv1, looper);
        this.j = fVar;
        this.k = a02;
        this.l = e12;
        this.m = aVar;
        this.i.a((vv1<?>) this);
    }

    @DexIgnore
    public final rv1.f a(Looper looper, qw1.a<O> aVar) {
        this.k.a((c02) aVar);
        return this.j;
    }

    @DexIgnore
    public final rv1.f i() {
        return this.j;
    }

    @DexIgnore
    public final dz1 a(Context context, Handler handler) {
        return new dz1(context, handler, this.l, this.m);
    }
}
