package com.fossil;

import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.ui.device.domain.usecase.DownloadFirmwareByDeviceModelUsecase;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;
import com.portfolio.platform.uirenew.onboarding.ota.UpdateFirmwarePresenter;
import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class dr5 implements Factory<br5> {
    @DexIgnore
    public static UpdateFirmwarePresenter a(yq5 yq5, DeviceRepository deviceRepository, UserRepository userRepository, cj4 cj4, an4 an4, UpdateFirmwareUsecase updateFirmwareUsecase, DownloadFirmwareByDeviceModelUsecase downloadFirmwareByDeviceModelUsecase) {
        return new UpdateFirmwarePresenter(yq5, deviceRepository, userRepository, cj4, an4, updateFirmwareUsecase, downloadFirmwareByDeviceModelUsecase);
    }
}
