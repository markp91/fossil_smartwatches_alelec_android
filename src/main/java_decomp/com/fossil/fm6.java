package com.fossil;

import com.fossil.tl6;
import com.misfit.frameworks.buttonservice.ButtonService;
import java.util.concurrent.atomic.AtomicReferenceFieldUpdater;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fm6 extends gm6 implements tl6 {
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater d;
    @DexIgnore
    public static /* final */ AtomicReferenceFieldUpdater e;
    @DexIgnore
    public volatile Object _delayed; // = null;
    @DexIgnore
    public volatile Object _queue; // = null;
    @DexIgnore
    public volatile boolean isCompleted;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a extends c {
        @DexIgnore
        public /* final */ lk6<cd6> d;
        @DexIgnore
        public /* final */ /* synthetic */ fm6 e;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(fm6 fm6, long j, lk6<? super cd6> lk6) {
            super(j);
            wg6.b(lk6, "cont");
            this.e = fm6;
            this.d = lk6;
        }

        @DexIgnore
        public void run() {
            this.d.a((dl6) this.e, cd6.a);
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b extends c {
        @DexIgnore
        public /* final */ Runnable d;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(long j, Runnable runnable) {
            super(j);
            wg6.b(runnable, "block");
            this.d = runnable;
        }

        @DexIgnore
        public void run() {
            this.d.run();
        }

        @DexIgnore
        public String toString() {
            return super.toString() + this.d.toString();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class c implements Runnable, Comparable<c>, am6, ap6 {
        @DexIgnore
        public Object a;
        @DexIgnore
        public int b; // = -1;
        @DexIgnore
        public long c;

        @DexIgnore
        public c(long j) {
            this.c = j;
        }

        @DexIgnore
        public zo6<?> a() {
            Object obj = this.a;
            if (!(obj instanceof zo6)) {
                obj = null;
            }
            return (zo6) obj;
        }

        @DexIgnore
        public int b() {
            return this.b;
        }

        @DexIgnore
        public final synchronized void dispose() {
            Object obj = this.a;
            if (obj != im6.a) {
                if (!(obj instanceof d)) {
                    obj = null;
                }
                d dVar = (d) obj;
                if (dVar != null) {
                    dVar.b(this);
                }
                this.a = im6.a;
            }
        }

        @DexIgnore
        public String toString() {
            return "Delayed[nanos=" + this.c + ']';
        }

        @DexIgnore
        public void a(zo6<?> zo6) {
            if (this.a != im6.a) {
                this.a = zo6;
                return;
            }
            throw new IllegalArgumentException("Failed requirement.".toString());
        }

        @DexIgnore
        public void a(int i) {
            this.b = i;
        }

        @DexIgnore
        /* renamed from: a */
        public int compareTo(c cVar) {
            wg6.b(cVar, "other");
            int i = ((this.c - cVar.c) > 0 ? 1 : ((this.c - cVar.c) == 0 ? 0 : -1));
            if (i > 0) {
                return 1;
            }
            return i < 0 ? -1 : 0;
        }

        @DexIgnore
        public final boolean a(long j) {
            return j - this.c >= 0;
        }

        @DexIgnore
        public final synchronized int a(long j, d dVar, fm6 fm6) {
            wg6.b(dVar, "delayed");
            wg6.b(fm6, "eventLoop");
            if (this.a == im6.a) {
                return 2;
            }
            synchronized (dVar) {
                c cVar = (c) dVar.a();
                if (fm6.isCompleted) {
                    return 1;
                }
                if (cVar == null) {
                    dVar.b = j;
                } else {
                    long j2 = cVar.c;
                    if (j2 - j < 0) {
                        j = j2;
                    }
                    if (j - dVar.b > 0) {
                        dVar.b = j;
                    }
                }
                if (this.c - dVar.b < 0) {
                    this.c = dVar.b;
                }
                dVar.a(this);
                return 0;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d extends zo6<c> {
        @DexIgnore
        public long b;

        @DexIgnore
        public d(long j) {
            this.b = j;
        }
    }

    /*
    static {
        Class<Object> cls = Object.class;
        Class<fm6> cls2 = fm6.class;
        d = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_queue");
        e = AtomicReferenceFieldUpdater.newUpdater(cls2, cls, "_delayed");
    }
    */

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0055  */
    public long C() {
        Runnable I;
        ap6 ap6;
        if (D()) {
            return o();
        }
        d dVar = (d) this._delayed;
        if (dVar == null || dVar.c()) {
            I = I();
            if (I != null) {
                I.run();
            }
            return o();
        }
        pn6 a2 = qn6.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        do {
            synchronized (dVar) {
                ap6 a4 = dVar.a();
                ap6 = null;
                if (a4 != null) {
                    c cVar = (c) a4;
                    if (cVar.a(a3) ? b(cVar) : false) {
                        ap6 = dVar.a(0);
                    }
                }
            }
        } while (((c) ap6) != null);
        I = I();
        if (I != null) {
        }
        return o();
    }

    @DexIgnore
    public final void H() {
        if (!nl6.a() || this.isCompleted) {
            while (true) {
                Object obj = this._queue;
                if (obj == null) {
                    if (d.compareAndSet(this, (Object) null, im6.b)) {
                        return;
                    }
                } else if (obj instanceof lo6) {
                    ((lo6) obj).a();
                    return;
                } else if (obj != im6.b) {
                    lo6 lo6 = new lo6(8, true);
                    if (obj != null) {
                        lo6.a((Runnable) obj);
                        if (d.compareAndSet(this, obj, lo6)) {
                            return;
                        }
                    } else {
                        throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                    }
                } else {
                    return;
                }
            }
        } else {
            throw new AssertionError();
        }
    }

    @DexIgnore
    public final Runnable I() {
        while (true) {
            Object obj = this._queue;
            if (obj == null) {
                return null;
            }
            if (obj instanceof lo6) {
                if (obj != null) {
                    lo6 lo6 = (lo6) obj;
                    Object f = lo6.f();
                    if (f != lo6.g) {
                        return (Runnable) f;
                    }
                    d.compareAndSet(this, obj, lo6.e());
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == im6.b) {
                return null;
            } else {
                if (d.compareAndSet(this, obj, (Object) null)) {
                    if (obj != null) {
                        return (Runnable) obj;
                    }
                    throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public boolean J() {
        if (!B()) {
            return false;
        }
        d dVar = (d) this._delayed;
        if (dVar != null && !dVar.c()) {
            return false;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof lo6) {
                return ((lo6) obj).c();
            }
            if (obj == im6.b) {
                return true;
            }
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void K() {
        c cVar;
        pn6 a2 = qn6.a();
        long a3 = a2 != null ? a2.a() : System.nanoTime();
        while (true) {
            d dVar = (d) this._delayed;
            if (dVar != null && (cVar = (c) dVar.f()) != null) {
                a(a3, cVar);
            } else {
                return;
            }
        }
    }

    @DexIgnore
    public final void L() {
        this._queue = null;
        this._delayed = null;
    }

    @DexIgnore
    public final am6 b(long j, Runnable runnable) {
        wg6.b(runnable, "block");
        long a2 = im6.a(j);
        if (a2 >= 4611686018427387903L) {
            return en6.a;
        }
        pn6 a3 = qn6.a();
        long a4 = a3 != null ? a3.a() : System.nanoTime();
        b bVar = new b(a2 + a4, runnable);
        b(a4, (c) bVar);
        return bVar;
    }

    @DexIgnore
    public final int c(long j, c cVar) {
        if (this.isCompleted) {
            return 1;
        }
        d dVar = (d) this._delayed;
        if (dVar == null) {
            e.compareAndSet(this, (Object) null, new d(j));
            Object obj = this._delayed;
            if (obj != null) {
                dVar = (d) obj;
            } else {
                wg6.a();
                throw null;
            }
        }
        return cVar.a(j, dVar, this);
    }

    @DexIgnore
    public long o() {
        c cVar;
        if (super.o() == 0) {
            return 0;
        }
        Object obj = this._queue;
        if (obj != null) {
            if (obj instanceof lo6) {
                if (!((lo6) obj).c()) {
                    return 0;
                }
            } else if (obj == im6.b) {
                return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
            } else {
                return 0;
            }
        }
        d dVar = (d) this._delayed;
        if (dVar == null || (cVar = (c) dVar.d()) == null) {
            return ButtonService.TIME_STAMP_FOR_NON_EXECUTABLE_METHOD;
        }
        long j = cVar.c;
        pn6 a2 = qn6.a();
        return ci6.a(j - (a2 != null ? a2.a() : System.nanoTime()), 0);
    }

    @DexIgnore
    public void shutdown() {
        on6.b.c();
        this.isCompleted = true;
        H();
        do {
        } while (C() <= 0);
        K();
    }

    @DexIgnore
    public am6 a(long j, Runnable runnable) {
        wg6.b(runnable, "block");
        return tl6.a.a(this, j, runnable);
    }

    @DexIgnore
    public void a(long j, lk6<? super cd6> lk6) {
        wg6.b(lk6, "continuation");
        long a2 = im6.a(j);
        if (a2 < 4611686018427387903L) {
            pn6 a3 = qn6.a();
            long a4 = a3 != null ? a3.a() : System.nanoTime();
            a aVar = new a(this, a2 + a4, lk6);
            nk6.a((lk6<?>) lk6, (am6) aVar);
            b(a4, (c) aVar);
        }
    }

    @DexIgnore
    public final void b(long j, c cVar) {
        wg6.b(cVar, "delayedTask");
        int c2 = c(j, cVar);
        if (c2 != 0) {
            if (c2 == 1) {
                a(j, cVar);
            } else if (c2 != 2) {
                throw new IllegalStateException("unexpected result".toString());
            }
        } else if (a(cVar)) {
            G();
        }
    }

    @DexIgnore
    public final void a(af6 af6, Runnable runnable) {
        wg6.b(af6, "context");
        wg6.b(runnable, "block");
        a(runnable);
    }

    @DexIgnore
    public final void a(Runnable runnable) {
        wg6.b(runnable, "task");
        if (b(runnable)) {
            G();
        } else {
            pl6.g.a(runnable);
        }
    }

    @DexIgnore
    public final boolean b(Runnable runnable) {
        while (true) {
            Object obj = this._queue;
            if (this.isCompleted) {
                return false;
            }
            if (obj == null) {
                if (d.compareAndSet(this, (Object) null, runnable)) {
                    return true;
                }
            } else if (obj instanceof lo6) {
                if (obj != null) {
                    lo6 lo6 = (lo6) obj;
                    int a2 = lo6.a(runnable);
                    if (a2 == 0) {
                        return true;
                    }
                    if (a2 == 1) {
                        d.compareAndSet(this, obj, lo6.e());
                    } else if (a2 == 2) {
                        return false;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.Queue<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> /* = kotlinx.coroutines.internal.LockFreeTaskQueueCore<kotlinx.coroutines.Runnable /* = java.lang.Runnable */> */");
                }
            } else if (obj == im6.b) {
                return false;
            } else {
                lo6 lo62 = new lo6(8, true);
                if (obj != null) {
                    lo62.a((Runnable) obj);
                    lo62.a(runnable);
                    if (d.compareAndSet(this, obj, lo62)) {
                        return true;
                    }
                } else {
                    throw new rc6("null cannot be cast to non-null type kotlinx.coroutines.Runnable /* = java.lang.Runnable */");
                }
            }
        }
    }

    @DexIgnore
    public final boolean a(c cVar) {
        d dVar = (d) this._delayed;
        return (dVar != null ? (c) dVar.d() : null) == cVar;
    }
}
