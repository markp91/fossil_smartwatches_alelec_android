package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class x50 extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ z50 a;
    @DexIgnore
    public /* final */ ic0 b;
    @DexIgnore
    public lc0 c;
    @DexIgnore
    public mc0 d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<x50> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                z50 valueOf = z50.valueOf(readString);
                parcel.setDataPosition(0);
                switch (et0.a[valueOf.ordinal()]) {
                    case 1:
                        return f60.CREATOR.createFromParcel(parcel);
                    case 2:
                        return c60.CREATOR.createFromParcel(parcel);
                    case 3:
                        return d60.CREATOR.createFromParcel(parcel);
                    case 4:
                        return a60.CREATOR.createFromParcel(parcel);
                    case 5:
                        return w50.CREATOR.createFromParcel(parcel);
                    case 6:
                        return e60.CREATOR.createFromParcel(parcel);
                    case 7:
                        return t50.CREATOR.createFromParcel(parcel);
                    case 8:
                        return v50.CREATOR.createFromParcel(parcel);
                    case 9:
                        return u50.CREATOR.createFromParcel(parcel);
                    case 10:
                        return b60.CREATOR.createFromParcel(parcel);
                    default:
                        throw new kc6();
                }
            } else {
                wg6.a();
                throw null;
            }
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new x50[i];
        }
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ x50(z50 z50, ic0 ic0, lc0 lc0, mc0 mc0, int i) {
        this(z50, (i & 2) != 0 ? new e31() : ic0, (i & 4) != 0 ? new lc0(0, 62) : lc0, (i & 8) != 0 ? new mc0(mc0.CREATOR.a()) : mc0);
    }

    @DexIgnore
    public final void a(lc0 lc0) {
        this.c = lc0;
    }

    @DexIgnore
    public final ic0 b() {
        return this.b;
    }

    @DexIgnore
    public final int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            x50 x50 = (x50) obj;
            return this.a == x50.a && !(wg6.a(this.b, x50.b) ^ true) && !(wg6.a(this.c, x50.c) ^ true) && !(wg6.a(this.d, x50.d) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.complication.Complication");
    }

    @DexIgnore
    public final z50 getId() {
        return this.a;
    }

    @DexIgnore
    public final lc0 getPositionConfig() {
        return this.c;
    }

    @DexIgnore
    public final mc0 getThemeConfig() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = this.b.hashCode();
        int hashCode2 = this.c.hashCode();
        return this.d.hashCode() + ((hashCode2 + ((hashCode + (this.a.hashCode() * 31)) * 31)) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.a.name());
        }
        if (parcel != null) {
            parcel.writeParcelable(this.b, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.d, i);
        }
    }

    @DexIgnore
    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("name", this.a.a()).put("pos", this.c.a()).put("data", this.b.a()).put("theme", this.d.a());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        return jSONObject;
    }

    @DexIgnore
    public x50(z50 z50, ic0 ic0, lc0 lc0, mc0 mc0) {
        this.a = z50;
        this.b = ic0;
        this.c = lc0;
        this.d = mc0;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public x50(Parcel parcel) {
        this(r0, r2, r3, (mc0) r6);
        String readString = parcel.readString();
        if (readString != null) {
            wg6.a(readString, "parcel.readString()!!");
            z50 valueOf = z50.valueOf(readString);
            Parcelable readParcelable = parcel.readParcelable(ic0.class.getClassLoader());
            if (readParcelable != null) {
                ic0 ic0 = (ic0) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(lc0.class.getClassLoader());
                if (readParcelable2 != null) {
                    lc0 lc0 = (lc0) readParcelable2;
                    Parcelable readParcelable3 = parcel.readParcelable(mc0.class.getClassLoader());
                    if (readParcelable3 != null) {
                    } else {
                        wg6.a();
                        throw null;
                    }
                } else {
                    wg6.a();
                    throw null;
                }
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
