package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class oc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ fc0[] c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<oc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(s90.class.getClassLoader());
            if (readParcelable != null) {
                return new oc0((s90) readParcelable, (uc0) parcel.readParcelable(uc0.class.getClassLoader()), (fc0[]) parcel.createTypedArray(fc0.CREATOR));
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new oc0[i];
        }
    }

    @DexIgnore
    public oc0(s90 s90, uc0 uc0, fc0[] fc0Arr) {
        super(s90, uc0);
        this.c = fc0Arr;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.c != null) {
                JSONArray jSONArray = new JSONArray();
                for (fc0 b : this.c) {
                    jSONArray.put(b.b());
                }
                jSONObject.put("buddyChallengeApp._.config.challenge_list", jSONArray);
            } else {
                getDeviceMessage();
            }
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        wg6.a(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = mi0.A.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public JSONObject b() {
        JSONObject b = super.b();
        bm0 bm0 = bm0.CHALLENGES;
        fc0[] fc0Arr = this.c;
        return cw0.a(b, bm0, fc0Arr != null ? cw0.a((p40[]) fc0Arr) : JSONObject.NULL);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        fc0[] fc0Arr;
        if (this == obj) {
            return true;
        }
        if (!wg6.a(oc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            oc0 oc0 = (oc0) obj;
            fc0[] fc0Arr2 = this.c;
            if (fc0Arr2 == null || (fc0Arr = oc0.c) == null) {
                if (this.c == null || oc0.c == null) {
                    return false;
                }
            } else if (!Arrays.equals(fc0Arr2, fc0Arr)) {
                return false;
            }
            return true;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.BuddyChallengeWatchAppListChallengesData");
    }

    @DexIgnore
    public final fc0[] getBuddyChallenges() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        fc0[] fc0Arr = this.c;
        return hashCode + (fc0Arr != null ? fc0Arr.hashCode() : 0);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
    }

    @DexIgnore
    public oc0(s90 s90, fc0[] fc0Arr) {
        super(s90, (uc0) null);
        this.c = fc0Arr;
    }
}
