package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class c84 extends b84 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j s; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray t; // = new SparseIntArray();
    @DexIgnore
    public /* final */ ConstraintLayout q;
    @DexIgnore
    public long r;

    /*
    static {
        t.put(2131363218, 1);
    }
    */

    @DexIgnore
    public c84(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 2, s, t));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.r = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.r != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.r = 1;
        }
        g();
    }

    @DexIgnore
    public c84(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[1]);
        this.r = -1;
        this.q = objArr[0];
        this.q.setTag((Object) null);
        a(view);
        f();
    }
}
