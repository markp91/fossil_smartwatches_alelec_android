package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xc2 extends uc2 implements wc2 {
    @DexIgnore
    public xc2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.fitness.internal.IDataSourcesCallback");
    }

    @DexIgnore
    public final void a(o82 o82) throws RemoteException {
        Parcel zza = zza();
        sd2.a(zza, (Parcelable) o82);
        b(1, zza);
    }
}
