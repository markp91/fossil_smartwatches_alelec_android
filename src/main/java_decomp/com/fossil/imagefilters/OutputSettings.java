package com.fossil.imagefilters;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class OutputSettings {
    @DexIgnore
    public static /* final */ OutputSettings BACKGROUND; // = new OutputSettings(240, 240, OutputFormat.BACKGROUND);
    @DexIgnore
    public static /* final */ OutputSettings ICON; // = new OutputSettings(24, 24, OutputFormat.ICON);
    @DexIgnore
    public /* final */ OutputFormat mFormat;
    @DexIgnore
    public /* final */ int mHeight;
    @DexIgnore
    public /* final */ int mWidth;

    @DexIgnore
    public OutputSettings(int i, int i2, OutputFormat outputFormat) {
        this.mWidth = i;
        this.mHeight = i2;
        this.mFormat = outputFormat;
    }

    @DexIgnore
    public OutputFormat getFormat() {
        return this.mFormat;
    }

    @DexIgnore
    public int getHeight() {
        return this.mHeight;
    }

    @DexIgnore
    public int getWidth() {
        return this.mWidth;
    }

    @DexIgnore
    public String toString() {
        return "OutputSettings{mWidth=" + this.mWidth + ",mHeight=" + this.mHeight + ",mFormat=" + this.mFormat + "}";
    }
}
