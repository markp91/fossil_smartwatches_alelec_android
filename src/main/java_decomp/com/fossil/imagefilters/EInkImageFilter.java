package com.fossil.imagefilters;

import android.graphics.Bitmap;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class EInkImageFilter {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CppProxy extends EInkImageFilter {
        @DexIgnore
        public static /* final */ /* synthetic */ boolean $assertionsDisabled; // = false;
        @DexIgnore
        public /* final */ AtomicBoolean destroyed; // = new AtomicBoolean(false);
        @DexIgnore
        public /* final */ long nativeRef;

        @DexIgnore
        public CppProxy(long j) {
            if (j != 0) {
                this.nativeRef = j;
                return;
            }
            throw new RuntimeException("nativeRef is zero");
        }

        @DexIgnore
        public static native EInkImageFilter create();

        @DexIgnore
        private native void nativeDestroy(long j);

        @DexIgnore
        private native FilterResult native_apply(long j, Bitmap bitmap, FilterType filterType, boolean z, boolean z2, OutputSettings outputSettings);

        @DexIgnore
        private native ArrayList<FilterResult> native_applyFilters(long j, Bitmap bitmap, ArrayList<FilterType> arrayList, boolean z, boolean z2, OutputSettings outputSettings);

        @DexIgnore
        public void _djinni_private_destroy() {
            if (!this.destroyed.getAndSet(true)) {
                nativeDestroy(this.nativeRef);
            }
        }

        @DexIgnore
        public FilterResult apply(Bitmap bitmap, FilterType filterType, boolean z, boolean z2, OutputSettings outputSettings) {
            return native_apply(this.nativeRef, bitmap, filterType, z, z2, outputSettings);
        }

        @DexIgnore
        public ArrayList<FilterResult> applyFilters(Bitmap bitmap, ArrayList<FilterType> arrayList, boolean z, boolean z2, OutputSettings outputSettings) {
            return native_applyFilters(this.nativeRef, bitmap, arrayList, z, z2, outputSettings);
        }

        @DexIgnore
        public void finalize() throws Throwable {
            _djinni_private_destroy();
            super.finalize();
        }
    }

    @DexIgnore
    public static EInkImageFilter create() {
        return CppProxy.create();
    }

    @DexIgnore
    public abstract FilterResult apply(Bitmap bitmap, FilterType filterType, boolean z, boolean z2, OutputSettings outputSettings);

    @DexIgnore
    public abstract ArrayList<FilterResult> applyFilters(Bitmap bitmap, ArrayList<FilterType> arrayList, boolean z, boolean z2, OutputSettings outputSettings);
}
