package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cv1 {
    @DexIgnore
    public static /* final */ int common_full_open_on_phone; // = 2131230951;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark; // = 2131230952;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_focused; // = 2131230953;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal; // = 2131230954;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_dark_normal_background; // = 2131230955;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_disabled; // = 2131230956;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light; // = 2131230957;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_focused; // = 2131230958;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal; // = 2131230959;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_icon_light_normal_background; // = 2131230960;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark; // = 2131230961;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_focused; // = 2131230962;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal; // = 2131230963;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_dark_normal_background; // = 2131230964;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_disabled; // = 2131230965;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light; // = 2131230966;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_focused; // = 2131230967;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal; // = 2131230968;
    @DexIgnore
    public static /* final */ int common_google_signin_btn_text_light_normal_background; // = 2131230969;
    @DexIgnore
    public static /* final */ int googleg_disabled_color_18; // = 2131231002;
    @DexIgnore
    public static /* final */ int googleg_standard_color_18; // = 2131231003;
}
