package com.fossil;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import java.io.File;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uw {
    @DexIgnore
    public static /* final */ File f; // = new File("/proc/self/fd");
    @DexIgnore
    public static volatile uw g;
    @DexIgnore
    public /* final */ boolean a; // = c();
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = true;

    @DexIgnore
    public uw() {
        if (Build.VERSION.SDK_INT >= 28) {
            this.b = 20000;
            this.c = 0;
            return;
        }
        this.b = 700;
        this.c = 128;
    }

    @DexIgnore
    public static uw b() {
        if (g == null) {
            synchronized (uw.class) {
                if (g == null) {
                    g = new uw();
                }
            }
        }
        return g;
    }

    @DexIgnore
    /* JADX WARNING: Can't fix incorrect switch cases order */
    public static boolean c() {
        char c2;
        String str = Build.MODEL;
        if (str == null || str.length() < 7) {
            return true;
        }
        String substring = Build.MODEL.substring(0, 7);
        switch (substring.hashCode()) {
            case -1398613787:
                if (substring.equals("SM-A520")) {
                    c2 = 6;
                    break;
                }
            case -1398431166:
                if (substring.equals("SM-G930")) {
                    c2 = 5;
                    break;
                }
            case -1398431161:
                if (substring.equals("SM-G935")) {
                    c2 = 4;
                    break;
                }
            case -1398431073:
                if (substring.equals("SM-G960")) {
                    c2 = 2;
                    break;
                }
            case -1398431068:
                if (substring.equals("SM-G965")) {
                    c2 = 3;
                    break;
                }
            case -1398343746:
                if (substring.equals("SM-J720")) {
                    c2 = 1;
                    break;
                }
            case -1398222624:
                if (substring.equals("SM-N935")) {
                    c2 = 0;
                    break;
                }
            default:
                c2 = 65535;
                break;
        }
        switch (c2) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                if (Build.VERSION.SDK_INT != 26) {
                    return true;
                }
                return false;
            default:
                return true;
        }
    }

    @DexIgnore
    public boolean a(int i, int i2, boolean z, boolean z2) {
        int i3;
        if (!z || !this.a || Build.VERSION.SDK_INT < 26 || z2 || i < (i3 = this.c) || i2 < i3 || !a()) {
            return false;
        }
        return true;
    }

    @DexIgnore
    @TargetApi(26)
    public boolean a(int i, int i2, BitmapFactory.Options options, boolean z, boolean z2) {
        boolean a2 = a(i, i2, z, z2);
        if (a2) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return a2;
    }

    @DexIgnore
    public final synchronized boolean a() {
        int i = this.d + 1;
        this.d = i;
        if (i >= 50) {
            boolean z = false;
            this.d = 0;
            int length = f.list().length;
            if (length < this.b) {
                z = true;
            }
            this.e = z;
            if (!this.e && Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + this.b);
            }
        }
        return this.e;
    }
}
