package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ri4 extends ii4 {
    @DexIgnore
    public /* final */ int a;
    @DexIgnore
    public /* final */ Boolean b;

    @DexIgnore
    public ri4(int i, boolean z) {
        this.a = i;
        this.b = Boolean.valueOf(z);
    }

    @DexIgnore
    public int a() {
        return this.a;
    }

    @DexIgnore
    public boolean b() {
        return this.b.booleanValue();
    }
}
