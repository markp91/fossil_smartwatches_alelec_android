package com.fossil;

import com.fossil.bm3;
import com.fossil.gm3;
import com.fossil.im3;
import com.fossil.nm3;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jm3<K, V> extends gm3<K, V> implements xn3<K, V> {
    @DexIgnore
    public static /* final */ long serialVersionUID; // = 0;
    @DexIgnore
    public /* final */ transient im3<V> f;
    @DexIgnore
    public transient jm3<V, K> g;
    @DexIgnore
    public transient im3<Map.Entry<K, V>> h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b<K, V> extends im3<Map.Entry<K, V>> {
        @DexIgnore
        public /* final */ transient jm3<K, V> b;

        @DexIgnore
        public b(jm3<K, V> jm3) {
            this.b = jm3;
        }

        @DexIgnore
        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return this.b.containsEntry(entry.getKey(), entry.getValue());
        }

        @DexIgnore
        public boolean isPartialView() {
            return false;
        }

        @DexIgnore
        public int size() {
            return this.b.size();
        }

        @DexIgnore
        public jo3<Map.Entry<K, V>> iterator() {
            return this.b.entryIterator();
        }
    }

    @DexIgnore
    public jm3(bm3<K, im3<V>> bm3, int i, Comparator<? super V> comparator) {
        super(bm3, i);
        this.f = a(comparator);
    }

    @DexIgnore
    public static <K, V> jm3<K, V> a(zm3<? extends K, ? extends V> zm3, Comparator<? super V> comparator) {
        jk3.a(zm3);
        if (zm3.isEmpty() && comparator == null) {
            return of();
        }
        if (zm3 instanceof jm3) {
            jm3<K, V> jm3 = (jm3) zm3;
            if (!jm3.isPartialView()) {
                return jm3;
            }
        }
        bm3.b bVar = new bm3.b(zm3.asMap().size());
        int i = 0;
        for (Map.Entry next : zm3.asMap().entrySet()) {
            Object key = next.getKey();
            im3<V> a2 = a(comparator, (Collection) next.getValue());
            if (!a2.isEmpty()) {
                bVar.a(key, a2);
                i += a2.size();
            }
        }
        return new jm3<>(bVar.a(), i, comparator);
    }

    @DexIgnore
    public static <V> im3.a<V> b(Comparator<? super V> comparator) {
        return comparator == null ? new im3.a<>() : new nm3.a(comparator);
    }

    @DexIgnore
    public static <K, V> a<K, V> builder() {
        return new a<>();
    }

    @DexIgnore
    public static <K, V> jm3<K, V> copyOf(zm3<? extends K, ? extends V> zm3) {
        return a(zm3, (Comparator) null);
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of() {
        return il3.INSTANCE;
    }

    @DexIgnore
    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        Comparator comparator = (Comparator) objectInputStream.readObject();
        int readInt = objectInputStream.readInt();
        if (readInt >= 0) {
            bm3.b builder = bm3.builder();
            int i = 0;
            int i2 = 0;
            while (i < readInt) {
                Object readObject = objectInputStream.readObject();
                int readInt2 = objectInputStream.readInt();
                if (readInt2 > 0) {
                    im3.a b2 = b(comparator);
                    for (int i3 = 0; i3 < readInt2; i3++) {
                        b2.a(objectInputStream.readObject());
                    }
                    im3 a2 = b2.a();
                    if (a2.size() == readInt2) {
                        builder.a(readObject, a2);
                        i2 += readInt2;
                        i++;
                    } else {
                        throw new InvalidObjectException("Duplicate key-value pairs exist for key " + readObject);
                    }
                } else {
                    throw new InvalidObjectException("Invalid value count " + readInt2);
                }
            }
            try {
                gm3.e.a.a(this, (Object) builder.a());
                gm3.e.b.a(this, i2);
                gm3.e.c.a(this, (Object) a(comparator));
            } catch (IllegalArgumentException e) {
                throw ((InvalidObjectException) new InvalidObjectException(e.getMessage()).initCause(e));
            }
        } else {
            throw new InvalidObjectException("Invalid key count " + readInt);
        }
    }

    @DexIgnore
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        objectOutputStream.writeObject(valueComparator());
        wn3.a(this, objectOutputStream);
    }

    @DexIgnore
    public Comparator<? super V> valueComparator() {
        im3<V> im3 = this.f;
        if (im3 instanceof nm3) {
            return ((nm3) im3).comparator();
        }
        return null;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a<K, V> extends gm3.c<K, V> {
        @DexIgnore
        public a() {
            super(bn3.a().c().b());
        }

        @DexIgnore
        public a<K, V> a(K k, V v) {
            zm3<K, V> zm3 = this.a;
            jk3.a(k);
            jk3.a(v);
            zm3.put(k, v);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Map.Entry<? extends K, ? extends V> entry) {
            zm3<K, V> zm3 = this.a;
            Object key = entry.getKey();
            jk3.a(key);
            Object value = entry.getValue();
            jk3.a(value);
            zm3.put(key, value);
            return this;
        }

        @DexIgnore
        public a<K, V> a(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
            super.a(iterable);
            return this;
        }

        @DexIgnore
        public jm3<K, V> a() {
            if (this.b != null) {
                xn3<K, V> b = bn3.a().c().b();
                for (E e : jn3.from(this.b).onKeys().immutableSortedCopy(this.a.asMap().entrySet())) {
                    b.putAll(e.getKey(), (Iterable) e.getValue());
                }
                this.a = b;
            }
            return jm3.a(this.a, this.c);
        }
    }

    @DexIgnore
    public static <K, V> jm3<K, V> copyOf(Iterable<? extends Map.Entry<? extends K, ? extends V>> iterable) {
        a aVar = new a();
        aVar.a(iterable);
        return aVar.a();
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of(K k, V v) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        return builder.a();
    }

    @DexIgnore
    public jm3<V, K> inverse() {
        jm3<V, K> jm3 = this.g;
        if (jm3 != null) {
            return jm3;
        }
        jm3<V, K> a2 = a();
        this.g = a2;
        return a2;
    }

    @DexIgnore
    public im3<Map.Entry<K, V>> entries() {
        im3<Map.Entry<K, V>> im3 = this.h;
        if (im3 != null) {
            return im3;
        }
        b bVar = new b(this);
        this.h = bVar;
        return bVar;
    }

    @DexIgnore
    public im3<V> get(K k) {
        return (im3) fk3.a((im3) this.map.get(k), this.f);
    }

    @DexIgnore
    @Deprecated
    public im3<V> removeAll(Object obj) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    @Deprecated
    public im3<V> replaceValues(K k, Iterable<? extends V> iterable) {
        throw new UnsupportedOperationException();
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of(K k, V v, K k2, V v2) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        return builder.a();
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        return builder.a();
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        return builder.a();
    }

    @DexIgnore
    public final jm3<V, K> a() {
        a builder = builder();
        Iterator it = entries().iterator();
        while (it.hasNext()) {
            Map.Entry entry = (Map.Entry) it.next();
            builder.a(entry.getValue(), entry.getKey());
        }
        jm3<V, K> a2 = builder.a();
        a2.g = this;
        return a2;
    }

    @DexIgnore
    public static <K, V> jm3<K, V> of(K k, V v, K k2, V v2, K k3, V v3, K k4, V v4, K k5, V v5) {
        a builder = builder();
        builder.a((Object) k, (Object) v);
        builder.a((Object) k2, (Object) v2);
        builder.a((Object) k3, (Object) v3);
        builder.a((Object) k4, (Object) v4);
        builder.a((Object) k5, (Object) v5);
        return builder.a();
    }

    @DexIgnore
    public static <V> im3<V> a(Comparator<? super V> comparator, Collection<? extends V> collection) {
        return comparator == null ? im3.copyOf(collection) : nm3.copyOf(comparator, collection);
    }

    @DexIgnore
    public static <V> im3<V> a(Comparator<? super V> comparator) {
        return comparator == null ? im3.of() : nm3.emptySet(comparator);
    }
}
