package com.fossil;

import java.util.Arrays;
import java.util.Hashtable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zm1 {
    @DexIgnore
    public byte[] a;
    @DexIgnore
    public byte[] b; // = new byte[8];
    @DexIgnore
    public byte[] c; // = new byte[8];
    @DexIgnore
    public /* final */ Hashtable<rg1, ci0> d; // = new Hashtable<>();

    @DexIgnore
    public final byte[] a() {
        byte[] bArr = this.a;
        if (bArr == null) {
            return null;
        }
        if (bArr.length <= 16) {
            return bArr;
        }
        byte[] copyOf = Arrays.copyOf(bArr, 16);
        wg6.a(copyOf, "java.util.Arrays.copyOf(this, newSize)");
        return copyOf;
    }

    @DexIgnore
    public final void b(rg1 rg1) {
        ci0 a2 = a(rg1);
        byte[] bArr = this.b;
        byte[] bArr2 = this.c;
        if (!Arrays.equals(a2.b, bArr) || !Arrays.equals(a2.c, bArr2)) {
            a2.d = 0;
        }
        a2.b = bArr;
        a2.c = bArr2;
        a2.d++;
        a2.e = 0;
    }

    @DexIgnore
    public final void a(byte[] bArr, byte[] bArr2) {
        if (bArr.length == 8 && bArr2.length == 8) {
            this.b = bArr;
            this.c = bArr2;
        }
    }

    @DexIgnore
    public final ci0 a(rg1 rg1) {
        ci0 ci0 = this.d.get(rg1);
        if (ci0 == null) {
            ci0 = new ci0(rg1, this.b, this.c, 0, 0);
        }
        this.d.put(rg1, ci0);
        return ci0;
    }
}
