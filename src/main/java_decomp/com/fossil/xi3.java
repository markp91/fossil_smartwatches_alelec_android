package com.fossil;

import android.graphics.RectF;
import java.util.Arrays;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class xi3 implements zi3 {
    @DexIgnore
    public /* final */ float a;

    @DexIgnore
    public xi3(float f) {
        this.a = f;
    }

    @DexIgnore
    public float a(RectF rectF) {
        return this.a;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj instanceof xi3) && this.a == ((xi3) obj).a) {
            return true;
        }
        return false;
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(new Object[]{Float.valueOf(this.a)});
    }
}
