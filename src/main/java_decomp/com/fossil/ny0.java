package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract /* synthetic */ class ny0 {
    @DexIgnore
    public static /* final */ /* synthetic */ int[] a; // = new int[w31.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] b; // = new int[ud0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] c; // = new int[td0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] d; // = new int[xh0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] e; // = new int[cd0.values().length];
    @DexIgnore
    public static /* final */ /* synthetic */ int[] f; // = new int[x61.values().length];

    /*
    static {
        a[w31.HARDWARE_LOG.ordinal()] = 1;
        a[w31.ALARM.ordinal()] = 2;
        a[w31.ACTIVITY_FILE.ordinal()] = 3;
        a[w31.NOTIFICATION_FILTER.ordinal()] = 4;
        a[w31.DEVICE_CONFIG.ordinal()] = 5;
        a[w31.DATA_COLLECTION_FILE.ordinal()] = 6;
        a[w31.OTA.ordinal()] = 7;
        a[w31.FONT.ordinal()] = 8;
        a[w31.MUSIC_CONTROL.ordinal()] = 9;
        a[w31.UI_SCRIPT.ordinal()] = 10;
        a[w31.ASSET.ordinal()] = 11;
        a[w31.NOTIFICATION.ordinal()] = 12;
        a[w31.DEVICE_INFO.ordinal()] = 13;
        a[w31.ALL_FILE.ordinal()] = 14;
        a[w31.MICRO_APP.ordinal()] = 15;
        a[w31.WATCH_PARAMETERS_FILE.ordinal()] = 16;
        a[w31.UI_PACKAGE_FILE.ordinal()] = 17;
        a[w31.LUTS_FILE.ordinal()] = 18;
        a[w31.RATE_FILE.ordinal()] = 19;
        a[w31.REPLY_MESSAGES_FILE.ordinal()] = 20;
        b[ud0.ETA.ordinal()] = 1;
        b[ud0.TRAVEL.ordinal()] = 2;
        c[td0.COMMUTE_TIME.ordinal()] = 1;
        c[td0.RING_PHONE.ordinal()] = 2;
        d[xh0.JSON_FILE_EVENT.ordinal()] = 1;
        d[xh0.HEARTBEAT_EVENT.ordinal()] = 2;
        d[xh0.SERVICE_CHANGE_EVENT.ordinal()] = 3;
        d[xh0.CONNECTION_PARAM_CHANGE_EVENT.ordinal()] = 4;
        d[xh0.APP_NOTIFICATION_EVENT.ordinal()] = 5;
        d[xh0.MUSIC_EVENT.ordinal()] = 6;
        d[xh0.BACKGROUND_SYNC_EVENT.ordinal()] = 7;
        d[xh0.MICRO_APP_EVENT.ordinal()] = 8;
        d[xh0.TIME_SYNC_EVENT.ordinal()] = 9;
        d[xh0.AUTHENTICATION_REQUEST_EVENT.ordinal()] = 10;
        d[xh0.BATTERY_EVENT.ordinal()] = 11;
        d[xh0.ENCRYPTED_DATA.ordinal()] = 12;
        e[cd0.START.ordinal()] = 1;
        e[cd0.STOP.ordinal()] = 2;
        f[x61.UPDATE_CHALLENGE_INFO.ordinal()] = 1;
        f[x61.GET_CHALLENGE_INFO.ordinal()] = 2;
        f[x61.LIST_CHALLENGES.ordinal()] = 3;
    }
    */
}
