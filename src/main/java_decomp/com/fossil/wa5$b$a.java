package com.fossil;

import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.dashboard.HomeDashboardPresenter$checkDeviceStatus$1$activityStatistic$1", f = "HomeDashboardPresenter.kt", l = {106}, m = "invokeSuspend")
public final class wa5$b$a extends sf6 implements ig6<il6, xe6<? super ActivityStatistic>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ HomeDashboardPresenter.b this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public wa5$b$a(HomeDashboardPresenter.b bVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = bVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        wa5$b$a wa5_b_a = new wa5$b$a(this.this$0, xe6);
        wa5_b_a.p$ = (il6) obj;
        return wa5_b_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((wa5$b$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            SummariesRepository n = this.this$0.this$0.A;
            this.L$0 = il6;
            this.label = 1;
            obj = n.getActivityStatisticAwait(this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return obj;
    }
}
