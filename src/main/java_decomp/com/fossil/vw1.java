package com.fossil;

import android.os.Looper;
import com.fossil.uw1;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vw1 {
    @DexIgnore
    public /* final */ Set<uw1<?>> a; // = Collections.newSetFromMap(new WeakHashMap());

    @DexIgnore
    public final void a() {
        for (uw1<?> a2 : this.a) {
            a2.a();
        }
        this.a.clear();
    }

    @DexIgnore
    public static <L> uw1<L> a(L l, Looper looper, String str) {
        w12.a(l, (Object) "Listener must not be null");
        w12.a(looper, (Object) "Looper must not be null");
        w12.a(str, (Object) "Listener type must not be null");
        return new uw1<>(looper, l, str);
    }

    @DexIgnore
    public static <L> uw1.a<L> a(L l, String str) {
        w12.a(l, (Object) "Listener must not be null");
        w12.a(str, (Object) "Listener type must not be null");
        w12.a(str, (Object) "Listener type must not be empty");
        return new uw1.a<>(l, str);
    }
}
