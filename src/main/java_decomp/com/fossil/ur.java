package com.fossil;

import com.bumptech.glide.load.ImageHeaderParser;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ur {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements g {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;

        @DexIgnore
        public a(InputStream inputStream) {
            this.a = inputStream;
        }

        @DexIgnore
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.a(this.a);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b implements g {
        @DexIgnore
        public /* final */ /* synthetic */ ByteBuffer a;

        @DexIgnore
        public b(ByteBuffer byteBuffer) {
            this.a = byteBuffer;
        }

        @DexIgnore
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            return imageHeaderParser.a(this.a);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements g {
        @DexIgnore
        public /* final */ /* synthetic */ os a;
        @DexIgnore
        public /* final */ /* synthetic */ xt b;

        @DexIgnore
        public c(os osVar, xt xtVar) {
            this.a = osVar;
            this.b = xtVar;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x002a A[SYNTHETIC, Splitter:B:14:0x002a] */
        public ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException {
            zw zwVar = null;
            try {
                zw zwVar2 = new zw(new FileInputStream(this.a.b().getFileDescriptor()), this.b);
                try {
                    ImageHeaderParser.ImageType a2 = imageHeaderParser.a((InputStream) zwVar2);
                    try {
                        zwVar2.close();
                    } catch (IOException unused) {
                    }
                    this.a.b();
                    return a2;
                } catch (Throwable th) {
                    th = th;
                    zwVar = zwVar2;
                    if (zwVar != null) {
                        try {
                            zwVar.close();
                        } catch (IOException unused2) {
                        }
                    }
                    this.a.b();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (zwVar != null) {
                }
                this.a.b();
                throw th;
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements f {
        @DexIgnore
        public /* final */ /* synthetic */ InputStream a;
        @DexIgnore
        public /* final */ /* synthetic */ xt b;

        @DexIgnore
        public d(InputStream inputStream, xt xtVar) {
            this.a = inputStream;
            this.b = xtVar;
        }

        @DexIgnore
        public int a(ImageHeaderParser imageHeaderParser) throws IOException {
            try {
                return imageHeaderParser.a(this.a, this.b);
            } finally {
                this.a.reset();
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements f {
        @DexIgnore
        public /* final */ /* synthetic */ os a;
        @DexIgnore
        public /* final */ /* synthetic */ xt b;

        @DexIgnore
        public e(os osVar, xt xtVar) {
            this.a = osVar;
            this.b = xtVar;
        }

        @DexIgnore
        /* JADX WARNING: Removed duplicated region for block: B:14:0x002c A[SYNTHETIC, Splitter:B:14:0x002c] */
        public int a(ImageHeaderParser imageHeaderParser) throws IOException {
            zw zwVar = null;
            try {
                zw zwVar2 = new zw(new FileInputStream(this.a.b().getFileDescriptor()), this.b);
                try {
                    int a2 = imageHeaderParser.a(zwVar2, this.b);
                    try {
                        zwVar2.close();
                    } catch (IOException unused) {
                    }
                    this.a.b();
                    return a2;
                } catch (Throwable th) {
                    th = th;
                    zwVar = zwVar2;
                    if (zwVar != null) {
                        try {
                            zwVar.close();
                        } catch (IOException unused2) {
                        }
                    }
                    this.a.b();
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                if (zwVar != null) {
                }
                this.a.b();
                throw th;
            }
        }
    }

    @DexIgnore
    public interface f {
        @DexIgnore
        int a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public interface g {
        @DexIgnore
        ImageHeaderParser.ImageType a(ImageHeaderParser imageHeaderParser) throws IOException;
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType a(List<ImageHeaderParser> list, ByteBuffer byteBuffer) throws IOException {
        if (byteBuffer == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        return a(list, (g) new b(byteBuffer));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType b(List<ImageHeaderParser> list, InputStream inputStream, xt xtVar) throws IOException {
        if (inputStream == null) {
            return ImageHeaderParser.ImageType.UNKNOWN;
        }
        if (!inputStream.markSupported()) {
            inputStream = new zw(inputStream, xtVar);
        }
        inputStream.mark(5242880);
        return a(list, (g) new a(inputStream));
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType a(List<ImageHeaderParser> list, g gVar) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            ImageHeaderParser.ImageType a2 = gVar.a(list.get(i));
            if (a2 != ImageHeaderParser.ImageType.UNKNOWN) {
                return a2;
            }
        }
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    @DexIgnore
    public static ImageHeaderParser.ImageType b(List<ImageHeaderParser> list, os osVar, xt xtVar) throws IOException {
        return a(list, (g) new c(osVar, xtVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, InputStream inputStream, xt xtVar) throws IOException {
        if (inputStream == null) {
            return -1;
        }
        if (!inputStream.markSupported()) {
            inputStream = new zw(inputStream, xtVar);
        }
        inputStream.mark(5242880);
        return a(list, (f) new d(inputStream, xtVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, os osVar, xt xtVar) throws IOException {
        return a(list, (f) new e(osVar, xtVar));
    }

    @DexIgnore
    public static int a(List<ImageHeaderParser> list, f fVar) throws IOException {
        int size = list.size();
        for (int i = 0; i < size; i++) {
            int a2 = fVar.a(list.get(i));
            if (a2 != -1) {
                return a2;
            }
        }
        return -1;
    }
}
