package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class w90 extends x90 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ String d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<w90> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new w90(parcel, (qg6) null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new w90[i];
        }
    }

    @DexIgnore
    public w90(byte b, int i, String str) {
        super(e90.COMMUTE_TIME_WATCH_APP, b, i);
        this.d = str;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if ((!wg6.a(w90.class, obj != null ? obj.getClass() : null)) || !super.equals(obj)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.d, ((w90) obj).d) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.event.request.CommuteTimeWatchAppRequest");
    }

    @DexIgnore
    public final String getDestination() {
        return this.d;
    }

    @DexIgnore
    public int hashCode() {
        return this.d.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public /* synthetic */ w90(Parcel parcel, qg6 qg6) {
        super(parcel);
        String readString = parcel.readString();
        if (readString != null) {
            this.d = readString;
        } else {
            wg6.a();
            throw null;
        }
    }
}
