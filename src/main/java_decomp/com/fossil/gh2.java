package com.fossil;

import android.os.Looper;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class gh2 implements vv2 {
    @DexIgnore
    public final yv1<Status> a(wv1 wv1, zv2 zv2) {
        return wv1.b(new ih2(this, wv1, zv2));
    }

    @DexIgnore
    public final yv1<Status> a(wv1 wv1, LocationRequest locationRequest, zv2 zv2) {
        w12.a(Looper.myLooper(), (Object) "Calling thread must be a prepared Looper thread.");
        return wv1.b(new hh2(this, wv1, locationRequest, zv2));
    }
}
