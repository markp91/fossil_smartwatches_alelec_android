package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc3 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ qc3 a;
    @DexIgnore
    public /* final */ /* synthetic */ yc3 b;

    @DexIgnore
    public zc3(yc3 yc3, qc3 qc3) {
        this.b = yc3;
        this.a = qc3;
    }

    @DexIgnore
    public final void run() {
        try {
            qc3 qc3 = (qc3) this.b.b.then(this.a);
            if (qc3 == null) {
                this.b.onFailure(new NullPointerException("Continuation returned null"));
                return;
            }
            qc3.a(sc3.b, this.b);
            qc3.a(sc3.b, (lc3) this.b);
            qc3.a(sc3.b, (jc3) this.b);
        } catch (oc3 e) {
            if (e.getCause() instanceof Exception) {
                this.b.c.a((Exception) e.getCause());
            } else {
                this.b.c.a((Exception) e);
            }
        } catch (Exception e2) {
            this.b.c.a(e2);
        }
    }
}
