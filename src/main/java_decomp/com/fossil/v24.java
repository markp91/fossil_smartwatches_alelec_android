package com.fossil;

import com.portfolio.platform.ApplicationEventListener;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.SleepSummariesRepository;
import com.portfolio.platform.data.source.SummariesRepository;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.WatchFaceRepository;
import com.portfolio.platform.data.source.WatchLocalizationRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.data.source.remote.GuestApiService;
import com.portfolio.platform.helper.AlarmHelper;
import com.portfolio.platform.helper.AnalyticsHelper;
import com.portfolio.platform.manager.validation.DataValidationManager;
import com.portfolio.platform.receiver.SmsMmsReceiver;
import com.portfolio.platform.service.ShakeFeedbackService;
import com.portfolio.platform.ui.user.usecase.DeleteLogoutUserUseCase;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class v24 implements MembersInjector<PortfolioApp> {
    @DexIgnore
    public static void a(PortfolioApp portfolioApp, an4 an4) {
        portfolioApp.c = an4;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, UserRepository userRepository) {
        portfolioApp.d = userRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SummariesRepository summariesRepository) {
        portfolioApp.e = summariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SleepSummariesRepository sleepSummariesRepository) {
        portfolioApp.f = sleepSummariesRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, AlarmHelper alarmHelper) {
        portfolioApp.g = alarmHelper;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, GuestApiService guestApiService) {
        portfolioApp.h = guestApiService;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, u04 u04) {
        portfolioApp.i = u04;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApiServiceV2 apiServiceV2) {
        portfolioApp.j = apiServiceV2;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ex5 ex5) {
        portfolioApp.o = ex5;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, z24 z24) {
        portfolioApp.p = z24;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DeleteLogoutUserUseCase deleteLogoutUserUseCase) {
        portfolioApp.q = deleteLogoutUserUseCase;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, AnalyticsHelper analyticsHelper) {
        portfolioApp.r = analyticsHelper;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ApplicationEventListener applicationEventListener) {
        portfolioApp.s = applicationEventListener;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DeviceRepository deviceRepository) {
        portfolioApp.t = deviceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ik4 ik4) {
        portfolioApp.u = ik4;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ShakeFeedbackService shakeFeedbackService) {
        portfolioApp.v = shakeFeedbackService;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DianaPresetRepository dianaPresetRepository) {
        portfolioApp.w = dianaPresetRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, d06 d06) {
        portfolioApp.x = d06;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchLocalizationRepository watchLocalizationRepository) {
        portfolioApp.y = watchLocalizationRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, DataValidationManager dataValidationManager) {
        portfolioApp.z = dataValidationManager;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, WatchFaceRepository watchFaceRepository) {
        portfolioApp.A = watchFaceRepository;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, so4 so4) {
        portfolioApp.O = so4;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, SmsMmsReceiver smsMmsReceiver) {
        portfolioApp.P = smsMmsReceiver;
    }

    @DexIgnore
    public static void a(PortfolioApp portfolioApp, ThemeRepository themeRepository) {
        portfolioApp.Q = themeRepository;
    }
}
