package com.fossil;

import android.text.TextUtils;
import java.text.BreakIterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cy5 {
    @DexIgnore
    public static CharSequence a(CharSequence charSequence) {
        if (charSequence != null) {
            return charSequence.toString().toLowerCase();
        }
        return null;
    }

    @DexIgnore
    public static CharSequence b(CharSequence charSequence) {
        if (charSequence != null) {
            return charSequence.toString().toUpperCase();
        }
        return null;
    }

    @DexIgnore
    public static CharSequence c(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return charSequence;
        }
        String charSequence2 = charSequence.toString();
        StringBuilder sb = new StringBuilder();
        int i = 0;
        while (true) {
            if (i >= charSequence2.length()) {
                break;
            } else if (Character.isAlphabetic(charSequence2.charAt(i))) {
                sb.append(Character.toUpperCase(charSequence2.charAt(i)));
                sb.append(charSequence2.substring(i + 1, charSequence2.length()).toLowerCase());
                break;
            } else {
                sb.append(charSequence2.charAt(i));
                i++;
            }
        }
        return sb.toString();
    }

    @DexIgnore
    public static CharSequence d(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return charSequence;
        }
        String[] split = charSequence.toString().toLowerCase().split(" ");
        StringBuilder sb = new StringBuilder();
        for (String str : split) {
            if (!str.isEmpty()) {
                sb.append(str.substring(0, 1).toUpperCase() + str.substring(1));
                sb.append(" ");
            }
        }
        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    @DexIgnore
    public static CharSequence e(CharSequence charSequence) {
        if (TextUtils.isEmpty(charSequence)) {
            return charSequence;
        }
        BreakIterator sentenceInstance = BreakIterator.getSentenceInstance();
        StringBuilder sb = new StringBuilder();
        String charSequence2 = charSequence.toString();
        sentenceInstance.setText(charSequence2);
        int first = sentenceInstance.first();
        while (true) {
            int i = first;
            first = sentenceInstance.next();
            if (first == -1) {
                return sb.toString();
            }
            int i2 = i + 1;
            sb.append(charSequence2.substring(i, i2).toUpperCase());
            sb.append(charSequence2.substring(i2, first).toLowerCase());
        }
    }
}
