package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class t63 implements v63 {
    @DexIgnore
    public /* final */ x53 a;

    @DexIgnore
    public t63(x53 x53) {
        w12.a(x53);
        this.a = x53;
    }

    @DexIgnore
    public u53 a() {
        return this.a.a();
    }

    @DexIgnore
    public t43 b() {
        return this.a.b();
    }

    @DexIgnore
    public Context c() {
        return this.a.c();
    }

    @DexIgnore
    public bb3 d() {
        return this.a.d();
    }

    @DexIgnore
    public void e() {
        this.a.i();
    }

    @DexIgnore
    public void f() {
        this.a.a().f();
    }

    @DexIgnore
    public void g() {
        this.a.a().g();
    }

    @DexIgnore
    public d03 h() {
        return this.a.G();
    }

    @DexIgnore
    public r43 i() {
        return this.a.x();
    }

    @DexIgnore
    public ma3 j() {
        return this.a.w();
    }

    @DexIgnore
    public h53 k() {
        return this.a.q();
    }

    @DexIgnore
    public cb3 l() {
        return this.a.p();
    }

    @DexIgnore
    public k42 zzm() {
        return this.a.zzm();
    }
}
