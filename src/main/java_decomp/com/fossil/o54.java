package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class o54 extends n54 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j w; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray x; // = new SparseIntArray();
    @DexIgnore
    public long v;

    /*
    static {
        x.put(2131362081, 1);
        x.put(2131361851, 2);
        x.put(2131363218, 3);
        x.put(2131362695, 4);
        x.put(2131361951, 5);
        x.put(2131362495, 6);
        x.put(2131363270, 7);
        x.put(2131361945, 8);
        x.put(2131362493, 9);
        x.put(2131363271, 10);
        x.put(2131361943, 11);
        x.put(2131362492, 12);
    }
    */

    @DexIgnore
    public o54(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 13, w, x));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.v = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.v != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.v = 1;
        }
        g();
    }

    @DexIgnore
    public o54(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[11], objArr[8], objArr[5], objArr[1], objArr[12], objArr[9], objArr[6], objArr[4], objArr[0], objArr[3], objArr[7], objArr[10]);
        this.v = -1;
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
