package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum qq2 {
    INT(0),
    LONG(0L),
    FLOAT(Float.valueOf(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)),
    DOUBLE(Double.valueOf(0.0d)),
    BOOLEAN(false),
    STRING(""),
    BYTE_STRING(yl2.zza),
    ENUM((String) null),
    MESSAGE((String) null);
    
    @DexIgnore
    public /* final */ Object zzj;

    @DexIgnore
    public qq2(Object obj) {
        this.zzj = obj;
    }
}
