package com.fossil;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.ActionProvider;
import android.view.CollapsibleActionView;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import com.fossil.b9;
import java.lang.reflect.Method;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class u1 extends m1 implements MenuItem {
    @DexIgnore
    public /* final */ v7 d;
    @DexIgnore
    public Method e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends b9 {
        @DexIgnore
        public /* final */ ActionProvider a;

        @DexIgnore
        public a(Context context, ActionProvider actionProvider) {
            super(context);
            this.a = actionProvider;
        }

        @DexIgnore
        public boolean hasSubMenu() {
            return this.a.hasSubMenu();
        }

        @DexIgnore
        public View onCreateActionView() {
            return this.a.onCreateActionView();
        }

        @DexIgnore
        public boolean onPerformDefaultAction() {
            return this.a.onPerformDefaultAction();
        }

        @DexIgnore
        public void onPrepareSubMenu(SubMenu subMenu) {
            this.a.onPrepareSubMenu(u1.this.a(subMenu));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends a implements ActionProvider.VisibilityListener {
        @DexIgnore
        public b9.b c;

        @DexIgnore
        public b(u1 u1Var, Context context, ActionProvider actionProvider) {
            super(context, actionProvider);
        }

        @DexIgnore
        public boolean isVisible() {
            return this.a.isVisible();
        }

        @DexIgnore
        public void onActionProviderVisibilityChanged(boolean z) {
            b9.b bVar = this.c;
            if (bVar != null) {
                bVar.onActionProviderVisibilityChanged(z);
            }
        }

        @DexIgnore
        public View onCreateActionView(MenuItem menuItem) {
            return this.a.onCreateActionView(menuItem);
        }

        @DexIgnore
        public boolean overridesItemVisibility() {
            return this.a.overridesItemVisibility();
        }

        @DexIgnore
        public void refreshVisibility() {
            this.a.refreshVisibility();
        }

        @DexIgnore
        public void setVisibilityListener(b9.b bVar) {
            this.c = bVar;
            this.a.setVisibilityListener(bVar != null ? this : null);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class c extends FrameLayout implements d1 {
        @DexIgnore
        public /* final */ CollapsibleActionView a;

        @DexIgnore
        public c(View view) {
            super(view.getContext());
            this.a = (CollapsibleActionView) view;
            addView(view);
        }

        @DexIgnore
        public View a() {
            return (View) this.a;
        }

        @DexIgnore
        public void onActionViewCollapsed() {
            this.a.onActionViewCollapsed();
        }

        @DexIgnore
        public void onActionViewExpanded() {
            this.a.onActionViewExpanded();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class d implements MenuItem.OnActionExpandListener {
        @DexIgnore
        public /* final */ MenuItem.OnActionExpandListener a;

        @DexIgnore
        public d(MenuItem.OnActionExpandListener onActionExpandListener) {
            this.a = onActionExpandListener;
        }

        @DexIgnore
        public boolean onMenuItemActionCollapse(MenuItem menuItem) {
            return this.a.onMenuItemActionCollapse(u1.this.a(menuItem));
        }

        @DexIgnore
        public boolean onMenuItemActionExpand(MenuItem menuItem) {
            return this.a.onMenuItemActionExpand(u1.this.a(menuItem));
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class e implements MenuItem.OnMenuItemClickListener {
        @DexIgnore
        public /* final */ MenuItem.OnMenuItemClickListener a;

        @DexIgnore
        public e(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
            this.a = onMenuItemClickListener;
        }

        @DexIgnore
        public boolean onMenuItemClick(MenuItem menuItem) {
            return this.a.onMenuItemClick(u1.this.a(menuItem));
        }
    }

    @DexIgnore
    public u1(Context context, v7 v7Var) {
        super(context);
        if (v7Var != null) {
            this.d = v7Var;
            return;
        }
        throw new IllegalArgumentException("Wrapped Object can not be null.");
    }

    @DexIgnore
    public void a(boolean z) {
        try {
            if (this.e == null) {
                this.e = this.d.getClass().getDeclaredMethod("setExclusiveCheckable", new Class[]{Boolean.TYPE});
            }
            this.e.invoke(this.d, new Object[]{Boolean.valueOf(z)});
        } catch (Exception e2) {
            Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
        }
    }

    @DexIgnore
    public boolean collapseActionView() {
        return this.d.collapseActionView();
    }

    @DexIgnore
    public boolean expandActionView() {
        return this.d.expandActionView();
    }

    @DexIgnore
    public ActionProvider getActionProvider() {
        b9 a2 = this.d.a();
        if (a2 instanceof a) {
            return ((a) a2).a;
        }
        return null;
    }

    @DexIgnore
    public View getActionView() {
        View actionView = this.d.getActionView();
        return actionView instanceof c ? ((c) actionView).a() : actionView;
    }

    @DexIgnore
    public int getAlphabeticModifiers() {
        return this.d.getAlphabeticModifiers();
    }

    @DexIgnore
    public char getAlphabeticShortcut() {
        return this.d.getAlphabeticShortcut();
    }

    @DexIgnore
    public CharSequence getContentDescription() {
        return this.d.getContentDescription();
    }

    @DexIgnore
    public int getGroupId() {
        return this.d.getGroupId();
    }

    @DexIgnore
    public Drawable getIcon() {
        return this.d.getIcon();
    }

    @DexIgnore
    public ColorStateList getIconTintList() {
        return this.d.getIconTintList();
    }

    @DexIgnore
    public PorterDuff.Mode getIconTintMode() {
        return this.d.getIconTintMode();
    }

    @DexIgnore
    public Intent getIntent() {
        return this.d.getIntent();
    }

    @DexIgnore
    public int getItemId() {
        return this.d.getItemId();
    }

    @DexIgnore
    public ContextMenu.ContextMenuInfo getMenuInfo() {
        return this.d.getMenuInfo();
    }

    @DexIgnore
    public int getNumericModifiers() {
        return this.d.getNumericModifiers();
    }

    @DexIgnore
    public char getNumericShortcut() {
        return this.d.getNumericShortcut();
    }

    @DexIgnore
    public int getOrder() {
        return this.d.getOrder();
    }

    @DexIgnore
    public SubMenu getSubMenu() {
        return a(this.d.getSubMenu());
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.d.getTitle();
    }

    @DexIgnore
    public CharSequence getTitleCondensed() {
        return this.d.getTitleCondensed();
    }

    @DexIgnore
    public CharSequence getTooltipText() {
        return this.d.getTooltipText();
    }

    @DexIgnore
    public boolean hasSubMenu() {
        return this.d.hasSubMenu();
    }

    @DexIgnore
    public boolean isActionViewExpanded() {
        return this.d.isActionViewExpanded();
    }

    @DexIgnore
    public boolean isCheckable() {
        return this.d.isCheckable();
    }

    @DexIgnore
    public boolean isChecked() {
        return this.d.isChecked();
    }

    @DexIgnore
    public boolean isEnabled() {
        return this.d.isEnabled();
    }

    @DexIgnore
    public boolean isVisible() {
        return this.d.isVisible();
    }

    @DexIgnore
    public MenuItem setActionProvider(ActionProvider actionProvider) {
        b9 b9Var;
        if (Build.VERSION.SDK_INT >= 16) {
            b9Var = new b(this, this.a, actionProvider);
        } else {
            b9Var = new a(this.a, actionProvider);
        }
        v7 v7Var = this.d;
        if (actionProvider == null) {
            b9Var = null;
        }
        v7Var.a(b9Var);
        return this;
    }

    @DexIgnore
    public MenuItem setActionView(View view) {
        if (view instanceof CollapsibleActionView) {
            view = new c(view);
        }
        this.d.setActionView(view);
        return this;
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2) {
        this.d.setAlphabeticShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setCheckable(boolean z) {
        this.d.setCheckable(z);
        return this;
    }

    @DexIgnore
    public MenuItem setChecked(boolean z) {
        this.d.setChecked(z);
        return this;
    }

    @DexIgnore
    public MenuItem setContentDescription(CharSequence charSequence) {
        this.d.setContentDescription(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setEnabled(boolean z) {
        this.d.setEnabled(z);
        return this;
    }

    @DexIgnore
    public MenuItem setIcon(Drawable drawable) {
        this.d.setIcon(drawable);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintList(ColorStateList colorStateList) {
        this.d.setIconTintList(colorStateList);
        return this;
    }

    @DexIgnore
    public MenuItem setIconTintMode(PorterDuff.Mode mode) {
        this.d.setIconTintMode(mode);
        return this;
    }

    @DexIgnore
    public MenuItem setIntent(Intent intent) {
        this.d.setIntent(intent);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2) {
        this.d.setNumericShortcut(c2);
        return this;
    }

    @DexIgnore
    public MenuItem setOnActionExpandListener(MenuItem.OnActionExpandListener onActionExpandListener) {
        this.d.setOnActionExpandListener(onActionExpandListener != null ? new d(onActionExpandListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener onMenuItemClickListener) {
        this.d.setOnMenuItemClickListener(onMenuItemClickListener != null ? new e(onMenuItemClickListener) : null);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3) {
        this.d.setShortcut(c2, c3);
        return this;
    }

    @DexIgnore
    public void setShowAsAction(int i) {
        this.d.setShowAsAction(i);
    }

    @DexIgnore
    public MenuItem setShowAsActionFlags(int i) {
        this.d.setShowAsActionFlags(i);
        return this;
    }

    @DexIgnore
    public MenuItem setTitle(CharSequence charSequence) {
        this.d.setTitle(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTitleCondensed(CharSequence charSequence) {
        this.d.setTitleCondensed(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setTooltipText(CharSequence charSequence) {
        this.d.setTooltipText(charSequence);
        return this;
    }

    @DexIgnore
    public MenuItem setVisible(boolean z) {
        return this.d.setVisible(z);
    }

    @DexIgnore
    public MenuItem setAlphabeticShortcut(char c2, int i) {
        this.d.setAlphabeticShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setIcon(int i) {
        this.d.setIcon(i);
        return this;
    }

    @DexIgnore
    public MenuItem setNumericShortcut(char c2, int i) {
        this.d.setNumericShortcut(c2, i);
        return this;
    }

    @DexIgnore
    public MenuItem setShortcut(char c2, char c3, int i, int i2) {
        this.d.setShortcut(c2, c3, i, i2);
        return this;
    }

    @DexIgnore
    public MenuItem setTitle(int i) {
        this.d.setTitle(i);
        return this;
    }

    @DexIgnore
    public MenuItem setActionView(int i) {
        this.d.setActionView(i);
        View actionView = this.d.getActionView();
        if (actionView instanceof CollapsibleActionView) {
            this.d.setActionView((View) new c(actionView));
        }
        return this;
    }
}
