package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class bt1 implements Factory<zs1> {
    @DexIgnore
    public static /* final */ bt1 a; // = new bt1();

    @DexIgnore
    public static bt1 a() {
        return a;
    }

    @DexIgnore
    public static zs1 b() {
        zs1 a2 = at1.a();
        z76.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    @DexIgnore
    public zs1 get() {
        return b();
    }
}
