package com.fossil;

import com.portfolio.platform.ui.BaseActivity;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class eu5 {
    @DexIgnore
    public /* final */ zt5 a;

    @DexIgnore
    public eu5(BaseActivity baseActivity, zt5 zt5) {
        wg6.b(baseActivity, "mContext");
        wg6.b(zt5, "mView");
        this.a = zt5;
    }

    @DexIgnore
    public final zt5 a() {
        return this.a;
    }
}
