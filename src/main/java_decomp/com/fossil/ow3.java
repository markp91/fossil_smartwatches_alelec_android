package com.fossil;

import com.fossil.dx3;
import com.fossil.ow3;
import com.fossil.ow3.a;
import java.io.IOException;
import java.io.OutputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class ow3<MessageType extends ow3<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements dx3 {
    @DexIgnore
    public int a; // = 0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static abstract class a<MessageType extends ow3<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements dx3.a {
        @DexIgnore
        public static lx3 b(dx3 dx3) {
            return new lx3(dx3);
        }

        @DexIgnore
        public abstract BuilderType a(MessageType messagetype);

        @DexIgnore
        public BuilderType a(dx3 dx3) {
            if (a().getClass().isInstance(dx3)) {
                return a((ow3) dx3);
            }
            throw new IllegalArgumentException("mergeFrom(MessageLite) can only merge messages of the same type.");
        }
    }

    @DexIgnore
    public void a(OutputStream outputStream) throws IOException {
        uw3 a2 = uw3.a(outputStream, uw3.e(d()));
        a(a2);
        a2.a();
    }

    @DexIgnore
    public lx3 f() {
        return new lx3((dx3) this);
    }
}
