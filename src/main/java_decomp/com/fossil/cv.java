package com.fossil;

import android.net.Uri;
import android.text.TextUtils;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cv implements vr {
    @DexIgnore
    public /* final */ dv b;
    @DexIgnore
    public /* final */ URL c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public String e;
    @DexIgnore
    public URL f;
    @DexIgnore
    public volatile byte[] g;
    @DexIgnore
    public int h;

    @DexIgnore
    public cv(URL url) {
        this(url, dv.a);
    }

    @DexIgnore
    public String a() {
        String str = this.d;
        if (str != null) {
            return str;
        }
        URL url = this.c;
        q00.a(url);
        return url.toString();
    }

    @DexIgnore
    public final byte[] b() {
        if (this.g == null) {
            this.g = a().getBytes(vr.a);
        }
        return this.g;
    }

    @DexIgnore
    public Map<String, String> c() {
        return this.b.a();
    }

    @DexIgnore
    public final String d() {
        if (TextUtils.isEmpty(this.e)) {
            String str = this.d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.c;
                q00.a(url);
                str = url.toString();
            }
            this.e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.e;
    }

    @DexIgnore
    public final URL e() throws MalformedURLException {
        if (this.f == null) {
            this.f = new URL(d());
        }
        return this.f;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (!(obj instanceof cv)) {
            return false;
        }
        cv cvVar = (cv) obj;
        if (!a().equals(cvVar.a()) || !this.b.equals(cvVar.b)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public URL f() throws MalformedURLException {
        return e();
    }

    @DexIgnore
    public int hashCode() {
        if (this.h == 0) {
            this.h = a().hashCode();
            this.h = (this.h * 31) + this.b.hashCode();
        }
        return this.h;
    }

    @DexIgnore
    public String toString() {
        return a();
    }

    @DexIgnore
    public cv(String str) {
        this(str, dv.a);
    }

    @DexIgnore
    public void a(MessageDigest messageDigest) {
        messageDigest.update(b());
    }

    @DexIgnore
    public cv(URL url, dv dvVar) {
        q00.a(url);
        this.c = url;
        this.d = null;
        q00.a(dvVar);
        this.b = dvVar;
    }

    @DexIgnore
    public cv(String str, dv dvVar) {
        this.c = null;
        q00.a(str);
        this.d = str;
        q00.a(dvVar);
        this.b = dvVar;
    }
}
