package com.fossil;

import com.google.android.gms.common.api.Status;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class yt1 implements Runnable {
    @DexIgnore
    public static /* final */ y32 c; // = new y32("RevokeAccessOperation", new String[0]);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ ax1 b; // = new ax1((wv1) null);

    @DexIgnore
    public yt1(String str) {
        w12.b(str);
        this.a = str;
    }

    @DexIgnore
    public static yv1<Status> a(String str) {
        if (str == null) {
            return zv1.a(new Status(4), (wv1) null);
        }
        yt1 yt1 = new yt1(str);
        new Thread(yt1).start();
        return yt1.b;
    }

    @DexIgnore
    public final void run() {
        Status status = Status.g;
        try {
            String valueOf = String.valueOf(this.a);
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(valueOf.length() != 0 ? "https://accounts.google.com/o/oauth2/revoke?token=".concat(valueOf) : new String("https://accounts.google.com/o/oauth2/revoke?token=")).openConnection();
            httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                status = Status.e;
            } else {
                c.b("Unable to revoke access!", new Object[0]);
            }
            y32 y32 = c;
            StringBuilder sb = new StringBuilder(26);
            sb.append("Response Code: ");
            sb.append(responseCode);
            y32.a(sb.toString(), new Object[0]);
        } catch (IOException e) {
            y32 y322 = c;
            String valueOf2 = String.valueOf(e.toString());
            y322.b(valueOf2.length() != 0 ? "IOException when revoking access: ".concat(valueOf2) : new String("IOException when revoking access: "), new Object[0]);
        } catch (Exception e2) {
            y32 y323 = c;
            String valueOf3 = String.valueOf(e2.toString());
            y323.b(valueOf3.length() != 0 ? "Exception when revoking access: ".concat(valueOf3) : new String("Exception when revoking access: "), new Object[0]);
        }
        this.b.a(status);
    }
}
