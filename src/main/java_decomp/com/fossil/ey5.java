package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.portfolio.platform.PortfolioApp;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ey5 {
    @DexIgnore
    public static /* final */ ic6 b; // = jc6.a(a.INSTANCE);
    @DexIgnore
    public static /* final */ String c;
    @DexIgnore
    public static /* final */ b d; // = new b((qg6) null);
    @DexIgnore
    public an4 a;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends xg6 implements gg6<ey5> {
        @DexIgnore
        public static /* final */ a INSTANCE; // = new a();

        @DexIgnore
        public a() {
            super(0);
        }

        @DexIgnore
        public final ey5 invoke() {
            return c.b.a();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public static /* final */ /* synthetic */ li6[] a;

        /*
        static {
            dh6 dh6 = new dh6(kh6.a(b.class), "INSTANCE", "getINSTANCE()Lcom/portfolio/platform/util/UserUtils;");
            kh6.a((ch6) dh6);
            a = new li6[]{dh6};
        }
        */

        @DexIgnore
        public b() {
        }

        @DexIgnore
        public final ey5 a() {
            ic6 b = ey5.b;
            b bVar = ey5.d;
            li6 li6 = a[0];
            return (ey5) b.getValue();
        }

        @DexIgnore
        public /* synthetic */ b(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public static /* final */ ey5 a; // = new ey5();
        @DexIgnore
        public static /* final */ c b; // = new c();

        @DexIgnore
        public final ey5 a() {
            return a;
        }
    }

    /*
    static {
        String simpleName = ey5.class.getSimpleName();
        wg6.a((Object) simpleName, "UserUtils::class.java.simpleName");
        c = simpleName;
    }
    */

    @DexIgnore
    public ey5() {
        PortfolioApp.get.instance().g().a(this);
    }

    @DexIgnore
    public final boolean a(int i, int i2) {
        TimeUnit timeUnit = TimeUnit.MILLISECONDS;
        long currentTimeMillis = System.currentTimeMillis();
        an4 an4 = this.a;
        if (an4 != null) {
            int days = (int) timeUnit.toDays(currentTimeMillis - an4.o());
            FLogger.INSTANCE.getLocal().d(c, "Inside .isHappyUser");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = c;
            local.d(str, "crashThreshold = " + i + ", " + "successfulSyncThreshold = " + i2 + ", delta in Day = " + days);
            if (days >= i) {
                an4 an42 = this.a;
                if (an42 == null) {
                    wg6.d("mSharePrefs");
                    throw null;
                } else if (an42.e() >= i2) {
                    return true;
                }
            }
            return false;
        }
        wg6.d("mSharePrefs");
        throw null;
    }

    @DexIgnore
    public final boolean a() {
        an4 an4 = this.a;
        if (an4 != null) {
            return an4.L();
        }
        wg6.d("mSharePrefs");
        throw null;
    }
}
