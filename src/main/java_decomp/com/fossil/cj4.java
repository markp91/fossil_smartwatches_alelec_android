package com.fossil;

import com.misfit.frameworks.buttonservice.utils.FossilDeviceSerialPatternUtil;
import com.portfolio.platform.ui.device.domain.usecase.DianaSyncUseCase;
import com.portfolio.platform.ui.device.domain.usecase.HybridSyncUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetDianaDeviceSettingUseCase;
import com.portfolio.platform.uirenew.pairing.usecase.GetHybridDeviceSettingUseCase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cj4 {
    @DexIgnore
    public /* final */ GetDianaDeviceSettingUseCase a;
    @DexIgnore
    public /* final */ GetHybridDeviceSettingUseCase b;
    @DexIgnore
    public /* final */ HybridSyncUseCase c;
    @DexIgnore
    public /* final */ DianaSyncUseCase d;

    @DexIgnore
    public cj4(GetDianaDeviceSettingUseCase getDianaDeviceSettingUseCase, GetHybridDeviceSettingUseCase getHybridDeviceSettingUseCase, HybridSyncUseCase hybridSyncUseCase, DianaSyncUseCase dianaSyncUseCase) {
        wg6.b(getDianaDeviceSettingUseCase, "mGetDianaDeviceSettingUseCase");
        wg6.b(getHybridDeviceSettingUseCase, "mGetHybridDeviceSettingUseCase");
        wg6.b(hybridSyncUseCase, "mHybridSyncUseCase");
        wg6.b(dianaSyncUseCase, "mDianaSyncUseCase");
        this.a = getDianaDeviceSettingUseCase;
        this.b = getHybridDeviceSettingUseCase;
        this.c = hybridSyncUseCase;
        this.d = dianaSyncUseCase;
    }

    @DexIgnore
    public final m24<xs5, ys5, ws5> a(String str) {
        wg6.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = bj4.a[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.b;
            }
            if (i == 4) {
                return this.a;
            }
        }
        return this.b;
    }

    @DexIgnore
    public final m24<bs4, cs4, as4> b(String str) {
        wg6.b(str, "serial");
        FossilDeviceSerialPatternUtil.DEVICE deviceBySerial = FossilDeviceSerialPatternUtil.getDeviceBySerial(str);
        if (deviceBySerial != null) {
            int i = bj4.b[deviceBySerial.ordinal()];
            if (i == 1 || i == 2 || i == 3) {
                return this.c;
            }
            if (i == 4) {
                return this.d;
            }
        }
        return this.d;
    }
}
