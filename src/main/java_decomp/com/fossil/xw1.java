package com.fossil;

import android.os.RemoteException;
import com.fossil.rv1;
import com.fossil.rv1.b;
import com.fossil.uw1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class xw1<A extends rv1.b, L> {
    @DexIgnore
    public /* final */ uw1<L> a;
    @DexIgnore
    public /* final */ iv1[] b; // = null;
    @DexIgnore
    public /* final */ boolean c; // = false;

    @DexIgnore
    public xw1(uw1<L> uw1) {
        this.a = uw1;
    }

    @DexIgnore
    public void a() {
        this.a.a();
    }

    @DexIgnore
    public abstract void a(A a2, rc3<Void> rc3) throws RemoteException;

    @DexIgnore
    public uw1.a<L> b() {
        return this.a.b();
    }

    @DexIgnore
    public iv1[] c() {
        return this.b;
    }

    @DexIgnore
    public final boolean d() {
        return this.c;
    }
}
