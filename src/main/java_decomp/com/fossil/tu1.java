package com.fossil;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Scope;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class tu1 implements Parcelable.Creator<GoogleSignInAccount> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = f22.b(parcel);
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        Uri uri = null;
        String str5 = null;
        String str6 = null;
        ArrayList<Scope> arrayList = null;
        String str7 = null;
        String str8 = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            switch (f22.a(a)) {
                case 1:
                    i = f22.q(parcel2, a);
                    break;
                case 2:
                    str = f22.e(parcel2, a);
                    break;
                case 3:
                    str2 = f22.e(parcel2, a);
                    break;
                case 4:
                    str3 = f22.e(parcel2, a);
                    break;
                case 5:
                    str4 = f22.e(parcel2, a);
                    break;
                case 6:
                    uri = f22.a(parcel2, a, Uri.CREATOR);
                    break;
                case 7:
                    str5 = f22.e(parcel2, a);
                    break;
                case 8:
                    j = f22.s(parcel2, a);
                    break;
                case 9:
                    str6 = f22.e(parcel2, a);
                    break;
                case 10:
                    arrayList = f22.c(parcel2, a, Scope.CREATOR);
                    break;
                case 11:
                    str7 = f22.e(parcel2, a);
                    break;
                case 12:
                    str8 = f22.e(parcel2, a);
                    break;
                default:
                    f22.v(parcel2, a);
                    break;
            }
        }
        f22.h(parcel2, b);
        return new GoogleSignInAccount(i, str, str2, str3, str4, uri, str5, j, str6, arrayList, str7, str8);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new GoogleSignInAccount[i];
    }
}
