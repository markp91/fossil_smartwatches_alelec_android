package com.fossil;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ap2 {
    @DexIgnore
    public static /* final */ ap2 c; // = new ap2();
    @DexIgnore
    public /* final */ hp2 a; // = new eo2();
    @DexIgnore
    public /* final */ ConcurrentMap<Class<?>, fp2<?>> b; // = new ConcurrentHashMap();

    @DexIgnore
    public static ap2 a() {
        return c;
    }

    @DexIgnore
    public final <T> fp2<T> a(Class<T> cls) {
        hn2.a(cls, "messageType");
        fp2<T> fp2 = (fp2) this.b.get(cls);
        if (fp2 != null) {
            return fp2;
        }
        fp2<T> zza = this.a.zza(cls);
        hn2.a(cls, "messageType");
        hn2.a(zza, "schema");
        fp2<T> putIfAbsent = this.b.putIfAbsent(cls, zza);
        return putIfAbsent != null ? putIfAbsent : zza;
    }

    @DexIgnore
    public final <T> fp2<T> a(T t) {
        return a(t.getClass());
    }
}
