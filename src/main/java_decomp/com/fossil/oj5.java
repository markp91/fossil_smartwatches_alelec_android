package com.fossil;

import com.fossil.pj5;
import com.portfolio.platform.data.ActivityStatistic;
import com.portfolio.platform.data.SleepStatistic;
import com.portfolio.platform.data.model.MFUser;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface oj5 extends xt4<nj5> {
    @DexIgnore
    void K();

    @DexIgnore
    void O();

    @DexIgnore
    void a(int i, String str);

    @DexIgnore
    void a(mj5 mj5);

    @DexIgnore
    void a(ActivityStatistic activityStatistic);

    @DexIgnore
    void a(SleepStatistic sleepStatistic);

    @DexIgnore
    void a(MFUser mFUser);

    @DexIgnore
    void a(boolean z, boolean z2);

    @DexIgnore
    void c(ArrayList<pj5.b> arrayList);

    @DexIgnore
    void d();

    @DexIgnore
    void e();

    @DexIgnore
    void z();
}
