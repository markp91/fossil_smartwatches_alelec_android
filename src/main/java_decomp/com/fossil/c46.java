package com.fossil;

import android.content.Context;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class c46 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ Context a;
    @DexIgnore
    public /* final */ /* synthetic */ int b;

    @DexIgnore
    public c46(Context context, int i) {
        this.a = context;
        this.b = i;
    }

    @DexIgnore
    public final void run() {
        try {
            q36.f(this.a);
            o46.b(this.a).a(this.b);
        } catch (Throwable th) {
            q36.m.a(th);
            q36.a(this.a, th);
        }
    }
}
