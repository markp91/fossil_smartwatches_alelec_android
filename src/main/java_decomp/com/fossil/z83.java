package com.fossil;

import android.os.Bundle;
import android.os.RemoteException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class z83 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ String a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;
    @DexIgnore
    public /* final */ /* synthetic */ boolean c;
    @DexIgnore
    public /* final */ /* synthetic */ ra3 d;
    @DexIgnore
    public /* final */ /* synthetic */ ev2 e;
    @DexIgnore
    public /* final */ /* synthetic */ l83 f;

    @DexIgnore
    public z83(l83 l83, String str, String str2, boolean z, ra3 ra3, ev2 ev2) {
        this.f = l83;
        this.a = str;
        this.b = str2;
        this.c = z;
        this.d = ra3;
        this.e = ev2;
    }

    @DexIgnore
    public final void run() {
        Bundle bundle = new Bundle();
        try {
            l43 d2 = this.f.d;
            if (d2 == null) {
                this.f.b().t().a("Failed to get user properties", this.a, this.b);
                return;
            }
            bundle = ma3.a(d2.a(this.a, this.b, this.c, this.d));
            this.f.I();
            this.f.j().a(this.e, bundle);
        } catch (RemoteException e2) {
            this.f.b().t().a("Failed to get user properties", this.a, e2);
        } finally {
            this.f.j().a(this.e, bundle);
        }
    }
}
