package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum nf0 {
    ALARM((byte) 0),
    REMINDER((byte) 1);
    
    @DexIgnore
    public static /* final */ qn1 e; // = null;
    @DexIgnore
    public /* final */ byte a;

    /*
    static {
        e = new qn1((qg6) null);
    }
    */

    @DexIgnore
    public nf0(byte b) {
        this.a = b;
    }
}
