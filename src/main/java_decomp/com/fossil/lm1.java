package com.fossil;

import com.facebook.places.internal.LocationScannerImpl;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lm1 extends p21 {
    @DexIgnore
    public /* final */ md0 S;

    @DexIgnore
    public lm1(ue1 ue1, q41 q41, md0 md0) {
        super(ue1, q41, eh1.PUT_WATCH_PARAMETERS_FILE, true, md0.b(), md0.getData(), LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES, (String) null, 192);
        this.S = md0;
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.WATCH_PARAMETERS_FILE, (Object) this.S.a());
    }
}
