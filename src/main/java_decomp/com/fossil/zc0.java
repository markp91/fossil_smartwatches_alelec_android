package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zc0 extends tc0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);
    @DexIgnore
    public /* final */ v80 c;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<zc0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            Parcelable readParcelable = parcel.readParcelable(ca0.class.getClassLoader());
            if (readParcelable != null) {
                ca0 ca0 = (ca0) readParcelable;
                Parcelable readParcelable2 = parcel.readParcelable(uc0.class.getClassLoader());
                if (readParcelable2 != null) {
                    uc0 uc0 = (uc0) readParcelable2;
                    Parcelable readParcelable3 = parcel.readParcelable(v80.class.getClassLoader());
                    if (readParcelable3 != null) {
                        return new zc0(ca0, uc0, (v80) readParcelable3);
                    }
                    wg6.a();
                    throw null;
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new zc0[i];
        }
    }

    @DexIgnore
    public zc0(ca0 ca0, v80 v80) {
        super(ca0, (uc0) null);
        this.c = v80;
    }

    @DexIgnore
    public byte[] a(short s, w40 w40) {
        x90 deviceRequest = getDeviceRequest();
        Integer valueOf = deviceRequest != null ? Integer.valueOf(deviceRequest.c()) : null;
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("weatherInfo", this.c.b());
        } catch (JSONException e) {
            qs0.h.a(e);
        }
        JSONObject jSONObject2 = new JSONObject();
        String str = valueOf == null ? "push" : OrmLiteConfigUtil.RESOURCE_DIR_NAME;
        try {
            JSONObject jSONObject3 = new JSONObject();
            jSONObject3.put("id", valueOf);
            jSONObject3.put("set", jSONObject);
            jSONObject2.put(str, jSONObject3);
        } catch (JSONException e2) {
            qs0.h.a(e2);
        }
        String jSONObject4 = jSONObject2.toString();
        wg6.a(jSONObject4, "deviceResponseJSONObject.toString()");
        Charset f = mi0.A.f();
        if (jSONObject4 != null) {
            byte[] bytes = jSONObject4.getBytes(f);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            return bytes;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public JSONObject b() {
        return cw0.a(super.b(), this.c.a());
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(zc0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            return !(wg6.a(this.c, ((zc0) obj).c) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.devicedata.WeatherComplicationData");
    }

    @DexIgnore
    public final v80 getWeatherInfo() {
        return this.c;
    }

    @DexIgnore
    public int hashCode() {
        return this.c.hashCode() + (super.hashCode() * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeParcelable(this.c, i);
        }
    }

    @DexIgnore
    public zc0(ca0 ca0, uc0 uc0, v80 v80) {
        super(ca0, uc0);
        this.c = v80;
    }

    @DexIgnore
    public zc0(v80 v80) {
        this((ca0) null, (uc0) null, v80);
    }
}
