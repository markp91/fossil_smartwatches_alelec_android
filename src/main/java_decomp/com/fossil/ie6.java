package com.fossil;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ie6 extends he6 {
    @DexIgnore
    public static final <K, V> List<lc6<K, V>> a(Map<? extends K, ? extends V> map) {
        wg6.b(map, "$this$toList");
        if (map.size() == 0) {
            return qd6.a();
        }
        Iterator<Map.Entry<? extends K, ? extends V>> it = map.entrySet().iterator();
        if (!it.hasNext()) {
            return qd6.a();
        }
        Map.Entry next = it.next();
        if (!it.hasNext()) {
            return pd6.a(new lc6(next.getKey(), next.getValue()));
        }
        ArrayList arrayList = new ArrayList(map.size());
        arrayList.add(new lc6(next.getKey(), next.getValue()));
        do {
            Map.Entry next2 = it.next();
            arrayList.add(new lc6(next2.getKey(), next2.getValue()));
        } while (it.hasNext());
        return arrayList;
    }
}
