package com.fossil;

import java.nio.charset.Charset;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class mq6 {
    @DexIgnore
    public static String a(String str, String str2) {
        return a(str, str2, fr6.j);
    }

    @DexIgnore
    public static String a(String str, String str2, Charset charset) {
        String base64 = mt6.encodeString(str + ":" + str2, charset).base64();
        return "Basic " + base64;
    }
}
