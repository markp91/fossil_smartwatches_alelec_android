package com.fossil;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class iy2 extends jh2 implements wx2 {
    @DexIgnore
    public iy2(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaViewDelegate");
    }

    @DexIgnore
    public final void a(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) bundle);
        Parcel a = a(7, zza);
        if (a.readInt() != 0) {
            bundle.readFromParcel(a);
        }
        a.recycle();
    }

    @DexIgnore
    public final void b(Bundle bundle) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (Parcelable) bundle);
        b(2, zza);
    }

    @DexIgnore
    public final void c() throws RemoteException {
        b(11, zza());
    }

    @DexIgnore
    public final void d() throws RemoteException {
        b(3, zza());
    }

    @DexIgnore
    public final x52 getView() throws RemoteException {
        Parcel a = a(8, zza());
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final void onLowMemory() throws RemoteException {
        b(6, zza());
    }

    @DexIgnore
    public final void onPause() throws RemoteException {
        b(4, zza());
    }

    @DexIgnore
    public final void b() throws RemoteException {
        b(5, zza());
    }

    @DexIgnore
    public final void a(ey2 ey2) throws RemoteException {
        Parcel zza = zza();
        lh2.a(zza, (IInterface) ey2);
        b(9, zza);
    }

    @DexIgnore
    public final void a() throws RemoteException {
        b(10, zza());
    }
}
