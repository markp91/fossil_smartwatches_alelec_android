package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.u12;
import com.google.android.gms.fitness.data.DataType;
import java.util.Collections;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class de2 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<de2> CREATOR; // = new fe2();
    @DexIgnore
    public /* final */ List<DataType> a;

    @DexIgnore
    public de2(List<DataType> list) {
        this.a = list;
    }

    @DexIgnore
    public final List<DataType> p() {
        return Collections.unmodifiableList(this.a);
    }

    @DexIgnore
    public final String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a("dataTypes", this.a);
        return a2.toString();
    }

    @DexIgnore
    public final void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.c(parcel, 1, Collections.unmodifiableList(this.a), false);
        g22.a(parcel, a2);
    }
}
