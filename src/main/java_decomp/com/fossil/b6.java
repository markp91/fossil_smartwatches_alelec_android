package com.fossil;

import androidx.collection.SimpleArrayMap;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class b6<T> {
    @DexIgnore
    public /* final */ v8<ArrayList<T>> a; // = new w8(10);
    @DexIgnore
    public /* final */ SimpleArrayMap<T, ArrayList<T>> b; // = new SimpleArrayMap<>();
    @DexIgnore
    public /* final */ ArrayList<T> c; // = new ArrayList<>();
    @DexIgnore
    public /* final */ HashSet<T> d; // = new HashSet<>();

    @DexIgnore
    public void a(T t) {
        if (!this.b.containsKey(t)) {
            this.b.put(t, null);
        }
    }

    @DexIgnore
    public boolean b(T t) {
        return this.b.containsKey(t);
    }

    @DexIgnore
    public List c(T t) {
        return this.b.get(t);
    }

    @DexIgnore
    public List<T> d(T t) {
        int size = this.b.size();
        ArrayList arrayList = null;
        for (int i = 0; i < size; i++) {
            ArrayList e = this.b.e(i);
            if (e != null && e.contains(t)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(this.b.c(i));
            }
        }
        return arrayList;
    }

    @DexIgnore
    public boolean e(T t) {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ArrayList e = this.b.e(i);
            if (e != null && e.contains(t)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final ArrayList<T> b() {
        ArrayList<T> a2 = this.a.a();
        return a2 == null ? new ArrayList<>() : a2;
    }

    @DexIgnore
    public ArrayList<T> c() {
        this.c.clear();
        this.d.clear();
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            a(this.b.c(i), this.c, this.d);
        }
        return this.c;
    }

    @DexIgnore
    public void a(T t, T t2) {
        if (!this.b.containsKey(t) || !this.b.containsKey(t2)) {
            throw new IllegalArgumentException("All nodes must be present in the graph before being added as an edge");
        }
        ArrayList arrayList = this.b.get(t);
        if (arrayList == null) {
            arrayList = b();
            this.b.put(t, arrayList);
        }
        arrayList.add(t2);
    }

    @DexIgnore
    public void a() {
        int size = this.b.size();
        for (int i = 0; i < size; i++) {
            ArrayList e = this.b.e(i);
            if (e != null) {
                a(e);
            }
        }
        this.b.clear();
    }

    @DexIgnore
    public final void a(T t, ArrayList<T> arrayList, HashSet<T> hashSet) {
        if (!arrayList.contains(t)) {
            if (!hashSet.contains(t)) {
                hashSet.add(t);
                ArrayList arrayList2 = this.b.get(t);
                if (arrayList2 != null) {
                    int size = arrayList2.size();
                    for (int i = 0; i < size; i++) {
                        a(arrayList2.get(i), arrayList, hashSet);
                    }
                }
                hashSet.remove(t);
                arrayList.add(t);
                return;
            }
            throw new RuntimeException("This graph contains cyclic dependencies");
        }
    }

    @DexIgnore
    public final void a(ArrayList<T> arrayList) {
        arrayList.clear();
        this.a.a(arrayList);
    }
}
