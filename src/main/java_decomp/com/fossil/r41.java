package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum r41 {
    WORKOUT_SESSION_PRIMARY_ID((byte) 3),
    CONNECTION_PARAMETERS_REQUEST((byte) 9),
    CURRENT_CONNECTION_PARAMETERS((byte) 10),
    STREAMING_CONFIG((byte) 12),
    CALIBRATION_SETTING((byte) 21),
    ADVANCE_BLE_REQUEST((byte) 23),
    DIAGNOSTIC_FUNCTIONS((byte) 241),
    HARDWARE_TEST((byte) 242);
    
    @DexIgnore
    public /* final */ byte a;

    @DexIgnore
    public r41(byte b) {
        this.a = b;
    }

    @DexIgnore
    public final byte a() {
        return this.a;
    }
}
