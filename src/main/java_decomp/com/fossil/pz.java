package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.fossil.gt;
import java.util.List;
import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class pz<R> implements jz, xz, oz {
    @DexIgnore
    public static /* final */ boolean D; // = Log.isLoggable("Request", 2);
    @DexIgnore
    public int A;
    @DexIgnore
    public boolean B;
    @DexIgnore
    public RuntimeException C;
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ u00 b;
    @DexIgnore
    public /* final */ Object c;
    @DexIgnore
    public /* final */ mz<R> d;
    @DexIgnore
    public /* final */ kz e;
    @DexIgnore
    public /* final */ Context f;
    @DexIgnore
    public /* final */ yq g;
    @DexIgnore
    public /* final */ Object h;
    @DexIgnore
    public /* final */ Class<R> i;
    @DexIgnore
    public /* final */ gz<?> j;
    @DexIgnore
    public /* final */ int k;
    @DexIgnore
    public /* final */ int l;
    @DexIgnore
    public /* final */ br m;
    @DexIgnore
    public /* final */ yz<R> n;
    @DexIgnore
    public /* final */ List<mz<R>> o;
    @DexIgnore
    public /* final */ c00<? super R> p;
    @DexIgnore
    public /* final */ Executor q;
    @DexIgnore
    public rt<R> r;
    @DexIgnore
    public gt.d s;
    @DexIgnore
    public long t;
    @DexIgnore
    public volatile gt u;
    @DexIgnore
    public a v;
    @DexIgnore
    public Drawable w;
    @DexIgnore
    public Drawable x;
    @DexIgnore
    public Drawable y;
    @DexIgnore
    public int z;

    @DexIgnore
    public enum a {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    @DexIgnore
    public pz(Context context, yq yqVar, Object obj, Object obj2, Class<R> cls, gz<?> gzVar, int i2, int i3, br brVar, yz<R> yzVar, mz<R> mzVar, List<mz<R>> list, kz kzVar, gt gtVar, c00<? super R> c00, Executor executor) {
        this.a = D ? String.valueOf(super.hashCode()) : null;
        this.b = u00.b();
        this.c = obj;
        this.f = context;
        this.g = yqVar;
        this.h = obj2;
        this.i = cls;
        this.j = gzVar;
        this.k = i2;
        this.l = i3;
        this.m = brVar;
        this.n = yzVar;
        this.d = mzVar;
        this.o = list;
        this.e = kzVar;
        this.u = gtVar;
        this.p = c00;
        this.q = executor;
        this.v = a.PENDING;
        if (this.C == null && yqVar.g()) {
            this.C = new RuntimeException("Glide request origin trace");
        }
    }

    @DexIgnore
    public static <R> pz<R> a(Context context, yq yqVar, Object obj, Object obj2, Class<R> cls, gz<?> gzVar, int i2, int i3, br brVar, yz<R> yzVar, mz<R> mzVar, List<mz<R>> list, kz kzVar, gt gtVar, c00<? super R> c00, Executor executor) {
        return new pz(context, yqVar, obj, obj2, cls, gzVar, i2, i3, brVar, yzVar, mzVar, list, kzVar, gtVar, c00, executor);
    }

    @DexIgnore
    public final void b() {
        if (this.B) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00a6, code lost:
        return;
     */
    @DexIgnore
    public void c() {
        synchronized (this.c) {
            b();
            this.b.a();
            this.t = m00.a();
            if (this.h == null) {
                if (r00.b(this.k, this.l)) {
                    this.z = this.k;
                    this.A = this.l;
                }
                a(new mt("Received null model"), m() == null ? 5 : 3);
            } else if (this.v == a.RUNNING) {
                throw new IllegalArgumentException("Cannot restart a running request");
            } else if (this.v == a.COMPLETE) {
                a((rt<?>) this.r, pr.MEMORY_CACHE);
            } else {
                this.v = a.WAITING_FOR_SIZE;
                if (r00.b(this.k, this.l)) {
                    a(this.k, this.l);
                } else {
                    this.n.b((xz) this);
                }
                if ((this.v == a.RUNNING || this.v == a.WAITING_FOR_SIZE) && i()) {
                    this.n.b(n());
                }
                if (D) {
                    a("finished run method in " + m00.a(this.t));
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0035, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0037, code lost:
        r4.u.b((com.fossil.rt<?>) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        return;
     */
    @DexIgnore
    public void clear() {
        rt<R> rtVar;
        synchronized (this.c) {
            b();
            this.b.a();
            if (this.v != a.CLEARED) {
                k();
                if (this.r != null) {
                    rtVar = this.r;
                    this.r = null;
                } else {
                    rtVar = null;
                }
                if (h()) {
                    this.n.c(n());
                }
                this.v = a.CLEARED;
            }
        }
    }

    @DexIgnore
    public boolean d() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    @DexIgnore
    public void e() {
        synchronized (this.c) {
            if (isRunning()) {
                clear();
            }
        }
    }

    @DexIgnore
    public boolean f() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.CLEARED;
        }
        return z2;
    }

    @DexIgnore
    public boolean g() {
        boolean z2;
        synchronized (this.c) {
            z2 = this.v == a.COMPLETE;
        }
        return z2;
    }

    @DexIgnore
    public final boolean h() {
        kz kzVar = this.e;
        return kzVar == null || kzVar.f(this);
    }

    @DexIgnore
    public final boolean i() {
        kz kzVar = this.e;
        return kzVar == null || kzVar.c(this);
    }

    @DexIgnore
    public boolean isRunning() {
        boolean z2;
        synchronized (this.c) {
            if (this.v != a.RUNNING) {
                if (this.v != a.WAITING_FOR_SIZE) {
                    z2 = false;
                }
            }
            z2 = true;
        }
        return z2;
    }

    @DexIgnore
    public final boolean j() {
        kz kzVar = this.e;
        return kzVar == null || kzVar.d(this);
    }

    @DexIgnore
    public final void k() {
        b();
        this.b.a();
        this.n.a((xz) this);
        gt.d dVar = this.s;
        if (dVar != null) {
            dVar.a();
            this.s = null;
        }
    }

    @DexIgnore
    public final Drawable l() {
        if (this.w == null) {
            this.w = this.j.f();
            if (this.w == null && this.j.e() > 0) {
                this.w = a(this.j.e());
            }
        }
        return this.w;
    }

    @DexIgnore
    public final Drawable m() {
        if (this.y == null) {
            this.y = this.j.g();
            if (this.y == null && this.j.h() > 0) {
                this.y = a(this.j.h());
            }
        }
        return this.y;
    }

    @DexIgnore
    public final Drawable n() {
        if (this.x == null) {
            this.x = this.j.m();
            if (this.x == null && this.j.n() > 0) {
                this.x = a(this.j.n());
            }
        }
        return this.x;
    }

    @DexIgnore
    public final boolean o() {
        kz kzVar = this.e;
        return kzVar == null || !kzVar.a().d();
    }

    @DexIgnore
    public final void p() {
        kz kzVar = this.e;
        if (kzVar != null) {
            kzVar.b(this);
        }
    }

    @DexIgnore
    public final void q() {
        kz kzVar = this.e;
        if (kzVar != null) {
            kzVar.e(this);
        }
    }

    @DexIgnore
    public final void r() {
        if (i()) {
            Drawable drawable = null;
            if (this.h == null) {
                drawable = m();
            }
            if (drawable == null) {
                drawable = l();
            }
            if (drawable == null) {
                drawable = n();
            }
            this.n.a(drawable);
        }
    }

    @DexIgnore
    public final Drawable a(int i2) {
        return hx.a((Context) this.g, i2, this.j.w() != null ? this.j.w() : this.f.getTheme());
    }

    @DexIgnore
    public void a(int i2, int i3) {
        Object obj;
        this.b.a();
        Object obj2 = this.c;
        synchronized (obj2) {
            try {
                if (D) {
                    a("Got onSizeReady in " + m00.a(this.t));
                }
                if (this.v == a.WAITING_FOR_SIZE) {
                    this.v = a.RUNNING;
                    float r2 = this.j.r();
                    this.z = a(i2, r2);
                    this.A = a(i3, r2);
                    if (D) {
                        a("finished setup for calling load in " + m00.a(this.t));
                    }
                    obj = obj2;
                    try {
                        this.s = this.u.a(this.g, this.h, this.j.q(), this.z, this.A, this.j.p(), this.i, this.m, this.j.d(), this.j.x(), this.j.G(), this.j.D(), this.j.j(), this.j.B(), this.j.z(), this.j.y(), this.j.i(), this, this.q);
                        if (this.v != a.RUNNING) {
                            this.s = null;
                        }
                        if (D) {
                            a("finished onSizeReady in " + m00.a(this.t));
                        }
                    } catch (Throwable th) {
                        th = th;
                        throw th;
                    }
                }
            } catch (Throwable th2) {
                th = th2;
                obj = obj2;
                throw th;
            }
        }
    }

    @DexIgnore
    public static int a(int i2, float f2) {
        return i2 == Integer.MIN_VALUE ? i2 : Math.round(f2 * ((float) i2));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004f, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0051, code lost:
        r5.u.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x00aa, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ac, code lost:
        r5.u.b(r6);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        return;
     */
    @DexIgnore
    public void a(rt<?> rtVar, pr prVar) {
        this.b.a();
        rt<?> rtVar2 = null;
        try {
            synchronized (this.c) {
                try {
                    this.s = null;
                    if (rtVar == null) {
                        a(new mt("Expected to receive a Resource<R> with an object of " + this.i + " inside, but instead got null."));
                        return;
                    }
                    Object obj = rtVar.get();
                    if (obj != null) {
                        if (this.i.isAssignableFrom(obj.getClass())) {
                            if (!j()) {
                                this.r = null;
                                this.v = a.COMPLETE;
                            } else {
                                a(rtVar, obj, prVar);
                                return;
                            }
                        }
                    }
                    try {
                        this.r = null;
                        StringBuilder sb = new StringBuilder();
                        sb.append("Expected to receive an object of ");
                        sb.append(this.i);
                        sb.append(" but instead got ");
                        sb.append(obj != null ? obj.getClass() : "");
                        sb.append("{");
                        sb.append(obj);
                        sb.append("} inside Resource{");
                        sb.append(rtVar);
                        sb.append("}.");
                        sb.append(obj != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.");
                        a(new mt(sb.toString()));
                    } catch (Throwable th) {
                        th = th;
                        try {
                            throw th;
                        } catch (Throwable th2) {
                            th = th2;
                            rtVar2 = rtVar;
                        }
                    }
                } catch (Throwable th3) {
                    th = th3;
                    rtVar = null;
                    throw th;
                }
            }
        } catch (Throwable th4) {
            th = th4;
            if (rtVar2 != null) {
                this.u.b(rtVar2);
            }
            throw th;
        }
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x00ab A[Catch:{ all -> 0x00bc }] */
    public final void a(rt<R> rtVar, R r2, pr prVar) {
        boolean z2;
        boolean o2 = o();
        this.v = a.COMPLETE;
        this.r = rtVar;
        if (this.g.e() <= 3) {
            Log.d("Glide", "Finished loading " + r2.getClass().getSimpleName() + " from " + prVar + " for " + this.h + " with size [" + this.z + "x" + this.A + "] in " + m00.a(this.t) + " ms");
        }
        boolean z3 = true;
        this.B = true;
        try {
            if (this.o != null) {
                z2 = false;
                for (mz<R> a2 : this.o) {
                    z2 |= a2.a(r2, this.h, this.n, prVar, o2);
                }
            } else {
                z2 = false;
            }
            if (this.d != null) {
                if (this.d.a(r2, this.h, this.n, prVar, o2)) {
                    if (!z3 && !z2) {
                        this.n.a(r2, this.p.a(prVar, o2));
                    }
                    this.B = false;
                    q();
                }
            }
            z3 = false;
            if (!z3 && !z2) {
            }
            this.B = false;
            q();
        } catch (Throwable th) {
            this.B = false;
            throw th;
        }
    }

    @DexIgnore
    public void a(mt mtVar) {
        a(mtVar, 5);
    }

    @DexIgnore
    public Object a() {
        this.b.a();
        return this.c;
    }

    @DexIgnore
    /* JADX INFO: finally extract failed */
    public final void a(mt mtVar, int i2) {
        boolean z2;
        this.b.a();
        synchronized (this.c) {
            mtVar.setOrigin(this.C);
            int e2 = this.g.e();
            if (e2 <= i2) {
                Log.w("Glide", "Load failed for " + this.h + " with size [" + this.z + "x" + this.A + "]", mtVar);
                if (e2 <= 4) {
                    mtVar.logRootCauses("Glide");
                }
            }
            this.s = null;
            this.v = a.FAILED;
            boolean z3 = true;
            this.B = true;
            try {
                if (this.o != null) {
                    z2 = false;
                    for (mz<R> a2 : this.o) {
                        z2 |= a2.a(mtVar, this.h, this.n, o());
                    }
                } else {
                    z2 = false;
                }
                if (this.d == null || !this.d.a(mtVar, this.h, this.n, o())) {
                    z3 = false;
                }
                if (!z2 && !z3) {
                    r();
                }
                this.B = false;
                p();
            } catch (Throwable th) {
                this.B = false;
                throw th;
            }
        }
    }

    @DexIgnore
    public boolean a(jz jzVar) {
        int i2;
        int i3;
        Object obj;
        Class<R> cls;
        gz<?> gzVar;
        br brVar;
        int size;
        int i4;
        int i5;
        Object obj2;
        Class<R> cls2;
        gz<?> gzVar2;
        br brVar2;
        int size2;
        jz jzVar2 = jzVar;
        if (!(jzVar2 instanceof pz)) {
            return false;
        }
        synchronized (this.c) {
            i2 = this.k;
            i3 = this.l;
            obj = this.h;
            cls = this.i;
            gzVar = this.j;
            brVar = this.m;
            size = this.o != null ? this.o.size() : 0;
        }
        pz pzVar = (pz) jzVar2;
        synchronized (pzVar.c) {
            i4 = pzVar.k;
            i5 = pzVar.l;
            obj2 = pzVar.h;
            cls2 = pzVar.i;
            gzVar2 = pzVar.j;
            brVar2 = pzVar.m;
            size2 = pzVar.o != null ? pzVar.o.size() : 0;
        }
        return i2 == i4 && i3 == i5 && r00.a(obj, obj2) && cls.equals(cls2) && gzVar.equals(gzVar2) && brVar == brVar2 && size == size2;
    }

    @DexIgnore
    public final void a(String str) {
        Log.v("Request", str + " this: " + this.a);
    }
}
