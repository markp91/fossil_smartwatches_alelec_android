package com.fossil;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class h53 extends s63 {
    @DexIgnore
    public static /* final */ Pair<String, Long> C; // = new Pair<>("", 0L);
    @DexIgnore
    public i53 A; // = new i53(this, "deep_link_retrieval_attempts", 0);
    @DexIgnore
    public /* final */ k53 B; // = new k53(this, "firebase_feature_rollouts", (String) null);
    @DexIgnore
    public SharedPreferences c;
    @DexIgnore
    public l53 d;
    @DexIgnore
    public /* final */ i53 e; // = new i53(this, "last_upload", 0);
    @DexIgnore
    public /* final */ i53 f; // = new i53(this, "last_upload_attempt", 0);
    @DexIgnore
    public /* final */ i53 g; // = new i53(this, "backoff", 0);
    @DexIgnore
    public /* final */ i53 h; // = new i53(this, "last_delete_stale", 0);
    @DexIgnore
    public /* final */ i53 i; // = new i53(this, "midnight_offset", 0);
    @DexIgnore
    public /* final */ i53 j; // = new i53(this, "first_open_time", 0);
    @DexIgnore
    public /* final */ i53 k; // = new i53(this, "app_install_time", 0);
    @DexIgnore
    public /* final */ k53 l; // = new k53(this, "app_instance_id", (String) null);
    @DexIgnore
    public String m;
    @DexIgnore
    public boolean n;
    @DexIgnore
    public long o;
    @DexIgnore
    public /* final */ i53 p; // = new i53(this, "time_before_start", 10000);
    @DexIgnore
    public /* final */ i53 q; // = new i53(this, "session_timeout", 1800000);
    @DexIgnore
    public /* final */ j53 r; // = new j53(this, "start_new_session", true);
    @DexIgnore
    public /* final */ k53 s; // = new k53(this, "non_personalized_ads", (String) null);
    @DexIgnore
    public /* final */ j53 t; // = new j53(this, "use_dynamite_api", false);
    @DexIgnore
    public /* final */ j53 u; // = new j53(this, "allow_remote_dynamite", false);
    @DexIgnore
    public /* final */ i53 v; // = new i53(this, "last_pause_time", 0);
    @DexIgnore
    public /* final */ i53 w; // = new i53(this, "time_active", 0);
    @DexIgnore
    public boolean x;
    @DexIgnore
    public j53 y; // = new j53(this, "app_backgrounded", false);
    @DexIgnore
    public j53 z; // = new j53(this, "deep_link_retrieval_complete", false);

    @DexIgnore
    public h53(x53 x53) {
        super(x53);
    }

    @DexIgnore
    public final SharedPreferences A() {
        g();
        n();
        return this.c;
    }

    @DexIgnore
    public final Pair<String, Boolean> a(String str) {
        g();
        long c2 = zzm().c();
        String str2 = this.m;
        if (str2 != null && c2 < this.o) {
            return new Pair<>(str2, Boolean.valueOf(this.n));
        }
        this.o = c2 + l().a(str, l03.g);
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(true);
        try {
            AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(c());
            if (advertisingIdInfo != null) {
                this.m = advertisingIdInfo.getId();
                this.n = advertisingIdInfo.isLimitAdTrackingEnabled();
            }
            if (this.m == null) {
                this.m = "";
            }
        } catch (Exception e2) {
            b().A().a("Unable to get advertising id", e2);
            this.m = "";
        }
        AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
        return new Pair<>(this.m, Boolean.valueOf(this.n));
    }

    @DexIgnore
    public final String b(String str) {
        g();
        String str2 = (String) a(str).first;
        MessageDigest x2 = ma3.x();
        if (x2 == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new Object[]{new BigInteger(1, x2.digest(str2.getBytes()))});
    }

    @DexIgnore
    public final void c(String str) {
        g();
        SharedPreferences.Editor edit = A().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final void d(String str) {
        g();
        SharedPreferences.Editor edit = A().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    @DexIgnore
    public final void m() {
        this.c = c().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.x = this.c.getBoolean("has_been_opened", false);
        if (!this.x) {
            SharedPreferences.Editor edit = this.c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.d = new l53(this, "health_monitor", Math.max(0, l03.h.a(null).longValue()));
    }

    @DexIgnore
    public final boolean q() {
        return true;
    }

    @DexIgnore
    public final String s() {
        g();
        return A().getString("gmp_app_id", (String) null);
    }

    @DexIgnore
    public final String t() {
        g();
        return A().getString("admob_app_id", (String) null);
    }

    @DexIgnore
    public final Boolean u() {
        g();
        if (!A().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(A().getBoolean("use_service", false));
    }

    @DexIgnore
    public final void v() {
        g();
        b().B().a("Clearing collection preferences.");
        if (l().a(l03.k0)) {
            Boolean w2 = w();
            SharedPreferences.Editor edit = A().edit();
            edit.clear();
            edit.apply();
            if (w2 != null) {
                b(w2.booleanValue());
                return;
            }
            return;
        }
        boolean contains = A().contains("measurement_enabled");
        boolean z2 = true;
        if (contains) {
            z2 = c(true);
        }
        SharedPreferences.Editor edit2 = A().edit();
        edit2.clear();
        edit2.apply();
        if (contains) {
            b(z2);
        }
    }

    @DexIgnore
    public final Boolean w() {
        g();
        if (A().contains("measurement_enabled")) {
            return Boolean.valueOf(A().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    @DexIgnore
    public final String x() {
        g();
        String string = A().getString("previous_os_version", (String) null);
        h().n();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = A().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    @DexIgnore
    public final boolean y() {
        g();
        return A().getBoolean("deferred_analytics_collection", false);
    }

    @DexIgnore
    public final boolean z() {
        return this.c.contains("deferred_analytics_collection");
    }

    @DexIgnore
    public final void b(boolean z2) {
        g();
        b().B().a("Setting measurementEnabled", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = A().edit();
        edit.putBoolean("measurement_enabled", z2);
        edit.apply();
    }

    @DexIgnore
    public final boolean c(boolean z2) {
        g();
        return A().getBoolean("measurement_enabled", z2);
    }

    @DexIgnore
    public final void d(boolean z2) {
        g();
        b().B().a("Updating deferred analytics collection", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = A().edit();
        edit.putBoolean("deferred_analytics_collection", z2);
        edit.apply();
    }

    @DexIgnore
    public final void a(boolean z2) {
        g();
        b().B().a("Setting useService", Boolean.valueOf(z2));
        SharedPreferences.Editor edit = A().edit();
        edit.putBoolean("use_service", z2);
        edit.apply();
    }

    @DexIgnore
    public final boolean a(long j2) {
        return j2 - this.q.a() > this.v.a();
    }
}
