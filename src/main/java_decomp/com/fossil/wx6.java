package com.fossil;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import okhttp3.RequestBody;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wx6<T> implements fx6<T, RequestBody> {
    @DexIgnore
    public static /* final */ uq6 c; // = uq6.a("application/json; charset=UTF-8");
    @DexIgnore
    public static /* final */ Charset d; // = Charset.forName("UTF-8");
    @DexIgnore
    public /* final */ Gson a;
    @DexIgnore
    public /* final */ TypeAdapter<T> b;

    @DexIgnore
    public wx6(Gson gson, TypeAdapter<T> typeAdapter) {
        this.a = gson;
        this.b = typeAdapter;
    }

    @DexIgnore
    public RequestBody a(T t) throws IOException {
        jt6 jt6 = new jt6();
        JsonWriter a2 = this.a.a(new OutputStreamWriter(jt6.d(), d));
        this.b.write(a2, t);
        a2.close();
        return RequestBody.a(c, jt6.m());
    }
}
