package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class NotificationIcon extends jd0 {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a((qg6) null);

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator<ld0> {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            String readString = parcel.readString();
            if (readString != null) {
                wg6.a(readString, "parcel.readString()!!");
                byte[] createByteArray = parcel.createByteArray();
                if (createByteArray != null) {
                    wg6.a(createByteArray, "parcel.createByteArray()!!");
                    return new NotificationIcon(readString, createByteArray);
                }
                wg6.a();
                throw null;
            }
            wg6.a();
            throw null;
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new NotificationIcon[i];
        }
    }

    @DexIgnore
    public NotificationIcon(String str, byte[] bArr) {
        super(str, bArr);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(getClass(), obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationIcon notificationIcon = (NotificationIcon) obj;
            if (c() == notificationIcon.c() || wg6.a(cw0.a((int) c()), notificationIcon.getFileName()) || wg6.a(getFileName(), cw0.a((int) notificationIcon.c()))) {
                return true;
            }
            return false;
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.model.file.NotificationIcon");
    }

    @DexIgnore
    public int hashCode() {
        return (int) c();
    }
}
