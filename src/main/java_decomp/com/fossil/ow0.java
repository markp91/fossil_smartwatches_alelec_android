package com.fossil;

import android.bluetooth.BluetoothGatt;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ow0 extends ok0 {
    @DexIgnore
    public /* final */ li1 j; // = li1.VERY_HIGH;
    @DexIgnore
    public h91 k; // = h91.DISCONNECTED;

    @DexIgnore
    public ow0(at0 at0) {
        super(hm0.DISCONNECT, at0);
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        c(p51);
        t31 t31 = p51.a;
        if (t31.a != x11.SUCCESS) {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        } else if (this.k == h91.DISCONNECTED) {
            ch0 = ch0.a(this.d, (hm0) null, lf0.SUCCESS, (t31) null, 5);
        } else {
            ch0 = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        }
        this.d = ch0;
    }

    @DexIgnore
    public li1 b() {
        return this.j;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.f;
    }

    @DexIgnore
    public boolean b(p51 p51) {
        return (p51 instanceof kr0) && ((kr0) p51).b == h91.DISCONNECTED;
    }

    @DexIgnore
    public void c(p51 p51) {
        this.k = ((kr0) p51).b;
    }

    @DexIgnore
    public void a(ue1 ue1) {
        int i = db1.b[ue1.s.ordinal()];
        if (i == 1) {
            ue1.a.post(new uu0(ue1, new t31(x11.SUCCESS, 0, 2), h91.DISCONNECTED));
        } else if (i == 2) {
            ue1.a(3, 0);
            BluetoothGatt bluetoothGatt = ue1.b;
            if (bluetoothGatt != null) {
                cc0 cc0 = cc0.DEBUG;
                bluetoothGatt.disconnect();
            }
            ue1.a(0, 0);
        } else if (i == 3) {
            ue1.a(3, 0);
            BluetoothGatt bluetoothGatt2 = ue1.b;
            if (bluetoothGatt2 != null) {
                cc0 cc02 = cc0.DEBUG;
                bluetoothGatt2.disconnect();
            } else {
                cc0 cc03 = cc0.DEBUG;
            }
            ue1.a(0, 0);
        }
    }
}
