package com.fossil;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class zb {
    @DexIgnore
    public /* final */ CopyOnWriteArrayList<a> a; // = new CopyOnWriteArrayList<>();
    @DexIgnore
    public /* final */ FragmentManager b;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public /* final */ FragmentManager.g a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public a(FragmentManager.g gVar, boolean z) {
            this.a = gVar;
            this.b = z;
        }
    }

    @DexIgnore
    public zb(FragmentManager fragmentManager) {
        this.b = fragmentManager;
    }

    @DexIgnore
    public void a(FragmentManager.g gVar, boolean z) {
        this.a.add(new a(gVar, z));
    }

    @DexIgnore
    public void b(Fragment fragment, Context context, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().b(fragment, context, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.b(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void c(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().c(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.c(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void d(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().d(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.d(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void e(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().e(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.e(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void f(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().f(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.f(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void g(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().g(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.g(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void a(FragmentManager.g gVar) {
        synchronized (this.a) {
            int i = 0;
            int size = this.a.size();
            while (true) {
                if (i >= size) {
                    break;
                } else if (this.a.get(i).a == gVar) {
                    this.a.remove(i);
                    break;
                } else {
                    i++;
                }
            }
        }
    }

    @DexIgnore
    public void a(Fragment fragment, Context context, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().a(fragment, context, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment, context);
            }
        }
    }

    @DexIgnore
    public void b(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().b(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.b(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void c(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().c(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.c(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void d(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().d(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.d(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void a(Fragment fragment, Bundle bundle, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().a(fragment, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment, bundle);
            }
        }
    }

    @DexIgnore
    public void b(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().b(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.b(this.b, fragment);
            }
        }
    }

    @DexIgnore
    public void a(Fragment fragment, View view, Bundle bundle, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().a(fragment, view, bundle, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment, view, bundle);
            }
        }
    }

    @DexIgnore
    public void a(Fragment fragment, boolean z) {
        Fragment y = this.b.y();
        if (y != null) {
            y.getParentFragmentManager().x().a(fragment, true);
        }
        Iterator<a> it = this.a.iterator();
        while (it.hasNext()) {
            a next = it.next();
            if (!z || next.b) {
                next.a.a(this.b, fragment);
            }
        }
    }
}
