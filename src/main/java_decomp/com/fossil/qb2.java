package com.fossil;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qb2 extends FilterInputStream {
    @DexIgnore
    public long a;
    @DexIgnore
    public long b; // = -1;

    @DexIgnore
    public qb2(InputStream inputStream, long j) {
        super(inputStream);
        mb2.a(inputStream);
        this.a = 1048577;
    }

    @DexIgnore
    public final int available() throws IOException {
        return (int) Math.min((long) this.in.available(), this.a);
    }

    @DexIgnore
    public final synchronized void mark(int i) {
        this.in.mark(i);
        this.b = this.a;
    }

    @DexIgnore
    public final int read() throws IOException {
        if (this.a == 0) {
            return -1;
        }
        int read = this.in.read();
        if (read != -1) {
            this.a--;
        }
        return read;
    }

    @DexIgnore
    public final synchronized void reset() throws IOException {
        if (!this.in.markSupported()) {
            throw new IOException("Mark not supported");
        } else if (this.b != -1) {
            this.in.reset();
            this.a = this.b;
        } else {
            throw new IOException("Mark not set");
        }
    }

    @DexIgnore
    public final long skip(long j) throws IOException {
        long skip = this.in.skip(Math.min(j, this.a));
        this.a -= skip;
        return skip;
    }

    @DexIgnore
    public final int read(byte[] bArr, int i, int i2) throws IOException {
        long j = this.a;
        if (j == 0) {
            return -1;
        }
        int read = this.in.read(bArr, i, (int) Math.min((long) i2, j));
        if (read != -1) {
            this.a -= (long) read;
        }
        return read;
    }
}
