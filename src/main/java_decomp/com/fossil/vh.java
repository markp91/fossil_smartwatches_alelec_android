package com.fossil;

import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class vh {
    @DexIgnore
    public /* final */ oh mDatabase;
    @DexIgnore
    public /* final */ AtomicBoolean mLock; // = new AtomicBoolean(false);
    @DexIgnore
    public volatile mi mStmt;

    @DexIgnore
    public vh(oh ohVar) {
        this.mDatabase = ohVar;
    }

    @DexIgnore
    private mi createNewStatement() {
        return this.mDatabase.compileStatement(createQuery());
    }

    @DexIgnore
    private mi getStmt(boolean z) {
        if (!z) {
            return createNewStatement();
        }
        if (this.mStmt == null) {
            this.mStmt = createNewStatement();
        }
        return this.mStmt;
    }

    @DexIgnore
    public mi acquire() {
        assertNotMainThread();
        return getStmt(this.mLock.compareAndSet(false, true));
    }

    @DexIgnore
    public void assertNotMainThread() {
        this.mDatabase.assertNotMainThread();
    }

    @DexIgnore
    public abstract String createQuery();

    @DexIgnore
    public void release(mi miVar) {
        if (miVar == this.mStmt) {
            this.mLock.set(false);
        }
    }
}
