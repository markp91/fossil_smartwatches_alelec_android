package com.fossil;

import java.util.Arrays;
import java.util.Iterator;
import java.util.RandomAccess;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class je6<T> extends id6<T> implements RandomAccess {
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public int c;
    @DexIgnore
    public int d;
    @DexIgnore
    public /* final */ Object[] e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a extends hd6<T> {
        @DexIgnore
        public int c;
        @DexIgnore
        public int d;
        @DexIgnore
        public /* final */ /* synthetic */ je6 e;

        @DexIgnore
        public a(je6 je6) {
            this.e = je6;
            this.c = je6.size();
            this.d = je6.c;
        }

        @DexIgnore
        public void a() {
            if (this.c == 0) {
                b();
                return;
            }
            b(this.e.e[this.d]);
            this.d = (this.d + 1) % this.e.b;
            this.c--;
        }
    }

    @DexIgnore
    public je6(Object[] objArr, int i) {
        wg6.b(objArr, "buffer");
        this.e = objArr;
        boolean z = true;
        if (i >= 0) {
            if (i > this.e.length ? false : z) {
                this.b = this.e.length;
                this.d = i;
                return;
            }
            throw new IllegalArgumentException(("ring buffer filled size: " + i + " cannot be larger than the buffer size: " + this.e.length).toString());
        }
        throw new IllegalArgumentException(("ring buffer filled size should not be negative but it is " + i).toString());
    }

    @DexIgnore
    public final void add(T t) {
        if (!b()) {
            this.e[(this.c + size()) % this.b] = t;
            this.d = size() + 1;
            return;
        }
        throw new IllegalStateException("ring buffer is full");
    }

    @DexIgnore
    public T get(int i) {
        id6.a.a(i, size());
        return this.e[(this.c + i) % this.b];
    }

    @DexIgnore
    public Iterator<T> iterator() {
        return new a(this);
    }

    @DexIgnore
    public <T> T[] toArray(T[] tArr) {
        wg6.b(tArr, "array");
        if (tArr.length < size()) {
            tArr = Arrays.copyOf(tArr, size());
            wg6.a((Object) tArr, "java.util.Arrays.copyOf(this, newSize)");
        }
        int size = size();
        int i = 0;
        int i2 = this.c;
        int i3 = 0;
        while (i3 < size && i2 < this.b) {
            tArr[i3] = this.e[i2];
            i3++;
            i2++;
        }
        while (i3 < size) {
            tArr[i3] = this.e[i];
            i3++;
            i++;
        }
        if (tArr.length > size()) {
            tArr[size()] = null;
        }
        if (tArr != null) {
            return tArr;
        }
        throw new rc6("null cannot be cast to non-null type kotlin.Array<T>");
    }

    @DexIgnore
    public int a() {
        return this.d;
    }

    @DexIgnore
    public final boolean b() {
        return size() == this.b;
    }

    @DexIgnore
    public final void c(int i) {
        boolean z = true;
        if (i >= 0) {
            if (i > size()) {
                z = false;
            }
            if (!z) {
                throw new IllegalArgumentException(("n shouldn't be greater than the buffer size: n = " + i + ", size = " + size()).toString());
            } else if (i > 0) {
                int i2 = this.c;
                int b2 = (i2 + i) % this.b;
                if (i2 > b2) {
                    md6.a(this.e, null, i2, this.b);
                    md6.a(this.e, null, 0, b2);
                } else {
                    md6.a(this.e, null, i2, b2);
                }
                this.c = b2;
                this.d = size() - i;
            }
        } else {
            throw new IllegalArgumentException(("n shouldn't be negative but it is " + i).toString());
        }
    }

    @DexIgnore
    public final je6<T> a(int i) {
        Object[] objArr;
        int i2 = this.b;
        int b2 = ci6.b(i2 + (i2 >> 1) + 1, i);
        if (this.c == 0) {
            objArr = Arrays.copyOf(this.e, b2);
            wg6.a((Object) objArr, "java.util.Arrays.copyOf(this, newSize)");
        } else {
            objArr = toArray(new Object[b2]);
        }
        return new je6<>(objArr, size());
    }

    @DexIgnore
    public je6(int i) {
        this(new Object[i], 0);
    }

    @DexIgnore
    public Object[] toArray() {
        return toArray(new Object[size()]);
    }
}
