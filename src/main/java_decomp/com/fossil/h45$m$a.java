package com.fossil;

import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.WatchFaceWrapper;
import com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1", f = "DianaCustomizeViewModel.kt", l = {503}, m = "invokeSuspend")
public final class h45$m$a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
    @DexIgnore
    public /* final */ /* synthetic */ List $it;
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public Object L$1;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ DianaCustomizeViewModel.m this$0;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.uirenew.home.customize.diana.DianaCustomizeViewModel$watchFaceObserver$1$1$1", f = "DianaCustomizeViewModel.kt", l = {}, m = "invokeSuspend")
    public static final class a extends sf6 implements ig6<il6, xe6<? super Boolean>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ List $list;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ h45$m$a this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(h45$m$a h45_m_a, List list, xe6 xe6) {
            super(2, xe6);
            this.this$0 = h45_m_a;
            this.$list = list;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            a aVar = new a(this.this$0, this.$list, xe6);
            aVar.p$ = (il6) obj;
            return aVar;
        }

        @DexIgnore
        public final Object invoke(Object obj, Object obj2) {
            return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                this.this$0.this$0.a.f.clear();
                return hf6.a(this.this$0.this$0.a.f.addAll(this.$list));
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h45$m$a(DianaCustomizeViewModel.m mVar, List list, xe6 xe6) {
        super(2, xe6);
        this.this$0 = mVar;
        this.$it = list;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        h45$m$a h45_m_a = new h45$m$a(this.this$0, this.$it, xe6);
        h45_m_a.p$ = (il6) obj;
        return h45_m_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((h45$m$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a2 = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            if (!wg6.a((Object) this.this$0.a.o, (Object) this.$it)) {
                this.this$0.a.o = this.$it;
                List list = this.$it;
                wg6.a((Object) list, "it");
                DianaPreset dianaPreset = (DianaPreset) this.this$0.a.b.a();
                List<WatchFaceWrapper> a3 = WatchFaceHelper.a(list, dianaPreset != null ? dianaPreset.getComplications() : null);
                cn6 c = zl6.c();
                a aVar = new a(this, a3, (xe6) null);
                this.L$0 = il6;
                this.L$1 = a3;
                this.label = 1;
                if (gk6.a(c, aVar, this) == a2) {
                    return a2;
                }
            }
        } else if (i == 1) {
            List list2 = (List) this.L$1;
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        return cd6.a;
    }
}
