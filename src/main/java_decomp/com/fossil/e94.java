package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.RelativeLayout;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class e94 extends d94 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j v; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray w; // = new SparseIntArray();
    @DexIgnore
    public /* final */ RelativeLayout t;
    @DexIgnore
    public long u;

    /*
    static {
        w.put(2131362081, 1);
        w.put(2131361851, 2);
        w.put(2131362695, 3);
        w.put(2131363218, 4);
        w.put(2131363129, 5);
        w.put(2131362213, 6);
    }
    */

    @DexIgnore
    public e94(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 7, v, w));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.u = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.u != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.u = 1;
        }
        g();
    }

    @DexIgnore
    public e94(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[2], objArr[1], objArr[6], objArr[3], objArr[5], objArr[4]);
        this.u = -1;
        this.t = objArr[0];
        this.t.setTag((Object) null);
        a(view);
        f();
    }
}
