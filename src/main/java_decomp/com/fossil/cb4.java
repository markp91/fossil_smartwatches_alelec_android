package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class cb4 extends bb4 {
    @DexIgnore
    public static /* final */ SparseIntArray A; // = new SparseIntArray();
    @DexIgnore
    public static /* final */ ViewDataBinding.j z; // = null;
    @DexIgnore
    public long y;

    /*
    static {
        A.put(2131362885, 1);
        A.put(2131363216, 2);
        A.put(2131362993, 3);
        A.put(2131362484, 4);
        A.put(2131362086, 5);
        A.put(2131363218, 6);
        A.put(2131363128, 7);
        A.put(2131362781, 8);
        A.put(2131362054, 9);
        A.put(2131362315, 10);
        A.put(2131362541, 11);
        A.put(2131362404, 12);
        A.put(2131362324, 13);
    }
    */

    @DexIgnore
    public cb4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 14, z, A));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.y = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.y != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.y = 1;
        }
        g();
    }

    @DexIgnore
    public cb4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[9], objArr[5], objArr[10], objArr[13], objArr[12], objArr[4], objArr[11], objArr[8], objArr[0], objArr[1], objArr[3], objArr[7], objArr[2], objArr[6]);
        this.y = -1;
        this.u.setTag((Object) null);
        a(view);
        f();
    }
}
