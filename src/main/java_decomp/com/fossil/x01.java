package com.fossil;

import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class x01 extends j61 {
    @DexIgnore
    public m51 A; // = m51.DISCONNECTED;

    @DexIgnore
    public x01(ue1 ue1) {
        super(lx0.DISCONNECT_HID, ue1);
    }

    @DexIgnore
    public void a(ni1 ni1) {
    }

    @DexIgnore
    public void a(ok0 ok0) {
        this.A = ((iy0) ok0).k;
        this.g.add(new ne0(0, (rg1) null, (byte[]) null, cw0.a(new JSONObject(), bm0.NEW_HID_STATE, (Object) cw0.a((Enum<?>) this.A)), 7));
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.CURRENT_HID_STATE, (Object) cw0.a((Enum<?>) this.y.s)), bm0.MAC_ADDRESS, (Object) this.y.t);
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(super.i(), bm0.NEW_HID_STATE, (Object) cw0.a((Enum<?>) this.A));
    }

    @DexIgnore
    public ok0 l() {
        return new iy0(this.y.v);
    }
}
