package com.fossil;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class t82 implements Parcelable.Creator<r82> {
    @DexIgnore
    /* JADX WARNING: type inference failed for: r1v3, types: [android.os.Parcelable] */
    /* JADX WARNING: Multi-variable type inference failed */
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        long j = 0;
        long j2 = 0;
        f72 f72 = null;
        IBinder iBinder = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 1) {
                f72 = f22.a(parcel, a, f72.CREATOR);
            } else if (a2 == 2) {
                iBinder = f22.p(parcel, a);
            } else if (a2 == 3) {
                j = f22.s(parcel, a);
            } else if (a2 != 4) {
                f22.v(parcel, a);
            } else {
                j2 = f22.s(parcel, a);
            }
        }
        f22.h(parcel, b);
        return new r82(f72, iBinder, j, j2);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new r82[i];
    }
}
