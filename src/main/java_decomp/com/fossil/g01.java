package com.fossil;

import java.nio.ByteBuffer;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class g01 extends p40 {
    @DexIgnore
    public static /* final */ my0 d; // = new my0((qg6) null);
    @DexIgnore
    public /* final */ byte a;
    @DexIgnore
    public /* final */ byte b;
    @DexIgnore
    public /* final */ short c;

    @DexIgnore
    public g01(short s) {
        this.c = s;
        ByteBuffer putShort = ByteBuffer.allocate(2).putShort(this.c);
        wg6.a(putShort, "ByteBuffer.allocate(2).putShort(fileHandle)");
        this.a = putShort.get(0);
        this.b = putShort.get(1);
    }

    @DexIgnore
    public JSONObject a() {
        return cw0.a(cw0.a(cw0.a(cw0.a(new JSONObject(), bm0.FILE_TYPE, (Object) cw0.a(this.a)), bm0.FILE_INDEX, (Object) cw0.a(this.b)), bm0.FILE_HANDLE, (Object) cw0.a(this.c)), bm0.FILE_HANDLE_DESCRIPTION, (Object) d.a(this.c));
    }

    @DexIgnore
    public g01(byte b2, byte b3) {
        this(ByteBuffer.allocate(2).put(b2).put(b3).getShort(0));
    }
}
