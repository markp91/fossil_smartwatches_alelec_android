package com.fossil;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@SuppressLint({"ViewConstructor"})
public class dj extends ViewGroup implements aj {
    @DexIgnore
    public ViewGroup a;
    @DexIgnore
    public View b;
    @DexIgnore
    public /* final */ View c;
    @DexIgnore
    public int d;
    @DexIgnore
    public Matrix e;
    @DexIgnore
    public /* final */ ViewTreeObserver.OnPreDrawListener f; // = new a();

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements ViewTreeObserver.OnPreDrawListener {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public boolean onPreDraw() {
            View view;
            x9.I(dj.this);
            dj djVar = dj.this;
            ViewGroup viewGroup = djVar.a;
            if (viewGroup == null || (view = djVar.b) == null) {
                return true;
            }
            viewGroup.endViewTransition(view);
            x9.I(dj.this.a);
            dj djVar2 = dj.this;
            djVar2.a = null;
            djVar2.b = null;
            return true;
        }
    }

    @DexIgnore
    public dj(View view) {
        super(view.getContext());
        this.c = view;
        setWillNotDraw(false);
        setLayerType(2, (Paint) null);
    }

    @DexIgnore
    public static void b(View view, ViewGroup viewGroup, Matrix matrix) {
        ViewGroup viewGroup2 = (ViewGroup) view.getParent();
        matrix.reset();
        ek.b(viewGroup2, matrix);
        matrix.preTranslate((float) (-viewGroup2.getScrollX()), (float) (-viewGroup2.getScrollY()));
        ek.c(viewGroup, matrix);
    }

    @DexIgnore
    public void a(Matrix matrix) {
        this.e = matrix;
    }

    @DexIgnore
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        a(this.c, this);
        this.c.getViewTreeObserver().addOnPreDrawListener(this.f);
        ek.a(this.c, 4);
        if (this.c.getParent() != null) {
            ((View) this.c.getParent()).invalidate();
        }
    }

    @DexIgnore
    public void onDetachedFromWindow() {
        this.c.getViewTreeObserver().removeOnPreDrawListener(this.f);
        ek.a(this.c, 0);
        a(this.c, (dj) null);
        if (this.c.getParent() != null) {
            ((View) this.c.getParent()).invalidate();
        }
        super.onDetachedFromWindow();
    }

    @DexIgnore
    public void onDraw(Canvas canvas) {
        wi.a(canvas, true);
        canvas.setMatrix(this.e);
        ek.a(this.c, 0);
        this.c.invalidate();
        ek.a(this.c, 4);
        drawChild(canvas, this.c, getDrawingTime());
        wi.a(canvas, false);
    }

    @DexIgnore
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
    }

    @DexIgnore
    public void setVisibility(int i) {
        super.setVisibility(i);
        if (a(this.c) == this) {
            ek.a(this.c, i == 0 ? 4 : 0);
        }
    }

    @DexIgnore
    public void a(ViewGroup viewGroup, View view) {
        this.a = viewGroup;
        this.b = view;
    }

    @DexIgnore
    public static void a(View view, View view2) {
        ek.a(view2, view2.getLeft(), view2.getTop(), view2.getLeft() + view.getWidth(), view2.getTop() + view.getHeight());
    }

    @DexIgnore
    public static void b(View view) {
        dj a2 = a(view);
        if (a2 != null) {
            a2.d--;
            if (a2.d <= 0) {
                ((bj) a2.getParent()).removeView(a2);
            }
        }
    }

    @DexIgnore
    public static dj a(View view) {
        return (dj) view.getTag(kj.ghost_view);
    }

    @DexIgnore
    public static void a(View view, dj djVar) {
        view.setTag(kj.ghost_view, djVar);
    }

    @DexIgnore
    public static dj a(View view, ViewGroup viewGroup, Matrix matrix) {
        bj bjVar;
        if (view.getParent() instanceof ViewGroup) {
            bj a2 = bj.a(viewGroup);
            dj a3 = a(view);
            int i = 0;
            if (!(a3 == null || (bjVar = (bj) a3.getParent()) == a2)) {
                i = a3.d;
                bjVar.removeView(a3);
                a3 = null;
            }
            if (a3 == null) {
                if (matrix == null) {
                    matrix = new Matrix();
                    b(view, viewGroup, matrix);
                }
                a3 = new dj(view);
                a3.a(matrix);
                if (a2 == null) {
                    a2 = new bj(viewGroup);
                } else {
                    a2.a();
                }
                a((View) viewGroup, (View) a2);
                a((View) viewGroup, (View) a3);
                a2.a(a3);
                a3.d = i;
            } else if (matrix != null) {
                a3.a(matrix);
            }
            a3.d++;
            return a3;
        }
        throw new IllegalArgumentException("Ghosted views must be parented by a ViewGroup");
    }
}
