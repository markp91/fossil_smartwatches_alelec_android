package com.fossil;

import android.view.View;
import android.widget.LinearLayout;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class va4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleSwitchCompat A;
    @DexIgnore
    public /* final */ View B;
    @DexIgnore
    public /* final */ View C;
    @DexIgnore
    public /* final */ View q;
    @DexIgnore
    public /* final */ View r;
    @DexIgnore
    public /* final */ FlexibleButton s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ FlexibleTextView u;
    @DexIgnore
    public /* final */ FlexibleTextView v;
    @DexIgnore
    public /* final */ hh4 w;
    @DexIgnore
    public /* final */ LinearLayout x;
    @DexIgnore
    public /* final */ ConstraintLayout y;
    @DexIgnore
    public /* final */ RecyclerView z;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public va4(Object obj, View view, int i, View view2, View view3, FlexibleButton flexibleButton, ConstraintLayout constraintLayout, ConstraintLayout constraintLayout2, FlexibleTextView flexibleTextView, FlexibleTextView flexibleTextView2, FlexibleTextView flexibleTextView3, FlexibleTextView flexibleTextView4, FlexibleTextView flexibleTextView5, FlexibleTextView flexibleTextView6, FlexibleTextView flexibleTextView7, FlexibleTextView flexibleTextView8, FlexibleTextView flexibleTextView9, FlexibleTextView flexibleTextView10, FlexibleTextView flexibleTextView11, hh4 hh4, ConstraintLayout constraintLayout3, LinearLayout linearLayout, LinearLayout linearLayout2, LinearLayout linearLayout3, ConstraintLayout constraintLayout4, RecyclerView recyclerView, FlexibleSwitchCompat flexibleSwitchCompat, View view4, View view5) {
        super(obj, view, i);
        this.q = view2;
        this.r = view3;
        this.s = flexibleButton;
        this.t = constraintLayout;
        this.u = flexibleTextView;
        this.v = flexibleTextView7;
        this.w = hh4;
        a(this.w);
        this.x = linearLayout;
        this.y = constraintLayout4;
        this.z = recyclerView;
        this.A = flexibleSwitchCompat;
        this.B = view4;
        this.C = view5;
    }
}
