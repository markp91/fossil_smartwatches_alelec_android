package com.fossil;

import android.view.View;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;
import com.portfolio.platform.view.FlexibleEditText;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;
import com.portfolio.platform.view.fastscrollrecyclerview.AlphabetFastScrollRecyclerView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class fc4 extends ViewDataBinding {
    @DexIgnore
    public /* final */ FlexibleEditText q;
    @DexIgnore
    public /* final */ RTLImageView r;
    @DexIgnore
    public /* final */ RTLImageView s;
    @DexIgnore
    public /* final */ ConstraintLayout t;
    @DexIgnore
    public /* final */ AlphabetFastScrollRecyclerView u;

    @DexIgnore
    public fc4(Object obj, View view, int i, FlexibleEditText flexibleEditText, FlexibleTextView flexibleTextView, RTLImageView rTLImageView, RTLImageView rTLImageView2, ConstraintLayout constraintLayout, AlphabetFastScrollRecyclerView alphabetFastScrollRecyclerView) {
        super(obj, view, i);
        this.q = flexibleEditText;
        this.r = rTLImageView;
        this.s = rTLImageView2;
        this.t = constraintLayout;
        this.u = alphabetFastScrollRecyclerView;
    }
}
