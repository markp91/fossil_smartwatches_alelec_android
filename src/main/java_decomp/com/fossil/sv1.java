package com.fossil;

import com.google.android.gms.common.api.Status;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class sv1 extends Exception {
    @DexIgnore
    public /* final */ Status mStatus;

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public sv1(Status status) {
        super(r3.toString());
        int B = status.B();
        String C = status.C() != null ? status.C() : "";
        StringBuilder sb = new StringBuilder(String.valueOf(C).length() + 13);
        sb.append(B);
        sb.append(": ");
        sb.append(C);
        this.mStatus = status;
    }

    @DexIgnore
    public int getStatusCode() {
        return this.mStatus.B();
    }

    @DexIgnore
    @Deprecated
    public String getStatusMessage() {
        return this.mStatus.C();
    }
}
