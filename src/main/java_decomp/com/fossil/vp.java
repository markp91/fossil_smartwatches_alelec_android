package com.fossil;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class vp {
    @DexIgnore
    public /* final */ AtomicInteger a;
    @DexIgnore
    public /* final */ Set<up<?>> b;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<up<?>> c;
    @DexIgnore
    public /* final */ PriorityBlockingQueue<up<?>> d;
    @DexIgnore
    public /* final */ ip e;
    @DexIgnore
    public /* final */ op f;
    @DexIgnore
    public /* final */ xp g;
    @DexIgnore
    public /* final */ pp[] h;
    @DexIgnore
    public jp i;
    @DexIgnore
    public /* final */ List<a> j;

    @DexIgnore
    public interface a<T> {
        @DexIgnore
        void a(up<T> upVar);
    }

    @DexIgnore
    public vp(ip ipVar, op opVar, int i2, xp xpVar) {
        this.a = new AtomicInteger();
        this.b = new HashSet();
        this.c = new PriorityBlockingQueue<>();
        this.d = new PriorityBlockingQueue<>();
        this.j = new ArrayList();
        this.e = ipVar;
        this.f = opVar;
        this.h = new pp[i2];
        this.g = xpVar;
    }

    @DexIgnore
    public int a() {
        return this.a.incrementAndGet();
    }

    @DexIgnore
    public void b() {
        c();
        this.i = new jp(this.c, this.d, this.e, this.g);
        this.i.start();
        for (int i2 = 0; i2 < this.h.length; i2++) {
            pp ppVar = new pp(this.d, this.f, this.e, this.g);
            this.h[i2] = ppVar;
            ppVar.start();
        }
    }

    @DexIgnore
    public void c() {
        jp jpVar = this.i;
        if (jpVar != null) {
            jpVar.b();
        }
        for (pp ppVar : this.h) {
            if (ppVar != null) {
                ppVar.b();
            }
        }
    }

    @DexIgnore
    public <T> up<T> a(up<T> upVar) {
        upVar.setRequestQueue(this);
        synchronized (this.b) {
            this.b.add(upVar);
        }
        upVar.setSequence(a());
        upVar.addMarker("add-to-queue");
        if (!upVar.shouldCache()) {
            this.d.add(upVar);
            return upVar;
        }
        this.c.add(upVar);
        return upVar;
    }

    @DexIgnore
    public <T> void b(up<T> upVar) {
        synchronized (this.b) {
            this.b.remove(upVar);
        }
        synchronized (this.j) {
            for (a a2 : this.j) {
                a2.a(upVar);
            }
        }
    }

    @DexIgnore
    public vp(ip ipVar, op opVar, int i2) {
        this(ipVar, opVar, i2, new mp(new Handler(Looper.getMainLooper())));
    }

    @DexIgnore
    public vp(ip ipVar, op opVar) {
        this(ipVar, opVar, 4);
    }
}
