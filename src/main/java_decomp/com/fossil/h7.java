package com.fossil;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.Handler;
import com.fossil.a7;
import com.fossil.d7;
import com.fossil.j8;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class h7 {
    @DexIgnore
    public static /* final */ m7 a;
    @DexIgnore
    public static /* final */ t4<String, Typeface> b; // = new t4<>(16);

    /*
    static {
        int i = Build.VERSION.SDK_INT;
        if (i >= 28) {
            a = new l7();
        } else if (i >= 26) {
            a = new k7();
        } else if (i >= 24 && j7.a()) {
            a = new j7();
        } else if (Build.VERSION.SDK_INT >= 21) {
            a = new i7();
        } else {
            a = new m7();
        }
    }
    */

    @DexIgnore
    public static String a(Resources resources, int i, int i2) {
        return resources.getResourcePackageName(i) + "-" + i + "-" + i2;
    }

    @DexIgnore
    public static Typeface b(Resources resources, int i, int i2) {
        return b.b(a(resources, i, i2));
    }

    @DexIgnore
    public static Typeface a(Context context, a7.a aVar, Resources resources, int i, int i2, d7.a aVar2, Handler handler, boolean z) {
        Typeface typeface;
        if (aVar instanceof a7.d) {
            a7.d dVar = (a7.d) aVar;
            boolean z2 = false;
            if (!z ? aVar2 == null : dVar.a() == 0) {
                z2 = true;
            }
            typeface = j8.a(context, dVar.b(), aVar2, handler, z2, z ? dVar.c() : -1, i2);
        } else {
            typeface = a.a(context, (a7.b) aVar, resources, i2);
            if (aVar2 != null) {
                if (typeface != null) {
                    aVar2.a(typeface, handler);
                } else {
                    aVar2.a(-3, handler);
                }
            }
        }
        if (typeface != null) {
            b.a(a(resources, i, i2), typeface);
        }
        return typeface;
    }

    @DexIgnore
    public static Typeface b(Context context, Typeface typeface, int i) {
        a7.b a2 = a.a(typeface);
        if (a2 == null) {
            return null;
        }
        return a.a(context, a2, context.getResources(), i);
    }

    @DexIgnore
    public static Typeface a(Context context, Resources resources, int i, String str, int i2) {
        Typeface a2 = a.a(context, resources, i, str, i2);
        if (a2 != null) {
            b.a(a(resources, i, i2), a2);
        }
        return a2;
    }

    @DexIgnore
    public static Typeface a(Context context, CancellationSignal cancellationSignal, j8.f[] fVarArr, int i) {
        return a.a(context, cancellationSignal, fVarArr, i);
    }

    @DexIgnore
    public static Typeface a(Context context, Typeface typeface, int i) {
        Typeface b2;
        if (context == null) {
            throw new IllegalArgumentException("Context cannot be null");
        } else if (Build.VERSION.SDK_INT >= 21 || (b2 = b(context, typeface, i)) == null) {
            return Typeface.create(typeface, i);
        } else {
            return b2;
        }
    }
}
