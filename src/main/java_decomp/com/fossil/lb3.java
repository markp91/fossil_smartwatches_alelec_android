package com.fossil;

import com.fossil.rv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class lb3 implements rv1.d.f {
    @DexIgnore
    public static /* final */ lb3 j; // = new lb3(false, false, (String) null, false, (String) null, (String) null, false, (Long) null, (Long) null);
    @DexIgnore
    public /* final */ boolean a; // = false;
    @DexIgnore
    public /* final */ boolean b; // = false;
    @DexIgnore
    public /* final */ String c; // = null;
    @DexIgnore
    public /* final */ boolean d; // = false;
    @DexIgnore
    public /* final */ String e; // = null;
    @DexIgnore
    public /* final */ String f; // = null;
    @DexIgnore
    public /* final */ boolean g; // = false;
    @DexIgnore
    public /* final */ Long h; // = null;
    @DexIgnore
    public /* final */ Long i; // = null;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
    }

    /*
    static {
        new a();
    }
    */

    @DexIgnore
    public lb3(boolean z, boolean z2, String str, boolean z3, String str2, String str3, boolean z4, Long l, Long l2) {
    }

    @DexIgnore
    public final Long a() {
        return this.h;
    }

    @DexIgnore
    public final String b() {
        return this.e;
    }

    @DexIgnore
    public final String c() {
        return this.f;
    }

    @DexIgnore
    public final Long d() {
        return this.i;
    }

    @DexIgnore
    public final String e() {
        return this.c;
    }

    @DexIgnore
    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof lb3)) {
            return false;
        }
        lb3 lb3 = (lb3) obj;
        return this.a == lb3.a && this.b == lb3.b && u12.a(this.c, lb3.c) && this.d == lb3.d && this.g == lb3.g && u12.a(this.e, lb3.e) && u12.a(this.f, lb3.f) && u12.a(this.h, lb3.h) && u12.a(this.i, lb3.i);
    }

    @DexIgnore
    public final boolean f() {
        return this.d;
    }

    @DexIgnore
    public final boolean g() {
        return this.b;
    }

    @DexIgnore
    public final boolean h() {
        return this.a;
    }

    @DexIgnore
    public final int hashCode() {
        return u12.a(Boolean.valueOf(this.a), Boolean.valueOf(this.b), this.c, Boolean.valueOf(this.d), Boolean.valueOf(this.g), this.e, this.f, this.h, this.i);
    }

    @DexIgnore
    public final boolean i() {
        return this.g;
    }
}
