package com.fossil;

import android.os.Parcel;
import java.util.Arrays;
import org.json.JSONArray;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zs0 extends em0 {
    @DexIgnore
    public static /* final */ gr0 CREATOR; // = new gr0((qg6) null);
    @DexIgnore
    public /* final */ lr0[] c;
    @DexIgnore
    public /* final */ byte[] d;

    @DexIgnore
    public zs0(byte b, lr0[] lr0Arr, byte[] bArr) {
        super(xh0.BACKGROUND_SYNC_EVENT, b);
        this.c = lr0Arr;
        this.d = bArr;
    }

    @DexIgnore
    public JSONObject a() {
        JSONArray jSONArray = new JSONArray();
        for (lr0 a : this.c) {
            jSONArray.put(a.a());
        }
        return cw0.a(cw0.a(super.a(), bm0.FRAMES, (Object) jSONArray), bm0.TOTAL_FRAMES, (Object) Integer.valueOf(jSONArray.length()));
    }

    @DexIgnore
    public byte[] b() {
        return this.d;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(zs0.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            zs0 zs0 = (zs0) obj;
            return this.b == zs0.b && Arrays.equals(this.c, zs0.c) && Arrays.equals(this.d, zs0.d);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.asyncevent.BackgroundSyncEvent");
    }

    @DexIgnore
    public int hashCode() {
        return Arrays.hashCode(this.d) + (((this.b * 31) + this.c.hashCode()) * 31);
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeTypedArray(this.c, i);
        }
        if (parcel != null) {
            parcel.writeByteArray(this.d);
        }
    }

    @DexIgnore
    public /* synthetic */ zs0(Parcel parcel, qg6 qg6) {
        super(parcel);
        Object[] createTypedArray = parcel.createTypedArray(lr0.CREATOR);
        if (createTypedArray != null) {
            this.c = (lr0[]) createTypedArray;
            byte[] createByteArray = parcel.createByteArray();
            if (createByteArray != null) {
                this.d = createByteArray;
            } else {
                wg6.a();
                throw null;
            }
        } else {
            wg6.a();
            throw null;
        }
    }
}
