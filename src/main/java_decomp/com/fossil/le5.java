package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class le5 {
    @DexIgnore
    public /* final */ ge5 a;
    @DexIgnore
    public /* final */ ve5 b;
    @DexIgnore
    public /* final */ qe5 c;

    @DexIgnore
    public le5(ge5 ge5, ve5 ve5, qe5 qe5) {
        wg6.b(ge5, "mGoalTrackingOverviewDayView");
        wg6.b(ve5, "mGoalTrackingOverviewWeekView");
        wg6.b(qe5, "mGoalTrackingOverviewMonthView");
        this.a = ge5;
        this.b = ve5;
        this.c = qe5;
    }

    @DexIgnore
    public final ge5 a() {
        return this.a;
    }

    @DexIgnore
    public final qe5 b() {
        return this.c;
    }

    @DexIgnore
    public final ve5 c() {
        return this.b;
    }
}
