package com.fossil;

import android.content.Context;
import android.os.Looper;
import com.fossil.rv1;
import com.fossil.wv1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class zb2 extends rv1.a<cf2, rv1.d.C0044d> {
    @DexIgnore
    public zb2() {
    }

    @DexIgnore
    public final /* synthetic */ rv1.f a(Context context, Looper looper, e12 e12, Object obj, wv1.b bVar, wv1.c cVar) {
        rv1.d.C0044d dVar = (rv1.d.C0044d) obj;
        return new cf2(context, looper, e12, bVar, cVar);
    }
}
