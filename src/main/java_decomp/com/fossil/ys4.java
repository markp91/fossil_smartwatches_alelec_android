package com.fossil;

import dagger.internal.Factory;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class ys4 implements Factory<xs4> {
    @DexIgnore
    public static /* final */ ys4 a; // = new ys4();

    @DexIgnore
    public static ys4 a() {
        return a;
    }

    @DexIgnore
    public static xs4 b() {
        return new xs4();
    }

    @DexIgnore
    public xs4 get() {
        return b();
    }
}
