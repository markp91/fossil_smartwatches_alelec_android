package com.fossil;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj0 extends vf1 {
    @DexIgnore
    public long K;
    @DexIgnore
    public long L;
    @DexIgnore
    public long M;
    @DexIgnore
    public /* final */ long N;
    @DexIgnore
    public /* final */ long O;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public /* synthetic */ uj0(long j, long j2, short s, ue1 ue1, int i, int i2) {
        super(dl1.VERIFY_DATA, s, lx0.VERIFY_DATA, ue1, (i2 & 16) != 0 ? 3 : i);
        this.N = j;
        this.O = j2;
    }

    @DexIgnore
    public JSONObject a(byte[] bArr) {
        this.C = true;
        JSONObject jSONObject = new JSONObject();
        if (bArr.length >= 12) {
            ByteBuffer order = ByteBuffer.wrap(bArr).order(ByteOrder.LITTLE_ENDIAN);
            this.K = cw0.b(order.getInt(0));
            this.L = cw0.b(order.getInt(4));
            this.M = cw0.b(order.getInt(8));
            cw0.a(cw0.a(cw0.a(jSONObject, bm0.VERIFIED_DATA_OFFSET, (Object) Long.valueOf(this.K)), bm0.VERIFIED_DATA_LENGTH, (Object) Long.valueOf(this.L)), bm0.VERIFIED_DATA_CRC, (Object) Long.valueOf(this.M));
        }
        return jSONObject;
    }

    @DexIgnore
    public JSONObject h() {
        return cw0.a(cw0.a(super.h(), bm0.OFFSET, (Object) Long.valueOf(this.N)), bm0.LENGTH, (Object) Long.valueOf(this.O));
    }

    @DexIgnore
    public JSONObject i() {
        return cw0.a(cw0.a(cw0.a(super.i(), bm0.VERIFIED_DATA_OFFSET, (Object) Long.valueOf(this.K)), bm0.VERIFIED_DATA_LENGTH, (Object) Long.valueOf(this.L)), bm0.VERIFIED_DATA_CRC, (Object) Long.valueOf(this.M));
    }

    @DexIgnore
    public byte[] n() {
        byte[] array = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.N).putInt((int) this.O).array();
        wg6.a(array, "ByteBuffer.allocate(8)\n \u2026                 .array()");
        return array;
    }
}
