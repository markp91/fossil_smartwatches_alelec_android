package com.fossil;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ux {
    @DexIgnore
    public /* final */ kr a;
    @DexIgnore
    public /* final */ Handler b;
    @DexIgnore
    public /* final */ List<b> c;
    @DexIgnore
    public /* final */ fr d;
    @DexIgnore
    public /* final */ au e;
    @DexIgnore
    public boolean f;
    @DexIgnore
    public boolean g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public er<Bitmap> i;
    @DexIgnore
    public a j;
    @DexIgnore
    public boolean k;
    @DexIgnore
    public a l;
    @DexIgnore
    public Bitmap m;
    @DexIgnore
    public a n;
    @DexIgnore
    public d o;
    @DexIgnore
    public int p;
    @DexIgnore
    public int q;
    @DexIgnore
    public int r;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends tz<Bitmap> {
        @DexIgnore
        public /* final */ Handler d;
        @DexIgnore
        public /* final */ int e;
        @DexIgnore
        public /* final */ long f;
        @DexIgnore
        public Bitmap g;

        @DexIgnore
        public a(Handler handler, int i, long j) {
            this.d = handler;
            this.e = i;
            this.f = j;
        }

        @DexIgnore
        public void c(Drawable drawable) {
            this.g = null;
        }

        @DexIgnore
        public Bitmap e() {
            return this.g;
        }

        @DexIgnore
        public void a(Bitmap bitmap, b00<? super Bitmap> b00) {
            this.g = bitmap;
            this.d.sendMessageAtTime(this.d.obtainMessage(1, this), this.f);
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a();
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class c implements Handler.Callback {
        @DexIgnore
        public c() {
        }

        @DexIgnore
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                ux.this.a((a) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                ux.this.d.a((yz<?>) (a) message.obj);
                return false;
            }
        }
    }

    @DexIgnore
    public interface d {
        @DexIgnore
        void a();
    }

    @DexIgnore
    public ux(wq wqVar, kr krVar, int i2, int i3, bs<Bitmap> bsVar, Bitmap bitmap) {
        this(wqVar.c(), wq.d(wqVar.e()), krVar, (Handler) null, a(wq.d(wqVar.e()), i2, i3), bsVar, bitmap);
    }

    @DexIgnore
    public static vr n() {
        return new g00(Double.valueOf(Math.random()));
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r3v0, types: [com.fossil.bs<android.graphics.Bitmap>, com.fossil.bs, java.lang.Object] */
    /* JADX WARNING: Unknown variable types count: 1 */
    public void a(bs<Bitmap> r3, Bitmap bitmap) {
        q00.a(r3);
        bs bsVar = r3;
        q00.a(bitmap);
        this.m = bitmap;
        this.i = this.i.a((gz<?>) new nz().a((bs<Bitmap>) r3));
        this.p = r00.a(bitmap);
        this.q = bitmap.getWidth();
        this.r = bitmap.getHeight();
    }

    @DexIgnore
    public void b(b bVar) {
        this.c.remove(bVar);
        if (this.c.isEmpty()) {
            m();
        }
    }

    @DexIgnore
    public Bitmap c() {
        a aVar = this.j;
        return aVar != null ? aVar.e() : this.m;
    }

    @DexIgnore
    public int d() {
        a aVar = this.j;
        if (aVar != null) {
            return aVar.e;
        }
        return -1;
    }

    @DexIgnore
    public Bitmap e() {
        return this.m;
    }

    @DexIgnore
    public int f() {
        return this.a.c();
    }

    @DexIgnore
    public int g() {
        return this.r;
    }

    @DexIgnore
    public int h() {
        return this.a.h() + this.p;
    }

    @DexIgnore
    public int i() {
        return this.q;
    }

    @DexIgnore
    public final void j() {
        if (this.f && !this.g) {
            if (this.h) {
                q00.a(this.n == null, "Pending target must be null when starting from the first frame");
                this.a.f();
                this.h = false;
            }
            a aVar = this.n;
            if (aVar != null) {
                this.n = null;
                a(aVar);
                return;
            }
            this.g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.a.d());
            this.a.b();
            this.l = new a(this.b, this.a.g(), uptimeMillis);
            this.i.a((gz<?>) nz.b(n())).a((Object) this.a).a(this.l);
        }
    }

    @DexIgnore
    public final void k() {
        Bitmap bitmap = this.m;
        if (bitmap != null) {
            this.e.a(bitmap);
            this.m = null;
        }
    }

    @DexIgnore
    public final void l() {
        if (!this.f) {
            this.f = true;
            this.k = false;
            j();
        }
    }

    @DexIgnore
    public final void m() {
        this.f = false;
    }

    @DexIgnore
    public ByteBuffer b() {
        return this.a.e().asReadOnlyBuffer();
    }

    @DexIgnore
    public ux(au auVar, fr frVar, kr krVar, Handler handler, er<Bitmap> erVar, bs<Bitmap> bsVar, Bitmap bitmap) {
        this.c = new ArrayList();
        this.d = frVar;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new c()) : handler;
        this.e = auVar;
        this.b = handler;
        this.i = erVar;
        this.a = krVar;
        a(bsVar, bitmap);
    }

    @DexIgnore
    public void a(b bVar) {
        if (this.k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.c.contains(bVar)) {
            boolean isEmpty = this.c.isEmpty();
            this.c.add(bVar);
            if (isEmpty) {
                l();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    @DexIgnore
    public void a() {
        this.c.clear();
        k();
        m();
        a aVar = this.j;
        if (aVar != null) {
            this.d.a((yz<?>) aVar);
            this.j = null;
        }
        a aVar2 = this.l;
        if (aVar2 != null) {
            this.d.a((yz<?>) aVar2);
            this.l = null;
        }
        a aVar3 = this.n;
        if (aVar3 != null) {
            this.d.a((yz<?>) aVar3);
            this.n = null;
        }
        this.a.clear();
        this.k = true;
    }

    @DexIgnore
    public void a(a aVar) {
        d dVar = this.o;
        if (dVar != null) {
            dVar.a();
        }
        this.g = false;
        if (this.k) {
            this.b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f) {
            this.n = aVar;
        } else {
            if (aVar.e() != null) {
                k();
                a aVar2 = this.j;
                this.j = aVar;
                for (int size = this.c.size() - 1; size >= 0; size--) {
                    this.c.get(size).a();
                }
                if (aVar2 != null) {
                    this.b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            j();
        }
    }

    @DexIgnore
    public static er<Bitmap> a(fr frVar, int i2, int i3) {
        return frVar.e().a((gz<?>) ((nz) ((nz) nz.b(ft.a).b(true)).a(true)).a(i2, i3));
    }
}
