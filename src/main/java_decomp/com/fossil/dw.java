package com.fossil;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class dw<DataType> implements zr<DataType, BitmapDrawable> {
    @DexIgnore
    public /* final */ zr<DataType, Bitmap> a;
    @DexIgnore
    public /* final */ Resources b;

    @DexIgnore
    public dw(Resources resources, zr<DataType, Bitmap> zrVar) {
        q00.a(resources);
        this.b = resources;
        q00.a(zrVar);
        this.a = zrVar;
    }

    @DexIgnore
    public boolean a(DataType datatype, xr xrVar) throws IOException {
        return this.a.a(datatype, xrVar);
    }

    @DexIgnore
    public rt<BitmapDrawable> a(DataType datatype, int i, int i2, xr xrVar) throws IOException {
        return xw.a(this.b, this.a.a(datatype, i, i2, xrVar));
    }
}
