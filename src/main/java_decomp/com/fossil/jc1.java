package com.fossil;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jc1 implements Runnable {
    @DexIgnore
    public /* final */ /* synthetic */ File a;
    @DexIgnore
    public /* final */ /* synthetic */ String b;

    @DexIgnore
    public jc1(File file, ag1 ag1, String str) {
        this.a = file;
        this.b = str;
    }

    @DexIgnore
    public final void run() {
        FileWriter fileWriter;
        String str = this.b + mi0.A.y();
        try {
            fileWriter = new FileWriter(this.a, true);
            try {
                fileWriter.append(str);
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException unused) {
            }
        } catch (IOException unused2) {
            fileWriter = null;
            if (fileWriter != null) {
                try {
                    fileWriter.flush();
                } catch (IOException unused3) {
                    return;
                }
            }
            if (fileWriter != null) {
                fileWriter.close();
            }
        }
    }
}
