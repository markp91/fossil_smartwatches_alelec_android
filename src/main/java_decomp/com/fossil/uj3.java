package com.fossil;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Space;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatTextView;
import com.facebook.places.internal.LocationScannerImpl;
import com.google.android.material.textfield.TextInputLayout;
import java.util.ArrayList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uj3 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ TextInputLayout b;
    @DexIgnore
    public LinearLayout c;
    @DexIgnore
    public int d;
    @DexIgnore
    public FrameLayout e;
    @DexIgnore
    public int f;
    @DexIgnore
    public Animator g;
    @DexIgnore
    public /* final */ float h; // = ((float) this.a.getResources().getDimensionPixelSize(pf3.design_textinput_caption_translate_y));
    @DexIgnore
    public int i;
    @DexIgnore
    public int j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public boolean l;
    @DexIgnore
    public TextView m;
    @DexIgnore
    public int n;
    @DexIgnore
    public ColorStateList o;
    @DexIgnore
    public CharSequence p;
    @DexIgnore
    public boolean q;
    @DexIgnore
    public TextView r;
    @DexIgnore
    public int s;
    @DexIgnore
    public ColorStateList t;
    @DexIgnore
    public Typeface u;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a extends AnimatorListenerAdapter {
        @DexIgnore
        public /* final */ /* synthetic */ int a;
        @DexIgnore
        public /* final */ /* synthetic */ TextView b;
        @DexIgnore
        public /* final */ /* synthetic */ int c;
        @DexIgnore
        public /* final */ /* synthetic */ TextView d;

        @DexIgnore
        public a(int i, TextView textView, int i2, TextView textView2) {
            this.a = i;
            this.b = textView;
            this.c = i2;
            this.d = textView2;
        }

        @DexIgnore
        public void onAnimationEnd(Animator animator) {
            int unused = uj3.this.i = this.a;
            Animator unused2 = uj3.this.g = null;
            TextView textView = this.b;
            if (textView != null) {
                textView.setVisibility(4);
                if (this.c == 1 && uj3.this.m != null) {
                    uj3.this.m.setText((CharSequence) null);
                }
                TextView textView2 = this.d;
                if (textView2 != null) {
                    textView2.setTranslationY(LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
                    this.d.setAlpha(1.0f);
                }
            }
        }

        @DexIgnore
        public void onAnimationStart(Animator animator) {
            TextView textView = this.d;
            if (textView != null) {
                textView.setVisibility(0);
            }
        }
    }

    @DexIgnore
    public uj3(TextInputLayout textInputLayout) {
        this.a = textInputLayout.getContext();
        this.b = textInputLayout;
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        c();
        this.p = charSequence;
        this.r.setText(charSequence);
        if (this.i != 2) {
            this.j = 2;
        }
        a(this.i, this.j, a(this.r, charSequence));
    }

    @DexIgnore
    public void c() {
        Animator animator = this.g;
        if (animator != null) {
            animator.cancel();
        }
    }

    @DexIgnore
    public boolean c(int i2) {
        return i2 == 0 || i2 == 1;
    }

    @DexIgnore
    public boolean d() {
        return b(this.j);
    }

    @DexIgnore
    public CharSequence e() {
        return this.k;
    }

    @DexIgnore
    public int f() {
        TextView textView = this.m;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    @DexIgnore
    public ColorStateList g() {
        TextView textView = this.m;
        if (textView != null) {
            return textView.getTextColors();
        }
        return null;
    }

    @DexIgnore
    public CharSequence h() {
        return this.p;
    }

    @DexIgnore
    public int i() {
        TextView textView = this.r;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return -1;
    }

    @DexIgnore
    public void j() {
        this.k = null;
        c();
        if (this.i == 1) {
            if (!this.q || TextUtils.isEmpty(this.p)) {
                this.j = 0;
            } else {
                this.j = 2;
            }
        }
        a(this.i, this.j, a(this.m, (CharSequence) null));
    }

    @DexIgnore
    public void k() {
        c();
        if (this.i == 2) {
            this.j = 0;
        }
        a(this.i, this.j, a(this.r, (CharSequence) null));
    }

    @DexIgnore
    public boolean l() {
        return this.l;
    }

    @DexIgnore
    public boolean m() {
        return this.q;
    }

    @DexIgnore
    public void d(int i2) {
        this.n = i2;
        TextView textView = this.m;
        if (textView != null) {
            this.b.a(textView, i2);
        }
    }

    @DexIgnore
    public void e(int i2) {
        this.s = i2;
        TextView textView = this.r;
        if (textView != null) {
            ua.d(textView, i2);
        }
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        c();
        this.k = charSequence;
        this.m.setText(charSequence);
        if (this.i != 1) {
            this.j = 1;
        }
        a(this.i, this.j, a(this.m, charSequence));
    }

    @DexIgnore
    public final boolean b() {
        return (this.c == null || this.b.getEditText() == null) ? false : true;
    }

    @DexIgnore
    public void b(TextView textView, int i2) {
        FrameLayout frameLayout;
        if (this.c != null) {
            if (!c(i2) || (frameLayout = this.e) == null) {
                this.c.removeView(textView);
            } else {
                this.f--;
                a((ViewGroup) frameLayout, this.f);
                this.e.removeView(textView);
            }
            this.d--;
            a((ViewGroup) this.c, this.d);
        }
    }

    @DexIgnore
    public final boolean a(TextView textView, CharSequence charSequence) {
        return x9.E(this.b) && this.b.isEnabled() && (this.j != this.i || textView == null || !TextUtils.equals(textView.getText(), charSequence));
    }

    @DexIgnore
    public final void a(int i2, int i3, boolean z) {
        if (z) {
            AnimatorSet animatorSet = new AnimatorSet();
            this.g = animatorSet;
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = arrayList;
            int i4 = i2;
            int i5 = i3;
            a(arrayList2, this.q, this.r, 2, i4, i5);
            a(arrayList2, this.l, this.m, 1, i4, i5);
            zf3.a(animatorSet, arrayList);
            animatorSet.addListener(new a(i3, a(i2), i2, a(i3)));
            animatorSet.start();
        } else {
            a(i2, i3);
        }
        this.b.z();
        this.b.d(z);
        this.b.D();
    }

    @DexIgnore
    public void b(boolean z) {
        if (this.q != z) {
            c();
            if (z) {
                this.r = new AppCompatTextView(this.a);
                this.r.setId(rf3.textinput_helper_text);
                Typeface typeface = this.u;
                if (typeface != null) {
                    this.r.setTypeface(typeface);
                }
                this.r.setVisibility(4);
                x9.g(this.r, 1);
                e(this.s);
                b(this.t);
                a(this.r, 1);
            } else {
                k();
                b(this.r, 1);
                this.r = null;
                this.b.z();
                this.b.D();
            }
            this.q = z;
        }
    }

    @DexIgnore
    public final void a(int i2, int i3) {
        TextView a2;
        TextView a3;
        if (i2 != i3) {
            if (!(i3 == 0 || (a3 = a(i3)) == null)) {
                a3.setVisibility(0);
                a3.setAlpha(1.0f);
            }
            if (!(i2 == 0 || (a2 = a(i2)) == null)) {
                a2.setVisibility(4);
                if (i2 == 1) {
                    a2.setText((CharSequence) null);
                }
            }
            this.i = i3;
        }
    }

    @DexIgnore
    public final boolean b(int i2) {
        if (i2 != 1 || this.m == null || TextUtils.isEmpty(this.k)) {
            return false;
        }
        return true;
    }

    @DexIgnore
    public final void a(List<Animator> list, boolean z, TextView textView, int i2, int i3, int i4) {
        if (textView != null && z) {
            if (i2 == i4 || i2 == i3) {
                list.add(a(textView, i4 == i2));
                if (i4 == i2) {
                    list.add(a(textView));
                }
            }
        }
    }

    @DexIgnore
    public void b(ColorStateList colorStateList) {
        this.t = colorStateList;
        TextView textView = this.r;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }

    @DexIgnore
    public final ObjectAnimator a(TextView textView, boolean z) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.ALPHA, new float[]{z ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES});
        ofFloat.setDuration(167);
        ofFloat.setInterpolator(yf3.a);
        return ofFloat;
    }

    @DexIgnore
    public final ObjectAnimator a(TextView textView) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(textView, View.TRANSLATION_Y, new float[]{-this.h, 0.0f});
        ofFloat.setDuration(217);
        ofFloat.setInterpolator(yf3.d);
        return ofFloat;
    }

    @DexIgnore
    public final TextView a(int i2) {
        if (i2 == 1) {
            return this.m;
        }
        if (i2 != 2) {
            return null;
        }
        return this.r;
    }

    @DexIgnore
    public void a() {
        if (b()) {
            x9.b(this.c, x9.t(this.b.getEditText()), 0, x9.s(this.b.getEditText()), 0);
        }
    }

    @DexIgnore
    public void a(TextView textView, int i2) {
        if (this.c == null && this.e == null) {
            this.c = new LinearLayout(this.a);
            this.c.setOrientation(0);
            this.b.addView(this.c, -1, -2);
            this.e = new FrameLayout(this.a);
            this.c.addView(this.e, -1, new FrameLayout.LayoutParams(-2, -2));
            this.c.addView(new Space(this.a), new LinearLayout.LayoutParams(0, 0, 1.0f));
            if (this.b.getEditText() != null) {
                a();
            }
        }
        if (c(i2)) {
            this.e.setVisibility(0);
            this.e.addView(textView);
            this.f++;
        } else {
            this.c.addView(textView, i2);
        }
        this.c.setVisibility(0);
        this.d++;
    }

    @DexIgnore
    public final void a(ViewGroup viewGroup, int i2) {
        if (i2 == 0) {
            viewGroup.setVisibility(8);
        }
    }

    @DexIgnore
    public void a(boolean z) {
        if (this.l != z) {
            c();
            if (z) {
                this.m = new AppCompatTextView(this.a);
                this.m.setId(rf3.textinput_error);
                Typeface typeface = this.u;
                if (typeface != null) {
                    this.m.setTypeface(typeface);
                }
                d(this.n);
                a(this.o);
                this.m.setVisibility(4);
                x9.g(this.m, 1);
                a(this.m, 0);
            } else {
                j();
                b(this.m, 0);
                this.m = null;
                this.b.z();
                this.b.D();
            }
            this.l = z;
        }
    }

    @DexIgnore
    public void a(Typeface typeface) {
        if (typeface != this.u) {
            this.u = typeface;
            a(this.m, typeface);
            a(this.r, typeface);
        }
    }

    @DexIgnore
    public final void a(TextView textView, Typeface typeface) {
        if (textView != null) {
            textView.setTypeface(typeface);
        }
    }

    @DexIgnore
    public void a(ColorStateList colorStateList) {
        this.o = colorStateList;
        TextView textView = this.m;
        if (textView != null && colorStateList != null) {
            textView.setTextColor(colorStateList);
        }
    }
}
