package com.fossil;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendar;
import com.google.android.material.datepicker.MaterialCalendarGridView;
import com.google.android.material.datepicker.MaterialDatePicker;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class jh3 extends RecyclerView.g<b> {
    @DexIgnore
    public /* final */ zg3 a;
    @DexIgnore
    public /* final */ ch3<?> b;
    @DexIgnore
    public /* final */ MaterialCalendar.l c;
    @DexIgnore
    public /* final */ int d;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements AdapterView.OnItemClickListener {
        @DexIgnore
        public /* final */ /* synthetic */ MaterialCalendarGridView a;

        @DexIgnore
        public a(MaterialCalendarGridView materialCalendarGridView) {
            this.a = materialCalendarGridView;
        }

        @DexIgnore
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
            if (this.a.getAdapter().e(i)) {
                jh3.this.c.a(this.a.getAdapter().getItem(i).longValue());
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class b extends RecyclerView.ViewHolder {
        @DexIgnore
        public /* final */ TextView a;
        @DexIgnore
        public /* final */ MaterialCalendarGridView b;

        @DexIgnore
        public b(LinearLayout linearLayout, boolean z) {
            super(linearLayout);
            this.a = (TextView) linearLayout.findViewById(rf3.month_title);
            x9.a((View) this.a, true);
            this.b = (MaterialCalendarGridView) linearLayout.findViewById(rf3.month_grid);
            if (!z) {
                this.a.setVisibility(8);
            }
        }
    }

    @DexIgnore
    public jh3(Context context, ch3<?> ch3, zg3 zg3, MaterialCalendar.l lVar) {
        hh3 e = zg3.e();
        hh3 b2 = zg3.b();
        hh3 d2 = zg3.d();
        if (e.compareTo(d2) > 0) {
            throw new IllegalArgumentException("firstPage cannot be after currentPage");
        } else if (d2.compareTo(b2) <= 0) {
            this.d = (ih3.e * MaterialCalendar.a(context)) + (MaterialDatePicker.f(context) ? MaterialCalendar.a(context) : 0);
            this.a = zg3;
            this.b = ch3;
            this.c = lVar;
            setHasStableIds(true);
        } else {
            throw new IllegalArgumentException("currentPage cannot be after lastPage");
        }
    }

    @DexIgnore
    public CharSequence b(int i) {
        return a(i).b();
    }

    @DexIgnore
    public int getItemCount() {
        return this.a.c();
    }

    @DexIgnore
    public long getItemId(int i) {
        return this.a.e().b(i).c();
    }

    @DexIgnore
    /* renamed from: a */
    public void onBindViewHolder(b bVar, int i) {
        hh3 b2 = this.a.e().b(i);
        bVar.a.setText(b2.b());
        MaterialCalendarGridView materialCalendarGridView = (MaterialCalendarGridView) bVar.b.findViewById(rf3.month_grid);
        if (materialCalendarGridView.getAdapter() == null || !b2.equals(materialCalendarGridView.getAdapter().a)) {
            ih3 ih3 = new ih3(b2, this.b, this.a);
            materialCalendarGridView.setNumColumns(b2.e);
            materialCalendarGridView.setAdapter((ListAdapter) ih3);
        } else {
            materialCalendarGridView.getAdapter().notifyDataSetChanged();
        }
        materialCalendarGridView.setOnItemClickListener(new a(materialCalendarGridView));
    }

    @DexIgnore
    public b onCreateViewHolder(ViewGroup viewGroup, int i) {
        LinearLayout linearLayout = (LinearLayout) LayoutInflater.from(viewGroup.getContext()).inflate(tf3.mtrl_calendar_month_labeled, viewGroup, false);
        if (!MaterialDatePicker.f(viewGroup.getContext())) {
            return new b(linearLayout, false);
        }
        linearLayout.setLayoutParams(new RecyclerView.LayoutParams(-1, this.d));
        return new b(linearLayout, true);
    }

    @DexIgnore
    public hh3 a(int i) {
        return this.a.e().b(i);
    }

    @DexIgnore
    public int a(hh3 hh3) {
        return this.a.e().b(hh3);
    }
}
