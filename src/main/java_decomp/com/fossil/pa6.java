package com.fossil;

import android.content.Context;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class pa6 implements ka6 {
    @DexIgnore
    public /* final */ Context a;
    @DexIgnore
    public /* final */ File b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ File d;
    @DexIgnore
    public l96 e; // = new l96(this.d);
    @DexIgnore
    public File f;

    @DexIgnore
    public pa6(Context context, File file, String str, String str2) throws IOException {
        this.a = context;
        this.b = file;
        this.c = str2;
        this.d = new File(this.b, str);
        e();
    }

    @DexIgnore
    public OutputStream a(File file) throws IOException {
        throw null;
    }

    @DexIgnore
    public void a(byte[] bArr) throws IOException {
        this.e.a(bArr);
    }

    @DexIgnore
    public boolean b() {
        return this.e.l();
    }

    @DexIgnore
    public List<File> c() {
        return Arrays.asList(this.f.listFiles());
    }

    @DexIgnore
    public void d() {
        try {
            this.e.close();
        } catch (IOException unused) {
        }
        this.d.delete();
    }

    @DexIgnore
    public final void e() {
        this.f = new File(this.b, this.c);
        if (!this.f.exists()) {
            this.f.mkdirs();
        }
    }

    @DexIgnore
    public int a() {
        return this.e.p();
    }

    @DexIgnore
    public void a(String str) throws IOException {
        this.e.close();
        a(this.d, new File(this.f, str));
        this.e = new l96(this.d);
    }

    @DexIgnore
    public final void a(File file, File file2) throws IOException {
        FileInputStream fileInputStream;
        OutputStream outputStream = null;
        try {
            fileInputStream = new FileInputStream(file);
            try {
                outputStream = a(file2);
                z86.a((InputStream) fileInputStream, outputStream, new byte[1024]);
                z86.a((Closeable) fileInputStream, "Failed to close file input stream");
                z86.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
            } catch (Throwable th) {
                th = th;
                z86.a((Closeable) fileInputStream, "Failed to close file input stream");
                z86.a((Closeable) outputStream, "Failed to close output stream");
                file.delete();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileInputStream = null;
            z86.a((Closeable) fileInputStream, "Failed to close file input stream");
            z86.a((Closeable) outputStream, "Failed to close output stream");
            file.delete();
            throw th;
        }
    }

    @DexIgnore
    public List<File> a(int i) {
        ArrayList arrayList = new ArrayList();
        for (File add : this.f.listFiles()) {
            arrayList.add(add);
            if (arrayList.size() >= i) {
                break;
            }
        }
        return arrayList;
    }

    @DexIgnore
    public void a(List<File> list) {
        for (File next : list) {
            z86.c(this.a, String.format("deleting sent analytics file %s", new Object[]{next.getName()}));
            next.delete();
        }
    }

    @DexIgnore
    public boolean a(int i, int i2) {
        return this.e.a(i, i2);
    }
}
