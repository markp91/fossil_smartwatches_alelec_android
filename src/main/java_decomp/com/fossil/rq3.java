package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface rq3 {
    @DexIgnore
    <T> void a(Class<T> cls, pq3<? super T> pq3);

    @DexIgnore
    <T> void a(Class<T> cls, Executor executor, pq3<? super T> pq3);
}
