package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ro6<T> extends ck6<T> implements kf6 {
    @DexIgnore
    public /* final */ xe6<T> d;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ro6(af6 af6, xe6<? super T> xe6) {
        super(af6, true);
        wg6.b(af6, "context");
        wg6.b(xe6, "uCont");
        this.d = xe6;
    }

    @DexIgnore
    public void a(Object obj, int i) {
        if (obj instanceof vk6) {
            Throwable th = ((vk6) obj).a;
            if (i != 4) {
                th = to6.a(th, (xe6<?>) this.d);
            }
            jn6.a(this.d, th, i);
            return;
        }
        jn6.b(this.d, obj, i);
    }

    @DexIgnore
    public final boolean f() {
        return true;
    }

    @DexIgnore
    public final kf6 getCallerFrame() {
        return (kf6) this.d;
    }

    @DexIgnore
    public final StackTraceElement getStackTraceElement() {
        return null;
    }

    @DexIgnore
    public int j() {
        return 2;
    }
}
