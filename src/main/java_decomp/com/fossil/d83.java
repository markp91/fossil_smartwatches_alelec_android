package com.fossil;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class d83 implements Runnable {
    @DexIgnore
    public /* final */ URL a;
    @DexIgnore
    public /* final */ z73 b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ Map<String, String> d; // = null;
    @DexIgnore
    public /* final */ /* synthetic */ b83 e;

    @DexIgnore
    public d83(b83 b83, String str, URL url, byte[] bArr, Map<String, String> map, z73 z73) {
        this.e = b83;
        w12.b(str);
        w12.a(url);
        w12.a(z73);
        this.a = url;
        this.b = z73;
        this.c = str;
    }

    @DexIgnore
    public final /* synthetic */ void a(int i, Exception exc, byte[] bArr, Map map) {
        this.b.a(this.c, i, exc, bArr, map);
    }

    @DexIgnore
    public final void b(int i, Exception exc, byte[] bArr, Map<String, List<String>> map) {
        this.e.a().a((Runnable) new c83(this, i, exc, bArr, map));
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0070  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007d  */
    public final void run() {
        int i;
        HttpURLConnection httpURLConnection;
        Map map;
        int i2;
        Map map2;
        int responseCode;
        Map headerFields;
        this.e.f();
        try {
            httpURLConnection = this.e.a(this.a);
            try {
                if (this.d != null) {
                    for (Map.Entry next : this.d.entrySet()) {
                        httpURLConnection.addRequestProperty((String) next.getKey(), (String) next.getValue());
                    }
                }
                responseCode = httpURLConnection.getResponseCode();
            } catch (IOException e2) {
                e = e2;
                map = null;
                i = 0;
                if (httpURLConnection != null) {
                }
                b(i, e, (byte[]) null, map);
            } catch (Throwable th) {
                th = th;
                map2 = null;
                i2 = 0;
                if (httpURLConnection != null) {
                }
                b(i2, (Exception) null, (byte[]) null, map2);
                throw th;
            }
            try {
                headerFields = httpURLConnection.getHeaderFields();
            } catch (IOException e3) {
                e = e3;
                i = responseCode;
                map = null;
                if (httpURLConnection != null) {
                }
                b(i, e, (byte[]) null, map);
            } catch (Throwable th2) {
                th = th2;
                i2 = responseCode;
                map2 = null;
                if (httpURLConnection != null) {
                }
                b(i2, (Exception) null, (byte[]) null, map2);
                throw th;
            }
            try {
                byte[] a2 = b83.a(httpURLConnection);
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
                b(responseCode, (Exception) null, a2, headerFields);
            } catch (IOException e4) {
                IOException iOException = e4;
                i = responseCode;
                map = headerFields;
                e = iOException;
                if (httpURLConnection != null) {
                }
                b(i, e, (byte[]) null, map);
            } catch (Throwable th3) {
                Throwable th4 = th3;
                i2 = responseCode;
                map2 = headerFields;
                th = th4;
                if (httpURLConnection != null) {
                }
                b(i2, (Exception) null, (byte[]) null, map2);
                throw th;
            }
        } catch (IOException e5) {
            e = e5;
            map = null;
            httpURLConnection = null;
            i = 0;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            b(i, e, (byte[]) null, map);
        } catch (Throwable th5) {
            th = th5;
            map2 = null;
            httpURLConnection = null;
            i2 = 0;
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            b(i2, (Exception) null, (byte[]) null, map2);
            throw th;
        }
    }
}
