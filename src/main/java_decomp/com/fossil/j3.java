package com.fossil;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.q1;
import com.fossil.x1;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class j3 implements r2 {
    @DexIgnore
    public Toolbar a;
    @DexIgnore
    public int b;
    @DexIgnore
    public View c;
    @DexIgnore
    public View d;
    @DexIgnore
    public Drawable e;
    @DexIgnore
    public Drawable f;
    @DexIgnore
    public Drawable g;
    @DexIgnore
    public boolean h;
    @DexIgnore
    public CharSequence i;
    @DexIgnore
    public CharSequence j;
    @DexIgnore
    public CharSequence k;
    @DexIgnore
    public Window.Callback l;
    @DexIgnore
    public boolean m;
    @DexIgnore
    public g2 n;
    @DexIgnore
    public int o;
    @DexIgnore
    public int p;
    @DexIgnore
    public Drawable q;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class a implements View.OnClickListener {
        @DexIgnore
        public /* final */ k1 a; // = new k1(j3.this.a.getContext(), 0, 16908332, 0, 0, j3.this.i);

        @DexIgnore
        public a() {
        }

        @DexIgnore
        public void onClick(View view) {
            j3 j3Var = j3.this;
            Window.Callback callback = j3Var.l;
            if (callback != null && j3Var.m) {
                callback.onMenuItemSelected(0, this.a);
            }
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public class b extends da {
        @DexIgnore
        public boolean a; // = false;
        @DexIgnore
        public /* final */ /* synthetic */ int b;

        @DexIgnore
        public b(int i) {
            this.b = i;
        }

        @DexIgnore
        public void a(View view) {
            this.a = true;
        }

        @DexIgnore
        public void b(View view) {
            if (!this.a) {
                j3.this.a.setVisibility(this.b);
            }
        }

        @DexIgnore
        public void c(View view) {
            j3.this.a.setVisibility(0);
        }
    }

    @DexIgnore
    public j3(Toolbar toolbar, boolean z) {
        this(toolbar, z, h0.abc_action_bar_up_description, e0.abc_ic_ab_back_material);
    }

    @DexIgnore
    public void a(Drawable drawable) {
        this.f = drawable;
        r();
    }

    @DexIgnore
    public void a(boolean z) {
    }

    @DexIgnore
    public void b(CharSequence charSequence) {
        this.j = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setSubtitle(charSequence);
        }
    }

    @DexIgnore
    public void c(int i2) {
        if (i2 != this.p) {
            this.p = i2;
            if (TextUtils.isEmpty(this.a.getNavigationContentDescription())) {
                d(this.p);
            }
        }
    }

    @DexIgnore
    public void collapseActionView() {
        this.a.c();
    }

    @DexIgnore
    public boolean d() {
        return this.a.m();
    }

    @DexIgnore
    public boolean e() {
        return this.a.l();
    }

    @DexIgnore
    public boolean f() {
        return this.a.r();
    }

    @DexIgnore
    public void g() {
        this.a.d();
    }

    @DexIgnore
    public Context getContext() {
        return this.a.getContext();
    }

    @DexIgnore
    public CharSequence getTitle() {
        return this.a.getTitle();
    }

    @DexIgnore
    public boolean h() {
        return this.a.k();
    }

    @DexIgnore
    public Menu i() {
        return this.a.getMenu();
    }

    @DexIgnore
    public int j() {
        return this.o;
    }

    @DexIgnore
    public ViewGroup k() {
        return this.a;
    }

    @DexIgnore
    public int l() {
        return this.b;
    }

    @DexIgnore
    public void m() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    public void n() {
        Log.i("ToolbarWidgetWrapper", "Progress display unsupported");
    }

    @DexIgnore
    public final int o() {
        if (this.a.getNavigationIcon() == null) {
            return 11;
        }
        this.q = this.a.getNavigationIcon();
        return 15;
    }

    @DexIgnore
    public final void p() {
        if ((this.b & 4) == 0) {
            return;
        }
        if (TextUtils.isEmpty(this.k)) {
            this.a.setNavigationContentDescription(this.p);
        } else {
            this.a.setNavigationContentDescription(this.k);
        }
    }

    @DexIgnore
    public final void q() {
        if ((this.b & 4) != 0) {
            Toolbar toolbar = this.a;
            Drawable drawable = this.g;
            if (drawable == null) {
                drawable = this.q;
            }
            toolbar.setNavigationIcon(drawable);
            return;
        }
        this.a.setNavigationIcon((Drawable) null);
    }

    @DexIgnore
    public final void r() {
        Drawable drawable;
        int i2 = this.b;
        if ((i2 & 2) == 0) {
            drawable = null;
        } else if ((i2 & 1) != 0) {
            drawable = this.f;
            if (drawable == null) {
                drawable = this.e;
            }
        } else {
            drawable = this.e;
        }
        this.a.setLogo(drawable);
    }

    @DexIgnore
    public void setIcon(int i2) {
        setIcon(i2 != 0 ? u0.c(getContext(), i2) : null);
    }

    @DexIgnore
    public void setTitle(CharSequence charSequence) {
        this.h = true;
        c(charSequence);
    }

    @DexIgnore
    public void setVisibility(int i2) {
        this.a.setVisibility(i2);
    }

    @DexIgnore
    public void setWindowCallback(Window.Callback callback) {
        this.l = callback;
    }

    @DexIgnore
    public void setWindowTitle(CharSequence charSequence) {
        if (!this.h) {
            c(charSequence);
        }
    }

    @DexIgnore
    public j3(Toolbar toolbar, boolean z, int i2, int i3) {
        Drawable drawable;
        this.o = 0;
        this.p = 0;
        this.a = toolbar;
        this.i = toolbar.getTitle();
        this.j = toolbar.getSubtitle();
        this.h = this.i != null;
        this.g = toolbar.getNavigationIcon();
        i3 a2 = i3.a(toolbar.getContext(), (AttributeSet) null, j0.ActionBar, a0.actionBarStyle, 0);
        this.q = a2.b(j0.ActionBar_homeAsUpIndicator);
        if (z) {
            CharSequence e2 = a2.e(j0.ActionBar_title);
            if (!TextUtils.isEmpty(e2)) {
                setTitle(e2);
            }
            CharSequence e3 = a2.e(j0.ActionBar_subtitle);
            if (!TextUtils.isEmpty(e3)) {
                b(e3);
            }
            Drawable b2 = a2.b(j0.ActionBar_logo);
            if (b2 != null) {
                a(b2);
            }
            Drawable b3 = a2.b(j0.ActionBar_icon);
            if (b3 != null) {
                setIcon(b3);
            }
            if (this.g == null && (drawable = this.q) != null) {
                b(drawable);
            }
            a(a2.d(j0.ActionBar_displayOptions, 0));
            int g2 = a2.g(j0.ActionBar_customNavigationLayout, 0);
            if (g2 != 0) {
                a(LayoutInflater.from(this.a.getContext()).inflate(g2, this.a, false));
                a(this.b | 16);
            }
            int f2 = a2.f(j0.ActionBar_height, 0);
            if (f2 > 0) {
                ViewGroup.LayoutParams layoutParams = this.a.getLayoutParams();
                layoutParams.height = f2;
                this.a.setLayoutParams(layoutParams);
            }
            int b4 = a2.b(j0.ActionBar_contentInsetStart, -1);
            int b5 = a2.b(j0.ActionBar_contentInsetEnd, -1);
            if (b4 >= 0 || b5 >= 0) {
                this.a.b(Math.max(b4, 0), Math.max(b5, 0));
            }
            int g3 = a2.g(j0.ActionBar_titleTextStyle, 0);
            if (g3 != 0) {
                Toolbar toolbar2 = this.a;
                toolbar2.b(toolbar2.getContext(), g3);
            }
            int g4 = a2.g(j0.ActionBar_subtitleTextStyle, 0);
            if (g4 != 0) {
                Toolbar toolbar3 = this.a;
                toolbar3.a(toolbar3.getContext(), g4);
            }
            int g5 = a2.g(j0.ActionBar_popupTheme, 0);
            if (g5 != 0) {
                this.a.setPopupTheme(g5);
            }
        } else {
            this.b = o();
        }
        a2.a();
        c(i2);
        this.k = this.a.getNavigationContentDescription();
        this.a.setNavigationOnClickListener(new a());
    }

    @DexIgnore
    public void d(int i2) {
        a((CharSequence) i2 == 0 ? null : getContext().getString(i2));
    }

    @DexIgnore
    public void setIcon(Drawable drawable) {
        this.e = drawable;
        r();
    }

    @DexIgnore
    public boolean a() {
        return this.a.n();
    }

    @DexIgnore
    public void a(Menu menu, x1.a aVar) {
        if (this.n == null) {
            this.n = new g2(this.a.getContext());
            this.n.a(f0.action_menu_presenter);
        }
        this.n.a(aVar);
        this.a.a((q1) menu, this.n);
    }

    @DexIgnore
    public void b(int i2) {
        a(i2 != 0 ? u0.c(getContext(), i2) : null);
    }

    @DexIgnore
    public void b() {
        this.m = true;
    }

    @DexIgnore
    public final void c(CharSequence charSequence) {
        this.i = charSequence;
        if ((this.b & 8) != 0) {
            this.a.setTitle(charSequence);
        }
    }

    @DexIgnore
    public void b(boolean z) {
        this.a.setCollapsible(z);
    }

    @DexIgnore
    public void b(Drawable drawable) {
        this.g = drawable;
        q();
    }

    @DexIgnore
    public boolean c() {
        return this.a.b();
    }

    @DexIgnore
    public void a(int i2) {
        View view;
        int i3 = this.b ^ i2;
        this.b = i2;
        if (i3 != 0) {
            if ((i3 & 4) != 0) {
                if ((i2 & 4) != 0) {
                    p();
                }
                q();
            }
            if ((i3 & 3) != 0) {
                r();
            }
            if ((i3 & 8) != 0) {
                if ((i2 & 8) != 0) {
                    this.a.setTitle(this.i);
                    this.a.setSubtitle(this.j);
                } else {
                    this.a.setTitle((CharSequence) null);
                    this.a.setSubtitle((CharSequence) null);
                }
            }
            if ((i3 & 16) != 0 && (view = this.d) != null) {
                if ((i2 & 16) != 0) {
                    this.a.addView(view);
                } else {
                    this.a.removeView(view);
                }
            }
        }
    }

    @DexIgnore
    public void a(b3 b3Var) {
        Toolbar toolbar;
        View view = this.c;
        if (view != null && view.getParent() == (toolbar = this.a)) {
            toolbar.removeView(this.c);
        }
        this.c = b3Var;
        if (b3Var != null && this.o == 2) {
            this.a.addView(this.c, 0);
            Toolbar.LayoutParams layoutParams = (Toolbar.LayoutParams) this.c.getLayoutParams();
            layoutParams.width = -2;
            layoutParams.height = -2;
            layoutParams.a = 8388691;
            b3Var.setAllowCollapse(true);
        }
    }

    @DexIgnore
    public void a(View view) {
        View view2 = this.d;
        if (!(view2 == null || (this.b & 16) == 0)) {
            this.a.removeView(view2);
        }
        this.d = view;
        if (view != null && (this.b & 16) != 0) {
            this.a.addView(this.d);
        }
    }

    @DexIgnore
    public ba a(int i2, long j2) {
        ba a2 = x9.a(this.a);
        a2.a(i2 == 0 ? 1.0f : LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
        a2.a(j2);
        a2.a((ca) new b(i2));
        return a2;
    }

    @DexIgnore
    public void a(CharSequence charSequence) {
        this.k = charSequence;
        p();
    }

    @DexIgnore
    public void a(x1.a aVar, q1.a aVar2) {
        this.a.a(aVar, aVar2);
    }
}
