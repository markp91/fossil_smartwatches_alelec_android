package com.fossil;

import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.LocationSource;
import com.portfolio.platform.data.source.DianaPresetRepository;
import com.portfolio.platform.data.source.UserRepository;
import com.portfolio.platform.data.source.remote.ApiServiceV2;
import com.portfolio.platform.service.watchapp.commute.WatchAppCommuteTimeManager;
import dagger.MembersInjector;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class rq4 implements MembersInjector<pq4> {
    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, PortfolioApp portfolioApp) {
        watchAppCommuteTimeManager.a = portfolioApp;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, ApiServiceV2 apiServiceV2) {
        watchAppCommuteTimeManager.b = apiServiceV2;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, LocationSource locationSource) {
        watchAppCommuteTimeManager.c = locationSource;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, UserRepository userRepository) {
        watchAppCommuteTimeManager.d = userRepository;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, an4 an4) {
        watchAppCommuteTimeManager.e = an4;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, ce ceVar) {
        watchAppCommuteTimeManager.f = ceVar;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, xs4 xs4) {
        watchAppCommuteTimeManager.g = xs4;
    }

    @DexIgnore
    public static void a(WatchAppCommuteTimeManager watchAppCommuteTimeManager, DianaPresetRepository dianaPresetRepository) {
        watchAppCommuteTimeManager.h = dianaPresetRepository;
    }
}
