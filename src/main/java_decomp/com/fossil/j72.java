package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j72 {
    @DexIgnore
    public static /* final */ h72 A; // = h72.f("body_temperature_measurement_location");
    @DexIgnore
    public static /* final */ h72 B; // = h72.f("cervical_mucus_texture");
    @DexIgnore
    public static /* final */ h72 C; // = h72.f("cervical_mucus_amount");
    @DexIgnore
    public static /* final */ h72 D; // = h72.f("cervical_position");
    @DexIgnore
    public static /* final */ h72 E; // = h72.f("cervical_dilation");
    @DexIgnore
    public static /* final */ h72 F; // = h72.f("cervical_firmness");
    @DexIgnore
    public static /* final */ h72 G; // = h72.f("menstrual_flow");
    @DexIgnore
    public static /* final */ h72 H; // = h72.f("ovulation_test_result");
    @DexIgnore
    public static /* final */ h72 a; // = h72.g("blood_pressure_systolic");
    @DexIgnore
    public static /* final */ h72 b; // = h72.g("blood_pressure_systolic_average");
    @DexIgnore
    public static /* final */ h72 c; // = h72.g("blood_pressure_systolic_min");
    @DexIgnore
    public static /* final */ h72 d; // = h72.g("blood_pressure_systolic_max");
    @DexIgnore
    public static /* final */ h72 e; // = h72.g("blood_pressure_diastolic");
    @DexIgnore
    public static /* final */ h72 f; // = h72.g("blood_pressure_diastolic_average");
    @DexIgnore
    public static /* final */ h72 g; // = h72.g("blood_pressure_diastolic_min");
    @DexIgnore
    public static /* final */ h72 h; // = h72.g("blood_pressure_diastolic_max");
    @DexIgnore
    public static /* final */ h72 i; // = h72.f("body_position");
    @DexIgnore
    public static /* final */ h72 j; // = h72.f("blood_pressure_measurement_location");
    @DexIgnore
    public static /* final */ h72 k; // = h72.g("blood_glucose_level");
    @DexIgnore
    public static /* final */ h72 l; // = h72.f("temporal_relation_to_meal");
    @DexIgnore
    public static /* final */ h72 m; // = h72.f("temporal_relation_to_sleep");
    @DexIgnore
    public static /* final */ h72 n; // = h72.f("blood_glucose_specimen_source");
    @DexIgnore
    public static /* final */ h72 o; // = h72.g("oxygen_saturation");
    @DexIgnore
    public static /* final */ h72 p; // = h72.g("oxygen_saturation_average");
    @DexIgnore
    public static /* final */ h72 q; // = h72.g("oxygen_saturation_min");
    @DexIgnore
    public static /* final */ h72 r; // = h72.g("oxygen_saturation_max");
    @DexIgnore
    public static /* final */ h72 s; // = h72.g("supplemental_oxygen_flow_rate");
    @DexIgnore
    public static /* final */ h72 t; // = h72.g("supplemental_oxygen_flow_rate_average");
    @DexIgnore
    public static /* final */ h72 u; // = h72.g("supplemental_oxygen_flow_rate_min");
    @DexIgnore
    public static /* final */ h72 v; // = h72.g("supplemental_oxygen_flow_rate_max");
    @DexIgnore
    public static /* final */ h72 w; // = h72.f("oxygen_therapy_administration_mode");
    @DexIgnore
    public static /* final */ h72 x; // = h72.f("oxygen_saturation_system");
    @DexIgnore
    public static /* final */ h72 y; // = h72.f("oxygen_saturation_measurement_method");
    @DexIgnore
    public static /* final */ h72 z; // = h72.g("body_temperature");
}
