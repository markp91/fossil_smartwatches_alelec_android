package com.fossil;

import java.lang.reflect.Array;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class j04 {
    @DexIgnore
    public /* final */ byte[][] a;
    @DexIgnore
    public /* final */ int b;
    @DexIgnore
    public /* final */ int c;

    @DexIgnore
    public j04(int i, int i2) {
        this.a = (byte[][]) Array.newInstance(byte.class, new int[]{i2, i});
        this.b = i;
        this.c = i2;
    }

    @DexIgnore
    public byte a(int i, int i2) {
        return this.a[i2][i];
    }

    @DexIgnore
    public int b() {
        return this.c;
    }

    @DexIgnore
    public int c() {
        return this.b;
    }

    @DexIgnore
    public String toString() {
        StringBuilder sb = new StringBuilder((this.b * 2 * this.c) + 2);
        for (int i = 0; i < this.c; i++) {
            for (int i2 = 0; i2 < this.b; i2++) {
                byte b2 = this.a[i][i2];
                if (b2 == 0) {
                    sb.append(" 0");
                } else if (b2 != 1) {
                    sb.append("  ");
                } else {
                    sb.append(" 1");
                }
            }
            sb.append(10);
        }
        return sb.toString();
    }

    @DexIgnore
    public byte[][] a() {
        return this.a;
    }

    @DexIgnore
    public void a(int i, int i2, int i3) {
        this.a[i2][i] = (byte) i3;
    }

    @DexIgnore
    public void a(int i, int i2, boolean z) {
        this.a[i2][i] = z ? (byte) 1 : 0;
    }

    @DexIgnore
    public void a(byte b2) {
        for (int i = 0; i < this.c; i++) {
            for (int i2 = 0; i2 < this.b; i2++) {
                this.a[i][i2] = b2;
            }
        }
    }
}
