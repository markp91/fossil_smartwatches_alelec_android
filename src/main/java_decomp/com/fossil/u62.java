package com.fossil;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.fossil.x52;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class u62 extends cb2 implements t62 {
    @DexIgnore
    public u62(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    @DexIgnore
    public final x52 a(x52 x52, String str, int i, x52 x522) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        zza.writeInt(i);
        eb2.a(zza, (IInterface) x522);
        Parcel a = a(2, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    @DexIgnore
    public final x52 b(x52 x52, String str, int i, x52 x522) throws RemoteException {
        Parcel zza = zza();
        eb2.a(zza, (IInterface) x52);
        zza.writeString(str);
        zza.writeInt(i);
        eb2.a(zza, (IInterface) x522);
        Parcel a = a(3, zza);
        x52 a2 = x52.a.a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
