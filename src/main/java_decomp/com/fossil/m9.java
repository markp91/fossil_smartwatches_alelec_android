package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public interface m9 {
    @DexIgnore
    boolean isNestedScrollingEnabled();

    @DexIgnore
    void setNestedScrollingEnabled(boolean z);

    @DexIgnore
    void stopNestedScroll();
}
