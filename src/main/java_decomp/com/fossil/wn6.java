package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class wn6 {
    @DexIgnore
    public static final Object a(xe6<? super cd6> xe6) {
        Object obj;
        af6 context = xe6.getContext();
        a(context);
        xe6<? super cd6> a = ef6.a(xe6);
        if (!(a instanceof vl6)) {
            a = null;
        }
        vl6 vl6 = (vl6) a;
        if (vl6 == null) {
            obj = cd6.a;
        } else if (!vl6.g.b(context)) {
            obj = xl6.a((vl6<? super cd6>) vl6) ? ff6.a() : cd6.a;
        } else {
            vl6.d(cd6.a);
            obj = ff6.a();
        }
        if (obj == ff6.a()) {
            nf6.c(xe6);
        }
        return obj;
    }

    @DexIgnore
    public static final void a(af6 af6) {
        wg6.b(af6, "$this$checkCompletion");
        rm6 rm6 = (rm6) af6.get(rm6.n);
        if (rm6 != null && !rm6.isActive()) {
            throw rm6.k();
        }
    }
}
