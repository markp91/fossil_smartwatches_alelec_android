package com.fossil;

import com.facebook.appevents.UserDataStore;
import com.facebook.places.internal.LocationScannerImpl;
import com.fossil.d5;
import com.fossil.z4;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class w4 implements z4.a {
    @DexIgnore
    public d5 a; // = null;
    @DexIgnore
    public float b; // = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    @DexIgnore
    public boolean c;
    @DexIgnore
    public /* final */ v4 d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public w4(x4 x4Var) {
        this.d = new v4(this, x4Var);
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(d5Var, -1.0f);
            this.d.a(d5Var2, 1.0f);
        } else {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public boolean b() {
        d5 d5Var = this.a;
        return d5Var != null && (d5Var.g == d5.a.UNRESTRICTED || this.b >= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES);
    }

    @DexIgnore
    public w4 c(d5 d5Var, int i) {
        if (i < 0) {
            this.b = (float) (i * -1);
            this.d.a(d5Var, 1.0f);
        } else {
            this.b = (float) i;
            this.d.a(d5Var, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public void clear() {
        this.d.a();
        this.a = null;
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
    }

    @DexIgnore
    public void d() {
        this.a = null;
        this.d.a();
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        this.e = false;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:25:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ce  */
    public String e() {
        String str;
        boolean z;
        String str2;
        float b2;
        int i;
        String str3;
        if (this.a == null) {
            str = "" + "0";
        } else {
            str = "" + this.a;
        }
        String str4 = str + " = ";
        if (this.b != LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            str4 = str4 + this.b;
            z = true;
        } else {
            z = false;
        }
        int i2 = this.d.a;
        for (int i3 = 0; i3 < i2; i3++) {
            d5 a2 = this.d.a(i3);
            if (!(a2 == null || b2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES)) {
                String d5Var = a2.toString();
                if (!z) {
                    if (b2 < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
                        str2 = str2 + "- ";
                    }
                    if (b2 == 1.0f) {
                        str3 = str2 + d5Var;
                    } else {
                        str3 = str2 + b2 + " " + d5Var;
                    }
                    z = true;
                } else if (i > 0) {
                    str2 = str2 + " + ";
                    if (b2 == 1.0f) {
                    }
                    z = true;
                } else {
                    str2 = str2 + " - ";
                }
                b2 = (b2 = this.d.b(i3)) * -1.0f;
                if (b2 == 1.0f) {
                }
                z = true;
            }
        }
        if (z) {
            return str2;
        }
        return str2 + "0.0";
    }

    @DexIgnore
    public d5 getKey() {
        return this.a;
    }

    @DexIgnore
    public String toString() {
        return e();
    }

    @DexIgnore
    public boolean b(d5 d5Var) {
        return this.d.a(d5Var);
    }

    @DexIgnore
    public w4 b(d5 d5Var, int i) {
        this.a = d5Var;
        float f = (float) i;
        d5Var.e = f;
        this.b = f;
        this.e = true;
        return this;
    }

    @DexIgnore
    public d5 c(d5 d5Var) {
        return this.d.a((boolean[]) null, d5Var);
    }

    @DexIgnore
    public void d(d5 d5Var) {
        d5 d5Var2 = this.a;
        if (d5Var2 != null) {
            this.d.a(d5Var2, -1.0f);
            this.a = null;
        }
        float a2 = this.d.a(d5Var, true) * -1.0f;
        this.a = d5Var;
        if (a2 != 1.0f) {
            this.b /= a2;
            this.d.a(a2);
        }
    }

    @DexIgnore
    public w4 a(d5 d5Var, int i) {
        this.d.a(d5Var, (float) i);
        return this;
    }

    @DexIgnore
    public boolean c() {
        return this.a == null && this.b == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES && this.d.a == 0;
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, d5 d5Var3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(d5Var, -1.0f);
            this.d.a(d5Var2, 1.0f);
            this.d.a(d5Var3, 1.0f);
        } else {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
            this.d.a(d5Var3, -1.0f);
        }
        return this;
    }

    @DexIgnore
    public w4 b(d5 d5Var, d5 d5Var2, d5 d5Var3, int i) {
        boolean z = false;
        if (i != 0) {
            if (i < 0) {
                i *= -1;
                z = true;
            }
            this.b = (float) i;
        }
        if (!z) {
            this.d.a(d5Var, -1.0f);
            this.d.a(d5Var2, 1.0f);
            this.d.a(d5Var3, -1.0f);
        } else {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
            this.d.a(d5Var3, 1.0f);
        }
        return this;
    }

    @DexIgnore
    public w4 a(float f, float f2, float f3, d5 d5Var, d5 d5Var2, d5 d5Var3, d5 d5Var4) {
        this.b = LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES;
        if (f2 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES || f == f3) {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
            this.d.a(d5Var4, 1.0f);
            this.d.a(d5Var3, -1.0f);
        } else if (f == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
        } else if (f3 == LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(d5Var3, 1.0f);
            this.d.a(d5Var4, -1.0f);
        } else {
            float f4 = (f / f2) / (f3 / f2);
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
            this.d.a(d5Var4, f4);
            this.d.a(d5Var3, -f4);
        }
        return this;
    }

    @DexIgnore
    public w4 b(d5 d5Var, d5 d5Var2, d5 d5Var3, d5 d5Var4, float f) {
        this.d.a(d5Var3, 0.5f);
        this.d.a(d5Var4, 0.5f);
        this.d.a(d5Var, -0.5f);
        this.d.a(d5Var2, -0.5f);
        this.b = -f;
        return this;
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, int i, float f, d5 d5Var3, d5 d5Var4, int i2) {
        if (d5Var2 == d5Var3) {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var4, 1.0f);
            this.d.a(d5Var2, -2.0f);
            return this;
        }
        if (f == 0.5f) {
            this.d.a(d5Var, 1.0f);
            this.d.a(d5Var2, -1.0f);
            this.d.a(d5Var3, -1.0f);
            this.d.a(d5Var4, 1.0f);
            if (i > 0 || i2 > 0) {
                this.b = (float) ((-i) + i2);
            }
        } else if (f <= LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.d.a(d5Var, -1.0f);
            this.d.a(d5Var2, 1.0f);
            this.b = (float) i;
        } else if (f >= 1.0f) {
            this.d.a(d5Var3, -1.0f);
            this.d.a(d5Var4, 1.0f);
            this.b = (float) i2;
        } else {
            float f2 = 1.0f - f;
            this.d.a(d5Var, f2 * 1.0f);
            this.d.a(d5Var2, f2 * -1.0f);
            this.d.a(d5Var3, -1.0f * f);
            this.d.a(d5Var4, 1.0f * f);
            if (i > 0 || i2 > 0) {
                this.b = (((float) (-i)) * f2) + (((float) i2) * f);
            }
        }
        return this;
    }

    @DexIgnore
    public w4 a(z4 z4Var, int i) {
        this.d.a(z4Var.a(i, "ep"), 1.0f);
        this.d.a(z4Var.a(i, UserDataStore.EMAIL), -1.0f);
        return this;
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, d5 d5Var3, float f) {
        this.d.a(d5Var, -1.0f);
        this.d.a(d5Var2, 1.0f - f);
        this.d.a(d5Var3, f);
        return this;
    }

    @DexIgnore
    public w4 a(d5 d5Var, d5 d5Var2, d5 d5Var3, d5 d5Var4, float f) {
        this.d.a(d5Var, -1.0f);
        this.d.a(d5Var2, 1.0f);
        this.d.a(d5Var3, f);
        this.d.a(d5Var4, -f);
        return this;
    }

    @DexIgnore
    public void a() {
        float f = this.b;
        if (f < LocationScannerImpl.MIN_DISTANCE_BETWEEN_UPDATES) {
            this.b = f * -1.0f;
            this.d.b();
        }
    }

    @DexIgnore
    public boolean a(z4 z4Var) {
        boolean z;
        d5 a2 = this.d.a(z4Var);
        if (a2 == null) {
            z = true;
        } else {
            d(a2);
            z = false;
        }
        if (this.d.a == 0) {
            this.e = true;
        }
        return z;
    }

    @DexIgnore
    public d5 a(z4 z4Var, boolean[] zArr) {
        return this.d.a(zArr, (d5) null);
    }

    @DexIgnore
    public void a(z4.a aVar) {
        if (aVar instanceof w4) {
            w4 w4Var = (w4) aVar;
            this.a = null;
            this.d.a();
            int i = 0;
            while (true) {
                v4 v4Var = w4Var.d;
                if (i < v4Var.a) {
                    this.d.a(v4Var.a(i), w4Var.d.b(i), true);
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    @DexIgnore
    public void a(d5 d5Var) {
        int i = d5Var.d;
        float f = 1.0f;
        if (i != 1) {
            if (i == 2) {
                f = 1000.0f;
            } else if (i == 3) {
                f = 1000000.0f;
            } else if (i == 4) {
                f = 1.0E9f;
            } else if (i == 5) {
                f = 1.0E12f;
            }
        }
        this.d.a(d5Var, f);
    }
}
