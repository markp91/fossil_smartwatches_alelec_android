package com.fossil;

import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.FirmwareData;
import com.misfit.frameworks.buttonservice.model.UserProfile;
import com.portfolio.platform.CoroutineUseCase;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.MFDeviceModel;
import com.portfolio.platform.data.model.Device;
import com.portfolio.platform.data.source.DeviceRepository;
import com.portfolio.platform.ui.device.domain.usecase.UpdateFirmwareUsecase;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fs4 extends m24<b, d, c> {
    @DexIgnore
    public static /* final */ String f;
    @DexIgnore
    public /* final */ DeviceRepository d;
    @DexIgnore
    public /* final */ an4 e;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements CoroutineUseCase.b {
        @DexIgnore
        public /* final */ String a;
        @DexIgnore
        public /* final */ FirmwareData b;

        @DexIgnore
        public b(String str, FirmwareData firmwareData) {
            wg6.b(str, "deviceSerial");
            wg6.b(firmwareData, "firmwareData");
            this.a = str;
            this.b = firmwareData;
        }

        @DexIgnore
        public final String a() {
            return this.a;
        }

        @DexIgnore
        public final FirmwareData b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c implements CoroutineUseCase.a {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements CoroutineUseCase.d {
    }

    /*
    static {
        new a((qg6) null);
        String simpleName = UpdateFirmwareUsecase.class.getSimpleName();
        wg6.a((Object) simpleName, "UpdateFirmwareUsecase::class.java.simpleName");
        f = simpleName;
    }
    */

    @DexIgnore
    public fs4(DeviceRepository deviceRepository, an4 an4) {
        wg6.b(deviceRepository, "mDeviceRepository");
        wg6.b(an4, "mSharedPreferencesManager");
        this.d = deviceRepository;
        this.e = an4;
    }

    @DexIgnore
    public String c() {
        return f;
    }

    @DexIgnore
    public Object a(b bVar, xe6<Object> xe6) {
        FLogger.INSTANCE.getLocal().d(f, "running UseCase");
        if (bVar == null) {
            FLogger.INSTANCE.getLocal().e(f, "Error when update firmware, requestValues is NULL");
            return new c();
        }
        try {
            Device deviceBySerial = this.d.getDeviceBySerial(PortfolioApp.get.instance().e());
            if (deviceBySerial == null) {
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                String str = f;
                local.e(str, "Error when update firmware, no device exists in DB with serial=" + bVar.a());
                return new c();
            }
            FirmwareData b2 = bVar.b();
            String deviceModel = b2.getDeviceModel();
            String sku = deviceBySerial.getSku();
            if (sku == null) {
                sku = "";
            }
            if (!MFDeviceModel.isSame(deviceModel, sku)) {
                FLogger.INSTANCE.getLocal().e(f, "Firmware is not comparable with device model. Cannot OTA.");
                return new c();
            }
            this.e.a(deviceBySerial.getDeviceId(), deviceBySerial.getFirmwareRevision());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = f;
            local2.d(str2, "Start update firmware with version=" + b2.getFirmwareVersion() + ", currentVersion=" + deviceBySerial.getFirmwareRevision());
            PortfolioApp instance = PortfolioApp.get.instance();
            String a2 = bVar.a();
            UserProfile j = PortfolioApp.get.instance().j();
            if (j != null) {
                instance.a(a2, b2, j);
                return new d();
            }
            wg6.a();
            throw null;
        } catch (Exception e2) {
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = f;
            local3.d(str3, "running UseCase failed with exception=" + e2);
            e2.printStackTrace();
            return new c();
        }
    }
}
