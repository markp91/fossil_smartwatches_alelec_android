package com.fossil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class av6 {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a extends ObjectInputStream {
        @DexIgnore
        public static /* final */ Map<String, Class<?>> b; // = new HashMap();
        @DexIgnore
        public /* final */ ClassLoader a;

        /*
        static {
            b.put("byte", Byte.TYPE);
            b.put("short", Short.TYPE);
            b.put("int", Integer.TYPE);
            b.put("long", Long.TYPE);
            b.put("float", Float.TYPE);
            b.put("double", Double.TYPE);
            b.put("boolean", Boolean.TYPE);
            b.put("char", Character.TYPE);
            b.put("void", Void.TYPE);
        }
        */

        @DexIgnore
        public a(InputStream inputStream, ClassLoader classLoader) throws IOException {
            super(inputStream);
            this.a = classLoader;
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
            return r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0025, code lost:
            throw r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0018, code lost:
            return java.lang.Class.forName(r3, false, java.lang.Thread.currentThread().getContextClassLoader());
         */
        /* JADX WARNING: Code restructure failed: missing block: B:7:0x0019, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:8:0x001a, code lost:
            r3 = b.get(r3);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0022, code lost:
            if (r3 != null) goto L_0x0024;
         */
        @DexIgnore
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x000c */
        public Class<?> resolveClass(ObjectStreamClass objectStreamClass) throws IOException, ClassNotFoundException {
            String name = objectStreamClass.getName();
            return Class.forName(name, false, this.a);
        }
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:29:0x004c A[SYNTHETIC, Splitter:B:29:0x004c] */
    public static <T extends Serializable> T a(T t) {
        a aVar = null;
        if (t == null) {
            return null;
        }
        try {
            a aVar2 = new a(new ByteArrayInputStream(b(t)), t.getClass().getClassLoader());
            try {
                T t2 = (Serializable) aVar2.readObject();
                try {
                    aVar2.close();
                    return t2;
                } catch (IOException e) {
                    throw new zu6("IOException on closing cloned object data InputStream.", e);
                }
            } catch (ClassNotFoundException e2) {
                e = e2;
                a aVar3 = aVar2;
                throw new zu6("ClassNotFoundException while reading cloned object data", e);
            } catch (IOException e3) {
                e = e3;
                aVar = aVar2;
                throw new zu6("IOException while reading cloned object data", e);
            } catch (Throwable th) {
                th = th;
                aVar = aVar2;
                if (aVar != null) {
                }
                throw th;
            }
        } catch (ClassNotFoundException e4) {
            e = e4;
            throw new zu6("ClassNotFoundException while reading cloned object data", e);
        } catch (IOException e5) {
            e = e5;
            throw new zu6("IOException while reading cloned object data", e);
        } catch (Throwable th2) {
            th = th2;
            if (aVar != null) {
                try {
                    aVar.close();
                } catch (IOException e6) {
                    throw new zu6("IOException on closing cloned object data InputStream.", e6);
                }
            }
            throw th;
        }
    }

    @DexIgnore
    public static byte[] b(Serializable serializable) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(512);
        a(serializable, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0020 A[SYNTHETIC, Splitter:B:19:0x0020] */
    public static void a(Serializable serializable, OutputStream outputStream) {
        if (outputStream != null) {
            ObjectOutputStream objectOutputStream = null;
            try {
                ObjectOutputStream objectOutputStream2 = new ObjectOutputStream(outputStream);
                try {
                    objectOutputStream2.writeObject(serializable);
                    try {
                        objectOutputStream2.close();
                    } catch (IOException unused) {
                    }
                } catch (IOException e) {
                    e = e;
                    objectOutputStream = objectOutputStream2;
                    try {
                        throw new zu6((Throwable) e);
                    } catch (Throwable th) {
                        th = th;
                        if (objectOutputStream != null) {
                            try {
                                objectOutputStream.close();
                            } catch (IOException unused2) {
                            }
                        }
                        throw th;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    objectOutputStream = objectOutputStream2;
                    if (objectOutputStream != null) {
                    }
                    throw th;
                }
            } catch (IOException e2) {
                e = e2;
                throw new zu6((Throwable) e);
            }
        } else {
            throw new IllegalArgumentException("The OutputStream must not be null");
        }
    }
}
