package com.fossil;

import com.zendesk.sdk.network.impl.HelpCenterCachingInterceptor;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class cq6 {
    @DexIgnore
    public /* final */ boolean a;
    @DexIgnore
    public /* final */ boolean b;
    @DexIgnore
    public /* final */ int c;
    @DexIgnore
    public /* final */ int d;
    @DexIgnore
    public /* final */ boolean e;
    @DexIgnore
    public /* final */ boolean f;
    @DexIgnore
    public /* final */ boolean g;
    @DexIgnore
    public /* final */ int h;
    @DexIgnore
    public /* final */ int i;
    @DexIgnore
    public /* final */ boolean j;
    @DexIgnore
    public /* final */ boolean k;
    @DexIgnore
    public /* final */ boolean l;
    @DexIgnore
    public String m;

    /*
    static {
        a aVar = new a();
        aVar.b();
        aVar.a();
        a aVar2 = new a();
        aVar2.c();
        aVar2.a(Integer.MAX_VALUE, TimeUnit.SECONDS);
        aVar2.a();
    }
    */

    @DexIgnore
    public cq6(boolean z, boolean z2, int i2, int i3, boolean z3, boolean z4, boolean z5, int i4, int i5, boolean z6, boolean z7, boolean z8, String str) {
        this.a = z;
        this.b = z2;
        this.c = i2;
        this.d = i3;
        this.e = z3;
        this.f = z4;
        this.g = z5;
        this.h = i4;
        this.i = i5;
        this.j = z6;
        this.k = z7;
        this.l = z8;
        this.m = str;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0041  */
    public static cq6 a(sq6 sq6) {
        int i2;
        int i3;
        String str;
        sq6 sq62 = sq6;
        int b2 = sq6.b();
        int i4 = 0;
        boolean z = true;
        String str2 = null;
        boolean z2 = false;
        boolean z3 = false;
        int i5 = -1;
        int i6 = -1;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        int i7 = -1;
        int i8 = -1;
        boolean z7 = false;
        boolean z8 = false;
        boolean z9 = false;
        while (i4 < b2) {
            String a2 = sq62.a(i4);
            String b3 = sq62.b(i4);
            if (a2.equalsIgnoreCase(HelpCenterCachingInterceptor.REGULAR_CACHING_HEADER)) {
                if (str2 == null) {
                    str2 = b3;
                    for (i2 = 0; i2 < b3.length(); i2 = i3) {
                        int a3 = yr6.a(b3, i2, "=,;");
                        String trim = b3.substring(i2, a3).trim();
                        if (a3 == b3.length() || b3.charAt(a3) == ',' || b3.charAt(a3) == ';') {
                            i3 = a3 + 1;
                            str = null;
                        } else {
                            int b4 = yr6.b(b3, a3 + 1);
                            if (b4 >= b3.length() || b3.charAt(b4) != '\"') {
                                i3 = yr6.a(b3, b4, ",;");
                                str = b3.substring(b4, i3).trim();
                            } else {
                                int i9 = b4 + 1;
                                int a4 = yr6.a(b3, i9, "\"");
                                str = b3.substring(i9, a4);
                                i3 = a4 + 1;
                            }
                        }
                        if ("no-cache".equalsIgnoreCase(trim)) {
                            z2 = true;
                        } else if ("no-store".equalsIgnoreCase(trim)) {
                            z3 = true;
                        } else if ("max-age".equalsIgnoreCase(trim)) {
                            i5 = yr6.a(str, -1);
                        } else if ("s-maxage".equalsIgnoreCase(trim)) {
                            i6 = yr6.a(str, -1);
                        } else if ("private".equalsIgnoreCase(trim)) {
                            z4 = true;
                        } else if ("public".equalsIgnoreCase(trim)) {
                            z5 = true;
                        } else if ("must-revalidate".equalsIgnoreCase(trim)) {
                            z6 = true;
                        } else if ("max-stale".equalsIgnoreCase(trim)) {
                            i7 = yr6.a(str, Integer.MAX_VALUE);
                        } else if ("min-fresh".equalsIgnoreCase(trim)) {
                            i8 = yr6.a(str, -1);
                        } else if ("only-if-cached".equalsIgnoreCase(trim)) {
                            z7 = true;
                        } else if ("no-transform".equalsIgnoreCase(trim)) {
                            z8 = true;
                        } else if ("immutable".equalsIgnoreCase(trim)) {
                            z9 = true;
                        }
                        sq6 sq63 = sq6;
                    }
                    i4++;
                    sq62 = sq6;
                }
            } else if (!a2.equalsIgnoreCase("Pragma")) {
                i4++;
                sq62 = sq6;
            }
            z = false;
            while (i2 < b3.length()) {
            }
            i4++;
            sq62 = sq6;
        }
        return new cq6(z2, z3, i5, i6, z4, z5, z6, i7, i8, z7, z8, z9, !z ? null : str2);
    }

    @DexIgnore
    public boolean b() {
        return this.e;
    }

    @DexIgnore
    public boolean c() {
        return this.f;
    }

    @DexIgnore
    public int d() {
        return this.c;
    }

    @DexIgnore
    public int e() {
        return this.h;
    }

    @DexIgnore
    public int f() {
        return this.i;
    }

    @DexIgnore
    public boolean g() {
        return this.g;
    }

    @DexIgnore
    public boolean h() {
        return this.a;
    }

    @DexIgnore
    public boolean i() {
        return this.b;
    }

    @DexIgnore
    public boolean j() {
        return this.j;
    }

    @DexIgnore
    public String toString() {
        String str = this.m;
        if (str != null) {
            return str;
        }
        String a2 = a();
        this.m = a2;
        return a2;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public boolean a;
        @DexIgnore
        public boolean b;
        @DexIgnore
        public int c; // = -1;
        @DexIgnore
        public int d; // = -1;
        @DexIgnore
        public int e; // = -1;
        @DexIgnore
        public boolean f;
        @DexIgnore
        public boolean g;
        @DexIgnore
        public boolean h;

        @DexIgnore
        public a a(int i, TimeUnit timeUnit) {
            if (i >= 0) {
                long seconds = timeUnit.toSeconds((long) i);
                this.d = seconds > 2147483647L ? Integer.MAX_VALUE : (int) seconds;
                return this;
            }
            throw new IllegalArgumentException("maxStale < 0: " + i);
        }

        @DexIgnore
        public a b() {
            this.a = true;
            return this;
        }

        @DexIgnore
        public a c() {
            this.f = true;
            return this;
        }

        @DexIgnore
        public cq6 a() {
            return new cq6(this);
        }
    }

    @DexIgnore
    public cq6(a aVar) {
        this.a = aVar.a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = -1;
        this.e = false;
        this.f = false;
        this.g = false;
        this.h = aVar.d;
        this.i = aVar.e;
        this.j = aVar.f;
        this.k = aVar.g;
        this.l = aVar.h;
    }

    @DexIgnore
    public final String a() {
        StringBuilder sb = new StringBuilder();
        if (this.a) {
            sb.append("no-cache, ");
        }
        if (this.b) {
            sb.append("no-store, ");
        }
        if (this.c != -1) {
            sb.append("max-age=");
            sb.append(this.c);
            sb.append(", ");
        }
        if (this.d != -1) {
            sb.append("s-maxage=");
            sb.append(this.d);
            sb.append(", ");
        }
        if (this.e) {
            sb.append("private, ");
        }
        if (this.f) {
            sb.append("public, ");
        }
        if (this.g) {
            sb.append("must-revalidate, ");
        }
        if (this.h != -1) {
            sb.append("max-stale=");
            sb.append(this.h);
            sb.append(", ");
        }
        if (this.i != -1) {
            sb.append("min-fresh=");
            sb.append(this.i);
            sb.append(", ");
        }
        if (this.j) {
            sb.append("only-if-cached, ");
        }
        if (this.k) {
            sb.append("no-transform, ");
        }
        if (this.l) {
            sb.append("immutable, ");
        }
        if (sb.length() == 0) {
            return "";
        }
        sb.delete(sb.length() - 2, sb.length());
        return sb.toString();
    }
}
