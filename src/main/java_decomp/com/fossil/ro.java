package com.fossil;

import java.util.concurrent.Executor;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public enum ro implements Executor {
    INSTANCE;

    @DexIgnore
    public void execute(Runnable runnable) {
        runnable.run();
    }

    @DexIgnore
    public String toString() {
        return "DirectExecutor";
    }
}
