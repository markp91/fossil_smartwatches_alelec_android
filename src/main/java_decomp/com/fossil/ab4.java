package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import androidx.databinding.ViewDataBinding;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class ab4 extends za4 {
    @DexIgnore
    public static /* final */ ViewDataBinding.j X; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray Y; // = new SparseIntArray();
    @DexIgnore
    public long W;

    /*
    static {
        Y.put(2131362630, 1);
        Y.put(2131362611, 2);
        Y.put(2131362084, 3);
        Y.put(2131362782, 4);
        Y.put(2131363224, 5);
        Y.put(2131363223, 6);
        Y.put(2131362970, 7);
        Y.put(2131362061, 8);
        Y.put(2131363260, 9);
        Y.put(2131362992, 10);
        Y.put(2131361984, 11);
        Y.put(2131361813, 12);
        Y.put(2131362099, 13);
        Y.put(2131362089, 14);
        Y.put(2131362858, 15);
        Y.put(2131362857, 16);
        Y.put(2131362859, 17);
        Y.put(2131362860, 18);
        Y.put(2131362054, 19);
        Y.put(2131363261, 20);
        Y.put(2131362324, 21);
        Y.put(2131362233, 22);
        Y.put(2131362053, 23);
        Y.put(2131363220, 24);
        Y.put(2131362754, 25);
        Y.put(2131362047, 26);
        Y.put(2131362387, 27);
        Y.put(2131362328, 28);
        Y.put(2131361964, 29);
        Y.put(2131362255, 30);
        Y.put(2131362254, 31);
        Y.put(2131362256, 32);
        Y.put(2131362258, 33);
        Y.put(2131362259, 34);
        Y.put(2131362257, 35);
        Y.put(2131363322, 36);
        Y.put(2131362891, 37);
        Y.put(2131362036, 38);
        Y.put(2131363218, 39);
        Y.put(2131361947, 40);
        Y.put(2131361939, 41);
    }
    */

    @DexIgnore
    public ab4(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 42, X, Y));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.W = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.W != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.W = 1;
        }
        g();
    }

    @DexIgnore
    public ab4(jb jbVar, View view, Object[] objArr) {
        super(jbVar, view, 0, objArr[12], objArr[41], objArr[40], objArr[29], objArr[11], objArr[38], objArr[26], objArr[23], objArr[19], objArr[8], objArr[3], objArr[14], objArr[13], objArr[22], objArr[31], objArr[30], objArr[32], objArr[35], objArr[33], objArr[34], objArr[21], objArr[28], objArr[27], objArr[2], objArr[1], objArr[25], objArr[4], objArr[0], objArr[16], objArr[15], objArr[17], objArr[18], objArr[37], objArr[7], objArr[10], objArr[39], objArr[24], objArr[6], objArr[5], objArr[9], objArr[20], objArr[36]);
        this.W = -1;
        this.L.setTag((Object) null);
        a(view);
        f();
    }
}
