package com.fossil;

import java.util.HashMap;
import java.util.Map;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class g20 {
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ Map<String, Object> b; // = new HashMap();

    @DexIgnore
    public g20(String str) {
        this.a = str;
    }

    @DexIgnore
    public g20 a(String str, String str2) {
        this.b.put(str, str2);
        return this;
    }

    @DexIgnore
    public j10 a() {
        j10 j10 = new j10(this.a);
        for (String next : this.b.keySet()) {
            Object obj = this.b.get(next);
            if (obj instanceof String) {
                j10.a(next, (String) obj);
            } else if (obj instanceof Number) {
                j10.a(next, (Number) obj);
            }
        }
        return j10;
    }
}
