package com.fossil;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicBoolean;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class b30 implements Thread.UncaughtExceptionHandler {
    @DexIgnore
    public /* final */ a a;
    @DexIgnore
    public /* final */ b b;
    @DexIgnore
    public /* final */ boolean c;
    @DexIgnore
    public /* final */ Thread.UncaughtExceptionHandler d;
    @DexIgnore
    public /* final */ AtomicBoolean e; // = new AtomicBoolean(false);

    @DexIgnore
    public interface a {
        @DexIgnore
        void a(b bVar, Thread thread, Throwable th, boolean z);
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        yb6 a();
    }

    @DexIgnore
    public b30(a aVar, b bVar, boolean z, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.a = aVar;
        this.b = bVar;
        this.c = z;
        this.d = uncaughtExceptionHandler;
    }

    @DexIgnore
    public boolean a() {
        return this.e.get();
    }

    @DexIgnore
    public void uncaughtException(Thread thread, Throwable th) {
        this.e.set(true);
        try {
            this.a.a(this.b, thread, th, this.c);
        } catch (Exception e2) {
            c86.g().e("CrashlyticsCore", "An error occurred in the uncaught exception handler", e2);
        } catch (Throwable th2) {
            c86.g().d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
            this.d.uncaughtException(thread, th);
            this.e.set(false);
            throw th2;
        }
        c86.g().d("CrashlyticsCore", "Crashlytics completed exception processing. Invoking default exception handler.");
        this.d.uncaughtException(thread, th);
        this.e.set(false);
    }
}
