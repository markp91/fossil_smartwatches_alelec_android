package com.fossil;

import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Iterator;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public abstract class yl2 implements Serializable, Iterable<Byte> {
    @DexIgnore
    public static /* final */ em2 a; // = (vl2.a() ? new lm2((bm2) null) : new cm2((bm2) null));
    @DexIgnore
    public static /* final */ yl2 zza; // = new im2(hn2.b);
    @DexIgnore
    public int zzc; // = 0;

    /*
    static {
        new am2();
    }
    */

    @DexIgnore
    public static int a(byte b) {
        return b & 255;
    }

    @DexIgnore
    public static yl2 zza(byte[] bArr, int i, int i2) {
        zzb(i, i + i2, bArr.length);
        return new im2(a.a(bArr, i, i2));
    }

    @DexIgnore
    public static gm2 zzc(int i) {
        return new gm2(i, (bm2) null);
    }

    @DexIgnore
    public abstract boolean equals(Object obj);

    @DexIgnore
    public final int hashCode() {
        int i = this.zzc;
        if (i == 0) {
            int zza2 = zza();
            i = zza(zza2, 0, zza2);
            if (i == 0) {
                i = 1;
            }
            this.zzc = i;
        }
        return i;
    }

    @DexIgnore
    public /* synthetic */ Iterator iterator() {
        return new bm2(this);
    }

    @DexIgnore
    public final String toString() {
        return String.format("<ByteString@%s size=%d>", new Object[]{Integer.toHexString(System.identityHashCode(this)), Integer.valueOf(zza())});
    }

    @DexIgnore
    public abstract byte zza(int i);

    @DexIgnore
    public abstract int zza();

    @DexIgnore
    public abstract int zza(int i, int i2, int i3);

    @DexIgnore
    public abstract yl2 zza(int i, int i2);

    @DexIgnore
    public abstract String zza(Charset charset);

    @DexIgnore
    public abstract void zza(zl2 zl2) throws IOException;

    @DexIgnore
    public abstract byte zzb(int i);

    @DexIgnore
    public final String zzb() {
        return zza() == 0 ? "" : zza(hn2.a);
    }

    @DexIgnore
    public abstract boolean zzc();

    @DexIgnore
    public final int zzd() {
        return this.zzc;
    }

    @DexIgnore
    public static yl2 zza(byte[] bArr) {
        return new im2(bArr);
    }

    @DexIgnore
    public static int zzb(int i, int i2, int i3) {
        int i4 = i2 - i;
        if ((i | i2 | i4 | (i3 - i2)) >= 0) {
            return i4;
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(32);
            sb.append("Beginning index: ");
            sb.append(i);
            sb.append(" < 0");
            throw new IndexOutOfBoundsException(sb.toString());
        } else if (i2 < i) {
            StringBuilder sb2 = new StringBuilder(66);
            sb2.append("Beginning index larger than ending index: ");
            sb2.append(i);
            sb2.append(", ");
            sb2.append(i2);
            throw new IndexOutOfBoundsException(sb2.toString());
        } else {
            StringBuilder sb3 = new StringBuilder(37);
            sb3.append("End index: ");
            sb3.append(i2);
            sb3.append(" >= ");
            sb3.append(i3);
            throw new IndexOutOfBoundsException(sb3.toString());
        }
    }

    @DexIgnore
    public static yl2 zza(String str) {
        return new im2(str.getBytes(hn2.a));
    }
}
