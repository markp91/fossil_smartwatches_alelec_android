package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class jr0 extends ok0 {
    @DexIgnore
    public /* final */ li1 j; // = li1.VERY_HIGH;
    @DexIgnore
    public h91 k; // = h91.DISCONNECTED;
    @DexIgnore
    public /* final */ boolean l;

    @DexIgnore
    public jr0(boolean z, at0 at0) {
        super(hm0.CONNECT, at0);
        this.l = z;
    }

    @DexIgnore
    public void a(ue1 ue1) {
        ue1.a(this.l);
    }

    @DexIgnore
    public li1 b() {
        return this.j;
    }

    @DexIgnore
    public hn1<p51> c() {
        return this.i.e;
    }

    @DexIgnore
    public void a(p51 p51) {
        ch0 ch0;
        c(p51);
        t31 t31 = p51.a;
        if (t31.a != x11.SUCCESS) {
            ch0 a = ch0.d.a(t31);
            ch0 = ch0.a(this.d, (hm0) null, a.b, a.c, 1);
        } else if (this.k == h91.CONNECTED) {
            ch0 = ch0.a(this.d, (hm0) null, lf0.SUCCESS, (t31) null, 5);
        } else {
            ch0 = ch0.a(this.d, (hm0) null, lf0.UNEXPECTED_RESULT, (t31) null, 5);
        }
        this.d = ch0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r2 = ((com.fossil.kr0) r2).b;
     */
    @DexIgnore
    public boolean b(p51 p51) {
        h91 h91;
        return (p51 instanceof kr0) && (h91 == h91.CONNECTED || h91 == h91.DISCONNECTED);
    }

    @DexIgnore
    public void c(p51 p51) {
        this.k = ((kr0) p51).b;
    }
}
