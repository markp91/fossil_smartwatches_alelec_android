package com.fossil;

import java.util.Map;
import java.util.Set;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class uz1 {
    @DexIgnore
    public /* final */ p4<lw1<?>, gv1> a; // = new p4<>();
    @DexIgnore
    public /* final */ p4<lw1<?>, String> b; // = new p4<>();
    @DexIgnore
    public /* final */ rc3<Map<lw1<?>, String>> c; // = new rc3<>();
    @DexIgnore
    public int d;
    @DexIgnore
    public boolean e; // = false;

    @DexIgnore
    public uz1(Iterable<? extends xv1<?>> iterable) {
        for (xv1 a2 : iterable) {
            this.a.put(a2.a(), null);
        }
        this.d = this.a.keySet().size();
    }

    @DexIgnore
    public final qc3<Map<lw1<?>, String>> a() {
        return this.c.a();
    }

    @DexIgnore
    public final Set<lw1<?>> b() {
        return this.a.keySet();
    }

    @DexIgnore
    public final void a(lw1<?> lw1, gv1 gv1, String str) {
        this.a.put(lw1, gv1);
        this.b.put(lw1, str);
        this.d--;
        if (!gv1.E()) {
            this.e = true;
        }
        if (this.d != 0) {
            return;
        }
        if (this.e) {
            this.c.a((Exception) new tv1(this.a));
            return;
        }
        this.c.a(this.b);
    }
}
