package com.fossil;

import com.portfolio.platform.data.model.Style;
import com.portfolio.platform.data.source.ThemeRepository;
import com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel;
import java.util.ArrayList;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
@lf6(c = "com.portfolio.platform.uirenew.home.profile.theme.ThemesViewModel$onThemeChange$1$1", f = "ThemesViewModel.kt", l = {51}, m = "invokeSuspend")
public final class nm5$e$a extends sf6 implements ig6<il6, xe6<? super ArrayList<Style>>, Object> {
    @DexIgnore
    public Object L$0;
    @DexIgnore
    public int label;
    @DexIgnore
    public il6 p$;
    @DexIgnore
    public /* final */ /* synthetic */ ThemesViewModel.e this$0;

    @DexIgnore
    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public nm5$e$a(ThemesViewModel.e eVar, xe6 xe6) {
        super(2, xe6);
        this.this$0 = eVar;
    }

    @DexIgnore
    public final xe6<cd6> create(Object obj, xe6<?> xe6) {
        wg6.b(xe6, "completion");
        nm5$e$a nm5_e_a = new nm5$e$a(this.this$0, xe6);
        nm5_e_a.p$ = (il6) obj;
        return nm5_e_a;
    }

    @DexIgnore
    public final Object invoke(Object obj, Object obj2) {
        return ((nm5$e$a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
    }

    @DexIgnore
    public final Object invokeSuspend(Object obj) {
        Object a = ff6.a();
        int i = this.label;
        if (i == 0) {
            nc6.a(obj);
            il6 il6 = this.p$;
            ThemeRepository e = this.this$0.this$0.g;
            String str = this.this$0.$themeId;
            this.L$0 = il6;
            this.label = 1;
            obj = e.getListStyleById(str, this);
            if (obj == a) {
                return a;
            }
        } else if (i == 1) {
            il6 il62 = (il6) this.L$0;
            nc6.a(obj);
        } else {
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
        ArrayList arrayList = (ArrayList) obj;
        return arrayList != null ? arrayList : new ArrayList();
    }
}
