package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.fossil.u12;
import com.fossil.wearables.fsl.fitness.SampleRaw;
import java.util.concurrent.TimeUnit;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class k72 extends e22 {
    @DexIgnore
    public static /* final */ Parcelable.Creator<k72> CREATOR; // = new n72();
    @DexIgnore
    public /* final */ long a;
    @DexIgnore
    public /* final */ long b;
    @DexIgnore
    public /* final */ String c;
    @DexIgnore
    public /* final */ String d;
    @DexIgnore
    public /* final */ String e;
    @DexIgnore
    public /* final */ int f;
    @DexIgnore
    public /* final */ t72 g;
    @DexIgnore
    public /* final */ Long h;

    @DexIgnore
    public k72(long j, long j2, String str, String str2, String str3, int i, t72 t72, Long l) {
        this.a = j;
        this.b = j2;
        this.c = str;
        this.d = str2;
        this.e = str3;
        this.f = i;
        this.g = t72;
        this.h = l;
    }

    @DexIgnore
    public String B() {
        return this.d;
    }

    @DexIgnore
    public String C() {
        return this.c;
    }

    @DexIgnore
    public long a(TimeUnit timeUnit) {
        return timeUnit.convert(this.b, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public long b(TimeUnit timeUnit) {
        return timeUnit.convert(this.a, TimeUnit.MILLISECONDS);
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof k72)) {
            return false;
        }
        k72 k72 = (k72) obj;
        return this.a == k72.a && this.b == k72.b && u12.a(this.c, k72.c) && u12.a(this.d, k72.d) && u12.a(this.e, k72.e) && u12.a(this.g, k72.g) && this.f == k72.f;
    }

    @DexIgnore
    public int hashCode() {
        return u12.a(Long.valueOf(this.a), Long.valueOf(this.b), this.d);
    }

    @DexIgnore
    public String p() {
        return this.e;
    }

    @DexIgnore
    public String toString() {
        u12.a a2 = u12.a((Object) this);
        a2.a(SampleRaw.COLUMN_START_TIME, Long.valueOf(this.a));
        a2.a(SampleRaw.COLUMN_END_TIME, Long.valueOf(this.b));
        a2.a("name", this.c);
        a2.a("identifier", this.d);
        a2.a("description", this.e);
        a2.a("activity", Integer.valueOf(this.f));
        a2.a("application", this.g);
        return a2.toString();
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        int a2 = g22.a(parcel);
        g22.a(parcel, 1, this.a);
        g22.a(parcel, 2, this.b);
        g22.a(parcel, 3, C(), false);
        g22.a(parcel, 4, B(), false);
        g22.a(parcel, 5, p(), false);
        g22.a(parcel, 7, this.f);
        g22.a(parcel, 8, (Parcelable) this.g, i, false);
        g22.a(parcel, 9, this.h, false);
        g22.a(parcel, a2);
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static class a {
        @DexIgnore
        public long a; // = 0;
        @DexIgnore
        public long b; // = 0;
        @DexIgnore
        public String c; // = null;
        @DexIgnore
        public String d; // = null;
        @DexIgnore
        public String e; // = "";
        @DexIgnore
        public int f; // = 4;
        @DexIgnore
        public Long g;

        @DexIgnore
        public a a(long j, TimeUnit timeUnit) {
            w12.b(j >= 0, "End time should be positive.");
            this.b = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a b(long j, TimeUnit timeUnit) {
            w12.b(j > 0, "Start time should be positive.");
            this.a = timeUnit.toMillis(j);
            return this;
        }

        @DexIgnore
        public a c(String str) {
            w12.a(str != null && TextUtils.getTrimmedLength(str) > 0);
            this.d = str;
            return this;
        }

        @DexIgnore
        public a d(String str) {
            w12.a(str.length() <= 100, "Session name cannot exceed %d characters", 100);
            this.c = str;
            return this;
        }

        @DexIgnore
        public a a(String str) {
            this.f = pe2.a(str);
            return this;
        }

        @DexIgnore
        public a b(String str) {
            w12.a(str.length() <= 1000, "Session description cannot exceed %d characters", 1000);
            this.e = str;
            return this;
        }

        @DexIgnore
        public k72 a() {
            boolean z = true;
            w12.b(this.a > 0, "Start time should be specified.");
            long j = this.b;
            if (j != 0 && j <= this.a) {
                z = false;
            }
            w12.b(z, "End time should be later than start time.");
            if (this.d == null) {
                String str = this.c;
                if (str == null) {
                    str = "";
                }
                long j2 = this.a;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 20);
                sb.append(str);
                sb.append(j2);
                this.d = sb.toString();
            }
            return new k72(this);
        }
    }

    @DexIgnore
    public k72(a aVar) {
        this(aVar.a, aVar.b, aVar.c, aVar.d, aVar.e, aVar.f, (t72) null, aVar.g);
    }
}
