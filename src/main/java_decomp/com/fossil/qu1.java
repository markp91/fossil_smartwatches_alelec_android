package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.SignInConfiguration;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class qu1 implements Parcelable.Creator<SignInConfiguration> {
    @DexIgnore
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = f22.b(parcel);
        String str = null;
        GoogleSignInOptions googleSignInOptions = null;
        while (parcel.dataPosition() < b) {
            int a = f22.a(parcel);
            int a2 = f22.a(a);
            if (a2 == 2) {
                str = f22.e(parcel, a);
            } else if (a2 != 5) {
                f22.v(parcel, a);
            } else {
                googleSignInOptions = (GoogleSignInOptions) f22.a(parcel, a, GoogleSignInOptions.CREATOR);
            }
        }
        f22.h(parcel, b);
        return new SignInConfiguration(str, googleSignInOptions);
    }

    @DexIgnore
    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInConfiguration[i];
    }
}
