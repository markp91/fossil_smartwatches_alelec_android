package com.fossil;

import java.nio.ByteBuffer;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fn0 {
    @DexIgnore
    public /* synthetic */ fn0(qg6 qg6) {
    }

    @DexIgnore
    public final byte[] a(HandMovingConfig[] handMovingConfigArr) {
        ByteBuffer allocate = ByteBuffer.allocate(handMovingConfigArr.length * 5);
        for (HandMovingConfig b : handMovingConfigArr) {
            allocate.put(b.b());
        }
        byte[] array = allocate.array();
        wg6.a(array, "array.array()");
        return array;
    }
}
