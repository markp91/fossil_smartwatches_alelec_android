package com.fossil;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public class x8<T> extends w8<T> {
    @DexIgnore
    public /* final */ Object c; // = new Object();

    @DexIgnore
    public x8(int i) {
        super(i);
    }

    @DexIgnore
    public T a() {
        T a;
        synchronized (this.c) {
            a = super.a();
        }
        return a;
    }

    @DexIgnore
    public boolean a(T t) {
        boolean a;
        synchronized (this.c) {
            a = super.a(t);
        }
        return a;
    }
}
