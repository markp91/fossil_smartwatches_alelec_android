package com.fossil;

import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import com.fossil.fs;
import com.fossil.jv;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class fj4 implements jv<gj4, InputStream> {

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class a implements fs<InputStream> {
        @DexIgnore
        public volatile boolean a;
        @DexIgnore
        public /* final */ gj4 b;

        @DexIgnore
        public a(fj4 fj4, gj4 gj4) {
            wg6.b(gj4, "mAppIconModel");
            this.b = gj4;
        }

        @DexIgnore
        public void a() {
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r4v4, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public void a(br brVar, fs.a<? super InputStream> aVar) {
            wg6.b(brVar, "priority");
            wg6.b(aVar, Constants.CALLBACK);
            try {
                Drawable applicationIcon = PortfolioApp.get.instance().getPackageManager().getApplicationIcon(this.b.a().getIdentifier());
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                Bitmap a2 = cx5.a(applicationIcon);
                if (a2 != null) {
                    a2.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                }
                aVar.a(this.a ? null : new ByteArrayInputStream(byteArrayOutputStream.toByteArray()));
            } catch (PackageManager.NameNotFoundException e) {
                aVar.a(e);
            }
        }

        @DexIgnore
        public pr b() {
            return pr.LOCAL;
        }

        @DexIgnore
        public void cancel() {
            this.a = true;
        }

        @DexIgnore
        public Class<InputStream> getDataClass() {
            return InputStream.class;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b implements kv<gj4, InputStream> {
        @DexIgnore
        public fj4 a(nv nvVar) {
            wg6.b(nvVar, "multiFactory");
            return new fj4();
        }
    }

    @DexIgnore
    public boolean a(gj4 gj4) {
        wg6.b(gj4, "appIconModel");
        return true;
    }

    @DexIgnore
    public jv.a<InputStream> a(gj4 gj4, int i, int i2, xr xrVar) {
        wg6.b(gj4, "appIconModel");
        wg6.b(xrVar, "options");
        return new jv.a<>(gj4, new a(this, gj4));
    }
}
