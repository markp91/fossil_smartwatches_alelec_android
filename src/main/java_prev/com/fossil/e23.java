package com.fossil;

import android.content.Context;
import android.os.Build;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.buttonservice.utils.BluetoothUtils;
import com.misfit.frameworks.buttonservice.utils.LocationUtils;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.diana.preset.DianaPreset;
import com.portfolio.platform.data.model.diana.preset.DianaPresetComplicationSetting;
import com.portfolio.platform.data.model.diana.preset.DianaPresetWatchAppSetting;
import com.portfolio.platform.data.model.room.microapp.HybridPreset;
import com.portfolio.platform.data.model.room.microapp.HybridPresetAppSetting;
import com.portfolio.platform.enums.PermissionCodes;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.uirenew.permission.PermissionActivity;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import kotlin.Triple;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class e23 {
    @DexIgnore
    public static /* final */ boolean a; // = (Build.VERSION.SDK_INT >= 29);
    @DexIgnore
    public static /* final */ HashMap<String, ArrayList<Integer>> b;
    @DexIgnore
    public static h23 c;
    @DexIgnore
    public static /* final */ e23 d; // = new e23();

    /*
    static {
        HashMap<String, ArrayList<Integer>> hashMap = new HashMap<>();
        hashMap.put("PAIR_DEVICE", ap4.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("SYNC_DEVICE", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("NOTIFICATION_CONTACTS", ap4.a((T[]) new Integer[]{4, 100, 101}));
        hashMap.put("NOTIFICATION_APPS", ap4.a((T[]) new Integer[]{10}));
        hashMap.put("SET_ALARMS", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("SET_NOTIFICATION", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("SET_COMPLICATION", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("SET_WATCH_APP_COMMUTE_TIME", ap4.a((T[]) new Integer[]{1, 2, 3, 15}));
        hashMap.put("SET_COMPLICATION_WEATHER", ap4.a((T[]) new Integer[]{1, 2, 3, 15}));
        hashMap.put("SET_COMPLICATION_CHANCE_OF_RAIN", ap4.a((T[]) new Integer[]{1, 2, 15}));
        hashMap.put("SET_WATCH_APP", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("SET_WATCH_APP_MUSIC", ap4.a((T[]) new Integer[]{1, 10}));
        hashMap.put("SET_WATCH_APP_WEATHER", ap4.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("FIND_DEVICE", ap4.a((T[]) new Integer[]{2, 3}));
        hashMap.put("EDIT_AVATAR", ap4.a((T[]) new Integer[]{11, 12}));
        hashMap.put("UPDATE_FIRMWARE", ap4.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("NOTIFICATION_CONTACTS_ASSIGNMENT", ap4.a((T[]) new Integer[]{4}));
        hashMap.put("SET_MICRO_APP", ap4.a((T[]) new Integer[]{1}));
        hashMap.put("SET_MICRO_APP_MUSIC", ap4.a((T[]) new Integer[]{1, 10}));
        hashMap.put("SET_MICRO_APP_COMMUTE_TIME", ap4.a((T[]) new Integer[]{1, 2, 3}));
        hashMap.put("GET_USER_LOCATION", ap4.a((T[]) new Integer[]{2, 3}));
        hashMap.put("CAPTURE_IMAGE", ap4.a((T[]) new Integer[]{11}));
        b = hashMap;
    }
    */

    @DexIgnore
    public final void a(h23 h23) {
        c = h23;
    }

    @DexIgnore
    public final void b(Context context, String str) {
        jr4.b(str, "microAppId");
        a(this, context, b(str), false, 4, null);
    }

    @DexIgnore
    public final boolean c(int i) {
        return i == 2 || i == 4 || i == 5 || i == 6 || i == 15 || i == 7 || i == 8 || i == 9 || i == 11 || i == 12 || i == 100 || i == 101 || i == 13 || i == 14;
    }

    @DexIgnore
    public static /* synthetic */ boolean a(e23 e23, Context context, String str, boolean z, int i, Object obj) {
        if ((i & 4) != 0) {
            z = true;
        }
        return e23.a(context, str, z);
    }

    /* JADX WARNING: type inference failed for: r10v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v6, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v9, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v15, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v18, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v22, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v2, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v27, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v30, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v8, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v34, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r6v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v38, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r3v12, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v42, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v46, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v50, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v53, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v56, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v59, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v62, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v65, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v68, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v71, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v75, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v79, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v83, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r10v86, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0276, code lost:
        r8 = r1;
        r1 = r10;
        r10 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x029a, code lost:
        r10 = r0;
     */
    @DexIgnore
    public final Triple<String, String, String> b(int i) {
        String str = null;
        String str2;
        String str3;
        String str4;
        String str5 = "";
        String str6 = null;
        if (i == 15) {
            str5 = v13.a(PortfolioApp.Z.c(), 2131821075);
            jr4.a((Object) str5, "LanguageHelper.getString\u2026BackgroundLocationAccess)");
            str = v13.a(PortfolioApp.Z.c(), 2131821476);
            jr4.a((Object) str, "LanguageHelper.getString\u2026_service_general_explain)");
        } else if (i == 100) {
            str5 = v13.a(PortfolioApp.Z.c(), 2131821077);
            jr4.a((Object) str5, "LanguageHelper.getString\u2026_PhonePermissionRequired)");
            str = v13.a(PortfolioApp.Z.c(), 2131821489);
            jr4.a((Object) str, "LanguageHelper.getString\u2026call_and_sms_permissions)");
        } else if (i != 101) {
            switch (i) {
                case 1:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821141);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    str2 = v13.a(PortfolioApp.Z.c(), 2131821141);
                    jr4.a((Object) str2, "LanguageHelper.getString\u2026seTurnOnBluetoothInOrder)");
                    break;
                case 2:
                    str3 = v13.a(PortfolioApp.Z.c(), 2131821139);
                    jr4.a((Object) str3, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    str4 = v13.a(PortfolioApp.Z.c(), 2131821139);
                    jr4.a((Object) str4, "LanguageHelper.getString\u2026tionPermissionIsRequired)");
                    StringBuilder sb = new StringBuilder();
                    sb.append("https://support.google.com/accounts/answer/6179507?hl=");
                    Locale a2 = v13.a();
                    jr4.a(a2, "LanguageHelper.getLocale()");
                    sb.append(a2.getLanguage());
                    str6 = sb.toString();
                    break;
                case 3:
                    str3 = v13.a(PortfolioApp.Z.c(), 2131821140);
                    jr4.a((Object) str3, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
                    str4 = v13.a(PortfolioApp.Z.c(), 2131821140);
                    jr4.a((Object) str4, "LanguageHelper.getString\u2026esAreRequiredForLocation)");
                    StringBuilder sb2 = new StringBuilder();
                    sb2.append("https://support.google.com/accounts/answer/3467281?hl=");
                    Locale a3 = v13.a();
                    jr4.a(a3, "LanguageHelper.getLocale()");
                    sb2.append(a3.getLanguage());
                    str6 = sb2.toString();
                    break;
                case 4:
                    or4 or4 = com.fossil.or4.a;
                    String a4 = v13.a(PortfolioApp.Z.c(), 2131821020);
                    jr4.a((Object) a4, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    Object[] objArr = {PortfolioApp.Z.c().i()};
                    str3 = String.format(a4, Arrays.copyOf(objArr, objArr.length));
                    jr4.a((Object) str3, "java.lang.String.format(format, *args)");
                    or4 or42 = com.fossil.or4.a;
                    String a5 = v13.a(PortfolioApp.Z.c(), 2131821020);
                    jr4.a((Object) a5, "LanguageHelper.getString\u2026randToAccessYourContacts)");
                    Object[] objArr2 = {PortfolioApp.Z.c().i()};
                    str4 = String.format(a5, Arrays.copyOf(objArr2, objArr2.length));
                    jr4.a((Object) str4, "java.lang.String.format(format, *args)");
                    break;
                case 5:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821647);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026_phone_state_is_required)");
                    str2 = v13.a(PortfolioApp.Z.c(), 2131821647);
                    jr4.a((Object) str2, "LanguageHelper.getString\u2026_phone_state_is_required)");
                    break;
                case 6:
                    or4 or43 = com.fossil.or4.a;
                    String a6 = v13.a(PortfolioApp.Z.c(), 2131821019);
                    jr4.a((Object) a6, "LanguageHelper.getString\u2026owBrandToAccessYourPhone)");
                    Object[] objArr3 = {PortfolioApp.Z.c().i()};
                    str5 = String.format(a6, Arrays.copyOf(objArr3, objArr3.length));
                    jr4.a((Object) str5, "java.lang.String.format(format, *args)");
                    or4 or44 = com.fossil.or4.a;
                    String a7 = v13.a(PortfolioApp.Z.c(), 2131821019);
                    jr4.a((Object) a7, "LanguageHelper.getString\u2026owBrandToAccessYourPhone)");
                    Object[] objArr4 = {PortfolioApp.Z.c().i()};
                    str = String.format(a7, Arrays.copyOf(objArr4, objArr4.length));
                    jr4.a((Object) str, "java.lang.String.format(format, *args)");
                    break;
                case 7:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821648);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026ing.read_sms_is_required)");
                    str = v13.a(PortfolioApp.Z.c(), 2131821648);
                    jr4.a((Object) str, "LanguageHelper.getString\u2026ing.read_sms_is_required)");
                    break;
                case 8:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821650);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026.receive_sms_is_required)");
                    str = v13.a(PortfolioApp.Z.c(), 2131821650);
                    jr4.a((Object) str, "LanguageHelper.getString\u2026.receive_sms_is_required)");
                    break;
                case 9:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821649);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026.receive_mms_is_required)");
                    str = v13.a(PortfolioApp.Z.c(), 2131821649);
                    jr4.a((Object) str, "LanguageHelper.getString\u2026.receive_mms_is_required)");
                    break;
                case 10:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821599);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026ation_access_is_required)");
                    str = v13.a(PortfolioApp.Z.c(), 2131821599);
                    jr4.a((Object) str, "LanguageHelper.getString\u2026ation_access_is_required)");
                    break;
                case 11:
                    or4 or45 = com.fossil.or4.a;
                    String a8 = v13.a(PortfolioApp.Z.c(), 2131821018);
                    jr4.a((Object) a8, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    Object[] objArr5 = {PortfolioApp.Z.c().i()};
                    str5 = String.format(a8, Arrays.copyOf(objArr5, objArr5.length));
                    jr4.a((Object) str5, "java.lang.String.format(format, *args)");
                    or4 or46 = com.fossil.or4.a;
                    String a9 = v13.a(PortfolioApp.Z.c(), 2131821018);
                    jr4.a((Object) a9, "LanguageHelper.getString\u2026wBrandToAccessYourCamera)");
                    Object[] objArr6 = {PortfolioApp.Z.c().i()};
                    str = String.format(a9, Arrays.copyOf(objArr6, objArr6.length));
                    jr4.a((Object) str, "java.lang.String.format(format, *args)");
                    break;
                case 12:
                    str5 = v13.a(PortfolioApp.Z.c(), 2131821076);
                    jr4.a((Object) str5, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    str = v13.a(PortfolioApp.Z.c(), 2131821076);
                    jr4.a((Object) str, "LanguageHelper.getString\u2026SdCardPermissionRequired)");
                    break;
                default:
                    str = str5;
                    break;
            }
        } else {
            str5 = v13.a(PortfolioApp.Z.c(), 2131821078);
            jr4.a((Object) str5, "LanguageHelper.getString\u2026t__SmsPermissionRequired)");
            str = v13.a(PortfolioApp.Z.c(), 2131821489);
            jr4.a((Object) str, "LanguageHelper.getString\u2026call_and_sms_permissions)");
        }
        return new Triple<>(str5, str, str6);
    }

    @DexIgnore
    public final boolean a(Context context, String str, boolean z) {
        jr4.b(str, "feature");
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (Map.Entry next : b.entrySet()) {
            if (jr4.a(next.getKey(), (Object) str)) {
                for (Object intValue : (Iterable) next.getValue()) {
                    int intValue2 = ((Number)intValue).intValue();
                    if (intValue2 == 100) {
                        arrayList2.add(100);
                        Context context2 = (Context) weakReference.get();
                        if (context2 != null) {
                            for (String str2 : d.a()) {
                                if (!o45.a(context2, str2)) {
                                    arrayList.add(100);
                                }
                            }
                            oo4 oo4 = com.fossil.oo4.a;
                        }
                    } else if (intValue2 != 101) {
                        switch (intValue2) {
                            case 1:
                                arrayList2.add(1);
                                if (BluetoothUtils.isBluetoothEnable()) {
                                    break;
                                } else {
                                    arrayList.add(1);
                                    break;
                                }
                            case 2:
                                arrayList2.add(2);
                                Context context3 = (Context) weakReference.get();
                                if (context3 != null) {
                                    if (!LocationUtils.isLocationPermissionGranted(context3)) {
                                        arrayList.add(2);
                                    }
                                    oo4 oo42 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 3:
                                arrayList2.add(3);
                                Context context4 = (Context) weakReference.get();
                                if (context4 != null) {
                                    if (!LocationUtils.isLocationEnable(context4)) {
                                        arrayList.add(3);
                                    }
                                    oo4 oo43 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 4:
                                arrayList2.add(4);
                                Context context5 = (Context) weakReference.get();
                                if (context5 != null) {
                                    if (!o45.a(context5, "android.permission.READ_CONTACTS")) {
                                        arrayList.add(4);
                                    }
                                    oo4 oo44 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 5:
                                arrayList2.add(5);
                                Context context6 = (Context) weakReference.get();
                                if (context6 != null) {
                                    if (!o45.a(context6, "android.permission.READ_PHONE_STATE")) {
                                        arrayList.add(5);
                                    }
                                    oo4 oo45 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 6:
                                arrayList2.add(6);
                                Context context7 = (Context) weakReference.get();
                                if (context7 != null) {
                                    if (!o45.a(context7, "android.permission.READ_CALL_LOG")) {
                                        arrayList.add(6);
                                    }
                                    oo4 oo46 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 7:
                                arrayList2.add(7);
                                Context context8 = (Context) weakReference.get();
                                if (context8 != null) {
                                    if (!o45.a(context8, "android.permission.READ_SMS")) {
                                        arrayList.add(7);
                                    }
                                    oo4 oo47 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 8:
                                arrayList2.add(8);
                                Context context9 = (Context) weakReference.get();
                                if (context9 != null) {
                                    if (!o45.a(context9, "android.permission.RECEIVE_SMS")) {
                                        arrayList.add(8);
                                    }
                                    oo4 oo48 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 9:
                                arrayList2.add(9);
                                Context context10 = (Context) weakReference.get();
                                if (context10 != null) {
                                    if (!o45.a(context10, "android.permission.RECEIVE_MMS")) {
                                        arrayList.add(9);
                                    }
                                    oo4 oo49 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 10:
                                arrayList2.add(10);
                                if (PortfolioApp.Z.c().E()) {
                                    break;
                                } else {
                                    int hashCode = str.hashCode();
                                    if (hashCode == -1624709818 ? str.equals("NOTIFICATION_APPS") : !(hashCode != 216213615 || !str.equals("SET_MICRO_APP_MUSIC"))) {
                                        h23 h23 = c;
                                        if (h23 == null) {
                                            Boolean.valueOf(arrayList.add(10));
                                            break;
                                        } else {
                                            if (h23.i()) {
                                                arrayList.add(10);
                                            }
                                            oo4 oo410 = oo4.a;
                                            break;
                                        }
                                    } else {
                                        arrayList.add(10);
                                        break;
                                    }
                                }
                            case 11:
                                arrayList2.add(11);
                                Context context11 = (Context) weakReference.get();
                                if (context11 != null) {
                                    if (!o45.a(context11, "android.permission.CAMERA")) {
                                        arrayList.add(11);
                                    }
                                    oo4 oo411 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 12:
                                arrayList2.add(12);
                                Context context12 = (Context) weakReference.get();
                                if (context12 != null) {
                                    if (!o45.a(context12, "android.permission.READ_EXTERNAL_STORAGE")) {
                                        arrayList.add(12);
                                    }
                                    oo4 oo412 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 13:
                                arrayList2.add(13);
                                Context context13 = (Context) weakReference.get();
                                if (context13 != null) {
                                    if (!o45.a(context13, "android.permission.CALL_PHONE")) {
                                        arrayList.add(13);
                                    }
                                    oo4 oo413 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 14:
                                arrayList2.add(14);
                                Context context14 = (Context) weakReference.get();
                                if (context14 != null) {
                                    if (!o45.a(context14, "android.permission.ANSWER_PHONE_CALLS")) {
                                        arrayList.add(14);
                                    }
                                    oo4 oo414 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                            case 15:
                                arrayList2.add(15);
                                Context context15 = (Context) weakReference.get();
                                if (context15 != null) {
                                    if (a && !LocationUtils.isBackgroundLocationPermissionGranted(context15)) {
                                        arrayList.add(15);
                                    }
                                    oo4 oo415 = oo4.a;
                                    break;
                                } else {
                                    break;
                                }
                        }
                    } else {
                        arrayList2.add(101);
                        Context context16 = (Context) weakReference.get();
                        if (context16 != null) {
                            for (String str3 : d.b()) {
                                if (!o45.a(context16, str3)) {
                                    arrayList.add(101);
                                }
                            }
                            oo4 oo416 = oo4.a;
                        }
                    }
                }
            }
        }
        if (!(!arrayList.isEmpty())) {
            return true;
        }
        if (z) {
            Context context17 = (Context) weakReference.get();
            if (context17 != null) {
                PermissionActivity.a aVar = PermissionActivity.C;
                jr4.a(context17, "it");
                aVar.a(context17, arrayList2);
                oo4 oo417 = oo4.a;
            }
        }
        return false;
    }

    @DexIgnore
    public final ArrayList<String> b() {
        throw null;
        // return ap4.a((T[]) new String[]{"android.permission.READ_SMS", "android.permission.RECEIVE_SMS", "android.permission.RECEIVE_MMS"});
    }

    @DexIgnore
    public final String b(String str) {
        if (jr4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
            return "SET_MICRO_APP_MUSIC";
        }
        return jr4.a((Object) str, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue()) ? "SET_MICRO_APP_COMMUTE_TIME" : "SET_MICRO_APP";
    }

    @DexIgnore
    public final boolean a(Context context, Set<Integer> set) {
        jr4.b(set, "setOfPerms");
        FLogger.INSTANCE.getLocal().d("PermissionManager", "checkPermsGrantedForPreset");
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList = new ArrayList();
        for (Number intValue : set) {
            int intValue2 = intValue.intValue();
            if (intValue2 != 1) {
                if (intValue2 == 2) {
                    Context context2 = (Context) weakReference.get();
                    if (context2 != null && !LocationUtils.isLocationPermissionGranted(context2)) {
                        arrayList.add(2);
                    }
                } else if (intValue2 == 3) {
                    Context context3 = (Context) weakReference.get();
                    if (context3 != null && !LocationUtils.isLocationEnable(context3)) {
                        arrayList.add(3);
                    }
                } else if (intValue2 != 10) {
                    if (intValue2 == 15 && a && !LocationUtils.isBackgroundLocationPermissionGranted(context)) {
                        arrayList.add(15);
                    }
                } else if (!PortfolioApp.Z.c().E()) {
                    arrayList.add(10);
                }
            } else if (!BluetoothUtils.isBluetoothEnable()) {
                arrayList.add(1);
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "listPermissionsNotGranted=" + arrayList);
        if (!(!arrayList.isEmpty())) {
            return true;
        }
        Context context4 = (Context) weakReference.get();
        if (context4 == null) {
            return false;
        }
        PermissionActivity.a aVar = PermissionActivity.C;
        jr4.a(context4, "it");
        aVar.a(context4, arrayList);
        return false;
    }

    @DexIgnore
    public final Set<Integer> a(DianaPreset dianaPreset) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        linkedHashSet.add(1);
        if (dianaPreset != null) {
            for (DianaPresetComplicationSetting id : dianaPreset.getComplications()) {
                String id2 = id.getId();
                int hashCode = id2.hashCode();
                if (hashCode != -48173007) {
                    if (hashCode == 1223440372 && id2.equals("weather")) {
                        linkedHashSet.add(2);
                        linkedHashSet.add(3);
                        if (a) {
                            linkedHashSet.add(15);
                        }
                    }
                } else if (id2.equals("chance-of-rain")) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (a) {
                        linkedHashSet.add(15);
                    }
                }
            }
            for (DianaPresetWatchAppSetting id3 : dianaPreset.getWatchapps()) {
                String id4 = id3.getId();
                int hashCode2 = id4.hashCode();
                if (hashCode2 != -829740640) {
                    if (hashCode2 != 104263205) {
                        if (hashCode2 == 1223440372 && id4.equals("weather")) {
                            linkedHashSet.add(2);
                            linkedHashSet.add(3);
                            if (a) {
                                linkedHashSet.add(15);
                            }
                        }
                    } else if (id4.equals(Constants.MUSIC)) {
                        linkedHashSet.add(10);
                    }
                } else if (id4.equals("commute-time")) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (a) {
                        linkedHashSet.add(15);
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "setOfPerms=" + linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public final Set<Integer> a(HybridPreset hybridPreset) {
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        boolean z = true;
        linkedHashSet.add(1);
        if (Build.VERSION.SDK_INT < 29) {
            z = false;
        }
        if (hybridPreset != null) {
            for (HybridPresetAppSetting appId : hybridPreset.getButtons()) {
                String appId2 = appId.getAppId();
                if (jr4.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_HID_MEDIA_CONTROL_MUSIC.getValue())) {
                    linkedHashSet.add(10);
                } else if (jr4.a((Object) appId2, (Object) MicroAppInstruction.MicroAppID.UAPP_COMMUTE_TIME.getValue())) {
                    linkedHashSet.add(2);
                    linkedHashSet.add(3);
                    if (z) {
                        linkedHashSet.add(15);
                    }
                }
            }
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        local.d("PermissionManager", "setOfPerms=" + linkedHashSet);
        return linkedHashSet;
    }

    @DexIgnore
    public final boolean a(Context context, ArrayList<PermissionCodes> arrayList) {
        jr4.b(arrayList, "permissionErrorCodes");
        WeakReference weakReference = new WeakReference(context);
        ArrayList arrayList2 = new ArrayList();
        throw null;
        // for (PermissionCodes ordinal : arrayList) {
        //     switch (d23.a[ordinal.ordinal()]) {
        //         case 1:
        //             arrayList2.add(1);
        //             break;
        //         case 2:
        //         case 3:
        //             arrayList2.add(2);
        //             break;
        //         case 4:
        //             arrayList2.add(15);
        //             break;
        //         case 5:
        //         case 6:
        //             arrayList2.add(3);
        //             break;
        //     }
        // }
        // if (!(!arrayList2.isEmpty())) {
        //     return false;
        // }
        // Context context2 = (Context) weakReference.get();
        // if (context2 != null) {
        //     PermissionActivity.a aVar = PermissionActivity.C;
        //     jr4.a(context2, "it");
        //     aVar.a(context2, arrayList2);
        // }
        // return true;
    }

    @DexIgnore
    public final void a(Context context, String str) {
        jr4.b(str, "complicationId");
        a(this, context, a(str), false, 4, null);
    }

    @DexIgnore
    public final ArrayList<String> a(int i) {
        ArrayList<String> arrayList = new ArrayList<>();
        if (i == 2) {
            arrayList.add("android.permission.ACCESS_FINE_LOCATION");
        } else if (i == 100) {
            arrayList.addAll(a());
        } else if (i != 101) {
            switch (i) {
                case 4:
                    arrayList.add("android.permission.READ_CONTACTS");
                    break;
                case 5:
                    arrayList.add("android.permission.READ_PHONE_STATE");
                    break;
                case 6:
                    arrayList.add("android.permission.READ_CALL_LOG");
                    break;
                case 7:
                    arrayList.add("android.permission.READ_SMS");
                    break;
                case 8:
                    arrayList.add("android.permission.RECEIVE_SMS");
                    break;
                case 9:
                    arrayList.add("android.permission.RECEIVE_MMS");
                    break;
                default:
                    switch (i) {
                        case 11:
                            arrayList.add("android.permission.CAMERA");
                            break;
                        case 12:
                            arrayList.add("android.permission.READ_EXTERNAL_STORAGE");
                            break;
                        case 13:
                            arrayList.add("android.permission.CALL_PHONE");
                            break;
                        case 14:
                            arrayList.add("android.permission.ANSWER_PHONE_CALLS");
                            break;
                        case 15:
                            arrayList.add("android.permission.ACCESS_BACKGROUND_LOCATION");
                            break;
                    }
            }
        } else {
            arrayList.addAll(b());
        }
        return arrayList;
    }

    @DexIgnore
    public final boolean a(Context context, int i) {
        WeakReference weakReference = new WeakReference(context);
        if (i == 100) {
            Context context2 = (Context) weakReference.get();
            if (context2 == null) {
                return true;
            }
            boolean z = true;
            for (String str : d.a()) {
                if (!o45.a(context2, str)) {
                    z = false;
                }
            }
            return z;
        } else if (i != 101) {
            switch (i) {
                case 1:
                    return BluetoothUtils.isBluetoothEnable();
                case 2:
                    Context context3 = (Context) weakReference.get();
                    if (context3 != null) {
                        return LocationUtils.isLocationPermissionGranted(context3);
                    }
                    break;
                case 3:
                    Context context4 = (Context) weakReference.get();
                    if (context4 != null) {
                        return LocationUtils.isLocationEnable(context4);
                    }
                    break;
                case 4:
                    Context context5 = (Context) weakReference.get();
                    if (context5 != null) {
                        return o45.a(context5, "android.permission.READ_CONTACTS");
                    }
                    break;
                case 5:
                    Context context6 = (Context) weakReference.get();
                    if (context6 != null) {
                        return o45.a(context6, "android.permission.READ_PHONE_STATE");
                    }
                    break;
                case 6:
                    Context context7 = (Context) weakReference.get();
                    if (context7 != null) {
                        return o45.a(context7, "android.permission.READ_CALL_LOG");
                    }
                    break;
                case 7:
                    Context context8 = (Context) weakReference.get();
                    if (context8 != null) {
                        return o45.a(context8, "android.permission.READ_SMS");
                    }
                    break;
                case 8:
                    Context context9 = (Context) weakReference.get();
                    if (context9 != null) {
                        return o45.a(context9, "android.permission.RECEIVE_SMS");
                    }
                    break;
                case 9:
                    Context context10 = (Context) weakReference.get();
                    if (context10 != null) {
                        return o45.a(context10, "android.permission.RECEIVE_MMS");
                    }
                    break;
                case 10:
                    return PortfolioApp.Z.c().E();
                case 11:
                    Context context11 = (Context) weakReference.get();
                    if (context11 != null) {
                        return o45.a(context11, "android.permission.CAMERA");
                    }
                    break;
                case 12:
                    Context context12 = (Context) weakReference.get();
                    if (context12 != null) {
                        return o45.a(context12, "android.permission.READ_EXTERNAL_STORAGE");
                    }
                    break;
                case 13:
                    Context context13 = (Context) weakReference.get();
                    if (context13 != null) {
                        return o45.a(context13, "android.permission.CALL_PHONE");
                    }
                    break;
                case 14:
                    Context context14 = (Context) weakReference.get();
                    if (context14 != null) {
                        return o45.a(context14, "android.permission.ANSWER_PHONE_CALLS");
                    }
                    break;
                case 15:
                    return LocationUtils.isBackgroundLocationPermissionGranted(context);
            }
            return false;
        } else {
            Context context15 = (Context) weakReference.get();
            if (context15 == null) {
                return true;
            }
            boolean z2 = true;
            for (String str2 : d.b()) {
                if (!o45.a(context15, str2)) {
                    z2 = false;
                }
            }
            return z2;
        }
    }

    @DexIgnore
    public final ArrayList<String> a() {
        throw null;
        // if (!DeviceHelper.o.l() || !DeviceHelper.o.f(PortfolioApp.Z.c().e())) {
        //     return ap4.a((T[]) new String[]{"android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG"});
        // }
        // return ap4.a((T[]) new String[]{"android.permission.READ_PHONE_STATE", "android.permission.READ_CALL_LOG", "android.permission.CALL_PHONE", "android.permission.ANSWER_PHONE_CALLS"});
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:30:0x007d A[RETURN, SYNTHETIC] */
    public final String a(String str) {
        switch (str.hashCode()) {
            case -829740640:
                return str.equals("commute-time") ? "SET_WATCH_APP_COMMUTE_TIME" : "SET_WATCH_APP";
            case -331239923:
                return str.equals(Constants.BATTERY) ? "SET_COMPLICATION" : "SET_WATCH_APP";
            case -168965370:
                if (!str.equals(Constants.CALORIES)) {
                    return "SET_WATCH_APP";
                }
                break;
            case -85386984:
                if (!str.equals("active-minutes")) {
                    return "SET_WATCH_APP";
                }
                break;
            case -48173007:
                return str.equals("chance-of-rain") ? "SET_COMPLICATION_CHANCE_OF_RAIN" : "SET_WATCH_APP";
            case 3076014:
                if (!str.equals(HardwareLog.COLUMN_DATE)) {
                    return "SET_WATCH_APP";
                }
                break;
            case 96634189:
                if (!str.equals("empty")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 104263205:
                return str.equals(Constants.MUSIC) ? "SET_WATCH_APP_MUSIC" : "SET_WATCH_APP";
            case 109761319:
                if (!str.equals("steps")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 134170930:
                if (!str.equals("second-timezone")) {
                    return "SET_WATCH_APP";
                }
                break;
            case 1223440372:
                if (str.equals("weather")) {
                    return "SET_COMPLICATION_WEATHER";
                }
                return str.equals("weather") ? "SET_WATCH_APP_WEATHER" : "SET_WATCH_APP";
            case 1884273159:
                if (!str.equals("heart-rate")) {
                    return "SET_WATCH_APP";
                }
                break;
            default:
                return "SET_WATCH_APP";
        }
        throw null;
    }
}
