package com.portfolio.platform.service.notification;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;
import com.fossil.ap4;
import com.fossil.aq4;
import com.fossil.b94;
import com.fossil.blesdk.device.data.notification.NotificationVibePattern;
import com.fossil.d94;
import com.fossil.eq4;
import com.fossil.er4;
import com.fossil.ew4;
import com.fossil.g23;
import com.fossil.h23;
import com.fossil.jr4;
import com.fossil.lo4;
import com.fossil.ms3;
import com.fossil.mv4;
import com.fossil.oo4;
import com.fossil.oq4;
import com.fossil.pt4;
import com.fossil.sz2;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wp4;
import com.fossil.xq4;
import com.fossil.yu4;
import com.fossil.zt4;
import com.fossil.zu4;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;
import com.portfolio.platform.helper.DeviceHelper;
import com.portfolio.platform.service.notification.DianaNotificationComponent.d;
import com.portfolio.platform.uirenew.home.alerts.diana.details.domain.model.AppWrapper;
import com.portfolio.platform.util.NotificationAppHelper;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.coroutines.CoroutineContext;
import kotlin.coroutines.jvm.internal.SuspendLambda;
import kotlin.jvm.internal.Ref$ObjectRef;
import kotlin.text.Regex;
import kotlin.text.StringsKt__StringsKt;
import kotlinx.coroutines.CoroutineStart;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexReplace;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.ADD)
public final class DianaNotificationComponent {
    @DexIgnore
    public static /* final */ HashMap<String, b> j; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, g> k;
    @DexIgnore
    public static /* final */ a l; // = new a((er4) null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ c b; // = new c(this.g);
    @DexIgnore
    public int c; // = 1380;
    @DexIgnore
    public int d; // = 1140;
    @DexIgnore
    public e e;
    @DexIgnore
    public PhoneCallObserver f;
    @DexIgnore
    public /* final */ h23 g;
    @DexIgnore
    public /* final */ DNDSettingsDatabase h;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase i;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public final class PhoneCallObserver extends ContentObserver {
        @DexIgnore
        public a a; // = new a(this, 0, (String) null, 0, 0, 15, (er4) null);

        @DexEdit(defaultAction = DexAction.IGNORE)
        @eq4(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public final class Anon1 extends SuspendLambda implements xq4<Object, Object, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public yu4 p$;
            @DexIgnore
            public /* final */ /* synthetic */ PhoneCallObserver this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public Anon1(PhoneCallObserver phoneCallObserver, wp4 wp4) {
                super(2, wp4);
                this.this$0 = phoneCallObserver;
            }

            @DexIgnore
            public final wp4<oo4> create(Object obj, wp4<?> wp4) {
                jr4.b(wp4, "completion");
                Anon1 anon1 = new Anon1(this.this$0, wp4);
                anon1.p$ = (yu4) obj;
                throw null;
                // return anon1;
            }

            @DexIgnore
            public final Object invoke(Object obj, Object obj2) {
                throw null;
                // return ((Anon1) create(obj, (wp4) obj2)).invokeSuspend(oo4.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                aq4.a();
                if (this.label == 0) {
                    lo4.a(obj);
                    PhoneCallObserver phoneCallObserver = this.this$0;
                    a b = phoneCallObserver.a();
                    if (b == null) {
                        b = new a(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    phoneCallObserver.a = b;
                    return oo4.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexEdit(defaultAction = DexAction.IGNORE)
        public final class a {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public a(PhoneCallObserver phoneCallObserver, long j, String str, int i, long j2) {
                jr4.b(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }

            @DexIgnore
            /* JADX WARNING: Illegal instructions before constructor call */
            public /* synthetic */ a(PhoneCallObserver phoneCallObserver, long j, String str, int i, long j2, int i2, er4 er4) {
                throw null;
                // this(phoneCallObserver, r0, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
                // long j3 = (i2 & 1) != 0 ? -1 : j;
            }
        }

        @DexIgnore
        public PhoneCallObserver() {
            super(null);
            throw null;
            // ew4 unused = zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new Anon1(this, (wp4) null), 3, (Object) null);
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            b();
        }

        @DexIgnore
        /* JADX WARNING: type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        public final void b() {
            d94.a aVar = d94.a;
            Context applicationContext = PortfolioApp.Z.c().getApplicationContext();
            jr4.a(applicationContext, "PortfolioApp.instance.applicationContext");
            if (!aVar.d(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(DianaNotificationComponent.this.b(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                throw null;
                // ew4 unused = zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$PhoneCallObserver$processMissedCall$Anon1(this, (wp4) null), 3, (Object) null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            b();
        }

        /* JADX WARNING: type inference failed for: r4v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0065, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            com.fossil.oq4.a(r4, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
            throw r1;
         */
        @DexIgnore
        @SuppressLint({"MissingPermission"})
        public final a a() {
            Cursor query = PortfolioApp.Z.c().getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{"_id", "number", HardwareLog.COLUMN_DATE, "type"}, null, null, "date DESC LIMIT 1");
            if (query != null) {
                if (query.moveToFirst()) {
                    long j = query.getLong(query.getColumnIndex("_id"));
                    String string = query.getString(query.getColumnIndex("number"));
                    int i = query.getInt(query.getColumnIndex("type"));
                    long j2 = query.getLong(query.getColumnIndex(HardwareLog.COLUMN_DATE));
                    query.close();
                    jr4.a((Object) string, "number");
                    a aVar = new a(this, j, string, i, j2);
                    try {
                        oq4.a(query, null);
                        return aVar;
                    } catch (Exception e) {
                        e.printStackTrace();
                        FLogger.INSTANCE.getLocal().e(DianaNotificationComponent.this.b(), e.getMessage());
                    }
                } else {
                    query.close();
                    oo4 oo4 = com.fossil.oo4.a;
                    oq4.a(query, null);
                }
            }
            return null;
        }
    }

    @DexIgnore
    public enum PhoneStateEnum {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public final HashMap<String, b> a() {
            if (DianaNotificationComponent.j.isEmpty()) {
                int i = 1;
                for (DianaNotificationObj.AApplicationName aApplicationName : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = aApplicationName.getPackageName();
                    if (!jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !jr4.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        DianaNotificationComponent.j.put(aApplicationName.getPackageName(), new b(i, aApplicationName, false, false));
                    } else {
                        DianaNotificationComponent.j.put(aApplicationName.getPackageName(), new b(i, aApplicationName, false, true));
                    }
                    i++;
                }
            }
            return DianaNotificationComponent.j;
        }

        @DexIgnore
        public /* synthetic */ a(er4 er4) {
            this();
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            jr4.b(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
    public static class d {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public long g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public int j;
        @DexIgnore
        public boolean k;
        @DexAdd
        public boolean dnd;

        @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(er4 er4) {
                this();
            }
        }

        /*
        static {
            new a((er4) null);
        }
        */

        @DexIgnore
        public d() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final void c(String str) {
            jr4.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final String d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof d)) {
                return false;
            }
            d dVar = (d) obj;
            return jr4.a((Object) this.a, (Object) dVar.a) && this.g == dVar.g;
        }

        @DexIgnore
        public final void f(String str) {
            jr4.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String g() {
            return this.d;
        }

        @DexIgnore
        public final long h() {
            return this.g;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final boolean i() {
            return this.k;
        }

        @DexIgnore
        public final boolean j() {
            return this.i;
        }

        @DexIgnore
        public final boolean k() {
            return this.h;
        }

        @DexIgnore
        public final void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void b(String str) {
            jr4.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.h = z;
        }

        @DexIgnore
        public final void d(String str) {
            jr4.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            jr4.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final String f() {
            return this.f;
        }

        @DexIgnore
        public final void a(long j2) {
            this.g = j2;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final int c() {
            return this.j;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.k = z;
        }

        @DexIgnore
        public final boolean b(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final boolean c(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexAdd
        public byte[] drawableToPixels(Drawable drawable, int width, int height, int background, boolean invert) {

            Bitmap bitmap = null;
            // Draw icon onto 24 x 24 bitmap

            // if (drawable instanceof BitmapDrawable) {
            //     bitmap = ((BitmapDrawable) drawable).getBitmap();
            // } else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (drawable instanceof AdaptiveIconDrawable) {
                    Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                    Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                    Drawable[] drr = new Drawable[2];
                    drr[0] = backgroundDr;
                    drr[1] = foregroundDr;

                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    LayerDrawable layerDrawable = new LayerDrawable(drr);
                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);
                }
            }
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
            }
            if (bitmap != null) {

                // Convert to greyscale
                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bmpGrayscale);

                if ((background & 0xFF000000) != 0) {
                    Paint paint = new Paint();
                    paint.setColor(background);
                    c.drawRect(0F, 0F, (float) width, (float) height, paint);
                }

                Paint paint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                paint.setColorFilter(f);

                c.drawBitmap(bitmap, 0, 0, paint);
                // scaled.recycle();

                // Downsample to 2 bit image
                int[] pixels = new int[width * height];
                bmpGrayscale.getPixels(pixels, 0, width, 0, 0, width, height);
                byte[] b_pixels = new byte[pixels.length];

                int n_light_pix = 0;
                for (int i = 0; i < pixels.length; i++) {
                    int pix = pixels[i];
                    //todo invert
                    b_pixels[i] = (byte) (pix >> 6 & 0x03);
                    if (invert) {
                        b_pixels[i] = (byte)(3 - b_pixels[i]);
                    }
                    if (b_pixels[i] > 0) {
                        n_light_pix++;
                    }
                }
                bitmap.recycle();
                bmpGrayscale.recycle();
                if ((100.0 * n_light_pix / pixels.length) < 5) {
                    // under 5% pixels visible, need to invert the background and try again
                    return null;
                }
                return b_pixels;
            }
            return null;
        }

        @DexIgnore
        public final boolean a(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!StringsKt__StringsKt.a(tag, "sms", true) && !StringsKt__StringsKt.a(tag, "mms", true) && !StringsKt__StringsKt.a(tag, "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        @DexAdd
        public static byte[] rle_encode(byte[] chars) {
            try {
                if (chars == null || chars.length == 0) return null;

                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                Writer writer = new OutputStreamWriter(bout);

                // StringBuilder builder = new StringBuilder();
                byte current = chars[0];
                int count = 1;

                for (int i = 1; i < chars.length; i++) {
                    if (current != chars[i]) {
                        if (count > 0) {
                            writer.write(count);
                            writer.write(current);
                        }
                        current = chars[i];
                        count = 1;
                    } else {
                        count++;
                        if (count == 254) {
                            writer.write(count);
                            writer.write(current);
                            count = 0;
                        }
                    }
                }
                if (count > 1) writer.write(count);
                writer.write(current);
                writer.flush();
                return bout.toByteArray();
            } catch (IOException unused) {
                return null;
            }
        }


        @DexAdd
        private static boolean checkDND(Notification notification) {
            // Return true if the notification should be hidden for DND
            String category = notification.category;
            NotificationManager notificationManager = (NotificationManager)PortfolioApp.Z.c().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int filter = notificationManager.getCurrentInterruptionFilter();

                switch (filter) {
                    case NotificationManager.INTERRUPTION_FILTER_NONE:
                        return true;

                    case NotificationManager.INTERRUPTION_FILTER_ALARMS:
                    if (!TextUtils.equals(Notification.CATEGORY_ALARM, category)) {
                        return true;
                    }
                    break;

                    case NotificationManager.INTERRUPTION_FILTER_PRIORITY:
                        String[] activeCategories = NotificationManager.Policy.priorityCategoriesToString(notificationManager.getNotificationPolicy().priorityCategories).split(",");
                        for (String active : activeCategories) {
                            if (TextUtils.equals(active, category)) {
                                return false;
                            }
                        }
                        return true;

                    case NotificationManager.INTERRUPTION_FILTER_UNKNOWN:
                    case NotificationManager.INTERRUPTION_FILTER_ALL:
                    default:
                        return false;
                }
            }
            return false;
        }



        @DexAdd
        private static final Object fileLock;

        static {
            fileLock = new Object();
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        /* JADX WARNING: Code restructure failed: missing block: B:5:0x003c, code lost:
            if (r0 != null) goto L_0x004b;
         */
		@DexEdit
		public d(StatusBarNotification statusBarNotification, @DexIgnore Void tag) {
		    throw null;
		    // this();
            // CharSequence charSequence;
            // String str = null;
            // CharSequence charSequence2;
            // jr4.b(statusBarNotification, "sbn");
            // this.a = a((long) statusBarNotification.getId(), statusBarNotification.getTag());
            // String packageName = statusBarNotification.getPackageName();
            // jr4.a((Object) packageName, "sbn.packageName");
            // this.c = packageName;
            // b bVar = DianaNotificationComponent.l.a().get(this.c);
            // if (bVar != null) {
            //     DianaNotificationObj.AApplicationName a2 = bVar.a();
            //     if (a2 != null) {
            //         charSequence = a2.getAppName();
            //     }
            // }
            // charSequence = PortfolioApp.Z.c().f(this.c);
            // CharSequence charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            // this.d = (charSequence3 != null ? charSequence3 : charSequence).toString();
            // this.e = a(this.d);
            // CharSequence charSequence4 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            // if (!(charSequence4 == null || pt4.a(charSequence4))) {
            //     Notification notification = statusBarNotification.getNotification();
            //     if (notification != null) {
            //         Bundle bundle = notification.extras;
            //         if (bundle != null) {
            //             charSequence2 = bundle.getCharSequence("android.bigText");
            //             str = a(String.valueOf(charSequence2));
            //         }
            //     }
            //     charSequence2 = null;
            //     str = a(String.valueOf(charSequence2));
            // } else {
            //     String charSequence5 = statusBarNotification.getNotification().extras.getCharSequence("android.text");
            //     str = a((charSequence5 == null ? "" : charSequence5));
            // }
            // this.f = str;
            // this.h = c(statusBarNotification);
            // this.i = b(statusBarNotification);
            // this.g = statusBarNotification.getNotification().when;
            // this.j = statusBarNotification.getNotification().priority;
            // this.k = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            // if (a(statusBarNotification)) {
            //     CharSequence charSequence6 = statusBarNotification.getNotification().tickerText;
            //     if (charSequence6 != null) {
            //         int length = charSequence6.length();
            //         int i2 = 0;
            //         while (true) {
            //             if (i2 >= length) {
            //                 i2 = -1;
            //                 break;
            //             } else if (jr4.a((Object) String.valueOf(charSequence6.charAt(i2)), (Object) ":")) {
            //                 break;
            //             } else {
            //                 i2++;
            //             }
            //         }
            //         if (i2 != -1) {
            //             String obj = charSequence6.subSequence(0, i2).toString();
            //             if (obj != null) {
            //                 this.e = StringsKt__StringsKt.d(obj);
            //                 String obj2 = charSequence6.subSequence(i2 + 1, charSequence6.length()).toString();
            //                 if (obj2 != null) {
            //                     this.f = StringsKt__StringsKt.d(obj2);
            //                     return;
            //                 }
            //                 throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            //             }
            //             throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
            //         }
            //     }
            // }
        }

        @DexAdd
        public d(StatusBarNotification statusBarNotification) {
            this(statusBarNotification, (Void) null);

            String packageName = this.c;

            Context context = PortfolioApp.Z.c().getApplicationContext();
            File iconFile = new File(context.getExternalFilesDir("appIcons"), packageName + ".icon");

            synchronized (fileLock) {
                if (!iconFile.exists()) {
                    Bitmap bitmap = null;
                    try {
                        PackageManager manager = context.getPackageManager();
                        Resources resources = manager.getResourcesForApplication(packageName);
                        // Icon small = statusBarNotification.getNotification().getSmallIcon();
                        Drawable drawable = resources.getDrawable(statusBarNotification.getNotification().icon);


                        int notifIconWidth = 24;
                        int notifIconHeight = 24;


                        byte[] notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0, false);
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xff000000, false);
                        }
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xffffffff, true);
                        }
                        if (notifIcon != null) {

                            // Convert to RLE with header and footer
                            byte[] rle = rle_encode(notifIcon);
                            notifIcon = new byte[rle.length + 4];
                            notifIcon[0] = (byte) notifIconWidth;
                            notifIcon[1] = (byte) notifIconHeight;
                            notifIcon[notifIcon.length - 1] = (byte) 0xff;
                            notifIcon[notifIcon.length - 2] = (byte) 0xff;
                            System.arraycopy(rle, 0, notifIcon, 2, rle.length);

                            // Write to file for next time

                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(iconFile));
                            bos.write(notifIcon);
                            bos.flush();
                            bos.close();
                        }

                        // Send Filters to watch
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        List<AppWrapper> allApps = notificationAppHelper.a(context);
                        List<AppWrapper> apps = new ArrayList<>();
                        for (AppWrapper a: allApps) {
                            InstalledApp installedApp2 = a.getInstalledApp();
                            if (installedApp2 != null) {
                                Boolean isSelected = installedApp2.isSelected();
                                // wd4.a(isSelected, "it.isSelected");
                                if (isSelected) {
                                    apps.add(a);
                                }
                            }
                        }

                        List<AppNotificationFilter> j = new ArrayList<>();
                        // From NotificationAppsPresenter, search for "setRuleToDevice, showProgressDialog"
                        //noinspection CollectionAddAllCanBeReplacedWithConstructor
                        j.addAll(b94.a(apps, false));
                        List<AppNotificationFilter> phone_sms_filters = notificationAppHelper.a(0, 0);
                        j.addAll(phone_sms_filters);
                        PortfolioApp.Z.c().b(new AppNotificationFilterSettings(j, System.currentTimeMillis()), PortfolioApp.Z.c().e());

                    } catch (PackageManager.NameNotFoundException | NullPointerException | IOException | IllegalArgumentException e) {
                        e.printStackTrace();
                    } finally {
                        if (bitmap != null) {
                            bitmap.recycle();
                        }
                    }
                }
            }

            this.dnd = checkDND(statusBarNotification.getNotification());

        }

        @DexIgnore
        public final String a(String str) {
            String replace = new Regex("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final String a(long j2, String str) {
            return j2 + ':' + str;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class e extends d {
        @DexIgnore
        public String l;

        @DexIgnore
        public e() {
        }

        @DexIgnore
        public final e clone() {
            e eVar = new e();
            eVar.c(d());
            eVar.a(a());
            eVar.b(b());
            eVar.d(e());
            eVar.e(f());
            eVar.f(g());
            eVar.a(h());
            eVar.a(i());
            eVar.c(k());
            eVar.b(j());
            eVar.l = this.l;
            return eVar;
        }

        @DexIgnore
        public final String l() {
            return this.l;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public e(String str, String str2, String str3, String str4, Date date) {
            this();
            jr4.b(str, "packageName");
            jr4.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            jr4.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            jr4.b(str4, "message");
            jr4.b(date, "startDate");
            c(a(date.getTime(), null));
            b(str);
            f(str2);
            d(str2);
            e(str4);
            a(date.getTime());
            this.l = str3;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class f extends d {
        @DexIgnore
        public f() {
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public f(String str, String str2, long j) {
            this();
            jr4.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            jr4.b(str2, "message");
            c(a(j, null));
            b(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            f(str);
            d(str);
            e(str2);
            a(j);
        }
    }

    /*
    static {
        HashMap<String, g> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new g("com.facebook.orca", "SMS"));
        k = hashMap;
    }
    */

    @DexIgnore
    public DianaNotificationComponent(h23 h23, DNDSettingsDatabase dNDSettingsDatabase, NotificationSettingsDatabase notificationSettingsDatabase) {
        jr4.b(h23, "mSharedPreferencesManager");
        jr4.b(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        jr4.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.g = h23;
        this.h = dNDSettingsDatabase;
        this.i = notificationSettingsDatabase;
        String simpleName = DianaNotificationComponent.class.getSimpleName();
        jr4.a((Object) simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
        l.a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0104, code lost:
        if (r6 < r0) goto L_0x012d;
     */
    @DexIgnore
    public final boolean c() {
        if (this.g.E()) {
            Calendar instance = Calendar.getInstance();
            jr4.a(instance, "Calendar.getInstance()");
            Date n = sz2.n(instance.getTime());
            jr4.a(n, "DateHelper.getStartOfDay\u2026endar.getInstance().time)");
            long time = n.getTime();
            Calendar instance2 = Calendar.getInstance();
            jr4.a(instance2, "Calendar.getInstance()");
            Date time2 = instance2.getTime();
            jr4.a(time2, "Calendar.getInstance().time");
            long time3 = (time2.getTime() - time) / ((long) 60000);
            List<DNDScheduledTimeModel> listDNDScheduledTimeModel = this.h.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
            if (!listDNDScheduledTimeModel.isEmpty()) {
                for (DNDScheduledTimeModel next : listDNDScheduledTimeModel) {
                    int component2 = next.component2();
                    if (next.component3() == 0) {
                        this.c = component2;
                    } else {
                        this.d = component2;
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "currentMinutesOfDay = " + a((int) time3) + " -- mDNDStartTime = " + a(this.c) + " -- mDNDEndTime = " + a(this.d));
            int i2 = this.d;
            int i3 = this.c;
            if (i2 > i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime greater than mDNDStartTime");
                long j2 = (long) this.d;
                if (((long) this.c) <= time3 && j2 >= time3) {
                    FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                    return true;
                }
            } else if (i2 < i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime smaller than mDNDStartTime");
                long j3 = (long) DateTimeConstants.MINUTES_PER_DAY;
                if (((long) this.c) > time3 || j3 < time3) {
                    long j4 = (long) this.d;
                    if (0 <= time3) {
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            } else if (i2 == i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime equals mDNDStartTime");
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is not valid - show notification ");
        return false;
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ List<d> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.o();
        @DexIgnore
        public /* final */ h23 c;

        @DexIgnore
        public c(h23 h23) {
            jr4.b(h23, "mSharedPref");
            this.c = h23;
        }

        @DexIgnore
        public final void a(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.c(this.b);
        }

        @DexIgnore
        public final int a() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        /* JADX WARNING: type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
            return r11;
         */
        @DexIgnore
        public final synchronized d a(d dVar) {
            d dVar2;
            boolean z;
            jr4.b(dVar, "notificationStatus");
            if (this.a.contains(dVar)) {
                dVar = null;
            } else if (pt4.b(dVar.b(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.Z.c()), true)) {
                Iterator<DianaNotificationComponent.d> it = this.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        dVar2 = null;
                        break;
                    }
                    dVar2 = it.next();
                    d dVar3 = dVar2;
                    if (!pt4.b(dVar3.f(), dVar.f(), true) || (dVar3.h() != dVar.h() && dVar.h() - dVar3.h() > 1)) {
                        z = false;
                        // continue;
                    } else {
                        z = true;
                        // continue;
                    }
                    if (z) {
                        break;
                    }
                }
                d dVar4 = dVar2;
                if (dVar4 != null) {
                    dVar4.c(dVar.d());
                    dVar4.a(dVar.h());
                    return null;
                }
                dVar.a(a());
                this.a.add(dVar);
                a(a() + 1);
            } else {
                dVar.a(a());
                this.a.add(dVar);
                a(a() + 1);
            }
            throw null;
        }

        @DexIgnore
        public final synchronized List<d> a(String str, String str2) {
            ArrayList arrayList;
            jr4.b(str, "realId");
            jr4.b(str2, "packageName");
            List<d> list = this.a;
            arrayList = new ArrayList();
            for (Object next : list) {
                d dVar = (d) next;
                if (jr4.a((Object) dVar.d(), (Object) str) && jr4.a((Object) dVar.b(), (Object) str2)) {
                    arrayList.add(next);
                }
            }
            this.a.removeAll(arrayList);
            return arrayList;
        }
    }

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g {
        @DexIgnore
        public String a;

        @DexIgnore
        public g() {
            this.a = "";
        }

        @DexIgnore
        public final boolean a(String str) {
            jr4.b(str, "realId");
            return StringsKt__StringsKt.a(str, this.a, true);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public g(String str, String str2) {
            this();
            jr4.b(str, "packageName");
            jr4.b(str2, "tag");
            this.a = str2;
        }
    }

    @DexIgnore
    public final synchronized void d(d dVar) {
        jr4.b(dVar, "notification");
        b bVar = l.a().get(dVar.b());
        if (bVar == null || (!dVar.j() && dVar.k())) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved() not supported............");
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "  Notification Posted: " + dVar.b());
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.d(str2, "  Id: " + dVar.d());
            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
            String str3 = this.a;
            local3.d(str3, "  Sender: " + dVar.e());
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str4 = this.a;
            local4.d(str4, "  Title: " + dVar.g());
            ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
            String str5 = this.a;
            local5.d(str5, "  Text: " + dVar.f());
            ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
            String str6 = this.a;
            local6.d(str6, "  Summary: " + dVar.k());
            ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
            String str7 = this.a;
            local7.d(str7, "  IsOngoing: " + dVar.j());
            ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
            String str8 = this.a;
            local8.d(str8, "  When: " + dVar.h());
            ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
            String str9 = this.a;
            local9.d(str9, "  IsDownloadEvent: " + dVar.i());
        } else {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved() supported............");
            ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
            String str10 = this.a;
            local10.d(str10, "  Notification Posted: " + dVar.b());
            ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
            String str11 = this.a;
            local11.d(str11, "  Id: " + dVar.d());
            ILocalFLogger local12 = FLogger.INSTANCE.getLocal();
            String str12 = this.a;
            local12.d(str12, "  Sender: " + dVar.e());
            ILocalFLogger local13 = FLogger.INSTANCE.getLocal();
            String str13 = this.a;
            local13.d(str13, "  Title: " + dVar.g());
            ILocalFLogger local14 = FLogger.INSTANCE.getLocal();
            String str14 = this.a;
            local14.d(str14, "  Text: " + dVar.f());
            ILocalFLogger local15 = FLogger.INSTANCE.getLocal();
            String str15 = this.a;
            local15.d(str15, "  Summary: " + dVar.k());
            ILocalFLogger local16 = FLogger.INSTANCE.getLocal();
            String str16 = this.a;
            local16.d(str16, "  IsOngoing: " + dVar.j());
            ILocalFLogger local17 = FLogger.INSTANCE.getLocal();
            String str17 = this.a;
            local17.d(str17, "  When: " + dVar.h());
            ILocalFLogger local18 = FLogger.INSTANCE.getLocal();
            String str18 = this.a;
            local18.d(str18, "  IsDownloadEvent: " + dVar.i());
            for (d dVar2 : this.b.a(dVar.d(), dVar.b())) {
                PortfolioApp.Z.c().a(a(), new DianaNotificationObj(dVar2.a(), NotificationBaseObj.ANotificationType.REMOVED, bVar.a(), dVar2.g(), dVar2.e(), dVar2.f(), ap4.d(NotificationBaseObj.ANotificationFlag.IMPORTANT)));
            }
        }
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final ew4 b(StatusBarNotification statusBarNotification) {
        jr4.b(statusBarNotification, "sbn");
        throw null;
        // return zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processAppNotification$Anon1(this, statusBarNotification, (wp4) null), 3, (Object) null);
    }

    @DexIgnore
    public final void b(d dVar) {
        Object obj;
        FLogger.INSTANCE.getLocal().d(this.a, "handleNotificationAdded " + dVar);
        b bVar = l.a().get(dVar.b());
        List allAppFilters = g23.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        boolean z = false;
        NotificationBaseObj.ANotificationType aNotificationType = null;
        if (bVar == null) {
            jr4.a(allAppFilters, "appNotificationFilters");
            Iterator it = allAppFilters.iterator();
            while (true) {
                if (!it.hasNext()) {
                    obj = null;
                    break;
                }
                obj = it.next();
                AppFilter appFilter = (AppFilter) obj;
                jr4.a(appFilter, "it");
                if (jr4.a((Object) appFilter.getType(), (Object) dVar.b())) {
                    break;
                }
            }
            if (obj == null) {
                FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
                a(dVar, (NotificationBaseObj.ANotificationType) null);
                return;
            }
            FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but not in list icon supported");
            bVar = new b(17, new DianaNotificationObj.AApplicationName(PortfolioApp.Z.c().f(dVar.b()), dVar.b(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
        } else {
            z = true;
        }
        if ((dVar.j() || !dVar.k()) && a(dVar, bVar) && !dVar.i()) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
            a(dVar, bVar.a().getNotificationType());
            a(dVar, bVar, z);
            return;
        }
        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
        DianaNotificationObj.AApplicationName a2 = bVar.a();
        if (a2 != null) {
            aNotificationType = a2.getNotificationType();
        }
        a(dVar, aNotificationType);
    }

    @DexIgnore
    public final String a() {
        return PortfolioApp.Z.c().e();
    }

    @DexIgnore
    public final ew4 a(String str, Date date, PhoneStateEnum phoneStateEnum) {
        jr4.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        jr4.b(date, "startTime");
        jr4.b(phoneStateEnum, Constants.STATE);
        throw null;
        // return zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processPhoneCall$Anon1(this, phoneStateEnum, str, date, (wp4) null), 3, (Object) null);
    }

    @DexIgnore
    public final ew4 a(String str, String str2, long j2) {
        jr4.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        jr4.b(str2, "content");
        throw null;
        // return zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$processSmsMms$Anon1(this, str, str2, j2, (wp4) null), 3, (Object) null);
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final ew4 a(StatusBarNotification statusBarNotification) {
        jr4.b(statusBarNotification, "sbn");
        throw null;
        // return zt4.b(zu4.a(mv4.a()), (CoroutineContext) null, (CoroutineStart) null, new DianaNotificationComponent$onNotificationAppRemoved$Anon1(this, statusBarNotification, (wp4) null), 3, (Object) null);
    }

    @DexIgnore
    public final boolean a(d dVar, b bVar) {
        return bVar.b() || dVar.c() > -2;
    }

    @DexIgnore
    public final boolean a(String str, boolean z) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.i.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(z);
        boolean z2 = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z2 = a(str);
                } else if (i2 == 2) {
                    z2 = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowCallOrMessageEvent() - from sender " + str + " is " + z2 + " because type = type = " + i2);
        return z2;
    }

    @DexIgnore
    public final boolean a(String str) {
        List<ContactGroup> allContactGroups = g23.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactWithPhoneNumber : allContactGroups) {
            if (contactWithPhoneNumber.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    /* JADX WARNING: type inference failed for: r2v3, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0132  */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    public final void a(d dVar) { // sending new notification
        boolean z;
        b bVar;
        NotificationBaseObj.ANotificationType aNotificationType;
        Object obj;
        FLogger.INSTANCE.getLocal().d(this.a, "handleNewLogic " + dVar);
        Ref$ObjectRef ref$ObjectRef = new Ref$ObjectRef();
        boolean z2 = true;
        if (jr4.a((Object) dVar.b(), (Object) Telephony.Sms.getDefaultSmsPackage(PortfolioApp.Z.c()))) {
            g gVar = k.get(dVar.b());
            if (gVar == null || gVar.a(dVar.d())) {
                ref$ObjectRef.element = l.a().get(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
                z = true;
                if (ref$ObjectRef.element == null) {
                    List allAppFilters = g23.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                    jr4.a(allAppFilters, "appNotificationFilters");
                    Iterator it = allAppFilters.iterator();
                    while (true) {
                        aNotificationType = null;
                        if (!it.hasNext()) {
                            obj = null;
                            break;
                        }
                        obj = it.next();
                        AppFilter appFilter = (AppFilter) obj;
                        jr4.a(appFilter, "it");
                        if (jr4.a((Object) appFilter.getType(), (Object) dVar.b())) {
                            break;
                        }
                    }
                    if (obj == null) {
                        FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
                        b bVar2 = (DianaNotificationComponent.b) ref$ObjectRef.element;
                        if (bVar2 != null) {
                            DianaNotificationObj.AApplicationName a2 = bVar2.a();
                            if (a2 != null) {
                                aNotificationType = a2.getNotificationType();
                            }
                        }
                        a(dVar, aNotificationType);
                        return;
                    }
                    FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but no supported icon");
                    ref$ObjectRef.element = new b(17, new DianaNotificationObj.AApplicationName(PortfolioApp.Z.c().f(dVar.b()), dVar.b(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
                    z2 = false;
                }
                bVar = (DianaNotificationComponent.b) ref$ObjectRef.element;
                if (bVar != null) {
                    return;
                }
                if (jr4.a(bVar.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL()) || jr4.a(bVar.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL())) {
                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() incoming call or miss called not supported............");
                    return;
                }
                if (z && a(dVar, (DianaNotificationComponent.b) ref$ObjectRef.element) && c(dVar.e())) {
                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported - handle for sms - new solution");
                    a(dVar, (DianaNotificationComponent.b) ref$ObjectRef.element, z2);
                } else if (z || ((!dVar.j() && dVar.k()) || !a(dVar, (DianaNotificationComponent.b) ref$ObjectRef.element) || dVar.i())) {
                    if (jr4.a((Object) dVar.b(), (Object) "com.google.android.googlequicksearchbox")) {
                        a(dVar, (DianaNotificationComponent.b) ref$ObjectRef.element, z2);
                    }
                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
                } else {
                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
                    a(dVar, (DianaNotificationComponent.b) ref$ObjectRef.element, z2);
                }
                a(dVar, bVar.a().getNotificationType());
                return;
            }
            ref$ObjectRef.element = l.a().get(dVar.b());
        } else {
            ref$ObjectRef.element = l.a().get(dVar.b());
        }
        z = false;
        if (ref$ObjectRef.element == null) {
        }
        bVar = (DianaNotificationComponent.b) ref$ObjectRef.element;
        if (bVar != null) {
        }
    }

    @DexIgnore
    public final boolean b(String str) {
        List<ContactGroup> allContactGroups = g23.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            jr4.a(contactGroup, "contactGroup");
            Object obj = contactGroup.getContacts().get(0);
            jr4.a(obj, "contactGroup.contacts[0]");
            if (pt4.b(((Contact) obj).getDisplayName(), str, true)) {
                return true;
            }
        }
        return false;
    }

    @DexIgnore
    public final synchronized void c(d dVar) {
        if (this.g.M()) {
            a(dVar);
        } else {
            b(dVar);
        }
    }

    @DexWrap
    public final void b(d dVar, b bVar, boolean z) {
        if (TextUtils.isEmpty(dVar.f())) {
            // FLogger.INSTANCE.getLocal().d(this.a, "Skip this notification from " + dVar.b() + " since content is empty");
            dVar.f = " ";
            // return;
        }

        b(dVar, bVar, z);
    }

    // @DexIgnore
    // public final void b(d dVar, b bVar, boolean z) {
    //     String str;
    //     String str2;
    //     NotificationBaseObj.ANotificationFlag aNotificationFlag;
    //     FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - isSupportedIcon " + z + " appName " + bVar.a().getAppName() + " sender = " + dVar.e() + " - text = " + dVar.f());
    //     if (TextUtils.isEmpty(dVar.f())) {
    //         FLogger.INSTANCE.getLocal().d(this.a, "Skip this notification from " + dVar.b() + " since content is empty");
    //         return;
    //     }
    //     if (z) {
    //         str = dVar.e();
    //     } else {
    //         str = bVar.a().getAppName();
    //     }
    //     String str3 = str;
    //     if (z) {
    //         str2 = dVar.f();
    //     } else {
    //         str2 = dVar.e() + ':' + dVar.f();
    //     }
    //     String str4 = str2;
    //     if (!DeviceHelper.o.l() || !jr4.a((Object) dVar.b(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName())) {
    //         aNotificationFlag = NotificationBaseObj.ANotificationFlag.IMPORTANT;
    //     } else {
    //         aNotificationFlag = NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACTION;
    //     }
    //     PortfolioApp.Z.c().a(a(), new DianaNotificationObj(dVar.a(), bVar.a().getNotificationType(), bVar.a(), dVar.g(), str3, str4, ap4.d(aNotificationFlag)));
    // }

    @DexIgnore
    public final boolean c(String str) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.i.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        boolean z = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z = b(str);
                } else if (i2 == 2) {
                    z = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowMessageFromContactName() - from sender " + str + " is " + z + ", type = type = " + i2);
        return z;
    }

    /* JADX WARNING: type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARNING: type inference failed for: r2v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x008e, code lost:
        if (r0 != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0090, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b3, code lost:
        if (r0 != null) goto L_0x0090;
     */
    @DexIgnore
    public final String d(String str) {
        if (d94.a.e(PortfolioApp.Z.c())) {
            Cursor cursor = null;
            try {
                cursor = PortfolioApp.Z.c().getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name"}, null, null, null);
                if (cursor == null || cursor.getCount() <= 0) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.a;
                    local.d(str2, "readDisplayNameFromPhoneNumber(), number=" + str + ", no display name.");
                } else {
                    cursor.moveToFirst();
                    String string = cursor.getString(0);
                    jr4.a((Object) string, "c.getString(0)");
                    try {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = this.a;
                        local2.d(str3, "readDisplayNameFromPhoneNumber(), number=" + str + ", displayName=" + string);
                        str = string;
                    } catch (Exception e2) {
                        str = string;
                        try {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str4 = this.a;
                            local3.d(str4, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e2);
                        } catch (Throwable th) {
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                }
            } catch (Exception e3) {
                ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                String str42 = this.a;
                local32.d(str42, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e3);
            }
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str5 = this.a;
            local4.d(str5, "readDisplayNameFromPhoneNumber(), number=" + str + ", no read contact permission.");
        }
        return str;
    }

    @DexIgnore
    public final void b(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            PhoneCallObserver phoneCallObserver = this.f;
            if (phoneCallObserver != null) {
                context.getContentResolver().unregisterContentObserver(phoneCallObserver);
                this.f = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexWrap
    public final void a(d dVar, b bVar, boolean z) {
        if (dVar.dnd) {
            FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() - skipped for DND.");
        } else {
            a(dVar, bVar, z);
        }
    }


    // @DexIgnore
    // public final void a(d dVar, b bVar, boolean z) {
    //     d a2 = this.b.a(dVar);
    //     if (a2 == null) {
    //         FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() - this notification existed.");
    //     } else if (c()) {
    //     } else {
    //         // if (!pt4.a(a2.e()) || !pt4.a(a2.f())) {
    //             b(a2, bVar, z);
    //         // }
    //     }
    // }

    @DexIgnore
    public final void a(d dVar, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + dVar.b());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + dVar.d());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + dVar.e());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + dVar.g());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + dVar.f());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + dVar.k());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + dVar.j());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + dVar.h());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + dVar.c());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + dVar.i());
    }

    @DexIgnore
    public final void a(Context context) {
        if (d94.a.d(context)) {
            this.f = new PhoneCallObserver();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            PhoneCallObserver phoneCallObserver = this.f;
            if (phoneCallObserver != null) {
                contentResolver.registerContentObserver(uri, false, phoneCallObserver);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            jr4.a();
            throw null;
        }
    }
}
