package com.portfolio.platform.data.source.local.alarm;

import android.os.Parcel;
import android.os.Parcelable;
import com.fossil.er4;
import com.fossil.jr4;
import com.fossil.pe2;
import com.fossil.re2;
import com.portfolio.platform.data.model.Explore;
import java.util.Arrays;
import kotlin.TypeCastException;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore
public final class Alarm implements Parcelable {
    @DexIgnore
    public static /* final */ CREATOR CREATOR; // = new CREATOR((er4) null);
    @DexIgnore
    @re2("createdAt")
    public String createdAt;
    @DexIgnore
    @re2("days")
    public int[] days;
    @DexIgnore
    @re2("hour")
    public int hour;
    @DexIgnore
    @re2("id")
    public String id;
    @DexIgnore
    @re2("isActive")
    public boolean isActive;
    @DexIgnore
    @re2("isRepeated")
    public boolean isRepeated;
    @DexIgnore
    @re2("minute")
    public int minute;
    @DexIgnore
    @pe2
    public int pinType;
    @DexIgnore
    @re2("title")
    public String title;
    @DexIgnore
    @re2("updatedAt")
    public String updatedAt;
    @DexIgnore
    @re2("uri")
    public String uri;

    @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class CREATOR implements Parcelable.Creator<Alarm> {
        @DexIgnore
        public CREATOR() {
        }

        @DexIgnore
        public /* synthetic */ CREATOR(er4 er4) {
            this();
        }

        @DexIgnore
        public Alarm createFromParcel(Parcel parcel) {
            jr4.b(parcel, "parcel");
            return new Alarm(parcel);
        }

        @DexIgnore
        public Alarm[] newArray(int i) {
            return new Alarm[i];
        }
    }

    @DexIgnore
    public Alarm(String str, String str2, String str3, int i, int i2, int[] iArr, boolean z, boolean z2, String str4, String str5, int i3) {
        jr4.b(str2, "uri");
        jr4.b(str3, Explore.COLUMN_TITLE);
        jr4.b(str5, "updatedAt");
        this.id = str;
        this.uri = str2;
        this.title = str3;
        this.hour = i;
        this.minute = i2;
        this.days = iArr;
        this.isActive = z;
        this.isRepeated = z2;
        this.createdAt = str4;
        this.updatedAt = str5;
        this.pinType = i3;
    }

    @DexIgnore
    public static /* synthetic */ Alarm copy$default(Alarm alarm, String str, String str2, String str3, int i, int i2, int[] iArr, boolean z, boolean z2, String str4, String str5, int i3, int i4, Object obj) {
        Alarm alarm2 = alarm;
        int i5 = i4;
        return alarm.copy((i5 & 1) != 0 ? alarm2.id : str, (i5 & 2) != 0 ? alarm2.uri : str2, (i5 & 4) != 0 ? alarm2.title : str3, (i5 & 8) != 0 ? alarm2.hour : i, (i5 & 16) != 0 ? alarm2.minute : i2, (i5 & 32) != 0 ? alarm2.days : iArr, (i5 & 64) != 0 ? alarm2.isActive : z, (i5 & 128) != 0 ? alarm2.isRepeated : z2, (i5 & 256) != 0 ? alarm2.createdAt : str4, (i5 & 512) != 0 ? alarm2.updatedAt : str5, (i5 & 1024) != 0 ? alarm2.pinType : i3);
    }

    @DexIgnore
    public final String component1() {
        return this.id;
    }

    @DexIgnore
    public final String component10() {
        return this.updatedAt;
    }

    @DexIgnore
    public final int component11() {
        return this.pinType;
    }

    @DexIgnore
    public final String component2() {
        return this.uri;
    }

    @DexIgnore
    public final String component3() {
        return this.title;
    }

    @DexIgnore
    public final int component4() {
        return this.hour;
    }

    @DexIgnore
    public final int component5() {
        return this.minute;
    }

    @DexIgnore
    public final int[] component6() {
        return this.days;
    }

    @DexIgnore
    public final boolean component7() {
        return this.isActive;
    }

    @DexIgnore
    public final boolean component8() {
        return this.isRepeated;
    }

    @DexIgnore
    public final String component9() {
        return this.createdAt;
    }

    @DexIgnore
    public final Alarm copy(String str, String str2, String str3, int i, int i2, int[] iArr, boolean z, boolean z2, String str4, String str5, int i3) {
        jr4.b(str2, "uri");
        String str6 = str3;
        jr4.b(str6, Explore.COLUMN_TITLE);
        String str7 = str5;
        jr4.b(str7, "updatedAt");
        return new Alarm(str, str2, str6, i, i2, iArr, z, z2, str4, str7, i3);
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!jr4.a((Object) Alarm.class, (Object) obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            Alarm alarm = (Alarm) obj;
            return !(jr4.a((Object) this.id, (Object) alarm.id) ^ true) && !(jr4.a((Object) this.uri, (Object) alarm.uri) ^ true);
        }
        throw new TypeCastException("null cannot be cast to non-null type com.portfolio.platform.data.source.local.alarm.Alarm");
    }

    @DexIgnore
    public final String getCreatedAt() {
        return this.createdAt;
    }

    @DexIgnore
    public final int[] getDays() {
        return this.days;
    }

    @DexIgnore
    public final int getHour() {
        return this.hour;
    }

    @DexIgnore
    public final String getId() {
        return this.id;
    }

    @DexIgnore
    public final long getMillisecond() {
        return ((long) ((this.minute + (this.hour * 60)) * 60)) * 1000;
    }

    @DexIgnore
    public final int getMinute() {
        return this.minute;
    }

    @DexIgnore
    public final int getPinType() {
        return this.pinType;
    }

    @DexIgnore
    public final String getTitle() {
        return this.title;
    }

    @DexIgnore
    public final int getTotalMinutes() {
        return this.minute + (this.hour * 60);
    }

    @DexIgnore
    public final String getUpdatedAt() {
        return this.updatedAt;
    }

    @DexIgnore
    public final String getUri() {
        return this.uri;
    }

    @DexIgnore
    public int hashCode() {
        String str = this.id;
        return ((str != null ? str.hashCode() : 0) * 31) + this.uri.hashCode();
    }

    @DexIgnore
    public final boolean isActive() {
        return this.isActive;
    }

    @DexIgnore
    public final boolean isRepeated() {
        return this.isRepeated;
    }

    @DexIgnore
    public final void setActive(boolean z) {
        this.isActive = z;
    }

    @DexIgnore
    public final void setCreatedAt(String str) {
        this.createdAt = str;
    }

    @DexIgnore
    public final void setDays(int[] iArr) {
        this.days = iArr;
    }

    @DexIgnore
    public final void setHour(int i) {
        this.hour = i;
    }

    @DexIgnore
    public final void setId(String str) {
        this.id = str;
    }

    @DexIgnore
    public final void setMinute(int i) {
        this.minute = i;
    }

    @DexIgnore
    public final void setPinType(int i) {
        this.pinType = i;
    }

    @DexIgnore
    public final void setRepeated(boolean z) {
        this.isRepeated = z;
    }

    @DexIgnore
    public final void setTitle(String str) {
        jr4.b(str, "<set-?>");
        this.title = str;
    }

    @DexIgnore
    public final void setTotalMinutes(int i) {
        this.hour = i / 60;
        this.minute = i % 60;
    }

    @DexIgnore
    public final void setUpdatedAt(String str) {
        jr4.b(str, "<set-?>");
        this.updatedAt = str;
    }

    @DexIgnore
    public final void setUri(String str) {
        jr4.b(str, "<set-?>");
        this.uri = str;
    }

    @DexIgnore
    public String toString() {
        return "Alarm(id=" + this.id + ", uri=" + this.uri + ", title=" + this.title + ", hour=" + this.hour + ", minute=" + this.minute + ", days=" + Arrays.toString(this.days) + ", isActive=" + this.isActive + ", isRepeated=" + this.isRepeated + ", createdAt=" + this.createdAt + ", updatedAt=" + this.updatedAt + ", pinType=" + this.pinType + ")";
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        jr4.b(parcel, "parcel");
        parcel.writeString(this.id);
        parcel.writeString(this.uri);
        parcel.writeString(this.title);
        parcel.writeInt(this.hour);
        parcel.writeInt(this.minute);
        parcel.writeIntArray(this.days);
        parcel.writeByte(this.isActive ? (byte) 1 : 0);
        parcel.writeByte(this.isRepeated ? (byte) 1 : 0);
        parcel.writeString(this.createdAt);
        parcel.writeString(this.updatedAt);
        parcel.writeInt(this.pinType);
    }

    @DexIgnore
    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Alarm(String str, String str2, String str3, int i, int i2, int[] iArr, boolean z, boolean z2, String str4, String str5, int i3, int i4, er4 er4) {
        this(str, str2, str3, i, i2, iArr, z, z2, str4, str5, (i4 & 1024) != 0 ? 1 : i3);
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public Alarm(Parcel parcel) {
        throw null;
        // this(r2, r3, r4, r5, r6, r7, r0, r9, r10, r11, parcel.readInt());
        // String str;
        // String str2;
        // String str3;
        // jr4.b(parcel, "parcel");
        // String readString = parcel.readString();
        // String readString2 = parcel.readString();
        // String str4 = readString2 != null ? readString2 : "";
        // String readString3 = parcel.readString();
        // if (readString3 != null) {
        //     str = readString3;
        // } else {
        //     str = "";
        // }
        // int readInt = parcel.readInt();
        // int readInt2 = parcel.readInt();
        // int[] createIntArray = parcel.createIntArray();
        // byte b = (byte) 0;
        // boolean z = parcel.readByte() != b;
        // boolean z2 = parcel.readByte() != b;
        // String readString4 = parcel.readString();
        // if (readString4 != null) {
        //     str2 = readString4;
        // } else {
        //     str2 = "";
        // }
        // String readString5 = parcel.readString();
        // if (readString5 != null) {
        //     str3 = readString5;
        // } else {
        //     str3 = "";
        // }
    }
}
