package com.fossil;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseArray;
import com.fossil.tj4;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wearables.fsl.shared.BaseFeatureModel;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.FNotification;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.SKUModel;
import com.portfolio.platform.data.model.setting.SpecialSkuSetting;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationAppHelper {
    @DexIgnore
    public static /* final */ String a;
    @DexIgnore
    public static /* final */ NotificationAppHelper b; // = new NotificationAppHelper();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /*
    static {
        String simpleName = NotificationAppHelper.class.getSimpleName();
        wg6.a((Object) simpleName, "NotificationAppHelper::class.java.simpleName");
        a = simpleName;
    }
    */

    @DexIgnore
    public final List<AppFilter> a(MFDeviceFamily mFDeviceFamily) {
        List<AppFilter> allAppFilters = mFDeviceFamily != null ? zm4.p.a().a().getAllAppFilters(mFDeviceFamily.ordinal()) : null;
        return allAppFilters == null ? new ArrayList() : allAppFilters;
    }

    @DexIgnore
    public final AppFilter a(String str, MFDeviceFamily mFDeviceFamily) {
        wg6.b(str, "type");
        if (TextUtils.isEmpty(str) || mFDeviceFamily == null) {
            return null;
        }
        return zm4.p.a().a().getAppFilterMatchingType(str, mFDeviceFamily.ordinal());
    }

    @DexIgnore
    public final boolean a(String str) {
        wg6.b(str, "type");
        return a(str, MFDeviceFamily.DEVICE_FAMILY_SAM) != null;
    }

    @DexIgnore
    public final Object a(d15 d15, mz4 mz4, NotificationSettingsDatabase notificationSettingsDatabase, an4 an4, xe6<? super List<AppNotificationFilter>> xe6) {
        throw null;
        // return gk6.a(zl6.a(), new vx5$a(an4, notificationSettingsDatabase, mz4, d15, null), xe6);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [android.util.SparseArray<java.util.List<com.fossil.wearables.fsl.shared.BaseFeatureModel>>, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.contact.Contact, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final AppNotificationFilterSettings a(SparseArray<List<BaseFeatureModel>> sparseArray, boolean z) {
        throw null;
        // String str;
        // ArrayList arrayList = new ArrayList();
        // if (sparseArray != null) {
        //     SparseArray<List<BaseFeatureModel>> clone = sparseArray.clone();
        //     wg6.a((Object) clone, "it.clone()");
        //     int size = clone.size();
        //     short s = -1;
        //     for (int i = 0; i < size; i++) {
        //         List<ContactGroup> valueAt = clone.valueAt(i);
        //         if (valueAt != null) {
        //             for (ContactGroup contactGroup : valueAt) {
        //                 if (contactGroup.isEnabled()) {
        //                     short c = (short) al4.c(contactGroup.getHour());
        //                     int i2 = 10000;
        //                     if (contactGroup instanceof ContactGroup) {
        //                         ContactGroup contactGroup2 = contactGroup;
        //                         if (contactGroup2.getContacts() != null) {
        //                             for (Contact contact : contactGroup2.getContacts()) {
        //                                 wg6.a((Object) contact, "contact");
        //                                 if (contact.isUseCall()) {
        //                                     if (z) {
        //                                         s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForCall();
        //                                     }
        //                                     NotificationHandMovingConfig notificationHandMovingConfig = new NotificationHandMovingConfig(c, c, s, i2);
        //                                     DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
        //                                     AppNotificationFilter appNotificationFilter = new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), "", phone_incoming_call.getNotificationType()));
        //                                     appNotificationFilter.setSender(contact.getDisplayName());
        //                                     appNotificationFilter.setHandMovingConfig(notificationHandMovingConfig);
        //                                     appNotificationFilter.setVibePattern(NotificationVibePattern.CALL);
        //                                     arrayList.add(appNotificationFilter);
        //                                 }
        //                                 if (contact.isUseSms()) {
        //                                     if (z) {
        //                                         s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForSms();
        //                                     }
        //                                     NotificationHandMovingConfig notificationHandMovingConfig2 = new NotificationHandMovingConfig(c, c, s, i2);
        //                                     DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
        //                                     AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType()));
        //                                     appNotificationFilter2.setSender(contact.getDisplayName());
        //                                     appNotificationFilter2.setHandMovingConfig(notificationHandMovingConfig2);
        //                                     appNotificationFilter2.setVibePattern(NotificationVibePattern.TEXT);
        //                                     arrayList.add(appNotificationFilter2);
        //                                 }
        //                                 i2 = 10000;
        //                             }
        //                         }
        //                     } else if (contactGroup instanceof AppFilter) {
        //                         AppFilter appFilter = (AppFilter) contactGroup;
        //                         String type = appFilter.getType();
        //                         if (!(type == null || xj6.a(type))) {
        //                             if (appFilter.getName() == null) {
        //                                 str = "";
        //                             } else {
        //                                 str = appFilter.getName();
        //                             }
        //                             if (z) {
        //                                 s = (short) SpecialSkuSetting.AngleSubeye.MOVEMBER.getAngleForApp();
        //                             }
        //                             NotificationHandMovingConfig notificationHandMovingConfig3 = new NotificationHandMovingConfig(c, c, s, 10000);
        //                             wg6.a((Object) str, "appName");
        //                             String type2 = appFilter.getType();
        //                             wg6.a((Object) type2, "item.type");
        //                             AppNotificationFilter appNotificationFilter3 = new AppNotificationFilter(new FNotification(str, type2, "", NotificationBaseObj.ANotificationType.NOTIFICATION));
        //                             appNotificationFilter3.setHandMovingConfig(notificationHandMovingConfig3);
        //                             appNotificationFilter3.setVibePattern(NotificationVibePattern.DEFAULT_OTHER_APPS);
        //                             arrayList.add(appNotificationFilter3);
        //                         }
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
        // ILocalFLogger local = FLogger.INSTANCE.getLocal();
        // String str2 = a;
        // local.d(str2, "buildAppNotificationFilterSettings size = " + arrayList.size());
        // return new AppNotificationFilterSettings(arrayList, System.currentTimeMillis());
    }

    @DexIgnore
    public final boolean a(SKUModel sKUModel, String str) {
        SpecialSkuSetting.SpecialSku specialSku;
        wg6.b(str, "serial");
        if (sKUModel != null) {
            SpecialSkuSetting.SpecialSku.Companion companion = SpecialSkuSetting.SpecialSku.Companion;
            String sku = sKUModel.getSku();
            if (sku == null) {
                sku = "";
            }
            specialSku = companion.fromType(sku);
        } else {
            specialSku = SpecialSkuSetting.SpecialSku.Companion.fromSerialNumber(str);
        }
        return specialSku == SpecialSkuSetting.SpecialSku.MOVEMBER;
    }

    @DexIgnore
    public final List<AppNotificationFilter> a(Context context, NotificationSettingsDatabase notificationSettingsDatabase, an4 an4) {
        int i;
        wg6.b(context, "context");
        wg6.b(notificationSettingsDatabase, "notificationSettingsDatabase");
        wg6.b(an4, "sharedPrefs");
        ArrayList arrayList = new ArrayList();
        boolean B = an4.B();
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(true);
        int i2 = 0;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData2 = notificationSettingsDatabase.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = a;
        local.d(str, "buildNotificationAppFilters(), callSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData + ", messageSettingsTypeModel = " + notificationSettingsWithIsCallNoLiveData2 + ", isAllAppToggleEnabled = " + B);
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i = notificationSettingsWithIsCallNoLiveData.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowCallsFrom", 0, true));
            i = 0;
        }
        if (notificationSettingsWithIsCallNoLiveData2 != null) {
            i2 = notificationSettingsWithIsCallNoLiveData2.getSettingsType();
        } else {
            notificationSettingsDatabase.getNotificationSettingsDao().insertNotificationSettings(new NotificationSettingsModel("AllowMessagesFrom", 0, false));
        }
        List<AppNotificationFilter> a2 = a(i, i2);
        List<AppWrapper> a3 = a(context);
        arrayList.addAll(a2);
        arrayList.addAll(ux5.a(a3, B));
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.contact.ContactGroup, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final List<AppNotificationFilter> a(int i, int i2) {
        int i3 = i;
        int i4 = i2;
        ArrayList arrayList = new ArrayList();
        List allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (i3 == 0) {
            DianaNotificationObj.AApplicationName phone_incoming_call = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
            arrayList.add(new AppNotificationFilter(new FNotification(phone_incoming_call.getAppName(), phone_incoming_call.getPackageName(), phone_incoming_call.getIconFwPath(), phone_incoming_call.getNotificationType())));
        } else if (i3 == 1) {
            int size = allContactGroups.size();
            for (int i5 = 0; i5 < size; i5++) {
                ContactGroup contactGroup = (ContactGroup) allContactGroups.get(i5);
                DianaNotificationObj.AApplicationName phone_incoming_call2 = DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                FNotification fNotification = new FNotification(phone_incoming_call2.getAppName(), phone_incoming_call2.getPackageName(), phone_incoming_call2.getIconFwPath(), phone_incoming_call2.getNotificationType());
                wg6.a((Object) contactGroup, "item");
                List contacts = contactGroup.getContacts();
                wg6.a((Object) contacts, "item.contacts");
                if (!contacts.isEmpty()) {
                    AppNotificationFilter appNotificationFilter = new AppNotificationFilter(fNotification);
                    Object obj = contactGroup.getContacts().get(0);
                    wg6.a(obj, "item.contacts[0]");
                    appNotificationFilter.setSender(((Contact) obj).getDisplayName());
                    arrayList.add(appNotificationFilter);
                }
            }
        }
        if (i4 == 0) {
            DianaNotificationObj.AApplicationName messages = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
            arrayList.add(new AppNotificationFilter(new FNotification(messages.getAppName(), messages.getPackageName(), messages.getIconFwPath(), messages.getNotificationType())));
        } else if (i4 == 1) {
            int size2 = allContactGroups.size();
            for (int i6 = 0; i6 < size2; i6++) {
                ContactGroup contactGroup2 = (ContactGroup) allContactGroups.get(i6);
                DianaNotificationObj.AApplicationName messages2 = DianaNotificationObj.AApplicationName.Companion.getMESSAGES();
                FNotification fNotification2 = new FNotification(messages2.getAppName(), messages2.getPackageName(), messages2.getIconFwPath(), messages2.getNotificationType());
                wg6.a((Object) contactGroup2, "item");
                List contacts2 = contactGroup2.getContacts();
                wg6.a((Object) contacts2, "item.contacts");
                if (!contacts2.isEmpty()) {
                    AppNotificationFilter appNotificationFilter2 = new AppNotificationFilter(fNotification2);
                    Object obj2 = contactGroup2.getContacts().get(0);
                    wg6.a(obj2, "item.contacts[0]");
                    appNotificationFilter2.setSender(((Contact) obj2).getDisplayName());
                    arrayList.add(appNotificationFilter2);
                }
            }
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.appfilter.AppFilter, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    @DexIgnore
    public final List<AppWrapper> a(Context context) {
        List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        LinkedList linkedList = new LinkedList();
        Iterator<tj4.b> it = tj4.f.f().iterator();
        while (it.hasNext()) {
            tj4.b next = it.next();
            if (TextUtils.isEmpty(next.b()) || !xj6.b(next.b(), context.getPackageName(), true)) {
                InstalledApp installedApp = new InstalledApp(next.b(), next.a(), false);
                Iterator it2 = allAppFilters.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    AppFilter appFilter = (AppFilter) it2.next();
                    wg6.a((Object) appFilter, "appFilter");
                    if (wg6.a((Object) appFilter.getType(), (Object) installedApp.getIdentifier())) {
                        installedApp.setSelected(true);
                        installedApp.setDbRowId(appFilter.getDbRowId());
                        installedApp.setCurrentHandGroup(appFilter.getHour());
                        break;
                    }
                }
                AppWrapper appWrapper = new AppWrapper();
                appWrapper.setInstalledApp(installedApp);
                appWrapper.setUri(next.c());
                appWrapper.setCurrentHandGroup(installedApp.getCurrentHandGroup());
                linkedList.add(appWrapper);
            }
        }
        return linkedList;
    }
}
