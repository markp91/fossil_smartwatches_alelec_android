package com.fossil;

import android.util.SparseIntArray;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.ViewDataBinding;

import com.fossil.wearables.fossil.R;
import com.portfolio.platform.view.FlexibleButton;
import com.portfolio.platform.view.FlexibleSwitchCompat;
import com.portfolio.platform.view.FlexibleTextView;
import com.portfolio.platform.view.RTLImageView;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexReplace;

@DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
public class WatchSettingFragmentMapping extends WatchSettingFragmentBinding {
    @DexIgnore
    public static /* final */ ViewDataBinding.j J; // = null;
    @DexIgnore
    public static /* final */ SparseIntArray K; // = new SparseIntArray();
    @DexIgnore
    public long I;

    /*
    static {
        K.put(2131361929, 1);
        K.put(2131363218, 2);
        K.put(2131362566, 3);
        K.put(2131363139, 4);
        K.put(2131363111, 5);
        K.put(2131361928, 6);
        K.put(2131361932, 7);
        K.put(2131362136, 8);
        K.put(2131362077, 9);
        K.put(2131363162, 10);
        K.put(2131363163, 11);
        K.put(2131362958, 12);
        K.put(2131363154, 13);
        K.put(2131363155, 14); // fw_version_value
        K.put(2131362959, 15);
        K.put(2131363208, 16);
        K.put(2131363209, 17); // serial_value
        K.put(2131363228, 18);
        K.put(2131362230, 19);
        K.put(2131362231, 20);
        K.put(2131362229, 21);
        K.put(2131363152, 22);
        K.put(2131362960, 23);
        K.put(2131363102, 24);
        K.put(2131363103, 25);
        K.put(2131363201, 26);
    }
    */

    static {
        K.put(R.id.tv_key_value, 27);

        K.put(R.id.switch_android_dnd, 28);
        K.put(R.id.switch_android_dnd_high, 29);
        K.put(R.id.switch_enpty_notifications, 30);
        K.put(R.id.switch_auto_sync, 31);
    }

    @DexReplace
    public WatchSettingFragmentMapping(jb jbVar, View view) {
        this(jbVar, view, ViewDataBinding.a(jbVar, view, 32, J, K));
    }

    @DexIgnore
    public void a() {
        synchronized (this) {
            this.I = 0;
        }
    }

    @DexIgnore
    public boolean e() {
        synchronized (this) {
            if (this.I != 0) {
                return true;
            }
            return false;
        }
    }

    @DexIgnore
    public void f() {
        synchronized (this) {
            this.I = 1;
        }
        g();
    }

    @DexReplace
    public WatchSettingFragmentMapping(jb jbVar, View view, Object[] objArr) {
        super(
            jbVar,
            view,
            0,
            (FlexibleButton) objArr[6],
            (RTLImageView) objArr[1],
            (FlexibleButton) objArr[7],
            (ConstraintLayout) objArr[9],
            (CardView) objArr[8],
            (FlexibleButton) objArr[21],
            (FlexibleButton) objArr[19],
            (FlexibleButton) objArr[20],
            (ImageView) objArr[3],
            (ScrollView) objArr[0],
            (View) objArr[12],
            (View) objArr[15],
            (View) objArr[23],
            (FlexibleTextView) objArr[24],
            (FlexibleTextView) objArr[25],
            (FlexibleTextView) objArr[5],
            (FlexibleTextView) objArr[4],
            (FlexibleTextView) objArr[22],
            (FlexibleTextView) objArr[13],
            (FlexibleTextView) objArr[14],
            (FlexibleTextView) objArr[10],
            (FlexibleTextView) objArr[11],
            (FlexibleTextView) objArr[26],
            (FlexibleTextView) objArr[16],
            (FlexibleTextView) objArr[17],
            (FlexibleTextView) objArr[2],
            (FlexibleTextView) objArr[18],
            (FlexibleTextView) objArr[27],
            (FlexibleSwitchCompat) objArr[28],
            (FlexibleSwitchCompat) objArr[29],
            (FlexibleSwitchCompat) objArr[30],
            (FlexibleSwitchCompat) objArr[31]

        );
        this.I = -1;
        super.y.setTag(null);
        a(view);
        f();
    }
}
