package com.fossil;

import android.os.Parcel;
import android.os.Parcelable;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import org.json.JSONException;
import org.json.JSONObject;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class NotificationFilter extends p40 implements Parcelable {
    @DexIgnore
    public static /* final */ a CREATOR; // = new a(null);
    @DexIgnore
    public long a;
    @DexIgnore
    public byte b;
    @DexIgnore
    public String c;
    @DexIgnore
    public short d;
    @DexIgnore
    public NotificationHandMovingConfig e;
    @DexIgnore
    public NotificationIconConfig f;
    @DexIgnore
    public NotificationVibePattern g;
    @DexIgnore
    public /* final */ String h;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a implements Parcelable.Creator {
        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
        }

        @DexIgnore
        public Object createFromParcel(Parcel parcel) {
            return new NotificationFilter(parcel, null);
        }

        @DexIgnore
        public Object[] newArray(int i) {
            return new NotificationFilter[i];
        }
    }

    /*
    static {
        lg6 lg6 = lg6.a;
    }
    */

    @DexIgnore
    public NotificationFilter(String str) {
        this.h = str;
        h51 h51 = com.fossil.h51.a;
        String str2 = this.h;
        Charset f2 = mi0.A.f();
        if (str2 != null) {
            byte[] bytes = str2.getBytes(f2);
            wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
            throw null;
            // this.a = h51.a(md6.a(bytes, (byte) 0), q11.CRC32);
            // this.c = "";
            // this.d = 255;
            // return;
        }
        throw new rc6("null cannot be cast to non-null type java.lang.String");
    }

    @DexIgnore
    public final byte[] a(byte b2, byte[] bArr) {
        if (bArr.length + 2 > 100) {
            return new byte[0];
        }
        byte[] array = ByteBuffer.allocate(bArr.length + 2).order(ByteOrder.LITTLE_ENDIAN).put(b2).put((byte) bArr.length).put(bArr).array();
        wg6.a(array, "ByteBuffer.allocate(HEAD\u2026\n                .array()");
        return array;
    }

    @DexIgnore
    public final byte[] b() throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte b2 = or0.c.a;
        byte[] array = ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt((int) this.a).array();
        wg6.a(array, "ByteBuffer.allocate(4)\n \u2026                 .array()");
        byteArrayOutputStream.write(a(b2, array));
        byteArrayOutputStream.write(a(or0.d.a, new byte[]{this.b}));
        if (this.c.length() > 0) {
            byte b3 = or0.b.a;
            String a2 = cw0.a(this.c);
            Charset f2 = mi0.A.f();
            if (a2 != null) {
                byte[] bytes = a2.getBytes(f2);
                wg6.a(bytes, "(this as java.lang.String).getBytes(charset)");
                byteArrayOutputStream.write(a(b3, bytes));
            } else {
                throw new rc6("null cannot be cast to non-null type java.lang.String");
            }
        }
        short s = this.d;
        if (s != ((short) -1)) {
            byteArrayOutputStream.write(a(or0.f.a, new byte[]{(byte) s}));
        }
        NotificationHandMovingConfig notificationHandMovingConfig = this.e;
        if (notificationHandMovingConfig != null) {
            byteArrayOutputStream.write(a(or0.g.a, notificationHandMovingConfig.b()));
        }
        NotificationVibePattern notificationVibePattern = this.g;
        if (notificationVibePattern != null) {
            byteArrayOutputStream.write(a(or0.h.a, new byte[]{notificationVibePattern.a()}));
        }
        NotificationIconConfig notificationIconConfig = this.f;
        if (notificationIconConfig != null) {
            byteArrayOutputStream.write(a(or0.e.a, notificationIconConfig.c()));
        }
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        byte[] array2 = ByteBuffer.allocate(byteArray.length + 2).order(ByteOrder.LITTLE_ENDIAN).putShort((short) byteArray.length).put(byteArray).array();
        wg6.a(array2, "ByteBuffer\n             \u2026\n                .array()");
        return array2;
    }

    @DexIgnore
    public int describeContents() {
        return 0;
    }

    @DexIgnore
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!wg6.a(NotificationFilter.class, obj != null ? obj.getClass() : null)) {
            return false;
        }
        if (obj != null) {
            NotificationFilter notificationFilter = (NotificationFilter) obj;
            throw null;
            // return this.a == notificationFilter.a && this.b == notificationFilter.b && wg6.a(this.c, notificationFilter.c) == true && this.d == notificationFilter.d && !(wg6.a(this.e, notificationFilter.e) ^ true) && this.g == notificationFilter.g && !(wg6.a(this.f, notificationFilter.f) ^ true);
        }
        throw new rc6("null cannot be cast to non-null type com.fossil.blesdk.device.data.notification.NotificationFilter");
    }

    @DexIgnore
    public final long getAppBundleCrc() {
        return this.a;
    }

    @DexIgnore
    public final String getAppPackageName() {
        return this.h;
    }

    @DexIgnore
    public final NotificationHandMovingConfig getHandMovingConfig() {
        return this.e;
    }

    @DexIgnore
    public final NotificationIconConfig getIconConfig() {
        return this.f;
    }

    @DexIgnore
    public final short getPriority() {
        return this.d;
    }

    @DexIgnore
    public final String getSender() {
        return this.c;
    }

    @DexIgnore
    public final NotificationVibePattern getVibePattern() {
        return this.g;
    }

    @DexIgnore
    public int hashCode() {
        int hashCode = ((this.c.hashCode() + (((Long.valueOf(this.a).hashCode() * 31) + this.b) * 31)) * 31) + this.d;
        NotificationHandMovingConfig notificationHandMovingConfig = this.e;
        if (notificationHandMovingConfig != null) {
            hashCode = (hashCode * 31) + notificationHandMovingConfig.hashCode();
        }
        NotificationVibePattern notificationVibePattern = this.g;
        if (notificationVibePattern != null) {
            hashCode = (hashCode * 31) + notificationVibePattern.hashCode();
        }
        NotificationIconConfig notificationIconConfig = this.f;
        return notificationIconConfig != null ? (hashCode * 31) + notificationIconConfig.hashCode() : hashCode;
    }

    @DexIgnore
    public final NotificationFilter setHandMovingConfig(NotificationHandMovingConfig notificationHandMovingConfig) {
        this.e = notificationHandMovingConfig;
        return this;
    }

    @DexIgnore
    public final NotificationFilter setIconConfig(NotificationIconConfig notificationIconConfig) {
        this.f = notificationIconConfig;
        return this;
    }

    @DexIgnore
    /* renamed from: setPriority  reason: collision with other method in class */
    public final void m0setPriority(short s) {
        throw null;
        // lg6 lg6 = lg6.a;
        // if (s < 0 || 255 < s) {
        //     s = -1;
        // }
        // this.d = s;
    }

    @DexIgnore
    public final NotificationFilter setSender(String str) {
        throw null;
        // this.c = cw0.a(str, 97, null, null, 6);
        // return this;
    }

    @DexIgnore
    public final NotificationFilter setVibePatternConfig(NotificationVibePattern notificationVibePattern) {
        this.g = notificationVibePattern;
        return this;
    }

    @DexIgnore
    public void writeToParcel(Parcel parcel, int i) {
        if (parcel != null) {
            parcel.writeString(this.h);
        }
        if (parcel != null) {
            parcel.writeLong(this.a);
        }
        if (parcel != null) {
            parcel.writeByte(this.b);
        }
        if (parcel != null) {
            parcel.writeString(this.c);
        }
        if (parcel != null) {
            parcel.writeInt(this.d);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.e, i);
        }
        if (parcel != null) {
            parcel.writeSerializable(this.g);
        }
        if (parcel != null) {
            parcel.writeParcelable(this.f, i);
        }
    }

    @DexIgnore
    public final NotificationFilter setPriority(short s) {
        m0setPriority(s);
        return this;
    }

    @DexIgnore
    /* renamed from: setSender  reason: collision with other method in class */
    public final void m1setSender(String str) {
        throw null;
        // this.c = cw0.a(str, 97, null, null, 6);
    }

    @DexIgnore
    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        // try {
            cw0.a(jSONObject, bm0.b3, this.h);
            cw0.a(jSONObject, bm0.a3, cw0.a((int) this.a));
            cw0.a(jSONObject, bm0.S, Byte.valueOf(this.b));
            cw0.a(jSONObject, bm0.h, this.c);
            cw0.a(jSONObject, bm0.P, Short.valueOf(this.d));
            bm0 bm0 = com.fossil.bm0.z2;
            NotificationHandMovingConfig notificationHandMovingConfig = this.e;
            JSONObject jSONObject2 = null;
            cw0.a(jSONObject, bm0, notificationHandMovingConfig != null ? notificationHandMovingConfig.a() : null);
            bm0 bm02 = bm0.A2;
            NotificationVibePattern notificationVibePattern = this.g;
            cw0.a(jSONObject, bm02, notificationVibePattern != null ? Byte.valueOf(notificationVibePattern.a()) : null);
            bm0 bm03 = bm0.f3;
            NotificationIconConfig notificationIconConfig = this.f;
            if (notificationIconConfig != null) {
                jSONObject2 = notificationIconConfig.a();
            }
            cw0.a(jSONObject, bm03, jSONObject2);
        // } catch (JSONException e2) {
        //     qs0.h.a(e2);
        // }
        return jSONObject;
    }

    @DexIgnore
    public NotificationFilter(String str, NotificationHandMovingConfig notificationHandMovingConfig, NotificationVibePattern notificationVibePattern) {
        this(str);
        this.e = notificationHandMovingConfig;
        this.g = notificationVibePattern;
    }

    @DexIgnore
    /* JADX WARNING: Illegal instructions before constructor call */
    public /* synthetic */ NotificationFilter(Parcel parcel, qg6 qg6) {
        throw null;
        // this(r4);
        // String readString = parcel.readString();
        // if (readString != null) {
        //     wg6.a(readString, "parcel.readString()!!");
        //     this.a = parcel.readLong();
        //     this.b = parcel.readByte();
        //     String readString2 = parcel.readString();
        //     m1setSender(readString2 == null ? "" : readString2);
        //     m0setPriority((short) parcel.readInt());
        //     this.e = (NotificationHandMovingConfig) parcel.readParcelable(NotificationHandMovingConfig.class.getClassLoader());
        //     this.g = (NotificationVibePattern) parcel.readSerializable();
        //     this.f = (NotificationIconConfig) parcel.readParcelable(NotificationIconConfig.class.getClassLoader());
        //     return;
        // }
        // wg6.a();
        // throw null;
    }
}
