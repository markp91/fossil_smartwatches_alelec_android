package com.portfolio.platform.view;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.fossil.c9;
import com.fossil.jj4;
import com.fossil.oy5;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.w6;
import com.fossil.wearables.fossil.R;
import com.fossil.wg6;
import com.fossil.x24;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.model.microapp.MicroAppInstruction;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.manager.ThemeManager;
import com.sina.weibo.sdk.utils.ResourceManager;


import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;

@DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
public final class CustomizeWidget extends ConstraintLayout {
    @DexIgnore
    public /* final */ int A; // = Color.parseColor("#FFFF00");
    @DexIgnore
    public Integer B;
    @DexIgnore
    public Integer C;
    @DexIgnore
    public Integer D;
    @DexIgnore
    public int E;
    @DexIgnore
    public int F;
    @DexIgnore
    public int G;
    @DexIgnore
    public Drawable H;
    @DexIgnore
    public Drawable I;
    @DexIgnore
    public Drawable J;
    @DexIgnore
    public String K;
    @DexIgnore
    public String L;
    @DexIgnore
    public String M;
    @DexIgnore
    public int N;
    @DexIgnore
    public int O;
    @DexIgnore
    public float P;
    @DexIgnore
    public float Q;
    @DexIgnore
    public String R;
    @DexIgnore
    public int S;
    @DexIgnore
    public int T;
    @DexIgnore
    public float U;
    @DexIgnore
    public float V;
    @DexIgnore
    public String W;
    @DexIgnore
    public int a0;
    @DexIgnore
    public String b0;
    @DexIgnore
    public Drawable backgroundImage;
    @DexIgnore
    public int c0;
    @DexIgnore
    public String d0;
    @DexIgnore
    public int e0;
    @DexIgnore
    public float f0;
    @DexIgnore
    public float g0;
    @DexIgnore
    public boolean h0;
    @DexIgnore
    public boolean i0;
    @DexIgnore
    public /* final */ ColorDrawable k0;
    @DexIgnore
    public boolean l0;
    @DexIgnore
    public View u;
    @DexIgnore
    public View v;
    @DexIgnore
    public ImageView w;
    @DexIgnore
    public TextView x;
    @DexIgnore
    public TextView y;
    @DexIgnore
    public ProgressBar z;

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore
    public interface b {
        @DexIgnore
        void a(CustomizeWidget customizeWidget);
    }

    @DexIgnore
    public interface c {
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class d implements c9.c {
        @DexIgnore
        public /* final */ /* synthetic */ CustomizeWidget a;
        @DexIgnore
        public /* final */ /* synthetic */ b b;
        @DexIgnore
        public /* final */ /* synthetic */ float c;
        @DexIgnore
        public /* final */ /* synthetic */ String d;
        @DexIgnore
        public /* final */ /* synthetic */ Intent e;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a extends View.DragShadowBuilder {
            @DexIgnore
            public /* final */ /* synthetic */ d a;
            @DexIgnore
            public /* final */ /* synthetic */ c9 b;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(d dVar, c9 c9Var, View view, View view2) {
                super(view2);
                this.a = dVar;
                this.b = c9Var;
            }

            @DexIgnore
            public void onDrawShadow(Canvas canvas) {
                wg6.b(canvas, "canvas");
                float f = this.a.c;
                canvas.scale(f, f);
                View view = getView();
                if (view != null) {
                    view.draw(canvas);
                }
                this.a.a.setDragMode(true);
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
             arg types: [android.view.View, java.lang.String]
             candidates:
              com.fossil.wg6.a(int, int):int
              com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
              com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
              com.fossil.wg6.a(int, java.lang.String):void
              com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
              com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
            @DexIgnore
            public void onProvideShadowMetrics(Point point, Point point2) {
                wg6.b(point, "shadowSize");
                wg6.b(point2, "shadowTouchPoint");
                super.onProvideShadowMetrics(point, point2);
                View view = getView();
                wg6.a((Object) view, "view");
                int width = (int) (((float) view.getWidth()) * this.a.c);
                View view2 = getView();
                wg6.a((Object) view2, "view");
                int height = (int) (((float) view2.getHeight()) * this.a.c);
                point.set(width, height);
                point2.set(width / 2, height / 2);
                ILocalFLogger local = FLogger.INSTANCE.getLocal();
                local.d("WidgetControl", "onDragStart - shadowTouchPoint=" + point2);
                c9 c9Var = this.b;
                if (c9Var != null) {
                    c9Var.a(point2);
                }
            }
        }

        @DexIgnore
        public d(CustomizeWidget customizeWidget, b bVar, float f, String str, Intent intent) {
            this.a = customizeWidget;
            this.b = bVar;
            this.c = f;
            this.d = str;
            this.e = intent;
        }

        @DexIgnore
        /* JADX WARN: Type inference failed for: r8v5, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        /* JADX WARN: Type inference failed for: r8v6, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
        public final boolean a(View view, c9 c9Var) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            StringBuilder sb = new StringBuilder();
            sb.append("onDragStart - v=");
            sb.append(view != null ? Integer.valueOf(view.getId()) : null);
            sb.append(", helper=");
            sb.append(c9Var);
            local.d("WidgetControl", sb.toString());
            b bVar = this.b;
            if (bVar != null) {
                bVar.a(this.a);
            }
            a aVar = new a(this, c9Var, view, view);
            ClipData clipData = new ClipData(new ClipDescription(this.d, new String[]{"text/plain"}), new ClipData.Item(this.e));
            FLogger.INSTANCE.getLocal().d("WidgetControl", "onDragStart - created ClipDescription");
            if (Build.VERSION.SDK_INT >= 24) {
                return this.a.startDragAndDrop(clipData, aVar, null, 257);
            }
            return this.a.startDrag(clipData, aVar, null, 257);
        }
    }

    /*
    static {
        new a(null);
    }
    */

    /* JADX WARN: Type inference failed for: r6v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, ?, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @SuppressLint("ResourceType")
    @DexIgnore
    public CustomizeWidget(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        String b2;
        String b3;
        int i = this.A;
        this.E = i;
        this.F = i;
        this.G = i;
        this.N = i;
        this.O = i;
        this.P = -1.0f;
        this.Q = -1.0f;
        this.S = i;
        this.T = i;
        this.U = -1.0f;
        this.V = -1.0f;
        this.a0 = i;
        this.b0 = "";
        this.c0 = i;
        this.d0 = "";
        this.e0 = i;
        this.f0 = -1.0f;
        this.g0 = -1.0f;
        this.k0 = new ColorDrawable(w6.a(getContext(), (int) com.fossil.wearables.fossil.R.color.transparent));
        TypedArray typedArray = null;
        LayoutInflater layoutInflater = (LayoutInflater) (context != null ? context.getSystemService("layout_inflater") : null);
        View inflate = layoutInflater != null ? layoutInflater.inflate(2131558808, (ViewGroup) this, true) : null;
        if (inflate != null) {
            View findViewById = findViewById(2131362850);
            wg6.a((Object) findViewById, "findViewById(R.id.root)");
            this.u = findViewById;
            this.u.setElevation(getElevation());
            View findViewById2 = findViewById(2131362474);
            wg6.a((Object) findViewById2, "findViewById(R.id.holder)");
            this.v = findViewById2;
            View findViewById3 = inflate.findViewById(2131362631);
            wg6.a((Object) findViewById3, "view.findViewById(R.id.iv_top)");
            this.w = (ImageView) findViewById3;
            View findViewById4 = inflate.findViewById(2131363221);
            wg6.a((Object) findViewById4, "view.findViewById(R.id.tv_top)");
            this.x = (TextView) findViewById4;
            View findViewById5 = inflate.findViewById(2131363101);
            wg6.a((Object) findViewById5, "view.findViewById(R.id.tv_bottom)");
            this.y = (TextView) findViewById5;
            View findViewById6 = inflate.findViewById(2131362782);
            wg6.a((Object) findViewById6, "view.findViewById(R.id.pb_progress)");
            this.z = (ProgressBar) findViewById6;
            typedArray = context != null ? context.obtainStyledAttributes(attributeSet, x24.WidgetControl) : typedArray;
            if (typedArray != null) {
                this.B = Integer.valueOf(typedArray.getResourceId(0, 2131230894));
                this.C = Integer.valueOf(typedArray.getResourceId(2, 2131230896));
                Integer.valueOf(typedArray.getResourceId(2, 2131230896));
                this.D = this.B;
                this.H = typedArray.getDrawable(22);
                this.W = typedArray.getString(7);
                this.I = typedArray.getDrawable(21);
                this.J = typedArray.getDrawable(23);
                this.M = typedArray.getString(16);
                this.N = typedArray.getColor(17, this.A);
                this.O = typedArray.getColor(18, this.A);
                this.P = typedArray.getDimension(19, -1.0f);
                this.Q = typedArray.getDimension(20, -1.0f);
                this.R = typedArray.getString(4);
                this.S = typedArray.getColor(5, this.A);
                this.T = typedArray.getColor(6, this.A);
                this.U = typedArray.getDimension(8, -1.0f);
                this.V = typedArray.getDimension(9, -1.0f);
                String string = typedArray.getString(10);
                this.b0 = string == null ? "" : string;
                String string2 = typedArray.getString(11);
                this.d0 = string2 == null ? "" : string2;
                this.U = typedArray.getDimension(12, -1.0f);
                this.g0 = typedArray.getDimension(13, -1.0f);
                this.h0 = typedArray.getBoolean(15, false);
                this.i0 = typedArray.getBoolean(14, false);
                setRemoveMode(this.i0);
                typedArray.recycle();
            }
            if (!TextUtils.isEmpty(this.b0) && (b3 = ThemeManager.l.a().b(this.b0)) != null) {
                this.e0 = Color.parseColor(b3);
                this.a0 = this.e0;
            }
            if (!TextUtils.isEmpty(this.d0) && (b2 = ThemeManager.l.a().b(this.d0)) != null) {
                this.c0 = Color.parseColor(b2);
            }
            if (!this.h0) {
                this.w.setImageTintList(ColorStateList.valueOf(this.a0));
                return;
            }
            return;
        }
        throw new rc6("null cannot be cast to non-null type android.view.View");
    }

    @DexIgnore
    private final void setTopIconSrc(Drawable drawable) {
        this.I = drawable;
        if (drawable != null) {
            this.I = drawable;
            this.w.setImageDrawable(this.I);
            if (!this.h0) {
                this.w.setImageDrawable(this.I);
                this.w.setImageTintList(ColorStateList.valueOf(this.a0));
                this.e0 = this.a0;
            }
        }
    }

    @DexIgnore
    public final CustomizeWidget a(c cVar) {
        return this;
    }

    @DexIgnore
    public final void a(Integer num, Integer num2, Integer num3, Integer num4) {
        if (num3 != null) {
            num3.intValue();
            this.a0 = num3.intValue();
            this.N = num3.intValue();
            this.S = num3.intValue();
        }
        if (num != null) {
            num.intValue();
            this.c0 = num.intValue();
            this.O = num.intValue();
            this.T = num.intValue();
        }
        if (num2 != null) {
            num2.intValue();
            this.F = num2.intValue();
        }
        if (num4 != null) {
            num4.intValue();
            this.E = num4.intValue();
        }
        f();
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r2v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    public final void b(String str) {
        wg6.b(str, "complicationId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(w6.c(getContext(), 2131231149));
                    return;
                }
                break;
            case -331239923:
                if (str.equals(Constants.BATTERY)) {
                    setTopIconSrc(w6.c(getContext(), 2131231145));
                    return;
                }
                break;
            case -168965370:
                if (str.equals(Constants.CALORIES)) {
                    setTopIconSrc(w6.c(getContext(), 2131231147));
                    return;
                }
                break;
            case -85386984:
                if (str.equals("active-minutes")) {
                    setTopIconSrc(w6.c(getContext(), 2131231161));
                    return;
                }
                break;
            case -48173007:
                if (str.equals("chance-of-rain")) {
                    setTopIconSrc(w6.c(getContext(), 2131231154));
                    return;
                }
                break;
            case 3076014:
                if (str.equals(HardwareLog.COLUMN_DATE)) {
                    setTopIconSrc(w6.c(getContext(), 2131231146));
                    return;
                }
                break;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(w6.c(getContext(), 2131231148));
                    return;
                }
                break;
            case 109761319:
                if (str.equals("steps")) {
                    setTopIconSrc(w6.c(getContext(), 2131231155));
                    return;
                }
                break;
            case 134170930:
                if (str.equals("second-timezone")) {
                    setTopIconSrc(w6.c(getContext(), 2131231158));
                    return;
                }
                break;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(w6.c(getContext(), 2131231159));
                    return;
                }
                break;
            case 1884273159:
                if (str.equals("heart-rate")) {
                    setTopIconSrc(w6.c(getContext(), 2131231150));
                    return;
                }
                break;
        }
        setTopIconSrc(w6.c(getContext(), 2131231148));
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r1v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    public final void c(String str) {
        wg6.b(str, "microAppId");
        switch (oy5.a[MicroAppInstruction.MicroAppID.Companion.getMicroAppId(str).ordinal()]) {
            case 1:
                setTopIconSrc(w6.c(getContext(), 2131231124));
                return;
            case 2:
                setTopIconSrc(w6.c(getContext(), 2131231188));
                return;
            case 3:
                setTopIconSrc(w6.c(getContext(), 2131231180));
                return;
            case 4:
                setTopIconSrc(w6.c(getContext(), 2131231181));
                return;
            case 5:
                setTopIconSrc(w6.c(getContext(), 2131231186));
                return;
            case 6:
                setTopIconSrc(w6.c(getContext(), 2131231184));
                return;
            case 7:
                setTopIconSrc(w6.c(getContext(), 2131231183));
                return;
            case 8:
                setTopIconSrc(w6.c(getContext(), 2131231100));
                return;
            case 9:
                setTopIconSrc(w6.c(getContext(), 2131231187));
                return;
            case 10:
                setTopIconSrc(w6.c(getContext(), 2131231191));
                return;
            case 11:
                setTopIconSrc(w6.c(getContext(), 2131231192));
                return;
            case 12:
                setTopIconSrc(w6.c(getContext(), 2131231179));
                return;
            case 13:
                setTopIconSrc(w6.c(getContext(), 2131231193));
                return;
            case 14:
                setTopIconSrc(w6.c(getContext(), 2131231189));
                return;
            case 15:
                setTopIconSrc(w6.c(getContext(), 2131231190));
                return;
            default:
                return;
        }
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r1v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    public final void d(String str) {
        wg6.b(str, "watchAppId");
        switch (str.hashCode()) {
            case -829740640:
                if (str.equals("commute-time")) {
                    setTopIconSrc(w6.c(getContext(), 2131231054));
                    return;
                }
                return;
            case -740386388:
                if (str.equals("diagnostics")) {
                    setTopIconSrc(w6.c(getContext(), 2131231184));
                    return;
                }
                return;
            case -420342747:
                if (str.equals("wellness")) {
                    setTopIconSrc(w6.c(getContext(), 2131231160));
                    return;
                }
                return;
            case 96634189:
                if (str.equals("empty")) {
                    setTopIconSrc(null);
                    return;
                }
                return;
            case 104263205:
                if (str.equals(Constants.MUSIC)) {
                    setTopIconSrc(w6.c(getContext(), 2131231151));
                    return;
                }
                return;
            case 110364485:
                if (str.equals("timer")) {
                    setTopIconSrc(w6.c(getContext(), 2131231215));
                    return;
                }
                return;
            case 1223440372:
                if (str.equals("weather")) {
                    setTopIconSrc(w6.c(getContext(), 2131231159));
                    return;
                }
                return;
            case 1374620322:
                if (str.equals("notification-panel")) {
                    setTopIconSrc(w6.c(getContext(), 2131231186));
                    return;
                }
                return;
            case 1525170845:
                if (str.equals("workout")) {
                    setTopIconSrc(w6.c(getContext(), 2131231161));
                    return;
                }
                return;
            case 1860261700:
                if (str.equals("stop-watch")) {
                    setTopIconSrc(w6.c(getContext(), 2131231190));
                    return;
                }
                return;
            default:
                return;
        }
    }

    @DexIgnore
    public final void e() {
        if (this.l0) {
            this.l0 = false;
            this.z.setProgress(0);
        }
    }

    /* JADX WARN: Type inference failed for: r6v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.zz, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @SuppressLint("WrongConstant")
    @DexIgnore
    public final void f() {
        if (!TextUtils.isEmpty(this.M)) {
            this.x.setText(this.M);
        } else {
            this.x.setVisibility(4);
        }
        if (this.i0) {
            this.w.setVisibility(0);
            ImageView imageView = this.w;
            Drawable drawable = this.H;
            if (drawable == null) {
                drawable = this.k0;
            }
            imageView.setImageDrawable(drawable);
            this.x.setVisibility(4);
            if (!TextUtils.isEmpty(this.W)) {
                this.y.setText(this.W);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        } else if (this.h0) {
            String str = this.L;
            if (str == null) {
                str = this.K;
            }
            if (!TextUtils.isEmpty(str)) {
                wg6.a((Object) jj4.a(this.w).a(str).b().a(this.w), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable2 = this.J;
                if (drawable2 == null) {
                    drawable2 = this.I;
                }
                if (drawable2 != null) {
                    this.w.setImageDrawable(drawable2);
                    this.w.setVisibility(0);
                    this.x.setVisibility(4);
                } else {
                    this.w.setImageDrawable(this.k0);
                    this.w.setVisibility(4);
                    this.x.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.R)) {
                this.y.setText(this.R);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        } else {
            if (!TextUtils.isEmpty(this.K)) {
                wg6.a((Object) jj4.a(this.w).a(this.K).b().a(this.w), "GlideApp.with(ivTop)\n   \u2026             .into(ivTop)");
            } else {
                Drawable drawable3 = this.I;
                if (drawable3 != null) {
                    this.w.setImageDrawable(drawable3);
                    this.w.setVisibility(0);
                    this.x.setVisibility(4);
                } else {
                    this.w.setImageDrawable(this.k0);
                    this.w.setVisibility(4);
                    this.x.setVisibility(0);
                }
            }
            if (!TextUtils.isEmpty(this.R)) {
                this.y.setText(this.R);
                this.y.setVisibility(0);
            } else {
                this.y.setVisibility(8);
            }
        }
        this.w.setVisibility(0);
        if (this.h0) {
            this.D = this.C;
            this.G = this.F;
            this.u.getBackground().clearColorFilter();
            if (this.F != this.A) {
                this.u.getBackground().mutate().setColorFilter(this.F, PorterDuff.Mode.SRC_IN);
            } else {
                Drawable drawable4 = this.backgroundImage;
                if (drawable4 != null) {
                    this.u.setBackground(drawable4);
                } else {
                    View view = this.u;
                    Context context = getContext();
                    Integer num = this.D;
                    if (num != null) {
                        view.setBackground(w6.c(context, num.intValue()));
                    } else {
                        wg6.a();
                        throw null;
                    }
                }
            }
            int i = this.c0;
            if (i != this.A) {
                this.w.setImageTintList(ColorStateList.valueOf(i));
                this.e0 = this.c0;
            } else {
                this.w.setImageTintList(ColorStateList.valueOf(this.a0));
                this.e0 = this.a0;
            }
            int i2 = this.O;
            int i3 = this.A;
            if (i2 != i3) {
                this.x.setTextColor(i2);
            } else {
                int i4 = this.N;
                if (i4 != i3) {
                    this.x.setTextColor(i4);
                } else {
                    int i5 = this.c0;
                    if (i5 != i3) {
                        this.x.setTextColor(i5);
                    } else {
                        this.x.setTextColor(this.a0);
                    }
                }
            }
            float f = this.Q;
            if (f != -1.0f) {
                this.x.setTextSize(f);
            } else {
                float f2 = this.P;
                if (f2 != -1.0f) {
                    this.x.setTextSize(f2);
                } else {
                    float f3 = this.g0;
                    if (f3 != -1.0f) {
                        this.x.setTextSize(f3);
                    } else {
                        float f4 = this.f0;
                        if (f4 != -1.0f) {
                            this.x.setTextSize(f4);
                        }
                    }
                }
            }
            int i6 = this.T;
            int i7 = this.A;
            if (i6 != i7) {
                this.y.setTextColor(i6);
            } else {
                int i8 = this.S;
                if (i8 != i7) {
                    this.y.setTextColor(i8);
                } else {
                    int i9 = this.c0;
                    if (i9 != i7) {
                        this.y.setTextColor(i9);
                    } else {
                        this.y.setTextColor(this.a0);
                    }
                }
            }
            float f5 = this.V;
            if (f5 != -1.0f) {
                this.y.setTextSize(f5);
                return;
            }
            float f6 = this.U;
            if (f6 != -1.0f) {
                this.y.setTextSize(f6);
                return;
            }
            float f7 = this.g0;
            if (f7 != -1.0f) {
                this.y.setTextSize(f7);
                return;
            }
            float f8 = this.f0;
            if (f8 != -1.0f) {
                this.y.setTextSize(f8);
                return;
            }
            return;
        }
        this.D = this.B;
        this.G = this.E;
        this.u.getBackground().clearColorFilter();
        if (this.E != this.A) {
            this.u.getBackground().mutate().setColorFilter(this.E, PorterDuff.Mode.SRC_IN);
        } else {
            Drawable drawable5 = this.backgroundImage;
            if (drawable5 != null) {
                this.u.setBackground(drawable5);
            } else {
                View view2 = this.u;
                Context context2 = getContext();
                Integer num2 = this.D;
                if (num2 != null) {
                    view2.setBackground(w6.c(context2, num2.intValue()));
                } else {
                    wg6.a();
                    throw null;
                }
            }
        }
        this.w.setImageTintList(ColorStateList.valueOf(this.a0));
        int i10 = this.a0;
        this.e0 = i10;
        int i11 = this.N;
        int i12 = this.A;
        if (i11 != i12) {
            this.x.setTextColor(i11);
        } else if (i10 != i12) {
            this.x.setTextColor(i10);
        }
        float f9 = this.P;
        if (f9 != -1.0f) {
            this.x.setTextSize(f9);
        } else {
            float f10 = this.f0;
            if (f10 != -1.0f) {
                this.x.setTextSize(f10);
            }
        }
        int i13 = this.S;
        if (i13 != this.A) {
            this.y.setTextColor(i13);
        } else {
            this.y.setTextColor(this.a0);
        }
        float f11 = this.U;
        if (f11 != -1.0f) {
            this.y.setTextSize(f11);
            return;
        }
        float f12 = this.f0;
        if (f12 != -1.0f) {
            this.y.setTextSize(f12);
        }
    }

    @DexIgnore
    public final void g() {
        int i = this.A;
        this.E = i;
        this.F = i;
        this.c0 = i;
        this.O = i;
        this.T = i;
        this.N = i;
        this.S = i;
        f();
    }

    @DexIgnore
    public final int getBackgroundDrawableColor() {
        int i = this.G;
        if (i != this.A) {
            return i;
        }
        Integer num = this.D;
        if (num != null && num.intValue() == 2131230894) {
            return 2131099855;
        }
        if (num != null && num.intValue() == 2131230893) {
            return com.fossil.wearables.fossil.R.color.activeColorPrimary;
        }
        return 2131100404;
    }

    @DexIgnore
    public final int getBottomTextColor() {
        return this.y.getCurrentTextColor();
    }

    @DexIgnore
    public final int getIconTintColor() {
        return this.e0;
    }

    @DexIgnore
    public final void h() {
        f();
    }

    @DexIgnore
    public final void setBackgroundDrawableCus(Drawable drawable) {
        wg6.b(drawable, ResourceManager.DRAWABLE);
        this.backgroundImage = drawable;
        f();
    }

    @DexIgnore
    public final void setBackgroundRes(Integer num) {
        this.backgroundImage = null;
        this.B = num;
        f();
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    public final void setBottomContent(String str) {
        this.R = str;
        if (this.i0) {
            if (!TextUtils.isEmpty(str)) {
                this.y.setText(str);
            }
        } else if (!TextUtils.isEmpty(str)) {
            this.y.setVisibility(0);
            this.y.setText(str);
        } else {
            this.y.setVisibility(8);
        }
    }

    @DexIgnore
    public final void setDefaultColorRes(Integer num) {
        if (num != null) {
            num.intValue();
            this.a0 = num.intValue();
            f();
        }
    }

    @SuppressLint("WrongConstant")
    @DexIgnore
    /* JADX WARN: Type inference failed for: r3v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.ViewGroup] */
    public final void setDragMode(boolean z2) {
        if (z2) {
            this.u.setVisibility(4);
            this.v.setVisibility(0);
            setAlpha(1.0f);
            return;
        }
        this.u.setVisibility(0);
        this.v.setVisibility(4);
        setAlpha(1.0f);
    }

    @DexIgnore
    public final void setRemoveMode(boolean z2) {
        this.i0 = z2;
        f();
    }

    @DexIgnore
    public final void setSelectedWc(boolean z2) {
        this.h0 = z2;
        f();
    }

    @DexIgnore
    public static /* synthetic */ c9 a(CustomizeWidget customizeWidget, String str, Intent intent, View.OnDragListener onDragListener, b bVar, int i, Object obj) {
        throw null;
        // if ((i & 4) != 0) {
        //     onDragListener = null;
        // }
        // if ((i & 8) != 0) {
        //     bVar = null;
        // }
        // return customizeWidget.a(str, intent, onDragListener, bVar);
    }

    @DexIgnore
    /* JADX WARN: Type inference failed for: r7v0, types: [com.portfolio.platform.view.CustomizeWidget, android.view.View, android.view.ViewGroup] */
    public final c9 a(String str, Intent intent, View.OnDragListener onDragListener, b bVar) {
        wg6.b(str, "label");
        wg6.b(intent, "intentItem");
        setTag(getTag());
        c9 c9Var = new c9((View) this, new d(this, bVar, 1.0f, str, intent));
        c9Var.a();
        if (onDragListener != null) {
            setOnDragListener(onDragListener);
        }
        return c9Var;
    }

    @DexIgnore
    public final void d() {
        this.l0 = true;
        this.z.setProgress(100);
    }
}
