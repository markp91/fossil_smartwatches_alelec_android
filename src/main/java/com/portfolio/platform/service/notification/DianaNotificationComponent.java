package com.portfolio.platform.service.notification;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Telephony;
import android.service.notification.StatusBarNotification;
import android.text.TextUtils;

import com.fossil.AlelecPrefs;
import com.fossil.AppWrapper;
import com.fossil.NotificationAppHelper;
import com.fossil.an4;
import com.fossil.bk4;
import com.fossil.cd6;
import com.fossil.ux5;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilter;
import com.misfit.frameworks.buttonservice.model.notification.AppNotificationFilterSettings;
import com.portfolio.platform.data.model.InstalledApp;
import com.portfolio.platform.helper.DeviceHelper;
import com.fossil.ff6;
import com.fossil.fh6;
import com.fossil.hf6;
import com.fossil.ig6;
import com.fossil.il6;
import com.fossil.jf6;
import com.fossil.jh6;
import com.fossil.lf6;
import com.fossil.mj6;
import com.fossil.nc6;
import com.fossil.qg6;
import com.fossil.rc6;
import com.fossil.rm6;
import com.fossil.sf6;
import com.fossil.wearables.fsl.appfilter.AppFilter;
import com.fossil.wearables.fsl.contact.Contact;
import com.fossil.wearables.fsl.contact.ContactGroup;
import com.fossil.wg6;
import com.fossil.xe6;
import com.fossil.xj6;
import com.fossil.xx5;
import com.fossil.yf6;
import com.fossil.yj6;
import com.fossil.zm4;
import com.j256.ormlite.field.FieldType;
import com.misfit.frameworks.buttonservice.db.HardwareLog;
import com.misfit.frameworks.buttonservice.enums.MFDeviceFamily;
import com.misfit.frameworks.buttonservice.log.FLogger;
import com.misfit.frameworks.buttonservice.log.ILocalFLogger;
import com.misfit.frameworks.buttonservice.log.RemoteFLogger;
import com.misfit.frameworks.buttonservice.model.notification.DianaNotificationObj;
import com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj;
import com.misfit.frameworks.common.constants.Constants;
import com.portfolio.platform.PortfolioApp;
import com.portfolio.platform.data.model.DNDScheduledTimeModel;
import com.portfolio.platform.data.model.NotificationSettingsModel;
import com.portfolio.platform.data.model.PhoneFavoritesContact;
import com.portfolio.platform.data.model.QuickResponseSender;
import com.portfolio.platform.data.source.QuickResponseRepository;
import com.portfolio.platform.data.source.local.diana.notification.NotificationSettingsDatabase;
import com.portfolio.platform.data.source.local.dnd.DNDSettingsDatabase;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import org.joda.time.DateTimeConstants;


import lanchon.dexpatcher.annotation.DexAdd;
import lanchon.dexpatcher.annotation.DexEdit;
import lanchon.dexpatcher.annotation.DexIgnore;
import lanchon.dexpatcher.annotation.DexAction;
import lanchon.dexpatcher.annotation.DexWrap;

@DexEdit(defaultAction = DexAction.ADD, staticConstructorAction = DexAction.APPEND)
public final class DianaNotificationComponent {
    @DexIgnore
    public static /* final */ HashMap<String, b> l; // = new HashMap<>();
    @DexIgnore
    public static /* final */ HashMap<String, i> m;
    @DexIgnore
    public static /* final */ a n; // = new a(null);
    @DexIgnore
    public /* final */ String a;
    @DexIgnore
    public /* final */ c b; // = new c(this.h);
    @DexIgnore
    public int c; // = 1380;
    @DexIgnore
    public int d; // = 1140;
    @DexIgnore
    public g e;
    @DexIgnore
    public e f;
    @DexIgnore
    public /* final */ Queue<NotificationBaseObj> g; // = mn3.a(jl3.create(20));
    @DexIgnore
    public /* final */ an4 h;
    @DexIgnore
    public /* final */ DNDSettingsDatabase i;
    @DexIgnore
    public /* final */ QuickResponseRepository j;
    @DexIgnore
    public /* final */ NotificationSettingsDatabase k;

    @DexEdit(defaultAction = DexAction.IGNORE, staticConstructorAction = DexAction.APPEND)
    public static class NotificationMonitor {
        @DexIgnore
        public String a;
        @DexIgnore
        public int b;
        @DexIgnore
        public String c;
        @DexIgnore
        public String d;
        @DexIgnore
        public String e;
        @DexIgnore
        public String f;
        @DexIgnore
        public long g;
        @DexIgnore
        public boolean h;
        @DexIgnore
        public boolean i;
        @DexIgnore
        public int j;
        @DexIgnore
        public boolean k;
        @DexAdd
        public boolean dnd = false;

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public static final class a {
            @DexIgnore
            public a() {
            }

            @DexIgnore
            public /* synthetic */ a(qg6 qg6) {
                this();
            }
        }

        /*
        static {
            new a(null);
        }
        */

        @DexIgnore
        public NotificationMonitor() {
            this.a = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = "";
        }

        @DexIgnore
        public final int a() {
            return this.b;
        }

        @DexIgnore
        public final String b() {
            return this.c;
        }

        @DexIgnore
        public final void c(String str) {
            wg6.b(str, "<set-?>");
            this.a = str;
        }

        @DexIgnore
        public final String d() {
            return this.a;
        }

        @DexIgnore
        public final String e() {
            return this.e;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
        @DexIgnore
        public boolean equals(Object obj) {
            if (obj == null || !(obj instanceof NotificationMonitor)) {
                return false;
            }
            NotificationMonitor notificationMonitor = (NotificationMonitor) obj;
            if (!wg6.a((Object) this.a, (Object) notificationMonitor.a) || this.g != notificationMonitor.g) {
                return false;
            }
            return true;
        }

        @DexIgnore
        public final void f(String str) {
            wg6.b(str, "<set-?>");
            this.d = str;
        }

        @DexIgnore
        public final String g() {
            return this.d;
        }

        @DexIgnore
        public final long h() {
            return this.g;
        }

        @DexIgnore
        public int hashCode() {
            return 0;
        }

        @DexIgnore
        public final boolean i() {
            return this.k;
        }

        @DexIgnore
        public final boolean j() {
            return this.i;
        }

        @DexIgnore
        public final boolean k() {
            return this.h;
        }

        @DexIgnore
        public final void a(int i2) {
            this.b = i2;
        }

        @DexIgnore
        public final void b(String str) {
            wg6.b(str, "<set-?>");
            this.c = str;
        }

        @DexIgnore
        public final void c(boolean z) {
            this.h = z;
        }

        @DexIgnore
        public final void d(String str) {
            wg6.b(str, "<set-?>");
            this.e = str;
        }

        @DexIgnore
        public final void e(String str) {
            wg6.b(str, "<set-?>");
            this.f = str;
        }

        @DexIgnore
        public final String f() {
            return this.f;
        }

        @DexIgnore
        public final void a(long j2) {
            this.g = j2;
        }

        @DexIgnore
        public final void b(boolean z) {
            this.i = z;
        }

        @DexIgnore
        public final int c() {
            return this.j;
        }

        @DexIgnore
        public final void a(boolean z) {
            this.k = z;
        }

        @DexIgnore
        public final boolean b(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 2) == 2;
        }

        @DexIgnore
        public final boolean c(StatusBarNotification statusBarNotification) {
            return (statusBarNotification.getNotification().flags & 512) == 512;
        }

        @DexAdd
        public byte[] drawableToPixels(Drawable drawable, int width, int height, int background, boolean invert) {

            Bitmap bitmap = null;
            // Draw icon onto 24 x 24 bitmap

            // if (drawable instanceof BitmapDrawable) {
            //     bitmap = ((BitmapDrawable) drawable).getBitmap();
            // } else
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                if (drawable instanceof AdaptiveIconDrawable) {
                    Drawable backgroundDr = ((AdaptiveIconDrawable) drawable).getBackground();
                    Drawable foregroundDr = ((AdaptiveIconDrawable) drawable).getForeground();

                    Drawable[] drr = new Drawable[2];
                    drr[0] = backgroundDr;
                    drr[1] = foregroundDr;

                    bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(bitmap);
                    LayerDrawable layerDrawable = new LayerDrawable(drr);
                    layerDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                    layerDrawable.draw(canvas);
                }
            }
            if (bitmap == null) {
                bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(bitmap);

                drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
                drawable.draw(canvas);
            }
            if (bitmap != null) {

                // Convert to greyscale
                Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
                Canvas c = new Canvas(bmpGrayscale);

                if ((background & 0xFF000000) != 0) {
                    Paint paint = new Paint();
                    paint.setColor(background);
                    c.drawRect(0F, 0F, (float) width, (float) height, paint);
                }

                Paint paint = new Paint();
                ColorMatrix cm = new ColorMatrix();
                cm.setSaturation(0);
                ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
                paint.setColorFilter(f);

                c.drawBitmap(bitmap, 0, 0, paint);
                // scaled.recycle();

                // Downsample to 2 bit image
                int[] pixels = new int[width * height];
                bmpGrayscale.getPixels(pixels, 0, width, 0, 0, width, height);
                byte[] b_pixels = new byte[pixels.length];

                int n_light_pix = 0;
                for (int i = 0; i < pixels.length; i++) {
                    int pix = pixels[i];
                    //todo invert
                    b_pixels[i] = (byte) (pix >> 6 & 0x03);
                    if (invert) {
                        b_pixels[i] = (byte)(3 - b_pixels[i]);
                    }
                    if (b_pixels[i] > 0) {
                        n_light_pix++;
                    }
                }
                bitmap.recycle();
                bmpGrayscale.recycle();
                if ((100.0 * n_light_pix / pixels.length) < 5) {
                    // under 5% pixels visible, need to invert the background and try again
                    return null;
                }
                return b_pixels;
            }
            return null;
        }

        @DexAdd
        public static byte[] rle_encode(byte[] chars) {
            try {
                if (chars == null || chars.length == 0) return null;

                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                Writer writer = new OutputStreamWriter(bout);

                // StringBuilder builder = new StringBuilder();
                byte current = chars[0];
                int count = 1;

                for (int i = 1; i < chars.length; i++) {
                    if (current != chars[i]) {
                        if (count > 0) {
                            writer.write(count);
                            writer.write(current);
                        }
                        current = chars[i];
                        count = 1;
                    } else {
                        count++;
                        if (count == 254) {
                            writer.write(count);
                            writer.write(current);
                            count = 0;
                        }
                    }
                }
                if (count > 1) writer.write(count);
                writer.write(current);
                writer.flush();
                return bout.toByteArray();
            } catch (IOException unused) {
                return null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.yj6.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.fossil.yj6.a(java.lang.CharSequence, int, char):java.lang.CharSequence
          com.fossil.yj6.a(java.lang.String, char, java.lang.String):java.lang.String
          com.fossil.yj6.a(java.lang.String, int, char):java.lang.String
          com.fossil.yj6.a(java.lang.String, java.lang.CharSequence, java.lang.CharSequence):java.lang.String
          com.fossil.yj6.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
          com.fossil.xj6.a(java.lang.String, java.lang.String, boolean):boolean
          com.fossil.uj6.a(java.lang.Appendable, java.lang.Object, com.fossil.hg6):void
          com.fossil.yj6.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean */
        @DexIgnore
        public final boolean a(StatusBarNotification statusBarNotification) {
            String tag = statusBarNotification.getTag();
            if (tag == null || (!yj6.a((CharSequence) tag, (CharSequence) "sms", true) && !yj6.a((CharSequence) tag, (CharSequence) "mms", true) && !yj6.a((CharSequence) tag, (CharSequence) "rcs", true))) {
                return TextUtils.equals(statusBarNotification.getNotification().category, "msg");
            }
            return true;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
        @DexEdit
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public NotificationMonitor(StatusBarNotification statusBarNotification, @DexIgnore Void tag) {
            this();
            String str;
            CharSequence charSequence;
            Bundle bundle;
            DianaNotificationObj.AApplicationName a2;
            wg6.b(statusBarNotification, "sbn");
            this.a = a((long) statusBarNotification.getId(), statusBarNotification.getTag());
            String packageName = statusBarNotification.getPackageName();
            wg6.a((Object) packageName, "sbn.packageName");
            this.c = packageName;
            b bVar = DianaNotificationComponent.n.a().get(this.c);
            CharSequence f2 = (bVar == null || (a2 = bVar.a()) == null || (f2 = a2.getAppName()) == null) ? PortfolioApp.get.instance().f(this.c) : f2;
            CharSequence charSequence2 = statusBarNotification.getNotification().extras.getCharSequence("android.title");
            this.d = (charSequence2 != null ? charSequence2 : f2).toString();
            this.e = a(this.d);
            CharSequence charSequence3 = statusBarNotification.getNotification().extras.getCharSequence("android.bigText");
            if (!(charSequence3 == null || xj6.a(charSequence3))) {
                Notification notification = statusBarNotification.getNotification();
                str = a(String.valueOf((notification == null || (bundle = notification.extras) == null) ? null : bundle.getCharSequence("android.bigText")));
            } else {
                String charSequence4 = statusBarNotification.getNotification().extras.getCharSequence("android.text").toString();
                str = a((charSequence4 == null ? "" : charSequence4).toString());
            }
            this.f = str;
            this.h = c(statusBarNotification);
            this.i = b(statusBarNotification);
            this.g = statusBarNotification.getNotification().when;
            this.j = statusBarNotification.getNotification().priority;
            this.k = statusBarNotification.getNotification().extras.getInt("android.progressMax", 0) != 0;
            if (a(statusBarNotification) && (charSequence = statusBarNotification.getNotification().tickerText) != null) {
                int length = charSequence.length();
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        i2 = -1;
                        break;
                    } else if (wg6.a((Object) String.valueOf(charSequence.charAt(i2)), (Object) ":")) {
                        break;
                    } else {
                        i2++;
                    }
                }
                if (i2 != -1) {
                    String obj = charSequence.subSequence(0, i2).toString();
                    if (obj != null) {
                        this.e = yj6.d((CharSequence) obj).toString();
                        String obj2 = charSequence.subSequence(i2 + 1, charSequence.length()).toString();
                        if (obj2 != null) {
                            this.f = yj6.d((CharSequence) obj2).toString();
                            return;
                        }
                        throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                    }
                    throw new rc6("null cannot be cast to non-null type kotlin.CharSequence");
                }
            }
        }

        @DexAdd
        private static final Object fileLock = new Object();

        @DexAdd
        public NotificationMonitor(StatusBarNotification statusBarNotification) {
            this(statusBarNotification, (Void) null);
            this.dnd = checkDND(statusBarNotification.getNotification());

            // TODO cleanup below
            String packageName = this.c;

            Context context = PortfolioApp.get.instance().getApplicationContext();
            File iconFile = new File(context.getExternalFilesDir("appIcons"), packageName + ".icon");

            synchronized (fileLock) {
                if (!iconFile.exists()) {
                    Bitmap bitmap = null;
                    try {
                        PackageManager manager = context.getPackageManager();
                        Resources resources = manager.getResourcesForApplication(packageName);
                        // Icon small = statusBarNotification.getNotification().getSmallIcon();
                        Drawable drawable = resources.getDrawable(statusBarNotification.getNotification().icon);


                        int notifIconWidth = 24;
                        int notifIconHeight = 24;


                        byte[] notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0, false);
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xff000000, false);
                        }
                        if (notifIcon == null) {
                            notifIcon = drawableToPixels(drawable, notifIconWidth, notifIconHeight, 0xffffffff, true);
                        }
                        if (notifIcon != null) {

                            // Convert to RLE with header and footer
                            byte[] rle = rle_encode(notifIcon);
                            notifIcon = new byte[rle.length + 4];
                            notifIcon[0] = (byte) notifIconWidth;
                            notifIcon[1] = (byte) notifIconHeight;
                            notifIcon[notifIcon.length - 1] = (byte) 0xff;
                            notifIcon[notifIcon.length - 2] = (byte) 0xff;
                            System.arraycopy(rle, 0, notifIcon, 2, rle.length);

                            // Write to file for next time

                            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(iconFile));
                            bos.write(notifIcon);
                            bos.flush();
                            bos.close();
                        }

                        // Send Filters to watch
                        NotificationAppHelper notificationAppHelper = NotificationAppHelper.b;
                        List<AppWrapper> allApps = (List<AppWrapper>)notificationAppHelper.a(context);
                        List<AppWrapper> apps = new ArrayList<>();
                        for (AppWrapper a: allApps) {
                            InstalledApp installedApp2 = a.getInstalledApp();
                            if (installedApp2 != null) {
                                Boolean isSelected = installedApp2.isSelected();
                                // wd4.a(isSelected, "it.isSelected");
                                if (isSelected) {
                                    apps.add(a);
                                }
                            }
                        }

                        List<AppNotificationFilter> j = new ArrayList<>();
                        // From NotificationAppsPresenter, search for "setRuleToDevice, showProgressDialog", about 9 lines below.
                        //noinspection CollectionAddAllCanBeReplacedWithConstructor
                        j.addAll(ux5.a(apps, false));
                        List<AppNotificationFilter> phone_sms_filters = notificationAppHelper.a(0, 0);
                        j.addAll(phone_sms_filters);
                        PortfolioApp.get.instance().b(new AppNotificationFilterSettings(j, System.currentTimeMillis()), PortfolioApp.get.instance().e());

                    } catch (PackageManager.NameNotFoundException | NullPointerException | IOException | IllegalArgumentException e) {
                        e.printStackTrace();
                    } finally {
                        if (bitmap != null) {
                            bitmap.recycle();
                        }
                    }
                }
            }


        }

        @DexAdd
        private boolean checkDND(Notification notification) {
            // Return true if the notification should be hidden for DND
            String category = notification.category;
            NotificationManager notificationManager = (NotificationManager)PortfolioApp.get.instance().getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                int filter = notificationManager.getCurrentInterruptionFilter();

                FLogger.INSTANCE.getLocal().d(this.b(), "checkDND: filter = " + notificationManager.getCurrentInterruptionFilter());
                FLogger.INSTANCE.getLocal().d(this.b(), "checkDND: category = " + category);
                switch (filter) {
                    case NotificationManager.INTERRUPTION_FILTER_NONE:
                        return true;

                    case NotificationManager.INTERRUPTION_FILTER_ALARMS:
                        if (!TextUtils.equals(Notification.CATEGORY_ALARM, category)) {
                            return true;
                        }
                        break;

                    case NotificationManager.INTERRUPTION_FILTER_PRIORITY:
                        String[] activeCategories = NotificationManager.Policy.priorityCategoriesToString(notificationManager.getNotificationPolicy().priorityCategories).split(",");
                        StringBuilder log = new StringBuilder("checkDND: activeCategories = ");
                        boolean ret = true;
                        for (String active : activeCategories) {
                            log.append(active).append(",");
                            if (TextUtils.equals(active, category)) {
                                ret = false;
                            }
                        }
                        FLogger.INSTANCE.getLocal().d(this.b(),log.toString());

                        if (category == null && notification.priority >= Notification.PRIORITY_HIGH) {
                            FLogger.INSTANCE.getLocal().d(this.b(), "checkDND: priority = " + notification.priority + "dndHighSettingEnabled: " + dndHighSettingEnabled);
                            if (dndHighSettingEnabled) {
                                ret = false;
                            }
                        }

                        return ret;

                    case NotificationManager.INTERRUPTION_FILTER_UNKNOWN:
                    case NotificationManager.INTERRUPTION_FILTER_ALL:
                    default:
                        return false;
                }
            }
            return false;
        }

        @DexIgnore
        public final String a(String str) {
            String replace = new mj6("\\p{C}").replace(str, "");
            int length = replace.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean z2 = replace.charAt(!z ? i2 : length) <= ' ';
                if (!z) {
                    if (!z2) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!z2) {
                    break;
                } else {
                    length--;
                }
            }
            return replace.subSequence(i2, length + 1).toString();
        }

        @DexIgnore
        public final String a(long j2, String str) {
            return j2 + ':' + str;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class a {
        @DexIgnore
        public a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
        @DexIgnore
        public final HashMap<String, b> a() {
            if (DianaNotificationComponent.l.isEmpty()) {
                int i = 1;
                for (DianaNotificationObj.AApplicationName aApplicationName : DianaNotificationObj.AApplicationName.Companion.getSUPPORTED_ICON_NOTIFICATION_APPS()) {
                    String packageName = aApplicationName.getPackageName();
                    if (!wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) && !wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) && !wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGOOGLE_CALENDAR().getPackageName()) && !wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getHANGOUTS().getPackageName()) && !wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getGMAIL().getPackageName()) && !wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getWHATSAPP().getPackageName())) {
                        DianaNotificationComponent.l.put(aApplicationName.getPackageName(), new b(i, aApplicationName, false, false));
                    } else {
                        DianaNotificationComponent.l.put(aApplicationName.getPackageName(), new b(i, aApplicationName, false, true));
                    }
                    i++;
                }
            }
            return DianaNotificationComponent.l;
        }

        @DexIgnore
        public /* synthetic */ a(qg6 qg6) {
            this();
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class b {
        @DexIgnore
        public /* final */ DianaNotificationObj.AApplicationName a;
        @DexIgnore
        public /* final */ boolean b;

        @DexIgnore
        public b(int i, DianaNotificationObj.AApplicationName aApplicationName, boolean z, boolean z2) {
            wg6.b(aApplicationName, "notificationApp");
            this.a = aApplicationName;
            this.b = z2;
        }

        @DexIgnore
        public final DianaNotificationObj.AApplicationName a() {
            return this.a;
        }

        @DexIgnore
        public final boolean b() {
            return this.b;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public final class e extends ContentObserver {
        @DexIgnore
        public b a; // = new b(this, 0, null, 0, 0, 15, null);

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public final class a extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public a(e eVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                a aVar = new a(this.this$0, xe6);
                aVar.p$ = (il6) obj;
                throw null;
                // return aVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                // return ((a) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                ff6.a();
                if (this.label == 0) {
                    nc6.a(obj);
                    e eVar = this.this$0;
                    b b = eVar.a();
                    if (b == null) {
                        b = new b(this.this$0, 0, null, 0, 0, 15, null);
                    }
                    eVar.a = b;
                    return cd6.a;
                }
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        public final class b {
            @DexIgnore
            public /* final */ long a;
            @DexIgnore
            public /* final */ String b;
            @DexIgnore
            public /* final */ int c;

            @DexIgnore
            public b(e eVar, long j, String str, int i, long j2) {
                wg6.b(str, "number");
                this.a = j;
                this.b = str;
                this.c = i;
            }

            @DexIgnore
            public final long a() {
                return this.a;
            }

            @DexIgnore
            public final String b() {
                return this.b;
            }

            @DexIgnore
            public final int c() {
                return this.c;
            }

            @DexIgnore
            /* JADX INFO: this call moved to the top of the method (can break code semantics) */
            public /* synthetic */ b(e eVar, long j, String str, int i, long j2, int i2, qg6 qg6) {
                this(eVar, (i2 & 1) != 0 ? -1 : j, (i2 & 2) != 0 ? "" : str, (i2 & 4) != 0 ? 0 : i, (i2 & 8) != 0 ? 0 : j2);
            }
        }

        @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
        @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$PhoneCallObserver$processMissedCall$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
        public final class c extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
            @DexIgnore
            public int label;
            @DexIgnore
            public il6 p$;
            @DexIgnore
            public /* final */ /* synthetic */ e this$0;

            @DexIgnore
            /* JADX INFO: super call moved to the top of the method (can break code semantics) */
            public c(e eVar, xe6 xe6) {
                super(2, xe6);
                this.this$0 = eVar;
            }

            @DexIgnore
            public final xe6<cd6> create(Object obj, xe6<?> xe6) {
                wg6.b(xe6, "completion");
                c cVar = new c(this.this$0, xe6);
                cVar.p$ = (il6) obj;
                throw null;
                // return cVar;
            }

            @DexIgnore
            public final Object invoke(il6 obj, xe6 obj2) {
                throw null;
                // return ((c) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
            }

            @DexIgnore
            public final Object invokeSuspend(Object obj) {
                throw null;
                // ff6.a();
                // if (this.label == 0) {
                //     nc6.a(obj);
                //     b b = this.this$0.a();
                //     if (b != null) {
                //         b a = this.this$0.a;
                //         if (a != null) {
                //             if (a.a() < b.a()) {
                //                 if (b.c() == 3) {
                //                     ILocalFLogger local = FLogger.INSTANCE.getLocal();
                //                     String b2 = DianaNotificationComponent.this.b();
                //                     local.d(b2, "processMissedCall - number = " + b.b());
                //                     DianaNotificationComponent.this.a(b.b(), new Date(), f.MISSED);
                //                 } else if (b.c() == 5 || b.c() == 6 || b.c() == 1) {
                //                     ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                //                     String b3 = DianaNotificationComponent.this.b();
                //                     local2.d(b3, "processHangUpCall - number = " + b.b());
                //                     DianaNotificationComponent.this.a(b.b(), new Date(), f.PICKED);
                //                 }
                //             }
                //             this.this$0.a = b;
                //         } else {
                //             wg6.a();
                //             throw null;
                //         }
                //     }
                //     return cd6.a;
                // }
                // throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
        }

        @DexIgnore
        public e() {
            super(null);
            throw null;
            // rm6 unused = ik6.b(jl6.a(zl6.a()), null, null, new a(this, null), 3, null);
        }

        @DexIgnore
        public void onChange(boolean z, Uri uri) {
            b();
        }

        /* JADX WARN: Type inference failed for: r1v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.Context, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        @DexIgnore
        public final void b() {
            xx5.a aVar = xx5.a;
            Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            if (!aVar.d(applicationContext)) {
                FLogger.INSTANCE.getLocal().d(DianaNotificationComponent.this.b(), "processMissedCall() is not executed because permissions has not granted");
            } else {
                throw null;
                // rm6 unused = ik6.b(jl6.a(zl6.a()), null, null, new c(this, null), 3, null);
            }
        }

        @DexIgnore
        public void onChange(boolean z) {
            b();
        }

        /* JADX WARN: Type inference failed for: r4v1, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x0065, code lost:
            r1 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
            com.fossil.yf6.a(r4, r0);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x0069, code lost:
            throw r1;
         */
        @DexIgnore
        @SuppressLint({"MissingPermission"})
        public final b a() {
            Cursor query = PortfolioApp.get.instance().getContentResolver().query(CallLog.Calls.CONTENT_URI, new String[]{FieldType.FOREIGN_ID_FIELD_SUFFIX, "number", HardwareLog.COLUMN_DATE, "type"}, null, null, "date DESC LIMIT 1");
            if (query != null) {
                if (query.moveToFirst()) {
                    long j = query.getLong(query.getColumnIndex(FieldType.FOREIGN_ID_FIELD_SUFFIX));
                    String string = query.getString(query.getColumnIndex("number"));
                    int i = query.getInt(query.getColumnIndex("type"));
                    long j2 = query.getLong(query.getColumnIndex(HardwareLog.COLUMN_DATE));
                    query.close();
                    wg6.a((Object) string, "number");
                    b bVar = new b(this, j, string, i, j2);
                    try {
                        yf6.a(query, null);
                        return bVar;
                    } catch (Exception e) {
                        e.printStackTrace();
                        FLogger.INSTANCE.getLocal().e(DianaNotificationComponent.this.b(), e.getMessage());
                    }
                } else {
                    query.close();
                    throw null;
                    // cd6 cd6 = cd6.a;
                    // yf6.a(query, null);
                }
            }
            return null;
        }
    }

    @DexIgnore
    public enum f {
        RINGING,
        PICKED,
        MISSED,
        END
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class g extends NotificationMonitor {
        @DexIgnore
        public String l;

        @DexIgnore
        public g() {
        }

        @DexIgnore
        public final g clone() {
            g gVar = new g();
            super.c(d());
            super.a(a());
            super.b(b());
            super.d(e());
            super.e(f());
            super.f(g());
            super.a(h());
            super.a(i());
            super.c(k());
            super.b(j());
            gVar.l = this.l;
            return gVar;
        }

        @DexIgnore
        public final String l() {
            return this.l;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public g(String str, String str2, String str3, String str4, Date date) {
            this();
            wg6.b(str, "packageName");
            wg6.b(str2, RemoteFLogger.MESSAGE_SENDER_KEY);
            wg6.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            wg6.b(str4, "message");
            wg6.b(date, "startDate");
            c(a(date.getTime(), null));
            b(str);
            f(str2);
            d(str2);
            e(str4);
            a(date.getTime());
            this.l = str3;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class h extends NotificationMonitor {
        @DexIgnore
        public String l;

        @DexIgnore
        public h() {
        }

        @DexIgnore
        public final String l() {
            return this.l;
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public h(String str, String str2, String str3, long j) {
            this();
            wg6.b(str, RemoteFLogger.MESSAGE_SENDER_KEY);
            wg6.b(str2, "message");
            wg6.b(str3, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
            c(a(j, null));
            b(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
            f(str);
            d(str);
            e(str2);
            this.l = str3;
            a(j);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {388}, m = "checkConditionsToSendToDevice")
    public static final class j extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(DianaNotificationComponent dianaNotificationComponent, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a(null, null, false, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {319, 324, 331}, m = "handleNewLogic")
    public static final class k extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(DianaNotificationComponent dianaNotificationComponent, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.a((NotificationMonitor) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {267}, m = "handleNotificationAdded")
    public static final class l extends jf6 {
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public l(DianaNotificationComponent dianaNotificationComponent, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.b((NotificationMonitor) null, this);
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$onNotificationAppRemoved$1", f = "DianaNotificationComponent.kt", l = {}, m = "invokeSuspend")
    public static final class m extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public m(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            m mVar = new m(this.this$0, this.$sbn, xe6);
            mVar.p$ = (il6) obj;
            return (xe6)mVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            // return ((m) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            ff6.a();
            if (this.label == 0) {
                nc6.a(obj);
                NotificationMonitor notificationMonitor = new NotificationMonitor(this.$sbn);
                b bVar = DianaNotificationComponent.n.a().get(notificationMonitor.b());
                if (bVar != null) {
                    notificationMonitor.d(bVar.a().getAppName());
                }
                this.this$0.a(new NotificationMonitor(this.$sbn));
                return cd6.a;
            }
            throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processAppNotification$1", f = "DianaNotificationComponent.kt", l = {126}, m = "invokeSuspend")
    public static final class n extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ StatusBarNotification $sbn;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public n(DianaNotificationComponent dianaNotificationComponent, StatusBarNotification statusBarNotification, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$sbn = statusBarNotification;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            n nVar = new n(this.this$0, this.$sbn, xe6);
            nVar.p$ = (il6) obj;
            return (xe6)nVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            // return ((n) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
         arg types: [com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$n]
         candidates:
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                NotificationMonitor notificationMonitor = new NotificationMonitor(this.$sbn);
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                this.L$0 = il6;
                this.L$1 = notificationMonitor;
                this.label = 1;
                if (dianaNotificationComponent.a((NotificationMonitor) notificationMonitor, (xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                NotificationMonitor notificationMonitor2 = (NotificationMonitor) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processPhoneCall$1", f = "DianaNotificationComponent.kt", l = {82, 108}, m = "invokeSuspend")
    public static final class o extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ Date $startTime;
        @DexIgnore
        public /* final */ /* synthetic */ f $state;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public o(DianaNotificationComponent dianaNotificationComponent, f fVar, String str, Date date, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$state = fVar;
            this.$phoneNumber = str;
            this.$startTime = date;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            o oVar = new o(this.this$0, this.$state, this.$phoneNumber, this.$startTime, xe6);
            oVar.p$ = (il6) obj;
            return (xe6)oVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            // return ((o) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX WARN: Type inference failed for: r0v2, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARN: Type inference failed for: r2v3, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARN: Type inference failed for: r2v6, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX WARN: Type inference failed for: r0v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, boolean):boolean
         arg types: [com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, int]
         candidates:
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, java.lang.String, long):com.fossil.rm6
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, java.util.Date, com.portfolio.platform.service.notification.DianaNotificationComponent$f):com.fossil.rm6
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Calendar, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Date, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
         arg types: [com.portfolio.platform.service.notification.DianaNotificationComponent$g, com.portfolio.platform.service.notification.DianaNotificationComponent$o]
         candidates:
          com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
         arg types: [android.content.Context, java.lang.String]
         candidates:
          com.fossil.wg6.a(int, int):int
          com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
          com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
          com.fossil.wg6.a(int, java.lang.String):void
          com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
          com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            throw null;
            // Object a = ff6.a();
            // int i = this.label;
            // if (i == 0) {
            //     nc6.a(obj);
            //     il6 il6 = this.p$;
            //     ILocalFLogger local = FLogger.INSTANCE.getLocal();
            //     String b = this.this$0.b();
            //     local.d(b, "process phone call state " + this.$state + " phoneNumber " + this.$phoneNumber);
            //     int i2 = jq4.a[this.$state.ordinal()];
            //     if (i2 != 1) {
            //         if (i2 == 2) {
            //             g a2 = this.this$0.e;
            //             if (a2 != null && wg6.a((Object) a2.l(), (Object) this.$phoneNumber)) {
            //                 g gVar = new g(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
            //                 gVar.c(a2.d());
            //                 gVar.a(a2.a());
            //                 this.this$0.a(gVar);
            //                 this.this$0.e = (g) null;
            //             }
            //             DianaNotificationComponent dianaNotificationComponent = this.this$0;
            //             Context applicationContext = PortfolioApp.get.instance().getApplicationContext();
            //             wg6.a((Object) applicationContext, "PortfolioApp.instance.applicationContext");
            //             dianaNotificationComponent.b(applicationContext);
            //         } else if (i2 == 3) {
            //             if (!this.this$0.a(this.$phoneNumber, true)) {
            //                 return cd6.a;
            //             }
            //             g a3 = this.this$0.e;
            //             if (a3 != null && wg6.a((Object) a3.l(), (Object) this.$phoneNumber)) {
            //                 g gVar2 = new g(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), "", this.$phoneNumber, "", this.$startTime);
            //                 gVar2.a(a3.a());
            //                 gVar2.c(a3.d());
            //                 this.this$0.a(gVar2);
            //                 this.this$0.e = (g) null;
            //             }
            //             String a4 = this.this$0.d(this.$phoneNumber);
            //             DianaNotificationComponent dianaNotificationComponent2 = this.this$0;
            //             String packageName = DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName();
            //             String str = this.$phoneNumber;
            //             Calendar instance = Calendar.getInstance();
            //             wg6.a((Object) instance, "Calendar.getInstance()");
            //             Date time = instance.getTime();
            //             wg6.a((Object) time, "Calendar.getInstance().time");
            //             g gVar3 = new g(packageName, a4, str, "Missed Call", time);
            //             this.L$0 = il6;
            //             this.L$1 = a4;
            //             this.label = 2;
            //             if (dianaNotificationComponent2.b((NotificationMonitor) gVar3, (xe6<? super cd6>) this) == a) {
            //                 return a;
            //             }
            //         }
            //     } else if (!this.this$0.a(this.$phoneNumber, true)) {
            //         return cd6.a;
            //     } else {
            //         DianaNotificationComponent dianaNotificationComponent3 = this.this$0;
            //         Context applicationContext2 = PortfolioApp.get.instance().getApplicationContext();
            //         wg6.a((Object) applicationContext2, "PortfolioApp.instance.applicationContext");
            //         dianaNotificationComponent3.b(applicationContext2);
            //         DianaNotificationComponent dianaNotificationComponent4 = this.this$0;
            //         Context applicationContext3 = PortfolioApp.get.instance().getApplicationContext();
            //         wg6.a((Object) applicationContext3, "PortfolioApp.instance.applicationContext");
            //         dianaNotificationComponent4.a(applicationContext3);
            //         String a5 = this.this$0.d(this.$phoneNumber);
            //         g a6 = this.this$0.e;
            //         if (a6 != null) {
            //             this.this$0.a(a6);
            //         }
            //         g gVar4 = new g(DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName(), a5, this.$phoneNumber, "Incoming Call", this.$startTime);
            //         this.this$0.e = gVar4.clone();
            //         DianaNotificationComponent dianaNotificationComponent5 = this.this$0;
            //         this.L$0 = il6;
            //         this.L$1 = a5;
            //         this.L$2 = gVar4;
            //         this.label = 1;
            //         if (dianaNotificationComponent5.b((NotificationMonitor) gVar4, (xe6<? super cd6>) this) == a) {
            //             return a;
            //         }
            //     }
            //     return cd6.a;
            // } else if (i == 1) {
            //     g gVar5 = (g) this.L$2;
            //     String str2 = (String) this.L$1;
            //     il6 il62 = (il6) this.L$0;
            //     nc6.a(obj);
            //     return cd6.a;
            // } else if (i == 2) {
            //     String str3 = (String) this.L$1;
            //     il6 il63 = (il6) this.L$0;
            //     nc6.a(obj);
            // } else {
            //     throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            // }
            // DianaNotificationComponent dianaNotificationComponent6 = this.this$0;
            // Context applicationContext4 = PortfolioApp.get.instance().getApplicationContext();
            // wg6.a((Object) applicationContext4, "PortfolioApp.instance.applicationContext");
            // dianaNotificationComponent6.b(applicationContext4);
            // return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent$processSmsMms$1", f = "DianaNotificationComponent.kt", l = {121}, m = "invokeSuspend")
    public static final class p extends sf6 implements ig6<il6, xe6<? super cd6>, Object> {
        @DexIgnore
        public /* final */ /* synthetic */ String $content;
        @DexIgnore
        public /* final */ /* synthetic */ String $phoneNumber;
        @DexIgnore
        public /* final */ /* synthetic */ long $receivedTime;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public int label;
        @DexIgnore
        public il6 p$;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(DianaNotificationComponent dianaNotificationComponent, String str, String str2, long j, xe6 xe6) {
            super(2, xe6);
            this.this$0 = dianaNotificationComponent;
            this.$phoneNumber = str;
            this.$content = str2;
            this.$receivedTime = j;
        }

        @DexIgnore
        public final xe6<cd6> create(Object obj, xe6<?> xe6) {
            wg6.b(xe6, "completion");
            p pVar = new p(this.this$0, this.$phoneNumber, this.$content, this.$receivedTime, xe6);
            pVar.p$ = (il6) obj;
            return (xe6)pVar;
        }

        @DexIgnore
        public final Object invoke(il6 obj, xe6 obj2) {
            throw null;
            // return ((p) create(obj, (xe6) obj2)).invokeSuspend(cd6.a);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, boolean):boolean
         arg types: [com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, int]
         candidates:
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, java.lang.String, long):com.fossil.rm6
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, java.util.Date, com.portfolio.platform.service.notification.DianaNotificationComponent$f):com.fossil.rm6
          com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
         arg types: [com.portfolio.platform.service.notification.DianaNotificationComponent$h, com.portfolio.platform.service.notification.DianaNotificationComponent$p]
         candidates:
          com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
          com.portfolio.platform.service.notification.DianaNotificationComponent.b(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object */
        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            Object a = ff6.a();
            int i = this.label;
            if (i == 0) {
                nc6.a(obj);
                il6 il6 = this.p$;
                FLogger.INSTANCE.getLocal().d(this.this$0.b(), "Process SMS/MMS by old solution");
                if (!this.this$0.a(this.$phoneNumber, false)) {
                    return cd6.a;
                }
                String a2 = this.this$0.d(this.$phoneNumber);
                DianaNotificationComponent dianaNotificationComponent = this.this$0;
                h hVar = new h(a2, this.$content, this.$phoneNumber, this.$receivedTime);
                this.L$0 = il6;
                this.L$1 = a2;
                this.label = 1;
                if (dianaNotificationComponent.b((NotificationMonitor) hVar, (xe6<? super cd6>) this) == a) {
                    return a;
                }
            } else if (i == 1) {
                String str = (String) this.L$1;
                il6 il62 = (il6) this.L$0;
                nc6.a(obj);
            } else {
                throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
            }
            return cd6.a;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    @lf6(c = "com.portfolio.platform.service.notification.DianaNotificationComponent", f = "DianaNotificationComponent.kt", l = {444}, m = "sendToDevice")
    public static final class q extends jf6 {
        @DexIgnore
        public long J$0;
        @DexIgnore
        public Object L$0;
        @DexIgnore
        public Object L$1;
        @DexIgnore
        public Object L$2;
        @DexIgnore
        public Object L$3;
        @DexIgnore
        public Object L$4;
        @DexIgnore
        public Object L$5;
        @DexIgnore
        public Object L$6;
        @DexIgnore
        public Object L$7;
        @DexIgnore
        public boolean Z$0;
        @DexIgnore
        public int label;
        @DexIgnore
        public /* synthetic */ Object result;
        @DexIgnore
        public /* final */ /* synthetic */ DianaNotificationComponent this$0;

        @DexIgnore
        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public q(DianaNotificationComponent dianaNotificationComponent, xe6 xe6) {
            super(xe6);
            this.this$0 = dianaNotificationComponent;
        }

        @DexIgnore
        public final Object invokeSuspend(Object obj) {
            this.result = obj;
            this.label |= Integer.MIN_VALUE;
            return this.this$0.b(null, null, false, this);
        }
    }

    /*
    static {
        HashMap<String, i> hashMap = new HashMap<>();
        hashMap.put("com.facebook.orca", new i("com.facebook.orca", "SMS"));
        m = hashMap;
    }
    */

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public DianaNotificationComponent(an4 an4, DNDSettingsDatabase dNDSettingsDatabase, QuickResponseRepository quickResponseRepository, NotificationSettingsDatabase notificationSettingsDatabase) {
        wg6.b(an4, "mSharedPreferencesManager");
        wg6.b(dNDSettingsDatabase, "mDNDndSettingsDatabase");
        wg6.b(quickResponseRepository, "mQuickResponseRepository");
        wg6.b(notificationSettingsDatabase, "mNotificationSettingsDatabase");
        this.h = an4;
        this.i = dNDSettingsDatabase;
        this.j = quickResponseRepository;
        this.k = notificationSettingsDatabase;
        String simpleName = DianaNotificationComponent.class.getSimpleName();
        wg6.a((Object) simpleName, "DianaNotificationComponent::class.java.simpleName");
        this.a = simpleName;
        n.a();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Date, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0104, code lost:
        if (r6 < r0) goto L_0x012d;
     */
    @DexIgnore
    public final boolean c() {
        if (this.h.F()) {
            Calendar instance = Calendar.getInstance();
            wg6.a((Object) instance, "Calendar.getInstance()");
            Date o2 = bk4.o(instance.getTime());
            wg6.a((Object) o2, "DateHelper.getStartOfDay\u2026endar.getInstance().time)");
            long time = o2.getTime();
            Calendar instance2 = Calendar.getInstance();
            wg6.a((Object) instance2, "Calendar.getInstance()");
            Date time2 = instance2.getTime();
            wg6.a((Object) time2, "Calendar.getInstance().time");
            long time3 = (time2.getTime() - time) / ((long) 60000);
            List<DNDScheduledTimeModel> listDNDScheduledTimeModel = this.i.getDNDScheduledTimeDao().getListDNDScheduledTimeModel();
            if (!listDNDScheduledTimeModel.isEmpty()) {
                for (DNDScheduledTimeModel dNDScheduledTimeModel : listDNDScheduledTimeModel) {
                    int component2 = dNDScheduledTimeModel.component2();
                    if (dNDScheduledTimeModel.component3() == 0) {
                        this.c = component2;
                    } else {
                        this.d = component2;
                    }
                }
            }
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "currentMinutesOfDay = " + a((int) time3) + " -- mDNDStartTime = " + a(this.c) + " -- mDNDEndTime = " + a(this.d));
            int i2 = this.d;
            int i3 = this.c;
            if (i2 > i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime greater than mDNDStartTime");
                long j2 = (long) this.d;
                if (((long) this.c) <= time3 && j2 >= time3) {
                    FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                    return true;
                }
            } else if (i2 < i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime smaller than mDNDStartTime");
                long j3 = (long) DateTimeConstants.MINUTES_PER_DAY;
                if (((long) this.c) > time3 || j3 < time3) {
                    long j4 = (long) this.d;
                    if (0 <= time3) {
                    }
                }
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            } else if (i2 == i3) {
                FLogger.INSTANCE.getLocal().d(this.a, "case mDNDEndTime equals mDNDStartTime");
                FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is valid - ignore notification ");
                return true;
            }
        }
        FLogger.INSTANCE.getLocal().d(this.a, "isDNDScheduleTimeValid() - DND Schedule is not valid - show notification ");
        return false;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX WARN: Type inference failed for: r2v5, types: [com.portfolio.platform.PortfolioApp, android.app.Application] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x008e, code lost:
        if (r0 != null) goto L_0x0090;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0090, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00b3, code lost:
        if (r0 != null) goto L_0x0090;
     */
    @DexIgnore
    public final String d(String str) {
        Exception e = null;
        if (xx5.a.e(PortfolioApp.get.instance())) {
            Cursor cursor = null;
            try {
                cursor = PortfolioApp.get.instance().getContentResolver().query(Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(str)), new String[]{"display_name"}, null, null, null);
                if (cursor == null || cursor.getCount() <= 0) {
                    ILocalFLogger local = FLogger.INSTANCE.getLocal();
                    String str2 = this.a;
                    local.d(str2, "readDisplayNameFromPhoneNumber(), number=" + str + ", no display name.");
                } else {
                    cursor.moveToFirst();
                    String string = cursor.getString(0);
                    wg6.a((Object) string, "c.getString(0)");
                    try {
                        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
                        String str3 = this.a;
                        local2.d(str3, "readDisplayNameFromPhoneNumber(), number=" + str + ", displayName=" + string);
                        str = string;
                    } catch (Exception e2) {
                        e = e2;
                        str = string;
                        try {
                            ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
                            String str4 = this.a;
                            local3.d(str4, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e);
                        } catch (Throwable th) {
                            if (cursor != null) {
                                cursor.close();
                            }
                            throw th;
                        }
                    }
                }
            } catch (Exception e3) {
                e = e3;
                ILocalFLogger local32 = FLogger.INSTANCE.getLocal();
                String str42 = this.a;
                local32.d(str42, "readDisplayNameFromPhoneNumber(), exception while read display name, ex=" + e);
            }
        } else {
            ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
            String str5 = this.a;
            local4.d(str5, "readDisplayNameFromPhoneNumber(), number=" + str + ", no read contact permission.");
        }
        return str;
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class c {
        @DexIgnore
        public /* final */ List<NotificationMonitor> a; // = new ArrayList();
        @DexIgnore
        public int b; // = this.c.p();
        @DexIgnore
        public /* final */ an4 c;

        @DexIgnore
        public c(an4 an4) {
            wg6.b(an4, "mSharedPref");
            this.c = an4;
        }

        @DexIgnore
        public final void a(int i) {
            if (i >= Integer.MAX_VALUE) {
                i = 0;
            }
            this.b = i;
            this.c.c(this.b);
        }

        @DexIgnore
        public final int a() {
            int i = this.b;
            if (i < 0) {
                return 0;
            }
            return i;
        }

        /* JADX WARN: Type inference failed for: r2v1, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
        /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a7, code lost:
            return r11;
         */
        @DexIgnore
        public final synchronized NotificationMonitor a(NotificationMonitor notificationMonitor) {
            NotificationMonitor notificationMonitor2;
            boolean z;
            wg6.b(notificationMonitor, "notificationStatus");
            if (this.a.contains(notificationMonitor)) {
                notificationMonitor = null;
            } else if (xj6.b(notificationMonitor.b(), Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()), true)) {
                Iterator<NotificationMonitor> it = this.a.iterator();
                while (true) {
                    if (!it.hasNext()) {
                        notificationMonitor2 = null;
                        break;
                    }
                    notificationMonitor2 = it.next();
                    NotificationMonitor notificationMonitor3 = notificationMonitor2;
                    if (!xj6.b(notificationMonitor3.f(), notificationMonitor.f(), true) || (notificationMonitor3.h() != notificationMonitor.h() && notificationMonitor.h() - notificationMonitor3.h() > 1)) {
                        z = false;
                        continue;
                    } else {
                        z = true;
                        // continue;
                    }
                    if (z) {
                        break;
                    }
                }
                NotificationMonitor notificationMonitor4 = (NotificationMonitor) notificationMonitor2;
                if (notificationMonitor4 != null) {
                    notificationMonitor4.c(notificationMonitor.d());
                    notificationMonitor4.a(notificationMonitor.h());
                    return null;
                }
                notificationMonitor.a(a());
                this.a.add(notificationMonitor);
                a(a() + 1);
            } else {
                notificationMonitor.a(a());
                this.a.add(notificationMonitor);
                a(a() + 1);
            }
        throw null;
        }

        @DexIgnore
        public final synchronized List<NotificationMonitor> a(String str, String str2) {
            ArrayList arrayList;
            wg6.b(str, "realId");
            wg6.b(str2, "packageName");
            List<NotificationMonitor> list = this.a;
            arrayList = new ArrayList();
            for (NotificationMonitor t : list) {
                NotificationMonitor notificationMonitor = (NotificationMonitor) t;
                // if (wg6.a(notificationMonitor.d(), str) && wg6.a(notificationMonitor.b(), str2)) {
                    arrayList.add(t);
                // }
            }
            this.a.removeAll(arrayList);
            return arrayList;
        }
    }

    @DexIgnore // @DexEdit(defaultAction = DexAction.IGNORE)
    public static final class i {
        @DexIgnore
        public String a;

        @DexIgnore
        public i() {
            this.a = "";
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.fossil.yj6.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          com.fossil.yj6.a(java.lang.CharSequence, int, char):java.lang.CharSequence
          com.fossil.yj6.a(java.lang.String, char, java.lang.String):java.lang.String
          com.fossil.yj6.a(java.lang.String, int, char):java.lang.String
          com.fossil.yj6.a(java.lang.String, java.lang.CharSequence, java.lang.CharSequence):java.lang.String
          com.fossil.yj6.a(java.lang.String, java.lang.String, java.lang.String):java.lang.String
          com.fossil.xj6.a(java.lang.String, java.lang.String, boolean):boolean
          com.fossil.uj6.a(java.lang.Appendable, java.lang.Object, com.fossil.hg6):void
          com.fossil.yj6.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean */
        @DexIgnore
        public final boolean a(String str) {
            wg6.b(str, "realId");
            return yj6.a((CharSequence) str, (CharSequence) this.a, true);
        }

        @DexIgnore
        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public i(String str, String str2) {
            this();
            wg6.b(str, "packageName");
            wg6.b(str2, "tag");
            this.a = str2;
        }
    }

    @DexIgnore
    public final String b() {
        return this.a;
    }

    @DexIgnore
    public final rm6 b(StatusBarNotification statusBarNotification) {
        wg6.b(statusBarNotification, "sbn");
        throw null;
        // return ik6.b(jl6.a(zl6.a()), null, null, new n(this, statusBarNotification, null), 3, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.appfilter.AppFilter, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
     arg types: [com.fossil.NotificationMonitor, ?[OBJECT, ARRAY]]
     candidates:
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
     arg types: [com.fossil.NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b]
     candidates:
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
     arg types: [com.fossil.NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType]
     candidates:
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object b(NotificationMonitor dVar, xe6<? super cd6> xe6) {
        l lVar;
        int i2;
        Object obj;
        if (xe6 instanceof l) {
            lVar = (l) xe6;
            int i3 = lVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                lVar.label = i3 - Integer.MIN_VALUE;
                Object obj2 = lVar.result;
                Object a2 = ff6.a();
                i2 = lVar.label;
                if (i2 != 0) {
                    nc6.a(obj2);
                    FLogger.INSTANCE.getLocal().d(this.a, "handleNotificationAdded " + dVar);
                    b bVar = n.a().get(dVar.b());
                    List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                    boolean z = false;
                    NotificationBaseObj.ANotificationType aNotificationType = null;
                    if (bVar == null) {
                        wg6.a((Object) allAppFilters, "appNotificationFilters");
                        Iterator it = allAppFilters.iterator();
                        while (true) {
                            if (!it.hasNext()) {
                                obj = null;
                                break;
                            }
                            obj = it.next();
                            AppFilter appFilter = (AppFilter) obj;
                            wg6.a((Object) appFilter, "it");
                            if (hf6.a(wg6.a((Object) appFilter.getType(), (Object) dVar.b())).booleanValue()) {
                                break;
                            }
                        }
                        if (((AppFilter) obj) == null) {
                            FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
                            a((NotificationMonitor) dVar, (NotificationBaseObj.ANotificationType) null);
                            return cd6.a;
                        }
                        FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but not in list icon supported");
                        bVar = new b(17, new DianaNotificationObj.AApplicationName(PortfolioApp.get.instance().f(dVar.b()), dVar.b(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
                    } else {
                        z = true;
                    }
                    if ((dVar.j() || !dVar.k()) && a((NotificationMonitor) dVar, bVar) && !dVar.i()) {
                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
                        a((NotificationMonitor) dVar, bVar.a().getNotificationType());
                        lVar.L$0 = this;
                        lVar.L$1 = dVar;
                        lVar.L$2 = bVar;
                        lVar.L$3 = allAppFilters;
                        lVar.label = 1;
                        if (a(dVar, bVar, z, lVar) == a2) {
                            return a2;
                        }
                    } else {
                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() app not supported............");
                        DianaNotificationObj.AApplicationName a3 = bVar.a();
                        if (a3 != null) {
                            aNotificationType = a3.getNotificationType();
                        }
                        a((NotificationMonitor) dVar, aNotificationType);
                    }
                } else if (i2 == 1) {
                    List list = (List) lVar.L$3;
                    b bVar2 = (b) lVar.L$2;
                    NotificationMonitor notificationMonitor = (NotificationMonitor) lVar.L$1;
                    DianaNotificationComponent dianaNotificationComponent = (DianaNotificationComponent) lVar.L$0;
                    nc6.a(obj2);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        lVar = new l(this, xe6);
        Object obj22 = lVar.result;
        Object a22 = ff6.a();
        i2 = lVar.label;
        if (i2 != 0) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final String a() {
        return PortfolioApp.get.instance().e();
    }

    @DexIgnore
    public final rm6 a(String str, Date date, f fVar) {
        wg6.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        wg6.b(date, "startTime");
        wg6.b(fVar, Constants.STATE);
        throw null;
        // return ik6.b(jl6.a(zl6.a()), null, null, new o(this, fVar, str, date, null), 3, null);
    }

    @DexIgnore
    public final rm6 a(String str, String str2, long j2) {
        wg6.b(str, PhoneFavoritesContact.COLUMN_PHONE_NUMBER);
        wg6.b(str2, "content");
        throw null;
        // return ik6.b(jl6.a(zl6.a()), null, null, new p(this, str, str2, j2, null), 3, null);
    }

    @DexIgnore
    public final String a(int i2) {
        if (i2 < 0) {
            return "invalid time";
        }
        StringBuilder sb = new StringBuilder();
        sb.append(i2 / 60);
        sb.append(':');
        sb.append(i2 % 60);
        return sb.toString();
    }

    @DexIgnore
    public final rm6 a(StatusBarNotification statusBarNotification) {
        wg6.b(statusBarNotification, "sbn");
        throw null;
        // return ik6.b(jl6.a(zl6.a()), null, null, new m(this, statusBarNotification, null), 3, null);
    }

    @DexIgnore
    public final boolean a(NotificationMonitor notificationMonitor, b bVar) {
        if (!bVar.b() && notificationMonitor.c() <= -2) {
            return false;
        }
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Queue<com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj>, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final void d() {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onDeviceConnected, notificationList = " + this.g);
        Queue<NotificationBaseObj> queue = this.g;
        wg6.a((Object) queue, "mDeviceNotifications");
        for (NotificationBaseObj notificationBaseObj : queue) {
            PortfolioApp c2 = PortfolioApp.get.instance();
            String a2 = a();
            wg6.a((Object) notificationBaseObj, "it");
            c2.b(a2, notificationBaseObj);
        }
    }

    @DexIgnore
    public final boolean a(String str, boolean z) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.k.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(z);
        boolean z2 = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z2 = a(str);
                } else if (i2 == 2) {
                    z2 = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowCallOrMessageEvent() - from sender " + str + " is " + z2 + " because type = type = " + i2);
        return z2;
    }

    @DexIgnore
    public final boolean a(String str) {
        List<ContactGroup> allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            if (contactGroup.getContactWithPhoneNumber(str) != null) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r7v4, types: [android.content.Context, com.portfolio.platform.PortfolioApp] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.appfilter.AppFilter, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
     arg types: [com.fossil.NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType]
     candidates:
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean
     arg types: [com.fossil.NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b]
     candidates:
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, java.lang.String):java.lang.String
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, android.content.Context):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent, com.portfolio.platform.service.notification.DianaNotificationComponent$g):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.fossil.NotificationMonitor, com.fossil.xe6<? super com.fossil.cd6>):java.lang.Object
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(int, boolean):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj$ANotificationType):void
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(java.lang.String, boolean):boolean
      com.portfolio.platform.service.notification.DianaNotificationComponent.a(com.portfolio.platform.service.notification.DianaNotificationComponent$NotificationMonitor, com.portfolio.platform.service.notification.DianaNotificationComponent$b):boolean */
    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0107  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0025  */
    public final /* synthetic */ Object a(NotificationMonitor dVar, xe6<? super cd6> xe6) {
        k kVar;
        int i2;
        b bVar = null;
        DianaNotificationComponent dianaNotificationComponent = null;
        boolean z;
        NotificationBaseObj.ANotificationType aNotificationType;
        Object obj;
        DianaNotificationObj.AApplicationName a2;
        if (xe6 instanceof k) {
            kVar = (k) xe6;
            int i3 = kVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                kVar.label = i3 - Integer.MIN_VALUE;
                Object obj2 = kVar.result;
                Object a3 = ff6.a();
                i2 = kVar.label;
                if (i2 != 0) {
                    nc6.a(obj2);
                    FLogger.INSTANCE.getLocal().d(this.a, "handleNewLogic " + dVar);
                    jh6 jh6 = new jh6();
                    fh6 fh6 = new fh6();
                    fh6.element = true;
                    if (wg6.a((Object) dVar.b(), (Object) Telephony.Sms.getDefaultSmsPackage(PortfolioApp.get.instance()))) {
                        i iVar = m.get(dVar.b());
                        if (iVar == null || iVar.a(dVar.d())) {
                            jh6.element = (b) n.a().get(DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName());
                            z = true;
                            if (jh6.element == null) {
                                List allAppFilters = zm4.p.a().a().getAllAppFilters(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
                                wg6.a((Object) allAppFilters, "appNotificationFilters");
                                Iterator it = allAppFilters.iterator();
                                while (true) {
                                    aNotificationType = null;
                                    if (!it.hasNext()) {
                                        obj = null;
                                        break;
                                    }
                                    obj = it.next();
                                    AppFilter appFilter = (AppFilter) obj;
                                    wg6.a((Object) appFilter, "it");
                                    if (hf6.a(wg6.a((Object) appFilter.getType(), (Object) dVar.b())).booleanValue()) {
                                        break;
                                    }
                                }
                                if (((AppFilter) obj) == null) {
                                    FLogger.INSTANCE.getLocal().d(this.a, "App is not selected by user");
                                    b bVar2 = (DianaNotificationComponent.b) jh6.element;
                                    if (!(bVar2 == null || (a2 = bVar2.a()) == null)) {
                                        aNotificationType = a2.getNotificationType();
                                    }
                                    a((NotificationMonitor) dVar, aNotificationType);
                                    return cd6.a;
                                }
                                FLogger.INSTANCE.getLocal().d(this.a, "App is selected by user but no supported icon");
                                fh6.element = false;
                                jh6.element = new b(17, new DianaNotificationObj.AApplicationName(PortfolioApp.get.instance().f(dVar.b()), dVar.b(), "general_white.bin", NotificationBaseObj.ANotificationType.NOTIFICATION), false, true);
                            }
                            bVar = (DianaNotificationComponent.b) jh6.element;
                            if (bVar != null) {
                                if (wg6.a(bVar.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL()) || wg6.a(bVar.a(), DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL())) {
                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() incoming call or miss called not supported............");
                                    return cd6.a;
                                }
                                if (z && a((NotificationMonitor) dVar, (b)jh6.element) && c(dVar.e())) {
                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported - handle for sms - new solution");
                                    boolean z2 = fh6.element;
                                    kVar.L$0 = this;
                                    kVar.L$1 = dVar;
                                    kVar.L$2 = jh6;
                                    kVar.L$3 = fh6;
                                    kVar.L$4 = bVar;
                                    kVar.label = 1;
                                    if (a(dVar, (b)jh6.element, z2, kVar) == a3) {
                                        return a3;
                                    }
                                } else if (z || ((!dVar.j() && dVar.k()) || !a((NotificationMonitor) dVar, (b)jh6.element) || dVar.i())) {
                                    if (wg6.a((Object) dVar.b(), (Object) "com.google.android.googlequicksearchbox")) {
                                        boolean z3 = fh6.element;
                                        kVar.L$0 = this;
                                        kVar.L$1 = dVar;
                                        kVar.L$2 = jh6;
                                        kVar.L$3 = fh6;
                                        kVar.L$4 = bVar;
                                        kVar.label = 3;
                                        if (a(dVar, (b)jh6.element, z3, kVar) == a3) {
                                            return a3;
                                        }
                                    }
                                    dianaNotificationComponent = this;
                                } else {
                                    FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() supported............");
                                    boolean z4 = fh6.element;
                                    kVar.L$0 = this;
                                    kVar.L$1 = dVar;
                                    kVar.L$2 = jh6;
                                    kVar.L$3 = fh6;
                                    kVar.L$4 = bVar;
                                    kVar.label = 2;
                                    if (a(dVar, (b)jh6.element, z4, kVar) == a3) {
                                        return a3;
                                    }
                                }
                                dianaNotificationComponent = this;
                                dianaNotificationComponent.a((NotificationMonitor) dVar, bVar.a().getNotificationType());
                            }
                            return cd6.a;
                        }
                        jh6.element = (b) n.a().get(dVar.b());
                    } else {
                        jh6.element = (b) n.a().get(dVar.b());
                    }
                    z = false;
                    if (jh6.element == null) {
                    }
                    bVar = (b)jh6.element;
                    if (bVar != null) {
                    }
                    return cd6.a;
                } else if (i2 == 1 || i2 == 2) {
                    fh6 fh62 = (fh6) kVar.L$3;
                    jh6 jh62 = (jh6) kVar.L$2;
                    dianaNotificationComponent = (DianaNotificationComponent) kVar.L$0;
                    nc6.a(obj2);
                    bVar = (b) kVar.L$4;
                    dVar = (NotificationMonitor) kVar.L$1;
                    dianaNotificationComponent.a((NotificationMonitor) dVar, bVar.a().getNotificationType());
                    return cd6.a;
                } else if (i2 == 3) {
                    fh6 fh63 = (fh6) kVar.L$3;
                    jh6 jh63 = (jh6) kVar.L$2;
                    dianaNotificationComponent = (DianaNotificationComponent) kVar.L$0;
                    nc6.a(obj2);
                    bVar = (b) kVar.L$4;
                    dVar = (NotificationMonitor) kVar.L$1;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "processNotificationAdd() app not supported............");
                dianaNotificationComponent.a((NotificationMonitor) dVar, bVar.a().getNotificationType());
                return cd6.a;
            }
        }
        kVar = new k(this, xe6);
        Object obj22 = kVar.result;
        Object a32 = ff6.a();
        i2 = kVar.label;
        if (i2 != 0) {
        }
        FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "processNotificationAdd() app not supported............");
        dianaNotificationComponent.a((NotificationMonitor) dVar, bVar.a().getNotificationType());
        return cd6.a;
    }

    @DexIgnore
    public final boolean c(String str) {
        int i2;
        NotificationSettingsModel notificationSettingsWithIsCallNoLiveData = this.k.getNotificationSettingsDao().getNotificationSettingsWithIsCallNoLiveData(false);
        boolean z = true;
        if (notificationSettingsWithIsCallNoLiveData != null) {
            i2 = notificationSettingsWithIsCallNoLiveData.getSettingsType();
            if (i2 != 0) {
                if (i2 == 1) {
                    z = b(str);
                } else if (i2 == 2) {
                    z = false;
                }
            }
        } else {
            i2 = 0;
        }
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local.d(str2, "isAllowMessageFromContactName() - from sender " + str + " is " + z + ", type = type = " + i2);
        return z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [com.fossil.wearables.fsl.contact.ContactGroup, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final boolean b(String str) {
        List<ContactGroup> allContactGroups = zm4.p.a().b().getAllContactGroups(MFDeviceFamily.DEVICE_FAMILY_DIANA.getValue());
        if (allContactGroups.isEmpty()) {
            return false;
        }
        for (ContactGroup contactGroup : allContactGroups) {
            wg6.a((Object) contactGroup, "contactGroup");
            Object obj = contactGroup.getContacts().get(0);
            wg6.a(obj, "contactGroup.contacts[0]");
            if (xj6.b(((Contact) obj).getDisplayName(), str, true)) {
                return true;
            }
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Queue<com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj>, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final void c(int i2) {
        FLogger.INSTANCE.getLocal().d(this.a, "removeNotification");
        Queue<NotificationBaseObj> queue = this.g;
        wg6.a((Object) queue, "mDeviceNotifications");
        synchronized (queue) {
            Iterator<NotificationBaseObj> it = this.g.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().getUid() == i2) {
                        ILocalFLogger local = FLogger.INSTANCE.getLocal();
                        String str = this.a;
                        local.d(str, "notification removed, id = " + i2);
                        it.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
            throw null;
            // cd6 cd6 = cd6.a;
        }
    }

    @DexAdd
    public class AlelecSettingsListener implements SharedPreferences.OnSharedPreferenceChangeListener {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            Context ctx = PortfolioApp.get.instance().getApplicationContext();
            dndSettingEnabled = AlelecPrefs.getAndroidDND(ctx);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(ctx);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(ctx);
        }
    }

    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public AlelecSettingsListener dndSettingsListener = null;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public Boolean dndSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public static Boolean dndHighSettingEnabled = false;
    @DexAdd
    @SuppressWarnings("WeakerAccess")
    public Boolean emptyNotificationsEnabled = false;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Boolean, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexWrap
    public final /* synthetic */ Object b(NotificationMonitor dVar, b bVar, boolean z, xe6<? super cd6> xe6) {
        if (dndSettingsListener == null) {
            Context context = PortfolioApp.get.instance().getApplicationContext();
            dndSettingsListener = new AlelecSettingsListener();
            AlelecPrefs.registerAndroidDND(context, dndSettingsListener );
            AlelecPrefs.registerEmptyNotifications(context, dndSettingsListener );
            dndSettingEnabled = AlelecPrefs.getAndroidDND(context);
            dndHighSettingEnabled = AlelecPrefs.getAndroidDND_HIGH(context);
            emptyNotificationsEnabled = AlelecPrefs.getEmptyNotifications(context);
        }
        FLogger.INSTANCE.getLocal().v(this.a, "emptyNotificationsEnabled: " + emptyNotificationsEnabled + ", dndSettingEnabled: " + dndSettingEnabled + ", notif.dnd: " + dVar.dnd);

        if (dVar.dnd && dndSettingEnabled) {
            FLogger.INSTANCE.getLocal().v(this.a, "Skip this notification from " + dVar.b() + " since DND is enabled");
            return cd6.a;
        }
        if (emptyNotificationsEnabled && TextUtils.isEmpty(dVar.f())) {
            // Allow sending notifications with empty message
            dVar.f = " ";
        }
        return b(dVar, bVar, z, xe6);
    }


    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b  */
    public final /* synthetic */ Object b(NotificationMonitor dVar, b bVar, boolean z, xe6<? super cd6> xe6, @DexIgnore Void tag) {
        q qVar;
        int i2;
        b bVar2 = null;
        long j2;
        List list = null;
        String str = null;
        DianaNotificationComponent dianaNotificationComponent = null;
        String str2 = null;
        String str3 = null;
        Object obj = null;
        String str4;
        Boolean bool;
        NotificationMonitor dVar2 = dVar;
        if (xe6 instanceof q) {
            qVar = (q) xe6;
            int i3 = qVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                qVar.label = i3 - Integer.MIN_VALUE;
                Object obj2 = qVar.result;
                Object a2 = ff6.a();
                i2 = qVar.label;
                if (i2 != 0) {
                    nc6.a(obj2);
                    FLogger.INSTANCE.getLocal().d(this.a, "sendToDevice() - isSupportedIcon " + z + " appName " + bVar.a().getAppName() + " sender = " + dVar.e() + " - text = " + dVar.f());
                    if (TextUtils.isEmpty(dVar.f())) {
                        FLogger.INSTANCE.getLocal().d(this.a, "Skip this notification from " + dVar.b() + " since content is empty");
                        return cd6.a;
                    }
                    if (z) {
                        str4 = dVar.e();
                    } else {
                        str4 = bVar.a().getAppName();
                    }
                    str = str4;
                    if (z) {
                        str2 = dVar.f();
                    } else {
                        str2 = dVar.e() + ':' + dVar.f();
                    }
                    list = new ArrayList();
                    if (!DeviceHelper.o.l() || !wg6.a((Object) dVar.b(), (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName())) {
                        list.add(NotificationBaseObj.ANotificationFlag.IMPORTANT);
                    } else {
                        list.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_ACCEPT_CALL);
                        list.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REJECT_CALL);
                    }
                    DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL();
                    j2 = -1;
                    String packageName = bVar.a().getPackageName();
                    if (wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_INCOMING_CALL().getPackageName()) || wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getPHONE_MISSED_CALL().getPackageName()) || wg6.a((Object) packageName, (Object) DianaNotificationObj.AApplicationName.Companion.getMESSAGES().getPackageName())) {
                        if (dVar2 instanceof g) {
                            str3 = ((g) dVar2).l();
                        } else {
                            str3 = dVar2 instanceof h ? ((h) dVar2).l() : "";
                        }
                        FLogger.INSTANCE.getLocal().d(this.a, "Receive phone notification, number " + str3 + " isQuickResponseEnable " + this.h.O());
                        Boolean O = this.h.O();
                        wg6.a((Object) O, "mSharedPreferencesManager.isQuickResponseEnabled");
                        if (O.booleanValue()) {
                            if (str3 != null) {
                                bool = hf6.a(str3.length() > 0);
                            } else {
                                bool = null;
                            }
                            if (bool == null) {
                                wg6.a();
                                throw null;
                            } else if (bool.booleanValue()) {
                                list.add(NotificationBaseObj.ANotificationFlag.ALLOW_USER_REPLY_MESSAGE);
                                QuickResponseSender quickResponseSender = new QuickResponseSender(str3);
                                QuickResponseRepository quickResponseRepository = this.j;
                                qVar.L$0 = this;
                                qVar.L$1 = dVar2;
                                bVar2 = bVar;
                                qVar.L$2 = bVar2;
                                qVar.Z$0 = z;
                                qVar.L$3 = str;
                                qVar.L$4 = str2;
                                qVar.L$5 = list;
                                qVar.J$0 = -1;
                                qVar.L$6 = str3;
                                qVar.L$7 = quickResponseSender;
                                qVar.label = 1;
                                obj = quickResponseRepository.upsertQuickResponseSender(quickResponseSender, qVar);
                                if (obj == a2) {
                                    return a2;
                                }
                                dianaNotificationComponent = this;
                            }
                        }
                    }
                    bVar2 = bVar;
                    dianaNotificationComponent = this;
                    DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(dVar2.a(), bVar2.a().getNotificationType(), bVar2.a(), dVar2.g(), str, (int) j2, str2, list, hf6.a(dVar2.h()), null, null, 1536, null);
                    FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "sendDianaNotification " + dianaNotificationObj);
                    dianaNotificationComponent.g.offer(dianaNotificationObj);
                    PortfolioApp.get.instance().b(dianaNotificationComponent.a(), dianaNotificationObj);
                    return cd6.a;
                } else if (i2 == 1) {
                    QuickResponseSender quickResponseSender2 = (QuickResponseSender) qVar.L$7;
                    long j3 = qVar.J$0;
                    str = (String) qVar.L$3;
                    boolean z3 = qVar.Z$0;
                    dianaNotificationComponent = (DianaNotificationComponent) qVar.L$0;
                    nc6.a(obj2);
                    str3 = (String) qVar.L$6;
                    bVar2 = (b) qVar.L$2;
                    dVar2 = (NotificationMonitor) qVar.L$1;
                    list = (List) qVar.L$5;
                    obj = obj2;
                    str2 = (String) qVar.L$4;
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                j2 = ((Number) obj).longValue();
                FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "Update sender with number " + str3 + " id " + j2);
                DianaNotificationObj dianaNotificationObj2 = new DianaNotificationObj(dVar2.a(), bVar2.a().getNotificationType(), bVar2.a(), dVar2.g(), str, (int) j2, str2, list, hf6.a(dVar2.h()), null, null, 1536, null);
                FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "sendDianaNotification " + dianaNotificationObj2);
                dianaNotificationComponent.g.offer(dianaNotificationObj2);
                PortfolioApp.get.instance().b(dianaNotificationComponent.a(), dianaNotificationObj2);
                return cd6.a;
            }
        }
        qVar = new q(this, xe6);
        Object obj22 = qVar.result;
        Object a22 = ff6.a();
        i2 = qVar.label;
        if (i2 != 0) {
        }
        j2 = ((Number) obj).longValue();
        FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "Update sender with number " + str3 + " id " + j2);
        DianaNotificationObj dianaNotificationObj22 = new DianaNotificationObj(dVar2.a(), bVar2.a().getNotificationType(), bVar2.a(), dVar2.g(), str, (int) j2, str2, list, hf6.a(dVar2.h()), null, null, 1536, null);
        FLogger.INSTANCE.getLocal().d(dianaNotificationComponent.a, "sendDianaNotification " + dianaNotificationObj22);
        dianaNotificationComponent.g.offer(dianaNotificationObj22);
        PortfolioApp.get.instance().b(dianaNotificationComponent.a(), dianaNotificationObj22);
        return cd6.a;
    }

    @DexIgnore
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0043  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0023  */
    public final /* synthetic */ Object a(NotificationMonitor dVar, b bVar, boolean z, xe6<? super cd6> xe6) {
        j jVar;
        int i2;
        if (xe6 instanceof j) {
            jVar = (j) xe6;
            int i3 = jVar.label;
            if ((i3 & Integer.MIN_VALUE) != 0) {
                jVar.label = i3 - Integer.MIN_VALUE;
                Object obj = jVar.result;
                Object a2 = ff6.a();
                i2 = jVar.label;
                if (i2 != 0) {
                    nc6.a(obj);
                    NotificationMonitor a3 = this.b.a((NotificationMonitor) dVar);
                    if (a3 == null) {
                        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationAdd() - this notification existed.");
                    } else if (c() || (xj6.a((CharSequence) a3.e()) && xj6.a((CharSequence) a3.f()))) {
                        return cd6.a;
                    } else {
                        jVar.L$0 = this;
                        jVar.L$1 = dVar;
                        jVar.L$2 = bVar;
                        jVar.Z$0 = z;
                        jVar.L$3 = a3;
                        jVar.label = 1;
                        if (b(a3, bVar, z, jVar) == a2) {
                            return a2;
                        }
                    }
                } else if (i2 == 1) {
                    NotificationMonitor notificationMonitor = (NotificationMonitor) jVar.L$3;
                    boolean z2 = jVar.Z$0;
                    b bVar2 = (b) jVar.L$2;
                    NotificationMonitor notificationMonitor2 = (NotificationMonitor) jVar.L$1;
                    DianaNotificationComponent dianaNotificationComponent = (DianaNotificationComponent) jVar.L$0;
                    nc6.a(obj);
                } else {
                    throw new IllegalStateException("call to 'resume' before 'invoke' with coroutine");
                }
                return cd6.a;
            }
        }
        jVar = new j(this, xe6);
        Object obj2 = jVar.result;
        Object a22 = ff6.a();
        i2 = jVar.label;
        if (i2 != 0) {
        }
        return cd6.a;
    }

    @DexIgnore
    public final void a(NotificationMonitor notificationMonitor, NotificationBaseObj.ANotificationType aNotificationType) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "  Notification Posted: " + notificationMonitor.b());
        ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
        String str2 = this.a;
        local2.d(str2, "  Id: " + notificationMonitor.d());
        ILocalFLogger local3 = FLogger.INSTANCE.getLocal();
        String str3 = this.a;
        local3.d(str3, "  Sender: " + notificationMonitor.e());
        ILocalFLogger local4 = FLogger.INSTANCE.getLocal();
        String str4 = this.a;
        local4.d(str4, "  Title: " + notificationMonitor.g());
        ILocalFLogger local5 = FLogger.INSTANCE.getLocal();
        String str5 = this.a;
        local5.d(str5, "  Text: " + notificationMonitor.f());
        ILocalFLogger local6 = FLogger.INSTANCE.getLocal();
        String str6 = this.a;
        local6.d(str6, "  Summary: " + notificationMonitor.k());
        ILocalFLogger local7 = FLogger.INSTANCE.getLocal();
        String str7 = this.a;
        local7.d(str7, "  IsOngoing: " + notificationMonitor.j());
        ILocalFLogger local8 = FLogger.INSTANCE.getLocal();
        String str8 = this.a;
        local8.d(str8, "  When: " + notificationMonitor.h());
        ILocalFLogger local9 = FLogger.INSTANCE.getLocal();
        String str9 = this.a;
        local9.d(str9, "  Priority: " + notificationMonitor.c());
        ILocalFLogger local10 = FLogger.INSTANCE.getLocal();
        String str10 = this.a;
        StringBuilder sb = new StringBuilder();
        sb.append("  Type: ");
        sb.append(aNotificationType != null ? aNotificationType.name() : null);
        local10.d(str10, sb.toString());
        ILocalFLogger local11 = FLogger.INSTANCE.getLocal();
        String str11 = this.a;
        local11.d(str11, "  IsDownloadEvent: " + notificationMonitor.i());
    }

    @DexIgnore
    public final void b(Context context) {
        FLogger.INSTANCE.getLocal().d(this.a, "unregisterPhoneCallObserver");
        try {
            e eVar = this.f;
            if (eVar != null) {
                context.getContentResolver().unregisterContentObserver(eVar);
                this.f = null;
            }
        } catch (Exception e2) {
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "unregisterPhoneCallObserver e=" + e2);
        }
    }

    @DexIgnore
    public final void b(int i2) {
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationRemoved, id = " + i2);
        c(i2);
    }

    @DexIgnore
    public final synchronized void a(NotificationMonitor notificationMonitor) {
        wg6.b(notificationMonitor, "notification");
        FLogger.INSTANCE.getLocal().d(this.a, "processNotificationRemoved");
        Iterator it = this.b.a(notificationMonitor.d(), notificationMonitor.b()).iterator();
        while (it.hasNext()) {
            NotificationMonitor notificationMonitor2 = (NotificationMonitor) it.next();
            ILocalFLogger local = FLogger.INSTANCE.getLocal();
            String str = this.a;
            local.d(str, "sendNotificationFromQueue, found notification " + notificationMonitor2);
            throw null;
            // DianaNotificationObj dianaNotificationObj = new DianaNotificationObj(notificationMonitor2.a(), NotificationBaseObj.ANotificationType.REMOVED, new DianaNotificationObj.AApplicationName("", notificationMonitor.b(), "", NotificationBaseObj.ANotificationType.REMOVED), notificationMonitor2.g(), notificationMonitor2.e(), -1, notificationMonitor2.f(), qd6.d(NotificationBaseObj.ANotificationFlag.IMPORTANT), Long.valueOf(notificationMonitor2.h()), null, null, 1536, null);
            // c(notificationMonitor2.a());
            // PortfolioApp.get.instance().b(a(), dianaNotificationObj);
        }
    }

    @DexIgnore
    public final void a(Context context) {
        if (xx5.a.d(context)) {
            this.f = new e();
            ContentResolver contentResolver = context.getContentResolver();
            Uri uri = CallLog.Calls.CONTENT_URI;
            e eVar = this.f;
            if (eVar != null) {
                contentResolver.registerContentObserver(uri, false, eVar);
                FLogger.INSTANCE.getLocal().d(this.a, "registerPhoneCallObserver success");
                return;
            }
            wg6.a();
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.fossil.wg6.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Queue<com.misfit.frameworks.buttonservice.model.notification.NotificationBaseObj>, java.lang.String]
     candidates:
      com.fossil.wg6.a(int, int):int
      com.fossil.wg6.a(java.lang.String, java.lang.Object):java.lang.String
      com.fossil.wg6.a(java.lang.Throwable, java.lang.String):T
      com.fossil.wg6.a(int, java.lang.String):void
      com.fossil.wg6.a(java.lang.Object, java.lang.Object):boolean
      com.fossil.wg6.a(java.lang.Object, java.lang.String):void */
    @DexIgnore
    public final void a(int i2, boolean z) {
        NotificationBaseObj t;
        boolean z2;
        ILocalFLogger local = FLogger.INSTANCE.getLocal();
        String str = this.a;
        local.d(str, "onNotificationSentResult, id = " + i2 + ", isSuccess = " + z);
        if (z) {
            ILocalFLogger local2 = FLogger.INSTANCE.getLocal();
            String str2 = this.a;
            local2.d(str2, "onNotificationSentResult, to silent notification, id = " + i2);
            Queue<NotificationBaseObj> queue = this.g;
            wg6.a((Object) queue, "mDeviceNotifications");
            Iterator<NotificationBaseObj> it = queue.iterator();
            while (true) {
                if (!it.hasNext()) {
                    t = null;
                    break;
                }
                t = it.next();
                if (((NotificationBaseObj) t).getUid() == i2) {
                    z2 = true;
                    // continue;
                } else {
                    z2 = false;
                    continue;
                }
                if (z2) {
                    break;
                }
            }
            NotificationBaseObj notificationBaseObj = (NotificationBaseObj) t;
            if (notificationBaseObj != null) {
                notificationBaseObj.toSilentNotification();
            }
        }
    }
}
